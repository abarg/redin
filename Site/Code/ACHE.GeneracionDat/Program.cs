﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using ACHE.Business;
using ACHE.Model.EntityData;
using System.IO;
using System.Data.Entity.Infrastructure;

namespace ACHE.GeneracionDat
{
    static class Program
    {
        static void Main()
        {
            try
            {

                BorrarArchivos();//borro los archivos temporales para que no se vuelvan a pasar

                var listProcesos = obtenerProcesosActuales();

                int idProceso1 = listProcesos.First(x => x.TipoArchivo == 1).NroProceso + 1;
                generar("1", idProceso1.ToString());//ProcesoComerciosBeneficio -

                int idProceso2 = listProcesos.First(x => x.TipoArchivo == 2).NroProceso + 1;
                generar("2", idProceso2.ToString());//ProcesoTarjetaBeneficio

                int idProceso3 = listProcesos.First(x => x.TipoArchivo == 4).NroProceso + 1;
                generar("4", idProceso3.ToString());//ProcesoTarjetasPuntos

                int idProceso4 = listProcesos.First(x => x.TipoArchivo == 5).NroProceso + 1;
                generar("5", idProceso4.ToString());//ProcesoCuentasPuntos

                //int idProceso5 = listProcesos.First(x => x.TipoArchivo == 6).NroProceso + 1;
                //generar("6", idProceso5.ToString());//ProcesoPremiosPuntos

                enviarEmailError("", "Proceso DAT: Proceso finalizado");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, e.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en Main");
                //throw e;
            }
        }

        public static void generar(string TipoArchivo, string NroProceso)
        {

            //Beneficios - Comercios
            if (TipoArchivo.Equals("1"))
            {

                string diaDescuento = (((int)DateTime.Now.DayOfWeek) + 1).ToString();
                generarComerciosBeneficios(NroProceso, 1, diaDescuento);////TODO: ENVIAR DIA DEL DESCUENTO DE MAÑANA.
            }
            //Beneficios - Tarjetas
            else if (TipoArchivo.Equals("2"))
            {
                generarTarjetasBeneficios(NroProceso, 2);
            }
            //Puntos - Comercios
            else if (TipoArchivo.Equals("4"))
            {
                generarTarjetasPuntos(NroProceso, 4);
            }
            //Puntos - Cuentas
            else if (TipoArchivo.Equals("5"))
            {
                generarCuentasPuntos(NroProceso, 5);
            }
            //Puntos - Premios
            else if (TipoArchivo.Equals("6"))
            {
                generarPremiosPuntos(NroProceso, 6);
            }

        }

        #region Puntos

        private static void generarPremiosPuntos(string NroProceso, int TipoArchivo)
        {
            string rutaArchAppConfig = ConfigurationManager.AppSettings["Path.Temp"];
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "PREMCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longCantReg = 6;
            int longNroProcesoPie = 5;
            //string fecha = "";
            int longPie = 80;
            //Contenido --
            string contenidoCompleto = "";
            int longContenido = 80;
            string tipoReg = "D";
            string tipoMov = "";
            string codProd = "";
            int longCodProd = 6;
            string relleno1 = "";
            int longRelleno1 = 15;
            string valorPuntos = "";
            int longValorPuntos = 9;
            string fecDesde = "";
            string fecHasta = "";
            string fecAct = "";
            string desc = "";
            int longDesc = 20;
            string relleno2 = "";
            int longRelleno2 = 2;
            cPremio cPremio;
            List<Premios> listPremios;
            try
            {
                //Si hay datos se crea el archivo
                cPremio = new cPremio();

                listPremios = cPremio.getPremios();
                if (listPremios.Count > 0)
                {
                    cantReg = listPremios.Count.ToString();

                    //Fecha
                    string anio = DateTime.Now.Year.ToString();
                    string mes = DateTime.Now.Month.ToString();
                    string dia = DateTime.Now.Day.ToString();
                    string hora = DateTime.Now.Hour.ToString();
                    string minutos = DateTime.Now.Minute.ToString();
                    string segundos = DateTime.Now.Second.ToString();

                    if (mes.Length.Equals(1))
                        mes = mes.PadLeft(2, '0');
                    if (dia.Length.Equals(1))
                        dia = dia.PadLeft(2, '0');
                    if (hora.Length.Equals(1))
                        hora = hora.PadLeft(2, '0');
                    if (minutos.Length.Equals(1))
                        minutos = minutos.PadLeft(2, '0');
                    if (segundos.Length.Equals(1))
                        segundos = segundos.PadLeft(2, '0');

                    //Nombre del archivo
                    NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                    nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                    rutaArchivo = rutaArchAppConfig + "\\" + Path.GetFileName(nombreCompletoArchivo);

                    //Cabecera
                    NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                    horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                    cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                    cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                    //Pie
                    NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                    cantReg = cantReg.PadLeft(longCantReg, '0');
                    pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                    pieCompleto = pieCompleto.PadRight(longPie, ' ');

                    using (StreamWriter file = new StreamWriter(rutaArchivo))
                    {
                        //Se agrega la cabecera
                        cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                        file.WriteLine(cabeceraCompleta);

                        //Contenido   

                        foreach (Premios oPrem in listPremios)
                        {
                            tipoMov = oPrem.TipoMov;

                            codProd = oPrem.IDMarca.HasValue ? oPrem.Marcas.Codigo + oPrem.Codigo : oPrem.Codigo;
                            codProd = codProd.PadRight(longCodProd, '0');

                            relleno1 = relleno1.PadRight(longRelleno1, ' ');

                            valorPuntos = oPrem.ValorPuntos.ToString().Trim();
                            valorPuntos = valorPuntos.PadLeft(longValorPuntos, '0');

                            if (oPrem.FechaVigenciaDesde == null) fecDesde = DateTime.MinValue.ToString("yyyyMMdd");
                            DateTime dtDesde = oPrem.FechaVigenciaDesde ?? DateTime.MinValue;
                            fecDesde = dtDesde.ToString("yyyyMMdd");

                            if (oPrem.FechaVigenciaHasta == null) fecDesde = DateTime.MinValue.ToString("yyyyMMdd");
                            DateTime dtHasta = oPrem.FechaVigenciaHasta ?? DateTime.MinValue;
                            fecHasta = dtHasta.ToString("yyyyMMdd");

                            fecAct = DateTime.Now.ToString("yyMMddhhmm");

                            if (oPrem.Descripcion == null) oPrem.Descripcion = "";
                            desc = oPrem.Descripcion.Trim();//TODO: Remover y long
                            desc = desc.PadRight(longDesc, ' ');

                            relleno2 = relleno2.PadRight(longRelleno2, ' ');

                            contenidoCompleto = tipoReg + tipoMov + codProd + relleno1 + valorPuntos + fecDesde + fecHasta + fecAct + desc + relleno2;
                            contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                            //Se agrega el Contenido
                            contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                            file.WriteLine(contenidoCompleto);
                        }

                        //Se agrega el pie
                        pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                        file.WriteLine(pieCompleto);
                    }

                    //MoverArchivo(rutaArchivo, nombreCompletoArchivo);

                    actualizarBD(TipoArchivo);
                }
                else
                {
                    throw new Exception("No hay datos para generar el archivo.");
                }
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, ex.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en generarPremiosPuntos");

            }
        }

        private static void generarTarjetasPuntos(string NroProceso, int TipoArchivo)
        {

            string rutaArchAppConfig = ConfigurationManager.AppSettings["Path.Temp"];
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "TARJPTCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longNroProcesoPie = 5;
            int longPie = 80;
            int longRegPie = 6;
            //Contenido --
            string contenidoCompleto = "";
            string caracterContenido = "D";
            int longContenido = 81;
            string tipoMov = "";
            string numTar = "";
            int longNumTar = 19;
            string _longNumTar = "16";
            string codTitular = "T";
            string codResol = "C";
            string AammVto = "4912";
            string cuentaAsoc = "";
            int longCuentaAsoc = 15;
            string impDisponible = "000000000";
            string fecha = "";
            string reservado = "9350000";
            string frecDisponible = " ";
            try
            {
                //Si hay datos se crea el archivo
                using (var dbContext = new ACHEEntities())
                {
                    var listTarjetas = dbContext.Tarjetas//.Where(x => x.Numero.StartsWith("63711"))//Valido que empiece con el BIN
                        .Select(x => new
                        {
                            IDTarjeta = x.IDTarjeta,
                            Numero = x.Numero,
                            //PuntosDisponibles = Math.Round(x.Credito + x.Giftcard),
                            Estado = x.FechaBaja.HasValue ? "29" : "20",
                            Cuenta = x.IDTarjeta
                        }).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        cantReg = listTarjetas.Count.ToString();

                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        rutaArchivo = rutaArchAppConfig + "\\" + Path.GetFileName(nombreCompletoArchivo);

                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        //Pie
                        NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                        cantReg = cantReg.PadLeft(longRegPie, '0');
                        pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                        pieCompleto = pieCompleto.PadRight(longPie, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                tipoMov = "M";

                                numTar = oTar.Numero.Trim();
                                numTar = numTar.PadRight(longNumTar, ' ');

                                cuentaAsoc = oTar.Cuenta.ToString();
                                cuentaAsoc = cuentaAsoc.PadLeft(longCuentaAsoc, '0');
                                fecha = DateTime.Now.ToString("yyMMddHHMM");

                                contenidoCompleto = caracterContenido + tipoMov + numTar + _longNumTar + oTar.Estado + codTitular + codResol + AammVto + cuentaAsoc + impDisponible + fecha + reservado + frecDisponible;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);
                            }

                            //Se agrega el pie
                            pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                            file.WriteLine(pieCompleto);
                        }

                        //MoverArchivo(rutaArchivo, nombreCompletoArchivo);

                        actualizarBD(TipoArchivo);
                    }
                    else
                    {
                        throw new Exception("No hay datos para generar el archivo.");
                    }
                }

            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, ex.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en generarCuentasPuntos");

            }
        }

        private static void generarCuentasPuntos(string NroProceso, int TipoArchivo)
        {
            string rutaArchAppConfig = ConfigurationManager.AppSettings["Path.Temp"];
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "CUENCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 60;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longNroProcesoPie = 5;
            int longPie = 60;
            int longRegPie = 6;
            //Contenido --
            string contenidoCompleto = "";
            string caracterContenido = "D";
            int longContenido = 60;
            string tipoMov = "";
            string numCuenta = "";
            int longNumCuenta = 15;
            string estado = "10";
            string puntosDisponibles = "";
            int longPuntosDisponibles = 9;
            string limiteCanjes = "1000";
            string frecuencia = " ";
            string codigo = "935";
            string marca = "0";
            string fechaVigencia = "0000000000";
            string signo = "+";
            //string fecha = "";
            string fechaUltimaSaldo = DateTime.Now.ToString("yyMMddhhmm");
            //int longFecha = 10;

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 1000;

                    dbContext.Database.ExecuteSqlCommand("exec tmpActualizarArchivosVisa", new object[] { });

                    var listTarjetas = dbContext.InformeTarjetasVisa//.Where(x => x.IDTarjeta == 34050 || x.IDTarjeta == 41859 || x.IDTarjeta == 587)
                        .Select(x => new
                        {
                            Cuenta = x.IDTarjeta,
                            PuntosDisponibles = x.Credito + x.Giftcard,
                            Estado = x.FechaBaja.HasValue ? "19" : "10",
                        }).OrderByDescending(x => x.PuntosDisponibles).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        cantReg = listTarjetas.Count.ToString();

                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        rutaArchivo = rutaArchAppConfig + "\\" + Path.GetFileName(nombreCompletoArchivo);

                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        //Pie
                        NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                        cantReg = cantReg.PadLeft(longRegPie, '0');
                        pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                        pieCompleto = pieCompleto.PadRight(longPie, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                //if (oSoc.TipoMov == null) oSoc.TipoMov = "";
                                tipoMov = "M";// oSoc.TipoMov.Trim();

                                numCuenta = oTar.Cuenta.ToString();
                                numCuenta = numCuenta.PadLeft(longNumCuenta, '0');

                                //if (oTar.PuntosDisponibles == null) oTar.PuntosDisponibles = 0;
                                int puntos = 0;
                                if (oTar.Estado == "10")
                                {
                                    puntos = (int)oTar.PuntosDisponibles;
                                    if (puntos < 0)
                                        puntos = 0;
                                }
                                puntosDisponibles = puntos.ToString();
                                puntosDisponibles = puntosDisponibles.PadLeft(longPuntosDisponibles, '0');

                                contenidoCompleto = caracterContenido + tipoMov + numCuenta + estado + "00" + puntosDisponibles + fechaUltimaSaldo + limiteCanjes + frecuencia + codigo + marca + fechaVigencia + signo;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);

                            }

                            //Se agrega el pie
                            pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                            file.WriteLine(pieCompleto);
                        }

                        actualizarBD(TipoArchivo);
                    }
                    else
                    {
                        throw new Exception("No hay datos para generar el archivo.");
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, ex.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en generarCuentasPuntos");
            }
        }

        #endregion

        #region Beneficios

        private static void generarComerciosBeneficios(string NroProceso, int TipoArchivo, string diaDescuentos)
        {
            string rutaArchAppConfig = ConfigurationManager.AppSettings["Path.Temp"];
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "COMCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "HDR";
            int longNroProcesoCabecera = 10;
            string horaCompleta = "";
            int longCabecera = 123;
            //Contenido --
            string contenidoCompleto = "";
            //string caracterContenido = "D";
            int longContenido = 123;
            string CodAct = "";
            //int longCodAct = 2;
            string NumEst = "";
            //int longNumEst = 15;
            string NombreEstab = "";
            int longNombreEstab = 40;
            string localidad = "";
            int longLocalidad = 15;
            string pais = "AR";
            string rubroNac = "0000";
            string rubroInter = "0000";
            string moneda = "032";
            string estado = "";
            //int longEstado = 2;
            string provincia = "00";
            string banco = "935";
            string sucursal = "935";
            string descuento = "";
            int longDescuento = 2;
            string relleno1 = "0";
            string descuentoVip = "";
            int longDescuentoVip = 2;
            string affinity = "";
            int longAffinity = 4;
            string maxCuotas = "01";
            string tipoPlan = "0";
            string relleno2 = "0000";
            string grupoCerr = "0";
            bComercio bComercio;
            try
            {

                //Si hay datos se crea el archivo
                bComercio = new bComercio();
                var listComercios = bComercio.getComerciosForVisa();
                if (listComercios.Count > 0)
                {
                    //Fecha
                    string anio = DateTime.Now.Year.ToString();
                    string mes = DateTime.Now.Month.ToString();
                    string dia = DateTime.Now.Day.ToString();
                    string hora = DateTime.Now.Hour.ToString();
                    string minutos = DateTime.Now.Minute.ToString();
                    string segundos = DateTime.Now.Second.ToString();

                    if (mes.Length.Equals(1))
                        mes = mes.PadLeft(2, '0');
                    if (dia.Length.Equals(1))
                        dia = dia.PadLeft(2, '0');
                    if (hora.Length.Equals(1))
                        hora = hora.PadLeft(2, '0');
                    if (minutos.Length.Equals(1))
                        minutos = minutos.PadLeft(2, '0');
                    if (segundos.Length.Equals(1))
                        segundos = segundos.PadLeft(2, '0');

                    //Nombre del archivo
                    NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                    nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                    rutaArchivo = rutaArchAppConfig + "\\" + Path.GetFileName(nombreCompletoArchivo);

                    //Cabecera
                    NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                    horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                    cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
                    cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                    using (StreamWriter file = new StreamWriter(rutaArchivo))
                    {
                        //Se agrega la cabecera
                        cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                        file.WriteLine(cabeceraCompleta);

                        //Contenido   
                        foreach (Terminales oCom in listComercios)
                        {
                            try
                            {
                                if (oCom.POSSistema != "SOFT" && oCom.POSSistema != "POS")
                                {

                                    CodAct = oCom.Estado + "135";
                                    NumEst = int.Parse(oCom.NumEst).ToString();
                                    NumEst = NumEst.PadLeft(12, '0');

                                    NombreEstab = oCom.Comercios.NombreEst;
                                    if (NombreEstab == null)
                                        NombreEstab = "";

                                    if (NombreEstab.Length > longNombreEstab)
                                        NombreEstab = NombreEstab.Substring(0, longNombreEstab);
                                    NombreEstab = NombreEstab.PadRight(longNombreEstab, ' ');

                                    if (oCom.Domicilios != null)
                                    {
                                        if (!oCom.Domicilios.Ciudad.HasValue)
                                            localidad = "";
                                        else
                                            localidad = oCom.Domicilios.Ciudades.Nombre;
                                        if (localidad.Length > longLocalidad)
                                            localidad = localidad.Substring(0, longLocalidad);
                                        localidad = localidad.PadRight(longLocalidad, ' ');
                                    }
                                    else
                                    {
                                        localidad = "";
                                        localidad = localidad.PadRight(longLocalidad, ' ');
                                    }

                                    //if (oCom.EstadoBenef == null) oCom.EstadoBenef = "";
                                    //estado = oCom.EstadoBenef;
                                    //estado = estado.PadLeft(longEstado, '0');
                                    if (oCom.Estado == "DE")
                                        estado = "39";
                                    else
                                        estado = "30";

                                    descuentoVip = "";
                                    descuento = "";
                                    //if (oCom.Descuento == null) oCom.Descuento = "";
                                    if (diaDescuentos == "1")
                                    {
                                        descuento = oCom.Descuento.ToString();
                                        if (oCom.DescuentoVip.HasValue)
                                            descuentoVip = oCom.DescuentoVip.Value.ToString();
                                    }
                                    else if (diaDescuentos == "2")
                                    {
                                        descuento = oCom.Descuento2.ToString();
                                        if (oCom.DescuentoVip2.HasValue)
                                            descuentoVip = oCom.DescuentoVip2.Value.ToString();
                                    }
                                    else if (diaDescuentos == "3")
                                    {
                                        descuento = oCom.Descuento3.ToString();
                                        if (oCom.DescuentoVip3.HasValue)
                                            descuentoVip = oCom.DescuentoVip3.Value.ToString();
                                    }
                                    else if (diaDescuentos == "4")
                                    {
                                        descuento = oCom.Descuento4.ToString();
                                        if (oCom.DescuentoVip4.HasValue)
                                            descuentoVip = oCom.DescuentoVip4.Value.ToString();
                                    }
                                    else if (diaDescuentos == "5")
                                    {
                                        descuento = oCom.Descuento5.ToString();
                                        if (oCom.DescuentoVip5.HasValue)
                                            descuentoVip = oCom.DescuentoVip5.Value.ToString();
                                    }
                                    else if (diaDescuentos == "6")
                                    {
                                        descuento = oCom.Descuento6.ToString();
                                        if (oCom.DescuentoVip6.HasValue)
                                            descuentoVip = oCom.DescuentoVip6.Value.ToString();
                                    }
                                    else if (diaDescuentos == "7")
                                    {
                                        descuento = oCom.Descuento7.ToString();
                                        if (oCom.DescuentoVip7.HasValue)
                                            descuentoVip = oCom.DescuentoVip7.Value.ToString();
                                    }

                                    descuento = descuento.PadLeft(longDescuento, '0');
                                    descuentoVip = descuentoVip.PadLeft(longDescuentoVip, '0');

                                    if (oCom.AffinityBenef == null) oCom.AffinityBenef = "";
                                    affinity = oCom.AffinityBenef;
                                    affinity = affinity.PadLeft(longAffinity, '0');

                                    contenidoCompleto = CodAct + NumEst + NombreEstab + localidad + pais + rubroNac + rubroInter + moneda
                                    + estado + provincia + banco + sucursal + descuento + relleno1 + descuentoVip + affinity + maxCuotas + tipoPlan + relleno2 + grupoCerr;
                                    contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                    //Se agrega el Contenido
                                    contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                    file.WriteLine(contenidoCompleto);
                                }
                            }
                            catch (Exception ex)
                            {
                                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                //var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                                //BasicLog.AppendToFile(dir, msg, ex.ToString());
                                //enviarEmailError(msg, "Proceso DAT: Error en generarComerciosBeneficios IDCom= "+ oCom.IDComercio);
                            }
                        }
                    }

                    //MoverArchivo(rutaArchivo, nombreCompletoArchivo);

                    actualizarBD(TipoArchivo);
                }
                else
                {
                    throw new Exception("No hay datos para generar el archivo.");
                }
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, ex.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en generarComerciosBeneficios");
            }
        }

        private static void generarTarjetasBeneficios(string NroProceso, int TipoArchivo)
        {
            string rutaArchAppConfig = ConfigurationManager.AppSettings["Path.Temp"];
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "TARJCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "HDR";
            int longNroProcesoCabecera = 10;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido --
            string contenidoCompleto = "";
            int longContenido = 90;
            string numTar = "";
            int longNumTar = 19;
            string longLongNumTar = "";
            int _longLongNumTar = 2;
            string estado = "";
            int longEstado = 2;
            string limCompra = "";
            int longLimCompra = 9;
            string limAdelanto = "";
            int longLimAdelanto = 9;
            string limCuotas = "";
            int longLimCuotas = 9;
            string fecha = "";
            int longFecha = 9;
            string cuentaAsoc = "";
            int longCuentaAsoc = 15;
            string limProp = "";
            string AammVto = "";
            int longAammVto = 4;
            string affinity = "";
            int longAffinity = 4;

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var listTarjetas = dbContext.Tarjetas.Include("Marcas")//.Where(x => x.IDSocio.HasValue)
                        .Select(x => new
                        {
                            IDTarjeta = x.IDTarjeta,
                            Numero = x.Numero,
                            IDMarca = x.IDMarca,
                            Affinity = x.Marcas.Affinity,
                            //PuntosDisponibles = x.PuntosTotales,
                            Vencimiento = x.FechaVencimiento,
                            Estado = x.Estado
                        }).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        rutaArchivo = rutaArchAppConfig + "\\" + Path.GetFileName(nombreCompletoArchivo);

                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                //if (oTar.IDMarca == 2)
                                //{
                                //if (oSoc.NumTar == null) oSoc.NumTar = "";
                                numTar = oTar.Numero;
                                numTar = numTar.PadRight(longNumTar, ' ');

                                //if (oSoc.LongNumTar == null) oSoc.LongNumTar = "";
                                longLongNumTar = "16";
                                longLongNumTar = longLongNumTar.PadRight(_longLongNumTar, '0');

                                //if (oSoc.Estado == null) oSoc.Estado = "";
                                estado = "20";//oSoc.Estado;
                                if (oTar.Estado == "B" || oTar.Vencimiento < DateTime.Now)
                                    estado = "29";//oSoc.Estado;
                                estado = estado.PadRight(longEstado, '0');

                                //if (oSoc.LimCompra == null) oSoc.LimCompra = "";
                                if (oTar.IDMarca == 2)//CuponIN
                                    limCompra = "000000001";
                                else
                                    limCompra = "000000000";
                                limCompra = limCompra.PadRight(longLimCompra, '0');

                                //if (oSoc.LimAdelanto == null) oSoc.LimAdelanto = "";
                                limAdelanto = "000000000";// oSoc.LimAdelanto;
                                limAdelanto = limAdelanto.PadRight(longLimAdelanto, '0');

                                //if (oSoc.LimCuotas == null) oSoc.LimCuotas = "";
                                limCuotas = "000000000";// oSoc.LimCuotas;
                                limCuotas = limCuotas.PadRight(longLimCuotas, '0');

                                //if (oSoc.Fecha == null) oSoc.Fecha = DateTime.Now;
                                //if (oSoc.Fecha.ToString().Contains("0001")) oSoc.Fecha = DateTime.Now;
                                fecha = formatoJuliano(DateTime.Now);
                                fecha = fecha + hora + minutos;
                                fecha = fecha.PadLeft(longFecha, '0');

                                //if (oSoc.CuentaAsoc == null) oSoc.CuentaAsoc = "";
                                cuentaAsoc = "000000000";//oSoc.CuentaAsoc;
                                cuentaAsoc = cuentaAsoc.PadLeft(longCuentaAsoc, '0');

                                //if (oSoc.LimProp == null) oSoc.LimProp = "";
                                limProp = "T";// oSoc.LimProp;

                                //if (oSoc.AammVto == null) oSoc.AammVto = "4912";
                                AammVto = "4912"; //oSoc.AammVto;
                                AammVto = AammVto.PadLeft(longAammVto, '0');

                                //if (oSoc.AffinityBenef == null) oSoc.AffinityBenef = "";
                                affinity = oTar.Affinity;// "0000";// oSoc.AffinityBenef;
                                affinity = affinity.PadLeft(longAffinity, '0');

                                contenidoCompleto = numTar + longNumTar + estado + limCompra + limAdelanto + limCuotas + fecha + cuentaAsoc + limProp + AammVto + affinity;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);
                                //}
                            }
                        }

                        //MoverArchivo(rutaArchivo, nombreCompletoArchivo);

                        actualizarBD(TipoArchivo);
                    }
                    else
                        throw new Exception("No hay datos para generar el archivo.");
                }

            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, ex.ToString());
                enviarEmailError(msg, "Proceso DAT: Error en generarTarjetasBeneficios");

            }
        }

        #endregion

        private static List<Procesos> obtenerProcesosActuales()
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Procesos.Where(x => x.TipoArchivo == 1 || x.TipoArchivo == 2 || x.TipoArchivo == 4 || x.TipoArchivo == 5 || x.TipoArchivo == 6).ToList();
            }
        }

        private static void actualizarBD(int TipoArchivo)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Procesos.Where(x => x.TipoArchivo == TipoArchivo).FirstOrDefault();
                entity.NroProceso = entity.NroProceso + 1;
                entity.FechaActualizacion = DateTime.Now;
                dbContext.SaveChanges();
            }
        }

        private static void enviarEmailError(string msg, string asunto)
        {
            string emailTo = ConfigurationManager.AppSettings["Email.To"];
            ListDictionary datos = new ListDictionary();
            datos.Add("<DETALLE>", msg);

            bool send = EmailHelper.SendMessage(EmailTemplate.Alertas, datos, emailTo, asunto);
            if (!send)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\Email_XX.log";
                BasicLog.AppendToFile(dir, "Email: " + emailTo, "No se ha podido enviar email a administrador");
            }

        }

        private static string formatoJuliano(DateTime date)
        {
            return string.Format("{0:00000}", (date.Year % 100) * 1000 + date.DayOfYear);
        }

        private static void BorrarArchivos()
        {
            //string destFile = System.IO.Path.Combine(ConfigurationManager.AppSettings["Path.WebDav"], archivo);

            System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["Path.Temp"]);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            //System.IO.File.Move(rutaArchivo, destFile);
            //No hace falta. Se hace por ROBOCOPY por limitaciones de webdav
        }
    }
}
