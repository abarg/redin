﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.IO;
using ACHE.Model.ViewModels;
using System.Data;
using System.Text;
using System.Data.Entity.Infrastructure;

namespace ACHE.Services
{
    public static class GeneracionDat
    {
        public static string generarComerciosBeneficios(string NroProceso, string diaDescuentos, string filePath)
        {
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "COMCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "HDR";
            int longNroProcesoCabecera = 10;
            string horaCompleta = "";
            int longCabecera = 123;
            //Contenido --
            string contenidoCompleto = "";
            //string caracterContenido = "D";
            int longContenido = 123;
            string CodAct = "";
            //int longCodAct = 2;
            string NumEst = "";
            //int longNumEst = 15;
            string NombreEstab = "";
            int longNombreEstab = 40;
            string localidad = "";
            int longLocalidad = 15;
            string pais = "AR";
            string rubroNac = "0000";
            string rubroInter = "0000";
            string moneda = "032";
            string estado = "";
            //int longEstado = 2;
            string provincia = "00";
            string banco = "935";
            string sucursal = "935";
            string descuento = "";
            int longDescuento = 2;
            string relleno1 = "0";
            string descuentoVip = "";
            int longDescuentoVip = 2;
            string affinity = "";
            int longAffinity = 4;
            string maxCuotas = "01";
            string tipoPlan = "0";
            string relleno2 = "0000";
            string grupoCerr = "0";
            bComercio bComercio;
            try
            {
                //Si hay datos se crea el archivo
                bComercio = new bComercio();
                var listComercios = bComercio.getComerciosForVisa();
                if (listComercios.Count > 0)
                {
                    //Fecha
                    string anio = DateTime.Now.Year.ToString();
                    string mes = DateTime.Now.Month.ToString();
                    string dia = DateTime.Now.Day.ToString();
                    string hora = DateTime.Now.Hour.ToString();
                    string minutos = DateTime.Now.Minute.ToString();
                    string segundos = DateTime.Now.Second.ToString();

                    if (mes.Length.Equals(1))
                        mes = mes.PadLeft(2, '0');
                    if (dia.Length.Equals(1))
                        dia = dia.PadLeft(2, '0');
                    if (hora.Length.Equals(1))
                        hora = hora.PadLeft(2, '0');
                    if (minutos.Length.Equals(1))
                        minutos = minutos.PadLeft(2, '0');
                    if (segundos.Length.Equals(1))
                        segundos = segundos.PadLeft(2, '0');

                    //Nombre del archivo
                    NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                    nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                    DirectoryInfo dir = new DirectoryInfo(filePath);
                    if (!dir.Exists)
                        dir.Create();
                    rutaArchivo = filePath + Path.GetFileName(nombreCompletoArchivo);//HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

                    //Cabecera
                    NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                    horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                    cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
                    cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                    using (StreamWriter file = new StreamWriter(rutaArchivo))
                    {
                        //Se agrega la cabecera
                        cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                        file.WriteLine(cabeceraCompleta);

                        //Contenido   
                        foreach (Terminales oCom in listComercios)
                        {

                            try
                            {
                                //if (oCom.Estado == null) oCom.CodAct = "";
                                CodAct = oCom.Estado + "135";
                                //if (oCom.NumEstBenef == null) oCom.NumEstBenef = "";
                                NumEst = int.Parse(oCom.NumEst).ToString();
                                //if (NumEst.Contains("e")) NumEst = Double.Parse(NumEst, System.Globalization.NumberStyles.Float).ToString().Replace('.', ' ');
                                NumEst = NumEst.PadLeft(12, '0');

                                NombreEstab = oCom.Comercios.NombreEst;// bComercio.getComercio(oCom.IDComercio).NombreEst;
                                if (NombreEstab == null)
                                    NombreEstab = "";

                                if (NombreEstab.Length > longNombreEstab)
                                    NombreEstab = NombreEstab.Substring(0, longNombreEstab);
                                NombreEstab = NombreEstab.PadRight(longNombreEstab, ' ');

                                if (oCom.Domicilios != null)
                                {
                                    if (!oCom.Domicilios.Ciudad.HasValue)
                                        localidad = "";
                                    else
                                        localidad = oCom.Domicilios.Ciudades.Nombre;
                                    if (localidad.Length > longLocalidad)
                                        localidad = localidad.Substring(0, longLocalidad);
                                    localidad = localidad.PadRight(longLocalidad, ' ');
                                }
                                else
                                {
                                    localidad = "";
                                    localidad = localidad.PadRight(longLocalidad, ' ');
                                }

                                //if (oCom.EstadoBenef == null) oCom.EstadoBenef = "";
                                //estado = oCom.EstadoBenef;
                                //estado = estado.PadLeft(longEstado, '0');
                                if (oCom.Estado == "DE")
                                    estado = "39";
                                else
                                    estado = "30";

                                descuentoVip = "";
                                descuento = "";
                                //if (oCom.Descuento == null) oCom.Descuento = "";
                                if (diaDescuentos == "1")
                                {
                                    descuento = oCom.Descuento.ToString();
                                    if (oCom.DescuentoVip.HasValue)
                                        descuentoVip = oCom.DescuentoVip.Value.ToString();
                                }
                                else if (diaDescuentos == "2")
                                {
                                    descuento = oCom.Descuento2.ToString();
                                    if (oCom.DescuentoVip2.HasValue)
                                        descuentoVip = oCom.DescuentoVip2.Value.ToString();
                                }
                                else if (diaDescuentos == "3")
                                {
                                    descuento = oCom.Descuento3.ToString();
                                    if (oCom.DescuentoVip3.HasValue)
                                        descuentoVip = oCom.DescuentoVip3.Value.ToString();
                                }
                                else if (diaDescuentos == "4")
                                {
                                    descuento = oCom.Descuento4.ToString();
                                    if (oCom.DescuentoVip4.HasValue)
                                        descuentoVip = oCom.DescuentoVip4.Value.ToString();
                                }
                                else if (diaDescuentos == "5")
                                {
                                    descuento = oCom.Descuento5.ToString();
                                    if (oCom.DescuentoVip5.HasValue)
                                        descuentoVip = oCom.DescuentoVip5.Value.ToString();
                                }
                                else if (diaDescuentos == "6")
                                {
                                    descuento = oCom.Descuento6.ToString();
                                    if (oCom.DescuentoVip6.HasValue)
                                        descuentoVip = oCom.DescuentoVip6.Value.ToString();
                                }
                                else if (diaDescuentos == "7")
                                {
                                    descuento = oCom.Descuento7.ToString();
                                    if (oCom.DescuentoVip7.HasValue)
                                        descuentoVip = oCom.DescuentoVip7.Value.ToString();
                                }

                                descuento = descuento.PadLeft(longDescuento, '0');
                                descuentoVip = descuentoVip.PadLeft(longDescuentoVip, '0');

                                if (oCom.AffinityBenef == null) oCom.AffinityBenef = "";
                                affinity = oCom.AffinityBenef;
                                affinity = affinity.PadLeft(longAffinity, '0');

                                contenidoCompleto = CodAct + NumEst + NombreEstab + localidad + pais + rubroNac + rubroInter + moneda
                                + estado + provincia + banco + sucursal + descuento + relleno1 + descuentoVip + affinity + maxCuotas + tipoPlan + relleno2 + grupoCerr;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);
                            }
                            catch (Exception ex)
                            {
                                var msg = ex.Message;
                            }

                        }
                    }

                    return "/ArchivosTemp/Beneficios/" + nombreCompletoArchivo;
                }
                else
                {
                    throw new Exception("No hay datos para generar el archivo.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string generarTarjetasBeneficios(string NroProceso, string filePath)
        {
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "TARJCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "HDR";
            int longNroProcesoCabecera = 10;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido --
            string contenidoCompleto = "";
            int longContenido = 90;
            string numTar = "";
            int longNumTar = 19;
            string longLongNumTar = "";
            int _longLongNumTar = 2;
            string estado = "";
            int longEstado = 2;
            string limCompra = "";
            int longLimCompra = 9;
            string limAdelanto = "";
            int longLimAdelanto = 9;
            string limCuotas = "";
            int longLimCuotas = 9;
            string fecha = "";
            int longFecha = 9;
            string cuentaAsoc = "";
            int longCuentaAsoc = 15;
            string limProp = "";
            string AammVto = "";
            int longAammVto = 4;
            string affinity = "";
            int longAffinity = 4;

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var listTarjetas = dbContext.Tarjetas.Include("Marcas")//.Where(x => x.IDSocio.HasValue)
                        .Select(x => new
                        {
                            IDTarjeta = x.IDTarjeta,
                            Numero = x.Numero,
                            IDMarca = x.IDMarca,
                            Affinity = x.Marcas.Affinity,
                            //PuntosDisponibles = x.PuntosTotales,
                            Vencimiento = x.FechaVencimiento,
                            Estado = x.Estado
                        }).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        //NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        //nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        //rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        DirectoryInfo dir = new DirectoryInfo(filePath);
                        if (!dir.Exists)
                            dir.Create();
                        rutaArchivo = filePath + Path.GetFileName(nombreCompletoArchivo);//HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);


                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                //if (oTar.IDMarca == 2)
                                //{
                                //if (oSoc.NumTar == null) oSoc.NumTar = "";
                                numTar = oTar.Numero;
                                numTar = numTar.PadRight(longNumTar, ' ');

                                //if (oSoc.LongNumTar == null) oSoc.LongNumTar = "";
                                longLongNumTar = "16";
                                longLongNumTar = longLongNumTar.PadRight(_longLongNumTar, '0');

                                //if (oSoc.Estado == null) oSoc.Estado = "";
                                estado = "20";//oSoc.Estado;
                                if (oTar.Estado == "B" || oTar.Vencimiento.Date < DateTime.Now.Date)
                                    estado = "29";//oSoc.Estado;
                                estado = estado.PadRight(longEstado, '0');

                                //if (oSoc.LimCompra == null) oSoc.LimCompra = "";
                                if (oTar.IDMarca == 2)//CuponIN
                                    limCompra = "000000001";
                                else
                                    limCompra = "000000000";
                                limCompra = limCompra.PadRight(longLimCompra, '0');

                                //if (oSoc.LimAdelanto == null) oSoc.LimAdelanto = "";
                                limAdelanto = "000000000";// oSoc.LimAdelanto;
                                limAdelanto = limAdelanto.PadRight(longLimAdelanto, '0');

                                //if (oSoc.LimCuotas == null) oSoc.LimCuotas = "";
                                limCuotas = "000000000";// oSoc.LimCuotas;
                                limCuotas = limCuotas.PadRight(longLimCuotas, '0');

                                //if (oSoc.Fecha == null) oSoc.Fecha = DateTime.Now;
                                //if (oSoc.Fecha.ToString().Contains("0001")) oSoc.Fecha = DateTime.Now;
                                fecha = formatoJuliano(DateTime.Now);
                                fecha = fecha + hora + minutos;
                                fecha = fecha.PadLeft(longFecha, '0');

                                //if (oSoc.CuentaAsoc == null) oSoc.CuentaAsoc = "";
                                cuentaAsoc = "000000000";//oSoc.CuentaAsoc;
                                cuentaAsoc = cuentaAsoc.PadLeft(longCuentaAsoc, '0');

                                //if (oSoc.LimProp == null) oSoc.LimProp = "";
                                limProp = "T";// oSoc.LimProp;

                                //if (oSoc.AammVto == null) oSoc.AammVto = "4912";
                                AammVto = "4912"; //oSoc.AammVto;
                                AammVto = AammVto.PadLeft(longAammVto, '0');

                                //if (oSoc.AffinityBenef == null) oSoc.AffinityBenef = "";
                                affinity = oTar.Affinity;// "0000";// oSoc.AffinityBenef;
                                affinity = affinity.PadLeft(longAffinity, '0');

                                contenidoCompleto = numTar + longNumTar + estado + limCompra + limAdelanto + limCuotas + fecha + cuentaAsoc + limProp + AammVto + affinity;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);
                                //}
                            }
                        }

                        return "/ArchivosTemp/Beneficios/" + nombreCompletoArchivo;
                    }
                    else
                        throw new Exception("No hay datos para generar el archivo.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string formatoJuliano(DateTime date)
        {
            return string.Format("{0:00000}", (date.Year % 100) * 1000 + date.DayOfYear);
        }

        public static string generarCuentasPuntos(string NroProceso, string filePath)
        {
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "CUENCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 60;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longNroProcesoPie = 5;
            int longPie = 60;
            int longRegPie = 6;
            //Contenido --
            string contenidoCompleto = "";
            string caracterContenido = "D";
            int longContenido = 60;
            string tipoMov = "";
            string numCuenta = "";
            int longNumCuenta = 15;
            string estado = "10";
            string puntosDisponibles = "";
            int longPuntosDisponibles = 9;
            string limiteCanjes = "1000";
            string frecuencia = " ";
            string codigo = "935";
            string marca = "0";
            string fechaVigencia = "0000000000";
            string signo = "+";
            //string fecha = "";
            string fechaUltimaSaldo = DateTime.Now.ToString("yyMMddhhmm");
            //int longFecha = 10;

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 1000;

                    dbContext.Database.ExecuteSqlCommand("exec tmpActualizarArchivosVisa", new object[] { });

                    var listTarjetas = dbContext.InformeTarjetasVisa//.Where(x => x.IDTarjeta == 34050 || x.IDTarjeta == 41859 || x.IDTarjeta == 587)
                        .Select(x => new
                        {
                            Cuenta = x.IDTarjeta,
                            PuntosDisponibles = x.Credito + x.Giftcard,
                            Estado = x.FechaBaja.HasValue ? "19" : "10"
                        }).OrderByDescending(x => x.PuntosDisponibles).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        cantReg = listTarjetas.Count.ToString();

                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        //NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        //nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        //rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/") + Path.GetFileName(nombreCompletoArchivo);
                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        DirectoryInfo dir = new DirectoryInfo(filePath);
                        if (!dir.Exists)
                            dir.Create();
                        rutaArchivo = filePath + Path.GetFileName(nombreCompletoArchivo);//HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        //Pie
                        NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                        cantReg = cantReg.PadLeft(longRegPie, '0');
                        pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                        pieCompleto = pieCompleto.PadRight(longPie, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                //if (oSoc.TipoMov == null) oSoc.TipoMov = "";
                                tipoMov = "M";// oSoc.TipoMov.Trim();

                                numCuenta = oTar.Cuenta.ToString();
                                numCuenta = numCuenta.PadLeft(longNumCuenta, '0');

                                //if (oTar.PuntosDisponibles == null) oTar.PuntosDisponibles = 0;
                                int puntos = 0;
                                if (oTar.Estado == "10")
                                {
                                    puntos = (int)oTar.PuntosDisponibles;
                                    if (puntos < 0)
                                        puntos = 0;
                                }
                                puntosDisponibles = puntos.ToString();
                                puntosDisponibles = puntosDisponibles.PadLeft(longPuntosDisponibles, '0');

                                contenidoCompleto = caracterContenido + tipoMov + numCuenta + estado + "00" + puntosDisponibles + fechaUltimaSaldo + limiteCanjes + frecuencia + codigo + marca + fechaVigencia + signo;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);

                            }

                            //Se agrega el pie
                            pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                            file.WriteLine(pieCompleto);
                        }

                        return "/ArchivosTemp/Puntos/" + nombreCompletoArchivo;
                    }
                    else
                    {
                        throw new Exception("No hay datos para generar el archivo.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string generarPremiosPuntos(string NroProceso, string filePath)
        {
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "PREMCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longCantReg = 6;
            int longNroProcesoPie = 5;
            //string fecha = "";
            //int longFecha = 8;
            int longPie = 80;
            //int longRegPie = 6;
            //Contenido --
            string contenidoCompleto = "";
            int longContenido = 80;
            string tipoReg = "D";
            string tipoMov = "";
            string codProd = "";
            int longCodProd = 6;
            string relleno1 = "";
            int longRelleno1 = 15;
            string valorPuntos = "";
            int longValorPuntos = 9;
            string fecDesde = "";
            //int longFecDesde = 8;
            string fecHasta = "";
            //int longFecHasta = 8;
            string fecAct = "";
            //int longFecAct = 10;
            string desc = "";
            int longDesc = 20;
            string relleno2 = "";
            int longRelleno2 = 2;
            cPremio cPremio;
            List<Premios> listPremios;
            try
            {
                //Si hay datos se crea el archivo
                cPremio = new cPremio();

                listPremios = cPremio.getPremios();
                if (listPremios.Count > 0)
                {
                    cantReg = listPremios.Count.ToString();

                    //Fecha
                    string anio = DateTime.Now.Year.ToString();
                    string mes = DateTime.Now.Month.ToString();
                    string dia = DateTime.Now.Day.ToString();
                    string hora = DateTime.Now.Hour.ToString();
                    string minutos = DateTime.Now.Minute.ToString();
                    string segundos = DateTime.Now.Second.ToString();

                    if (mes.Length.Equals(1))
                        mes = mes.PadLeft(2, '0');
                    if (dia.Length.Equals(1))
                        dia = dia.PadLeft(2, '0');
                    if (hora.Length.Equals(1))
                        hora = hora.PadLeft(2, '0');
                    if (minutos.Length.Equals(1))
                        minutos = minutos.PadLeft(2, '0');
                    if (segundos.Length.Equals(1))
                        segundos = segundos.PadLeft(2, '0');

                    //Nombre del archivo
                    //NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                    //nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                    //rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/") + Path.GetFileName(nombreCompletoArchivo);

                    NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                    nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                    DirectoryInfo dir = new DirectoryInfo(filePath);
                    if (!dir.Exists)
                        dir.Create();
                    rutaArchivo = filePath + Path.GetFileName(nombreCompletoArchivo);//HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

                    //Cabecera
                    NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                    horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                    cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                    cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                    //Pie
                    NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                    cantReg = cantReg.PadLeft(longCantReg, '0');
                    pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                    pieCompleto = pieCompleto.PadRight(longPie, ' ');

                    using (StreamWriter file = new StreamWriter(rutaArchivo))
                    {
                        //Se agrega la cabecera
                        cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                        file.WriteLine(cabeceraCompleta);

                        //Contenido   

                        foreach (Premios oPrem in listPremios)
                        {
                            tipoMov = oPrem.TipoMov;

                            //if (oPrem.Codigo == null) oPrem.Codigo = "";
                            codProd = oPrem.IDMarca.HasValue ? oPrem.Marcas.Codigo + oPrem.Codigo : oPrem.Codigo;
                            codProd = codProd.PadRight(longCodProd, '0');

                            relleno1 = relleno1.PadRight(longRelleno1, ' ');

                            //if (oPrem.ValorPuntos == null) oPrem.ValorPuntos = 0;
                            valorPuntos = oPrem.ValorPuntos.ToString().Trim();
                            valorPuntos = valorPuntos.PadLeft(longValorPuntos, '0');

                            if (oPrem.FechaVigenciaDesde == null) fecDesde = DateTime.MinValue.ToString("yyyyMMdd");
                            DateTime dtDesde = oPrem.FechaVigenciaDesde ?? DateTime.MinValue;
                            fecDesde = dtDesde.ToString("yyyyMMdd");

                            if (oPrem.FechaVigenciaHasta == null) fecDesde = DateTime.MinValue.ToString("yyyyMMdd");
                            DateTime dtHasta = oPrem.FechaVigenciaHasta ?? DateTime.MinValue;
                            fecHasta = dtHasta.ToString("yyyyMMdd");

                            fecAct = DateTime.Now.ToString("yyMMddhhmm");

                            if (oPrem.Descripcion == null) oPrem.Descripcion = "";
                            desc = oPrem.Descripcion.Trim();//TODO: Remover y long
                            desc = desc.PadRight(longDesc, ' ');

                            relleno2 = relleno2.PadRight(longRelleno2, ' ');

                            contenidoCompleto = tipoReg + tipoMov + codProd + relleno1 + valorPuntos + fecDesde + fecHasta + fecAct + desc + relleno2;
                            contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                            //Se agrega el Contenido
                            contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                            file.WriteLine(contenidoCompleto);
                        }

                        //Se agrega el pie
                        pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                        file.WriteLine(pieCompleto);
                    }

                    return "/ArchivosTemp/Puntos/" + nombreCompletoArchivo;
                }
                else
                {
                    throw new Exception("No hay datos para generar el archivo.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string generarTarjetasPuntos(string NroProceso, string filePath)
        {
            string rutaArchivo = "";
            string _nroProceso = NroProceso;
            //Datos del nombre --
            string nombreCompletoArchivo = "";
            string nombreArchivo = "TARJPTCLUBIN_";
            string extensionArchivo = ".DAT";
            int longNroProcesoArchivo = 10;
            //Contenido: Cabecera --
            string cabeceraCompleta = "";
            string caracterCabecera = "C";
            int longNroProcesoCabecera = 5;
            string horaCompleta = "";
            int longCabecera = 80;
            //Contenido: Pie --
            string pieCompleto = "";
            string caracterPie = "T";
            string cantReg = "";
            int longNroProcesoPie = 5;
            int longPie = 80;
            int longRegPie = 6;
            //Contenido --
            string contenidoCompleto = "";
            string caracterContenido = "D";
            int longContenido = 81;
            string tipoMov = "";
            string numTar = "";
            int longNumTar = 19;
            string _longNumTar = "16";
            //int longLongNumTar = 2;
            //string estado = "20";
            string codTitular = "T";
            string codResol = "C";
            string AammVto = "4912";
            string cuentaAsoc = "";
            int longCuentaAsoc = 15;
            string impDisponible = "000000000";
            //int longImpDisponible = 9;
            string fecha = "";
            //int longFecha = 10;
            string reservado = "9350000";
            string frecDisponible = " ";
            try
            {
                //Si hay datos se crea el archivo
                using (var dbContext = new ACHEEntities())
                {
                    var listTarjetas = dbContext.Tarjetas//.Where(x => x.Numero.StartsWith("63711"))//Valido que empiece con el BIN
                        .Select(x => new
                        {
                            IDTarjeta = x.IDTarjeta,
                            Numero = x.Numero,
                        //PuntosDisponibles = Math.Round(x.Credito + x.Giftcard),
                        Estado = x.FechaBaja.HasValue ? "29" : "20",
                            Cuenta = x.IDTarjeta
                        }).ToList();

                    if (listTarjetas.Count > 0)
                    {
                        cantReg = listTarjetas.Count.ToString();

                        //Fecha
                        string anio = DateTime.Now.Year.ToString();
                        string mes = DateTime.Now.Month.ToString();
                        string dia = DateTime.Now.Day.ToString();
                        string hora = DateTime.Now.Hour.ToString();
                        string minutos = DateTime.Now.Minute.ToString();
                        string segundos = DateTime.Now.Second.ToString();

                        if (mes.Length.Equals(1))
                            mes = mes.PadLeft(2, '0');
                        if (dia.Length.Equals(1))
                            dia = dia.PadLeft(2, '0');
                        if (hora.Length.Equals(1))
                            hora = hora.PadLeft(2, '0');
                        if (minutos.Length.Equals(1))
                            minutos = minutos.PadLeft(2, '0');
                        if (segundos.Length.Equals(1))
                            segundos = segundos.PadLeft(2, '0');

                        //Nombre del archivo
                        //NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        //nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        //rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/") + Path.GetFileName(nombreCompletoArchivo);
                        NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                        nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                        DirectoryInfo dir = new DirectoryInfo(filePath);
                        if (!dir.Exists)
                            dir.Create();
                        rutaArchivo = filePath + Path.GetFileName(nombreCompletoArchivo);//HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);


                        //Cabecera
                        NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                        horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                        cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                        cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                        //Pie
                        NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                        cantReg = cantReg.PadLeft(longRegPie, '0');
                        pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                        pieCompleto = pieCompleto.PadRight(longPie, ' ');

                        using (StreamWriter file = new StreamWriter(rutaArchivo))
                        {
                            //Se agrega la cabecera
                            cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                            file.WriteLine(cabeceraCompleta);

                            //Contenido   
                            foreach (var oTar in listTarjetas)
                            {
                                //if (oSoc.TipoMov == null) oSoc.TipoMov = "";
                                tipoMov = "M";// oSoc.TipoMov.Trim();

                                numTar = oTar.Numero.Trim();
                                numTar = numTar.PadRight(longNumTar, ' ');

                                //_longNumTar = longNumTar.ToString();
                                //_longNumTar = _longNumTar.PadRight(longLongNumTar, '0');

                                cuentaAsoc = oTar.Cuenta.ToString();
                                cuentaAsoc = cuentaAsoc.PadLeft(longCuentaAsoc, '0');

                                //impDisponible = oTar.PuntosDisponibles.ToString();
                                //impDisponible = impDisponible.PadLeft(longImpDisponible, '0');

                                //if (oSoc.Fecha == null) oSoc.Fecha = DateTime.MinValue;
                                fecha = DateTime.Now.ToString("yyMMddHHMM");
                                //fecha = fecha.PadLeft(longFecha, '0');

                                //estado = oTar.Estado;
                                contenidoCompleto = caracterContenido + tipoMov + numTar + _longNumTar + oTar.Estado + codTitular + codResol + AammVto + cuentaAsoc + impDisponible + fecha + reservado + frecDisponible;
                                contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                                //Se agrega el Contenido
                                contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                                file.WriteLine(contenidoCompleto);
                            }

                            //Se agrega el pie
                            pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                            file.WriteLine(pieCompleto);
                        }

                        return "/ArchivosTemp/Puntos/" + nombreCompletoArchivo;
                    }
                    else
                    {
                        throw new Exception("No hay datos para generar el archivo.");
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
