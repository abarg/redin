﻿namespace ACHE.FacturaElectronica
{
    public class FEItemDetalle
    {
        public int Cantidad { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }

        public double Total
        {
            get
            {
                return Cantidad * Precio;
            }
        }
    }
}
