﻿namespace ACHE.FacturaElectronica
{
    public enum FETipoComprobante
    {
        SIN_DEFINIR = -1,
        FACTURAS_A = 1,
        NOTAS_DEBITO_A = 2,
        NOTAS_CREDITO_A = 3,
        FACTURAS_B = 6,
        NOTAS_DEBITO_B = 7,
        NOTAS_CREDITO_B = 8,
        FACTURAS_C = 11,
        NOTAS_DEBITO_C = 12,
        NOTAS_CREDITO_C = 13,
        FACTURAS_E = 19,
        NOTAS_DEBITO_E = 20,
        NOTAS_CREDITO_E = 21
    }
}