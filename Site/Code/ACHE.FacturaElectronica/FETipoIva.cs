﻿namespace ACHE.FacturaElectronica
{
    public enum FETipoIva
    {
        Iva0 = 3,
        Iva10_5 = 4,
        Iva21 = 5,
        Iva27 = 6
    }
}
