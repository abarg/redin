﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using ACHE.FacturaElectronica.Lib;
using ACHE.FacturaElectronica.WSFEV1;
using iTextSharp.text.pdf;
using BarcodeLib;
using System.Drawing;
using System.Web;

namespace ACHE.FacturaElectronica
{
    public class FEFacturaElectronica
    {
        private static object bloqueo = new object();

        public void GenerarComprobante(FEComprobante comprobante, string urlWsaaWsdl, string certificadoAfip, string templateFc)
        {
            lock (bloqueo)
            {
                Service objWSFEV1 = new Service();
                FEAuthRequest objFEAuthRequest = new FEAuthRequest();
                FETicket ticket = FEAutenticacion.GetTicket(comprobante.Cuit, "wsfe", urlWsaaWsdl, certificadoAfip);

                objWSFEV1.Url = ConfigurationManager.AppSettings["wsfev1"];

                objFEAuthRequest.Token = ticket.Token;
                objFEAuthRequest.Sign = ticket.Sign;
                objFEAuthRequest.Cuit = comprobante.Cuit;

                FECAECabRequest objFECAECabRequest = new FECAECabRequest();
                FECAERequest objFECAERequest = new FECAERequest();
                FECAEResponse objFECAEResponse = new FECAEResponse();

                objFECAECabRequest.CantReg = 1;
                objFECAECabRequest.PtoVta = comprobante.PtoVta;
                objFECAECabRequest.CbteTipo = (int)comprobante.TipoComprobante;


                FECAEDetRequest[] objFECAEDetRequest = new FECAEDetRequest[1];
                comprobante.NumeroComprobante = GetUltimoComprobante(comprobante.Cuit, comprobante.PtoVta, comprobante.TipoComprobante, objFEAuthRequest) + 1;

                objFECAEDetRequest[0] = new FECAEDetRequest();
                objFECAEDetRequest[0].Concepto = (int)comprobante.Concepto;
                objFECAEDetRequest[0].DocTipo = comprobante.DocTipo;
                objFECAEDetRequest[0].DocNro = comprobante.DocNro;
                objFECAEDetRequest[0].CbteDesde = comprobante.NumeroComprobante;
                objFECAEDetRequest[0].CbteHasta = comprobante.NumeroComprobante;
                objFECAEDetRequest[0].CbteFch = comprobante.Fecha.ToString("yyyyMMdd");
                objFECAEDetRequest[0].ImpTotal = Math.Round(comprobante.ImpTotal, 2);
                objFECAEDetRequest[0].ImpTotConc = Math.Round(comprobante.ImpTotConc, 2);
                objFECAEDetRequest[0].ImpNeto = Math.Round(comprobante.ImpNeto, 2);
                objFECAEDetRequest[0].ImpOpEx = Math.Round(comprobante.ImpOpEx, 2);
                objFECAEDetRequest[0].ImpTrib = Math.Round(comprobante.Tributos.Sum(t => t.Importe), 2);
                objFECAEDetRequest[0].ImpIVA = Math.Round(comprobante.DetalleIva.Sum(i => i.Importe), 2);
                objFECAEDetRequest[0].FchServDesde = comprobante.FchServDesde == null ? "" : comprobante.FchServDesde.Value.ToString("yyyyMMdd");
                objFECAEDetRequest[0].FchServHasta = comprobante.FchServHasta == null ? "" : comprobante.FchServHasta.Value.ToString("yyyyMMdd");
                objFECAEDetRequest[0].FchVtoPago = comprobante.FchVtoPago == null ? "" : comprobante.FchVtoPago.Value.ToString("yyyyMMdd");
                objFECAEDetRequest[0].MonId = comprobante.CodigoMoneda;
                objFECAEDetRequest[0].MonCotiz = comprobante.CotizacionMoneda;

                if (comprobante.DetalleIva.Count > 0)
                {
                    objFECAEDetRequest[0].Iva = new AlicIva[comprobante.DetalleIva.Count];

                    int regNo = 0;
                    foreach (FERegistroIVA regIva in comprobante.DetalleIva)
                        objFECAEDetRequest[0].Iva[regNo++] = new AlicIva() { BaseImp = Math.Round(regIva.BaseImp, 2), Importe = Math.Round(regIva.Importe, 2), Id = (int)regIva.TipoIva };
                }

                objFECAERequest.FeCabReq = objFECAECabRequest;
                objFECAERequest.FeDetReq = objFECAEDetRequest;

                //Invoco al método FECAEARegInformativo
                try
                {
                    objFECAEResponse = objWSFEV1.FECAESolicitar(objFEAuthRequest, objFECAERequest);
                    if (objFECAEResponse != null && objFECAEResponse.FeDetResp != null && !string.IsNullOrEmpty(objFECAEResponse.FeDetResp[0].CAE))
                    {
                        comprobante.CAE = objFECAEResponse.FeDetResp[0].CAE;
                        comprobante.FechaVencCAE = DateTime.ParseExact(objFECAEResponse.FeDetResp[0].CAEFchVto, "yyyyMMdd", null);
                        comprobante.ArchivoFactura = GetStreamPDF(comprobante, templateFc);
                    }

                    if (objFECAEResponse.Errors != null)
                    {
                        throw new Exception("Error: " + objFECAEResponse.Errors[0].Code + " - " + objFECAEResponse.Errors[0].Msg);
                    }
                    else if (objFECAEResponse != null && objFECAEResponse.FeDetResp != null && string.IsNullOrEmpty(objFECAEResponse.FeDetResp[0].CAE))
                    {
                        StringBuilder sb = new StringBuilder();

                        foreach (Obs obs in objFECAEResponse.FeDetResp[0].Observaciones)
                            sb.AppendLine(obs.Msg);

                        throw new Exception("No se pudo obtener el CAE, revise los datos cargados.\n" + sb.ToString());
                    }
                }
                catch
                {
                    throw;
                }
            }
        }

        private int GetUltimoComprobante(long cuit, int ptoVta, FETipoComprobante tipoComprobante, FEAuthRequest ticket)
        {
            Service objWSFE = new Service();

            FERecuperaLastCbteResponse ultimo = objWSFE.FECompUltimoAutorizado(ticket, ptoVta, (int)tipoComprobante);
            if (ultimo.Errors != null && ultimo.Errors.Any())
                throw new Exception(ultimo.Errors[0].Msg);
            return ultimo.CbteNro;
        }

        public void GrabarEnDisco(FEComprobante comprobante, string archivoDestino, string templateFc)
        {
            Stream streamPDF = GetStreamPDF(comprobante, templateFc);

            using (Stream destination = File.Create(archivoDestino))
            {
                for (int a = streamPDF.ReadByte(); a != -1; a = streamPDF.ReadByte())
                    destination.WriteByte((byte)a);
            }
        }

        public void ImprimirFacturaElectronicaToResponse(FEComprobante comprobante, string templateFc)
        {
            FEFacturaElectronica fe = new FEFacturaElectronica();
            Stream stream = GetStreamPDF(comprobante, templateFc);
            HttpResponse Response = HttpContext.Current.Response;

            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=Comprobante.pdf");
            Response.BinaryWrite(buffer);
            // myMemoryStream.WriteTo(Response.OutputStream); //works too
            Response.Flush();
            Response.Close();
            Response.End();
        }

        private Stream GetStreamPDF(FEComprobante comprobante, string templateFc)
        {
            var template = (templateFc == string.Empty ? ConfigurationManager.AppSettings["Template"] : templateFc);
            NFPDFWriter pdf = new NFPDFWriter(MedidasDocumento.A4, template);
            string letra = "";
            string comp = "";
            string codigo = "";

            codigo = ((int)comprobante.TipoComprobante).ToString().PadLeft(2, '0');

            switch (comprobante.TipoComprobante)
            {
                case FETipoComprobante.FACTURAS_A:
                    letra = "A";
                    comp = "FACTURA";
                    break;
                case FETipoComprobante.FACTURAS_B:
                    letra = "B";
                    comp = "FACTURA";
                    break;
                case FETipoComprobante.FACTURAS_C:
                    letra = "C";
                    comp = "FACTURA";
                    break;
                case FETipoComprobante.FACTURAS_E:
                    letra = "E";
                    comp = "FACTURA";
                    break;

                case FETipoComprobante.NOTAS_DEBITO_A:
                    letra = "A";
                    comp = "NOTA DE DÉBITO";
                    break;
                case FETipoComprobante.NOTAS_DEBITO_B:
                    letra = "B";
                    comp = "NOTA DE DÉBITO";
                    break;
                case FETipoComprobante.NOTAS_DEBITO_C:
                    letra = "C";
                    comp = "NOTA DE DÉBITO";
                    break;
                case FETipoComprobante.NOTAS_DEBITO_E:
                    letra = "E";
                    comp = "NOTA DE DÉBITO";
                    break;

                case FETipoComprobante.NOTAS_CREDITO_A:
                    letra = "A";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.NOTAS_CREDITO_B:
                    letra = "B";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.NOTAS_CREDITO_C:
                    letra = "C";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.NOTAS_CREDITO_E:
                    letra = "E";
                    comp = "NOTA DE CRÉDITO";
                    break;
            }

            pdf.EscribirXY(letra, 300, 50, 22, Alineado.Centro);
            pdf.EscribirXY("Cod. " + codigo, 300, 60, 10, Alineado.Centro);
            pdf.EscribirXY(comp + " Nº: " + comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0'), 331, 50, 10, Alineado.Izquierda);
            pdf.EscribirXY("Fecha: " + comprobante.Fecha.ToString("dd/MM/yyyy"), 331, 63, 10, Alineado.Izquierda);

            pdf.EscribirXY("Señor/es: " + comprobante.ClienteNombre, 27, 133, 10, Alineado.Izquierda);
            pdf.EscribirXY("Domicilio: " + comprobante.ClienteDomicilio, 27, 148, 10, Alineado.Izquierda);
            pdf.EscribirXY("Localidad: " + comprobante.ClienteLocalidad, 27, 163, 10, Alineado.Izquierda);
            pdf.EscribirXY("IVA: " + comprobante.ClienteCondiionIva, 27, 178, 10, Alineado.Izquierda);
            pdf.EscribirXY("CUIT: " + comprobante.DocNro, 330, 178, 10, Alineado.Izquierda);

            pdf.EscribirXY(comprobante.CondicionVenta, 119, 210, 10, Alineado.Izquierda);

            if (letra == "A")
            {
                pdf.EscribirXY("Subtotal: " + Math.Abs(comprobante.ImpNeto).ToString("N2"), 570, 650, 10, Alineado.Derecha);

                foreach (FERegistroIVA rIVA in comprobante.DetalleIva)
                {
                    if (rIVA.Importe != 0)
                    {
                        switch (rIVA.TipoIva)
                        {
                            case FETipoIva.Iva0:
                                pdf.EscribirXY("IVA 0%: " + Math.Abs(rIVA.Importe).ToString("N2"), 570, 665, 10, Alineado.Derecha);
                                break;

                            case FETipoIva.Iva10_5:
                                pdf.EscribirXY("IVA 10.5%: " + Math.Abs(rIVA.Importe).ToString("N2"), 570, 680, 10, Alineado.Derecha);
                                break;

                            case FETipoIva.Iva21:
                                pdf.EscribirXY("IVA 21%: " + Math.Abs(rIVA.Importe).ToString("N2"), 570, 695, 10, Alineado.Derecha);
                                break;

                            case FETipoIva.Iva27:
                                pdf.EscribirXY("IVA 27%: " + Math.Abs(rIVA.Importe).ToString("N2"), 570, 710, 10, Alineado.Derecha);
                                break;
                        }
                    }
                }

                /*if (factura.Iva != null)
                    pdf.EscribirXY(Math.Abs(factura.Iva.Value).ToString("N2") + "%", 450, 693, 10, Alineado.Derecha);*/
            }

            pdf.EscribirXY(Math.Abs(comprobante.ImpTotal).ToString("N2"), 570, 727, 10, Alineado.Derecha);
            pdf.EscribirBoxXY("Son Pesos " + NumeroALetrasMoneda(comprobante.ImpTotal) + ".", 27, 630, 10, 400);
            pdf.EscribirBoxXY(comprobante.Observaciones, 27, 680, 10, 430);
            pdf.EscribirXY("CAE: " + comprobante.CAE, 27, 727, 10, Alineado.Izquierda);
            pdf.EscribirXY("Vencimiento CAE: " + comprobante.FechaVencCAE.ToString("dd/MM/yyyy"), 27, 745, 10, Alineado.Izquierda);

            int i = 0;
            double itemPrecio;
            int fontDetalleFE = 10;

            if (ConfigurationManager.AppSettings["FontDetalleFE"] != null) fontDetalleFE = int.Parse(ConfigurationManager.AppSettings["FontDetalleFE"]);

            List<FEItemDetalle> items = new List<FEItemDetalle>();
        
            pdf.InsertarTablaDetalle(comprobante.ItemsDetalle);
            pdf.InsertarImagenXY(CodigoBarrasCAE(comprobante.Cuit.ToString(), codigo, comprobante.PtoVta.ToString().PadLeft(4, '0'), comprobante.CAE, comprobante.FechaVencCAE), 27, 53);

            return pdf.GenerarPDFStream();
        }

        private static Image CodigoBarrasCAE(string cuit, string codigoComprobante, string ptovta, string cae, DateTime vencimiento)
        {
            BarcodeLib.Barcode bar = new BarcodeLib.Barcode();
            string barcode;

            barcode = cuit + codigoComprobante + ptovta + cae + vencimiento.ToString("yyyyMMdd");
            barcode = barcode + DigitoVerificador(barcode);
            bar.Width = 340;
            bar.Height = 35;
            bar.ImageFormat = ImageFormat.Jpeg;
            bar.IncludeLabel = true;
            bar.LabelPosition = LabelPositions.BOTTOMLEFT;
            bar.Alignment = AlignmentPositions.LEFT;
            bar.LabelFont = new Font("Verdana", 6, FontStyle.Regular);
            //bar.Encode(TYPE.Interleaved2of5, barcode);
            //bar.SaveImage(@"c:\imagen.jpg", SaveTypes.JPG);

            return bar.Encode(TYPE.Interleaved2of5, barcode);
        }

        private static int DigitoVerificador(string codigoBarras)
        {
            int digito;
            int pares = 0;
            int impares = 0;

            for (int i = 0; i < codigoBarras.Length; i++)
            {
                if (i % 2 == 0)
                    pares = pares + int.Parse(codigoBarras.Substring(i, 1));
                else
                    impares = impares + int.Parse(codigoBarras.Substring(i, 1));
            }

            digito = 10 - ((pares + (3 * impares)) % 10);

            if (digito == 10)
                digito = 0;

            return digito;
        }

        public static string NumeroALetrasMoneda(double value)
        {
            string[] total = value.ToString().Replace(',', '.').Split('.');
            double total_entero = double.Parse(total[0]);
            double total_decimales = 0;

            if (total.Length > 1)
            {
                total[1] = (total[1] + "0").Substring(0, 2);
                total_decimales = double.Parse(total[1]);
            }

            return NumeroALetras(total_entero) + " con " + NumeroALetras(total_decimales);
        }

        private static string NumeroALetras(double value)
        {
            string Num2Text = "";

            value = Math.Abs(Math.Truncate(value));

            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + NumeroALetras(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + NumeroALetras(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";


            else if (value < 100) Num2Text = NumeroALetras(Math.Truncate(value / 10) * 10) + " Y " + NumeroALetras(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + NumeroALetras(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800))
                Num2Text = NumeroALetras(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000)
                Num2Text = NumeroALetras(Math.Truncate(value / 100) * 100) + " " + NumeroALetras(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + NumeroALetras(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + NumeroALetras(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + NumeroALetras(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000000)) + " MILLONES ";

                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0)
                    Num2Text = Num2Text + " " + NumeroALetras(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000)
                Num2Text = "UN BILLON " +
                           NumeroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0)
                    Num2Text = Num2Text + " " +
                               NumeroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }

            return Num2Text;
        }
    }
}
