﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;

namespace ACHE.FacturaElectronica
{
    public static class FEAutenticacion
    {
        private static readonly Hashtable tickets = new Hashtable();
        private static object bloqueo = new object();

        public static FETicket GetTicket(long cuit, string servicio, string urlWsaaWsdl, string certificadoAfip)
        {
            lock (bloqueo)
            {
                if (tickets.ContainsKey(cuit + "|" + servicio))
                {
                    if (((FETicket)tickets[cuit + "|" + servicio]).Vencimiento <= DateTime.Now.AddMinutes(20))
                    {
                        tickets.Remove(cuit + "|" + servicio);
                        return GenerarTicket(cuit, servicio, urlWsaaWsdl, certificadoAfip);
                    }
                    else
                        return (FETicket)tickets[cuit + "|" + servicio];
                }
                else
                    return GenerarTicket(cuit, servicio, urlWsaaWsdl, certificadoAfip);
            }
        }

        private static FETicket GenerarTicket(long cuit, string servicio, string urlWsaaWsdl, string certificadoAfip)
        {
            FETicket ticket = new FETicket();
            LoginTicket objTicketRespuesta;
            string strTicketRespuesta;
            string strUrlWsaaWsdl = (urlWsaaWsdl == string.Empty ? ConfigurationManager.AppSettings["wsaa"] : urlWsaaWsdl);
            string strRutaCertSigner = (certificadoAfip == string.Empty ? ConfigurationManager.AppSettings["CertificadoAFIP"] : certificadoAfip);

            try
            {
                if (!File.Exists(strRutaCertSigner))
                    throw new Exception("El certificado no existe en " + strRutaCertSigner);
                
                objTicketRespuesta = new LoginTicket();
                strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(servicio, strUrlWsaaWsdl, strRutaCertSigner, false);

                ticket.Cuit = cuit;
                ticket.Creado = objTicketRespuesta.GenerationTime;
                ticket.Servicio = objTicketRespuesta.Service;
                ticket.Sign = objTicketRespuesta.Sign;
                ticket.Token = objTicketRespuesta.Token;
                ticket.UniqueID = objTicketRespuesta.UniqueId;
                ticket.Vencimiento = objTicketRespuesta.ExpirationTime;

                lock (bloqueo)
                {
                    tickets.Add(cuit + "|" + servicio, ticket);
                }
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                throw excepcionAlObtenerTicket;
            }

            return ticket;
        }
    }
}