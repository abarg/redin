﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections.Specialized;

public partial class login : PaginaBase
{
    protected static string mensaje = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //LogOut
            if (!String.IsNullOrEmpty(Request.QueryString["logOut"]))
            {
                if (Request.QueryString["logOut"].Equals("true"))
                    Session.Remove("CurrentUser");            
            }
        }           
    }

    [WebMethod(true)]
    public static void ingresar(string usuario, string pwd)
    {
        bUsuario bUsuario = new bUsuario();
        Usuarios oUsuario;

        oUsuario = bUsuario.getUsuario(usuario, pwd);
        HttpContext.Current.Session.Remove("CurrentFranquiciasUser");            

        if (oUsuario != null)
            HttpContext.Current.Session["CurrentUser"] = oUsuario;
        else
        {
            mensaje = "Usuario y/o contraseña incorrecta.";
            throw new Exception(mensaje);
        }
    }

    [WebMethod(true)]
    public static void RecuperarDatos(string email)
    {
        bUsuario bUsuario = new bUsuario();
        EmailExtensions oEmailExtensions = new EmailExtensions();
        Usuarios oUsuario;

        if (!email.IsValidEmailAddress())
            throw new Exception("Email Incorrecto.");

        oUsuario = bUsuario.getUsuario(email);
        if (oUsuario != null)
        {
            string newPwd = string.Empty;
            newPwd = newPwd.GenerateRandom(6);

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<USUARIO>", oUsuario.Nombre + ", " + oUsuario.Apellido);
            replacements.Add("<PASSWORD>", newPwd);
            bool send = EmailHelper.SendMessage(EmailTemplate.RecuperoPwd, replacements, oUsuario.Email, "ClubIn: Recupero de contraseña");
            if (!send)
            {
                throw new Exception("El email con su nueva contraseña no pudo ser enviado.");
            }
            else{
                oUsuario.Pwd = newPwd;
                bUsuario.add(oUsuario);
            }
        }
        else
            throw new Exception("El email es inexistente.");
    }
}