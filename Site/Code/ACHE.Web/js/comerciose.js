﻿$(document).ready(function () {
    var idFranquicia = $("#hdnIDFranquicias").val();
    if (idFranquicia > 0) {
        $("#ddlFranquicias").attr("disabled", true);

    }


    configControls();
    $("#ddlFidely1").hide();
    $("#ddlFidely2").hide();
    $("#ddlFidely3").hide();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    toggleTabs();

    if ($("#hfIDComercio").val() != "0")
        $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());
    else
        $("#litTitulo").html("Alta de Comercio");
});

function deleteUpload(file, tipo) {

    var info = "{ id: " + parseInt($("#hfIDComercio").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "Comerciose.aspx/eliminar" + tipo,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#div" + tipo).hide();
            $("#img" + tipo).attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    //alert("2-" + msg);
    //$("#hdnAttachment").val("");
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/

/*** USUARIOS ***/

function agregarUsuario() {
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtUsuario").val() == "" || $("#txtEmailUsuarioComercio").val() == "" || $("#txtPwd").val() == "") {
        $("#divErrorUsuario").html("Por favor,complete todos los datos");
        $("#divErrorUsuario").show();
    }
    else {
        var info = "{ IDComercio: " + parseInt($("#hfIDComercio").val())
           + ", IDUsuario: " + parseInt($("#hfIDUsuario").val())
           + ", usuario: '" + $("#txtUsuario").val()
           + "', email: '" + $("#txtEmailUsuarioComercio").val()
           + "', pwd: '" + $("#txtPwd").val()
           + "', tipo: '" + $("#ddlTipoUsuario").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "Comerciose.aspx/procesarUsuario",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtUsuario").val("");
                $("#txtEmailUsuarioComercio").val("");
                $("#txtPwd").val("");
                $("#hfIDUsuario").val("0");
                $("#btnAgregarUsuario").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorUsuario").html(r.Message);
                $("#divErrorUsuario").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function filter() {
    $("#divErrorUsuario").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

/*** FIN USUARIOS ***/

/*** PRUEBAS POS ***/
function agregarPrueba() {
    $("#divErrorPrueba").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#hdnIDVerificacionPOS").val() == "")
        $("#hdnIDVerificacionPOS").val("0");

    var info = "{ idComercio: " + parseInt($("#hfIDComercio").val())
       + ", IDVerificacionPOS: " + parseInt($("#hdnIDVerificacionPOS").val())
       + ", estadoCanjes: '" + $("#cmbEstadoCanjes").val()
       + "', estadoGift: '" + $("#cmbEstadoGift").val()
       + "', estadoCompras: '" + $("#cmbEstadoCompras").val()
       + "', observacionesCanjes: '" + $("#txtObservacionesPosCanjes").val()
       + "', observacionesGift: '" + $("#txtObservacionesPosGift").val()
       + "', observacionesCompras: '" + $("#txtObservacionesPosCompras").val()
       + "', usuario: '" + $("#txtUsuarioPrueba").val()
       + "', obs: '" + $("#txtObsPrueba").val()
       + "', puntosCanjes: '" + $("#txtPuntosPosCanjes").val()
       + "', puntosGift: '" + $("#txtPuntosPosGift").val()
       + "', puntosCompras: '" + $("#txtPuntosPosCompras").val()
       + "', fechaPrueba: '" + $("#txtFechaPrueba").val()
       + "' }";

    $.ajax({
        type: "POST",
        url: "Comerciose.aspx/ProcesarPrueba",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#txtObservacionesPosCanjes, #txtObservacionesPosGift, #txtObservacionesPosCompras, #txtUsuarioPrueba, #txtObsPrueba, #txtPuntosPosCompras", "#txtPuntosPosCanjes", "#txtPuntosPosGift", "#txtFechaPrueba").val("");
            $("#modalPruebaPOS").modal("hide");
            $("#txtFechaHastaPruebas, #txtFechaDesdePruebas").val("");
            filterPruebas();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorPrueba").html(r.Message);
            $("#divErrorPrueba").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function validarPrueba() {
    $("#divErrorPrueba").hide();

    var isValid = true;
    var msg = "";
    if ($("#txtFechaPrueba").val() == "") {
        isValid = false;
        msg = "Debe ingresar la fecha";
    }
    else if ($("#txtUsuarioPrueba").val() == "") {
        isValid = false;
        msg = "Debe ingresar el usuario que realizó la prueba";
    }
    else if ($("#cmbEstadoCanjes").val() == "" || $("#cmbEstadoGift").val() == "" || $("#cmbEstadoCompras").val() == "") {
        isValid = false;
        msg = "Debe ingresar los estados de las pruebas";
    }

    if (!isValid) {
        $("#divErrorPrueba").show();
        $("#divErrorPrueba").html(msg);
    }
    else
        agregarPrueba();
}

function filterPruebas() {
    $("#divErrorPruebas").hide();
    var gridPuntos = $("#gridPruebas").data("kendoGrid");
    var $filterPuntos = new Array();

    gridPuntos.dataSource.filter($filterPuntos);
}

function verTodosPruebas() {
    $("#txtFechaDesdePruebas, #txtFechaHastaPruebas").val("");
    filterPruebas();
}

function nuevaPrueba() {
    $("#txtObservacionesPosCanjes, #txtObservacionesPosGift, #txtObservacionesPosCompras, #txtUsuarioPrueba, #txtObsPrueba, #txtPuntosPosCompras", "#txtPuntosPosCanjes", "#txtPuntosPosGift", "#txtFechaPrueba").val("");
    $("#txtFechaHastaPruebas, #txtFechaDesdePruebas").val("");
    $("#modalPruebaPOS").modal("show");
}

/*** FIN PRUEBAS POS ***/

/*** PUNTOS DE VENTA ***/

function agregarPuntoVenta() {

    $("#divErrorPuntos").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($('#formComercio').valid()) {

        if ($("#txtPuntoVenta").val() == "") {
            $("#divErrorPuntos").html("Por favor,complete todos los datos");
            $("#divErrorPuntos").show();
        }
        else {

            var fecha = new Date();
            var fechaActual = fecha.getDate() + "/"
                        + (fecha.getMonth() + 1) + "/"
                        + fecha.getFullYear();
            var esDefault = $('#chkDefault').is(':checked');

            var info = "{ IDComercio: " + parseInt($("#hfIDComercio").val())
               + ", IDPuntoVenta: " + parseInt($("#hfIDPuntoVenta").val())
               + ", numero: " + $("#txtPuntoVenta").val()
               + ", fechaAlta: '" + fechaActual
               + "', esDefault: '" + esDefault
               + "' }";

            //alert(info);

            $.ajax({
                type: "POST",
                url: "Comerciose.aspx/procesarPunto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtPuntoVenta").val("");
                    $("#btnAgregarPunto").html("Agregar");
                    filterPuntos();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    //alert('');
                    $("#divErrorPuntos").html(r.Message);
                    $("#divErrorPuntos").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
}

function filterPuntos() {
    $("#divErrorPuntos").hide();
    var grid = $("#gridPuntos").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

/*** FIN PUNTOS DE VENTA ***/

function showFidely() {
    if ($("#ddlPOSTipo").val() == "FIDELY") {
        //$("#ddlVisa1").hide();
        //$("#ddlFidely1").show();
        //$("#ddlFidely2").show();
        //$("#ddlFidely3").show();
    }
    else {
        //$("#ddlVisa1").show();
        $("#ddlFidely1").hide();
        $("#ddlFidely2").hide();
        $("#ddlFidely3").hide();
    }
}

function grabar() {

    $("#divError").hide();
    $("#divOk").hide();
    $('#formComercio').validate();

    //if ($("#ddlTipoDoc").val() == "CUIT" && $("#txtNroDocumento").val() != "00") {
    //    if (CuitEsValido($("#txtNroDocumento").val()) == false) {
    //        $("#divError").html("CUIT Inválido");
    //        $("#divError").show();
    //        $("#divOk").hide();
    //        $('html, body').animate({ scrollTop: 0 }, 'slow');

    //        return false;
    //    }
    //}
 
    if ($('#formComercio').valid()) {

        var rdbFormaPago = "D"; if ($("#rdbFormaPago_Tarjeta")[0].checked == true) rdbFormaPago = "T";
        var idMarca = 0;
        if ($("#ddlMarcas").val() != "")
            idMarca = parseInt($("#ddlMarcas").val());

        var idFranquicia = 0;
        if ($("#ddlFranquicias").val() != "")
            idFranquicia = parseInt($("#ddlFranquicias").val());

        var idZona = 0;
        if ($("#ddlZona").val() != "" && $("#ddlZona").val() != null)
            idZona = parseInt($("#ddlZona").val());

        var info = "{ IDComercio: '" + $("#hfIDComercio").val()
            + "', SDS: '" + $("#txtSDS").val()
            + "', RazonSocial: '" + $("#txtRazonSocial").val()
            + "', TipoDoc: '" + $("#ddlTipoDoc option:selected").html()
            + "', NroDocumento: '" + $("#txtNroDocumento").val()
            + "', NumEst: '" + $("#txtNumEst").val()
            + "', ComercioID: '" + $("#txtComercioID").val()
            + "', NombreEst: '" + $("#txtNombreEst").val()
            //+ "', Estado: '" + $("#txtEstado").val()
            + "', Rubro: '" + $("#ddlRubro").val()
            + "', Telefono: '" + $("#txtTelefono").val()
            + "', Celular: '" + $("#txtCelular").val()
            + "', Responsable: '" + $("#txtResponsable").val()
            + "', Cargo: '" + $("#txtCargo").val()
            + "', NombreFantasia: '" + $("#txtNombreFantasia").val()
            + "', IDMarca: " + idMarca
            + ", IDFranquicia: " + idFranquicia
            + ", IVA: '" + $("#ddlIVA option:selected").val()
            + "', Web: '" + $("#txtWeb").val()
            + "', Email: '" + $("#txtEmail").val()
            + "', Url: '" + $("#txtUrl").val()
            + "', Descuento: '" + $("#txtDescuento").val()
            + "', Descuento2: '" + $("#txtDescuento2").val()
            + "', Descuento3: '" + $("#txtDescuento3").val()
            + "', Descuento4: '" + $("#txtDescuento4").val()
            + "', Descuento5: '" + $("#txtDescuento5").val()
            + "', Descuento6: '" + $("#txtDescuento6").val()
            + "', Descuento7: '" + $("#txtDescuento7").val()
            //+ "', ModalidadDescuento: '" + $("#txtModalidadDescuento").val()
            + "', ModalidadDebito: " + $("#chkModalidadDebito").is(':checked')
            + ", ModalidadCredito: " + $("#chkModalidadCredito").is(':checked')
            + ", ModalidadEfectivo: " + $("#chkModalidadEfectivo").is(':checked')
            + ", DescripcionDescuento: '" + $("#txtDescuentoDescripcion").val()
            + "', DescuentoVip: '" + $("#txtDescuentoVip").val()
            + "', DescuentoVip2: '" + $("#txtDescuentoVip2").val()
            + "', DescuentoVip3: '" + $("#txtDescuentoVip3").val()
            + "', DescuentoVip4: '" + $("#txtDescuentoVip4").val()
            + "', DescuentoVip5: '" + $("#txtDescuentoVip5").val()
            + "', DescuentoVip6: '" + $("#txtDescuentoVip6").val()
            + "', DescuentoVip7: '" + $("#txtDescuentoVip7").val()
            + "', MulPuntosVip: '" + $("#txtMulPuntosVip").val()
            + "', MulPuntosVip2: '" + $("#txtMulPuntosVip2").val()
            + "', MulPuntosVip3: '" + $("#txtMulPuntosVip3").val()
            + "', MulPuntosVip4: '" + $("#txtMulPuntosVip4").val()
            + "', MulPuntosVip5: '" + $("#txtMulPuntosVip5").val()
            + "', MulPuntosVip6: '" + $("#txtMulPuntosVip6").val()
            + "', MulPuntosVip7: '" + $("#txtMulPuntosVip7").val()
            //+ "', ModalidadDescuentoVip: '" + $("#txtModalidadDescuentoVip").val()
            + "', ModalidadDebitoVip: " + $("#chkModalidadDebitoVip").is(':checked')
            + ", ModalidadCreditoVip: " + $("#chkModalidadCreditoVip").is(':checked')
            + ", ModalidadEfectivoVip: " + $("#chkModalidadEfectivoVip").is(':checked')
            + ", DescripcionDescuentoVip: '" + $("#txtDescuentoDescripcionVip").val()
           + "', POSArancel: " + parseInt($("#txtArancel").val())
            + ", Arancel2: " + parseInt($("#txtArancel2").val())
            + ", Arancel3: " + parseInt($("#txtArancel3").val())
            + ", Arancel4: " + parseInt($("#txtArancel4").val())
            + ", Arancel5: " + parseInt($("#txtArancel5").val())
            + ", Arancel6: " + parseInt($("#txtArancel6").val())
            + ", Arancel7: " + parseInt($("#txtArancel7").val())
            + ", POSPuntos: " + parseInt($("#txtPuntos").val())
            + ", Puntos2: " + parseInt($("#txtPuntos2").val())
            + ", Puntos3: " + parseInt($("#txtPuntos3").val())
            + ", Puntos4: " + parseInt($("#txtPuntos4").val())
            + ", Puntos5: " + parseInt($("#txtPuntos5").val())
            + ", Puntos6: " + parseInt($("#txtPuntos6").val())
            + ", Puntos7: " + parseInt($("#txtPuntos7").val())
            + ", CobrarUsoRed: " + $("#chkCobrarUsoRed").is(':checked')
            + ", MultPuntos1: " + $("#txtMulPuntos1").val()
            + ", MultPuntos2: " + $("#txtMulPuntos2").val()
            + ", MultPuntos3: " + $("#txtMulPuntos3").val()
            + ", MultPuntos4: " + $("#txtMulPuntos4").val()
            + ", MultPuntos5: " + $("#txtMulPuntos5").val()
            + ", MultPuntos6: " + $("#txtMulPuntos6").val()
            + ", MultPuntos7: " + $("#txtMulPuntos7").val()
            + ", Affinity: '" + $("#txtAffinity").val()
            + "', Observaciones: '" + $("#txtObservaciones").val()
            + "', CodigoDealer: '" + $("#txtCodigoDealer").val()
            + "', FechaAltaDealer: '" + $("#txtFechaAltaDealer").val()
            //+ "', GrabarDomicilio: '" + $("#hfGrabarDomicilio").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
            + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
            + "', Fax: '" + $("#txtFax").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "', Lat: '" + $("#txtLatitud").val()
            + "', Long: '" + $("#txtLongitud").val()
            + "', IDZona: " + idZona
            + ", Pais2: '" + $("#ddlPais2").val()
            + "', Provincia2: '" + $("#ddlProvincia2").val()
            + "', Ciudad2: '" + $("#ddlCiudad2").val()
            + "', Domicilio2: '" + $("#txtDomicilio2").val()
            + "', CodigoPostal2: '" + $("#txtCodigoPostal2").val()
            + "', TelefonoDom2: '" + $("#txtTelefonoDom2").val()
            + "', Fax2: '" + $("#txtFax2").val()
            + "', PisoDepto2: '" + $("#txtPisoDepto2").val()
            + "', POSTipo: '" + $("#ddlPOSTipo").val()
            //+ "', EstadoVisa: '" + $("#ddlEstadoVisa").val()
            + "', POSSistema: '" + $("#ddlPOSSistema").val()
            + "', POSTerminal: '" + $("#txtPOSTerminal").val()
            + "', POSEstablecimiento: '" + $("#txtPOSEstablecimiento").val()
            + "', POSMarca: '" + $("#txtPOSMarca").val()
            + "', POSObservaciones: '" + $("#txtPOSObservaciones").val()
            + "', POSFechaActivacion: '" + $("#txtPOSFechaActivacion").val()
            + "', POSFechaReprogramacion: '" + $("#txtPOSFechaReprogramacion").val()
            + "', Activo: '" + $("#chkActivo").is(':checked')
            + "', Reprogramado: '" + $("#chkPOSReprogramado").is(':checked')
            + "', Invalido: '" + $("#chkInvalido").is(':checked')
            + "', FormaPago: '" + rdbFormaPago
            + "', FormaPago_Banco: '" + $("#txtFormaPago_Banco").val()
            + "', FormaPago_TipoCuenta: '" + $("#ddlFormaPago_TipoCuenta").val()
            + "', FormaPago_NroCuenta: '" + $("#txtFormaPago_NroCuenta").val()
            + "', FormaPago_CBU: '" + $("#txtFormaPago_CBU").val()
            + "', FormaPago_Tarjeta: '" + $("#txtFormaPago_Tarjeta").val()
            + "', FormaPago_BancoEmisor: '" + $("#txtFormaPago_BancoEmisor").val()
            + "', FormaPago_NroTarjeta: '" + $("#txtFormaPago_NroTarjeta").val()
            + "', FormaPago_FechaVto: '" + $("#txtFormaPago_FechaVto").val()
            + "', FormaPago_CodigoSeg: '" + $("#txtFormaPago_CodigoSeg").val()
            + "', FidelyUsuario: '" + $("#txtFidelyUsuario").val()
            + "', FidelyPwd: '" + $("#txtFidelyPwd").val()
            + "', FidelyClaveLicencia: '" + $("#txtFidelyLicencia").val()
            //+ "', GrabarContacto: '" + $("#hfGrabarContacto").val()
            + "', Contacto_Nombre: '" + $("#txtContacto_Nombre").val()
            + "', Contacto_Apellido: '" + $("#txtContacto_Apellido").val()
            + "', Contacto_Cargo: '" + $("#txtContacto_Cargo").val()
            + "', Contacto_Telefono: '" + $("#txtContacto_Telefono").val()
            + "', Contacto_Celular: '" + $("#txtContacto_Celular").val()
            + "', Contacto_Email: '" + $("#txtContacto_Email").val()
            + "', Contacto_Observaciones: '" + $("#txtContacto_Observaciones").val()
            + "', Contacto_Documento: '" + $("#txtContacto_Documento").val()
            + "', EnvioMailsFc: '" + $("#txtEnvioFc").val()
            + "', CasaMatriz: " + $("#chkCasaMatriz").is(':checked')
            + ", posWeb: " + $("#chkPosWeb").is(':checked')
            + ", costoPosWeb: '" + $("#txtCostoPosWeb").val()
            + "', giftcardCarga: '" + $("#txtGiftcardCarga").val()
            + "', giftcardDescarga: '" + $("#txtGiftcardDescarga").val()
            + "', giftcardCobrarUsored: " + $("#chkGiftcardCobrarUsored").is(':checked')
            + ", giftcardPosWeb: " + $("#chkGiftcardPosWeb").is(':checked')
            + ", giftcardGeneraPuntos: " + $("#chkGiftcardGeneraPuntos").is(':checked')
            + ", giftcardCostoPosWeb: '" + $("#txtGiftcardCostoPosWeb").val()
            + "', cuponInArancel: '" + $("#txtCuponINArancel").val()
            + "', cuponInCobrarUsored: " + $("#chkCuponINCobrarUsoRed").is(':checked')
            + ", emailAlertas: '" + $("#txtEmailAlerta").val()
            + "', celularAlertas: '" + $("#txtCelularAlerta").val()
            + "', celularEmpresaAlertas: '" + $("#txtCelularEmpresaAlerta").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "Comerciose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());

                toggleTabs();
                filter();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function toggleTabs() {
    if ($("#hfIDComercio").val() != "0") {
        $($("#Tabs ul li a")[8]).removeClass("hide");
        $($("#Tabs ul li a")[10]).removeClass("hide");
        $($("#Tabs ul li a")[13]).removeClass("hide");
        $("#divUploadLogo").show();
        $("#divUploadFicha").show();
    }
    else {
        $($("#Tabs ul li a")[8]).addClass("hide");
        $($("#Tabs ul li a")[10]).addClass("hide");
        $($("#Tabs ul li a")[13]).addClass("hide");
    }

    toggleButtons();
}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[2]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[3]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[4]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[5]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[6]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[7]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[8]).click(function () {
        $("#formButtons").hide();
    });
    $($("#Tabs ul li a")[9]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[10]).click(function () {
        $("#formButtons").hide();
    });
    $($("#Tabs ul li a")[11]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[13]).click(function () {
        $("#formButtons").hide();
    });
}

function configControls() {
    configDatePicker();

    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtSDS, #txtNroDocumento, #txtNumEst, #txtComercioID, #txtEstado, #txtMulPuntos1, #txtMulPuntos2, #txtMulPuntos3, #txtMulPuntos4, #txtMulPuntos5, #txtMulPuntos6, #txtMulPuntos7").numeric();
    $("#txtFormaPago_NroCuenta, #txtFormaPago_CBU, #txtFormaPago_NroTarjeta, #txtFormaPago_FechaVto, #txtFormaPago_CodigoSeg").numeric();
    $("#txtContacto_Documento, #txtArancel, #txtPuntos").numeric();
    $("#txtDescuento, #txtDescuento2, #txtDescuento3, #txtDescuento4, #txtDescuento5, #txtDescuento6, #txtDescuento7").numeric();
    $("#txtDescuentoVip, #txtDescuentoVip2, #txtDescuentoVip3, #txtDescuentoVip4, #txtDescuentoVip5, #txtDescuentoVip6, #txtDescuentoVip7").numeric();
    $("#txtMulPuntosVip, #txtMulPuntosVip2, #txtMulPuntosVip3, #txtMulPuntosVip4, #txtMulPuntosVip5, #txtMulPuntosVip6, #txtMulPuntosVip7").numeric();
    $("#txtGiftcardCarga, #txtGiftcardDescarga, #txtCuponINArancel").numeric();
    $("#txtPuntoVenta").numeric();
    $("#txtMulPuntos1,#txtMulPuntos2,#txtMulPuntos3,#txtMulPuntos4,#txtMulPuntos5,#txtMulPuntos6,#txtMulPuntos7").numeric();

    $("#txtCostoPosWeb, #txtGiftcardCostoPosWeb").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    jQuery.validator.addMethod("url", function (value, element) {
        return this.optional(element) || /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:\w]))?)/.test(value);
    }, "Debe ingresar una url válida");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Comerciose.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idComercio: parseInt($("#hfIDComercio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".Acceder", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmailUsuarioComercio").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin")
            $("#ddlTipoUsuario").val("A");
        else
            $("#ddlTipoUsuario").val("B");
        $("#btnAgregarUsuario").html("Actualizar");
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Comerciose.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    //GRILLA PUNTOS DE VENTA

    $("#gridPuntos").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDPuntoVenta: { type: "integer" },
                        IDComercio: { type: "integer" },
                        Punto: { type: "integer" },
                        FechaAlta: { type: "date" },
                        EsDefault: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Comerciose.aspx/GetListaGrillaPuntos", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtPuntoVenta").val("");
                        $("#hfIDPuntoVenta").val("0");
                        $("#btnAgregarPuntoVenta").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idComercio: parseInt($("#hfIDComercio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDPuntoVenta", title: "ID", width: "50px" },
            { field: "Punto", title: "Punto", width: "100px" },
            { field: "FechaAlta", title: "Fecha de alta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "EsDefault", title: "EsDefault", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridPuntos").delegate(".editColumn", "click", function (e) {
        var grid = $("#gridPuntos").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.EsDefault == "Si") {
            $('#chkDefault').attr('checked', true);
        }
        else {
            $('#chkDefault').attr('checked', false);
        }

        $("#hfIDPuntoVenta").val(dataItem.IDPuntoVenta);
        $("#txtPuntoVenta").val(dataItem.Punto);
        $("#btnAgregarPuntoVenta").html("Actualizar");
    });

    $("#gridPuntos").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridPuntos").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Comerciose.aspx/DeletePunto",
                data: "{ id: " + dataItem.IDPuntoVenta + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterPuntos();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    //GRILLA PRUEBAS POS

    $("#gridPruebas").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        FechaPrueba: { type: "date" },
                        IDVerificacionPOS: { type: "integer" },
                        EstadoCanje: { type: "string" },
                        EstadoCompras: { type: "string" },
                        EstadoGift: { type: "string" },
                        PuntosCanjes: { type: "string" },
                        PuntosCompras: { type: "string" },
                        PuntosGift: { type: "string" },
                        ObservacionesCanjes: { type: "string" },
                        ObservacionesCompras: { type: "string" },
                        ObservacionesGift: { type: "string" },
                        ObservacionesGenerales: { type: "string" },
                        UsuarioPrueba: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Comerciose.aspx/GetListaGrillaPruebas",
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmailUsuarioComercio").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");
                        data = $.extend({ sort: null, filter: null, idComercio: parseInt($("#hfIDComercio").val()), desde: $("#txtFechaDesdePruebas").val(), hasta: $("#txtFechaHastaPruebas").val() }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        sortable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "FechaPrueba", title: "Fecha prueba", width: "100px", format: "{0:dd/MM/yyyy}" },
            { field: "EstadoCanjes", title: "Estado Canjes", width: "100px" },
            { field: "EstadoCompras", title: "Estado Compras", width: "100px" },
            { field: "EstadoGift", title: "Estado Gift", width: "100px" },
            { field: "IDVerificacionPOS", title: "ID", width: "50px", hidden: true },

            { field: "PuntosCanjes", title: "Puntos canjes", width: "100px" },
            { field: "PuntosCompras", title: "Puntos gift", width: "100px" },
            { field: "PuntosGift", title: "Puntos compras", width: "100px" },

            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridPruebas").delegate(".editColumn", "click", function (e) {
        var grid = $("#gridPruebas").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hdnIDVerificacionPOS").val(dataItem.IDPruebaPOS);
        $("#cmbEstadoCanjes").val(dataItem.EstadoCanjes);
        $("#cmbEstadoGift").val(dataItem.EstadoGift);
        $("#cmbEstadoCompras").val(dataItem.EstadoCompras);

        var fecha = dataItem.FechaPrueba;
        day = fecha.getDate(),
        month = fecha.getMonth() + 1,
        year = fecha.getFullYear();
        var mes =
        $("#txtFechaPrueba").val(day + "/" + month + "/" + year);

        $("#txtUsuarioPrueba").val(dataItem.UsuarioPrueba);

        $("#txtPuntosPosCanjes").val(dataItem.PuntosCanjes);
        $("#txtObservacionesPosCanjes").val(dataItem.ObservacionesCanjes);

        $("#txtPuntosPosGift").val(dataItem.PuntosGift);
        $("#txtObservacionesPosGift").val(dataItem.ObservacionesGift);

        $("#txtPuntosPosCompras").val(dataItem.PuntosCompras);
        $("#txtObservacionesPosCompras").val(dataItem.ObservacionesCompras);

        $("#txtObsPrueba").val(dataItem.ObservacionesGenerales);

        $("#modalPruebaPOS").modal("show");
    });

    $("#gridPruebas").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridPruebas").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Comerciose.aspx/DeletePrueba",
                data: "{ id: " + dataItem.IDVerificacionPOS + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterPruebas();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    /*if ($("#hfGrabarContacto").val() == "0")
        $($("#Tabs ul li a")[4]).attr("disabled", "disabled");
    else
        $($("#Tabs ul li a")[4]).removeAttr("disabled");
    if ($("#hfGrabarDomicilio").val() == "0") {
        $($("#Tabs ul li a")[5]).attr("disabled", "disabled");
        $($("#Tabs ul li a")[6]).attr("disabled", "disabled");
    }
    else {
        $($("#Tabs ul li a")[5]).removeAttr("disabled");
        $($("#Tabs ul li a")[6]).removeAttr("disabled");
    }*/

    $($("#Tabs ul li a")[2]).click(function () {
        if ($("#rdbFormaPago_Debito")[0].checked == false && $("#rdbFormaPago_Tarjeta")[0].checked == false)
            $("#rdbFormaPago_Debito")[0].checked = true;
    });

    $("#rdbFormaPago_Debito").click(function () {
        $("#divFormaPago_Debito").show();
        $("#divFormaPago_Tarjeta").hide();
    });
    $("#rdbFormaPago_Tarjeta").click(function () {
        $("#divFormaPago_Debito").hide();
        $("#divFormaPago_Tarjeta").show();
    });

    if ($("#rdbFormaPago_Debito")[0].checked == true) {
        $("#divFormaPago_Debito").show();
        $("#divFormaPago_Tarjeta").hide();
    }
    else if ($("#rdbFormaPago_Tarjeta")[0].checked == true) {
        $("#divFormaPago_Debito").hide();
        $("#divFormaPago_Tarjeta").show();
    }
}