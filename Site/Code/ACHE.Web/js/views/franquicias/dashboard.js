var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);

$(document).ready(function () {
    setTimeout("ObtenerComisiones()", 1000);
    setTimeout("ObtenerPromedioTicketMensual()", 1000);
    setTimeout("ObtenerTotalTasaUsoMensual()", 1000);
    setTimeout("ObtenerTotalTRMensual()", 1000);
    setTimeout("ObtenerTotalSociosSexo()", 1000);
    setTimeout("ObtenerTotalComisionMensual()", 1000);
    //setTimeout("ObtenerTotalPubLocal()", 1000);
    //setTimeout("ObtenerTotalPubNacional()", 1000);
    setTimeout("ObtenerTotalEmails()", 1000);
    setTimeout("ObtenerTotalCelulares()", 1000);
    setTimeout("ObtenerTotalTarjetasActivas()", 1000);
    setTimeout("ObtenerDineroDisponibleCredito()", 1000);

    setTimeout("gebo_charts.fl_tarjetas_activas()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_asignadas()", 2000);
    setTimeout("gebo_charts.fl_cant_transacciones()", 2000);
    setTimeout("gebo_charts.fl_comisiones()", 2000);
    setTimeout("gebo_charts.fl_comisiones_detalle()", 2000);
    setTimeout("gebo_charts.fl_terminales_activas()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_impresas()", 2000);
    setTimeout("gebo_charts.fl_estado_terminales()", 2000);

    setTimeout("ObtenerTop10Socios()", 1500);
    setTimeout("ObtenerTop10SociosMensual()", 1500);
    setTimeout("ObtenerTop10Comercios()", 1500);
    setTimeout("ObtenerTop10ComerciosMensual()", 1500);

    setTimeout("ObtenerPromedioArancel()", 1000);
    setTimeout("ObtenerPromedioPuntos()", 1000);
    setTimeout("ObtenerPromedioDescuentos()", 1000);
    setTimeout("ObtenerCantidadTarjetas()", 1000);
    setTimeout("ObtenerFacturacionComercios()", 1000);




});

//* charts
gebo_charts = {
    fl_terminales_activas: function () {
        // Setup the placeholder reference
        var elem = $('#fl_terminales_activas');

        var data = ObtenerTerminalesActivas();
        //var colors = [ "#eadac8", "#dcc1a3", "#cea97e", "#c09059", "#a8763f", "#835c31", "#5e4223", "#392815" ];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    }/*,
                    colors: colors*/
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_tarjetas_activas: function () {

        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_activas');

        var activas = ObtenerTarjetasActivas();
        var inactivas = ObtenerTarjetasInactivas();

        var data = [
            { label: "Activas", data: activas },
            { label: "Inactivas", data: inactivas }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_asignadas: function () {
        var elem = $('#fl_tarjetas_asignadas');

        var data = ObtenerTarjetasAsignadas();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_cant_transacciones: function () {
        var elem = $('#fl_cant_transacciones');

        var data = ObtenerCantTransacciones();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_impresas: function () {
        ObtenerTarjetasImpresas2();
        //var elem = $('#fl_tarjetas_impresas');

        ////******* Precious Metal Price - HORIZONTAL BAR CHART
        //var rawData = ObtenerTarjetasImpresas();
        //var dataSet = [{ label: "Marcas", data: rawData }];
        //var ticks = ObtenerTarjetasImpresasLabels();

        //var options = {
        //    series: {
        //        shadowSize: 1,
        //        bars: {
        //            show: true
        //        }
        //    },
        //    bars: {
        //        align: "center",
        //        barWidth: 0.5,
        //        horizontal: true,
        //        fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
        //        lineWidth: 1
        //    },
        //    xaxis: {
        //        axisLabel: "Cantidad",

        //        axisLabelUseCanvas: true,
        //        axisLabelFontSizePixels: 12,
        //        axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        //        axisLabelPadding: 5,
        //        tickFormatter: function (v, axis) {
        //            return v;//$.formatNumber(v, { format: "#,###", locale: "us" });
        //        }
        //    },
        //    yaxis: {
        //        axisLabel: "Marcas",
        //        axisLabelUseCanvas: true,
        //        axisLabelFontSizePixels: 12,
        //        axisLabelFontFamily: 'Verdana, Arial',
        //        axisLabelPadding: 3,
        //        ticks: ticks,
        //        tickLength: 0
        //    },
        //    grid: {
        //        backgroundColor: { colors: ["#fff", "#eee"] },
        //        hoverable: true,
        //        clickable: false,
        //        borderWidth: 1
        //    }
        //};

        //$.plot(elem, dataSet, options);


        //elem.bind("plothover", function (event, pos, item) {
        //    if (item) {
        //        if (previousPoint != item.datapoint) {
        //            previousPoint = item.datapoint;
        //            $("#flot-tooltip").remove();

        //            y = item.datapoint[0];
        //            z = item.series.color;

        //            showTooltip(item.pageX, item.pageY,
        //            "<b>Cantidad</b> = " + y,
        //            //"<b>Importe = </b> $" + y,
        //            z);
        //        }
        //    } else {
        //        $("#flot-tooltip").remove();
        //        previousPoint = null;
        //    }
        //});
    },

    fl_estado_terminales: function () {
        // Setup the placeholder reference
        var elem = $('#fl_estado_terminales');

        var data = ObtenerEstadoTerminales();
        var colors = ["#000", "#F5AA1A", "#70A415", "#f00", "#f8f412", "#0066FF", "#ccc"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_comisiones: function () {

        // Setup the placeholder reference
        var elem = $('#fl_comisiones_4meses');

        var com1 = ObtenerComision("TtCp");
        var com2 = ObtenerComision("TpCp");
        var com3 = ObtenerComision("TpCt");

        var data = [
            { label: "Tt Cp", data: com1 },
            { label: "Tp Cp", data: com2 },
            { label: "Tp Ct", data: com3 }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_comisiones_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_comisiones_detalle');
        
        var com1 = ObtenerComisionDiario("TtCp");
        var com2 = ObtenerComisionDiario("TpCp");
        var com3 = ObtenerComisionDiario("TpCt");

        for (var i = 0; i < com1.length; ++i) { com1[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < com2.length; ++i) { com2[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < com3.length; ++i) { com3[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                 { label: "Tt Cp", data: com1 },
                { label: "Tp Cp", data: com2 },
                { label: "Tp Ct", data: com3 }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = $" + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    }
};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerTerminalesActivas() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTerminalesPorPOS",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblTerminalesPorPOS").html("Terminales por pos (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasAsignadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasAsignadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasImpresas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasLabels() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([i, data[i].label]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

var oTable = null;

function ObtenerTarjetasImpresas2() {
    $.ajax({
        type: "POST",
        url: "dashboard.aspx/ObtenerTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyImpresas").html("");

                if (oTable != null) {
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tablaImpresas').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 4,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ning�n dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "�ltimo",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].Marca,
                        data.d[i].Activas,
                        data.d[i].Inactivas,
                        data.d[i].Total]
                      );
                }

                //$("#tableDetalle_info").parent().remove();
                $("#tablaImpresas").css("width", "100%");

                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyImpresas").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }
        }
    });
}

function ExportarTarjetasImpresas() {
    $("#lnkDownloadEmitidas").hide();
    $("#imgLoadingEmitidas").show();
    $("#btnExportarEmitidas").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/ExportarTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoadingEmitidas").hide();
                $("#lnkDownloadEmitidas").show();
                $("#lnkDownloadEmitidas").attr("href", data.d);
                $("#lnkDownloadEmitidas").attr("download", data.d);
                $("#btnExportarEmitidas").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingEmitidas").hide();
            $("#lnkDownloadEmitidas").hide();
            $("#btnExportarEmitidas").attr("disabled", false);
        }
    });
}

function ObtenerEstadoTerminales() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerEstadoTerminales",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblEstadoTerminales").html("Estado de las terminales (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

/*Tarjetas activas & inactivas, 4 meses*/
function ObtenerTarjetasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasInactivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasInactivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Cantidad Transacciones*/

function ObtenerCantTransacciones() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerCantTransacciones",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

/*** Comisiones ***/

function ObtenerComision(tipo) {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerComision",
        data: "{tipo: '" + tipo + "'}",
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, parseInt(data[i].data)]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerComisionDiario(tipo) {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerComisionDiario",
        data: "{tipo: '" + tipo + "'}",
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*METODOS DE OBTENCION DE DATOS DE LOS ICONOS*/

function ObtenerComisiones() {
    //$("#resultado_puntos_mensual").html();
    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerComisiones",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var aux = data.d.split("_");

                $("#resultado_comision1").html("$ " + aux[0]);
                $("#resultado_comision2").html("$ " + aux[1]);
                $("#resultado_comision3").html("$ " + aux[2]);
                $("#resultado_comisionTotal").html("$ " + aux[3]);
                $("#resultado_comisionTotalIVA").html("$ " + aux[4]);

                
            }
        }
    });
}

function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerTop10SociosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10SociosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySociosMes").html(data.d);
            }
        }
    });
}


function ObtenerTop10ComerciosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10ComerciosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComerciosMes").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}

function ObtenerTotalSociosSexo() {
    //$("#resultado_sexo").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalSociosPorSexo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var aux = data.d.split(",");
                $("#resultado_sexo").html(aux[0]);
                $("#totalSexo").html(aux[1]);
            }
        }
    });
}

function ObtenerTotalComisionMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalComisionMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_comision_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalTRMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTRMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tr_mensual").html(data.d);
            }
        }
    });
}

function ObtenerTotalTasaUsoMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTasaUsoMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tasauso_mensual").html(data.d);
            }
        }
    });
}

function ObtenerPromedioTicketMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioTicketMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_ticketpromedio_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerDineroDisponibleCredito() {

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerDineroDisponibleCredito",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_dinero_credito").html("$ " + data.d);
            }
        }
    });
}
/*
function ObtenerTotalPubLocal() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPubLocal",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_pubLocal").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalPubNacional() {
    //$("#resultado_puntos_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPubNacional",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_pubNacional").html("$ " + data.d);
            }
        }
    });
}
*/
function ObtenerTotalEmails() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalEmails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_mails").html(data.d);
            }
        }
    });
}

function ObtenerTotalCelulares() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalCelulares",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_cel").html(data.d);
            }
        }
    });
}

function ObtenerTotalTarjetasActivas() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTarjetasActivas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_tarjetas_activas").html(data.d);
            }
        }
    });
}


function ObtenerPromedioArancel() {

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioArancel",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_promedio_arancel").html("% " + data.d);
            }
        }
    });
}


function ObtenerPromedioPuntos() {

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioPuntos",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_promedio_puntos").html("% " + data.d);
            }
        }
    });
}

function ObtenerPromedioDescuentos() {

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioDescuentos",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_promedio_descuentos").html("% " + data.d);
            }
        }
    });
}

function ObtenerCantidadTarjetas() {

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerCantidadTarjetas",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
         
                $("#resultado_cantidad_tarjetas").html(data.d);
            }
        }
    });
}

function ObtenerFacturacionComercios() {
   
    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerFacturacionComercios",
  
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                
                $("#resultado_facturacion_comercios").html("$ "+ data.d);
            }
        }
    });
}



/*** METODOS AUXILIARES **/
function verDetalleTerminales(estado) {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Terminales");

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerDetalleTerminales",
        data: "{estado: '" + estado + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }

            $('#modalDetalle').modal('show');
        }
    });
}



function mostrarSociosGeneral() {
    $("#tblSociosMensual").hide();
    $("#tblSocios").show();
}
function mostrarComerciosGeneral() {
    $("#tblComerciosMensual").hide();
    $("#tblComercios").show();
}

function mostrarComerciosMensual() {
    $("#tblComerciosMensual").show();
    $("#tblComercios").hide();
}
function mostrarSociosMensual() {
    $("#tblSociosMensual").show();
    $("#tblSocios").hide();
}