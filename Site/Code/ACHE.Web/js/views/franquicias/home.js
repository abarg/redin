﻿var oTable = null;

function verDetalleComercios(tipo) {
    $("#btnExportar").show();
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    $("#bodyDetalle").html();
    $("#hdnTipo").val(tipo);

    $("#titDetalle").html("Detalle de Errores");
    
    if (tipo == "sinTrans")
        $("#headDetalle").html("<tr><th>Comercio</th><th>Marca</th></tr>");
    else $("#headDetalle").html("<tr><th>Comercio</th><th>Marca</th><th>Pendientes</th> </tr>");

    $.ajax({
        type: "POST",
        url: "home.aspx/obtenerDetalleComercio",
        data: "{tipo: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyDetalle").html("");

                if (oTable != null) {

                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalle').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].NombreComercio,
                      //   data.d[i].NumEst,
                        data.d[i].Marca,
                        data.d[i].Pendientes]
                      );
                }

                $("#tableDetalle_info").parent().remove();
                $("#tableDetalle").css("width", "100%");


                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyDetalle").html("<tr><td colspan='3'>No hay un detalle disponible</td></tr>");
            }

            $('#modalDetalle').modal('show');
        }
    });
}

function exportar() {
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "home.aspx/Exportar",
        data: "{ tipo: '" + $("#hdnTipo").val() + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}