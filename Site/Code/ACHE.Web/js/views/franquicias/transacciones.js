﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = $("#ddlMarcas").val();
    if (marca == "")
        marca = 0;

    var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "', documento: '" + $("#txtDocumento").val()
            + "', comercio: '" + $("#txtComercio").val()
            + "', idMarca: " + parseInt(marca)
            + ", origen: '" + $("#ddlOrigen").val()
            + "', operacion: '" + $("#ddlOperacion").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "transacciones.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


/*** Transacciones ***/
/*function NuevaTr() {
    $("#divError").hide();
    $("#divErrorTr").hide();
    $("#divOk").hide();

    $("#txtTrDescripcion").val("");
    $("#txtTrPuntos").val("");
    $("#txtTrTarjeta").val("");

    $('#modalTransacciones').modal('show');
}

function SaveTr() {
    $("#divErrorTr").hide();
    $("#divOk").hide();
    $('#formTr').validate();

    if ($('#formTr').valid()) {

        if ($("#txtTrImporte").val() == "" || $("#txtTrTarjeta").val() == "" || $("#ddlTrComercio").val() == "") {
            $("#divErrorTr").html("Por favor, ingrese el numero de tarjeta, comercio e importe");
            $("#divErrorTr").show();
        }
        else {

            var info = "{ tipo: '" + $("#ddlTrTipo").val()
                + "', tarjeta: '" + $("#txtTrTarjeta").val()
                + "', idComercio: " + parseInt($("#ddlTrComercio").val())
                + ", importe: " + parseInt($("#txtTrImporte").val())
                + ", descripcion: '" + $("#txtTrDescripcion").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "transacciones.aspx/grabar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#modalTransacciones').modal('hide');
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorTr").html(r.Message);
                    $("#divErrorTr").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        return false;
    }
}
*/

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtTarjeta, #txtDocumento, #txtTrTarjeta, #txtTrImporte").numeric();

    $("#txtComercio, #txtFechaDesde, #txtFechaHasta, #txtTarjeta, #txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    /*LoadComercios("../common.aspx/LoadComercios", "ddlTrComercio");
    $(".chzn_a").chosen({ allow_single_deselect: false });
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $('#formTr').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });*/

    $('#formTransacciones').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Fecha: { type: "string" },
                        Hora: { type: "string" },
                        Operacion: { type: "string" },
                        SDS: { type: "string" },
                        Comercio: { type: "string" },
                        Domicilio: { type: "string" },
                        NroEstablecimiento: { type: "string" },
                        POSTerminal: { type: "string" },
                        Tarjeta: { type: "string" },
                        Socio: { type: "string" },
                        NroDocumentoSocio: { type: "string" },
                        ImporteOriginal: { type: "number" },
                        ImporteAhorro: { type: "number" },
                        ImportePagado: { type: "number" },
                        Puntos: { type: "integer" },
                        PuntosTotales: { type: "integer" },
                        Marca: { type: "string" },
                        ComisionTtCp: { type: "number" },
                        ComisionTpCp: { type: "number" },
                        ComisionTpCt: { type: "number" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "transacciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "ID", title: "ID", width: "70px" },
            { field: "Fecha", title: "Fecha", width: "80px" },
            { field: "Hora", title: "Hora", width: "70px" },
            { field: "Operacion", title: "Operacion", width: "70px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "Comercio", title: "Comercio", width: "180px" },
            { field: "Domicilio", title: "Domicilio", width: "150px" },
            { field: "POSTerminal", title: "Terminal", width: "80px" },
            { field: "Tarjeta", title: "Tarjeta", width: "130px" },
            { field: "Socio", title: "Socio", width: "150px" },
            { field: "NroDocumentoSocio", title: "Doc Socio", width: "80px" },
            { field: "ImporteOriginal", title: "Importe", width: "70px" },
            { field: "ImporteAhorro", title: "Ahorro", width: "70px" },
            { field: "ImportePagado", title: "Ahorro", width: "70px" },
            /*{ field: "ComisionTtCp", title: "Tt Cp %", width: "70px", },
            { field: "ComisionTpCp", title: "Tp Cp %", width: "70px", },
            { field: "ComisionTpCt", title: "Tp Ct %", width: "70px", },*/
            { field: "Puntos", title: "Puntos", width: "50px" }
            //{ field: "PuntosTotales", title: "Total", width: "50px" }
        ]
    });


}

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formTransacciones').valid()) {

        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();

        var Tarjeta = $("#txtTarjeta").val();
        if (Tarjeta != "") {
            $filter.push({ field: "Tarjeta", operator: "contains", value: Tarjeta });
        }

        var Comercio = $("#txtComercio").val();
        if (Comercio != "") {
            $filter.push({ field: "Comercio", operator: "contains", value: Comercio });
        }

        var Documento = $("#txtDocumento").val();
        if (Documento != "") {
            $filter.push({ field: "NroDocumentoSocio", operator: "contains", value: Documento });
        }

        var origen = $("#ddlOrigen").val();
        if (origen != "") {
            $filter.push({ field: "Origen", operator: "equal", value: origen });
        }

        var marca = $("#ddlMarcas").val();
        if (marca != "") {
            $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
        }

        var operacion = $("#ddlOperacion").val();
        if (operacion != "") {
            $filter.push({ field: "Operacion", operator: "equal", value: operacion });
        }

        grid.dataSource.filter($filter);
    }
}

$(document).ready(function () {
    configControls();
});