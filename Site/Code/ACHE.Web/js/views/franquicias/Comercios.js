﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ SDS: '" + $("#txtSDS").val()
            + "', RazonSocial: '" + $("#txtRazonSocial").val()
            + "', NroDocumento: '" + $("#txtDocumento").val()
            + "', NumEst: '" + $("#txtEstablecimiento").val()
            + "', Descuento: '" + $("#txtDescuento").val()
            + "', Dealer: '" + $("#txtDealer").val()
            + "', POSTerminal: '" + $("#txtPosTerminal").val()
            //+ "', ConCobro: '" + $("#chkCobroRed").is(':checked')
            + "'}";

    $.ajax({
        type: "POST",
        url: "comercios.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function configControls() {
    $("#txtDocumento").numeric();
    $("#txtSDS").numeric();
    $("#txtPosTerminal").numeric();
    //$("#txtPosEstablecimiento").numeric();
    $("#txtEstablecimiento").numeric();
    $("#txtDescuento").numeric();
    
    $("#txtSDS, #txtDocumento, #txtRazonSocial, #txtEstablecimiento, #txtPosTerminal, #txtNombre, #txtDealer, #txtDescuento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDComercio: { type: "integer" },
                        SDS: { type: "integer" },
                        NombreFantasia: { type: "string" },
                        RazonSocial: { type: "string" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "integer" },
                        Telefono: { type: "string" },
                        Celular: { type: "string" },
                        Responsable: { type: "string" },
                        //Cargo : { type: "string" },
                        //Actividad : { type: "string" },
                        //CondicionIva : { type: "string" },
                        //Web : { type: "string" },
                        Email: { type: "string" },
                        //FechaAlta : { type: "date" },
                        //POSTipo : { type: "string" },
                        POSSistema: { type: "string" },
                        POSTerminal: { type: "integer" },
                        //POSEstablecimiento: { type: "integer" },
                        //POSMarca : { type: "string" },
                        //Observaciones : { type: "string" },
                        //Estado: { type: "integer" },
                        NumEst: { type: "string" }
                        //NombreEst : { type: "string" },
                        //CodAct : { type: "string" },
                        //Descuento : { type: "string" },
                        //DescuentoVip : { type: "string" },
                        //AffinityBenef : { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "comercios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDComercio", title: "ID", width: "50px" },
            { field: "SDS", title: "SDS", width: "75px" },
            { field: "NombreFantasia", title: "Nombre Fantasía", width: "150px" },
            { field: "RazonSocial", title: "Razón social", width: "150px" },
            { field: "TipoDocumento", title: "Tipo Doc.", width: "75px" },
            { field: "NroDocumento", title: "Nro. Doc", width: "100px" },
            { field: "Telefono", title: "Teléfono", width: "100px" },
            { field: "Responsable", title: "Responsable", width: "150px" },
            { field: "Email", title: "Email", width: "200px" },
            //{ field: "POSTipo", title: "POS Tipo", width: "100px" },
            { field: "POSSistema", title: "POS Sistema", width: "100px" },
            { field: "POSTerminal", title: "POS Terminal", width: "100px" },
            //{ field: "POSEstablecimiento", title: "POS Est.", width: "100px" },
            { field: "NumEst", title: "Nro de Est.", width: "150px" }
            //{ field: "Descuento", title: "Descuento", width: "75px" },
            //{ field: "DescuentoVip", title: "Desc. Vip", width: "75px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "comerciosedicion.aspx?IDComercio=" + dataItem.IDComercio;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "comercios.aspx/Delete",
                data: "{ id: " + dataItem.IDComercio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });

}

function Nuevo() {
    window.location.href = "comerciosedicion.aspx";
}

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var SDS = $("#txtSDS").val();
    if (SDS != "") {
        $filter.push({ field: "SDS", operator: "contains", value: SDS });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var RazonSocial = $("#txtRazonSocial").val();
    if (RazonSocial != "") {
        $filter.push({ field: "RazonSocial", operator: "contains", value: RazonSocial });
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "NombreFantasia", operator: "contains", value: nombre });
    }

    var Establecimiento = $("#txtEstablecimiento").val();
    if (Establecimiento != "") {
        $filter.push({ field: "NumEst", operator: "contains", value: Establecimiento });
    }

    var PosTerminal = $("#txtPosTerminal").val();
    if (PosTerminal != "") {
        $filter.push({ field: "PosTerminal", operator: "contains", value: PosTerminal });
    }

    var descuento = $("#txtDescuento").val();
    if (descuento != "") {
        $filter.push({ field: "Descuento", operator: "contains", value: descuento });
    }

    var dealer = $("#txtDealer").val();
    if (dealer != "") {
        $filter.push({ field: "CodigoDealer", operator: "equal", value: dealer });
    }

    var cp = $("#txtCp").val();
    if (cp != "") {
        $filter.push({ field: "CodigoPostal", operator: "equal", value: cp });
    }

    /*var cobro = $("#chkCobroRed").is(':checked');
    if (cobro)
        $filter.push({ field: "CobrarUsoRed", operator: "equal", value: 0 });
    else
        $filter.push({ field: "CobrarUsoRed", operator: "equal", value: 1 });
    */
    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});


function cargarSubRubros() {

    var idRubro = 0

    if (parseInt($("#ddlRubro").val()) > 0) {
        idRubro = parseInt($("#ddlRubro").val());
    }
    var info = "{ IDRubroPadre: " + idRubro + " }";

    $.ajax({
        type: "POST",
        url: "Comerciose.aspx/cargarSubRubros",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#ddlSubRubro").empty();
                $("#ddlSubRubro").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#ddlSubRubro").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                $('#ddlSubRubro').trigger("liszt:updated");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
        }
    });


}


