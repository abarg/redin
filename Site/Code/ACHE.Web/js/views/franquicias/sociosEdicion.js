﻿/**FOTO UPLOAD**/
function deleteUpload(file, tipo) {

    var info = "{ id: " + parseInt($("#hfIDSocio").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "SociosEdicion.aspx/eliminar" + tipo,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#div" + tipo).hide();
            $("#img" + tipo).attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}
function guardarFechaVencimiento() {
    $('#formEdicion').validate();
    if ($('#formEdicion').valid()) {

        var info = "{ id: " + parseInt($("#hdnID").val())
          + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
          + "', numero: '" + $("#hdnNumero").val()
          + "'}";

        $.ajax({
            type: "POST",
            url: "SociosEdicion.aspx/guardarFechaVenc",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divError").hide();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}
function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    //alert("2-" + msg);
    //$("#hdnAttachment").val("");
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/

function configControls() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDTarjeta: { type: "integer" },
                        Marca: { type: "string" },
                        Numero: { type: "string" },
                        Estado: { type: "string" },
                        IDSocio: { type: "integer" },
                        MotivoBaja: { type: "string" },
                        FechaBaja: { type: "date" },
                        FechaAsignacion: { type: "date" },
                        FechaVencimiento: { type: "date" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "SociosEdicion.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idSocio: parseInt($("#hfIDSocio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "Marca", title: "Marca", width: "100px" },
            { title: "", template: "#= renderOptions(data) #", width: "30px" },
            { field: "Numero", title: "Numero", width: "120px" },
            { field: "FechaAsignacion", title: "Fecha Asig", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "FechaVencimiento", title: "Fecha Venc", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "Estado", title: "Estado", width: "80px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "Credito", title: "Crédito $", width: "80px", format: "{0:c}" },
            { field: "Giftcard", title: "Giftcard $", width: "80px", format: "{0:c}" },
            { field: "Total", title: "Total $", width: "80px", format: "{0:c}" },
            { field: "POS", title: "POS", width: "80px" },
            { field: "MotivoBaja", title: "Motivo Baja", width: "120px" },
            { field: "FechaBaja", title: "Fecha de Baja", format: "{0:dd/MM/yyyy}", width: "80px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var fecha = dataItem.FechaVencimiento;
        day = fecha.getDate(),
        month = fecha.getMonth() + 1,
        year = fecha.getFullYear();
        $("#txtFechaVencimiento").val(day + "/" + month + "/" + year);
        $("#hdnID").val(dataItem.IDTarjeta);
        $("#hdnNumero").val(dataItem.Numero);
        $("#modalCargarFechaVencimiento").modal("show");
    });
}

function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    $filter.push({ field: "IDSocio", operator: "equal", value: parseInt($("#hfIDSocio").val()) });

    grid.dataSource.filter($filter);
}

function asociarTarjeta() {
    $("#divErrorTarjetas").hide();
    $("#divOk").hide();
    $("#divError").hide();
    $("#btnAsociarTarjeta").attr("disabled", true);

    if ($("#ddlTarjetas").val() == "") {
        $("#divErrorTarjetas").html("Seleccione una tarjeta");
        $("#divErrorTarjetas").show();
    }
    else {
        var info = "{ IDSocio: " + parseInt($("#hfIDSocio").val())
           + ", Tarjeta: '" + $("#ddlTarjetas").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "sociosEdicion.aspx/asociarTarjeta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtNuevaTarjeta").val("");
                filter();
                $("#btnAsociarTarjeta").attr("disabled", false);
                $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");
                $("#divResultado1").hide();
                $("#divResultado2").hide();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTarjetas").html(r.Message);
                $("#divErrorTarjetas").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#btnAsociarTarjeta").attr("disabled", false);
            }
        });
    }
}

function buscarTarjetas() {
    $("#divErrorTarjetas").hide();
    $("#divOk").hide();
    $("#divError").hide();
    $("#divResultado1").hide();
    $("#divResultado2").hide();

    if ($("#txtNuevaTarjeta").val() == "" || $("#txtNuevaTarjeta").val().length < 5) {
        $("#divErrorTarjetas").html("Ingrese una terminación de una tarjeta");
        $("#divErrorTarjetas").show();
    }
    else {
        $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");

        var info = "{ tarjeta: '" + $("#txtNuevaTarjeta").val() + "'}";

        $.ajax({
            type: "POST",
            url: "sociosEdicion.aspx/buscarTarjetas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTarjetas"));
                    }

                    if (data.d.length > 0) {
                        $("#divResultado1").show();
                        $("#divResultado2").show();
                    }
                    else {
                        $("#divErrorTarjetas").html("No se encontraron resultados");
                        $("#divErrorTarjetas").show();
                    }
                    //$('#ddlEmpleado').trigger('liszt:updated');
                }
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTarjetas").html(r.Message);
                $("#divErrorTarjetas").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}
function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    return html;
}
function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    /*if ($("#txtPuntosDisponibles").numeric().val() == "")
        $("#txtPuntosDisponibles").val("0");
    if ($("#txtPuntosTotales").numeric().val() == "")
        $("#txtPuntosTotales").val("0");*/

    
    if ($('#formSocio').valid()) {

        var rdbSexo = "I";
        if ($("#rdbFem")[0].checked == true)
            rdbSexo = "F";
        else if ($("#rdbMas")[0].checked == true)
            rdbSexo = "M";
        var info = "{ IDSocio: '" + $("#hfIDSocio").val()
            //+ "', Tarjeta: '" + $("#txtTarjeta").numeric().val()
            + "', NroCuenta: '" + $("#txtNroCuenta").val()
            + "', Nombre: '" + $("#txtNombre").val()
            + "', Apellido: '" + $("#txtApellido").val()
            + "', Email: '" + $("#txtEmail").val()
            + "', Dia: '" + $("#ddlDia option:selected").html()
            + "', Mes: '" + $("#ddlMes option:selected").val()
            + "', Anio: '" + $("#ddlAnio option:selected").html()
            + "', Sexo: '" + rdbSexo
            + "', TipoDoc: '" + $("#ddlTipoDoc option:selected").html()
            + "', NroDoc: '" + $("#txtNroDoc").val()
            + "', Telefono: '" + $("#txtTelefono").val()
            + "', Celular: '" + $("#txtCelular").val()
            + "', EmpresaCelular: '" + $("#ddlEmpresaCelular").val()
            // + "', PuntosDisponibles: '" + $("#txtPuntosDisponibles").numeric().val()
            // + "', PuntosTotales: '" + $("#txtPuntosTotales").numeric().val()
            + "', Observaciones: '" + $("#txtObservaciones").val()
            + "', NumeroSube: '" + $("#txtNumSUBE").val()
            + "', NumeroMonedero: '" + $("#txtNumMonedero").val()
            + "', NumeroTransporte: '" + $("#txtNumTarjTransporte").val()
            + "', Patente: '" + $("#txtPatente").val()
            + "', Twitter: '" + $("#txttwitter").val()
            + "', Facebook: '" + $("#txtFacebook").val()
            + "', GrabarDomicilio: '" + $("#hfGrabarDomicilio").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
            + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
            + "', Fax: '" + $("#txtFax").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "', Lat: '" + $("#txtLatitud").val()
            + "', Long: '" + $("#txtLongitud").val()
           + "', CostoTransaccional: '" + $("#txtCostoTransaccional").val()
            + "', CostoSeguro: '" + $("#txtCostoSeguro").val()
            + "', CostoPlusin: '" + $("#txtCostoPlusin").val()
            + "', CostoSMS: '" + $("#txtCostoSMS").val()

            + "'}";

        $.ajax({
            type: "POST",
            url: "sociosEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#hfGrabarDomicilio").val("1");
                $("#hfVerTarjetas").val("1");
                //$($("#Tabs ul li a")[1]).removeAttr("disabled");
                //$($("#Tabs ul li a")[2]).removeAttr("disabled");

                $("#hfIDSocio").val(data.d);
                toggleTabs();
                filter();
                //window.location.href = "socios.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

/*function irTarjetasSustitucion() {
    window.location.href = "TarjetasSustitucion.aspx?IDSocio=" + $("#hfIDSocio").val();
}*/

function toggleTabs() {
    if ($("#hfIDSocio").val() != "0") {
        $($("#Tabs ul li a")[2]).removeClass("hide");
        $("#divUploadFoto").show();
    }
    else
        $($("#Tabs ul li a")[2]).addClass("hide");
}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[2]).click(function () {
        $("#divBotones").hide();
    });
}

$(document).ready(function () {
    configControls();
    toggleTabs();
    toggleButtons();
    configDatePicker();
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtNroDoc, #txtNuevaTarjeta, #txtNroCuenta").numeric();
    $("#txtCostoTransaccional, #txtCostoSeguro,#txtCostoSMS, #txtCostoPlusin").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $('#formSocio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });



    if ($("#hfIDSocio").val() != "0")
        $("#litTitulo").html("Edición de " + $("#txtApellido").val() + ", " + $("#txtNombre").val());
    else
        $("#litTitulo").html("Alta de Socio");
});