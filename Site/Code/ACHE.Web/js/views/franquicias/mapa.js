﻿var map = null;
var gmarkers = [];
var gpolygons = [];
var markerCluster;



function initialize() {


    $("#imgLoading").hide();

    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map-canvas'), {
      zoom: 12,
      center: {lat: -34.543835, lng: -58.561412},
      mapTypeId: 'terrain'
    });

    var ctaLayer = new google.maps.KmlLayer({
        url: 'http://www.urubademo.com/partido.kml',
        map: map
    });

    loadMultiselects();

    sedesChangeController();

   // localidadesChangeController();

}

function loadMultiselects() {
   

    if ($('#ddlLocalidades').length) {
        $('#ddlLocalidades').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-searchLocalidades" autocomplete="off" placeholder="Selecciona 1 o más Localidades"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });
    }

    if ($('#ddlActividades').length) {
        $('#ddlActividades').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-searchAct" autocomplete="off" placeholder="Selecciona 1 o más Actividades"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

    }

    if ($('#ddlSedes').length) {
        $('#ddlSedes').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-searchSedes" autocomplete="off" placeholder="Selecciona 1 o más Sedes"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

    }

    
}


function cargarSubRubros() {

    var idRubro = 0

    if (parseInt($("#ddlRubro").val()) > 0) {
        idRubro = parseInt($("#ddlRubro").val());
    }
    var info = "{ IDRubroPadre: " + idRubro + " }";

    $.ajax({
        type: "POST",
        url: "mapa.aspx/cargarSubRubros",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#ddlSubRubro").empty();
                //$("#ddlSubRubro").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#ddlSubRubro").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                $('#ddlSubRubro').trigger("liszt:updated");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
        }
    });


}

function buscar() {

    $("#imgLoading").show();
    $("#btnBuscar").attr("disabled", true);

    //CLEAR
    clearMarkers();


    clearPolygons();

    //GET
    getPolygons();

    setTimeout(getMarkers, 1000);



}

function getPolygons() {

    if ($("#ddlLocalidades").val() != "" && $("#ddlLocalidades").val() != null)
        polygons = $("#ddlLocalidades").val();
    else
        polygons = "";


    var info = "{ Polygons: '" + polygons
        + "'}";


    $.ajax({
        type: "POST",
        url: "mapa.aspx/GetPolygons",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
   
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {

                    showPolygon(data.d[i]);          

                }
            }
        }
    });
}



function getMarkers() {


    $("#imgLoading").show();
    $("#btnBuscar").attr("disabled", true);


    var tipo = "C";
    if ($("#rdbSocios").is(':checked'))
        tipo = "S";

    var idMarca = 0;
    if ($("#ddlMarcas").val() != "")
        idMarca = parseInt($("#ddlMarcas").val());

    var idRubro = 0;
    if ($("#ddlRubro").val() != "")
        idRubro = parseInt($("#ddlRubro").val());
    var idSubRubro = 0;
    if ($("#ddlSubRubro").val() != "" && $("#ddlSubRubro").val() != null)
        idSubRubro = parseInt($("#ddlSubRubro").val());

    var actividades = '';
    if ($("#ddlActividades").val() !== null)
        actividades = $("#ddlActividades").val();

    var sedes = '';
    if ($("#ddlSedes").val() !== null)
        sedes = $("#ddlSedes").val();

    var info = "{ tipo: '" + tipo
        + "', marca: '" + idMarca
        + "', rubro: '" + idRubro
        + "', subRubro: '" + idSubRubro
        + "', edad: '" + $("#ddlEdad").val()
        + "', actividades: '" + actividades
        + "', sedes: '" + sedes
        + "'}";

    var visibleMarkers = [];


    $.ajax({
        type: "POST",
        url: "mapa.aspx/GetMarkers",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {

                    var name = data.d[i].Nombre;
                    var address = data.d[i].Direccion;
                    var lat = data.d[i].Lat.replace(/,/g, '.');
                    var lng = data.d[i].Lng.replace(/,/g, '.');
                    var cat = data.d[i].Categoria;
                    var contenido = data.d[i].Contenido;
                    var ficha = data.d[i].Ficha;
                    var foto = data.d[i].Foto;
                    //var icono = new google.maps.MarkerImage("/img/markers/" + data.d[i].Icono + ".png");                    var icono = "/files/rubros/" + data.d[i].Icono;

                    var markerCoords = new google.maps.LatLng(lat, lng);

                    /// chequeo si hay marcadores en la misma posicion y hago un offset minimo
                    if (gmarkers.length != 0) {
                        for (i = 0; i < gmarkers.length; i++) {
                            var existingMarker = gmarkers[i];
                            var pos = existingMarker.getPosition();
                            if (markerCoords.equals(pos)) {
                                var a = 360.0 / gmarkers.length;
                                var newLat = pos.lat() + -.00004 * Math.cos((+a * i) / 180 * Math.PI);  //x
                                var newLng = pos.lng() + -.00004 * Math.sin((+a * i) / 180 * Math.PI);  //Y
                                var markerCoords = new google.maps.LatLng(newLat, newLng);
                            }
                        }
                    }
                      var marker = new google.maps.Marker({
                        position: markerCoords,
                        icon: icono,
                        //shadow: iconShadow,
                        map: map,
                        title: name,
                        zIndex: Math.round(markerCoords.lat() * -100000) << 5,
                        mycategory: cat,
                        description: name,
                        content: contenido,
                        link: ficha,
                        photo: foto
                    });




                    var infoWindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {

                            var aux = '<div id="contentInfoWindow' + i + '" class="contentMap">\
                                <div class="contentImg">\
                                    <img src="' + this.photo + '" title="" style="width:150px" />\
                                    <a href="' + this.link + '">Ver ficha</a>\
                                </div>\
                                <div class="contentTxt">\
                                <h2>' + this.description + '</h2>' + this.content + ' </div>\
                                <div class="clear"></div>\
                            </div>'

                            infoWindow.setContent(aux);
                            infoWindow.open(map, marker);
                        }
                    })(marker, i));

                    gmarkers.push(marker);
                }


 
                if (gpolygons.length > 0) {



                    for (var p = 0; p < gpolygons.length; p++) {


                        for (var i = 0; i < gmarkers.length; i++) {


                            if (google.maps.geometry.poly.containsLocation(gmarkers[i].position, gpolygons[p])) {
                                visibleMarkers.push(gmarkers[i]);
                            }
                            else {
                                gmarkers[i].setVisible(false);
                            }
                        }
                    }
                }

                for (var v = 0; v < visibleMarkers.length; v++) {
                    visibleMarkers[v].setVisible(true);
                }

                console.log('p' + gpolygons.length)

                console.log('c' + visibleMarkers.length)

                if (gpolygons.length > 0)
                    $("#litResultados").html("Resultados encontrados: <b>" + visibleMarkers.length + "</b>");
                else {
                    $("#litResultados").html("Resultados encontrados: <b>" + gmarkers.length + "</b>");
                    visibleMarkers = gmarkers;
                    console.log('ca' + visibleMarkers.length);
                }



                 markerCluster = new MarkerClusterer(map, visibleMarkers,
                     { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
             




                $("#imgLoading").hide();
                $("#btnBuscar").attr("disabled", false);
            }
        }
        //error: onXMLLoadFailed
    });
}




function showPolygon(data) {

    var triangleCoords = [];

 
    var yourString = data.Paths;
    var string = yourString.split(' ');


    // Creamos un array de floats para cada par de coordenadas
    var a = string.length;
    for (i = 0; i < a; i++) {
        string[i] = parseFloat(string[i]);
    }

    // Creamos un array para guardar los nuevos valores
    var b = string.length / 2;
    var array = [];
    for (i = 0; i < b; i++) {
        array[i] = [0, 0];
    }

    // Creamos un array con todos los pares de coordenadas
    var k = 0;
    for (i = 0; i < b; i++) {
        for (j = 0; j < 2; j++) {
            array[i][j] = string[k];
            k++;
        }
    }


    var paths = [];

    for (var j = 0; j < array.length; j++) {
        var pt = new google.maps.LatLng(array[j][1], array[j][0])
        paths.push(pt);
    }

  var polygon = new google.maps.Polygon({
      paths: paths,
     strokeColor: data.StrokeColor,
     strokeOpacity: data.StrokeOpacity,
     strokeWeight: data.StrokeWeight,
     map: map,
     fillColor: data.FillColor,
     fillOpacity: data.FillOpacity,
     content: data.Nombre,
    _uniqueId: data.IDPolygon
  });

    gpolygons.push(polygon);

    console.log('pushed')

  polygon.setMap(map);


  polygon.addListener('click', showArrays);
  infoWindow = new google.maps.InfoWindow;
}


  /** @this {google.maps.Polygon} */
  function showArrays(event) {

    var vertices = this.getPath();

    var contentString = '<b>'+this.content+'</b><br>' +
        'Cliqueaste en las coordenadas: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
        '<br>';

    markerCnt = 0;

    for (var i = 0; i < gmarkers.length; i++) {
      if (google.maps.geometry.poly.containsLocation(gmarkers[i].getPosition(), this)) {
        markerCnt++;
      }
    }

    contentString += '<br>' + "Beneficiarios dentro del Poligono: " + markerCnt;


    infoWindow.setContent(contentString);
    infoWindow.setPosition(event.latLng);

    infoWindow.open(map);
  }

function clearMarkers() {

    console.log(gmarkers.length)

    if (gmarkers.length > 0)
        markerCluster.clearMarkers();

    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setVisible(false);
    }



    gmarkers = [];
    $("#litResultados").html("");
}

function clearPolygons() {


    for (var i = 0; i < gpolygons.length; i++) {
        gpolygons[i].setMap(null);
    }

    gpolygons = [];
    $("#litResultados").html("");
    

}

function markerClick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}

function showMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(true);
        }
    }
}

function hideMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(false);
        }
    }
}

function filtroClick(box, categoria) {
    if (box.checked) {
        showMarkers(categoria);
    } else {
        hideMarkers(categoria);
    }
}

function toggleTipo(tipo) {
    if (tipo == "comercios") {
        $("#divComercios").show();
        $("#divSocios").hide();
    }
    else {
        $("#divComercios").hide();
        $("#divSocios").show();
    }
}




// drag and drop

$('#mapSearch').draggable();


function display(tipo) {

    if (tipo == 'sexo') {
         $('#divSexo').toggle();
    }

    if (tipo == 'capas') {
         $('#divPolygons').toggle();
    }

    if (tipo == 'actividades') {
        $('#divActividades').toggle();
    }

    if (tipo == 'Sedes')
        $("#divSedes").toggle();


}



function sedesChangeController() {

    $("#ddlSedes").change(function () {

        console.log('asdasd')

        //$('#ddlMarcas').html('');

        var sedes = '';
        if ($("#ddlSedes").val() != null)
            sedes = $("#ddlSedes").val();


        var info = "{ sedes: '" + sedes
            + "', Actividades: '" + "none"
            + "'}";


        $.ajax({
            type: "POST",
            url: "mapa.aspx/actividadesPorSedes",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlActividades').empty().multiSelect('refresh');


                if (Object.keys(data.d).length > 0) {
                    $.each(data.d, function (k, v) {
                        $("#ddlActividades").append($("<option/>").val(v.ID).text(v.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);


                    $('#ddlActividades').multiSelect('refresh');

                }



                else {
                    $('#ddlActividades').empty().multiSelect('refresh');
                    console.log("nothing")
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}


    function localidadesChangeController() {

        $("#ddlLocalidades").change(function () {

            var localidades = '';
            if ($("#ddlLocalidades").val() != null)
                localidades = $("#ddlLocalidades").val();


            var info = "{ localidades: '" + localidades
                + "', Actividades: '" + "none"
                + "'}";


            $.ajax({
                type: "POST",
                url: "mapa.aspx/sedesPorLocalidades",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {

                    $('#ddlSedes').empty().multiSelect("refresh"); //remove all child nodes


                    if (Object.keys(data.d).length > 0) {
                        $.each(data.d, function (k, v) {
                            $("#ddlSedes").append($("<option/>").val(v.Nombre).text(v.Nombre));
                        });


                        $("#divErrorTr").hide();
                        $("#imgLoadingTr").hide();
                        $("#lnkDownloadTr").show();
                        $("#lnkDownloadTr").attr("href", data.d);
                        $("#lnkDownloadTr").attr("download", data.d);
                        $("#btnExportarTr").attr("disabled", false);

                        $("#ddlSedes").multiSelect("refresh");

                    }
                    else {
                        $("#ddlSedes").empty().trigger('liszt:updated');
                        console.log("nothing")
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorTr").html(r.Message);
                    $("#divErrorTr").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").hide();
                    $("#btnExportarTr").attr("disabled", false);
                }
            });
        });

    }