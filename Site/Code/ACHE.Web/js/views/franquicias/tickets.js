﻿

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtNroTicket, #txtFechaDesde, #txtFechaHasta, #Estado,#txtAsunto,#cmbAreas,#txtUsuario").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#txtNroTicket").numeric();

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDTicket: { type: "integer" },
                        Franquicia: { type: "string" },
                        FechaAlta: { type: "date" },
                        Estado: { type: "string" },
                        Area: { type: "string" },
                        Asunto: { type: "string" },
                        Usuario: { type: "string" },
                        FechaCierre: { type: "string" },
                        IDAreaTicket: { type: "integer" },
                        Prioridad: { type: "string" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Ticket.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Opciones", width: "50px" },
            { field: "IDTicket", title: "Ticket #", width: "70px" },
            { field: "Asunto", title: "Asunto", width: "200px" },
            { field: "Usuario", title: "Usuario", width: "120px" },
            //{ field: "Franquicia", title: "Franquicia", width: "150px" },
            { field: "FechaAlta", title: "Fecha Alta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "FechaCierre", title: "Fecha Cierre", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "Estado", title: "Estado", width: "100px" },
            { field: "Area", title: "Area", width: "120px" },
            { field: "Prioridad", title: "Prioridad", width: "120px" }

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Tickete.aspx?IDTicket=" + dataItem.IDTicket;
    });


}

function Nuevo()
{
    window.location.href = "ticketn.aspx";
}


function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var IDTicket = $("#txtNroTicket").val();
    if (IDTicket != "") {
        $filter.push({ field: "IDTicket", operator: "equal", value: parseInt(IDTicket) });
    }
    var Estado = $("#txtEstado").val();
    if (Estado != "") {
        $filter.push({ field: "Estado", operator: "equal", value: Estado });
    }
    var Prioridad = $("#txtPrioridad").val();
    if (Prioridad != "") {
        $filter.push({ field: "Prioridad", operator: "equal", value: Prioridad });
    }
    var Asunto = $("#txtAsunto").val();
    if (Asunto != "") {
        $filter.push({ field: "Asunto", operator: "contains", value: Asunto });
    }
    var IDAreaTicket = $("#cmbAreas").val();
    if (IDAreaTicket != "") {
        $filter.push({ field: "IDAreaTicket", operator: "equal", value: parseInt(IDAreaTicket) });
    }
    var Usuario = $("#txtUsuario").val();
    if (Usuario != "") {
        $filter.push({ field: "Usuario", operator: "contains", value: Usuario });
    }
    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});


function exportar() {

    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var idTicket = "";
    if ($("#txtNroTicket").val() != null && $("#txtNroTicket").val() != "") {
        idTicket = $("#txtNroTicket").val();
    }

    var estado = ""
    if ($("#txtEstado").val() != null && $("#txtEstado").val() != "") {
        estado = $("#txtEstado").val();
    }
    var prioridad = ""
    if ($("#txtPrioridad").val() != null && $("#txtPrioridad").val() != "") {
        prioridad = $("#txtPrioridad").val();
    }
    var Asunto = "";
    if ($("#txtAsunto").val() != null && $("#txtAsunto").val() != "") {
        Asunto = $("#txtAsunto").val();
    }
    var IDAreaTicket = 0;
    if ($("#cmbAreas").val() != null && $("#cmbAreas").val() != "") {
        IDAreaTicket = parseInt($("#cmbAreas").val());
    }
    var Usuario = "";
    if ($("#txtUsuario").val() != null && $("#txtUsuario").val() != "") {
        Usuario = $("#txtUsuario").val();
    }

    var info = "{ FechaDesde: '" + $("#txtFechaDesde").val()
            + "', FechaHasta: '" + $("#txtFechaHasta").val()
            + "',  IDTicket: '" + idTicket
            + "', Estado: '" + estado
            + "', Prioridad: '" + prioridad
            + "', Asunto: '" + Asunto
            + "', IDAreaTicket: " + IDAreaTicket
            + ", Usuario: '" + Usuario
            + "'}";

    $.ajax({
        type: "POST",
        url: "Ticket.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}