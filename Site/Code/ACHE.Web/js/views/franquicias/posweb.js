﻿function mostrarInfoSocio() {
    var info = "{ id: " + $("#ddlTrSocio").val() + "}";
    var urlTr = "transacciones.aspx/GetBySocio";

    if ($("#ddlTrSocio").val() != "0") {

        $.ajax({
            type: "POST",
            url: "posweb.aspx/obtenerInfoSocio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {

                    $("#lblDni").html("DNI: " + data.d.ID);

                    if (data.d.Nombre != "")
                        $("#imgSocio").attr("src", "/files/socios/" + data.d.Nombre);
                    else
                        $("#imgSocio").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTrSocio").html(r.Message);
                $("#divErrorTrSocio").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $("#lblDni").html("DNI: No disponible");
        $("#imgSocio").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");

        info = "{ tarjeta: '" + $("#ddlTrTarjeta").val() + "'}";
        var urlTr = "transacciones.aspx/GetByTarjeta";
    }

    $("#tBodyHistorial").html("");

    $.ajax({
        type: "POST",
        url: urlTr,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data != null) {
                var html = "";

                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        html += "<tr>";

                        html += "<td>" + data.d[i].Fecha + "</td>";
                        html += "<td>" + data.d[i].Hora + "</td>";
                        html += "<td>" + data.d[i].Operacion + "</td>";
                        html += "<td>" + data.d[i].Comercio + "</td>";
                        html += "<td>" + data.d[i].ImporteOriginal + "</td>";
                        html += "<td>" + data.d[i].ImporteAhorro + "</td>";
                        html += "<td>" + data.d[i].Puntos + "</td>";

                        html += "</tr>";
                    }
                    $("#tBodyHistorial").html(html);
                }
                else
                    $("#tBodyHistorial").html("<tr><td colspan='6'>No hay un detalle disponible</td></tr>");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTrSocio").html(r.Message);
            $("#divErrorTrSocio").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function getPuntos(puntos) {
    if ($("#ddlTrSocio").val() != "0")
        $("#lblPuntos").html("Puntos disponibles: " + puntos);
}

function getPrecio(idPremio) {
    var puntosPesos = idPremio.split('_')[1];
    $("#txtTrImporte").val(puntosPesos);
}

function buscarSocios() {
    if ($("#txtValorSocio").val() == "") {
        $("#divErrorTrSocio").html("Por favor, ingrese el valor a buscar");
        $("#divErrorTrSocio").show();

        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
    }
    else {
        $("#divErrorTrSocio").hide();
        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("");

        var info = "{ tipo: '" + $("#ddlTrBuscarSocio").val()
                + "', valor: '" + $("#txtValorSocio").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "posweb.aspx/buscarSocios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrSocio"));
                    }

                    if (data.d.length == 0) {
                        $("#divErrorTrSocio").html("No se encontraron socios");
                        $("#divErrorTrSocio").show();
                    }
                    else if (data.d.length == 1) {
                        $("#ddlTrSocio").val(data.d[0].ID);
                        if ($("#ddlTrSocio").val() == "0") {
                            $("#ddlTrTarjeta").html("<option value='" + data.d[0].Nombre + "'>" + data.d[0].Nombre + "</option>");
                            mostrarInfoSocio();
                        }
                        else {
                            mostrarInfoSocio();
                            buscarTarjetas();
                        }
                    }
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTrSocio").html(r.Message);
                $("#divErrorTrSocio").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function buscarTarjetas() {
    if ($("#ddlTrSocio").val() != "") {

        //alert($("#ddlTrSocio").val());
        if ($("#ddlTrSocio").val() == "0") {
            $("#ddlTrTarjeta").html("<option value='" + $("#ddlTrSocio option:selected").text() + "'>" + $("#ddlTrSocio option:selected").text() + "</option>");
            mostrarInfoSocio();
        }
        else {
            $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");

            var info = "{ socio: " + $("#ddlTrSocio").val() + "}";

            $.ajax({
                type: "POST",
                url: "posweb.aspx/buscarTarjetas",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data != null) {
                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrTarjeta"));
                        }

                        if (data.d.length == 0) {
                            $("#divErrorTrSocio").html("No se encontraron tarjetas asociadas");
                            $("#divErrorTrSocio").show();
                        }
                        else if (data.d.length == 1) {
                            $("#ddlTrTarjeta").val(data.d[0].ID);
                            getPuntos(data.d[0].ID);
                            $("#ddlTrComercio").focus();
                        }
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorTrSocio").html(r.Message);
                    $("#divErrorTrSocio").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });

            mostrarInfoSocio();
        }
    }
    else
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
}

/*
function buscarComercios() {
    if ($("#txtValorComercio").val() == "") {
        $("#divErrorTrComercio").html("Por favor, ingrese el valor a buscar");
        $("#divErrorTrComercio").show();
    }
    else {
        $("#divErrorTrComercio").hide();
        $("#ddlTrComercio").html("<option value=''><option>").trigger('liszt:updated');

        var info = "{ tipo: '" + $("#ddlTrBuscarComercio").val()
                + "', valor: '" + $("#txtValorComercio").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "posweb.aspx/buscarComercios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrComercio"));
                    }

                    if (data.d.length == 0) {
                        $("#divErrorTrComercio").html("No se encontraron comercios");
                        $("#divErrorTrComercio").show();
                    } else if (data.d.length == 1) {
                        $("#ddlTrComercio").val(data.d[0].ID);
                    }
                    $("#ddlTrComercio").trigger('liszt:updated');
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTrComercio").html(r.Message);
                $("#divErrorTrComercio").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}
*/
function changeTipoOperacion(operacion) {
    $("#ddlPremios").val("");
    $("#txtTrImporte").val("");

    if (operacion == "Canje") {
        if ($("#hdnTieneCatalogo").val() == "1") {
            $("#txtTrImporte").attr("disabled", true);
            $("#divCatalogo").show();
        }
        else {
            $("#divCatalogo").hide();
            $("#txtTrImporte").attr("disabled", false);
        }
    }
    else {
        $("#divCatalogo").hide();
        $("#txtTrImporte").attr("disabled", false);
    }
}

//function NuevaTr() {
//    $("#divError").hide();
//    $("#divErrorTr").hide();
//    $("#divErrorTrSocio").hide();
//    $("#divErrorTrComercio").hide();

//    $("#txtValorSocio").val("");
//    $("#txtValorComercio").val("");

//    $("#ddlTrSocio").html("");
//    $("#ddlTrTarjeta").html("");
//    $("#ddlTrComercio").val("");

//    $("#txtTrDescripcion").val("");
//    $("#txtTrImporte").val("");
//    //$("#txtTrPuntos").val("");
//    //$("#txtTrTarjeta").val("");

//    $('#modalTransacciones').modal('show');
//}

function confirmarOperacion(cargarOtraOperacion) {
    $("#divErrorTr").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    //alert($('#formEdicion').valid());

    if ($('#formEdicion').valid()) {

        if ($("#ddlTrTipo").val() == "Canje" && $("#hdnTieneCatalogo").val() == "1" && $("#ddlPremios").val() == "") {
            $("#divError").html("Por favor, seleccione un premio");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        else if ($("#txtTrImporte").val() == "" || $("#ddlTrTarjeta").val() == "" || $("#ddlTrComercio").val() == "") {
            $("#divError").html("Por favor, ingrese el numero de tarjeta, comercio e importe");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        else {

            var msg = "Usted está generando una operación de " + $("#ddlTrTipo option:selected").text() + ".<br /> ¿Desea continuar?";
            smoke.confirm(msg, function (e) {
                if (e) {
                    generarTransaccion(cargarOtraOperacion);
                    //smoke.alert('"yeah it is" pressed', { ok: "close" });
                } else {
                    //smoke.alert('"no way" pressed', { ok: "close" });
                }
            }, { ok: "Aceptar", cancel: "Cancelar" });
        }
    }
    else {
        return false;
    }

}

function anularOperacion(cargarOtraOperacion) {
    //alert($('#formEdicion').valid());


    var msg = "Usted está generando la anulación de la operación #" + $("#hdnID").val() + ".<br /> ¿Desea continuar?";
    smoke.confirm(msg, function (e) {
        if (e) {
            //generarTransaccion();
            smoke.alert('"yeah it is" pressed', { ok: "close" });
        } else {
            smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}

function generarTransaccion(cargarOtraOperacion) {

    $("#divErrorTr").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {

        if ($("#ddlTrTipo").val() == "Canje" && $("#hdnTieneCatalogo").val() == "1" && $("#ddlPremios").val() == "") {
            $("#divError").html("Por favor, seleccione un premio");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        else if ($("#txtTrImporte").val() == "" || $("#ddlTrTarjeta").val() == "" || $("#ddlTrComercio").val() == "") {
            $("#divError").html("Por favor, ingrese el numero de tarjeta, comercio e importe");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        else if ($("#txtTrImporte").val().includes(".")) {
            $("#divError").html("El importe no tiene el formato correcto, para agregar decimales ingrese ',' (coma) en vez de '.' (punto) previo a los decimales");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        else {

            var info = "{ tipo: '" + $("#ddlTrTipo").val()
                + "', tarjeta: '" + $("#ddlTrTarjeta option:selected").text()
                + "', idComercio: " + parseInt($("#ddlTrComercio").val())
                + ", importe: " + parseInt($("#txtTrImporte").val())
                + ", descripcion: '" + $("#txtTrDescripcion").val()
                + "', idPremio: '" + $("#ddlPremios").val().split('_')[0]
                + "', puntoVenta: '" + $("#txtPuntoVenta").val()
                + "', tipoComprobante: '" + $("#ddlTipoComprobante").val()
                + "', nroComprobante: '" + $("#txtNroComprobante").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "posweb.aspx/grabar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    if (!cargarOtraOperacion)
                        window.location.href = "poswebcomprobante.aspx?Id=" + data.d;
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function configControls() {
    $("#txtTrImporte, #txtPuntoVenta, #txtNroComprobante").numeric();
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtValorSocio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarSocios();
            return false;
        }
    });

    /*$("#txtValorComercio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarComercios();
            return false;
        }
    });*/

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
    });
}

$(document).ready(function () {
    configControls();

    $("#txtValorSocio").focus();
});