var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);

$(document).ready(function () {
    setTimeout("ObtenerCantTarjetasDinero()", 1000);
    setTimeout("ObtenerCantTarjetasStock()", 1000);
    setTimeout("ObtenerTotalSaldoCargado()", 1000);
    setTimeout("ObtenerTotalSaldoUtilizado()", 1000);
    setTimeout("ObtenerTotalArancelCarga()", 1000);
    setTimeout("ObtenerTotalArancelDescarga()", 1000);
    setTimeout("ObtenerDineroDisponibleGiftCard()", 1000);

    setTimeout("gebo_charts.fl_tarjetas_utilizadas()", 2000);
    setTimeout("gebo_charts.fl_carga_descarga()", 2000);
    setTimeout("gebo_charts.fl_carga_descarga_detalle()", 2000);

    setTimeout("ObtenerTop10Socios()", 1500);
    setTimeout("ObtenerTop10Comercios()", 1500);
});

//* charts
gebo_charts = {
    fl_tarjetas_utilizadas: function () {
        var elem = $('#fl_tarjetas_utilizadas');

        var data = ObtenerTarjetasUtilizadas();

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    fl_carga_descarga: function () {

        // Setup the placeholder reference
        var elem = $('#fl_carga_descarga');

        var carga = ObtenerCarga();
        var descarga = ObtenerDescarga();
        
        var data = [
            { label: "Carga", data: carga },
            { label: "Descarga", data: descarga }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_carga_descarga_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_carga_descarga_detalle');

        var carga = ObtenerCargaDiario();
        var descarga = ObtenerDescargaDiario();
        
        for (var i = 0; i < carga.length; ++i) { carga[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < descarga.length; ++i) { descarga[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Carga", data: carga },
                { label: "Descarga", data: descarga }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = $" + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    }
};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerTarjetasUtilizadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTarjetasUtilizadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Carga & Descarga, 4 meses*/
function ObtenerCarga() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerCarga",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerDescarga() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerDescarga",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}


/*Descarga & Carga, 30 d�as*/
function ObtenerCargaDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerCargaDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerDescargaDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerDescargaDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {;
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
            /*ddata.push([Date.UTC(2014, 0, 21, 08, 0, 0), 1180]);
            ddata.push([Date.UTC(2014, 0, 22, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 23, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 24, 08, 0, 0), 1212]);
            ddata.push([Date.UTC(2014, 0, 25, 08, 0, 0), 9922]);
            ddata.push([Date.UTC(2014, 0, 26, 08, 0, 0), 1888]);*/

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}


function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

/*METODOS DE OBTENCION DE DATOS DE LOS ICONOS*/

function ObtenerCantTarjetasDinero() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerCantTarjetasDinero",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cant_tarjetas_dinero").html(data.d);
            }
        }
    });
}

function ObtenerCantTarjetasStock() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerCantTarjetasStock",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cant_tarjetas_stock").html(data.d);
            }
        }
    });
}

function ObtenerTotalSaldoCargado() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTotalSaldoCargado",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_saldo_cargado").html("$ " + data.d);
            }
        }
    });
}

function ObtenerDineroDisponibleGiftCard() {

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerDineroDisponibleGiftCard",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_dinero_disponible_giftcard").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalSaldoUtilizado() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTotalSaldoUtilizado",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_saldo_utilizado").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalArancelCarga() {
    //$("#resultado_puntos_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTotalArancelCarga",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_arancel_carga").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalArancelDescarga() {
    //$("#resultado_facturacion_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTotalArancelDescarga",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_arancel_descarga").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-gift.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}
