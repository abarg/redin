﻿/**LOGO && UPLOAD**/
function deleteUpload(file, tipo) {

    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "premiose.aspx/eliminar" + tipo,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#div" + tipo).hide();
            $("#img" + tipo).attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    //alert("2-" + msg);
    //$("#hdnAttachment").val("");
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnID").val())
            + ", codigo: '" + $("#txtCodigo").val()
            + "', descripcion: '" + $("#txtDescripcion").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', valorPuntos: " + parseInt($("#txtValorPuntos").val())
            + ", valorPesos: " + parseInt($("#txtValorPesos").val())
            + ", stock: " + $("#txtStock").val()
            + ", stockMinimo: " + $("#txtStockMinimo").val()
            + ", idRubro: " + parseInt($("#ddlRubro").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "premiose.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "premios.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

$(document).ready(function () {
    configControls();
});


function configControls() {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    configDatePicker();
    $("#txtCodigo, #txtStock, #txtStockMinimo, #txtValorPuntos, #txtValorPesos").numeric();

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtDescripcion").val());
        $("#divUploadFoto").show();
    }
    else
        $("#litTitulo").html("Alta de Premio");
}