﻿function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnID").val())
            + ", control: '" + $("#txtCodigoControl").val()
            + "', parte1: '" + $("#txtCodigoParte1").val()
            + "', parte2: '" + $("#txtCodigoParte2").val()
            + "', idEmpresaCanje: " + $("#cmbEmpresa").val()
            + ", descripcion: '" + $("#txtDescripcion").val()
            + "', costoInterno: " + parseFloat($("#txtCostoInterno").val())
            + ", costoPuntos: " + parseInt($("#txtCostoPuntos").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "pinese.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "pines.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function configControls() {
    $("#txtCostoPuntos").numeric();
    $("#txtCostoInterno").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    configDatePicker();

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtCodigo").val());
        $("#spnModo").html("Edición");
    }
    else {
        $("#litTitulo").html("Alta de Pin");
        $("#spnModo").html("Alta");
    }
}

$(document).ready(function () {
    configControls();
});