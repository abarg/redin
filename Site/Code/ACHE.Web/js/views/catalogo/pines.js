﻿function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var empresa = $("#cmbEmpresa").val();
    if (empresa == "")
        empresa = 0;

    var info = "{ control: '" + $("#txtCodigoControl").val()
            + "', parte1: '" + $("#txtCodigoParte1").val()
            + "', parte2: '" + $("#txtCodigoParte2").val()
            + "', idEmpresa: " + empresa
            + " }";

    $.ajax({
        type: "POST",
        url: "pines.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    $("#txtCodigoControl").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#cmbEmpresa").change(function () {
        filter();
        return false;
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDPin: { type: "integer" },
                        IDEmpresaCanje: { type: "integer" },
                        EmpresaCanje: { type: "string" },
                        Control: { type: "string" },
                        Parte1: { type: "string" },
                        Parte2: { type: "string" },
                        CostoInterno: { type: "number" },
                        CostoPuntos: { type: "integer" },
                        Socio: { type: "string" },
                        FechaCompra: { type: "date" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "pines.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDPin", title: "ID", width: "50px", hidden: true },
            { field: "EmpresaCanje", title: "Empresa de Canje", width: "100px" },
            { field: "Control", title: "Control", width: "80px" },
            { field: "Parte1", title: "Parte 1", width: "80px" },
            { field: "Parte2", title: "Parte 2", width: "80px" },
            { field: "CostoInterno", title: "Costo", width: "80px", format:"{0:c}" },
            { field: "CostoPuntos", title: "Puntos", width: "80px" },
            { field: "Socio", title: "Socio", width: "120px" },
            { field: "FechaCompra", title: "Fecha Compra", width: "80px", format: "{0:dd/MM/yyyy}" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "pinese.aspx?ID=" + dataItem.IDPin;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "pines.aspx/Delete",
                data: "{ id: " + dataItem.IDPin + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function Nuevo() {
    window.location.href = "pinese.aspx";
}

function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var empresa = $("#cmbEmpresa").val();
    if (empresa != "") {
        $filter.push({ field: "IDEmpresaCanje", operator: "equal", value: parseInt(empresa) });
    }

    var control = $("#txtCodigoControl").val();
    if (control != "") {
        $filter.push({ field: "Control", operator: "contains", value: control });
    }
    var parte1 = $("#txtCodigoParte1").val();
    if (parte1 != "") {
        $filter.push({ field: "Parte1", operator: "contains", value: parte1 });
    }
    var parte2 = $("#txtCodigoParte2").val();
    if (parte2 != "") {
        $filter.push({ field: "Control", operator: "contains", value: parte2 });
    }
    grid.dataSource.filter($filter);
}

function verTodos() {
    $("#txtCodigoControl").val("");
    $("#txtCodigoParte1").val("");
    $("#txtCodigoParte2").val("");
    $("#cmbEmpresa").val("");
    filter();
}

$(document).ready(function () {
    configControls();
});