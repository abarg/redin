﻿function UploadCompleted(sender, args) {
    try {
        var fileExtension = args.get_fileName();
        fileExtension = fileExtension.toLowerCase();

        var xls = fileExtension.indexOf('.xls');
        var xlsx = fileExtension.indexOf('.xlsx');
        var txt = fileExtension.indexOf('.txt');
        var csv = fileExtension.indexOf('.csv');
        if ((xls > 0) || (xlsx > 0) || (txt > 0) || (csv > 0)) {
            $("#divOK").show();
            $("#divError").hide();
        }
        else {
            $("#divError").html("La extensión del archivo debe ser .xls, .xlsx, .txt o .csv");
            $("#divError").show();
            $("#divOK").hide();
        }
    }
    catch (e) {
        $("#divError").html(e);
        $("#divError").show();
    }
}


function UploadCompletedTxt(sender, args) {
    try {
        var fileExtension = args.get_fileName();
        var txt = fileExtension.toLowerCase().indexOf('.txt');
        if (txt > 0) {
            $("#divOK").show();
            $("#divError").hide();
        }
        else {
            $("#divError").html("La extensión del archivo debe ser .txt");
            $("#divError").show();
            $("#divOK").hide();
        }
    }
    catch (e) {
        $("#divError").html(e);
        $("#divError").show();
    }
}



function UploadError(sender) {
    $("#divError").html("Error al actualizar los datos");
    $("#divError").show();
    $("#divOK").hide();
}




$(document).ready(function () {

    if ($('#ddlTipoArchivo').val() == "5")
        $('#lnkPremiosPrueba').show();
    else 
        $('#lnkPremiosPrueba').hide();

    $('#ddlTipoArchivo').change(function () {
        if ($('#ddlTipoArchivo').val() == "5")
            $('#lnkPremiosPrueba').show();
        else if ($('#ddlTipoArchivo').val() == "3")
            $('#lnkPremiosPrueba').hide();
    });
});
