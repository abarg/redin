﻿function configControls() {
    $(".chzn-select-deselect").chosen({ allow_single_deselect: true });

    $('#cmbEmails').chosen().change();
    $("#cmbEmails").trigger("liszt:updated");
}

$(document).ready(function () {
    configControls();
    $("#cmbEmails").click(function () {
        var selectedValues = [];
        $("#cmbEmails :selected").each(function () {
            selectedValues.push($(this).val());
        });
        alert(selectedValues);
        return false;
    });
});


