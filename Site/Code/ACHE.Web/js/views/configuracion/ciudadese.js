﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();
    if ($('#formCiudad').valid()) {
        var info = "{ idCiudad: '" + $("#hdnIDCiudad").val()
            + "', Nombre: '" + $("#txtNombreCiudad").val()
            + "', idProvincia: " + parseInt($("#cmbProvincia").val())
            + ", CP: " + parseInt($("#txtCP").val())
            + "}";


        $.ajax({
            type: "POST",
            url: "Ciudadese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                window.location.href = "Ciudades.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {

    $("#cmbPaises").change(function cmbChange() {
        cargarProvincias();
    });
    if ($('#cmbProvincia').val() != "0") {
        cargarProvincias();
    }
    if ($("#hdnIDCiudad").val() > 0) {
        var idprov = $("#hdnIDProvincia").val();
        $('#cmbProvincia').val(parseInt(idprov));
    }


    
});



    function cargarProvincias() {
        $('#cmbProvincia').html('');
        var idPais = 0;
        if ($("#cmbPaises").val() != "")
            idPais = parseInt($("#cmbPaises").val());
        $.ajax({
            type: "POST",
            url: "Ciudadese.aspx/provinciasByPaises",
            data: "{ idPais: " + idPais
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async:false,
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#cmbProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divError").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });   
}