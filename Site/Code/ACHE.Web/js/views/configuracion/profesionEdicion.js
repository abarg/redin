﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formProfesion').valid()) {
        var info = "{ IDProfesion: '" + $("#hdnIDProfesion").val()
            + "', Nombre: '" + $("#txtNombre").val()

            + "'}";

        $.ajax({
            type: "POST",
            url: "ProfesionEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Profesion.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    $(".chosen").chosen();
});

