﻿/**LOGO**/

function deleteLogo(file, div) {

    var info = "{ id: " + parseInt($("#hdnIDRubro").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "rubrose.aspx/eliminarLogo",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
            $("#imgLogo").attr("src", "http://www.placehold.it/48x48/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    //alert("2-" + msg);
    //$("#hdnAttachment").val("");
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/

function agregarSubrubro() {
    $("#divErrorSubrubro").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtNombreRubro").val() == "") {
        $("#divErrorRubro").html("Por favor,complete todos los datos");
        $("#divErrorRubro").show();
    }
    else {
        var info = "{ IDSubrubro: " + parseInt($("#hdnIDSubrubro").val())
           + ", IDRubroPadre: " + parseInt($("#hdnIDRubro").val())
           + ", nombre: '" + $("#txtNombreSubrubro").val()
           + "'}";

        //alert(info);

        $.ajax({
            type: "POST",
            url: "rubrose.aspx/procesarSubrubro",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtNombreSubRubro").val("");
                $("#hdnIDSubrubro").val("0");
                $("#btnAgregarSubrubro").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorSubrubro").html(r.Message);
                $("#divErrorSubrubro").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function grabar() {
    $("#divError").hide();
    $("#divErrorRubro").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnIDRubro").val())
            + ", nombre: '" + $("#txtNombre").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "rubrose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#hdnIDRubro").val(data.d);
                $("#litTitulo").html("Edición de " + $("#txtNombre").val());

                $("#divUploadLogo").show();
                toggleTabs();
                filter();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function toggleTabs() {
    if ($("#hdnIDRubro").val() != "0") {
        $($("#Tabs ul li a")[1]).removeClass("hide");
    }
    else {
        $($("#Tabs ul li a")[1]).addClass("hide");
    }
}

function configControls() {

    //alert('configControls');

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSubrubro: { type: "integer" },
                        IDRubroPadre: { type: "integer" },
                        Nombre: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "rubrose.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtNombreSubrubro").val("");
                        $("#hdnIDSubrubro").val("0");
                        $("#btnAgregarSubrubro").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idRubro: parseInt($("#hdnIDRubro").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDSubrubro", title: "ID", width: "50px" },
            { field: "Nombre", title: "Nombre", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hdnIDSubrubro").val(dataItem.IDSubrubro);
        $("#txtNombreSubrubro").val(dataItem.Nombre);
        $("#btnAgregarSubrubro").html("Actualizar");
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "rubrose.aspx/Delete",
                data: "{ id: " + dataItem.IDSubrubro + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorSubrubro").html(r.Message);
                    $("#divErrorSubrubro").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function filter() {
    $("#divErrorSubrubro").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    toggleTabs();

    if ($("#hdnIDRubro").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
        $("#divUploadLogo").show();
    }
    else {
        $("#litTitulo").html("Alta de Rubro");
    }

});