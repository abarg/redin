﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var id = 0;
    if ($("#hfIDDealer").val() != "")
        id = $("#hfIDDealer").val();

    if ($('#formEdicion').valid()) {
        var info = "{ idDealer: " + id
            + ", nombre: '" + $("#txtNombre").val()
            + "', apellido: '" + $("#txtApellido").val()
            + "', pwd: '" + $("#txtPwd").val()
            + "', email: '" + $("#txtEmail").val()
            + "', codigo: '" + $("#txtCodigo").val()
            + "', comisionAlta: '" + $("#txtComisionAlta").val()
            + "', comisionTarjetas: '" + $("#txtComisionTarjetas").val()
            + "', comisionGiftCard: '" + $("#txtComisionGiftCard").val()
            + "', activo: " + $("#chkActivo").is(':checked')
            + "}";

        $.ajax({
            type: "POST",
            url: "Dealere.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
               // $('#divOk').show();
               // $("#divError").hide();
               //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Dealer.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}


$(document).ready(function () {

    $("#txtComisionAlta, #txtComisionTarjetas, #txtComisionGiftCard").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    /*
    if ($("#hfIDSocio").val() != "0")
        $("#litTitulo").html("Edición de " + $("#txtApellido").val() + ", " + $("#txtNombre").val());
    else
        $("#litTitulo").html("Alta de Socio");
    */
});