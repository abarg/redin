﻿function agregarUsuario() {
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtUsuario").val() == "" || $("#txtEmailUsuario").val() == "" || $("#txtPwd").val() == "") {
        $("#divErrorUsuario").html("Por favor,complete todos los datos");
        $("#divErrorUsuario").show();
    }
    else {
        var info = "{ IDFranquicia: " + parseInt($("#hdnID").val())
           + ", IDUsuario: " + parseInt($("#hfIDUsuario").val())
           + ", usuario: '" + $("#txtUsuario").val()
           + "', email: '" + $("#txtEmailUsuario").val()
           + "', pwd: '" + $("#txtPwd").val()
           + "', tipo: '" + $("#ddlTipoUsuario").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "franquiciase.aspx/procesarUsuario",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtUsuario").val("");
                $("#txtEmailUsuario").val("");
                $("#txtPwd").val("");
                $("#hfIDUsuario").val("0");
                $("#btnAgregarUsuario").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorUsuario").html(r.Message);
                $("#divErrorUsuario").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function grabar() {
    $("#divError").hide();
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($("#ddlTipoDoc").val() == "CUIT/CUIL") {
        if (this.CuitEsValido($("#txtNroDocumento").val()) == false) {
            $("#divError").html("CUIT Inválido");
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            return false;
        }
    }

    if ($('#formEdicion').valid()) {
        var rdbFormaPago = "D"; if ($("#rdbFormaPago_Tarjeta")[0].checked == true) rdbFormaPago = "T";

        var info = "{ IDFranquicia: " + $("#hdnID").val()
            + ", NombreFantasia: '" + $("#txtNombreFantasia").val()
            + "', RazonSocial: '" + $("#txtRazonSocial").val()
            + "', TipoDoc: '" + $("#ddlTipoDoc option:selected").val()
            + "', NroDocumento: '" + $("#txtNroDocumento").val()
            + "', IVA: '" + $("#ddlIVA option:selected").val()
            + "', Web: '" + $("#txtWeb").val()
            + "', Email: '" + $("#txtEmail").val()
            + "', Arancel: '" + $("#txtArancel").val()
            + "', ComisionTpCp: '" + $("#txtComisionTpCp").val()
            + "', ComisionTtCp: '" + $("#txtComisionTtCp").val()
            + "', ComisionTpCt: '" + $("#txtComisionTpCt").val()
            + "', PubLocal: '" + $("#txtPubLocal").val()
            + "', PubNacional: '" + $("#txtPubNacional").val()
            + "', Observaciones: '" + $("#txtObservaciones").val()
            + "', FechaAlta: '" + $("#txtFechaAlta").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
            + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
            + "', Fax: '" + $("#txtFax").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "', FormaPago: '" + rdbFormaPago
            + "', FormaPago_Banco: '" + $("#txtFormaPago_Banco").val()
            + "', FormaPago_TipoCuenta: '" + $("#ddlFormaPago_TipoCuenta").val()
            + "', FormaPago_NroCuenta: '" + $("#txtFormaPago_NroCuenta").val()
            + "', FormaPago_CBU: '" + $("#txtFormaPago_CBU").val()
            + "', FormaPago_Tarjeta: '" + $("#txtFormaPago_Tarjeta").val()
            + "', FormaPago_BancoEmisor: '" + $("#txtFormaPago_BancoEmisor").val()
            + "', FormaPago_NroTarjeta: '" + $("#txtFormaPago_NroTarjeta").val()
            + "', FormaPago_FechaVto: '" + $("#txtFormaPago_FechaVto").val()
            + "', FormaPago_CodigoSeg: '" + $("#txtFormaPago_CodigoSeg").val()
            + "', Contacto_Nombre: '" + $("#txtContacto_Nombre").val()
            + "', Contacto_Apellido: '" + $("#txtContacto_Apellido").val()
            + "', Contacto_Cargo: '" + $("#txtContacto_Cargo").val()
            + "', Contacto_Telefono: '" + $("#txtContacto_Telefono").val()
            + "', Contacto_Celular: '" + $("#txtContacto_Celular").val()
            + "', Contacto_Email: '" + $("#txtContacto_Email").val()
            + "', Contacto_Observaciones: '" + $("#txtContacto_Observaciones").val()
            + "', Contacto_Documento: '" + $("#txtContacto_Documento").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "franquiciase.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#hdnID").val(data.d);
                $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());
                toggleTabs();
                filter();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function filter() {
    $("#divErrorUsuario").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

function toggleTabs() {
    if ($("#hdnID").val() != "0") {
        $($("#Tabs ul li a")[5]).removeClass("hide");
    }
    else {
        $($("#Tabs ul li a")[5]).addClass("hide");
    }

    toggleButtons();
}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[2]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[3]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[4]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[5]).click(function () {
        $("#formButtons").hide();
    });
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtNroDocumento, #txtContacto_Documento").numeric();
    $("#txtFormaPago_NroCuenta, #txtFormaPago_CBU, #txtFormaPago_NroTarjeta, #txtFormaPago_FechaVto, #txtFormaPago_CodigoSeg").numeric();
    $("#txtArancel, #txtComisionTpCp, #txtComisionTtCp, #txtComisionTpCt, #txtPubLocal, #txtPubNacional").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "franquiciase.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmailUsuario").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idFranquicia: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmailUsuario").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin")
            $("#ddlTipoUsuario").val("A");
        else
            $("#ddlTipoUsuario").val("B");
        $("#btnAgregarUsuario").html("Actualizar");
    });

    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var usuario = dataItem.Usuario;
        var pwd = dataItem.Pwd;
        var info = "{ usuario: '" + usuario
            + "', pwd: '" + pwd
            + "'}";

        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginFranquiciaAutomatico",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/franquicias/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "franquiciase.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    $($("#Tabs ul li a")[1]).click(function () {
        if ($("#rdbFormaPago_Debito")[0].checked == false && $("#rdbFormaPago_Tarjeta")[0].checked == false)
            $("#rdbFormaPago_Debito")[0].checked = true;
    });

    $("#rdbFormaPago_Debito").click(function () {
        $("#divFormaPago_Debito").show();
        $("#divFormaPago_Tarjeta").hide();
    });
    $("#rdbFormaPago_Tarjeta").click(function () {
        $("#divFormaPago_Debito").hide();
        $("#divFormaPago_Tarjeta").show();
    });

    if ($("#rdbFormaPago_Debito")[0].checked == true) {
        $("#divFormaPago_Debito").show();
        $("#divFormaPago_Tarjeta").hide();
    }
    else if ($("#rdbFormaPago_Tarjeta")[0].checked == true) {
        $("#divFormaPago_Debito").hide();
        $("#divFormaPago_Tarjeta").show();
    }

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
}

$(document).ready(function () {
    configControls();

    toggleTabs();

    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        provinciasByPaises();
    });


    if ($("#hdnID").val() != "0")
        $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());
    else
        $("#litTitulo").html("Alta de Franquicia");
});


function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPais").val() != "")
        idPais = parseInt($("#ddlPais").val());

    $.ajax({
        type: "POST",
        url: "Franquiciase.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                $("#ddlProvincia").val("1");
                if (parseInt($("#ddlProvincia").val()) > 0) {
                    idProv = parseInt($("#ddlProvincia").val());
                }
                LoadCiudades2(idProv, 'ddlCiudad');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}