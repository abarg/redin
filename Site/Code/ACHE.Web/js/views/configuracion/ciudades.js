﻿var oTable = null;
function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ idProvincia: '" + $("#cmbProvincia").val()
            + "', nombre: '" + $("#txtNombreCiudad").val()
            + "', idPais: '" + $("#cmbPaises").val()   
            + "'}";

    $.ajax({
        type: "POST",
        url: "Ciudades.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    
    $("#txtNombreCiudad").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#cmbProvincia").change(function (event) {
        filter();
        return false;
    })

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDCiudad: { type: "integer" },
                        idprov: { type: "integer" },
                        Provincia: { type: "string" },
                        Pais: { type: "string" },
                        CP: { type: "integer" },
                        Nombre: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Ciudades.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "IDZona", title: "ID", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "100px" },
            { field: "Pais", title: "Pais", width: "250px" },
            { field: "Provincia", title: "Provincia", width: "250px" },
            { field: "Nombre", title: "Nombre", width: "300px" },
             { field: "CP", title: "CP", width: "200px" },
            { field: "IDProvincia", title: "IDProvincia", width: "50px", hidden: "hidden" }

            //{ title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Ciudadese.aspx?IDCiudad=" + dataItem.IDCiudad;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Ciudades.aspx/Delete",
                data: "{ id: " + dataItem.IDCiudad + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function verTodos() {
    $("#cmbPaises").val("");
    $("#cmbProvincia").val("");
    $("#txtNombreCiudad").val("");
    filter();
}

function Nuevo() {
    window.location.href = "Ciudadese.aspx";
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombreCiudad").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }

    var provincia = parseInt($("#cmbProvincia").val());
    if (provincia > 0) {
        $filter.push({ field: "IDProvincia", operator: "equal", value: provincia });
    }

    var pais = parseInt($("#cmbPaises").val());
    if (pais > 0) {
        $filter.push({ field: "IDPais", operator: "equal", value: pais });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
    $("#cmbPaises").change(function () {

        $('#cmbProvincia').html('');

        var idPais = 0;
        if ($("#cmbPaises").val() != "")
            idPais = parseInt($("#cmbPaises").val());

        $.ajax({
            type: "POST",
            url: "Ciudades.aspx/provinciasByPaises",
            data: "{ idPais: " + idPais
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#cmbProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divError").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });
});