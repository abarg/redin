﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formHobby').validate();

    if ($('#formHobby').valid()) {
        var info = "{ IDHobbie: '" + $("#hdnIDHobby").val()
            + "', Nombre: '" + $("#txtNombre").val()

            + "'}";

        $.ajax({
            type: "POST",
            url: "HobbysEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Hobbys.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    $(".chosen").chosen();
});

