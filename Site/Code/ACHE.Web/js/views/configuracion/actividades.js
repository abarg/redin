﻿var oTable = null;


function Nuevo() {
    window.location.href = "Actividadese.aspx";
}

function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var niveles = "";
    var horarios = "";
    var sedes = "";

    if($(".nivelInput").length)
        $(".nivelInput").each(function () {
            niveles += ($(this).val()) + ",";
        })

    if ($(".horarioInput").length)
        $(".horarioInput").each(function () {
            horarios += ($(this).val()) + ",";
        })

    if ($(".sedeInput").length)
        $(".sedeInput").each(function () {
            sedes += ($(this).val()) + ",";
        })


    if ($('#formActividad').valid()) {
        var info = "{ idActividad: '" + parseInt($("#hdnIDActividad").val())
            + "', idSubCategoria: '" + parseInt($("#cmbSubCategorias").val())
            + "', Actividad: '" + $("#txtActividad").val()    
            + "', Niveles: '" + niveles
            + "', Horarios: '" + horarios
            + "', Sedes: '" + sedes

            + "'}";


        $.ajax({
            type: "POST",
            url: "Actividadese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                window.location.href = "Actividades.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function verTodos() {
    $("#txtCategoria").val("");
    filter();
}


//DATATABLES
function configControls() {

    $("#cmbCategorias").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });


    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        CantidadBeneficiarios: { type: "int" },
                        Actividad: { type: 'string' },
                        SubCategoria: { type: 'string' },
                        Categoria: { type: "string" },
                        Secretaria: { type: "string" }

                    }
                }
            },
            pageSize: 10,
            batch: true,
            transport: {
                read: {
                    url: "Actividades.aspx/GetListaGrilla", 
                    contentType: "application/json; charset=utf-8", 
                    type: "POST" 
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [

            { field: "Secretaria", title: "Secretaria", width: "250px" },
            { field: "Categoria", title: "Categoria", width: "250px" },
            { field: "SubCategoria", title: "Sub-Categoria", width: "250px" },
            { field: "Actividad", title: "Actividad", width: "250px" },
            { field: "CantidadBeneficiarios", title: "Inscriptos", width: "75px" },

            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "100px" },
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "ActividadesE.aspx?IDActividad=" + dataItem.IDActividad;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "ActividadesCategorias.aspx/Delete",
                data: "{ id: " + dataItem.IDCategoria + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}


function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ txtCategoria: '" + $("#txtCategoria").val()
        + "'}";


    $.ajax({
        type: "POST",
        url: "ActividadesCategorias.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}



function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var categoria = $("#txtCategoria").val();
    if (categoria != "") {
        $filter.push({ field: "Categoria", operator: "contains", value: categoria });
    }

    grid.dataSource.filter($filter);
}



$(document).ready(function () {
    configControls();

    $("#cmbCategorias").change(function cmbChange() {
        cargarSubCategorias();
    });
    if ($('#cmbProvincia').val() != "0") {
        cargarSubCategorias();
    }
    if ($("#hdnIDActividad").val() > 0) {
        var idsubcat = $("#hdnIDSubCat").val();
        $('#cmbSubCategorias').val(parseInt(idsubcat));
    }


    var max_fields = 10; 

    var wrapperNiveles = $("#wrapperNiveles"); 
    var addNivelButton = $("#addInputNivel"); 

    var wrapperHorarios = $("#wrapperHorarios");
    var addHorarioButton = $("#addInputHorario"); 

    var wrapperSedes = $("#wrapperSedes");
    var addSedeButton = $("#addInputSede"); 


    var n = 1; 
    $(addNivelButton).click(function (e) { 
        e.preventDefault();
        if (n < max_fields) { 
            n++; 
            $(wrapperNiveles).append('<div class="form-group">   <label class="col-lg-2 control-label"><span class="f_req">*</span>Nivel</label> <div class="col-lg-4">  <input type="text" name="nivel[]" class="form-control nivelInput"/><a href="#" class="remove_field">Eliminar</a></div> </div>');
        }
    });

    $(wrapperNiveles).on("click", ".remove_field", function (e) { 
        e.preventDefault(); $(this).closest('.form-group').remove(); n--;
    })

    var h = 1;
    $(addHorarioButton).click(function (e) {
        e.preventDefault();
        if (h < max_fields) {
            h++;
            $(wrapperHorarios).append('<div class="form-group">   <label class="col-lg-2 control-label"><span class="f_req">*</span>Horario</label> <div class="col-lg-4">  <input type="text" name="horario[]" class="form-control horarioInput"/><a href="#" class="remove_field">Eliminar</a></div> </div>');
        }
    });

    $(wrapperHorarios).on("click", ".remove_field", function (e) {
        e.preventDefault(); $(this).closest('.form-group').remove(); h--;
    })

    var s = 1;
    $(addSedeButton).click(function (e) {
        e.preventDefault();
        if (s < max_fields) {

            s++;
            $(wrapperSedes).append('<div class="form-group">   <label class="col-lg-2 control-label"><span class="f_req">*</span>Sede</label> <div class="col-lg-4">  <select type="text" name="sede[]" id="selectSede-' + s + '" class="form-control sedeInput"></select> <a href="#" class="remove_field">Eliminar</a></div> </div>');

            $.ajax({
                type: "POST",
                url: "ActividadesE.aspx/comerciosByFranquicia",
                data: "{ idFranquicia: " + 47
                    + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, text) {

                    if (data.d != "" && data.d != null) {
                        $.each(data.d, function () {
                            $("#selectSede-"+s+"").append($("<option/>").val(this.ID).text(this.Nombre));
                        });

                        $("#divError").hide();
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });

           
        }
    });

    $(wrapperSedes).on("click", ".remove_field", function (e) {
        e.preventDefault(); $(this).closest('.form-group').remove(); s--;
    })


});

function cargarSubCategorias() {
    if ($('#txtActividad').length) {
        $('#cmbSubCategorias').html('');
        var idCategoria = 0;
        if ($("#cmbCategorias").val() != "")
            idCategoria = parseInt($("#cmbCategorias").val());
        $.ajax({
            type: "POST",
            url: "ActividadesE.aspx/subCategoriasByCategorias",
            data: "{ idCategoria: " + idCategoria
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#cmbSubCategorias").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divError").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function addNivelInput() {

}