﻿/**LOGO**/
function deleteLogo(file, div) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "marcase.aspx/eliminarLogo",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
            $("#imgLogo").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    //alert("2-" + msg);
    //$("#hdnAttachment").val("");
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/

function mostrarMensaje(tipo) {
    switch (tipo) {
        case "B":
            if ($('#chkSMSBienvenida').is(':checked'))
                $("#divBienvenida").show();
            else
                $("#divBienvenida").hide();
            break;
        case "C":
            if ($('#chkSMSCumpleanios').is(':checked'))
                $("#divCumpleanios").show();
            else
                $("#divCumpleanios").hide();
            break;
        case "S":
            if ($('#chkSMS').is(':checked'))
                $(".sms").show();
            else
                $(".sms").hide();
            break;
    }
}

function mostrarMensajeEmail(tipo) {
    switch (tipo) {
        case "BS":
            if ($('#chkEmailRegistroASocio').is(':checked'))
                $("#divBienvenidaASocio").show();
            else
                $("#divBienvenidaASocio").hide();
            break;
        case "C":
            if ($('#chkEmailCumpleanios').is(':checked'))
                $("#divEmailCumpleanios").show();
            else
                $("#divEmailCumpleanios").hide();
            break;
        case "BC":
            if ($('#chkEmailAComercio').is(':checked'))
                $("#divBienvenidaComercio").show();
            else
                $("#divBienvenidaComercio").hide();
            break;

    }
}

function agregarUsuario() {
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtUsuario").val() == "" || $("#txtEmail").val() == "" || $("#txtPwd").val() == "") {
        $("#divErrorUsuario").html("Por favor,complete todos los datos");
        $("#divErrorUsuario").show();
    }
    else {
        var info = "{ IDMarca: " + parseInt($("#hdnID").val())
           + ", IDUsuario: " + parseInt($("#hfIDUsuario").val())
           + ", usuario: '" + $("#txtUsuario").val()
           + "', email: '" + $("#txtEmail").val()
           + "', pwd: '" + $("#txtPwd").val()
           + "', tipo: '" + $("#ddlTipoUsuario").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "marcase.aspx/procesarUsuario",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtUsuario").val("");
                $("#txtEmail").val("");
                $("#txtPwd").val("");
                $("#hfIDUsuario").val("0");
                $("#btnAgregarUsuario").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorUsuario").html(r.Message);
                $("#divErrorUsuario").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}
/*
function agregarComercio() {
    $("#divErrorComercio").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#ddlComercio").val() == "") {
        $("#divErrorComercio").html("Por favor, seleccione un comercio");
        $("#divErrorComercio").show();
    }
    else {
        var info = "{ IDMarca: " + parseInt($("#hdnID").val()) + ", IDComercio: " + parseInt($("#ddlComercio").val()) + "}";

        $.ajax({
            type: "POST",
            url: "marcase.aspx/agregarComercio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#ddlComercio").val("");
                filterComercios();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorComercio").html(r.Message);
                $("#divErrorComercio").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}*/

function grabar() {
    $("#divError").hide();
    $("#divErrorUsuario").hide();
    $("#divErrorComercio").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var idComercioSMS = 0;

        if ($("#cmbComercioSMS").val() != "" && $("#cmbComercioSMS").val() != null)
            idComercioSMS = parseInt($("#cmbComercioSMS").val());

        var htmlBienvenida = $("#txtEmailBienvenidaASocio").val();
        
        var fechasCompletadas = true;

        //if ($("#txtFechaCaducidad").val() != "" || $("#txtFechaTopeCanje").val() != "") {
        //    if ($("#txtFechaCaducidad").val() == "" || $("#txtFechaTopeCanje").val() == "")
        //        fechasCompletadas = false;
        //}

        if (fechasCompletadas) {
            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", nombre: '" + $("#txtNombre").val()
                + "', prefijo: '" + $("#txtPrefijo").val()
                + "', affinity: '" + $("#txtAffinity").val()
                + "', arancel: " + parseInt($("#txtArancel").val())
                + ", color: '" + $("#ddlColor").val()
                + "', mostrarSoloPOSPropios: " + $("#chkMostrarSoloPOSPropios").is(':checked')
                + ", mostrarSoloTarjetasPropias: " + $("#chkMostrarSoloTarjetasPropias").is(':checked')
                + ", posWeb: " + $("#chkPosWeb").is(':checked')
                + ", idFranquicia: '" + $("#ddlFranquicias").val()
                + "', tipoCatalogo: '" + $("#ddlCatalogo").val()
                + "', codigo: '" + $("#txtCodigo").val()
                + "', giftcardWeb: " + $("#chkGiftcardWeb").is(':checked')
                + ", cuponInWeb: " + $("#chkCuponIN").is(':checked')
                + ", productos: " + $("#chkProductos").is(':checked')
                + ", sms: " + $("#chkSMS").is(':checked')
                + ", costoSMS: '" + $("#txtCostoSMS").val()
                + "', smsBienvenida: " + $("#chkSMSBienvenida").is(':checked')
                + ", mensajeBienvenida: '" + $("#txtSMSBienvenida").val()
                + "', smsCumpleanios: " + $("#chkSMSCumpleanios").is(':checked')
                + ", mensajeCumpleanios: '" + $("#txtSMSCumpleanios").val()
                + "', idComercioSMS: " + idComercioSMS
                + ", envioEmailRegistroSocio: " + $("#chkEmailRegistroASocio").is(':checked')
                + ", emailBienvenidaSocio: '" + htmlBienvenida
                + "', envioEmailCumpleanios: " + $("#chkEmailCumpleanios").is(':checked')
                + ", emailCumpleanios: '" + $("#txtEmailCumpleanios").val()
                + "', envioEmailRegistroComercio: " + $("#chkEmailAComercio").is(':checked')
                + ", emailBienvenidoComercio: '" + $("#txtEmailAComercio").val()
                + "', emailAlertas: '" + $("#txtEmailAlerta").val()
                + "', celularAlertas: '" + $("#txtCelularAlerta").val()
                + "', celularEmpresaAlertas: '" + $("#txtCelularEmpresaAlerta").val()
                + "', razonSocial: '" + $("#txtRazonSocial").val()
                + "', condicionIVA: '" + $("#ddlIVA option:selected").val()
                + "', tipoDoc: '" + $("#ddlTipoDoc option:selected").html()
                + "', nroDoc: '" + $("#txtNroDoc").val()
                + "', provincia: '" + $("#ddlProvincia").val()
                + "', ciudad: '" + $("#ddlCiudad").val()
                + "', domicilio: '" + $("#txtDomicilio").val()
                + "', pisoDepto: '" + $("#txtPisoDepto").val()
                + "', codigoPostal: '" + $("#txtCodigoPostal").val()
                + "', telefono: '" + $("#txtTelefonoDom").val()
                + "', costoTransaccional: '" + $("#txtCostoTransaccional").val()
                + "', costoSeguro: '" + $("#txtCostoSeguro").val()
                + "', costoPlusin: '" + $("#txtCostoPlusin").val()
                + "', costoSMS2: '" + $("#txtCostoSMS2").val()
                + "', fechaCaducidad: '" + $("#txtFechaCaducidad").val()
                + "', fechaTopeCanje: '" + $("#txtFechaTopeCanje").val()
               + "', cuitEmisor: '" + $("#txtCuitEmisor").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "marcase.aspx/grabar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $("#hdnID").val(data.d);
                    $("#litTitulo").html("Edición de " + $("#txtNombre").val());

                    $("#divUploadLogo").show();
                    toggleTabs();
                    filter();
                    //filterComercios();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {            
            $("#divError").html('Debe completar tanto la "Fecha de Caducidad" como la de "Tope de Canje" para poder guardar los cambios');
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function filter() {
    $("#divErrorUsuario").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}
/*
function filterComercios() {
    $("#divErrorComercio").hide();
    var grid = $("#gridComercios").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}*/

function toggleTabs() {
    if ($("#hdnID").val() != "0") {
        $($("#Tabs ul li a")[1]).removeClass("hide");
        //$($("#Tabs ul li a")[2]).removeClass("hide");
    }
    else {
        $($("#Tabs ul li a")[1]).addClass("hide");
        //$($("#Tabs ul li a")[2]).addClass("hide");
    }
}

function configControls() {
    $("#txtArancel").numeric();
    $("#txtCostoSMS").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    mostrarMensajeEmail('BS');
    mostrarMensajeEmail('C');
    mostrarMensajeEmail('BC');

    configDatePicker();
    configFechasDesdeHasta2("txtFechaTopeCanje","txtFechaCaducidad");

    $('.htmlEditor').redactor({
        buttons: ['html', 'formatting', 'bold', 'italic', 'deleted',
            'unorderedlist', 'orderedlist', 'outdent', 'indent',
            'image', 'file', 'link', 'alignment', 'horizontalrule'],
        //lang: 'es',
        minHeight: 300 // pixels
    });

    if ($('#chkSMS').is(':checked')) {
        if ($('#chkSMSBienvenida').is(':checked'))
            $("#divBienvenida").show();
        else
            $("#divBienvenida").hide();

        if ($('#chkSMSCumpleanios').is(':checked'))
            $("#divCumpleanios").show();
        else
            $("#divCumpleanios").hide();
        $(".sms").show();
    }
    else {
        $(".sms").hide();
    }

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 150,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9 ñ\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, numeros, puntos, guiones medios y espacios en blanco.");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Usuario: { type: "string" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmail").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmail").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin"){
            $("#ddlTipoUsuario").val("A");
        }else{
            $("#ddlTipoUsuario").val("B");
            $("#btnAgregarUsuario").html("Actualizar");
        }
    });


    $("#grid").delegate(".loginColumn", "click", function (e) {
    var grid = $("#grid").data("kendoGrid");
    var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
    var usuario = dataItem.Usuario;
    var pwd = dataItem.Pwd;
    var info = "{ usuario: '" + usuario
        + "', pwd: '" +pwd
        + "'}";
 
    $.ajax({
        type: "POST",
        url: "/loginAutomatico.aspx/loginMarcaAutomatico",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            window.location.href = "/marcas/home.aspx";

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
});

    /*$("#gridComercios").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Nombre: { type: "string" },
                        Domicilio: { type: "string" },
                        NroDocumento: { type: "string" },
                        CasaMatriz: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaGrillaComercios", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "ID", title: "ID", width: "50px" },
            { field: "Nombre", title: "Comercio", width: "200px" },
            { field: "Domicilio", title: "Domicilio", width: "200px" },
            { field: "NroDocumento", title: "Cuit", width: "100px" },
            { field: "CasaMatriz", title: "Casa Matriz", width: "80px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridComercios").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/DeleteComercio",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterComercios();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorComercio").html(r.Message);
                    $("#divErrorComercio").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });*/

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    LoadComercios("../common.aspx/LoadComercios", "ddlComercio");
    //LoadComercios("../common.aspx/LoadComercios", "cmbComercioSMS");
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    /*$('#fileupload').change(function () {
        $("#upload").hide();
        var ext = this.value.substr((this.value.lastIndexOf('.') + 1)).toLowerCase();
        if (ext == "jpg" || ext == "png" || ext == "gif") {
            $("#fileError").hide();
        }
        else {
            this.value = '';
            $("#fileError").html("Extension inválida");
            $("#fileError").show();
        }
    });


    $('#fileupload').fileupload({
        dataType: 'json',
        replaceFileInput: false,
        url: "/fileUpload.ashx",
        //maxFileSize: 5000000,
        //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        formData: { type: 'logos' },
        add: function (e, data) {
            data.context = $('<p/>').text('Subiendo...').appendTo(document.body);
            data.submit();
        },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });*/
}

/*
function subirLogo()
{
    var logoUploadUrl = '/fileUpload.ashx?type=logos';
    $("#uploadForm").attr("action", logoUploadUrl);

    $("#uploadTrg").load(function () {
        //upload complete

        //only way to reset contents of file upload control is to recreate it
        $("#fileUploadWrapper").html('<input type="file" name="file" id="fileUpload">');

        //reload the logo url, add ticks on the end so browser reloads it
        var ticks = ((new Date().getTime() * 10000) + 621355968000000000);
        var logoUrl = '/files/logos/client.gif?' + ticks;
        $("#imgLogo").attr('src', logoUrl);
    });
}

function fileUpload(form, action_url, div_id) {
    if ($('#fileupload').val() != "") {

        $("#fileError").hide();

        // Create the iframe...
        var iframe = document.createElement("iframe");
        iframe.setAttribute("id", "upload_iframe");
        iframe.setAttribute("name", "upload_iframe");
        iframe.setAttribute("width", "0");
        iframe.setAttribute("height", "0");
        iframe.setAttribute("border", "0");
        iframe.setAttribute("style", "width: 0; height: 0; border: none;");

        // Add to document...
        document.forms[0].appendChild(iframe);
        window.frames['upload_iframe'].name = "upload_iframe";

        iframeId = document.getElementById("upload_iframe");

        // Add event...
        var eventHandler = function () {

            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);

            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
            alert(content);
            $("#imgLogo").attr("src", content);
            //$("#" + div_id).html(content);
            $("#" + div_id).hide();
            //document.getElementById(div_id).innerHTML = content;

            // Del the iframe...
            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        }

        if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
        if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

        // Set properties of form...
        document.forms[0].setAttribute("target", "upload_iframe");
        document.forms[0].setAttribute("action", action_url);
        document.forms[0].setAttribute("method", "post");
        document.forms[0].setAttribute("enctype", "multipart/form-data");
        document.forms[0].setAttribute("encoding", "multipart/form-data");

        // Submit the form...
        document.forms[0].submit();

        $("#" + div_id).show();
        $("#" + div_id).html("Subiendo...");
    }
    else {
        ("#" + div_id).hide();
        $("#fileError").html("Por favor, seleccione una imagen.");
        $("#fileError").show();
    }
}
*/

$(document).ready(function () {
    configControls();
    $("#txtCostoTransaccional, #txtCostoSeguro,#txtCostoSMS, #txtCostoPlusin,#txtCostoSMS2").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    toggleTabs();

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
        $("#divUploadLogo").show();
    }
    else
        $("#litTitulo").html("Alta de Marca");
});

function configFechasDesdeHasta2(date, end) {

    $.validator.addMethod("greaterThan", function () {
        var valid = true;
        var desde = $("#" + date).val();
        var hasta = $("#" + end).val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        return valid;
    }, 'La "fecha tope canje" debe ser menor o igual a la "fecha caducidad"');
    $('form').validate({
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
    //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

    $('#' + date).change(cleanErrors);
    $('#' + end).change(cleanErrors);
}