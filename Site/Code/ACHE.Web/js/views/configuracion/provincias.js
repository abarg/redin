﻿var oTable = null;
function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ idPais: '" + $("#cmbPaises").val()
            + "', provincia: '" + $("#txtNombreProvincia").val()

            + "'}";

    $.ajax({
        type: "POST",
        url: "Provincias.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {

    $("#txtNombreProvincia").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#cmbPaises").change(function (event) {
        filter();
        return false;
    })

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDCiudad: { type: "integer" },
                        idprov: { type: "integer" },
                        Provincia: { type: "string" },
                        Pais: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Provincias.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "IDZona", title: "ID", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "100px" },
            { field: "Pais", title: "Pais", width: "300px" },
            { field: "Provincia", title: "Provincia", width: "300px" },
            { field: "IDProvincia", title: "IDProvincia", width: "50px", hidden: "hidden" }

            //{ title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },

        ]
    });
    verTodos();

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Provinciase.aspx?IDProvincia=" + dataItem.IDProvincia;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Provincias.aspx/Delete",
                data: "{ id: " + dataItem.IDProvincia + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function verTodos() {
    $("#cmbPaises").val("");
    $("#txtNombreProvincia").val("");
    filter();
}

function Nuevo() {
    window.location.href = "Provinciase.aspx";
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombreProvincia").val();
    if (nombre != "") {
        $filter.push({ field: "Provincia", operator: "contains", value: nombre });
    }

    var pais = parseInt($("#cmbPaises").val());
    if (pais > 0) {
        $filter.push({ field: "IDPais", operator: "equal", value: pais });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});