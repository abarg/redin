﻿function configControls() {
    $("#txtDocumento").numeric();

    $("#txtNombre, #txtDocumento, #txtRazonSocial").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDFranquicia: { type: "integer" },
                        Nombre: { type: "string" },
                        RazonSocial: { type: "string" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "string" },
                        Arancel: { type: "number" },
                        ComisionTtCp: { type: "number" },
                        ComisionTpCp: { type: "number" },
                        ComisionTpCt: { type: "number" },
                        PubLocal: { type: "number" },
                        PubNacional: { type: "number" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "franquicias.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { field: "IDFranquicia", title: "ID", width: "50px" },
            { field: "Nombre", title: "Nombre Fantasía", width: "150px" },
            { field: "RazonSocial", title: "Razón social", width: "150px" },
            { field: "TipoDocumento", title: "Tipo Doc.", width: "75px" },
            { field: "NroDocumento", title: "Nro. Doc", width: "100px" },
            { field: "Arancel", title: "Comision %", width: "70px", attributes: { class: "colCenter" } },
            { field: "ComisionTtCp", title: "Tt Cp %", width: "70px", attributes: { class: "colCenter" } },
            { field: "ComisionTpCp", title: "Tp Cp %", width: "70px", attributes: { class: "colCenter" } },
            { field: "ComisionTpCt", title: "Tp Ct %", width: "70px", attributes: { class: "colCenter" } }
            //{ field: "PubLocal", title: "Local %", width: "70px", attributes: { class: "colCenter" } },
            //{ field: "PubNacional", title: "Nacional %", width: "70px", attributes: { class: "colCenter" } },
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "franquiciase.aspx?IDFranquicia=" + dataItem.IDFranquicia;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "franquicias.aspx/Delete",
                data: "{ id: " + dataItem.IDFranquicia + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var idFranq = dataItem.IDFranquicia;
        var info = "{ IDFranquicia: " + idFranq
            + "}";
        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginFranquiciaAutomaticoAdmin",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/franquicias/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });
}

function Nuevo() {
    window.location.href = "franquiciase.aspx";
}

function filter() {
    configGrid();
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var RazonSocial = $("#txtRazonSocial").val();
    if (RazonSocial != "") {
        $filter.push({ field: "RazonSocial", operator: "contains", value: RazonSocial });
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls()
});