﻿var oTable = null;


function Nuevo() {
    window.location.href = "ActividadesSecretariasE.aspx";
}

function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formSecretaria').validate();
    if ($('#formSecretaria').valid()) {
        var info = "{ idSecretaria: '" + $("#hdnIDSecretaria").val()
            + "', Nombre: '" + $("#txtSecretaria").val()    
            + "'}";


        $.ajax({
            type: "POST",
            url: "ActividadesSecretariasE.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                window.location.href = "ActividadesSecretarias.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function verTodos() {
    $("#txtSecretaria").val("");
    filter();
}


//DATATABLES
function configControls() {

    $("#txtSecretaria").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });


    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSecretaria: { type: "integer" },
                        Secretaria: { type: "string" },
                    }
                }
            },
            pageSize: 10,
            batch: true,
            transport: {
                read: {
                    url: "ActividadesSecretarias.aspx/GetListaGrilla", 
                    contentType: "application/json; charset=utf-8", 
                    type: "POST" 
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Secretaria", title: "Secretaria", width: "250px" },
            { field: "CantidadBeneficiarios", title: "Inscriptos", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "100px" },
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "ActividadesSecretariasE.aspx?IDSecretaria=" + dataItem.IDSecretaria;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "ActividadesCategorias.aspx/Delete",
                data: "{ id: " + dataItem.IDCategoria + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}


function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ Secretaria: '" + $("#txtSecretaria").val()
        + "'}";

    $.ajax({
        type: "POST",
        url: "ActividadesSecretarias.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}



function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var secretaria = $("#txtSecretaria").val().toUpperCase();
    if (secretaria != "") {
        $filter.push({ field: "Secretaria", operator: "contains", value: secretaria });
    }

    grid.dataSource.filter($filter);
}


$(document).ready(function () {
    configControls();
});