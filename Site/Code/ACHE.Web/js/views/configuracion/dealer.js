﻿$(document).ready(function () {
    configControls();
});


function Nuevo() {
    window.location.href = "Dealere.aspx";
}


function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "equal", value:nombre });
    }
    grid.dataSource.filter($filter);
}


function configControls() {
    $("#txtNombre").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDDealer: { type: "integer" },
                        Nombre: { type: "string" },
                        Apellido: { type: "string" },
                        Email: { type: "string" },
                        Codigo: { type: "string" },
                        Pwd: { type: "string" },
                        ComisionAlta: { type: "decimal" },
                        ComisionTarjetas: { type: "decimal" },
                        ComisionGiftCard: { type: "decimal" },
                        FechaAlta: { type: "date" },
                        Activo: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Dealer.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "IDZona", title: "ID", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            //{ field: "FechaAlta", title: "Fecha Alta", width: "50px", format: "{0:dd/MM/yyyy}" },
            { field: "Nombre", title: "Nombre", width: "80px" },
            { field: "Apellido", title: "Apellido", width: "80px" },
            //{ field: "Pwd", title: "Pwd", width: "50px" },
            { field: "Email", title: "Email", width: "120px" },
            { field: "Codigo", title: "Código", width: "50px" },
            { field: "ComisionAlta", title: "Com. Alta", width: "50px" },
            { field: "ComisionTarjetas", title: "Com. Tarjetas", width: "50px" },
            { field: "ComisionGiftCard", title: "Com. Gift", width: "50px" },
            { field: "Activo", title: "Activo", width: "50px" },
            { field: "IDDealer", title: "IDDealer", width: "50px", hidden: "hidden" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Dealere.aspx?IDDealer=" + dataItem.IDDealer;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Dealer.aspx/Delete",
                data: "{ id: " + dataItem.IDDealer + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}
