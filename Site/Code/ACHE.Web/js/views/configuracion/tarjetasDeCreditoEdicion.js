﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formTarjetaDeCredito').valid()) {
        var info = "{ IDTarjetasDeCredito: '" + $("#hdnIDTarjetaDeCredito").val()
            + "', Nombre: '" + $("#txtNombre").val()

            + "'}";

        $.ajax({
            type: "POST",
            url: "TarjetasDeCreditoEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                

                window.location.href = "TarjetasDeCredito.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    $(".chosen").chosen();
});

