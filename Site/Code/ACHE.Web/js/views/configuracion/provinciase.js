﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();
    if ($('#formCiudad').valid()) {
        var info = "{ idProvincia: '" + $("#hdnIDProvincia").val()
            + "', Nombre: '" + $("#txtNombreProvincia").val()
            + "', idPais: " + parseInt($("#cmbPaises").val())
            + "}";


        $.ajax({
            type: "POST",
            url: "Provinciase.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Provincias.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}


