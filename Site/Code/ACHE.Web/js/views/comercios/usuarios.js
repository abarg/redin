﻿function agregarUsuario() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($('#formEdicion').valid()) {

        if ($("#txtUsuario").val() == "" || $("#txtEmail").val() == "" || $("#txtPwd").val() == "") {
            $("#divError").html("Por favor,complete todos los datos");
            $("#divError").show();
        }
        else {
            var info = "{ IDUsuario: " + parseInt($("#hdnIDUsuario").val())
               + ", usuario: '" + $("#txtUsuario").val()
               + "', email: '" + $("#txtEmail").val()
               + "', pwd: '" + $("#txtPwd").val()
               + "', tipo: '" + $("#ddlTipoUsuario").val()
               + "'}";


            $.ajax({
                type: "POST",
                url: "usuarios.aspx/procesarUsuario",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtUsuario").val("");
                    $("#txtEmail").val("");
                    $("#txtPwd").val("");
                    $("#hdnIDUsuario").val("0");
                    $("#btnAgregarUsuario").html("Agregar");
                    filter();
                },
                error: function (response) {

                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
}

function filter() {
    configGrid();

    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}


function configControls() {

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "usuarios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmail").val("");
                        $("#txtPwd").val("");
                        $("#hdnIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hdnIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmail").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin")
            $("#ddlTipoUsuario").val("A");
        else
            $("#ddlTipoUsuario").val("B");
        $("#btnAgregarUsuario").html("Actualizar");
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "usuarios.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

}
$(document).ready(function () {
    configControls();
});