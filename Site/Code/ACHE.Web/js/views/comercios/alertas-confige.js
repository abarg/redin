﻿function toggleTipoAlerta()
{
    if ($("#ddlTipo").val() == "Por monto") {
        $("#txtCantTR").val("");

        $("#divCantTR").hide();
        //$("#divFecha").hide();

        $("#divMontoDesde").show();
        $("#divMontoHasta").show();
    }
    else if ($("#ddlTipo").val() == "Por cantidad") {
        $("#divCantTR").show();
        //$("#divFecha").show();

        $("#txtMontoDesde,#txtMontoHasta").val("");

        $("#divMontoDesde").hide();
        $("#divMontoHasta").hide();
    }
    else {//CPor monto y cantidad
        $("#divCantTR").show();
        //$("#divFecha").show();
        $("#divMontoDesde").show();
        $("#divMontoHasta").show();
    }
}


function validarFechaMenorActual(date) {
    var x = new Date();
    var fecha = date.split("/");
    x.setFullYear(fecha[2], fecha[1] - 1, fecha[0]);
    var today = new Date();

    if (x >= today)
        return false;
    else
        return true;
}
function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        if (
            ($("#ddlTipo").val() == "Por monto" || $("#ddlTipo").val() == "Por monto y cantidad")
            && ($("#txtMontoDesde").val() == "" || $("#txtMontoHasta").val() == "")) {
            $('#divOK').hide();
            $("#divError").html("Ingrese los montos");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else if (
            ($("#ddlTipo").val() == "Por cantidad" || $("#ddlTipo").val() == "Por monto y cantidad")
            && $("#txtCantTR").val() == "") {
            $('#divOK').hide();
            $("#divError").html("Ingrese la cantidad");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }   else {
            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", nombre: '" + $("#txtNombre").val()
                + "', prioridad: '" + $("#ddlPrioridad").val()
                + "', tipo: '" + $("#ddlTipo").val()
                + "', montoDesde: '" + $("#txtMontoDesde").val()
                + "', montoHasta: '" + $("#txtMontoHasta").val()
                + "', cantidad: '" + $("#txtCantTR").val()
                + "', fecha: " + parseInt($("#ddlFecha").val())
                + ", activa: " + $("#chkActiva").is(':checked')
                + ", restringida: " + $("#rdbDeterminadas").is(':checked')
                + "}";

            $.ajax({
                type: "POST",
                url: "alertas-confige.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}


$(document).ready(function () {
    configControls();
});


function configControls() {
    configDatePicker();
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    $("#txtMontoDesde, #txtMontoHasta, #txtCantTR").numeric();
    toggleTipoAlerta();
    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
    }
    else
        $("#litTitulo").html("Alta de Alerta");
}