﻿function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}


function configControls() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDEncuesta: { type: "integer" },
                        Detalle: { type: "string" },
                        Rango: { type: "string" },
                        avgRating: { type: "integer" },
                        totalAnswers: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "encuestas.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        console.log(data.models)
                        return JSON.stringify({ products: data.models })
                    } else {

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Ver Respuestas' class='viewColumn'/></div>" }, title: "Ver Respuestas", width: "50px" },
            { field: "IDEncuesta", title: "ID", width: "25px" },
            { field: "Detalle", title: "Detalle", width: "100px" },
            { field: "Rango", title: "Rango", width: "50px" },
            { field: "avgRating", title: "Puntaje Promedio", width: "50px" },
            { field: "totalAnswers", title: "Cantidad Respuestas", width: "50px", attributes: { class: "colCenter" } }
        ]
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
    });

}

$(document).ready(function () {
    configControls();
});


var encuestasEdicion = {

    Guardar: function () {

        $("#divError").hide();
        $("#divOk").hide();
        var form = $('#formEdicion');


        var detalle = $("#txtPregunta").val();
        var rangoMin = $("#txtRangoMin").val();
        var rangoMax = $("#txtRangoMax").val();

        $.validator.addMethod("oneormorechecked", function (value, element) {
            return (rangoMin < rangoMax || rangoMin == rangoMax);
        }, "Atleast 1 must be selected");

        form.validate();

        var body = { detalle: detalle, rangoMin: rangoMin, rangoMax: rangoMax };
        

        if ($('#formEdicion').valid()) {
           
            $.ajax({
                type: "POST",
                url: "encuestase.aspx/grabar",
                data: "" + JSON.stringify(body) + "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    window.location.href = "encuestas.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    },

}

