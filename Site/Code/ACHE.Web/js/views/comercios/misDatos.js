﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();

    $("#form_misDatos").validate();

    if ($("#form_misDatos").valid()) {
        //var info = "{ nombre: '" + $("#txtNombre").val()
        //    + "', apellido: '" + $("#txtApellido").val()
        var info = "{ email: '" + $("#txtEmail").val() + "'}";

        $.ajax({
            type: "POST",
            url: "mis-datos.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

$(document).ready(function () {

    $('#form_misDatos').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        /*rules: {
            txtNombre: { required: true },
            txtApellido: { required: true },
            txtEmail: { required: true, email:true },
            txtPassword: { required: true, minlength: 3 }
        },*/
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});
