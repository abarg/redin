﻿function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ estado: '" + $("#ddlEstado").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "', nroDoc: '" + $("#txtDocumento").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', producto: '" + $("#txtProducto").val()
            + "', socio: '" + $("#txtSocio").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "canjes.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    $("#txtTarjeta, #txtDocumento").numeric();

    configDatePicker();
    //configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtTarjeta, #txtDocumento, #txtFechaDesde, #txtFechaHasta,#ddlEstado, #txtProducto, #txtSocio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDCanje: { type: "integer" },
                        Estado: { type: "string" },
                        IDEstado: { type: "integer" },
                        Producto: { type: "string" },
                        NroDocumento: { type: "string" },
                        Socio: { type: "string" },
                        Tarjeta: { type: "integer" },
                        Cantidad: { type: "integer" },
                        Fecha: { type: "date" },
                        Familia: { type: "string" }
                    }
                }
            },
            pageSize: 250,
            batch: true,
            transport: {
                read: {
                    url: "canjes.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
                        { title: "Ver", template: "#= renderOptions(data) #", width: "50px" },

         //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDCanje", title: "ID", width: "50px", hidden: true },
            { field: "Estado", title: "Estado", width: "100px" },
            { field: "Fecha", title: "Fecha", format: "{0:dd/MM/yyyy}", width: "80px" },
            //{ field: "Premio", title: "Premio", width: "170px" },
            { field: "Producto", title: "Producto", width: "170px" },
            { field: "Cantidad", title: "Cant", width: "80px" },
            { field: "Familia", title: "Familia", width: "80px" },
            { field: "Socio", title: "Socio", width: "170px" },
            { field: "Tarjeta", title: "Tarjeta", width: "120px" },
            { field: "NroDocumento", title: "NroDocumento", width: "100px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        
        window.location.href = "canjese.aspx?ID=" + dataItem.IDCanje;
    });


    $("#grid").delegate(".ViewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "remito-canje.aspx?IdCanje=" + dataItem.IDCanje;
    });
    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "canjes.aspx/Delete",
                data: "{ id: " + dataItem.IDCanje + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function renderOptions(data) {
    var html = "";
    if (data.Estado == "Entregado")
        html = "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='ViewColumn'/></div>";


    return html;
}

function Nuevo() {
    window.location.href = "canjese.aspx";
}

function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var tarjeta = $("#txtTarjeta").val();
    if (tarjeta != "") {
        $filter.push({ field: "Tarjeta", operator: "contains", value: tarjeta });
    }

    var nroDoc = $("#txtDocumento").val();
    if (nroDoc != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: nroDoc });
    }

    var estado = $("#ddlEstado").val();
    if (estado != "") {
        $filter.push({ field: "IDEstado", operator: "equal", value: parseInt(estado) });
    }

    var idFamilia = $("#cmbFamilia").val();
    if (idFamilia != "" && idFamilia != null) {
        $filter.push({ field: "IDFamilia", operator: "equal", value: parseInt(idFamilia) });
    }

    var producto = $("#txtProducto").val();
    if (producto != "" && producto != null) {
        $filter.push({ field: "Producto", operator: "contains", value: producto });
    }

    var socio = $("#txtSocio").val();
    if (socio != "" && socio != null) {
        $filter.push({ field: "Socio", operator: "contains", value: socio });
    }

    grid.dataSource.filter($filter);
}

function exportarCanjes() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);


    


    var $filter = "{ estado: '" + $("#ddlEstado").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "', nroDoc: '" + $("#txtDocumento").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', idFamilia: '" + $("#cmbFamilia").val()
            + "', producto: '" + $("#txtProducto").val()
            + "', socio: '" + $("#txtSocio").val()
            + "'}";


    $.ajax({
        type: "POST",
        url: "canjes.aspx/exportarCanjes",
        data: $filter,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });

}

function abrirpopupImpresion() {

    window.open("imprimir-etiquetas.aspx?estado=" + $("#ddlEstado").val()
                                    + "&tarjeta=" + $("#txtTarjeta").val()
                                    + "&documento=" + $("#txtDocumento").val()
                                    + "&fchdsd=" + $("#txtFechaDesde").val()
                                    + "&fchhst=" + $("#txtFechaHasta").val()
                                    + "&familia=" + $("#cmbFamilia").val()
                                    + "&producto=" + $("#txtProducto").val()
                                    + "&socio=" + $("#txtSocio").val()
                                    , "GenerarTablaCanjes", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no,fullscreen=yes");    
}

$(document).ready(function () {
    configControls()
});