﻿var oTable = null;

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ apellido: '" + $("#txtApellido").val()
            + "', documento: '" + $("#txtDocumento").val()
            + "', email: '" + $("#txtEmail").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "socios.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function configControls() {
    $("#txtDocumento").numeric();
    $("#txtTarjeta").numeric();
    
    $("#txtApellido, #txtDocumento, #txtTarjeta, #txtEmail").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSocio: { type: "integer" },
                        Tarjeta: { type: "string" },
                        Nombre: { type: "string" },//se usa para la busqueda de tarjetas
                        Apellido: { type: "string" },
                        Email: { type: "string" },
                        Sexo: { type: "string" },
                        FechaNacimiento: { type: "date" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "string" },
                        Telefono: { type: "string" },
                        Celular: { type: "string" },
                        EmpresaCelular: { type: "string" },
                        Observaciones: { type: "string" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "socios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { field: "IDSocio", title: "ID", width: "50px", hidden: true },
            { field: "Tarjeta", title: "Nro. de Tarjeta", width: "130px" },
            { field: "Apellido", title: "Socio", width: "200px" },
            //{ field: "Nombre", title: "Nombre", width: "125px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "NroDocumento", title: "Documento", width: "80px" },
            { field: "FechaNacimiento", title: "Fecha Nac.", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "Sexo", title: "Sexo", width: "50px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "Credito", title: "Crédito $", width: "80px", format: "{0:c}" },
            { field: "Giftcard", title: "Giftcard $", width: "80px", format: "{0:c}" },
            { field: "Total", title: "Total $", width: "80px", format: "{0:c}" },
            { field: "POS", title: "POS", width: "80px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "sociosedicion.aspx?IDSocio=" + dataItem.IDSocio;
    });


    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        //$('#tableDetalleTr').fixedHeaderTable('destroy');
        $("#titDetalleTr").html("Detalle de Transacciones " + dataItem.Apellido + ", " + dataItem.Nombre);


        $.ajax({
            type: "POST",
            url: "transacciones.aspx/GetBySocio",
            data: "{ id: " + dataItem.IDSocio + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#bodyDetalleTr").html("");

                if (data != null && data.d.length > 0) {

                    if (oTable != null) {

                        oTable.fnClearTable();
                        oTable.fnDestroy();
                    }

                    oTable = $('#tableDetalleTr').dataTable({
                        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                        "paging": true,
                        "bLengthChange": false,
                        "iDisplayLength": 10,
                        "ordering": false,
                        "bSort": false,
                        "info": false,
                        //"bDestroy": true,
                        "searching": false,
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                        "fnDrawCallback": function () {
                            var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                            if (pageCount == 1) {
                                $('.dataTables_paginate').first().hide();
                            } else {
                                $('.dataTables_paginate').first().show();
                            }
                        }
                    });

                    for (var i = 0; i < data.d.length; i++) {
                        oTable.fnAddData([
                            data.d[i].Fecha,
                            data.d[i].Hora,
                            data.d[i].Operacion,
                            //data.d[i].SDS,
                            data.d[i].Comercio,
                            data.d[i].NroEstablecimiento,
                            data.d[i].ImporteOriginal,
                            data.d[i].ImporteAhorro,
                            data.d[i].Puntos]
                        );
                    }

                    $("#tableDetalleTr_info").parent().remove();
                    $("#tableDetalleTr").css("width", "100%");
                    $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                    $(".dataTables_paginate").first().parent().addClass("col-sm-12");
                }
                else
                    $("#bodyDetalleTr").html("<tr><td colspan='9'>No hay un detalle disponible</td></tr>");

                $('#modalDetalleTr').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });

}

function renderOptions(data) {
    var html = "";
    html = "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/></div>";
    //html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/>";
    return html;
}

/*
function Nuevo() {
    window.location.href = "sociosedicion.aspx";
}*/

function filter() {
    configGrid();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var apellido = $("#txtApellido").val();
    if (apellido != "") {
        $filter.push({ field: "Apellido", operator: "contains", value: apellido });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var email = $("#txtEmail").val();
    if (email != "") {
        $filter.push({ field: "Email", operator: "contains", value: email });
    }

    var tarjeta = $("#txtTarjeta").val();
    if (tarjeta != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: tarjeta });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    $('#formSocio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});