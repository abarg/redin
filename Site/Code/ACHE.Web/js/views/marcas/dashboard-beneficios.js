﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);
var m5 = new Date().addMonths(-4);
var m6 = new Date().addMonths(-5);

var m12 = new Date().addMonths(-12);

$(document).ready(function () {


    setTimeout("ObtenerPuntosOtorgados()", 1000);
    setTimeout("ObtenerPuntosCanjeados()", 1000);
    setTimeout("gebo_charts.fl_puntos_otorgados()", 2000);

});

gebo_charts = {
    fl_puntos_otorgados: function () {
        var elem = $('#fl_puntos_otorgados');

        //var data = ObtenerPuntosOtorgados();

        var otorgados = ObtenerPuntosOtorgados();
        var canjeados = ObtenerPuntosCanjeados();
        var colors = ["#52398B", "#70A415"];

        var data = [
            { label: "Otorgado", data: otorgados },
            { label: "Canjeado", data: canjeados }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 7,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m6.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m5.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [5, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [6, MONTH_NAMES_SHORT[m1.getMonth()]]
                 
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                 lines: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                },
                points: {
                    show: true
                }
            },
            colors: colors
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>Cantidad</b> = " + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    }
};

function ObtenerPuntosOtorgados() {
    var ddata = [];
 
    $.ajax({
        type: "POST",
        url: "dashboard-beneficios.aspx/obtenerPuntosOtorgados",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
    
            }
         
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerPuntosCanjeados() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-beneficios.aspx/obtenerPuntosCanjeados",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}