﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);
var m5 = new Date().addMonths(-4);
var m6 = new Date().addMonths(-5);

var m12 = new Date().addMonths(-12);

$(document).ready(function () {

    setTimeout("ObtenerTotalEmails()", 1000);
    setTimeout("ObtenerTotalSMSEnviados()", 1000);
    setTimeout("ObtenerTotalSMSNoEnviados()", 1000);
    

});

function ObtenerTotalEmails() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-marketing.aspx/obtenerTotalEmails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_mails").html(data.d);
            }
        }
    });
}

function ObtenerTotalSMSEnviados() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-marketing.aspx/obtenerTotalSMSEnviados",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#totalSMSEnviado").html(data.d);
            }
        }
    });
}
function ObtenerTotalSMSNoEnviados() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-marketing.aspx/obtenerTotalSMSNoEnviados",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#totalSMSNoEnviado").html(data.d);
            }
        }
    });
}