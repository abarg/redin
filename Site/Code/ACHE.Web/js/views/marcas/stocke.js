﻿

$(document).ready(function () {

    $("#cmbSubFamilia,#cmbFamilia").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            GenerarTablaProductos();
            return false;
        }
    });

    //$(".chosen").chosen();
    //$("#txtStockActual").numeric();
    //$("#txtStockMinimo").numeric();

    //GenerarTablaProductos();

    $("#cmbFamilia").change(function (event) {
        var id = "";
        if ($("#cmbFamilia").val() != null) {
            var id = $("#cmbFamilia").val();
        }
        cargarSubFamilias(id);
    })
});


function limpiarPantalla() {
    $("#divOk").hide();
    $("#divError").hide();
    $("#divErrorEmptySearch").hide();
}

function cargarSubFamilias(id) {
    $("#cmbSubFamilia").html("");

    var info = "{ idFamiliaPadre:'" + id
        + "'}";

    $.ajax({
        type: "POST",
        url: "stocke.aspx/cargarSubFamilias",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $(data.d).each(function () {
                $("#cmbSubFamilia").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            //$("#cmbSubFamilia").trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function GenerarTablaProductos() {

    var idFamilia = 0;
    if ($("#cmbFamilia").val() != "" && $("#cmbFamilia").val() != null)
        idFamilia = parseInt($("#cmbFamilia").val());
    var idSubFamilia = 0;
    if ($("#cmbSubFamilia").val() != "" && $("#cmbSubFamilia").val() != null)
        idSubFamilia = parseInt($("#cmbSubFamilia").val());
    var idMarca = 0;
    if ($("#hdnIDMarca").val() != null && $("#hdnIDMarca").val() != "0")
        idMarca = parseInt($("#hdnIDMarca").val());


    var info = "{ idFamilia: " + idFamilia
        + ", idSubFamilia: " + idSubFamilia
        + ", idMarca: " + idMarca
        + "}";

    $.ajax({
        type: "POST",
        url: "stocke.aspx/generarTablaProductos",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyCostos").html(data.d);
            }
        }
    });
}


function ActualizarStock() {
    var n = 1;
    $(".selectTarjetas").each(function () {
        if ($("#txtAgregarStock" + n).val() != "" && $("#txtAgregarStock" + n).val() != null) {
            var idProducto = 0;
            if ($("#id" + n).val() != "" && $("#id" + n).val() != null)
                idProducto = parseInt($("#id" + n).val());

            var idFamilia = 0;
            if ($("#cmbFamilia").val() != "" && $("#cmbFamilia").val() != null)
                idFamilia = parseInt($("#cmbFamilia").val());
            var idSubFamilia = 0;
            if ($("#cmbSubFamilia").val() != "" && $("#cmbSubFamilia").val() != null)
                idSubFamilia = parseInt($("#cmbSubFamilia").val());

            var info = "{ idFamilia: " + idFamilia
                + ", idSubFamilia: " + idSubFamilia
                + ", agregarStock: '" + $("#txtAgregarStock" + n).val()
                + "',  stockMinimo: '" + $("#txtStockMinimo" + n).val()
                + "',  idProducto: " + idProducto
                + "}";

            if (idProducto != 0)
                actualizar(info);
        }
        n++;

    });
    //window.location.href = "stocke.aspx";
    $("#divOk").show();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    GenerarTablaProductos();
}

function actualizar(info) {
    $.ajax({
        type: "POST",
        url: "stocke.aspx/actualizarStock",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}
