﻿function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ mensaje: '" + $("#txtEmail").val() + "' }";

        $.ajax({
            type: "POST",
            url: "email-cumpleanios.aspx/GuardarMensaje",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

$(document).ready(function () {
    $('.htmlEditor').redactor({
        buttons: ['html', 'formatting', 'bold', 'italic', 'deleted',
            'unorderedlist', 'orderedlist', 'outdent', 'indent',
            'image', 'file', 'link', 'alignment', 'horizontalrule'],
        //lang: 'es',
        minHeight: 300 // pixels
    });
});