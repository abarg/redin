﻿function changeCheck() {
    if ($("#chk0").is(':checked')) {
        $("#divTodos").hide();
    }
    else {
        $("#divTodos").show();
    }
}

function configControls() {
    configDatePicker();

    if ($("#ddlTransacciones").val() != "") {
        $("#divFechaTrans").show()
    }

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 145,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9 ñ\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, números, puntos, guiones medios y espacios en blanco.");

    //jQuery.validator.addMethod("telefonos", function (value, element) {
    //    return this.optional(element) || /^[0-9;]+$/i.test(value);
    //}, "El mensaje sólo puede contener números y punto y coma como separador.");

    jQuery.validator.addMethod("telefonos", function (value, element) {
        return this.optional(element) || /^[0-9\-\ a-z ;]+$/i.test(value);
    }, "El mensaje sólo puede contener letras, números, guiones medios y punto y coma como separador.");

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de Campaña SMS " + $("#txtNombre").val());
    }
    else
        $("#litTitulo").html("Alta de Campaña SMS");
}

function generarActivacion() {
    var info = "{ id: " + parseInt($("#hdnID").val()) + "}";

    $.ajax({
        type: "POST",
        url: "smse.aspx/activar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $('#divOK').show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            smoke.alert("Su campaña se encuentra pendiente de activación. ", { ok: "<a href='sms.aspx' style='color:#fff'>cerrar</a>" });
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function realizarActivacion() {
    var msg = "La campaña fue creada correctamente. <br />¿Desea solicitar su activación para enviar los SMS?";
    smoke.confirm(msg, function (e) {
        if (e) {
            $("#divError").hide();
            $("#divOk").hide();
            generarActivacion();
            //smoke.alert('"yeah it is" pressed', { ok: "close" });
        } else {
            window.location.href = "sms.aspx";
            //smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}

function calcular() {
    var sexo = "";
    var edades = "";
    var precioSMS = $("#hdnPrecio").val();
    var idProfesion = "";
    var idProvincia = "";
    var idCiudad = "";
    var fechaDesdeTrans = "";
    var tipoTrans = "";

    if ($("#rdbTodos").is(':checked'))
        sexo = "T";
    else if ($("#rdbFemenino").is(':checked'))
        sexo = "F";
    else if ($("#rdbMasculino").is(':checked'))
        sexo = "M";

    if ($("#chk1").is(':checked'))
        edades += "0-18;";
    if ($("#chk2").is(':checked'))
        edades += "19-25;";
    if ($("#chk3").is(':checked'))
        edades += "26-30;";
    if ($("#chk4").is(':checked'))
        edades += "31-40;";
    if ($("#chk5").is(':checked'))
        edades += "40-50;";
    if ($("#chk6").is(':checked'))
        edades += "51-60;";
    if ($("#chk7").is(':checked'))
        edades += "61-200;";
    if ($("#chk0").is(':checked'))
        edades = "Todas";

    if ($("#chk1").is(':checked') && $("#chk2").is(':checked') && $("#chk3").is(':checked') && $("#chk4").is(':checked') && $("#chk5").is(':checked') && $("#chk6").is(':checked') && $("#chk7").is(':checked'))
        edades = "Todas";

    if ($("#ddlProvincias").val() != "")
        idProvincia = $("#ddlProvincias").val();

    if ($("#ddlCiudades").val() != "" && $("#ddlCiudades").val() != null)
        idCiudad = $("#ddlCiudades").val();

    if ($("#ddlTransacciones").val() != "")
        tipoTrans = $("#ddlTransacciones").val();

    if ($("#fechaDesde").val() != "")
        fechaDesdeTrans = $("#txtFechaDesde").val();

    if ($("#ddlProfesiones").val() != "")
        idProfesion = $("#ddlProfesiones").val();

    if (edades != "") {
        $("#lblError").hide();

        var info = "{ edad: '" + edades
            + "', sexo: '" + sexo
            + "', precioSMS: '" + precioSMS
            + "', idProvincia: '" + idProvincia
            + "', idCiudad: '" + idCiudad
            + "', tipoTrans: '" + tipoTrans
            + "', fechaDesdeTrans: '" + fechaDesdeTrans
            + "', idProfesion: '" + idProfesion
            + "', localidad: '" + $("#localidades").val()
            + "', sede: '" + $("#ddlSedes").val()
            + "', actividad: '" + $("#ddlActividades").val()
            + "' }";

        $.ajax({
            type: "POST",
            url: "smse.aspx/Calcular",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#spnCosto").text("$ " + data.d[0]);
                    $("#spnCantidad").text(data.d[1]);

                    $(".calculo").show();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $("#lblError").show();
        $("#lblError").html("Debe seleccionar un rango de edades, o todas");
    }
}

function calcularExt() {
    var telefonos = $("#txtTelefonos").val();
    var precioSMS = $("#hdnPrecio").val();
    var isValid = true;
    var msg = "";
    if (precioSMS == "") {
        isValid = false;
        msg = "Hubo un error obteniendo el costo, por favor intente nuevamente.";
    }
    var total = 0;
    if (telefonos != "") {
        var lastChar = telefonos[telefonos.length - 1];
        if (lastChar == ";")
            telefonos = telefonos.substring(0, telefonos.length - 1);
        var array = telefonos.split(';');
        var cant = array.length;
        if (cant > 0) {
            for (var i = 0; i < cant; i++) {
                if (array[i].length < 10) {
                    isValid = false;
                    msg = "Todos los teléfonos deben ser mayores a 10 caracteres";
                }
            }
        }
        else {
            isValid = false;
            msg = "Debe ingresar al menos un teléfono para calcular el costo.";
        }
    }
    else {
        isValid = false;
        msg = "Debe completar el campo teléfonos para calcular el costo.";
    }

    if (isValid) {
        total = parseFloat(precioSMS) * cant;
        $("#spnCosto").text("$ " + total);
        $("#spnCantidad").text(cant);
        $(".calculo").show();
    }
    else {
        $("#lblError").show();
        $("#lblError").html(msg);
    }
}


function verDetalle(tipo) {

    $("#hdnTipo").val(tipo);

    $("#btnExportar").show();
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Socios");

    $("#headDetalle").html("<tr><th>Socio</th></tr>");

    $(".loader").css("display", "block");



    var info = "{ id: " + parseInt($("#hdnID").val())
        + ", localidad: '" + $("#localidades").val()
        + "', sede: '" + $("#ddlSedes").val()
        + "', actividad: '" + $("#ddlActividades").val()
        + "', tipo: '" + 'I'
        + "' }";


    $.ajax({
        type: "POST",
        url: "smse.aspx/obtenerDetalle",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyDetalle").html("");

                if (typeof oTable !== 'undefined') {
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalle').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });


                for (var i = 0; i < data.d.length; i++) {

    

                    if (data.d[i].Nombre != null) {
                               
                            oTable.fnAddData([


                                data.d[i].Nombre + ' ' + data.d[i].Apellido
                            


                          
                                ]
                                );
                    }

                 }

                $(".loader").css("display", "none");
                

                $("#tableDetalle_info").parent().remove();
                $("#tableDetalle").css("width", "100%");


                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyDetalle").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }

            $('#modalDetalle').modal('show');
        }
    });
}



function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var sexo = "";
        var edades = "";
        var precioSMS = $("#hdnPrecio").val();
        var tipo = $("#hdnTipo").val();
        var idProfesion = 0;
        var idProvincia = 0;
        var idCiudad = 0;
        var fechaDesdeTrans = "";
        var tipoTrans = "";

        if (tipo == "I") {
            if ($("#rdbTodos").is(':checked'))
                sexo = "T";
            else if ($("#rdbFemenino").is(':checked'))
                sexo = "F";
            else if ($("#rdbMasculino").is(':checked'))
                sexo = "M";

            if ($("#chk1").is(':checked'))
                edades += "0-18;";
            if ($("#chk2").is(':checked'))
                edades += "19-25;";
            if ($("#chk3").is(':checked'))
                edades += "26-30;";
            if ($("#chk4").is(':checked'))
                edades += "31-40;";
            if ($("#chk5").is(':checked'))
                edades += "40-50;";
            if ($("#chk6").is(':checked'))
                edades += "51-60;";
            if ($("#chk7").is(':checked'))
                edades += "61-200;";
            if ($("#chk0").is(':checked'))
                edades = "Todas";

            if ($("#chk1").is(':checked') && $("#chk2").is(':checked') && $("#chk3").is(':checked') && $("#chk4").is(':checked') && $("#chk5").is(':checked') && $("#chk6").is(':checked') && $("#chk7").is(':checked'))
                edades = "Todas";

            if ($("#ddlProvincias").val() != "")
                idProvincia = $("#ddlProvincias").val();

            if ($("#ddlCiudades").val() != "" && $("#ddlCiudades").val() != null)
                idCiudad = $("#ddlCiudades").val();

            if ($("#ddlTransacciones").val() != "")
                tipoTrans = $("#ddlTransacciones").val();

            if ($("#fechaDesde").val() != "")
                fechaDesdeTrans = $("#txtFechaDesde").val();

            if ($("#ddlProfesiones").val() != "")
                idProfesion = $("#ddlProfesiones").val();
        }

        var telefonos = $("#txtTelefonos").val();
        var lastChar = telefonos[telefonos.length - 1];
        if (lastChar == ";")
            telefonos = telefonos.substring(0, telefonos.length - 1);
        var valid = true;
        if (telefonos != "") {
            var array = telefonos.split(';');
            var cant = array.length;
            for (var i = 0; i < cant; i++) {
                if (array[i].length < 10)
                    valid = false;
            }
        }

        if (edades == "" && tipo == "I") {
            $("#lblError").show();
            $("#lblError").html("Debe seleccionar un rango de edades, o todas");
        }
        else if (!valid && tipo == "E") {
            $("#lblError").show();
            $("#lblError").html("Todos los teléfonos deben ser mayores a 10 caracteres");
        }
        else {
            $("#lblError").hide();

            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", tipo: '" + $("#hdnTipo").val()
                + "', nombre: '" + $("#txtNombre").val()
                + "', sexo: '" + sexo
                + "', edad: '" + edades
                + "', mensaje: '" + $("#txtMensaje").val()
                + "', fechaEnvio: '" + $("#txtFechaEnvio").val()
                + "', telefonos: '" + telefonos
                + "', idProvincia: '" + parseInt(idProvincia)
                + "', idCiudad: '" + parseInt(idCiudad)
                + "', tipoTrans: '" + tipoTrans
                + "', fechaDesdeTrans: '" + fechaDesdeTrans
                + "', idProfesion: '" + idProfesion
                + "' }";

            $.ajax({
                type: "POST",
                url: "smse.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $("#hdnID").val(data.d);

                    realizarActivacion();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

$(document).ready(function () {
    $(".calculo").hide();

    var tipo = $("#hdnTipo").val();
    if (tipo == "I") {
        $(".externa").hide();
        $(".interna").show();
    }
    else if (tipo == "E") {
        $(".interna").hide();
        $(".externa").show();
    }

    configControls();
    changeCheck();

    sedesChangeController();

    localidadesChangeController();

    $('#sendNowChk').click(function () {
        $('#txtFechaEnvio').attr('disabled', this.checked)
    });


    $("#ddlProvincias").change(function () {

        $('#ddlCiudades').html('');
        $('#ddlCiudades').text('');

        var idProvincia = 0;
        if ($("#ddlProvincias").val() != "")
            idProvincia = parseInt($("#ddlProvincias").val());

        $.ajax({
            type: "POST",
            url: "smse.aspx/ciudadesByProvincias",
            data: "{ idProvincia: " + idProvincia
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#ddlCiudades").append($("<option/>").val(this.ID).text(this.Nombre));
                    });
                    $("#divError").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#spnCantidad").hide();
            }
        });
    });


    $("#ddlTransacciones").change(function () {

        if ($("#ddlTransacciones").val() != "") {
            $("#divFechaTrans").show();
        }
        else {
            $("#divFechaTrans").hide();
            $("#txtFechaDesde").val("");
        }

    });
});



function horariosChangeController() {

    $("#ddlActividades").change(function () {

        var sede = '';
        if ($("#ddlActividades").val() != null)
            sede = $("#ddlActividades").val();


        var info = "{ sede: '" + sede
            + "', Actividades: '" + "none"
            + "'}";

        $.ajax({
            type: "POST",
            url: "smse.aspx/actividadesPorSedes",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlHorarios').empty().trigger("liszt:updated"); //remove all child nodes


                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#ddlHorarios").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);
                }


                $('#ddlHorarios').trigger("liszt:updated");

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}


function sedesChangeController() {

    $("#ddlSedes").change(function () {

        var sede = '';
        if ($("#ddlSedes").val() != null)
            sede = $("#ddlSedes").val();


        var info = "{ sede: '" + sede
            + "', Actividades: '" + "none"
            + "'}";

        $.ajax({
            type: "POST",
            url: "smse.aspx/actividadesPorSedes",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlActividades').empty().trigger("liszt:updated"); //remove all child nodes


                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#ddlActividades").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);
                }


                $('#ddlActividades').trigger("liszt:updated"); 

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}


function localidadesChangeController() {

    $("#localidades").change(function () {


        localidad = $("#localidades").val();

        var info = "{ localidad: '" + localidad
            + "', Actividades: '" + "none"
            + "'}";


        $.ajax({
            type: "POST",
            url: "smse.aspx/sedesPorLocalidades",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlSedes').empty().trigger("liszt:updated"); //remove all child nodes


                if (Object.keys(data.d).length > 0) {
                    $.each(data.d, function (k, v) {
                        $("#ddlSedes").append($("<option/>").val(v.ID).text(v.Nombre));
                        console.log('wtg')
                    });


                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);

                    $("#ddlSedes").trigger("liszt:updated");

                }
                else {
                    $("#ddlSedes").empty().trigger('liszt:updated');
                    console.log("nothing")
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}