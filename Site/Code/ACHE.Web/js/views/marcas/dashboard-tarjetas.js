﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);



$(document).ready(function () {
    setTimeout("ObtenerTotalSociosSexo()", 1000);
    setTimeout("gebo_charts.fl_graphSexo()", 2000);

    setTimeout("gebo_charts.fl_tarjetas_activas()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_asignadas()", 2000);
    setTimeout("gebo_charts.fl_estado_terminales()", 2000);
});

//* charts
gebo_charts = {

    fl_tarjetas_activas: function () {

        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_activas');

        var activas = ObtenerTarjetasActivas();
        var inactivas = ObtenerTarjetasInactivas();
        var totalTarjetas = activas + inactivas;


       
        var data = [
            { label: activas, data: activas },
            { label: inactivas, data: inactivas}
        ];

        var colors = ["#F5AA1A", "#B6A3A3"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

                // Setup a visually pleasing rounding function
                round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_asignadas: function () {
        var elem = $('#fl_tarjetas_asignadas');

        var data = ObtenerTarjetasAsignadas();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                lines: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_estado_terminales: function () {
        // Setup the placeholder reference
        var elem = $('#fl_estado_terminales');

        var data = ObtenerEstadoTerminales();
        var colors = ["#000", "#F5AA1A", "#70A415", "#f00", "#f8f412", "#0066FF", "#7F7C7C", "#a47e3c"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_graphSexo: function () {
        // Setup the placeholder reference
        var elem = $('#fl_graphSexo');

        var data = ObtenerTotalSociosSexo();
        var colors = ["#52398B", "#70A415", "#000000"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            width: 3,
                            highlight: {
                                opacity: 0.2
                            }

                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

                // Setup a visually pleasing rounding function
                round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    }
  
};




function ObtenerEstadoTerminales() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerEstadoTerminales",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblEstadoTerminales").html("Estado de las terminales (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasAsignadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasAsignadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}


/*Tarjetas activas & inactivas, 4 meses*/
function ObtenerTarjetasActivas() {
    var ddata = [];
    var activas = 0;
    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                activas += parseInt(data[i].data);
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasInactivas() {
    var ddata = [];
    var inactivas = 0;
    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasInactivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                inactivas += parseInt(data[i].data);
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}
/*** METODOS AUXILIARES **/


function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

function verDetalleTerminales(estado) {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Terminales");
  
 
    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerDetalleTerminales",
        data: "{estado: '" + estado + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }

            $("#modalDetalle").modal("show");
        }
    });

}


function ObtenerTotalSociosSexo() {
    var ddata = [];
    var totalSexo = 0;
    var M = 0;
    var F = 0;
    var I = 0;
    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTotalSociosSexo",
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalSexo += parseInt(data[i].data);
                ddata.push(data[i]);
                M = parseInt(data[0].data);
                F = parseInt(data[1].data);
                I = parseInt(data[2].data);

            }
            $("#lblSexo").html("Total usuarios por sexo (" + totalSexo + ")");
            $("#sexo_masculino").html(" " + M);
            $("#sexo_femenino").html(" " + F);
            $("#sexo_indefinido").html(" " + I);
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;

}