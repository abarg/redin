﻿function showSinResultados() {
    $("#divLoading").remove();
    $("#divSinResultados").show();
}

function SearchMore(obj) {
    var page = parseInt($("#hdnPage").val());
    page++;

    $(obj).remove();

    $("#hdnPage").val(page);
    buscar();
}

function createLoading() {
    $("#results").append("<br/><img alt='Buscando..' src='../../img/ajax_loader.gif' id='divLoading' style='margin-left:10px' />");
    //<a class='showMore' id='divLoading'>Cargando...</a>");
}
function canjearProducto(idPremio) {
    if ($("#ddlSocios").val()=="")
        window.location.href = "canjese.aspx?IdProducto=" + idPremio;
    else
        window.location.href = "canjese.aspx?IdProducto=" + idPremio + "&idTarjeta=" + $("#ddlSocios").val();

}
function reiniciar() {
    $("#divSinResultados").hide();
    $("#divLoading").remove();
    $("#hdnPage").val("1");
    buscar();
}

function buscar() {
    var page = parseInt($("#hdnPage").val());

    if (page == 1)//Si es la pagina 1, borro todos los resultados
        $("#results").empty();//$("#resultados").html("");

    createLoading();

    var desde = 0;
    var hasta = 0;
    if ($("#txtPuntosDesde").val() != "")
        desde = parseInt($("#txtPuntosDesde").val());
    if ($("#txtPuntosHasta").val() != "")
        hasta = parseInt($("#txtPuntosHasta").val());

    var idFamilia = 0
    if ($("#cmbFamilia").val() != null && $("#cmbFamilia").val() != "")
        idFamilia = parseInt($("#cmbFamilia").val())

    var idSubFamilia = 0
    if ($("#cmbSubFamilia").val() != null && $("#cmbSubFamilia").val() != "")
        idSubFamilia = parseInt($("#cmbSubFamilia").val())

    var nombre = ""
    if ($("#txtNombre").val() != null && $("#txtNombre").val() != "")
        nombre = $("#txtNombre").val()

    var info = "{ pageSize: " + parseInt($("#ddlPageSize").val())
        + ", page: " + page
        + ", puntosDesde: " + desde
        + ", puntosHasta: " + hasta
        + ", idFamilia: " + idFamilia
        + ", idSubFamilia: " + idSubFamilia
        + ", nombre: '" + nombre
        + "'}";

    $.ajax({
        type: "POST",
        url: "catalogo.aspx/buscar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d.Items.length > 0) {
                $("#resultTmpl").tmpl(data.d.Items).appendTo("#results");
                $("#divLoading").remove();
                if (data.d.Cantidad == 1)
                    $("#results").append("<div align='center' id='showMoreResults' onclick='SearchMore(this);'><button class='btn btn-primary btn-sm'>MOSTRAR M&Aacute;S</button></div>");
            }
            else {
                if (page == 1)
                    showSinResultados();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
            $("#results").empty();
            /*$("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);*/
        }
    });
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    $("#txtPuntosDesde, #txtPuntosHasta").numeric();

    $("#txtNombre,#cmbFamilia, #cmbSubFamilia, #txtPuntosDesde, #txtPuntosHasta,#ddlPageSize").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            reiniciar();
            return false;
        }
    });
    $(".chosen").chosen();

    $("#ddlSocios").change(function () {
        puntosPorTarjeta($("#ddlSocios").val());
    });
}

$(document).ready(function () {
    configControls();
    $("#cmbFamilia").change(function (event) {
        if ($("#cmbFamilia").val() != null && $("#cmbFamilia").val() != "") {
            var id = $("#cmbFamilia").val();
            cargarSubFamilias(id);
        }
    })
});


function cargarSubFamilias(id) {
    $("#cmbSubFamilia").html("");

    var info = "{ idFamiliaPadre:'" + id
        + "'}";

    $.ajax({
        type: "POST",
        url: "catalogo.aspx/cargarSubFamilias",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $(data.d).each(function () {
                $("#cmbSubFamilia").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $("#cmbSubFamilia").trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function puntosPorTarjeta(idTarjeta) {

    if (idTarjeta != null && idTarjeta != "") {
        $.ajax({
            type: "POST",
            url: "catalogo.aspx/puntosPorTarjeta",
            data: "{ idTarjeta: " + idTarjeta
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $("#txtPuntosHasta").val(data.d);
                    $("#divError").hide();
                }
                else
                    $("#txtPuntosHasta").val(0);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });

    }
}