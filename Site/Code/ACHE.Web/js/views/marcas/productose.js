﻿$(document).ready(function () {
    configControls();
    $("#txtPrecio").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtStockActual,#txtStockMinimo").numeric();
});

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var idSubFamilia = 0
    if ($("#cmbSubFamilia").val() != null && $("#cmbSubFamilia").val() != "")
        idSubFamilia=parseInt($("#cmbSubFamilia").val())

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnID").val())
                + ", codigo: '" + $("#txtCodigo").val()
                + "', nombre: '" + $("#txtNombre").val()
                + "', idFamilia: " + parseInt($("#cmbFamilia").val())
                + ", descripcion: '" + $("#txtDescripcion").val()
                + "', precio: '" + $("#txtPrecio").val()
                + "', activo: " + $("#chkActivo").is(':checked')
                + ", idSubFamilia: " + idSubFamilia
                + ", stockActual: '" + $("#txtStockActual").val()
                + "', stockMinimo: '" + $("#txtStockMinimo").val()
                + "', puntos: '" + $("#txtPuntos").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "productose.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#divOk").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "Productos.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    
    } else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function configControls() {
    configDatePicker();

    $("#cmbSubFamilia,#cmbFamilia").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });


    if (parseInt($("#hdnID").val()) > 0)
        $(".subFamilia").show();

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
   
}

$('#cmbFamilia').on('change', function () {
    if ($("#cmbFamilia").val() != null && $("#cmbFamilia").val() != "") {
        var id = $("#cmbFamilia").val();
        cargarSubFamilias(id);
    }
})

function cargarSubFamilias(id) {
    $("#cmbSubFamilia").html("");

    var info = "{ idFamiliaPadre:'" + id
        + "'}";

    $.ajax({
        type: "POST",
        url: "productose.aspx/cargarSubFamilias",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:true,
        success: function (data, text) {
            $(data.d).each(function () {
                $("#cmbSubFamilia").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $("#cmbSubFamilia").trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });    
}

function deleteLogo(file, div) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "', div: '" + div + "'}";

    $.ajax({
        type: "POST",
        url: "productose.aspx/eliminarImagen",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
        },
        error: function (response) {
        }
    });

    return false;
}
