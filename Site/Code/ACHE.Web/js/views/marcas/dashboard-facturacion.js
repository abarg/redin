﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);



$(document).ready(function () {

    
    setTimeout("gebo_charts.fl_facturacion_ahorro()", 2000);
    setTimeout("gebo_charts.fl_facturacion_ahorro_detalle()", 2000);
    setTimeout("gebo_charts.fl_cant_transacciones()", 2000);

    setTimeout("ObtenerTop10Socios()", 2500);
    setTimeout("ObtenerTop10Comercios()", 2500);
    setTimeout("ObtenerTop10SociosMensual()", 2500);
    setTimeout("ObtenerTop10ComerciosMensual()", 2500);
});


gebo_charts = {
    fl_facturacion_ahorro: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro');

        var pagado = ObtenerImportePagado();
        var ahorro = ObtenerImporteAhorro();
        var original = ObtenerImporteOriginal();

        var data = [
            { label: "Pagado", data: pagado },
            { label: "Ahorro", data: ahorro },
            { label: "Original", data: original }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>" + item.series.label + "</b> = " + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },
 

    fl_facturacion_ahorro_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro_detalle');
        var pagado = ObtenerImportePagadoDiario();
        var ahorro = ObtenerImporteAhorroDiario();
        var original = ObtenerImporteOriginalDiario();

        for (var i = 0; i < pagado.length; ++i) { pagado[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < ahorro.length; ++i) { ahorro[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < original.length; ++i) { original[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Pagado", data: pagado },
                { label: "Ahorro", data: ahorro },
                { label: "Original", data: original }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>" + item.series.label + "</b> = $" + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },
    fl_cant_transacciones: function () {
        var elem = $('#fl_cant_transacciones');

        var data = ObtenerCantTransacciones();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>Cantidad</b> = " + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    }
};

/*Facturación & Ahorro, 4 meses*/
function ObtenerImporteAhorro() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImporteAhorro",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagado() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImportePagado",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginal() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImporteOriginal",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Facturación & Ahorro, 30 días*/
function ObtenerImporteAhorroDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImporteAhorroDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginalDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImporteOriginalDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ;
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
            /*ddata.push([Date.UTC(2014, 0, 21, 08, 0, 0), 1180]);
            ddata.push([Date.UTC(2014, 0, 22, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 23, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 24, 08, 0, 0), 1212]);
            ddata.push([Date.UTC(2014, 0, 25, 08, 0, 0), 9922]);
            ddata.push([Date.UTC(2014, 0, 26, 08, 0, 0), 1888]);*/

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagadoDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerImportePagadoDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}
function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()
        + "'}";

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
              
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });

}


/* Obtener Socios y Comercios*/
function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}

function ObtenerTop10SociosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerTop10SociosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySociosMes").html(data.d);
            }
        }
    });
}

function ObtenerTop10ComerciosMensual() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerTop10ComerciosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComerciosMes").html(data.d);
            }
        }
    });
}

/*Mostrar Mensual e Histórico */
function mostrarSociosGeneral() {
    $("#tblSociosMensual").hide();
    $("#tblSocios").show();
}
function mostrarComerciosGeneral() {
    $("#tblComerciosMensual").hide();
    $("#tblComercios").show();
}

function mostrarComerciosMensual() {
    $("#tblComerciosMensual").show();
    $("#tblComercios").hide();
}
function mostrarSociosMensual() {
    $("#tblSociosMensual").show();
    $("#tblSocios").hide();
}


/* Mostrar transacciones*/
function ObtenerCantTransacciones() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-facturacion.aspx/obtenerCantTransacciones",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}


function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtTarjeta, #txtDocumento, #txtTrTarjeta, #txtTrImporte").numeric();

    $("#txtComercio, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Fecha: { type: "string" },
                        Hora: { type: "string" },
                        Operacion: { type: "string" },
                        SDS: { type: "string" },
                        Comercio: { type: "string" },
                        Domicilio: { type: "string" },
                        NroEstablecimiento: { type: "string" },
                        POSTerminal: { type: "string" },
                        Tarjeta: { type: "string" },
                        Socio: { type: "string" },
                        NroDocumentoSocio: { type: "string" },
                        ImporteOriginal: { type: "number" },
                        ImporteAhorro: { type: "number" },
                        Puntos: { type: "integer" },
                        PuntosTotales: { type: "integer" },
                        Credito: { type: "number" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "dashboard-facturacion.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "ID", title: "ID", width: "70px", hidden: true },
            { field: "Fecha", title: "Fecha", width: "80px" },
            { field: "Hora", title: "Hora", width: "70px" },
            { field: "Operacion", title: "Operacion", width: "70px" },
            //{ field: "SDS", title: "SDS", width: "75px" },
            { field: "Comercio", title: "Comercio", width: "180px" },
            { field: "Domicilio", title: "Domicilio", width: "150px" },
            { field: "POSTerminal", title: "Terminal", width: "80px" },
            { field: "Tarjeta", title: "Tarjeta", width: "130px" },
            { field: "Socio", title: "Socio", width: "150px" },
            { field: "NroDocumentoSocio", title: "Doc Socio", width: "80px" },
            { field: "ImporteOriginal", title: "Importe", width: "70px" },
            { field: "ImporteAhorro", title: "Ahorro", width: "70px" },
            { field: "Puntos", title: "Puntos", width: "50px" },
            { field: "Credito", title: "Credito", width: "70px", format: "{0:c}" }
            //{ field: "PuntosTotales", title: "Total", width: "50px" }
        ]
    });


    $('#formTransacciones').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

$(document).ready(function () {
    configControls();
});


function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}