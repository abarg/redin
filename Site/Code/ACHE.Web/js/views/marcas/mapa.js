﻿var map = null;
var gmarkers = [];
//var gicons = [];//var markerclusterer = null;

/*gicons["Comercio"] = new google.maps.MarkerImage("/img/markers/red.png");
gicons["Femenino"] = new google.maps.MarkerImage("/img/markers/pink.png");
gicons["Masculino"] = new google.maps.MarkerImage("/img/markers/blue.png");gicons["Indefinido"] = new google.maps.MarkerImage("/img/markers/gray.png");*/
function initialize() {
    var point = new google.maps.LatLng(-37.9604967, -57.563621799999964);
    // create the map
    var myOptions = {
        zoom: 5,
        center: point,
        mapTypeControl: false,
        mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
        navigationControl: true,
        zoomControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

    //google.maps.event.addListener(map, 'click', function () {
    //    infoWindow.close();
    //});
}


function cargarSubRubros() {

    var idRubro = 0

    if (parseInt($("#ddlRubro").val()) > 0) {
        idRubro = parseInt($("#ddlRubro").val());
    }
    var info = "{ IDRubroPadre: " + idRubro + " }";

    $.ajax({
        type: "POST",
        url: "mapa.aspx/cargarSubRubros",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#ddlSubRubro").empty();
                //$("#ddlSubRubro").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#ddlSubRubro").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                $('#ddlSubRubro').trigger("liszt:updated");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
        }
    });


}
function buscar() {
    $("#imgLoading").show();
    $("#btnBuscar").attr("disabled", true);

    var tipo = "C";
    if ($("#rdbSocios").is(':checked'))
        tipo = "S";
    var idRubro = 0;
    if ($("#ddlRubro").val() != "")
        idRubro = parseInt($("#ddlRubro").val());
    var idSubRubro = 0;
    if ($("#ddlSubRubro").val() != "" && $("#ddlSubRubro").val() != null)
        idSubRubro = parseInt($("#ddlSubRubro").val());
    var info = "{ tipo: '" + tipo
           + "', edad: '" + $("#ddlEdad").val()
           + "', rubro: " + idRubro
           + ", subRubro: " + idSubRubro
           + "}";

    clearMarkers();

    $.ajax({
        type: "POST",
        url: "mapa.aspx/GetMarkers",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {

                    $("#litResultados").html("Resultados encontrados: <b>" + data.d.length + "</b>");

                    //Read the name, address, latitude and longitude for each Marker
                    var name = data.d[i].Nombre;
                    var address = data.d[i].Direccion;
                    var lat = data.d[i].Lat;
                    var lng = data.d[i].Lng;                    var cat = data.d[i].Categoria;                    var contenido = data.d[i].Contenido;                    var ficha = data.d[i].Ficha;                    var foto = data.d[i].Foto;                    //var icono = new google.maps.MarkerImage("/img/markers/" + data.d[i].Icono + ".png");                    var icono = "/files/rubros/" + data.d[i].Icono;

                    var markerCoords = new google.maps.LatLng(lat, lng);                    var marker = new google.maps.Marker({
                        position: markerCoords,
                        icon: icono,
                        //shadow: iconShadow,
                        map: map,
                        title: name,
                        zIndex: Math.round(markerCoords.lat() * -100000) << 5,
                        mycategory: cat,
                        description: name,
                        content: contenido,
                        link: ficha,
                        photo: foto
                    });

                    var infoWindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {

                            var aux = '<div id="contentInfoWindow' + i + '" class="contentMap">\
                                <div class="contentImg">\
                                    <img src="' + this.photo + '" title="" style="width:150px" />\
                                </div>\
                                <div class="contentTxt">\
                                <h2>' + this.description + '</h2>' + this.content + ' </div>\
                                <div class="clear"></div>\
                            </div>'

                            infoWindow.setContent(aux);
                            infoWindow.open(map, marker);
                        }
                    })(marker, i));

                    gmarkers.push(marker);
                }

                $("input:checkbox[name=chkFiltros]").each(function () {
                    if ($(this).is(':checked')) {
                        for (var i = 0; i < gmarkers.length; i++) {
                            if (gmarkers[i].mycategory == $(this).attr("value")) {
                                gmarkers[i].setVisible(true);
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < gmarkers.length; i++) {
                            if (gmarkers[i].mycategory == $(this).attr("value")) {
                                gmarkers[i].setVisible(false);
                            }
                        }
                    }
                });



                $("#imgLoading").hide();
                $("#btnBuscar").attr("disabled", false);
                //markerCluster = new MarkerClusterer(map, gmarkers);
                //markerCluster.setIgnoreHidden(true);

                // Listen for a cluster to be clicked
                //google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {

                //    var content = '';

                //    // Convert lat/long from cluster object to a usable MVCObject
                //    var info = new google.maps.MVCObject;
                //    info.set('position', cluster.center_);

                //    var infowindow = new google.maps.InfoWindow();
                //    infowindow.close();
                //    infowindow.setContent('<h1>Hi this is my Info Window');
                //    infowindow.open(map, info);

                //});
            }
        }
        //error: onXMLLoadFailed
    });
}

function clearMarkers() {
    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }

    gmarkers = [];
    $("#litResultados").html("");
}

function markerClick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}

function showMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(true);
        }
    }
    //markerCluster.repaint();

    //document.getElementById("chk" + categoria).checked = true;
}

function hideMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(false);
        }
    }
    //markerCluster.repaint();

    //document.getElementById("chk" + categoria).checked = false;
}

function filtroClick(box, categoria) {
    if (box.checked) {
        showMarkers(categoria);
    } else {
        hideMarkers(categoria);
    }
}

function toggleTipo() {
    if ($("#rdbComercios").is(':checked')) {
        $("#divComercios").show();
        $("#divSocios").hide();
    }
    else {
        $("#divComercios").hide();
        $("#divSocios").show();
    }
}


/*
function onXMLLoadFailed() {
    alert("Error al cargar los datos.");
}



function myclick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}*/