﻿function eliminarTarjeta(id)
{
    var info = "{ idTarjeta: " + id + "}";

    $.ajax({
        type: "POST",
        url: "alertas-seguimiento.aspx/eliminarTarjeta",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            obtenerTarjetas();
        },
        error: function (response) {

            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#btnAceptar").attr("disabled", false);
        }
    });
}

function obtenerTarjetas()
{
    $("#tBody").html("");

    $.ajax({
        type: "POST",
        url: "alertas-seguimiento.aspx/getTarjetasEnSeguimiento",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data != null) {
                var html = "";

                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        html += "<tr>";

                        html += "<td>" + data.d[i].Tarjeta + "</td>";
                        html += "<td>" + data.d[i].Apellido + " " + data.d[i].Nombre + "</td>";
                        html += "<td>" + data.d[i].NroDocumento + "</td>";
                        html += "<td>" + data.d[i].EstadoTarjeta + "</td>";
                        html += "<td><img src='../../img/grid/gridDelete.gif' onclick='eliminarTarjeta(" + data.d[i].IDSocio + ");' style='cursor:pointer' /></td>";
                        html += "</tr>";
                    }
                    $("#tBody").html(html);
                }
                else
                    $("#tBody").html("<tr><td colspan='3'>No hay tarjetas en seguimiento</td></tr>");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function agregar() {
    $("#divErrorTarjetas").hide();
    $("#divError").hide();
    $("#btnAsociarTarjeta").attr("disabled", true);

    if ($("#ddlTarjetas").val() == "") {
        $("#divError").html("Seleccione una tarjeta");
        $("#divError").show();
        $("#btnAceptar").attr("disabled", false);
    }
    else {
        var info = "{ idTarjeta: " + $("#ddlTarjetas").val() + "}";

        $.ajax({
            type: "POST",
            url: "alertas-seguimiento.aspx/agregarTarjeta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtTarjeta,#txtDNI,#txtNombre").val("");
                
                obtenerTarjetas();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#btnAceptar").attr("disabled", false);
            }
        });
    }
}

function buscarTarjetas() {
    $("#divError").hide();
    $("#divResultado1").hide();
    $("#divResultado2").hide();

    //if ($("#txtTarjeta").val() == "" || $("#txtTarjeta").val().length < 5) {
    //    $("#divError").html("Ingrese una terminación de una tarjeta");
    //    $("#divError").show();
    //}
    //else {
        $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");

        var info = "{ tarjeta: '" + $("#txtTarjeta").val()
                    + "', nombre: '" + $("#txtNombre").val()
                    + "', dni: '" + $("#txtDNI").val()
                    + "'}";

        $.ajax({
            type: "POST",
            url: "alertas-seguimiento.aspx/buscarTarjetas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTarjetas"));
                    }
                    if (data.d.length > 0) {
                        $("#divResultado1").show();
                        $("#divResultado2").show();
                    }
                    else {
                        $("#divError").html("No se encontraron resultados");
                        $("#divError").show();
                    }
                    //$('#ddlEmpleado').trigger('liszt:updated');
                }
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
   //}
}

$(document).ready(function () {

    $("#txtTarjeta,#txtNroDNI").numeric();
    $("#txtTarjeta,#txtDNI,#txtNombre").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarTarjetas();
            return false;
        }
    });

    obtenerTarjetas();

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore"
    });
});