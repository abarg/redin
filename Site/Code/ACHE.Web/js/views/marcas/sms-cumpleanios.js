﻿function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ mensaje: '" + $("#txtMensaje").val() + "' }";

        $.ajax({
            type: "POST",
            url: "sms-cumpleanios.aspx/GuardarMensaje",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function configControls() {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 150,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9 ñ\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, numeros, puntos, guiones medios y espacios en blanco.");
}

$(document).ready(function () {
    configControls();
});