﻿function filter() {
    $("#divError").hide();
    $("#divOk").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }

    grid.dataSource.filter($filter);
}

function guardar() {
    $('#formEdicion').validate();
    if ($('#formEdicion').valid()) {        
        var idPadre = 0;
        if ($("#ddlFamilias").val() != null && $("#ddlFamilias").val() != "")
            idPadre = parseInt($("#ddlFamilias").val());

        var info = "{ id: " + parseInt($("#hdnID").val())
                + ", nombre: '" + $("#txtNombreFamilia").val()
                + "', idPadre: " + idPadre
                + "}";

        $.ajax({
            type: "POST",
            url: "familias.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#modalCargarFamilia").modal("hide");
                filter();
                $("#divOk").show();
                $("#divError").hide();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#modalCargarFamilia").modal("hide");
                filter();
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }

}

function configControls() {
    $("#txtNombre").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    cargarFamilias();

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Nombre: { type: "string" },
                        NombrePadre: { type: "string" }
                        //IDPadre: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "familias.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            { field: "ID", title: "ID", width: "50px", hidden: true },
            { field: "Nombre", title: "Nombre", width: "100px" },
            { field: "NombrePadre", title: "Nombre Padre", width: "100px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        cargarFamilias();
        $("#hdnID").val(dataItem.ID);
        $("#txtNombreFamilia").val(dataItem.Nombre);
        //$("#ddlFamilias").val(dataItem.IDPadre);
        obtenerPadre(dataItem.ID);
        $("#modalCargarFamilia").modal("show");
        
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "familias.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    //html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/></div>";
    html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    return html;
}

function Nuevo() {
    //abrir pop up
    $("#modalCargarFamilia").modal("show");
    $("#txtNombreFamilia").val("");
    parseInt($("#hdnID").val(0));
    cargarFamilias();

}

$(document).ready(function () {

    configDatePicker();
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    configControls();
});

function cargarFamilias() {
    $("#ddlFamilias").html("");

    $.ajax({
        type: "POST",
        url: "familias.aspx/cargarFamiliasPadres",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $(data.d).each(function () {
                $("#ddlFamilias").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $("#ddlFamilias").trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function obtenerPadre(id) {

    var info = "{ id: " + parseInt($("#hdnID").val())
            + "}";
    $.ajax({
        type: "POST",
        url: "familias.aspx/obtenerPadre",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //async: true,
        success: function (data, text) {
            $("#ddlFamilias").val(data.d);
            return data.d;                
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}