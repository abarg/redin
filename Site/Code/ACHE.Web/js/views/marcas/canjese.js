﻿function getInfoPremio() {
    //var foto = $("#ddlPremio").val().split('_')[1];
    //var puntos = $("#ddlPremio").val().split('_')[2];
    //var pesos = $("#ddlPremio").val().split('_')[3];
    //if (foto == "" || foto == null)
    //    foto = "nogift.png";

    //$("#imgFoto").attr("src", "/files/premios/" + foto);
    //$("#spnValorPremio").html("Valor puntos: " + puntos + " - Valor pesos: " + pesos);
}

function buscarSocios() {
    if ($("#txtValorSocio").val() == "") {
        $("#divErrorTrSocio").html("Por favor, ingrese el valor a buscar");
        $("#divErrorTrSocio").show();

        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
    }
    else {
        $("#divErrorTrSocio").hide();
        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("");

        var info = "{ tipo: '" + $("#ddlTrBuscarSocio").val()
                + "', valor: '" + $("#txtValorSocio").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "posweb.aspx/buscarSocios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrSocio"));
                    }

                    if (data.d.length == 0) {
                        $("#divErrorTrSocio").html("No se encontraron socios");
                        $("#divErrorTrSocio").show();
                    }
                    else if (data.d.length == 1) {
                        $("#ddlTrSocio").val(data.d[0].ID);
                        if ($("#ddlTrSocio").val() == "0") {
                            $("#ddlTrTarjeta").html("<option value='" + data.d[0].Nombre + "'>" + data.d[0].Nombre + "</option>");
                            mostrarInfoSocio();
                        }
                        else {
                            mostrarInfoSocio();
                            buscarTarjetas();
                        }
                    }
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTrSocio").html(r.Message);
                $("#divErrorTrSocio").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function buscarTarjetas() {
    if ($("#ddlTrSocio").val() != "") {

        //alert($("#ddlTrSocio").val());
        if ($("#ddlTrSocio").val() == "0") {
            $("#ddlTrTarjeta").html("<option value='" + $("#ddlTrSocio option:selected").text() + "'>" + $("#ddlTrSocio option:selected").text() + "</option>");
            mostrarInfoSocio();
        }
        else {
            $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");

            var info = "{ socio: " + $("#ddlTrSocio").val() + "}";

            $.ajax({
                type: "POST",
                url: "posweb.aspx/buscarTarjetas",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data != null) {
                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrTarjeta"));
                        }

                        if (data.d.length == 0) {
                            $("#divErrorTrSocio").html("No se encontraron tarjetas asociadas");
                            $("#divErrorTrSocio").show();
                        }
                        else if (data.d.length == 1) {
                            $("#ddlTrTarjeta").val(data.d[0].ID);
                            getPuntos(data.d[0].ID);
                            $("#ddlTrComercio").focus();
                        }
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorTrSocio").html(r.Message);
                    $("#divErrorTrSocio").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });

            mostrarInfoSocio();
        }
    }
    else
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
}

function mostrarInfoSocio() {
    var info = "{ id: " + $("#ddlTrSocio").val() + ", max: 5}";

    if ($("#ddlTrSocio").val() != "0") {

        $.ajax({
            type: "POST",
            url: "posweb.aspx/obtenerInfoSocio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    //$("#lblDni").html("DNI: " + data.d.ID);
                    if (data.d.Nombre != "")
                        $("#imgSocio").attr("src", "/files/socios/" + data.d.Nombre);
                    else
                        $("#imgSocio").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTrSocio").html(r.Message);
                $("#divErrorTrSocio").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        //$("#lblDni").html("DNI: No disponible");
        $("#imgSocio").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
    }
}


function getPuntos(puntos) {
    if ($("#ddlTrSocio").val() != "0")
        $("#lblPuntos").html("Puntos disponibles: " + puntos);
}


$(document).ready(function () {
    configControls();
    $("#ddlEstado").change(function (event) {
        if ($("#ddlEstado").val() != null && $("#ddlEstado").val() != "") {
            var estado = $("#ddlEstado option:selected").text()
            if (estado.toLowerCase() == "rechazado") {
                $(".ddlMotivos").show();
            }
            else
                $(".ddlMotivos").hide();
        }
    })

    $("#ddlMotivos").change(function (event) {
        if ($("#ddlMotivos").val() != null && $("#ddlMotivos").val() != "") {
            var motivo = $("#ddlMotivos option:selected").text()
            if (motivo.toLowerCase() == "otros") {
                $("#txtOtros").show();
            }
            else
                $("#txtOtros").hide();
        }
    })
    if ($("#txtValorSocio").val() != "") {
        buscarSocios();
    }
   
});

     function imprimir() {
         //  window.print();
         var printContents =$("#print").html();
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();
         document.body.innerHTML = originalContents;
     }

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    //var idMarca = 0;
    //if ($("#hdnID").val() != "0" && parseInt($("#hdnID").val() != null))
    //    idMarca = parseInt($("#hdnID").val());

    var motivo = "";
    var obsOtros = "";

    if ($("#ddlMotivos").val() != null && $("#ddlMotivos").val() != "") {
        motivo = $("#ddlMotivos").val();
        if (motivo == "5") {
            obsOtros = $("#txtObsOtros").val();
        }
    }
    var estado = $("#ddlEstado option:selected").text();
    if ($("#ddlProductos").val() == "" || $("#ddlProductos").val() == null) {
        $("#divError").html("Debe seleccionar un producto");
        $("#divError").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }   
    else 
    {   var idProducto = 0
        if ($("#ddlProductos").val() != null && $("#ddlProductos").val() != "")
            idProducto = parseInt($("#ddlProductos").val())

        if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnID").val())
            + ", idProducto: '" + idProducto
            + "', estado: '" + estado
            + "', idEstado: " + $("#ddlEstado").val()
            + ", cantidad: " + parseInt($("#txtCantidad").val())
            + ", tarjeta: '" + $("#ddlTrTarjeta option:selected").text()
            + "', obs: '" + $("#txtObservaciones").val()
            + "', motivoRechazo: '" + motivo
            + "', obsOtros: '" + obsOtros
            + "'}";


        $.ajax({
            type: "POST",
            url: "canjese.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "canjes.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

    //else {
    //    $('html, body').animate({ scrollTop: 0 }, 'slow');
    //    return false;
    //}
}



function configControls() {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    $("#txtCantidad").numeric();

    $("#txtValorSocio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarSocios();
            return false;
        }
    });

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de Canje");
    }
    else {
        $("#litTitulo").html("Alta de Canje");
        $(".chzn_b").chosen({ allow_single_deselect: true });
    }

    if ($("#ddlEstado").val() != null && $("#ddlEstado").val() != "") {
        var estado = $("#ddlEstado option:selected").text()
        if (estado.toLowerCase() == "rechazado") {
            $(".ddlMotivos").show();
            if ($("#ddlMotivos").val() != null && $("#ddlMotivos").val() != "") {
                motivo = $("#ddlMotivos").val()
                //motivo = motivoRechazo();               
                if (motivo == "5") {
                    $("#txtOtros").show();
                }
            }
        }
        else
            $(".ddlMotivos").hide();
    }
}
