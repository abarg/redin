﻿function filter() {
    configGrid();

    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }

    var idFamilia = $("#cmbFamilia").val();
    if (idFamilia != "" && idFamilia != null) {
        $filter.push({ field: "IDFamilia", operator: "equal", value: parseInt(idFamilia) });
    }

    var idSubfamilia = $("#cmbSubFamilia").val();
    if (idSubfamilia != "" && idSubfamilia != null) {
        $filter.push({ field: "IDSubFamilia", operator: "equal", value: parseInt(idSubfamilia) });
    }

    grid.dataSource.filter($filter);
}


function configControls() {
    $("#txtNombre,#cmbSubFamilia,#cmbFamilia").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Nombre: { type: "string" },
                        Codigo: { type: "string" },
                        IDFamilia: { type: "integer" },
                        Familia: { type: "string" },
                        IDSubFamilia: { type: "integer" },
                        SubFamilia: { type: "string" },
                        Descripcion: { type: "string" },
                        Precio: { type: "decimal" },
                        Puntos: { type: "integer" },
                        StockActual: { type: "integer" },
                        StockMinimo: { type: "integer" },
                        Activo: { type: "string" },
                        PrecioXCantidad: { type: "decimal" }
                    }
                }
            },
            pageSize: 250,
            batch: true,
            transport: {
                read: {
                    url: "productos.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            { field: "ID", title: "ID", width: "50px", hidden: true },
            { field: "Nombre", title: "Nombre", width: "200px" },
            { field: "Codigo", title: "Codigo", width: "100px" },
            { field: "Familia", title: "Familia", width: "180px" },
            { field: "SubFamilia", title: "Sub Familia", width: "180px" },
            { field: "Descripcion", title: "Descripcion", width: "200px" },
            { field: "Precio", title: "Precio", width: "100px" },
            { field: "Puntos", title: "Puntos", width: "100px" },
            { field: "StockActual", title: "Stock Actual", width: "100px" },
            { field: "StockMinimo", title: "Stock Minimo", width: "100px" },
            { field: "Activo", title: "Activo", width: "70px" },
            { field: "PrecioXCantidad", title: "Precio x Cantidad", width: "70px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "productose.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "productos.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    //html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/></div>";
    html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    return html;
}

function Nuevo() {
    window.location.href = "productose.aspx";
}

$(document).ready(function () {
    configControls();

    $("#cmbFamilia").change(function (event) {
        if ($("#cmbFamilia").val() != null && $("#cmbFamilia").val() != "") {
            var id = $("#cmbFamilia").val();
            cargarSubFamilias(id);
        }
    })
});


function cargarSubFamilias(id) {
    $("#cmbSubFamilia").html("");

    var info = "{ idFamiliaPadre:'" + id
        + "'}";

    $.ajax({
        type: "POST",
        url: "productos.aspx/cargarSubFamilias",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $(data.d).each(function () {
                $("#cmbSubFamilia").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $("#cmbSubFamilia").trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //$("#modalCargarFamilia").modal("hide");
            //filter();
            $("#divOk").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}


function exportar() {

    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var nombre = "";
    if ($("#txtNombre").val() != null && $("#txtNombre").val() != "") {
        nombre = $("#txtNombre").val();
    }

    var idFamilia = 0;
    if ($("#cmbFamilia").val() != null && $("#cmbFamilia").val() != "") {
        idFamilia = parseInt($("#cmbFamilia").val());
    }
    var idSubFamilia = 0;
    if ($("#cmbSubFamilia").val() != null && $("#cmbSubFamilia").val() != "") {
        idSubFamilia = parseInt($("#cmbSubFamilia").val());
    }

    var info = "{ nombre: '" + nombre
            + "', idFamilia: " + idFamilia
            + ",  idSubfamilia: " + idSubFamilia
            + "}";

    $.ajax({
        type: "POST",
        url: "productos.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}
