﻿
$(document).ready(function () {

    GenerarTablaCanjes();
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtFechaDesde,#txtFechaHasta, #ddlEstado, #cmbFamilia, #txtSocio, #txtProducto").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrarEstadosMasivo();
            return false;
        }
    });
});

function GenerarTablaCanjes() {

    /*
        var info = "{ idFamilia: " + idFamilia
            + ", idSubFamilia: " + idSubFamilia
            + ", idMarca: " + idMarca
            + "}";
    */

    $.ajax({
        type: "POST",
        url: "cambiar-estados-masivo.aspx/generarTablaCanjes",
        //  data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyCanjes").html(data.d);
            }
        }
    });
}

function actualizarMasivamete() {
    
    var estadoACambiar = $("#cmbEstadoMasivo").val();
    //var estadoACambiar = $("#cmbEstadoMasivo option:selected").text();

    $('.estado').each(function () {
        var i = 1;
        var estadoActual = this.id;
        $('#'+estadoActual).val(estadoACambiar  );
        //$(estadoActual + 'option[value="' + estadoACambiar + '"]').attr("selected", "selected");
        //$(estadoActual +'option[value="'+estadoACambiar+'"]');
    });
    $('#modalDetalle').modal('hide');
}

function ActualizarEstados() {
    $('#divOk').hide();

    var estados = "";
    $('.estado').each(function () {
        estados += this.id + "-" + $("#" + this.id).val() + "#";
    });
    $.ajax({
        type: "POST",
        url: "cambiar-estados-masivo.aspx/GuardarEstados",
        data: "{ estados: '" + estados + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            GenerarTablaCanjes();
            $('#divOk').show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ":" + thrownError);
        }
    });
}

function filtrarEstadosMasivo() {

    var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()
      + "', estado: " + $("#ddlEstado").val()
      + ", idFamilia: " + $("#cmbFamilia").val()
      + ", socio: '" + $("#txtSocio").val()
      + "', producto: '" + $("#txtProducto").val()
      + "'}";

    $.ajax({
        type: "POST",
        url: "cambiar-estados-masivo.aspx/filtrarEstadosMasivos",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data != null) {
                $("#bodyCanjes").html(data.d);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ":" + thrownError);
        }
    });
}
