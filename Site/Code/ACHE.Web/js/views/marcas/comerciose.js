﻿$(document).ready(function () {

    configControl();
    toggleButtons();
    $('#tabArancelPuntos').show();

    $("#ddlFidely1").hide();
    $("#ddlFidely2").hide();
    $("#ddlFidely3").hide();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });


    if ($("#hfIDTerminal").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());

    }
    else
        $("#litTitulo").html("Alta de Comercio");
});

function configControl() {


    $("#gridAsociados").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Usuario: { type: "string" },
                        Email: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Comerciose.aspx/GetListaGrillaAsociados", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "12px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Email", title: "Email", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridAsociados").delegate(".editColumn", "click", function (e) {
        var grid = $("#gridAsociados").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.ID);
        $("#txtUsuario2").val(dataItem.Usuario);
        $("#txtEmailUsuarioComercio").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin")
            $("#ddlTipoUsuario").val("A");
        else
            $("#ddlTipoUsuario").val("B");
        $("#btnAgregarUsuario").html("Actualizar");
    });

    $("#gridAsociados").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridAsociados").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Comerciose.aspx/Delete",
                data: "{ id: " + dataItem.ID+ "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function filter() {
    var grid = $("#gridAsociados").data("kendoGrid");
    var $filter = new Array();

    var usuario = $("#txtUsuario").val();
    if (usuario != "" && usuario != null) {
        $filter.push({ field: "Usuario", operator: "contains", value: usuario });
    }


    grid.dataSource.filter($filter);
}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#formButtons").show();
        $("#tabArancelPuntos").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#formButtons").hide();
        $("#tabArancelPuntos").hide();
    });
}

function grabar() {

    $("#divError").hide();
    $("#divOk").hide();
    $('#formComercio').validate();

    if ($('#formComercio').valid()) {

        var idMarca = 0;
        if ($("#ddlMarcas").val() != "")
            idMarca = parseInt($("#ddlMarcas").val());

        var idDealer = 0;
        if ($("#ddlDealer").val() != "")
            idDealer = parseInt($("#ddlDealer").val());

        var idFranquicia = 0;
        if ($("#ddlFranquicias").val() != "")
            idFranquicia = parseInt($("#ddlFranquicias").val());

        var idZona = 0;
        if ($("#cmbZona").val() != "" && $("#cmbZona").val() != null)
            idZona = parseInt($("#cmbZona").val());


        var info = "{ IDTerminal: '" + $("#hfIDTerminal").val()
        + "', Descuento: '" + $("#txtDescuento").val()
        + "', Descuento2: '" + $("#txtDescuento2").val()
        + "', Descuento3: '" + $("#txtDescuento3").val()
        + "', Descuento4: '" + $("#txtDescuento4").val()
        + "', Descuento5: '" + $("#txtDescuento5").val()
        + "', Descuento6: '" + $("#txtDescuento6").val()
        + "', Descuento7: '" + $("#txtDescuento7").val()
        + "', DescuentoVip: '" + $("#txtDescuentoVip").val()
        + "', DescuentoVip2: '" + $("#txtDescuentoVip2").val()
        + "', DescuentoVip3: '" + $("#txtDescuentoVip3").val()
        + "', DescuentoVip4: '" + $("#txtDescuentoVip4").val()
        + "', DescuentoVip5: '" + $("#txtDescuentoVip5").val()
        + "', DescuentoVip6: '" + $("#txtDescuentoVip6").val()
        + "', DescuentoVip7: '" + $("#txtDescuentoVip7").val()
        + "', MulPuntosVip: '" + $("#txtMulPuntosVip").val()
        + "', MulPuntosVip2: '" + $("#txtMulPuntosVip2").val()
        + "', MulPuntosVip3: '" + $("#txtMulPuntosVip3").val()
        + "', MulPuntosVip4: '" + $("#txtMulPuntosVip4").val()
        + "', MulPuntosVip5: '" + $("#txtMulPuntosVip5").val()
        + "', MulPuntosVip6: '" + $("#txtMulPuntosVip6").val()
        + "', MulPuntosVip7: '" + $("#txtMulPuntosVip7").val()
        + "', MultPuntos1: '" + $("#txtMulPuntos1").val()
        + "', MultPuntos2: '" + $("#txtMulPuntos2").val()
        + "', MultPuntos3: '" + $("#txtMulPuntos3").val()
        + "', MultPuntos4: '" + $("#txtMulPuntos4").val()
        + "', MultPuntos5: '" + $("#txtMulPuntos5").val()
        + "', MultPuntos6: '" + $("#txtMulPuntos6").val()
        + "', MultPuntos7: '" + $("#txtMulPuntos7").val()
        + "'}";

        $.ajax({
            type: "POST",
            url: "Comerciose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());

                //toggleTabs();


                verificarEstablecimiento(data.d);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function agregarUsuario() {
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtUsuario2").val() == "" || $("#txtEmailUsuarioComercio").val() == "" || $("#txtPwd").val().length < 3) {
        $("#divError").html("Por favor,complete todos los datos");
        $("#divError").show();
    }
    else {
        var info = "{ IDTerminal: " + parseInt($("#hfIDTerminal").val())
           + ", IDUsuario: " + parseInt($("#hfIDUsuario").val())
           + ", usuario: '" + $("#txtUsuario2").val()
           + "', email: '" + $("#txtEmailUsuarioComercio").val()
           + "', pwd: '" + $("#txtPwd").val()
           + "', tipo: '" + $("#ddlTipoUsuario").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "Comerciose.aspx/procesarUsuario",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtUsuario2").val("");
                $("#txtEmailUsuarioComercio").val("");
                $("#txtPwd").val("");
                $("#hfIDUsuario").val("0");
                $("#btnAgregarUsuario").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorUsuario").html(r.Message);
                $("#divErrorUsuario").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}