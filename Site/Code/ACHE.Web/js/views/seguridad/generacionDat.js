﻿function showDias(tipo)
{
    if (tipo == "1")
        $("#divDias").show();
    else
        $("#divDias").hide();

}

function generar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formGeneracionDat').validate();
    $("#btnGenerar").attr("disabled", true);

    if ($('#formGeneracionDat').valid()) {
        var info = "{ TipoArchivo: '" + $("#ddlTipoArchivo").val()
            + "', NroProceso: '" + $("#txtNroProceso").val()
            + "', diaDescuento: '" + $("#ddlDia").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "GeneracionDat.aspx/generar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                //$("#lnkDownload").attr("href", data.d);
                //alert(window.location.href.split("/GeneracionData.aspx").join(data.d));
                $("#lnkDownload").attr("href", "https://www.redin.com.ar/modulos/seguridad" + data.d);
                $("#lnkDownload").attr("download", data.d.split("/")[3]);
                $("#btnGenerar").attr("disabled", false);
                //$("#lnkDownload").attr("href", window.location.href.split("/ReporteComercios.aspx").join(data.d));
                //window.open("http://wi271584.ferozo.com/modulos/seguridad" + data.d);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#btnGenerar").attr("disabled", false);
            }
        });
    }
    else {
        $("#btnGenerar").attr("disabled", false);
        $("#imgLoading").hide();
        return false;
    }
}

function exportar() {
    $("#imgLoading2").show();
    $("#divError").hide();
    $("#lnkDownloadCSV").hide();
    $('#formGeneracionDat').validate();
    $("#btnExportar").attr("disabled", true);

    if ($('#formGeneracionDat').valid()) {
        var info = "{ TipoArchivo: '" + $("#ddlTipoArchivo").val()
            + "', NroProceso: '" + $("#txtNroProceso").val()
            + "', diaDescuento: '" + $("#ddlDia").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "GeneracionDat.aspx/exportar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#imgLoading2").hide();
                $("#lnkDownloadCSV").show();
                //$("#lnkDownload").attr("href", data.d);
                //alert(window.location.href.split("/GeneracionData.aspx").join(data.d));
                $("#lnkDownloadCSV").attr("href", data.d);
                $("#lnkDownloadCSV").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
                //$("#lnkDownload").attr("href", window.location.href.split("/ReporteComercios.aspx").join(data.d));
                //window.open("http://wi271584.ferozo.com/modulos/seguridad" + data.d);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading2").hide();
                $("#lnkDownloadCSV").hide();
                $("#btnExportar").attr("disabled", false);


            }
        });
    }
    else {
        $("#btnExportar").attr("disabled", false);
        $("#imgLoading2").hide();
        return false;
    }
}

$(document).ready(function () {

    $('#formGeneracionDat').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});