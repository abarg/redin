﻿function showDias(tipo) {
    if (tipo == "1")
        $("#divDias").show();
    else
        $("#divDias").hide();

}

function generar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formGeneracionDat').validate();
    $("#btnGenerar").attr("disabled", true);

    if ($('#formGeneracionDat').valid()) {
        if ($("#searchable").val() == "" || $("#searchable").val() == null) {
            $("#divError").html("Debe selecionar al menos una factura");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#btnGenerar").attr("disabled", false);
            return false;
        }
        else {
            validarRetenciones($("#searchable").val());
        }
    }
    else {
        $("#btnGenerar").attr("disabled", false);
        $("#imgLoading").hide();
        return false;
    }
}

function escribirArchivo() {

    var info = "{ venc1: '" + $("#txtVencimiento1").val()
    + "', venc2: '" + $("#txtVencimiento2").val()
    + "', venc3: '" + $("#txtVencimiento3").val()
    + "', facturas: '" + $("#searchable").val()
    + "' }";

    $.ajax({
        type: "POST",
        url: "Galicia.aspx/EscribirArchivo",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#imgLoading").hide();
            $("#btnGenerar").attr("disabled", false);
            $("#lnkDownload").show();
            $("#lnkDownload").attr("href", "/files/galicia/texto.txt");
            var fecha = new Date();
            var dia = fecha.getDate();
            var mes = (fecha.getMonth() + 1)
            var anio = fecha.getFullYear();
            $("#lnkDownload").attr("download", "Galicia_" + dia + mes + anio);
            //$("#lnkDownload").attr("download", data.d.split("/")[3]);

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnGenerar").attr("disabled", false);
        }
    });
}

$(document).ready(function () {

    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtDesde", "txtHasta");

    $('#formGeneracionDat').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más empresas"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
});

function Previsualizar() {

    $("#bodyDetalle").html();

    //$("#titDetalle").html("Facturas");

    //$("#headDetalle").html("<tr><th>IDFactura</th><th>Nombre</th><th>IDComercio</th><th>Fecha Alta</th><th>Tipo</th><th>Numero</th><th>Documento</th><th>Importe Total</th><th>Documento</th><th>FormaPago</th><th>Forma Pago CBU</th><th>Forma Pago Tipo Cuenta</th><th>Tip oDocumento</th><th>Retenciones</th><th>Canje</th></tr>");
    $('#modalDetalle').modal('show');

    var info = "{ venc1: '" + $("#txtVencimiento1").val()
    + "', venc2: '" + $("#txtVencimiento2").val()
    + "', venc3: '" + $("#txtVencimiento3").val()
    + "', facturas: '" + $("#searchable").val()
    + "' }";

    $.ajax({
        type: "POST",
        url: "galicia.aspx/Previsualizar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#bodyDetalle").html(data.d);
        }
    });
}

function exportar() {
    $("#imgLoadingExl").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formGeneracionDat').validate();
    $("#btnGenerar").attr("disabled", true);

    if ($('#formGeneracionDat').valid()) {
        if ($("#searchable").val() == "" || $("#searchable").val() == null) {
            $("#divError").html("Debe selecionar al menos una factura");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingExl").hide();
            $("#btnGenerar").attr("disabled", false);
            return false;
        }
        else {
            var info = "{ venc1: '" + $("#txtVencimiento1").val()
                + "', venc2: '" + $("#txtVencimiento2").val()
                + "', venc3: '" + $("#txtVencimiento3").val()
                + "', facturas: '" + $("#searchable").val()
                + "' }";

            //alert(info);

            $.ajax({
                type: "POST",
                url: "Galicia.aspx/Exportar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#imgLoadingExl").hide();
                    $("#btnGenerar").attr("disabled", false);

                    $("#lnkDownloadExl").attr("href", data.d);
                    $("#lnkDownloadExl").attr("download", data.d);
                    $("#lnkDownloadExl").show();
                    // $("#btnExportar").attr("disabled", false);
                    /*                    var fecha = new Date();
                                        var dia = fecha.getDate();
                                        var mes = (fecha.getMonth() + 1)
                                        var anio = fecha.getFullYear();
                                        $("#lnkDownloadExl").attr("download", "Galicia_" + dia + mes + anio);*/
                    //$("#lnkDownload").attr("download", data.d.split("/")[3]);

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoadingExl").hide();
                    $("#lnkDownloadExl").hide();
                    $("#btnGenerar").attr("disabled", false);
                }
            });
        }
    }
    else {
        $("#btnGenerar").attr("disabled", false);
        $("#imgLoadingExl").hide();
        return false;
    }
}

function validarRetenciones(facturas) {

    var info = "{ facturas: '" + $("#searchable").val()
    + "' }";

    $.ajax({
        type: "POST",
        url: "Galicia.aspx/ValidarRetenciones",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d) {
                msg = "Existen facturas sin retenciones asociadas a comercios con retenciones, desdea continuar de todas formas?";
                smoke.confirm(msg, function (e) {
                    if (e) {
                        escribirArchivo();
                    } else {
                        $("#imgLoading").hide();
                        $("#lnkDownload").hide();
                        $("#btnGenerar").attr("disabled", false);
                    }
                }, { ok: "Aceptar", cancel: "Cancelar" });
            }
            else
                escribirArchivo();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnGenerar").attr("disabled", false);
        }
    });

}