﻿function generar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#formGeneracionArchivos").validate();
    $("#btnGenerar").attr("disabled", true);

    if ($('#formGeneracionArchivos').valid()) {
        var desde = $("#txtDesde").val();
        var hasta = $("#txtHasta").val();

        var info = "{ desde: '" + desde
            + "', hasta: '" + hasta
            + "' }";

        $.ajax({
            type: "POST",
            url: "Archivo_Auditar.aspx/EscribirArchivos",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#imgLoading").hide();
                $("#btnGenerar").attr("disabled", false);
                $("#lnkDownload").show();
                $("#lnkDownload2").show();
                $("#lnkDownload").attr("href", "/files/auditar/transacciones.txt");
                $("#lnkDownload2").attr("href", "/files/auditar/transacciones_total.txt");
                var fecha = new Date();
                var dia = fecha.getDate();
                var mes = (fecha.getMonth() + 1)
                var anio = fecha.getFullYear();
                $("#lnkDownload").attr("download", "Archivo_Transacciones_" + dia + mes + anio);
                $("#lnkDownload2").attr("download", "Archivo_Total_Transacciones_" + dia + mes + anio);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#lnkDownload2").hide();
                $("#btnGenerar").attr("disabled", false);
            }
        });
    }
    else {
        $("#btnGenerar").attr("disabled", false);
        $("#imgLoading").hide();
        return false;
    }
}

$(document).ready(function () {

    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtDesde", "txtHasta");

    $('#formGeneracionArchivos').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});