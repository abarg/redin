﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

	var id=0;
	if($("#hfIDUsuario").val()!="")
		id= $("#hfIDUsuario").val();
	
    if ($('#formEdicion').valid()) {
        var info = "{ IDUsuario: " + id
            + ", nombre: '" + $("#txtNombre").val()
            + "', apellido: '" + $("#txtApellido").val()
            + "', usuario: '" + $("#txtUsuario").val()
            + "', email: '" + $("#txtEmail").val()
            + "', pwd: '" + $("#txtPassword").val()
            + "', activo: " + $("#chkActivo").is(':checked')
            + "}";

        $.ajax({
            type: "POST",
            url: "UsuariosEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Usuarios.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}