﻿var oTable = null;

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = $("#hdnIDMarca").val();

    if (marca == "" || marca == null)
        marca = "0"

    var franquicia = $("#hdnIDFranquicia").val();

    if (franquicia == "" || franquicia == null)
        franquicia = "0";

    var info = "{ apellido: '" + $("#txtApellido").val()
        + "', documento: '" + $("#txtDocumento").val()
        + "', email: '" + $("#txtEmail").val()
        + "', cuenta: '" + $("#txtCuenta").val()
        + "', tarjeta: '" + $("#txtTarjeta").val()
        + "', idMarca: " + parseInt(marca)
        + ", idFranquicia: " + parseInt(franquicia)
        + "}";

    $.ajax({
        type: "POST",
        url: "Socios.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function exportarTr() {
    $("#divErrorTr").hide();
    $("#lnkDownloadTr").hide();
    $("#imgLoadingTr").show();
    var btnexportar = $("#btnExportarTr").attr("disabled", true);


    $.ajax({
        type: "POST",
        url: "Socios.aspx/ExportarTr",
        data: "{ idSocio: " + parseInt($("#hdnIDSocio").val()) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divErrorTr").hide();
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").show();
                $("#lnkDownloadTr").attr("href", data.d);
                $("#lnkDownloadTr").attr("download", data.d);
                $("#btnExportarTr").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTr").html(r.Message);
            $("#divErrorTr").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTr").hide();
            $("#lnkDownloadTr").hide();
            $("#btnExportarTr").attr("disabled", false);
        }
    });
}

function configControls() {
    $("#txtDocumento").numeric();
    $("#txtTarjeta").numeric();
    $("#txtCuenta").numeric();
    var idmarca = parseInt($("#hdnIDMarca").val());
    if (idmarca > 0) {
        $(".marca").hide();
    }


    var myColumns = [
        { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
        { field: "IDSocio", title: "ID", width: "50px" },
        { field: "Tarjeta", title: "Nro. de Tarjeta", width: "140px" },
        { field: "Marca", title: "Marca", width: "150px" },
        { field: "Apellido", title: "Socio", width: "170px" },
        { field: "Email", title: "Email", width: "175px" },
        { field: "NroDocumento", title: "Documento", width: "90px" },
        { field: "Ciudad", title: "Ciudad", width: "90px" },
        { field: "Puntos", title: "Puntos", width: "60px" },
        { field: "Credito", title: "Crédito $", width: "90px", format: "{0:c}" },
        { field: "Giftcard", title: "Giftcard $", width: "90px", format: "{0:c}" },
        { field: "Total", title: "Total $", width: "90px", format: "{0:c}" },
        { field: "POS", title: "POS", width: "60px" }
    ];

    if (idmarca > 0) {
        myColumns.splice(3, 1);
    }

    $("#txtApellido, #txtDocumento, #txtTarjeta, #txtCuenta, #txtEmail,#ddlMarcas,#ddlFranquicias").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSocio: { type: "integer" },
                        Tarjeta: { type: "string" },
                        Marca: { type: "string" },
                        //Nombre: { type: "string" },
                        Apellido: { type: "string" },
                        Email: { type: "string" },
                        //FechaNacimiento: { type: "date" },
                        Sexo: { type: "string" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "string" },
                        Telefono: { type: "string" },
                        Celular: { type: "string" },
                        Ciudad: { type: "string" },
                        EmpresaCelular: { type: "string" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" },
                        Observaciones: { type: "string" },
                        NumeroTarjetaSube: { type: "string" },
                        NumeroTarjetaMonedero: { type: "string" },
                        NumeroTarjetaTransporte: { type: "string" },

                        PatenteCoche: { type: "string" },
                        NumeroTransporte: { type: "string" },
                        NroCuenta: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Socios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },

        columns: myColumns

    });




    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Sociose.aspx?IDSocio=" + dataItem.IDSocio;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Socios.aspx/Delete",
                data: "{ id: " + dataItem.IDSocio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        //$('#tableDetalleTr').fixedHeaderTable('destroy');
        $("#titDetalleTr").html("Detalle de Transacciones " + dataItem.Apellido);// + ", " + dataItem.Nombre);


        $.ajax({
            type: "POST",
            url: "Socios.aspx/GetBySocio",
            data: "{ id: " + dataItem.IDSocio + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#bodyDetalleTr").html("");

                if (data != null && data.d.length > 0) {

                    if (oTable != null) {

                        oTable.fnClearTable();
                        oTable.fnDestroy();
                    }

                    oTable = $('#tableDetalleTr').dataTable({
                        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                        "paging": true,
                        "bLengthChange": false,
                        "iDisplayLength": 10,
                        "ordering": false,
                        "bSort": false,
                        "info": false,
                        //"bDestroy": true,
                        "searching": false,
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                        "fnDrawCallback": function () {
                            var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                            if (pageCount == 1) {
                                $('.dataTables_paginate').first().hide();
                            } else {
                                $('.dataTables_paginate').first().show();
                            }
                        }
                    });

                    for (var i = 0; i < data.d.length; i++) {
                        oTable.fnAddData([
                            data.d[i].Fecha,
                            data.d[i].Hora,
                            data.d[i].Operacion,
                            data.d[i].SDS,
                            data.d[i].Comercio,
                            data.d[i].Marca,
                            data.d[i].Tarjeta,
                            data.d[i].NroEstablecimiento,
                            data.d[i].ImporteOriginal,
                            data.d[i].ImporteAhorro,
                            data.d[i].Puntos]
                        );
                    }


                    $("#tableDetalleTr_info").parent().remove();
                    $("#tableDetalleTr").css("width", "100%");
                    $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                    $(".dataTables_paginate").first().parent().addClass("col-sm-12");
                }
                else
                    $("#bodyDetalleTr").html("<tr><td colspan='11'>No hay un detalle disponible</td></tr>");

                $("#hdnIDSocio").val(dataItem.IDSocio);
                $("#divErrorTr").hide();
                $('#modalDetalleTr').modal('show');
                $("#lnkDownloadTr").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });
}

function renderOptions(data) {
    var html = "";
    html = "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/>&nbsp;";
    html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";

    return html;
}

function renderDoc(data) {
    return data.TipoDocumento + " " + data.NroDocumento;
}

function Nuevo() {
    window.location.href = "Socios-alta.aspx";
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#hdnIDFranquicia").val("0");
    $("#hdnIDMarca").val("0");

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var apellido = $("#txtApellido").val();
    if (apellido != "") {
        $filter.push({ field: "Apellido", operator: "contains", value: apellido });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var email = $("#txtEmail").val();
    if (email != "") {
        $filter.push({ field: "Email", operator: "contains", value: email });
    }

    var tarjeta = $("#txtTarjeta").val();
    if (tarjeta != "") {
        $filter.push({ field: "Tarjeta", operator: "contains", value: tarjeta });
    }

    var ciudad = $("#ddlCiudades").val();
    if (ciudad.length > 1) {
        $filter.push({ field: "Ciudad", operator: "contains", value: ciudad });
    }

    var marca = $("#ddlMarcas").val();
    if (marca != null && marca != "") {
        $("#hdnIDMarca").val(marca);
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
    }
    var franquicia = $("#ddlFranquicias").val();
    if (franquicia != null && franquicia != "") {
        $("#hdnIDFranquicia").val(franquicia);
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }


    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    var idFranquicia = $("#hdnIDFranquicia").val();
    if (idFranquicia > 0) {
        $(".franquicias").show();
    }

    $('#formSocio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });


    $("#ddlFranquicias").change(function () {

        $('#ddlMarcas').html('');

        var idFranquicia = 0;
        if ($("#ddlFranquicias").val() != "")
            idFranquicia = parseInt($("#ddlFranquicias").val());

        $.ajax({
            type: "POST",
            url: "Socios.aspx/marcasByFranquicias",
            data: "{ idFranquicia: " + idFranquicia
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#ddlMarcas").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });
});