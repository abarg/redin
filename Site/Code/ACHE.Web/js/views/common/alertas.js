﻿
$(document).ready(function () {
    if (parseInt($("#hdnIDFranquicias").val()) > 0) {
        $(".franquicia").show();
        $(".admin").hide();
    }
    //else {
    //    $(".admin").show();
    //    $(".franquicia").hide();
    //}
});

function detalleFranquicia(id,prioridad) {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Alertas Franquicias");
    $("#headDetalle").html("<tr><th>Entidad</th><th>Cantidad</th><th></th></tr>");
    info = "{idFranquicia: " + parseInt(id) + ", prioridad :'" + prioridad + "'}";
    $.ajax({
        type: "POST",
        url: "alertas.aspx/obtenerDetalleFranquicia",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}

function alertasByEntidad(idFranquicia, prioridad, entidad) {
    $("#bodyDetalle").html();

    $("#titDetalle2").html("Alertas " + entidad);
    $("#headDetalle2").html("<tr><th>Entidad</th><th>Tipo</th><th>Nombre</th><th>Descripcion</th><th>Cantidad</th><th></th></tr>");
    info = "{idFranquicia: " + parseInt(idFranquicia) + ", prioridad :'" + prioridad + "', entidad :'" + entidad + "'}";
    $.ajax({
        type: "POST",
        url: "alertas.aspx/obtenerAlertasByEntidad",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle2").html(data.d);
            }
            $('#modalDetalle2').modal('show');
            $('#modalDetalle').modal('hide');
        }
    });
}

function detalleAlerta(id) {
    $('#modalDetalle2').modal('hide');
    $("#bodyDetalle3").html();

    $("#titDetalle3").html("Detalle de Alertas");
    $("#headDetalle3").html("<tr><th>Fecha</th><th>Entidad</th><th>Origen</th><th>Operacion</th><th>Importe</th><th>Tarjeta</th><th>Socio</th></tr>");
    $.ajax({
        type: "POST",
        url: "alertas.aspx/obtenerDetalleAlerta",
        data: "{idAlerta: " + parseInt(id) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle3").html(data.d);
            }
            $('#modalDetalle3').modal('show');
        }
    });
}


