﻿
function Nuevo() {
    window.location.href = "Terminalese.aspx";
}

function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = 0;
    if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "") {
        marca = $("#ddlMarcas").val();
    }

    var franquicia = 0
    if ($("#ddlFranquicias").val() != null && $("#ddlFranquicias").val() != "") {
        franquicia = $("#ddlFranquicias").val();
    }

    //var descuento = $("#txtDescuento").val();
    //if (descuento != "" && descuento != null) {
    //    $filter.push({ field: "Descuento", operator: "contains", value: descuento });
    //}

    var info = "{ SDS: '" + $("#txtSDS").val()
            + "', NombreFantasia: '" + $("#txtNombre").val()
            + "', RazonSocial: '" + $("#txtRazonSocial").val()
            + "', NroDocumento: '" + $("#txtDocumento").val()
            + "', NumEst: '" + $("#txtEstablecimiento").val()
            //+ "', Descuento: '" + descuento
            + "', POSTerminal: '" + $("#txtPosTerminal").val()
            + "', idMarca: " + parseInt(marca)
            + ", idFranquicia: " + parseInt(franquicia)
            //+ "', ConCobro: '" + $("#chkCobroRed").is(':checked')
            + "}";

    $.ajax({
        type: "POST",
        url: "Terminales.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

$(document).ready(function () {
    var idFranquicia = $("#hdnIDFranquicias").val();
    if (idFranquicia > 0) {
        $("#ddlFranquicias").attr("disabled", true);

    }

    configControls();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var SDS = $("#txtSDS").val();
    if (SDS != "") {
        $filter.push({ field: "SDS", operator: "contains", value: SDS });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var RazonSocial = $("#txtRazonSocial").val();
    if (RazonSocial != "") {
        $filter.push({ field: "RazonSocial", operator: "contains", value: RazonSocial });
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "NombreFantasia", operator: "contains", value: nombre });
    }

    var Establecimiento = $("#txtEstablecimiento").val();
    if (Establecimiento != "") {
        $filter.push({ field: "NumEst", operator: "contains", value: Establecimiento });
    }

    var PosTerminal = $("#txtPosTerminal").val();
    if (PosTerminal != "") {
        $filter.push({ field: "PosTerminal", operator: "contains", value: PosTerminal });
    }

    var descuento = $("#txtDescuento").val();
    if (descuento != "" && descuento != null) {
        $filter.push({ field: "Descuento", operator: "contains", value: descuento });
    }

    //var cp = $("#txtCp").val();
    //if (cp != "") {
    //    $filter.push({ field: "CodigoPostal", operator: "equal", value: cp });
    //}

    var marca = $("#ddlMarcas").val();
    if (marca != "") {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
    }

    var franquicia = $("#ddlFranquicias").val();
    if (franquicia != "") {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }

    var cbu = $("#txtCBU").val();
    if (cbu != "" && cbu != null) {
        $filter.push({ field: "CBU", operator: "contains", value: cbu });
    }
    grid.dataSource.filter($filter);
}


function configControls() {
    $("#txtDocumento").numeric();
    $("#txtSDS").numeric();
    $("#txtCBU").numeric();

    $("#txtDescuento").numeric();

    $("#txtSDS, #txtDocumento, #txtRazonSocial, #txtEstablecimiento, #txtPosTerminal, #txtNombre, #txtDealer, #txtDescuento,#txtCBU").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });


}

function configGrid() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDComercio: { type: "integer" },
                        IDTerminal: { type: "integer" },
                        SDS: { type: "integer" },
                        NombreFantasia: { type: "string" },
                        RazonSocial: { type: "string" },
                        //TipoDocumento: { type: "string" },
                        NroDocumento: { type: "integer" },
                        //Cargo : { type: "string" },
                        //Actividad : { type: "string" },
                        //CondicionIva : { type: "string" },
                        //Web : { type: "string" },
                        Email: { type: "string" },
                        //FechaAlta : { type: "date" },
                        //POSTipo : { type: "string" },
                        POSSistema: { type: "string" },
                        POSTerminal: { type: "integer" },
                        NumEst: { type: "string" },
                        IDMarca: { type: "integer" },
                        IDFranquicia: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Terminales.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDTerminal", title: "ID", width: "50px" },
            //{ field: "POSTipo", title: "POS Tipo", width: "100px" },
            { field: "POSSistema", title: "POS Sistema", width: "100px" },
            { field: "POSTerminal", title: "POS Terminal", width: "100px" },
            //{ field: "POSEstablecimiento", title: "POS Est.", width: "100px" },
            { field: "NumEst", title: "Nro de Est.", width: "150px" },
            { field: "SDS", title: "SDS", width: "75px" },
            { field: "NombreFantasia", title: "Nombre Fantasía", width: "150px" },
            { field: "RazonSocial", title: "Razón social", width: "150px" },
            //{ field: "TipoDocumento", title: "Tipo Doc.", width: "75px" },
            { field: "NroDocumento", title: "Nro. Doc", width: "100px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Terminalese.aspx?IDTerminal=" + dataItem.IDTerminal;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Terminales.aspx/Delete",
                data: "{ id: " + dataItem.IDTerminal + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });
    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var idComercio = dataItem.IDComercio;
        var info = "{ IDComercio: " + idComercio
            + "}";
        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginComercioAutomaticoAdmin",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/comercios/home.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });
}