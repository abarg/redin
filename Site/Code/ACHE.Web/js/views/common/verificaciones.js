﻿function UploadCompleted(sender, args) {
    try {
        var fileExtension = args.get_fileName();
        fileExtension = fileExtension.toLowerCase();


        var jpg = fileExtension.indexOf('.jpg');
        if ((jpg > 0)) {
            $("#divError").hide();
        }
        else {
            $("#divError").html("La extensión del archivo debe ser jpg");
            $("#divError").show();
            $("#divOK").hide();
        }
    }
    catch (e) {
        $("#divError").html(e);
        $("#divError").show();
    }
}

function UploadCompletedTxt(sender, args) {
    try {
        var fileExtension = args.get_fileName();
        var jpg = fileExtension.toLowerCase().indexOf('.jpg');
        if (jpg > 0) {
            $("#divError").hide();
        }
        else {
            $("#divError").html("La extensión del archivo debe ser .jpg");
            $("#divError").show();
            $("#divOK").hide();
        }
    }
    catch (e) {
        $("#divError").html(e);
        $("#divError").show();
    }
}



function UploadError(sender) {
    $("#divError").html("Error al actualizar los datos");
    $("#divError").show();
    $("#divOK").hide();
}





function configControls() {
    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más comercios"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
    $(".chosen").chosen();
}


function cargarProvincias() {
    var idFranquicia = parseInt($("#cmbFranquicia").val());

    if (idFranquicia > 0) 
        var info = "{ idFranquicia: " + idFranquicia + " }";
    else
        var info = "{ idFranquicia: " +  parseInt($("#hdnIDFranquicia").val()) + " }";

    $.ajax({
        type: "POST",
        url: "verificacionesPOS.aspx/CargarProvincias",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#cmbProvincia").empty();
                $("#cmbProvincia").append("<option value=''></option>");
                $("#cmbZona").empty();
                $("#cmbZona").append("<option value=''></option>");
                $("#cmbCiudad").empty();
                $("#cmbCiudad").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#cmbProvincia").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                $('#cmbProvincia, #cmbCiudad, #cmbZona').trigger("liszt:updated");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
        }
    });
    
  
}

function cargarCiudades() {
    var idProvincia = parseInt($("#cmbProvincia").val());
    if (idProvincia > 0) {
        var info = "{ idProvincia: " + idProvincia + " }";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/CargarCiudades",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null && data.d.length > 0) {
                    $("#cmbCiudad").empty();
                    $("#cmbCiudad").append("<option value=''></option>");
                    $("#cmbZona").empty();
                    $("#cmbZona").append("<option value=''></option>");
                    for (var i = 0; i < data.d.length; i++)
                        $("#cmbCiudad").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                    $('#cmbCiudad, #cmbZona').trigger("liszt:updated");
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
            }
        });
    }
}

function cargarZonas() {
    var idProvincia = parseInt($("#cmbProvincia").val());
    var idCiudad = parseInt($("#cmbCiudad").val());
    if (idProvincia > 0 && idCiudad > 0) {
        var info = "{ idProvincia: " + idProvincia + ", idCiudad: " + idCiudad + " }";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/CargarZonas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#cmbZona").empty();
                $("#cmbZona").append("<option value=''></option>");
                if (data.d != null && data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++)
                        $("#cmbZona").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                }
                $('#cmbZona').trigger("liszt:updated");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
            }
        });
    }
}

function validar() {
    var isValid = true;
    var msg = "";

    if ($("#cmbProvincia").val() == "" || $("#cmbProvincia").val() == "0" || $("#cmbProvincia").val() == null) {
        isValid = false;
        $("#spnProvincia").show();
    }
    else {
        guardarProvincia();
        $("#spnProvincia").hide();
    }

    if ($("#cmbCiudad").val() == "" || $("#cmbCiudad").val() == "0" || $("#cmbCiudad").val() == null) {
        isValid = false;
        $("#spnCiudad").show();
    }
    else {
        guardarCiudad();
        $("#spnCiudad").hide();
    }

    if ($("#cmbZona").val() == "" || $("#cmbZona").val() == "0" || $("#cmbZona").val() == null) {
        //isValid = false;
        //$("#spnZona").show();
    }
    else {
        guardarZona();
        //$("#spnZona").hide();
    }
    $("#hdnEstado").val($("#cmbEstado").val());
    if (($("#cmbFranquicia").val() == "" || $("#cmbFranquicia").val() == "0" || $("#cmbFranquicia").val() == null) && ($("#hdnIDFranquicia").val() == "0")) {//valida solo para el perfinl admin
        isValid = false;
        $("#spnFranquicia").show();
    }
    else {
        guardarFranquicia();
        $("#spnFranquicia").hide();
    }

    return isValid;
}


function generar() {
    var mapRoute = "";
    if ($('#insertMap').is(':checked') === true)
         mapRoute = this.buscar();

    $("#divError").hide();
    $("#imgLoading").show();
    $("#btnGenerar").attr("disabled", true);
    $("#btnDescargar").hide();
    if ($("#searchable").val() != "") {
        var info = "{ comercios: '" + $("#searchable").val()
            + "', zona: '" + $("#hdnZona").val()
            + "', insertMap: '" + $('#insertMap').is(':checked')
            + "', mapRoute: '" + mapRoute
            + "'}";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/GenerarPlanillas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null && data.d != "") {
                    $("#imgLoading").hide();
                    $("#btnDescargar").show();
                    $("#btnDescargar").show();
                    $("#btnDescargar").attr("href", data.d);
                    $("#btnDescargar").attr("download", data.d);
                    $("#btnGenerar").attr("disabled", false);
                }
                else {
                    $("#imgLoading").hide();
                    $("#btnDescargar").hide();
                    $("#btnGenerar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#imgLoading").hide();
                $("#btnDescargar").hide();
                $("#btnGenerar").attr("disabled", false);
            }
        });
    }
    else {
        $("#divError").html("Debe seleccionar al menos un comercio");
        $("#divError").show();
    }
}

function guardarProvincia() {
    $("#hdnIDProvincia").val($("#cmbProvincia").val());
}

function guardarCiudad() {
    $("#hdnIDCiudad").val($("#cmbCiudad").val());
}

function guardarFranquicia() {
    if (!($("#cmbFranquicia").val() == "" || $("#cmbFranquicia").val() == "0" || $("#cmbFranquicia").val() == null))
         $("#hdnIDFranquicia").val($("#cmbFranquicia").val());
}

function guardarZona() {
    $("#hdnIDZona").val($("#cmbZona").val());
    $("#hdnZona").val($("#cmbZona option:selected").text());
}

$(document).ready(function () {
    configControls();
    
});


//MAPA
var map = null;
var gmarkers = [];
function clearMarkers() {
    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }

    gmarkers = [];
    $("#litResultados").html("");
}


function buscar() {

    $("#divError").hide();

    var baseUrl = "https://maps.googleapis.com/maps/api/staticmap?";


    if ($("#searchable").val() != "") {
        var info = "{ idsComercios: '" + $("#searchable").val() + "'}";
        clearMarkers();
        $.ajax({
            type: "POST",
            url: "VerificacionesPOS.aspx/GetMarkers",
            data: info,
            async: !1,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {



                    for (var i = 0; i < data.d.length; i++) {


                        var name = data.d[i].Nombre;
                        var address = data.d[i].Direccion;
                        var lat = data.d[i].Lat;
                        var lng = data.d[i].Lng;                        var ficha = data.d[i].Ficha;                        var foto = data.d[i].Foto;

                        if (i == 0) {
                            if (lat != null && lng != null) {
                                baseUrl += "center=" + lat.trim() + "," + lng.trim();
                            }
                            baseUrl += "&zoom=13&size=650x650";
                            baseUrl += "&markers=size:tiny%7Ccolor:0xFFFF00%7Clabel:C%7C" + lat.trim() + "," + lng.trim();
                        }
                        else {
                            if(lat.trim() != "" && lng.trim() != "")
                             baseUrl += "|" + lat + "," + lng;
                        }
                        if (i == data.d.length - 1) {
                            baseUrl += "&key=AIzaSyACDbJVkiCBTZyheMz-e5WyZArVzc_eBAw";
                        }     

                        
                    
                    }

                    //return baseUrl;
                    window.open(baseUrl, '_blank');
                   
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
            }
        });
    }
    else {
        $("#divError").html("Debe seleccionar al menos un comercio");
        $("#divError").show();
    }

    window.open(baseUrl, '_blank');

    //return baseUrl;

}

var globalString;

function initialize() {
    var point = new google.maps.LatLng(-37.9604967, -57.563621799999964);
    // create the map
    var myOptions = {
        zoom: 14,
        center: point,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    map.setCenter(point);
    //google.maps.event.addListener(map, 'click', function () {
    //    infoWindow.close();
    //});
}

function markerClick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
}
function showMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(true);
        }
    }
}

function hideMarkers(categoria) {
    for (var i = 0; i < gmarkers.length; i++) {
        if (gmarkers[i].mycategory == categoria) {
            gmarkers[i].setVisible(false);
        }
    }
}
