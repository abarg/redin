﻿/**FOTO UPLOAD**/
var oTable = null;

function deleteUpload(file, tipo) {

    var info = "{ id: " + parseInt($("#hfIDSocio").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "Sociose.aspx/eliminar" + tipo,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#div" + tipo).hide();
            $("#img" + tipo).attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function guardarFechaVencimiento() {
    $('#formEdicion').validate();
    if ($('#formEdicion').valid()) {

        var info = "{ id: " + parseInt($("#hdnID").val())
          + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
          + "', numero: '" + $("#hdnNumero").val()
          + "'}";

        $.ajax({
            type: "POST",
            url: "sociose.aspx/guardarFechaVenc",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divError").hide();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {

    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

/**FIN LOGO**/
function irAlResponsable() {
    $.ajax({
        type: "POST",
        url: "Sociose.aspx/GetIDResponsable",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            window.location.href = "Sociose.aspx?IDSocio=" + data.d;
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);

        }
    });
}

function exportarTr() {
    $("#divErrorTr").hide();
    $("#lnkDownloadTr").hide();
    $("#imgLoadingTr").show();
    $("#btnExportarTr").attr("disabled", true);
   
    $.ajax({
        type: "POST",
        url: "Socios.aspx/ExportarTr",
        data: "{ idSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divErrorTr").hide();
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").show();
                $("#lnkDownloadTr").attr("href", data.d);
                $("#lnkDownloadTr").attr("download", data.d);
                $("#btnExportarTr").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTr").html(r.Message);
            $("#divErrorTr").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTr").hide();
            $("#lnkDownloadTr").hide();
            $("#btnExportarTr").attr("disabled", false);
        }
    });
}

function abrirPopUpTR() {
    $("#titDetalleTr").html("Detalle de Transacciones " + $("#txtApellido").val() + ", " + $("#txtNombre").val());// + ", " + dataItem.Nombre);

    $.ajax({
        type: "POST",
        url: "Socios.aspx/GetBySocio",
        data: "{ id: " + parseInt($("#hfIDSocio").val()) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#bodyDetalleTr").html("");

            if (data != null && data.d.length > 0) {

                if (oTable != null) {

                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalleTr').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].Fecha,
                        data.d[i].Hora,
                        data.d[i].Operacion,
                        data.d[i].SDS,
                        data.d[i].Comercio,
                        data.d[i].Marca,
                        data.d[i].Tarjeta,
                        data.d[i].NroEstablecimiento,
                        data.d[i].ImporteOriginal,
                        data.d[i].ImporteAhorro,
                        data.d[i].Puntos]
                      );
                }

                $("#tableDetalleTr_info").parent().remove();
                $("#tableDetalleTr").css("width", "100%");
                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else
                $("#bodyDetalleTr").html("<tr><td colspan='11'>No hay un detalle disponible</td></tr>");

           // $('#modalDetalleTr').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ":" + thrownError);
        }
    });
    $("#divErrorTr").hide();
    $("#lnkDownloadTr").hide();
    $("#modalDetalleTr").modal("show");
}

function configControls() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDTarjeta: { type: "integer" },
                        Socio: { type: "string" },
                        Marca: { type: "string" },
                        Numero: { type: "string" },
                        Estado: { type: "string" },
                        IDSocio: { type: "integer" },
                        MotivoBaja: { type: "string" },
                        FechaBaja: { type: "date" },
                        FechaAsignacion: { type: "date" },
                        FechaVencimiento: { type: "date" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" },
                        Marca: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Sociose.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idSocio: parseInt($("#hfIDSocio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "Marca", title: "Marca", width: "100px" },
            { title: "", template: "#= renderOptions(data) #", width: "30px" },
            { field: "Socio", title: "Socio", width: "120px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "Numero", title: "Numero", width: "120px" },
            { field: "FechaAsignacion", title: "Fecha Asig", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "FechaVencimiento", title: "Fecha Venc", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "Estado", title: "Estado", width: "80px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "Credito", title: "Crédito $", width: "80px", format: "{0:c}" },
            { field: "Giftcard", title: "Giftcard $", width: "80px", format: "{0:c}" },
            { field: "Total", title: "Total $", width: "80px", format: "{0:c}" },
            { field: "POS", title: "POS", width: "80px" },
            { field: "FechaBaja", title: "Fecha de Baja", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "MotivoBaja", title: "Motivo Baja", width: "120px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var fecha = dataItem.FechaVencimiento;
        day = fecha.getDate(),
        month = fecha.getMonth() + 1,
        year = fecha.getFullYear();
        $("#txtFechaVencimiento").val(day + "/" + month + "/" + year);
        $("#hdnID").val(dataItem.IDTarjeta);
        $("#hdnNumero").val(dataItem.Numero);
        $("#modalCargarFechaVencimiento").modal("show");
    });

    //GRILLA ASOCIADOS
    var myColumns = [
        { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "12px" },
        { field: "IDSocio", title: "ID", width: "50px" },
        { field: "NroDocumento", title: "Nro Documento", width: "100px" },
        { field: "Apellido", title: "Apellido", template: "<a class='apellidoColulmn'>#=Apellido#</a>", width: "100px" }
    ];
    var Responsable = parseInt($("#hdnResponsable").val());
    if (Responsable == 1) {
        myColumns.splice(0, 1)
    }

    $("#gridAsociados").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSocio: { type: "integer" },
                        NroDocumento: { type: "string" },
                        Apellido: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Sociose.aspx/GetListaGrillaAsociados", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idSocio: parseInt($("#hfIDSocio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: myColumns
    });

    $("#gridAsociados").delegate(".apellidoColulmn", "click", function (e) {
        var grid = $("#gridAsociados").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Sociose.aspx?IDSocio=" + dataItem.IDSocio;
    });

    $("#gridAsociados").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridAsociados").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Sociose.aspx/Desasociar",
                data: "{ id: " + dataItem.IDSocio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                    filterAsociados();
                    $("#divAsociadoOk").html("Los datos se actualizaron correctamente");
                    $("#divAsociadoOk").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);

                }
            });
        }
    });


}

function loadActividades() {
    //GRILLA Actividades
    var selectedIds = {};

    var myColumns = [
        { field: "select", title: "Inscripto", template: '<input type=\'checkbox\' />', sortable: false, width: 32 },
        { field: "Actividad", title: "Beneficio", width: "100px" },
        { field: "Nivel", title: "Nivel", width: "100px" },
        { field: "Horario", title: "Horario", width: "100px" },
        { field: "Sede", title: "Sede", width: "100px" },
        { field: "IDHorarioIDSede", title: "ID", width: "30px", attributes: { class: 'three' } },

    ];

    var ctlGrid = $("#grillaActividades");
    ctlGrid.kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            id: "IDHorarioIDSede",
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    id: "IDHorarioIDSede",
                    fields: {
                        IDHorarioIDSede: { type: "string" },
                        select: { type: "string", editable: false },
                        Actividad: { type: "string" },
                        Nivel: { type: "string" },
                        Horario: { type: "string" },
                        Sede: { type: "string" },
                    }
                }
            },
            pageSize: 5,
            batch: true,
            transport: {
                read: {
                    url: "Sociose.aspx/GetListaActividades",
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        return JSON.stringify({ products: data.models })
                    } else {

                        data = $.extend({ sort: null, filter: null, idSocio: parseInt($("#hfIDSocio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },

        selectable: "multiple",
        sortable: {
            mode: 'single',
            allowUnsort: false
        },
        pageable: true,
        columns: myColumns,
        dataBound: function () {
            var grid = this;
            //handle checkbox change
            grid.table.find("tr").find("td:first input")
                .change(function (e) {
                    var checkbox = $(this);
                    var selected = grid.table.find("tr").find("td:first input:checked").closest("tr");

                    var selectedID = selected.children('td.three').text();


                    grid.clearSelection();

                    //persist selection per page
                    var ids = selectedIds[grid.dataSource.page()] = [];

                    if (selected.length) {
                        grid.select(selected);
                        selected.each(function (idx, item) {
                            ids.push($(item).children('td.three').text());
                        });
                    }



                })
                .end()
                .mousedown(function (e) {
                    e.stopPropagation();
                })

            //select persisted rows
            var selected = $();
            var ids = selectedIds[grid.dataSource.page()] || [];


            for (var idx = 0, length = ids.length; idx < length; idx++) {

                selected = selected.add(grid.table.find("tr[data-id=" + ids[idx] + "]"));
            }

            console.log(selectedIds);

            selected
                .find("td:first input")
                .attr("checked", true)
                .trigger("change");


        }
    });
}

function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
 //   $filter.push({ field: "IDSocio", operator: "equal", value: parseInt($("#hfIDSocio").val()) });

    grid.dataSource.filter($filter);
}

function filterAsociados() {
    var grid = $("#gridAsociados").data("kendoGrid");

    var $filter = new Array();
    grid.dataSource.filter($filter);
}

function asociarTarjeta() {
    $("#divErrorTarjetas").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#ddlTarjetas").val() == "") {
        $("#divErrorTarjetas").html("Seleccione una tarjeta");
        $("#divErrorTarjetas").show();
    }
    else {
        var info = "{ IDSocio: " + parseInt($("#hfIDSocio").val())
           + ", Tarjeta: '" + $("#ddlTarjetas").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "Sociose.aspx/asociarTarjeta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtNuevaTarjeta").val("");
                $("#divResultado1").hide();
                $("#divResultado2").hide();
                filter();
                filterAsociados();
                var Responsable = parseInt($("#hdnResponsable").val());
                var idMarca = $("#hdnIDMarca").val();
                if (Responsable == -1 && idMarca == 0) {
                    $("#hdnResponsable").val("0")
                }
                mostrarGrupoFamiliar();

            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function buscarTarjetas() {
    $("#divErrorTarjetas").hide();
    $("#divOk").hide();
    $("#divError").hide();
    var valid = false;
    var idFranquicia = $("#hdnIDFranquicia").val();
    if (idFranquicia > 0) {
        $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");
        var info = "{ idMarca: " +0
           + ", tarjeta: '" + $("#txtNuevaTarjeta").val()
           + "'}";
        valid = true;
    } else if ($("#txtNuevaTarjeta").val() == "" ) {
        $("#divErrorTarjetas").html("Ingrese una terminación de una tarjeta");
        $("#divErrorTarjetas").show();
    }
    if(idFranquicia==0){
        if ($("#ddlMarcas").val() == "" ) {
            $("#divErrorTarjetas").html("Selecciona una marca");
            $("#divErrorTarjetas").show();
        }
        else if ($("#txtNuevaTarjeta").val() == "" ) {
            $("#divErrorTarjetas").html("Ingrese una terminación de una tarjeta");
            $("#divErrorTarjetas").show();
        }
        else {
            $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");

            var info = "{ idMarca: " + parseInt($("#ddlMarcas").val())
               + ", tarjeta: '" + $("#txtNuevaTarjeta").val()
               + "'}";
            valid = true;

        }
    }
    if(valid==true){
        $.ajax({
            type: "POST",
            url: "Sociose.aspx/buscarTarjetas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTarjetas"));
                    }

                    if (data.d.length > 0) {
                        $("#divResultado1").show();
                        if ($("#hdnIDMultimarca").val()>0)
                            $("#linkAnular").hide();
                            
                        $("#divResultado2").show();
                    }
                    else {
                        $("#divErrorTarjetas").html("No se encontraron resultados");
                        $("#divErrorTarjetas").show();
                    }
                }
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTarjetas").html(r.Message);
                $("#divErrorTarjetas").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    
    }
}

function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    return html;
}

function grabar() {
  
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formSocio').valid() && $("#ddlProvincia").val()!="") {
        var profesion = 0;
        if ($("#ddlProfesion").val() > 0)
            profesion = $("#ddlProfesion").val();
        var rdbSexo = "I";
        if ($("#rdbFem")[0].checked == true)
            rdbSexo = "F";
        else if ($("#rdbMas")[0].checked == true)
            rdbSexo = "M";
        var hobbies = "";
        if ($("#searchableHobbies").val() != null)
            hobbies = $("#searchableHobbies").val();

        var uniqueActividades = "";

        if ($("#searchableActividades").val() != null) {
                actividades = $("#searchableActividades").val();

                console.log(actividades);

       
                var uniqueActividades = [];
                $.each(actividades, function (i, el) {
                    if ($.inArray(el, uniqueActividades) === -1) uniqueActividades.push(el);
                });

        }
        var info = "{ IDSocio: '" + $("#hfIDSocio").val()
            + "', NroCuenta: '" + $("#txtNroCuenta").val()
            + "', Nombre: '" + $("#txtNombre").val()
            + "', Apellido: '" + $("#txtApellido").val()
            + "', Email: '" + $("#txtEmail").val()
            + "', Dia: '" + $("#ddlDia option:selected").html()
            + "', Mes: '" + $("#ddlMes option:selected").val()
            + "', Anio: '" + $("#ddlAnio option:selected").html()
            + "', Sexo: '" + rdbSexo
            + "', TipoDoc: '" + $("#ddlTipoDoc option:selected").text()
            + "', NroDoc: '" + $("#txtNroDoc").val()
            + "', Telefono: '" + $("#txtTelefono").val()
            + "', Celular: '" + $("#txtCelular").val()
            + "', EmpresaCelular: '" + $("#ddlEmpresaCelular").val()
            + "', Profesion: '" + profesion
            + "', Observaciones: '" + $("#txtObservaciones").val()
            + "', NumeroSube: '" + $("#txtNumSUBE").val()
            + "', NumeroMonedero: '" + $("#txtNumMonedero").val()
            + "', NumeroTransporte: '" + $("#txtNumTarjTransporte").val()
            + "', Patente: '" + $("#txtPatente").val()
            + "', Twitter: '" + $("#txttwitter").val()
            + "', Facebook: '" + $("#txtFacebook").val()
            + "', GrabarDomicilio: '" + $("#hfGrabarDomicilio").val()
            + "', Pais: '" + $("#ddlPais option:selected").text()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
            + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
            + "', Fax: '" + $("#txtFax").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "', Lat: '" + $("#txtLatitud").val()
            + "', Long: '" + $("#txtLongitud").val()
            + "', CostoTransaccionalConDescuento: '" + $("#txtCostoTransaccionalConDescuento").val()
            + "', CostoTransaccionalSoloPuntos: '" + $("#txtCostoTransaccionalSoloPuntos").val()
            + "', CostoTransaccionalCanje: '" + $("#txtCostoTransaccionalCanje").val()
            + "', CostoSeguro: '" + $("#txtCostoSeguro").val()
            + "', CostoSMS: '" + $("#txtCostoSMS").val()
            + "', Banco: '" + $("#txtBanco").val()
            + "', TipoCuenta: '" + $("#ddlTipoCuenta").val()
            + "', NroCuentaPLUSIN: '" + $("#txtNroCuentaPLUSIN").val()
            + "', CBU: '" + $("#txtCBU").val()
            + "', EmailPlusIn: '" + $("#txtEmailPlusIn").val()
             + "', fechaCaducidad: '" + $("#txtFechaCaducidad").val()
              + "', fechaTopeCanje: '" + $("#txtFechaTopeCanje").val()
            + "', Pwd: '" + $("#txtPwd").val()
            + "', ImporteTope: '" + $("#txtImporteTope").val()
            + "', Hobbies: '" + hobbies
            + "', Actividades: '" + uniqueActividades
            + "'}";

        $.ajax({
            type: "POST",
            url: "Sociose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#hfGrabarDomicilio").val("1");
                $("#hfVerTarjetas").val("1");

                if ($("#hfIDSocio").val() == "0") {//data.d);
                    window.location.href = "Sociose.aspx?IDSocio=" + data.d;
                }
                else {
                    toggleTabs();
                    filter();
                }
                
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function cargarHobbies() {

    $.ajax({
        type: "POST",
        url: "Sociose.aspx/getHobbies",
        data: "{ idsocio: " + $("#hfIDSocio").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            var values = new Array();
            $(data.d).each(function (index) {
                $('#searchableHobbies option[value="' + data.d[index] + '"]').attr('selected', true)
                console.log(data.d[index])
            });

            $('#searchableHobbies').multiSelect(values);

      


        },
        error: function (response) {

        }
    });
}

function cargarActividades() {
    $.ajax({
        type: "POST",
        url: "Sociose.aspx/getAsignaciones",
        data: "{ idsocio: " + $("#hfIDSocio").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            var values = new Array();
            $(data.d).each(function (index) {
                console.log(data.d[index].id)
                $('#searchableActividades option[value="' + data.d[index].id + '"]').attr('selected', true)
            });

            $('#searchableActividades').multiSelect(values);

        },
        error: function (response) {

        }
    });
}

function asociar() {
    var info = "{ idSocioEncontrado: " + parseInt($("#hdnIDSocio").val())
        + ", idSocio: " +parseInt($("#hfIDSocio").val())+"}";
    $.ajax({
        type: "POST",
        url: "Sociose.aspx/Asociar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            filter();
            filterAsociados();
            $("#divSocioEncontrado").hide();
            $("#divAsociadoOk").html("Los datos se actualizaron correctamente");
            $("#divAsociadoOk").show();

        },
        error: function (response) {
           
        }
    });
}

function buscar() {
    $("#divAsociadoOk").hide();
    $("#divSocioEncontrado").hide();
    $("#divSocioNoEncontrado").hide();
    $("#divError").hide();
    $("#hdnIDSocio").val("0");

    if ($("#txtDoc").val() != "") {
        $("#imgLoading").show();
        $("#btnBuscar").attr("disabled", true);
       
        var info = "{ documento: '" + $("#txtDoc").val()
            + "', idSocio: " + parseInt($("#hfIDSocio").val()) + "}";

            $.ajax({
                type: "POST",
                url: "Sociose.aspx/Buscar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != null) {
                        $("#divError").hide();
                        $("#imgLoading").hide();
                        $("#btnBuscar").attr("disabled", false);
                        $("#lblNombre").html(data.d.Nombre);
                        $("#lblApellido").html(data.d.Apellido);
                        $("#lblSexo").html(data.d.Sexo);
                        $("#lblEmail").html(data.d.Email);
                        $("#lblFechaNac").html(data.d.NroCuenta);
                        $("#hdnIDSocio").val(data.d.IDSocio);
                        $("#divSocioEncontrado").show();
                    }
                    else {
                        $("#imgLoading").hide();
                        $("#btnBuscar").attr("disabled", false);
                        $("#divSocioNoEncontrado").show();
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoading").hide();
                    $("#btnBuscar").attr("disabled", false);
                }
            });
    }
    else {
        return false;
    }
}

function mostrarGrupoFamiliar() {
     //Responsable=-1 alta
    //Responsable =0 no tiene ningun responsable asi que puede asociar tarjetas a el 
    //Responsable=1 tiene un responsable asi que no puede asociar tarjetas a el
    var idMarca = $("#hdnIDMarca").val();
    var Responsable = parseInt($("#hdnResponsable").val());

    if (idMarca == 0) {
        if (Responsable == 0) {
            $(".grupo").show();
            $(".nombreResponsable").hide();
            $(".asociado").show();
        } else if (Responsable == 1) {
            $(".grupo").show();
            $(".asociado").hide();
            $(".nombreResponsable").show();
        } else if (Responsable == -1) {
            $(".grupo").hide();
        }

    } else {
        if ( Responsable == 1) {
            $(".grupo").show();
            $(".asociado").hide();
            $(".nombreResponsable").show();
        }
        else if ( Responsable == 0) {
            $(".grupo").show();
            $(".asociado").show();
            $(".admin-franq").hide();
            $(".admin").hide();
            $(".nombreResponsable").hide();
            $("#divAsociarSocios").hide();
        }
        else if (Responsable == -1) {
            $(".grupo").hide();
        }
    }

   

}

function toggleTabs() {
    if ($("#hfIDSocio").val() != "0") {
        $($("#Tabs ul li a")[2]).removeClass("hide");
        $($("#Tabs ul li a")[7]).removeClass("hide");

        $("#divUploadFoto").show();
        $($("#Tabs ul li a")[6]).removeClass("hide");
        setTimeout("ObtenerPromedioTicket()", 1000);
        setTimeout("ObtenerTotalTasaUsoMensual()", 1000);
        setTimeout("ObtenerTotalTR()", 1000);
        setTimeout("ObtenerTotalCanjes()", 1000);
        setTimeout("ObtenerImporteAhorro()", 1000);
        setTimeout("ObtenerImportePagado()", 1000);
        setTimeout("ObtenerSaldoActual()", 1000);
        setTimeout("ObtenerPuntosActuales()", 1000);
        setTimeout("ObtenerCantTR()", 1000);
        setTimeout("ObtenerCantComerciosUnicos()", 1000);
    }
    else {
        $($("#Tabs ul li a")[2]).addClass("hide");
        $($("#Tabs ul li a")[6]).addClass("hide");
        $($("#Tabs ul li a")[7]).addClass("hide");
    }

}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[2]).click(function () {
        $("#divBotones").hide();
    });
    $($("#Tabs ul li a")[3]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[4]).click(function () {
        $("#divBotones").hide();
    });
    $($("#Tabs ul li a")[5]).click(function () {
        $("#divBotones").show();
    });
    $($("#Tabs ul li a")[6]).click(function () {
        $("#divBotones").hide();
    });
    $($("#Tabs ul li a")[7]).click(function () {
        $("#divBotones").hide();
    });
}


$(document).ready(function () {
    var idMarca = parseInt($("#hdnIDMarca").val());
    if (idMarca > 0) {
        $(".admin-franq").hide();
        $(".admin").hide();
    }

    var idFranquicia = parseInt($("#hdnIDFranquicia").val());
    if (idFranquicia > 0) {
        $(".admin").hide();
    }


    if (parseInt($("#hfIDSocio").val()) == 0) {
        provinciasByPaises();
    }

    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        provinciasByPaises();        
    });

   // $("#ddlTarjetas2").change(function () {
  //      puntosPorTarjeta($("#ddlTarjetas2").val());
  //  });

    mostrarGrupoFamiliar();
  
    configControls();
    toggleTabs();
    toggleButtons();
    configDatePicker();
    configFechasDesdeHasta2("txtFechaTopeCanje", "txtFechaCaducidad");
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtNroDoc, #txtNuevaTarjeta, #txtNroCuenta, #txtDoc, #txtCBU, #txtFormaPago_CBU_Rep,#txtImporteTope,#txtPuntos").numeric();

    $("#txtCostoTransaccionalConDescuento,#txtCostoTransaccionalSoloPuntos,#txtCostoTransaccionalCanje, #txtCostoSeguro,#txtCostoSMS, #txtCostoPlusin").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $('#formSocio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    if ($('#searchableHobbies').length) {
        //* searchable
        $('#searchableHobbies').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más hobbies"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all-hobbies').click(function () {
            $('#searchableHobbies').multiSelect('select_all');
            return false;
        });
        $('#deselect-all-hobbies').click(function () {
            $('#searchableHobbies').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchableHobbies')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchableHobbies').focus();
                return false;
            }
        })
    }

    if ($('#searchableActividades').length) {
        //* searchable
        $('#searchableActividades').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-searchActividades" autocomplete="off" placeholder="Selecciona 1 o más actividades"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all-hobbies').click(function () {
            $('#searchableActividades').multiSelect('select_all');
            return false;
        });
        $('#deselect-all-hobbies').click(function () {
            $('#searchableActividades').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-searchActividades').length) {
        $('#ms-searchActividades').quicksearch($('.ms-elem-selectable', '#ms-searchableActividades')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchableActividades').focus();
                return false;
            }
        })
    }

    if ($("#hfIDSocio").val() != "0") {
        cargarHobbies();
        cargarActividades();
    }

    if ($("#hfIDSocio").val() != "0")
        $("#litTitulo").html("Edición de " + $("#txtApellido").val() + ", " + $("#txtNombre").val());
    else
        $("#litTitulo").html("Alta de Socio");
});

function irTarjetasSustitucion() {
    window.location.href = "/modulos/gestion/TarjetasSustitucion.aspx?IDSocio=" + $("#hfIDSocio").val();
}

function ObtenerPromedioTicket() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerPromedioTicket",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_ticketpromedio").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalTasaUsoMensual() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerTotalTasaUsoMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tasauso_mensual").html(data.d + " %");
            }
        }
    });

}

function ObtenerTotalTR() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerTotalTRMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tr_mensual").html(data.d);
            }
        }
    });
}

function ObtenerTotalCanjes() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerTotalCanjes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_canjeado").html("$ " + data.d);
            }
        }
    });
}

function ObtenerImporteAhorro() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerImporteAhorro",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_importeAhorro").html("$ " + data.d);
            }
        }
    });
}

function ObtenerImportePagado() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerImportePagado",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_importe").html("$ " + data.d);
            }
        }
    });
}

function ObtenerSaldoActual() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerSaldoActual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_saldoActual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerPuntosActuales() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerPuntosActuales",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_puntosActuales").html( data.d);
            }
        }
    });
}

function ObtenerCantTR() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerCantTR",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cantTR").html(data.d);
            }
        }
    });
}

function ObtenerCantComerciosUnicos() {
    $.ajax({
        type: "POST",
        data: "{ IDSocio: " + parseInt($("#hfIDSocio").val()) + "}",
        url: "Sociose.aspx/obtenerCantComerciosUnicos",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cantComerciosUnicos").html(data.d);
            }
        }
    });
}

function configFechasDesdeHasta2(date, end) {

    $.validator.addMethod("greaterThan", function () {
        var valid = true;
        var desde = $("#" + date).val();
        var hasta = $("#" + end).val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        return valid;
    }, 'La "fecha tope canje" debe ser menor o igual a la "fecha caducidad"');
    $('form').validate({
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
    //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

    $('#' + date).change(cleanErrors);
    $('#' + end).change(cleanErrors);
}

function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPais").val() != "")
        idPais = parseInt($("#ddlPais").val());

    $('#ddlCiudad').html("<option value=''></option>").trigger('liszt:updated');

    $.ajax({
        type: "POST",
        url: "Sociose.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                var idProv = parseInt($("#ddlProvincia").val());
                LoadCiudades2(idProv, 'ddlCiudad');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function puntosPorTarjeta(idTarjeta) {

    if (idTarjeta != null && idTarjeta != "") {
        $.ajax({
            type: "POST",
            url: "Sociose.aspx/puntosPorTarjeta",
            data: "{ idTarjeta: " + idTarjeta
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $("#txtPuntos").val(data.d);
                    $("#divError").hide();
                }
                else
                    $("#txtPuntos").val(0);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });

    }
}

function trPuntos() {
    $("#divErrorTrPuntos").hide();
    $("#divOkTrPuntos").hide();
    
    if ($("#ddlTarjetas2").val() == null || $("#ddlTarjetas2").val() == "")
        errorTrPuntos("Debe seleccionar una tarjeta");

    else if ($("#ddlMotivos").val() == null || $("#ddlMotivos").val() == "")
        errorTrPuntos("Debe seleccionar un motivo");
    
    else if ($("#hfIDSocio").val() == null || $("#hfIDSocio").val() == "0") {
        errorTrPuntos("El socio no existe");
    }

    else if ($("#txtPuntos").val() == null || $("#txtPuntos").val() == "")
        errorTrPuntos("Debe ingresar una cantidad de puntos");

    else {

        var puntos = 0.00
        if ($("#txtPuntos").val() != null)
            puntos = parseFloat ($("#txtPuntos").val());

        var info = "{ numeroTarjeta: '" + $("#ddlTarjetas2 option:selected").text() + "',idmotivo: '" + $("#ddlMotivos").val() + "',puntos : " + puntos + ",idSocio : " + $("#hfIDSocio").val() + "}";

        $.ajax({
            type: "POST",
            url: "Sociose.aspx/trPuntos",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) { 
                $("#divOkTrPuntos").show();
                window.location.href = "Sociose.aspx?IDSocio=" + $("#hfIDSocio").val();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }

}

function errorTrPuntos(mensaje) {
    $("#divErrorTrPuntos").html(mensaje);
    $("#divErrorTrPuntos").show();
    
}