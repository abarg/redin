﻿function configControls() {
    $("#txtNombre").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    var idFranq = parseInt($("#hdnIDFranquicia").val());

    var myColumns = [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { field: "Franquicia", title: "Franquicia", width: "100px" },
            { field: "IDMarca", title: "ID", width: "50px" },
            { field: "Nombre", title: "Nombre", width: "200px" },
            { field: "Prefijo", title: "Prefijo", width: "80px" },
            { field: "Affinity", title: "Affinity", width: "70px", attributes: { class: "colCenter" } },
            { field: "MostrarSoloTarjetasPropias", title: "Solo Tarj Propias", width: "110px", attributes: { class: "colCenter" } },
            { field: "MostrarSoloPOSPropios", title: "Solo POS Propios", width: "110px", attributes: { class: "colCenter" } },
            { field: "Arancel", title: "Arancel", width: "70px", attributes: { class: "colCenter" } },
            { field: "FechaAlta", title: "Fecha Alta", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "IDFranquicia", title: "IDFranquicia", width: "100px", hidden: "hidden" },
    ]

    if (idFranq > 0) {
        myColumns.splice(3, 1)
    }
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDMarca: { type: "integer" },
                        Nombre: { type: "string" },
                        Prefijo: { type: "string" },
                        Affinity: { type: "string" },
                        MostrarSoloTarjetasPropias: { type: "string" },
                        MostrarSoloPOSPropios: { type: "string" },
                        FechaAlta: { type: "date" },
                        Arancel: { type: "integer" },
                        IDFranquicia: { type: "int" },
                        Franquicia: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcas.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        //columns: [
        //    { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
        //    { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
        //    { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
        //    { field: "Franquicia", title: "Franquicia", width: "100px" },
        //    { field: "IDMarca", title: "ID", width: "50px" },
        //    { field: "Nombre", title: "Nombre", width: "200px" },
        //    { field: "Prefijo", title: "Prefijo", width: "80px" },
        //    { field: "Affinity", title: "Affinity", width: "70px", attributes: { class: "colCenter" } },
        //    { field: "MostrarSoloTarjetasPropias", title: "Solo Tarj Propias", width: "110px", attributes: { class: "colCenter" } },
        //    { field: "MostrarSoloPOSPropios", title: "Solo POS Propios", width: "110px", attributes: { class: "colCenter" } },
        //    { field: "Arancel", title: "Arancel", width: "70px", attributes: { class: "colCenter" } },
        //    { field: "FechaAlta", title: "Fecha Alta", format: "{0:dd/MM/yyyy}", width: "80px" },
        //    { field: "IDFranquicia", title: "IDFranquicia", width: "100px", hidden: "hidden" },
        //]
        columns: myColumns
    });


    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "marcase.aspx?IDMarca=" + dataItem.IDMarca;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcas.aspx/Delete",
                data: "{ id: " + dataItem.IDMarca + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var idMarca = dataItem.IDMarca;
        var info = "{ idMarca: " + idMarca
            + "}";
        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginMarcaAutomaticoAdmin",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/marcas/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });

}

function Nuevo() {
    window.location.href = "marcase.aspx";
}

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }

    var franquicia = parseInt($("#ddlFranquicias").val());
    if (franquicia != "") {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: franquicia });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});


function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);


    var info = "{ idFranquicia: '" + $("#ddlFranquicias").val()
            + "', nombre: '" + $("#txtNombre").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "Marcas.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}
