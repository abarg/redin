﻿$(document).ready(function () {
    //    configControls();
    $("#txtDocumento,#txtNroSocio2").numeric();
    $("#lblNro2").val("");
    $("#tablaSocios").hide();
    $("#txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });
    $("#txtNroSocio2").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar2();
            return false;
        }
    });

    //$('.chckbox').mousedown(function () {
    //    alert("hola");
    //    if (!$(this).is(':checked')) {
    //        this.checked = confirm("Are you sure?");
    //        $(this).trigger("change");
    //    }
    //});

    //if (checkOneChecked()) {
    //    //$("#divddlSocio").show();
    //    //$("#divddlTarjeta").show();
    //    $("#divBuscar2").show();
    //}

    //$("#chkTrans1").click(function () {
    //    alert("hola");
    //});
});

function buscar() {
    $("#divOk").hide();
    $("#divError").hide();

    var documento = $("#txtDocumento").val();
    if (documento == "") {
        error("El campo de Nro. de Documento o Nro. Tarjeta no puede estar vacio");
    }
    else {
        var info = "{ DNI: '" + $("#txtDocumento").val() + "'}";

       
        limpiarPantalla();
        $("#imgLoadingBuscar").show();
        $.ajax({
            type: "POST",
            url: "TransferirPuntos.aspx/buscarDatosSocio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {          
                    generarTablaTarjetas(info);
                    $("#txtNombre").html(data.d.Nombre);
                    $("#txtApellido").html(data.d.Apellido);
                    $("#txtPuntos").html(data.d.Puntos);
                    $("#lblNro").hide();
                }
                else {
                    error("No existe ningun socio asociado a ese Nro Documento ó Nro Tarjeta")
                    $("#divBuscar2").hide();
                    $("#imgLoadingBuscar").hide();
                }
            }  
        });
      
    }
}

function generarTablaTarjetas(nroSocio) {
    var info = nroSocio;
    $.ajax({
        type: "POST",
        url: "TransferirPuntos.aspx/generarTabla",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#tblTarjetasDetalle").show();
            $(".selectTarjetas").show();
            $("#datosSocio").show();
            if (data.d != null) {
                $("#lblNro").hide();
                $("#bodySociosDetalle").html(data.d);
                $("#tablaTarjetas").show();                
                $("#divBuscar2").show();
                $("#lblNro2").show();                
            }
            else {
                $("#divBuscar2").hide();
                var html = "<tr><td colspan='4'>No hay tarjetas ni socios con ese Nro. Documento ó Nro.Tarjeta</td></tr>";
                $("#bodySociosDetalle").html(html);
                $("#bodySociosDetalle").show();
                $("#tablaTarjetas").show();
            }
            $("#txtNroSocio2").val("");
            $("#imgLoadingBuscar").hide();
        }
    });

}

function buscar2() {
    $("#divError2").hide();
    check = checkOneChecked();
    if ($("#txtNroSocio2").val() != "") {
        //$("#lblNro2").hide();
        if (check) {
            $("#divError").hide();
            $("#imgLoadingBuscar2").show();
            buscarSocioDestino($("#txtNroSocio2").val());
            buscarTarjetas();
        }
        else {
            $("#divError2").html("Debe seleccionar al menos una tarjeta para realizar la transferencia");
            $("#divError2").show();
        }
    }
    else {
        $("#divError2").html("El campo de Nro. de Documento o Nro. Tarjeta no puede estar vacio");
        $("#divError2").show();
        //$("#errorSocioNro2").show();
        //$("#lblNro2").hide();
    }

}

function buscarSocioDestino(nroSocioDestino) {

   
    var info = "{ DNI: '" + nroSocioDestino + "'}";
    $.ajax({
        type: "POST",
        url: "TransferirPuntos.aspx/buscarDatosSocio",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                $("#txtNombreDestino").html(data.d.Nombre);
                $("#txtApellidoDestino").html(data.d.Apellido);
            }
            else {
                //$("#errorSocioNro2").show();
                $("#divError2").html("No se encontraron tarjetas asociadas con ese Nro.Documento o Nro.Tarjeta");
                $("#divError2").show();
            }
            $("#imgLoadingBuscar2").hide();
        }
    });
    //$("#txtNroSocio2").val("");
}

function checkOneChecked() {
    if ($("input[type='checkbox']:checked").length != 0) {
        return true;
    }
    return false;
}

function buscarTarjetas() {
    $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");

    var info = "{ nroSocio: '" + $("#txtNroSocio2").val() + "'}";

    $.ajax({
        type: "POST",
        url: "TransferirPuntos.aspx/buscarTarjetas",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrTarjeta"));
                }
                if (data.d.length == 0) {
                    $("#divError2").html("No se encontraron tarjetas asociadas con ese Nro.Documento o Nro.Tarjeta");
                    $("#divError2").show();
                    //$("#errorSocioNro2").show();
                    //$("#lblNro2").hide();
                    $("#divddlTarjeta").hide();
                    $("#transPtos").hide();
                    $("#datosSocioDestino").hide();
                }
                else if (data.d.length == 1) {
                    $("#datosSocioDestino").show();
                    $("#ddlTrTarjeta").val(data.d[0].ID);
                    getPuntos(data.d[0].ID);
                }
                if (data.d.length > 0) {
                    $("#datosSocioDestino").show();
                    $("#divddlTarjeta").show();
                    $("#ddlTrTarjeta").show();
                    $("#transPtos").show();
                }
            }
            else {
                //$("#errorSocioNro2").show();
                //$("#lblNro2").hide();
                $("#divError2").html("No se encontraron tarjetas asociadas con ese Nro.Documento o Nro.Tarjeta");
                $("#divError2").show();
                $("#divddlTarjeta").hide();
                $("#transPtos").hide();
                $("#datosSocioDestino").hide();
            }
        },
        error: function (response) {
            $("#errorSocioNro2").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
    $("#txtNroSocio2").val("");

}

function getPuntos(puntos) {
    if ($("#ddlTrSocio").val() != "0")
        $("#lblPuntos").html("Puntos disponibles: " + puntos);
}

function transferirPuntos() {
    var tarjetaDestino = $("#ddlTrTarjeta").val();
    var n = 1;
    var tarjetaOrigen;
    var puntosATrans;
    var puntosDisponibles;
    var i = 0;
    var transCompleta = false;
    var tarjetaRepetida = false;
    if (tarjetaDestino != "") {
        $("#lblTransPtos").hide();        
        $(".selectTarjetas").each(function () {
            if ($("#ddlTrTarjeta option:selected").text() == $("#nroTarjeta" + n).val()) {
                tarjetaRepetida = true;
            }
            else n++;
        });

        if (tarjetaRepetida == false) {
            n=1
            $(".selectTarjetas").each(function () {
                var marcado = $('#chkTrans' + n).prop("checked") ? true : false;
                var checked = $('#chkTrans' + n).val();
                if (marcado) {
                    tarjetaOrigen = $("#nroTarjeta" + n).val();
                    puntosDisponibles = parseInt($("#puntosDisp" + n).val());
                    puntosATrans = parseInt($("#ptosATrans" + n).val());
                    i++;

                    $("#divOk").hide();
                    if (i >= 5) {
                        if (!checkOneChecked()) {
                            error("No hay seleccionada ninguna tarjeta de origen");
                        }
                        else if ($('#chkTrans' + n).prop("checked") && !(parseInt($("#ptosATrans" + n).val()) > 0)) {
                            error("No se han asignado los puntos de la tarjeta origen: " + tarjetaOrigen);
                        }
                        else if (puntosATrans > puntosDisponibles) {
                            error("Los puntos a transferir no pueden superar los disponibles");
                        }
                        else {
                            n++;
                            i = 0;
                            transCompleta = true;
                            var info = "{ nroTarjetaOrigen: '" + tarjetaOrigen + "', IDTarjetaDestino : '" + tarjetaDestino + "', puntosATrans : " + puntosATrans + "}";
                            $.ajax({
                                type: "POST",
                                url: "TransferirPuntos.aspx/transferirPuntos",
                                data: info,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data, text) {
                                    limpiarPantalla();
                                    $("#divOk").show();                               
                                },
                                error: function (response) {
                                    $("#divddlTarjeta").hide();
                                    $("#ddlTrTarjeta").hide();
                                    $("#transPtos").hide();
                                    $("#divOk").hide();
                                    error("No se ha podido realizar la transferencia de puntos");
                                }
                            });
                        }
                    }
                }
                else {
                    i = 0;
                    n++;
                }

            });
        }
        else {
            error("No se puede seleccionar una misma tarjeta de destino y de origen");
        }

        if (transCompleta) {
            limpiarPantalla();
            $("#divOk").show();
        }
    }
    else {
        $("#lblTransPtos").show();
    }
}

function limpiarPantalla() { 
    $("#txtDocumento").val("");
    $("#txtNroSocio2").val("");
    $("#tblTarjetasDetalle").hide();
    $(".selectTarjetas").hide();
    $("#datosSocio").hide();
    $("#datosSocioDestino").hide();
    $("#divError").hide();
    $("#divOk").hide();
    //$("#lblNro2").hide();
    $("#divError2").hide();
    $("#divddlTarjeta").hide();
    $("#transPtos").hide();
    $("#divddlTarjeta").hide();
    $("#ddlTrTarjeta").hide();
    $("#transPtos").hide();
    $("#divBuscar2").hide();    
}

function error(mensaje) {
    $("#divOk").hide();
    $("#divError").html(mensaje);
    $("#divError").show();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}