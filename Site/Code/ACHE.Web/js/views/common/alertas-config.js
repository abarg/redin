﻿function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var nombre = $("#txtNombre").val();
    if (nombre != "" && nombre !=null) {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre });
    }
    var idFranquicia = $("#ddlFranquicias").val();
    if (idFranquicia != "" && idFranquicia != null) {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(idFranquicia) });
    }
    var idMarca = $("#ddlMarcas").val();
    if (idMarca != "" && idMarca != null) {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(idMarca) });
    }
    var idComercio = $("#ddlComercios").val();
    if (idComercio != "" && idComercio != null) {
        $filter.push({ field: "IDComercio", operator: "equal", value: parseInt(idComercio) });
    }
    var idEmpresa = $("#ddlEmpresas").val();
    if (idEmpresa != "" && idEmpresa != null) {
        $filter.push({ field: "IDEmpresa", operator: "equal", value: parseInt(idEmpresa) });
    }

    grid.dataSource.filter($filter);
}

function configControls() {
    $("#txtNombre,#ddlFranquicias,#ddlComercios,#ddlMarcas,#ddlEmpresas").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    $(".chosen").chosen();

    var myColumns = [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            { field: "ID", title: "ID", width: "50px", hidden: true },
            { field: "TipoEntidad", title: "Tipo Entidad", width: "100px" },
            { field: "NombreEntidad", title: "Nombre Entidad", width: "100px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Nombre", title: "Nombre", width: "300px" },
            { field: "Prioridad", title: "Prioridad", width: "100px" },
            { field: "Restringido", title: "Válido para", width: "100px" },
            { field: "Activa", title: "Activa", width: "50px", attributes: { class: "colCenter" } }
    ];

    if ($("#hdnIDFranquicias").val() == "0") {
        $(".admin").show();
    }
    else if (parseInt($("#hdnIDFranquicias").val()) > 0) {
        myColumns.splice(2, 2);
        
    }

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Prioridad: { type: "string" },
                        Nombre: { type: "string" },
                        Activa: { type: "string" },
                        Tipo: { type: "string" },
                        Restringido: { type: "string" },
                        TipoEntidad: { type: "string" },
                        NombreEntidad: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "alertas-config.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {

                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: myColumns
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "alertas-confige.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "alertas-config.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    //html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/></div>";
    html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    return html;
}

function Nuevo() {
    window.location.href = "alertas-confige.aspx";
}

$(document).ready(function () {
    configControls();
});