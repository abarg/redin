﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    //    $('#formEdicion').validate();

    if ($('#formSorteo').valid()) {
        var idSorteo = parseInt($("#hdnIDSorteo").val());
        var info = "{ idSorteo: " + idSorteo
            + ", idMarca: '" + $("#cmbMarcas").val()
            + "', Titulo: '" + $("#txtTitulo").val()
            + "', Msj1: '" + $("#txtMsj1").val()
            + "', Msj2: '" + $("#txtMsj2").val()
            + "', Msj3: '" + $("#txtMsj3").val()
            + "', Msj4: '" + $("#txtMsj4").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', activo: " + $("#chkActivo").is(':checked')
            + ", Sexo: '" + $("#ddlSexo").val()
            + "', ImporteDesde: '" + $("#txtImporteDesde").val()
            + "', ImporteHasta: '" + $("#txtImporteHasta").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "Sorteose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Sorteos.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}



$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtImporteDesde,#txtImporteHasta").numeric();

    if (parseInt($("#hdnIDSorteo").val()) == 0) {
        provinciasByPaises();
    }

    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        provinciasByPaises();
    });
});


function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPais").val() != "")
        idPais = parseInt($("#ddlPais").val());

    $('#ddlCiudad').html("<option value=''></option>").trigger('liszt:updated');

    $.ajax({
        type: "POST",
        url: "Sociose.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                var idProv = parseInt($("#ddlProvincia").val());
                LoadCiudades2(idProv, 'ddlCiudad');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}