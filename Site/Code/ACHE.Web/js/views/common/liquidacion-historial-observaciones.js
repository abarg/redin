﻿function abrirModal() {
    $("#modalAgregarObservacion").modal("show");
}
function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    //   $filter.push({ field: "IDSocio", operator: "equal", value: parseInt($("#hfIDSocio").val()) });

    grid.dataSource.filter($filter);
}
function irAlDetalle() {
    window.location.href = "LiquidacionesDetalle.aspx?IDLiquidacion=" + $("#hdfIDLiquidacion").val();

}

function guardar() {
    $('#formEdicionObs').validate();
    if ($('#formEdicionObs').valid()) {

        var info = "{ observacion: '" + $("#txtObservacion").val()
          + "'}";

        $.ajax({
            type: "POST",
            url: "LiquidacionHistorialObservaciones.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#modalAgregarObservacion").modal("hide");
                filter();
                $("#divError").hide();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#modalAgregarObservacion").modal("hide");
                filter();
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function configControls() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        FechaGeneracion: { type: "date" },
                        Origen: { type: "string" },
                        Observaciones: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "LiquidacionHistorialObservaciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "FechaGeneracion", title: "Fecha", format: "{0:dd/MM/yyyy}", width: "50px" },
            { field: "Origen", title: "Origen", width: "50px" },
            { field: "Observaciones", title: "Observación", width: "500px" }
        ]
    });

}








$(document).ready(function () {
    configControls();
});



