﻿function agregarConcepto() {
    $('#formEdicion').validate();
    if ($('#formEdicion').valid()) {

        var info = "{ tipo: '" + $("#cmbConcepto").val() + "', idLiquidacion:" + $("#hfIDLiquidacion").val() + "}";
        $.ajax({
            type: "POST",
            url: "LiquidacionesDetalle.aspx/AgregarConcepto",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                GenerarTabla();
                cargarConceptos();
                $("#divError2").hide();
                $("#divError").hide();
                $("#divOk").show();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                GenerarTabla();
                $("#divOk").hide();
                $("#divError2").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}



$(document).ready(function () {
    cargarConceptos();
    GenerarTabla();
});

function irHistorialObs(id) {
    window.location.href = "LiquidacionHistorialObservaciones.aspx?IDLiquidacionDetalle=" + id;

}


function mostrarPaso4() {
    $("#divPaso4").show();
}

function irDetalle(id) {
    window.location.href = "LiquidacionDetallePorTipo.aspx?IDLiquidacionDetalle=" + id;
}
function EliminarDetalle(id) {
    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/EliminarDetalle",
        data: "{ id: " + id+ "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            location.reload();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divOk").hide();
            $("#divError2").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}
function enviarACasaMatriz() {
    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/enviarACasaMatriz",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            location.reload();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divOk").hide();
            $("#divError2").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function enviarAlFranquiciado() {
    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/enviarAlFranquiciado",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
          
            location.reload();

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divOk").hide();
            $("#divError2").hide();
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}


function GenerarTabla() {

    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/generarTabla",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyLiquidacionDetalle").html(data.d);
            }
        }
    });
}






function cargarConceptos() {

    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/cargarConceptos",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#cmbConcepto").empty();
            $("#cmbConcepto").append("<option value=''></option>");
            if (data.d != null && data.d.length > 0) {
                for (var i = 0; i < data.d.length; i++)
                    $("#cmbConcepto").append("<option value='" + data.d[i] + "'>" + data.d[i] + "</option>");
            } else {
                $('#pnlFormLiqDetalle').hide();

            }
            $('#cmbConcepto').trigger("liszt:updated");
        },
        error: function (response) {

        }
    });

}
/*


function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

function configControls() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDLiquidacionDetalle: { type: "integer" },
                        Concepto: { type: "string" },
                        SubTotal: { type: "decimal" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "LiquidacionesDetalle.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "IDZona", title: "ID", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Historial Observaciones", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Eliminar' class='viewColumn'/></div>" }, title: "Detalle", width: "50px" },
            { field: "Concepto", title: "Concepto", width: "100px" },
            { field: "SubTotal", title: "SubTotal", width: "100px" },
            { field: "IDLiquidacionDetalle", title: "IDLiquidacionDetalle", width: "50px", hidden: "hidden" }

            //{ title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "LiquidacionHistorialObservaciones.aspx?IDLiquidacionDetalle=" + dataItem.IDLiquidacionDetalle;
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "LiquidacionDetallePorTipo.aspx?IDLiquidacionDetalle=" + dataItem.IDLiquidacionDetalle;
    });
}



$(document).ready(function () {

    GenerarTabla();
    $("#txtImporteTotal").numeric();
});


function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    
    var info = "{ idLiquidacion: '" + $("#hfIDLiquidacion").val()
              + "'}";

    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/Exportar",
         data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}
function GenerarTabla() {

    var info = "{ idLiquidacion: '" + $("#hfIDLiquidacion").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "LiquidacionesDetalle.aspx/generarTabla",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyLiquidacionDetalle").html(data.d);
            }
        }
    });
}

function guardar() {
    var isValid = true;
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var msg;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
            info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
    });

     info += "'}";
    if (isValid) {
        $.ajax({
            type: "POST",
            url: "LiquidacionesDetalle.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divError").hide();
                $("#divOk").show();
                window.location.href = "Liquidaciones.aspx";
      //          $("#tblVerificacionesPOS").hide();
       //         $("#txtNroDeReferencia").val("");

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").hide();
                $("#divOk").hide();
            }
        });
    } else {
        $("#divError").html(msg);
        $("#divError").show();
        $("#divOk").hide();
    }
}



function enviarObs() {
    var isValid = true;
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var msg;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
        info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
    });

    info += "'}";
    if (isValid) {
        $.ajax({
            type: "POST",
            url: "LiquidacionesDetalle.aspx/enviarObs",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divError").hide();
                $("#divOk").show();
                window.location.href = "Liquidaciones.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").hide();
                $("#divOk").hide();
            }
        });
    } else {
        $("#divError").html(msg);
        $("#divError").show();
        $("#divOk").hide();
    }
}



function enviarAlFranquiciado() {
    var isValid = true;
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var msg;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
        info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
    });

    info += "'}";
    if (isValid) {
        $.ajax({
            type: "POST",
            url: "LiquidacionesDetalle.aspx/enviarAlFranquiciado",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divError").hide();
                $("#divOk").show();
                window.location.href = "Liquidaciones.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").hide();
                $("#divOk").hide();
            }
        });
    } else {
        $("#divError").html(msg);
        $("#divError").show();
        $("#divOk").hide();
    }
}


function confirmarLiquidacion() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var valid = true;
    if ($("#flpFactura").val() == "") {
        $("#divError").html("debe adjuntar una factura");
        $("#divError").show();
        valid = false;
    }
    if(valid){
        var info = "{ idLiquidacion: '" + $("#hfIDLiquidacion").val()
                   + "', archivo: '" + $("#flpFactura").val()
                 + "'}";
        $.ajax({
            type: "POST",
            url: "LiquidacionesDetalle.aspx/ConfirmarLiquidacion",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                window.location.href = "Liquidaciones.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
            }
        });
    }
}

function mostrarPaso4() {
    //$("#tblLiquidacionDetalle :input").attr("disabled", true);
    $("#divPaso4").show();
}

*/