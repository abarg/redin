﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);

$(document).ready(function () {
    var cargarDatos = true;

    var idFranquiciaAdmin = parseInt($("#hdnIDFranquiciasAdmin").val());
    //$(".dashboard").show();
    //alert(idFranquiciaAdmin);
    //alert($("#hdnIDFranquicias").val());
    if ($("#hdnIDFranquicias").val() != "")//se ejecuta desde perfil franquicias
    {
        $(".dashboard").show();
        $(".admin").hide();
    }
    else {
        if (idFranquiciaAdmin == 0) {
            $(".admin").show();
            $(".dashboard").hide();
            cargarDatos = false;
        } else {
            $(".dashboard").show();
            $(".admin").show();
        }
    }

    if (cargarDatos) {
        setTimeout("ObtenerCantidadActividades()", 100);
        setTimeout("ObtenerSociosMultipleActividad()", 500);
        setTimeout("ObtenerTotalSociosSexo()", 500);

        setTimeout("ObtenerTotalEmails()", 400);
        setTimeout("ObtenerTotalCelulares()", 600);
        setTimeout("ObtenerTotalTarjetasActivas()", 1000);


      //  setTimeout("gebo_charts.fl_tarjetas_asignadas()", 2000);
       // setTimeout("gebo_charts.fl_cant_transacciones()", 2000);

      //  setTimeout("gebo_charts.fl_estado_terminales()", 2000);

     //   setTimeout("ObtenerTermianales()", 100);


        setTimeout("ObtenerSociosPorBeneficio()", 100);

        setTimeout("ObtenerSociosPorSecretaria()", 100);


        setTimeout("ObtenerBeneficiariosUbicacionSanMartin()", 500);

        setTimeout("pieChartXActividad()", 500);

        setTimeout("pieChartXSecretaria()", 500);


    }



});

function pieChartXSecretaria() {

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieXSecretaria').get(0).getContext('2d')

    var pieChart = new Chart(pieChartCanvas)

    var data = [
        {
            value: 479,
            color: '#f56954',
            highlight: '#f56954',
            label: 'SECRETARIA DE DESARROLLO SOCIAL'
        },
        {
            value: 1105,
            color: '#00a65a',
            highlight: '#00a65a',
            label: 'SECRETARIA INTEGRACION EDUCATIVA, CULTURAL Y DEPORTIVA	'
        },

    ];
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(data, pieOptions)

  
}


function pieChartXActividad() {
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieXActividad').get(0).getContext('2d')
    var pieChart = new Chart(pieChartCanvas)
    var PieData = [
        {
            value: 776,
            color: '#f56954',
            highlight: '#f56954',
            label: 'NATACION'
        },
        {
            value: 351,
            color: '#00a65a',
            highlight: '#00a65a',
            label: 'PILETA'
        },
        {
            value: 128,
            color: '#f39c12',
            highlight: '#f39c12',
            label: 'PROGRAMA CASA DE DERECHOS'
        },
        {
            value: 351,
            color: '#00c0ef',
            highlight: '#00c0ef',
            label: 'PASES DE TRANSPORTE'
        },
    ]
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
}

function mostrarReporte(idFranquicia) {
    window.location.href = "Dashboard-franquicias.aspx?IDFranquicia=" + idFranquicia;
}

function ObtenerTermianales() {
    var data = {
        labels: ['Bananas', 'Apples', 'Grapes'],
        series: [20, 15, 40]
    };

    var options = {
        labelInterpolationFnc: function (value) {
            return value[0]
        }
    };

    var responsiveOptions = [
        ['screen and (min-width: 640px)', {
            chartPadding: 30,
            labelOffset: 100,
            labelDirection: 'explode',
            labelInterpolationFnc: function (value) {
                return value;
            }
        }],
        ['screen and (min-width: 1024px)', {
            labelOffset: 80,
            chartPadding: 20
        }]
    ];

    new Chartist.Pie('#myChart', data, options, responsiveOptions);


}

//* charts
gebo_charts = {
    fl_terminales_activas: function () {
        // Setup the placeholder reference
        var elem = $('#fl_terminales_activas');

        var data = ObtenerTerminalesActivas();
        //var colors = [ "#eadac8", "#dcc1a3", "#cea97e", "#c09059", "#a8763f", "#835c31", "#5e4223", "#392815" ];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    }/*,
                    colors: colors*/
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

                // Setup a visually pleasing rounding function
                round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_tarjetas_activas: function () {

        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_activas');

        var activas = ObtenerTarjetasActivas();
        var inactivas = ObtenerTarjetasInactivas();

        var data = [
            { label: "Activas", data: activas },
            { label: "Inactivas", data: inactivas }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>" + item.series.label + "</b> = " + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_asignadas: function () {
        var elem = $('#fl_tarjetas_asignadas');

        var data = ObtenerTarjetasAsignadas();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>Cantidad</b> = " + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    //fl_cant_transacciones: function () {
    //    var elem = $('#fl_cant_transacciones');

    //    var data = obtenercanttransacciones();
    //    //var data = [["january", 10], ["february", 8], ["march", 4], ["april", 13], ["may", 17], ["june", 9]];

    //    $.plot(elem, [data], {
    //        series: {
    //            shadowsize: 1,
    //            bars: {
    //                show: true,
    //                barwidth: 0.6,
    //                fillcolor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
    //                align: "center"
    //            }
    //        },
    //        xaxis: {
    //            mode: "categories",
    //            ticklength: 0, // hide gridlines
    //            //axislabel: 'mes',
    //            axislabelusecanvas: true,
    //            axislabelfontsizepixels: 12,
    //            axislabelfontfamily: 'verdana, arial, helvetica, tahoma, sans-serif',
    //            axislabelpadding: 5
    //        },
    //        grid: {
    //            backgroundcolor: { colors: ["#fff", "#eee"] },
    //            hoverable: true,
    //            clickable: false,
    //            borderwidth: 1
    //        }
    //    });

    //    elem.bind("plothover", function (event, pos, item) {
    //        if (item) {
    //            if (previousPoint != item.datapoint) {
    //                previousPoint = item.datapoint;
    //                $("#flot-tooltip").remove();

    //                y = item.datapoint[1];
    //                z = item.series.color;

    //                showTooltip(item.pageX, item.pageY,
    //                    "<b>Cantidad</b> = " + y,
    //                    z);
    //            }
    //        } else {
    //            $("#flot-tooltip").remove();
    //            previousPoint = null;
    //        }
    //    });
    //},

    fl_tarjetas_impresas: function () {
        ObtenerTarjetasImpresas2();
    },

    //fl_estado_terminales: function () {
    //    // Setup the placeholder reference
    //    var elem = $('#fl_estado_terminales');

    //    var data = ObtenerEstadoTerminales();
    //    var colors = ["#000", "#F5AA1A", "#70A415", "#f00", "#f8f412", "#0066FF", "#ccc"];

    //    // Setup the flot chart using our data
    //    function a_plotWithColors() {
    //        fl_a_plot = $.plot(elem, data,
    //            {
    //                //label: "Visitors by Location",
    //                series: {
    //                    pie: {
    //                        show: true,
    //                        //tilt: 0.5,
    //                        innerRadius: 0.5,
    //                        highlight: {
    //                            opacity: 0.2
    //                        }
    //                    }
    //                },
    //                grid: {
    //                    hoverable: true,
    //                    clickable: true
    //                },
    //                colors: colors
    //            }
    //        );
    //    }

    //    a_plotWithColors();

    //    // Create a tooltip on our chart
    //    elem.qtip({
    //        prerender: true,
    //        content: 'Loading...', // Use a loading message primarily
    //        position: {
    //            viewport: $(window), // Keep it visible within the window if possible
    //            target: 'mouse', // Position it in relation to the mouse
    //            adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
    //        },
    //        show: false, // We'll show it programatically, so no show event is needed
    //        style: {
    //            classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
    //            tip: false // Remove the default tip.
    //        }
    //    });


    //    // Bind the plot hover
    //    elem.on('plothover', function (event, coords, item) {
    //        // Grab the API reference
    //        var self = $(this),
    //            api = $(this).qtip(),
    //            previousPoint, content,

    //            // Setup a visually pleasing rounding function
    //            round = function (x) { return Math.round(x * 1000) / 1000; };

    //        // If we weren't passed the item object, hide the tooltip and remove cached point data
    //        if (!item) {
    //            api.cache.point = false;
    //            return api.hide(event);
    //        }

    //        // Proceed only if the data point has changed
    //        previousPoint = api.cache.point;
    //        if (previousPoint !== item.seriesIndex) {
    //            percent = parseFloat(item.series.percent).toFixed(2);
    //            // Update the cached point data
    //            api.cache.point = item.seriesIndex;

    //            // Setup new content
    //            content = item.series.label + ' ' + percent + '%';

    //            // Update the tooltip content
    //            api.set('content.text', content);

    //            // Make sure we don't get problems with animations
    //            api.elements.tooltip.stop(1, 1);

    //            // Show the tooltip, passing the coordinates
    //            api.show(coords);
    //        }
    //    });
    //},

    fl_comisiones: function () {

        // Setup the placeholder reference
        var elem = $('#fl_comisiones_4meses');

        var com1 = ObtenerComision("TtCp");
        var com2 = ObtenerComision("TpCp");
        var com3 = ObtenerComision("TpCt");

        var data = [
            { label: "Tt Cp", data: com1 },
            { label: "Tp Cp", data: com2 },
            { label: "Tp Ct", data: com3 }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>" + item.series.label + "</b> = " + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_comisiones_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_comisiones_detalle');

        var com1 = ObtenerComisionDiario("TtCp");
        var com2 = ObtenerComisionDiario("TpCp");
        var com3 = ObtenerComisionDiario("TpCt");

        for (var i = 0; i < com1.length; ++i) { com1[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < com2.length; ++i) { com2[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < com3.length; ++i) { com3[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Tt Cp", data: com1 },
                { label: "Tp Cp", data: com2 },
                { label: "Tp Ct", data: com3 }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                        "<b>" + item.series.label + "</b> = $" + y,
                        //"<b>Importe = </b> $" + y,
                        z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    }
};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerTerminalesActivas() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTerminalesPorPOS",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblTerminalesPorPOS").html("Terminales por pos (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasAsignadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTarjetasAsignadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasImpresas() {
    var ddata = [];
    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasLabels() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([i, data[i].label]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

var oTable = null;

function ObtenerTarjetasImpresas2() {
    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/ObtenerTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyImpresas").html("");

                if (oTable != null) {
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tablaImpresas').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 4,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].Marca,
                        data.d[i].Activas,
                        data.d[i].Inactivas,
                        data.d[i].Total]
                    );
                }

                //$("#tableDetalle_info").parent().remove();
                $("#tablaImpresas").css("width", "100%");

                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyImpresas").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }
        }
    });
}

function ExportarPorActividad(tipo) {

    console.log('okok')

    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "Dashboard-Actividades.aspx/ExportarPorActividad",
        data: "{idActividad: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function ObtenerEstadoTerminales() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerEstadoTerminales",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblEstadoTerminales").html("Estado de las terminales (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    console.log(ddata);

    return ddata;
}

/*Tarjetas activas & inactivas, 4 meses*/
function ObtenerTarjetasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTarjetasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasInactivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTarjetasInactivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Cantidad Transacciones*/

function ObtenerCantTransacciones() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerCantTransacciones",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

/*** Comisiones ***/

function ObtenerComision(tipo) {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerComision",
        data: "{tipo: '" + tipo + "'}",
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, parseInt(data[i].data)]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerComisionDiario(tipo) {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerComisionDiario",
        data: "{tipo: '" + tipo + "'}",
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*METODOS DE OBTENCION DE DATOS DE LOS ICONOS*/



function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerSociosPorBeneficio() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerSociosPorBeneficio",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data != null) {
                $("#bodySociosMes").html(data.d);
            }
        }
    });
}

function ObtenerSociosPorSecretaria() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerSociosPorSecretaria",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data != null) {
                $("#bodySociosPorSecretaria").html(data.d);
            }
        }
    });
}



function ObtenerTop10ComerciosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTop10ComerciosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComerciosMes").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}

function ObtenerTotalSociosSexo() {
    //$("#resultado_sexo").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerTotalSociosPorSexo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var aux = data.d.split(",");
                $("#resultado_sexo").html(aux[0]);
                $("#totalSexo").html(aux[1]);
            }
        }
    });
}

function ObtenerTotalComisionMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerTotalComisionMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_comision_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerSociosMultipleActividad() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerSociosMultipleActividad",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cantidad_socios_multiple_actividad").html(data.d);
            }
        }
    });
}


function ObtenerCantidadActividades() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerTotalActividades",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_actividades").html(data.d);
            }
        }
    });
}


function ObtenerTotalEmails() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerTotalEmails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_mails").html(data.d);
            }
        }
    });
}

function ObtenerTotalCelulares() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerTotalCelulares",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_cel").html(data.d);
            }
        }
    });
}


function ObtenerTotalTarjetasActivas() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerTotalTarjetasActivas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_tarjetas_activas").html(data.d);
            }
        }
    });
}


function ObtenerCantidadTarjetas() {

    $.ajax({
        type: "POST",
        url: "Dashboard-franquicias.aspx/obtenerCantidadTarjetas",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $("#resultado_cantidad_tarjetas").html(data.d);
            }
        }
    });
}

function ObtenerBeneficiariosUbicacionSanMartin() {

    $.ajax({
        type: "POST",
        url: "Dashboard-actividades.aspx/obtenerBeneficiariosUbicacionSanMartin",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $("#resultado_beneficiarios_ubicacion").html(data.d);
            }
        }
    });
}



/*** METODOS AUXILIARES **/


function verDetalle(tipo) {

    $("#hdnTipo").val(tipo);

    $("#btnExportar").show();
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Socios");

    $("#headDetalle").html("<tr><th>Socio</th></tr>");

    $(".loader").css("display", "block");

    $.ajax({
        type: "POST",
        url: "Dashboard-Actividades.aspx/obtenerDetallePorActividad",
        data: "{idActividad: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyDetalle").html("");

                if (oTable !== null){
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalle').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });


                for (var i = 0; i < data.d.length; i++) {



                    if (data.d[i].Nombre != null) {

                        oTable.fnAddData([


                            data.d[i].Nombre + ' ' + data.d[i].Apellido




                        ]
                        );
                    }

                }

                $(".loader").css("display", "none");

                $('#btnExportar').on('click', function () {

                    console.log('something');

                    ExportarPorActividad(tipo)

                } );

                console.log('get');


                $("#tableDetalle_info").parent().remove();
                $("#tableDetalle").css("width", "100%");


                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyDetalle").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }

            $('#modalDetalle').modal('show');
        }
    });
}