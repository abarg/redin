﻿var rowsProcessed = 0;
var rowsLength = 0;

var rowsInfo = $("#rowsInfo");
var btnAsignacion = $("#btnAsignacion");
var btnSocios = $("#btnSocios");
var btnActividades = $("#btnUploadActividades");



$(function () {

    asignacionController();
    sociosController();
    actividadesController();
  

    function increment() {

        //rowsProcessed += 1;

        //$("#rowsInfoContainer").html('');

        //$("#rowsInfoContainer").append(rowsProcessed + "/" + rowsLength);

        //if (rowsProcessed == rowsLength)
        //    clearInterval(interval);

    }

});

function exportarErrores(tipo) {

    $("#imgLoading").show();

    $.ajax({
        type: "POST",
        url: "importarBeneficiarios.aspx/exportarErrores",
        data: '{"tipo": "'+ tipo + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);

            }
        }
    });
}

function actividadesController() {

    $("#flpActividades").on("change", function () {

        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;

        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();

            reader.onload = function (e) {

                // count rows and columns
                var rows = e.target.result.split("\n");
                var columns = rows[0].split(";").length;
                var rowsLength = rows.length - 1;

                var rowsAndTimeRemainingEstimation = $("<p />");

                var estTimeRemaining = Math.round((rowsLength / 5) / 60);

                if (columns == 7)
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + rowsLength + " actividades, esto puede demorar alrededor de " + estTimeRemaining + " minutos. Desea continuar?");
                else
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + columns + " columnas; deberia tener 7. Desea continuar?")

                btnActividades.removeAttr("disabled");

                rowsInfo.removeAttr("hidden");

                $("#rowsInfoContainer").html('');
                $("#rowsInfoContainer").append(rowsAndTimeRemainingEstimation);
            }
            reader.readAsText($("#flpActividades")[0].files[0]);
        }
        else {
            alert("This browser does not support HTML5.");
        }

    });

    btnSocios.on("click", function () {

        $("#rowsInfoContainer").html('');

        $("#rowsInfoContainer").append("cargando..");

    });

}

function sociosController() {

    $("#flpArchivo").on("change", function () {

        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;

        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();

            reader.onload = function (e) {

                // count rows and columns
                var rows = e.target.result.split("\n"); 
                var columns = rows[0].split(";").length;
                var rowsLength = rows.length - 1;

                var rowsAndTimeRemainingEstimation = $("<p />");

                var estTimeRemaining = Math.round((rowsLength / 100) / 60);

                if(columns == 24)
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + rowsLength + " socios, esto puede demorar alrededor de " + estTimeRemaining + " minutos. Desea continuar?");
                else 
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + columns + " columnas; deberia tener 24. Desea continuar?")

                btnSocios.removeAttr("disabled");

                rowsInfo.removeAttr("hidden");

                $("#rowsInfoContainer").html('');
                $("#rowsInfoContainer").append(rowsAndTimeRemainingEstimation);
            }
            reader.readAsText($("#flpArchivo")[0].files[0]);
        }
        else {
            alert("This browser does not support HTML5.");
        }

    });

    btnSocios.on("click", function () {

        $("#rowsInfoContainer").html('');

        $("#rowsInfoContainer").append("cargando..");

    });

}

function asignacionController() {

    $("#flpAsignacion").on("change", function () {

        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;

        console.log($("#flpAsignacion").val().toLowerCase());

        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();

            reader.onload = function (e) {

                // count rows and columns
                var rows = e.target.result.split("\n");
                var columns = rows[0].split(";").length;
                var rowsLength = rows.length - 1;

                var rowsAndTimeRemainingEstimation = $("<p />");

                var estTimeRemaining = Math.round((rowsLength / 100) / 60);

                if (columns == 6)
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + rowsLength + " asignaciones, esto puede demorar alrededor de " + estTimeRemaining + " minutos. Desea continuar?");
                else
                    rowsAndTimeRemainingEstimation.append("El archivo contiene " + columns + " columnas; deberia tener 6. Desea continuar?")

                btnAsignacion.removeAttr("disabled");

                rowsInfo.removeAttr("hidden");

                $("#rowsInfoContainer").html('');
                $("#rowsInfoContainer").append(rowsAndTimeRemainingEstimation);
            }
            reader.readAsText($("#flpAsignacion")[0].files[0]);
        }
        else {
            alert("This browser does not support HTML5.");
        }

    });

    $("#btnUploadAsignacion").on("click", function () {

        $("#rowsInfoContainer").html('');

        $("#rowsInfoContainer").append("cargando..");

    });

}