﻿function toggleTipoAlerta() {
    if ($("#ddlTipo").val() == "Por monto") {
        $("#txtCantTR").val("");

        $("#divCantTR").hide();
        //$("#divFecha").hide();

        $("#divMontoDesde").show();
        $("#divMontoHasta").show();
    }
    else if ($("#ddlTipo").val() == "Por cantidad") {
        $("#divCantTR").show();
        //$("#divFecha").show();

        $("#txtMontoDesde,#txtMontoHasta").val("");

        $("#divMontoDesde").hide();
        $("#divMontoHasta").hide();
    }
    else {//CPor monto y cantidad
        $("#divCantTR").show();
        //$("#divFecha").show();
        $("#divMontoDesde").show();
        $("#divMontoHasta").show();
    }
}

function mostrarComboPeriodo() {
    $("#cmbPeriodo").show();
    $("#txtFecha").val("");
    $("#textFecha").hide();

}
function mostrartxtFecha() {
    $("#cmbPeriodo").hide();
    $("#textFecha").show();
}

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        if ($("#ddlEntidad").val() == "0") {
            $('#divOK').hide();
            $("#divError").html("Seleccione un tipo de entidad");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else if (
            ($("#ddlTipo").val() == "Por monto" || $("#ddlTipo").val() == "Por monto y cantidad")
            && ($("#txtMontoDesde").val() == "" || $("#txtMontoHasta").val() == "")) {
            $('#divOK').hide();
            $("#divError").html("Ingrese los montos");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else if (
            ($("#ddlTipo").val() == "Por cantidad" || $("#ddlTipo").val() == "Por monto y cantidad")
            && $("#txtCantTR").val() == "") {
            $('#divOK').hide();
            $("#divError").html("Ingrese la cantidad");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else {
            var entidad = "";
            var idEntidad = null;
            var isValid = false;

        if($("#ddlEntidad").val() == "2") {
            entidad = "marca"
            if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "") {
                isValid = true;
                idEntidad = parseInt($("#ddlMarcas").val());
            }
            else
                isValid = false;
        }
        else if ($("#ddlEntidad").val() == "3") {
            entidad = "comercio";
            //if ($("#ddlComercios").val() != null && $("#ddlComercios").val() != "") {
            if ($("#searchable").val() != null && $("#searchable").val() != "") {
                isValid = true;
                //idEntidad = parseInt($("#ddlComercios").val());
            }
            else
                isValid = false;
        }
        else if ($("#ddlEntidad").val() == "4") {
            entidad = "empresa";
            if ($("#ddlEmpresas").val() != null && $("#ddlEmpresas").val() != "") {
                isValid = true;
                idEntidad = parseInt($("#ddlEmpresas").val());
            }
            else
                isValid = false;
        }

        if (!isValid)
            error("Seleccione una " + entidad);
        else {
            if ($("#ddlEntidad").val() == "3") {
                var res = $("#searchable").val();
                
                for (i = 0; i < res.length; ++i) {
                    idEntidad = res[i];
                    crearAlerta(entidad, idEntidad);
                }
            }
            else
                crearAlerta(entidad, idEntidad);
                
            }
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function crearAlerta(entidad, idEntidad) {

    var info = "{ id: " + parseInt($("#hdnID").val())
        + ", nombre: '" + $("#txtNombre").val()
        + "', entidad: '" + entidad
        + "', idEntidad: " + idEntidad
        + ", prioridad: '" + $("#ddlPrioridad").val()
        + "', tipo: '" + $("#ddlTipo").val()
        + "', montoDesde: '" + $("#txtMontoDesde").val()
        + "', montoHasta: '" + $("#txtMontoHasta").val()
        + "', cantidad: '" + $("#txtCantTR").val()
        + "', fecha: " + parseInt($("#ddlFecha").val())
        + ", activa: " + $("#chkActiva").is(':checked')
        + ", restringida: " + $("#rdbDeterminadas").is(':checked')
        + "}";

    $.ajax({
        type: "POST",
        url: "alertas-confige.aspx/guardar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $('#divOK').show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

$(document).ready(function () {
    configControls();
    if ($("#hdnEntidad").val() == "marca")
        $(".marcas").show();
    if ($("#hdnEntidad").val() == "comercio")
        $(".comercios").show();
    if ($("#hdnEntidad").val() == "empresa")
        $(".empresas").show();
});

$("#ddlEntidad").change(function cmbChange() {
    limpiarCombos();
    if ($("#hdnID").val() == "0") {
        if ($("#ddlEntidad").val() == "0") {
            $(".franquicias").hide();
            $(".marcas").hide();
            $(".comercios").hide();
            $(".empresas").hide();

        }
        else if ($("#ddlEntidad").val() == "2") {
            $(".franquicias").hide();
            $(".marcas").show();
            $(".comercios").hide();
            $(".empresas").hide();
        }
        else if ($("#ddlEntidad").val() == "3") {
            $(".franquicias").hide();
            $(".marcas").hide();
            $(".comercios").show();
            $(".empresas").hide();
        }
        else if ($("#ddlEntidad").val() == "4") {
            $(".franquicias").hide();
            $(".marcas").hide();
            $(".comercios").hide();
            $(".empresas").show();
        }
    }
});

function configControls() {
    configDatePicker();
    $(".chosen").chosen();
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    $("#txtMontoDesde, #txtMontoHasta, #txtCantTR").numeric();
    toggleTipoAlerta();
    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
    }
    else
        $("#litTitulo").html("Alta de Alerta");

    configurarMultiselect();
}

function configurarMultiselect() {
    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más empresas"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
}

function error(msj) {
    $('#divOK').hide();
    $("#divError").html(msj);
    $("#divError").show();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function limpiarCombos() {
    $("#ddlMarcas").val("");
    //$("#ddlComercios").val("");
    $("#searchable").val("");
    $("#ddlEmpresas").val("");
}