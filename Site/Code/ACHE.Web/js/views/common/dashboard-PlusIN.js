﻿
$(document).ready(function () {
    var cargarDatosfranq = true;
    var cargarDatosadmin = true;
    var idFranquicia = parseInt($("#hdnIDFranquicias").val());
    var idAdmin = parseInt($("#hdnIDAdmin").val());
    if (idFranquicia == -1 && idAdmin != -1) { //perfil admin sin seleccionar franquicia
        $("#formFranquicia").show();
        $(".admin").show();
        $(".franquicia").hide();
        cargarDatosfranq = false;
    } else if (idFranquicia != -1 && idAdmin == -1) { // perfil franquicia
        $(".admin").hide();
        $("#formFranquicia").hide();
        $(".franquicia").show();
        var cargarDatosadmin = false;
    } else { // perfil admin seleccionando franquicia
        $("#formFranquicia").show();
        $(".admin").show();
        $(".franquicia").show();
    }

    if (cargarDatosadmin) {
        setTimeout("ObtenerCantidadSociosPlusIN()", 1000);
        setTimeout("ObtenerTotalFacturacionPLUSIN()", 1000);
    }
    if (cargarDatosfranq) {
        setTimeout("ObtenerCantidadSociosPlusINFranq()", 1000);
        setTimeout("ObtenerFacturacionPLUSINFranq()", 1000);

    }
});

function mostrarReporte(idFranquicia) {
    window.location.href = "Dashboard-PlusIN.aspx?IDFranquicia=" + idFranquicia;
}



function ObtenerCantidadSociosPlusIN() {
    //$("#resultado_facturacion_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-PlusIN.aspx/obtenerTotalSociosPlusIN",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cantSocios_plusIN").html(data.d);
            }
        }
    });
}


function ObtenerTotalFacturacionPLUSIN() {
    //$("#resultado_facturacion_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard-PlusIN.aspx/obtenerTotalFacturacionPLUSIN",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_facturacion_plusIN").html("$ " + data.d);
            }
        }
    });
}


function ObtenerCantidadSociosPlusINFranq() {
    $.ajax({
        type: "POST",
        data: "{ IDFranquicia: " + parseInt($("#hdnIDFranquicias").val()) + "}",
        url: "dashboard-PlusIN.aspx/obtenerTotalSociosPlusINFranq",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_cantSocios_plusIN_Franq").html(data.d);
            }
        }
    });
}


function ObtenerFacturacionPLUSINFranq() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        data: "{ IDFranquicia: " + parseInt($("#hdnIDFranquicias").val()) + "}",
        url: "dashboard-PlusIN.aspx/obtenerTotalFacturacionPLUSINFranq",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_facturacion_plusIN_Franq").html(data.d);
            }
        }
    });
}


