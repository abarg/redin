﻿function mostrarTablaVerificaciones() {
    $("#tblVerificacionesPOS").hide();

    if ($("#formVerifPos").valid()) {
        var info = "{ nroReferencia: '" + $("#txtNroDeReferencia").val()
                 + "'}";
        $.ajax({
            type: "POST",
            url: "VerificacionesPOSe.aspx/verificar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#divError").hide();
                $("#divOk").hide();
                $("#tblVerificacionesPOS").show();
                GenerarTabla();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
            }
        });
    }
}

function guardar() {
    var msg;
    var completo = true;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
        
        if ($(this).attr("id") == "cmbEstadoCompras" && ($(this).attr("value") == "")) {
            completo = false;
        } else if ($(this).attr("id") == "cmbEstadoGift" && ($(this).attr("value") == "")) {
            completo = false;
        } else if ($(this).attr("id") == "cmbEstadoCanjes" && ($(this).attr("value") == "")) {
            completo = false;
        }
        
        if (!($(this).attr("id") == "chkCalco") && !($(this).attr("id") == "chkDisplay") && !($(this).attr("id") == "chkCalcoPuerta") && !($(this).attr("id") == "chkFolletos")) {
            info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
        } else {
            info += "-" + $(this).attr("id") + "#" + $(this).is(":checked");
        }

    });


    info += "', nroReferencia: '" + $("#txtNroDeReferencia").val()
        +  "', completo: " + completo
         + "}";

    $.ajax({
        type: "POST",
        url: "VerificacionesPOSe.aspx/guardar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#divError").hide();
            $("#divOk").show();
            $("#tblVerificacionesPOS").hide();
            $("#txtNroDeReferencia").val("");

        },
        error: function (response) {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").hide();
            $("#divOk").hide();
        }
    });
    
}

function GenerarTabla() {

    var info = "{ nroReferencia: '" + $("#txtNroDeReferencia").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "VerificacionesPOSe.aspx/generarTabla",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data)
            if (data != null) {
                $("#bodyVerificacionesPOS").html(data.d);
            }
        }
    });
}

function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

$(document).ready(function () {

    $('#formVerifPos').validate({

        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    var nroRef = GetURLParameter('NroRef');
    if (nroRef != undefined && nroRef!="") {
        $("#txtNroDeReferencia").val(nroRef);
        mostrarTablaVerificaciones();
    }
 
});
