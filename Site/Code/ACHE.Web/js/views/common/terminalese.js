﻿$(document).ready(function () {
    var idFranquicia = $("#hdnIDFranquicias").val();
    if (idFranquicia > 0) {
        $("#ddlFranquicias").attr("disabled", true);

    }

    if (parseInt($("#hfIDTerminal").val()) == 0) {
        provinciasByPaises2();
    }

    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        provinciasByPaises();
    });

    $("#ddlPais2").change(function () {
        $('#ddlProvincia2').html('');
        provinciasByPaises2();
    });

    var queryString = getUrlParameter("modo");
    if (queryString == "I") {
        $($("#Tabs ul li a")[14]).trigger("click");
        $("#formButtons").hide();
    }

    configControls();
    $("#ddlFidely1").hide();
    $("#ddlFidely2").hide();
    $("#ddlFidely3").hide();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    toggleTabs();

    if ($("#hfIDTerminal").val() != "0") {
        $("#litTitulo").html("Edición de terminal " + $("#txtPOSTerminal").val());


        $(".tab-pane").click(function () {
            $("#hdnTabImg").val("0")
        });

        $("#tabImagenes").click(function () {
            $("#hdnTabImg").val("1")
        });

    }
    else
        $("#litTitulo").html("Alta de Terminal");
});


function filter() {
    $("#divErrorUsuario").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

/*** FIN USUARIOS ***/

/*** PRUEBAS POS ***/
function agregarPrueba() {
    $("#divErrorPrueba").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#hdnIDVerificacionPOS").val() == "")
        $("#hdnIDVerificacionPOS").val("0");

    var info = "{ idComercio: " + parseInt($("#hfIDComercio").val())
       + ", IDVerificacionPOS: " + parseInt($("#hdnIDVerificacionPOS").val())
       + ", estadoCanjes: '" + $("#cmbEstadoCanjes").val()
       + "', estadoGift: '" + $("#cmbEstadoGift").val()
       + "', estadoCompras: '" + $("#cmbEstadoCompras").val()
       + "', observacionesCanjes: '" + $("#txtObservacionesPosCanjes").val()
       + "', observacionesGift: '" + $("#txtObservacionesPosGift").val()
       + "', observacionesCompras: '" + $("#txtObservacionesPosCompras").val()
       + "', usuario: '" + $("#txtUsuarioPrueba").val()
       + "', obs: '" + $("#txtObsPrueba").val()
       + "', puntosCanjes: '" + $("#txtPuntosPosCanjes").val()
       + "', puntosGift: '" + $("#txtPuntosPosGift").val()
       + "', puntosCompras: '" + $("#txtPuntosPosCompras").val()
       + "', fechaPrueba: '" + $("#txtFechaPrueba").val()
       + "' }";

    $.ajax({
        type: "POST",
        url: "Terminalese.aspx/ProcesarPrueba",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#txtObservacionesPosCanjes, #txtObservacionesPosGift, #txtObservacionesPosCompras, #txtUsuarioPrueba, #txtObsPrueba, #txtPuntosPosCompras", "#txtPuntosPosCanjes", "#txtPuntosPosGift", "#txtFechaPrueba").val("");
            $("#modalPruebaPOS").modal("hide");
            $("#txtFechaHastaPruebas, #txtFechaDesdePruebas").val("");
            filterPruebas();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorPrueba").html(r.Message);
            $("#divErrorPrueba").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function validarPrueba() {
    $("#divErrorPrueba").hide();

    var isValid = true;
    var msg = "";
    if ($("#txtFechaPrueba").val() == "") {
        isValid = false;
        msg = "Debe ingresar la fecha";
    }
    else if ($("#txtUsuarioPrueba").val() == "") {
        isValid = false;
        msg = "Debe ingresar el usuario que realizó la prueba";
    }
    else if ($("#cmbEstadoCanjes").val() == "" || $("#cmbEstadoGift").val() == "" || $("#cmbEstadoCompras").val() == "") {
        isValid = false;
        msg = "Debe ingresar los estados de las pruebas";
    }

    if (!isValid) {
        $("#divErrorPrueba").show();
        $("#divErrorPrueba").html(msg);
    }
    else
        agregarPrueba();
}

function filterPruebas() {
    $("#divErrorPruebas").hide();
    var gridPuntos = $("#gridPruebas").data("kendoGrid");
    var $filterPuntos = new Array();

    gridPuntos.dataSource.filter($filterPuntos);
}

function verTodosPruebas() {
    $("#txtFechaDesdePruebas, #txtFechaHastaPruebas").val("");
    filterPruebas();
}

function nuevaPrueba() {
    $("#txtObservacionesPosCanjes, #txtObservacionesPosGift, #txtObservacionesPosCompras, #txtUsuarioPrueba, #txtObsPrueba, #txtPuntosPosCompras", "#txtPuntosPosCanjes", "#txtPuntosPosGift", "#txtFechaPrueba").val("");
    $("#txtFechaHastaPruebas, #txtFechaDesdePruebas").val("");
    $("#modalPruebaPOS").modal("show");
}

/*** FIN PRUEBAS POS ***/

/*** PUNTOS DE VENTA ***/

function agregarPuntoVenta() {

    $("#divErrorPuntos").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($('#formComercio').valid()) {

        if ($("#txtPuntoVenta").val() == "") {
            $("#divErrorPuntos").html("Por favor,complete todos los datos");
            $("#divErrorPuntos").show();
        }
        else {

            var fecha = new Date();
            var fechaActual = fecha.getDate() + "/"
                        + (fecha.getMonth() + 1) + "/"
                        + fecha.getFullYear();
            var esDefault = $('#chkDefault').is(':checked');

            var info = "{ IDComercio: " + parseInt($("#hfIDComercio").val())
               + ", IDPuntoVenta: " + parseInt($("#hfIDPuntoVenta").val())
               + ", numero: " + $("#txtPuntoVenta").val()
               + ", fechaAlta: '" + fechaActual
               + "', esDefault: '" + esDefault
               + "' }";

            //alert(info);

            $.ajax({
                type: "POST",
                url: "Terminalese.aspx/procesarPunto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtPuntoVenta").val("");
                    $("#btnAgregarPunto").html("Agregar");
                    filterPuntos();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    //alert('');
                    $("#divErrorPuntos").html(r.Message);
                    $("#divErrorPuntos").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
}

function filterPuntos() {
    $("#divErrorPuntos").hide();
    var grid = $("#gridPuntos").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

/*** FIN PUNTOS DE VENTA ***/

function showFidely() {
    if ($("#ddlPOSTipo").val() == "FIDELY") {
        //$("#ddlVisa1").hide();
        //$("#ddlFidely1").show();
        //$("#ddlFidely2").show();
        //$("#ddlFidely3").show();
    }
    else {
        //$("#ddlVisa1").show();
        $("#ddlFidely1").hide();
        $("#ddlFidely2").hide();
        $("#ddlFidely3").hide();
    }
}

function grabar() {

    $("#divError").hide();
    $("#divOk").hide();
    $('#formComercio').validate();

    if ($('#formComercio').valid()) {

        var idMarca = 0;
        if ($("#ddlMarcas").val() != "")
            idMarca = parseInt($("#ddlMarcas").val());

        var idDealer = 0;
        if ($("#ddlDealer").val() != "")
            idDealer = parseInt($("#ddlDealer").val());

        var idFranquicia = 0;
        if ($("#ddlFranquicias").val() != "")
            idFranquicia = parseInt($("#ddlFranquicias").val());

        var idZona = 0;
        if ($("#cmbZona").val() != "" && $("#cmbZona").val() != null)
            idZona = parseInt($("#cmbZona").val());
        
        //var info = "{ IDComercio: '" + $("#hfIDComercio").val()

        var idComercio = 0;
        if ($("#ddlComercios").val() != "" && $("#ddlComercios").val() != null)
            idComercio = parseInt($("#ddlComercios").val());
        //else
        //    {                    
        //        $("#divError").html("Debe seleccionar un comercio");
        //        $("#divError").show();
        //        $('html, body').animate({ scrollTop: 0 }, 'slow');
        //    }

        var info = "{ IDComercio: '" + idComercio
        + "', IDTerminal: '" + $("#hfIDTerminal").val()
            /*ARANCEL & PUNTOS*/ //59 campos
        + "', Descuento: '" + $("#txtDescuento").val()
        + "', Descuento2: '" + $("#txtDescuento2").val()
        + "', Descuento3: '" + $("#txtDescuento3").val()
        + "', Descuento4: '" + $("#txtDescuento4").val()
        + "', Descuento5: '" + $("#txtDescuento5").val()
        + "', Descuento6: '" + $("#txtDescuento6").val()
        + "', Descuento7: '" + $("#txtDescuento7").val()
        + "', ModalidadDebito: " + $("#chkModalidadDebito").is(':checked')
        + ", ModalidadCredito: " + $("#chkModalidadCredito").is(':checked')
        + ", ModalidadEfectivo: " + $("#chkModalidadEfectivo").is(':checked')
        + ", DescripcionDescuento: '" + $("#txtDescuentoDescripcion").val()
        + "', DescuentoVip: '" + $("#txtDescuentoVip").val()
        + "', DescuentoVip2: '" + $("#txtDescuentoVip2").val()
        + "', DescuentoVip3: '" + $("#txtDescuentoVip3").val()
        + "', DescuentoVip4: '" + $("#txtDescuentoVip4").val()
        + "', DescuentoVip5: '" + $("#txtDescuentoVip5").val()
        + "', DescuentoVip6: '" + $("#txtDescuentoVip6").val()
        + "', DescuentoVip7: '" + $("#txtDescuentoVip7").val()
        + "',PuntosVip: '" + $("#txtPuntosVip1").val()
        + "',PuntosVip2: '" + $("#txtPuntosVip2").val()
        + "',PuntosVip3: '" + $("#txtPuntosVip3").val()
        + "',PuntosVip4: '" + $("#txtPuntosVip4").val()
        + "',PuntosVip5: '" + $("#txtPuntosVip5").val()
        + "',PuntosVip6: '" + $("#txtPuntosVip6").val()
        + "',PuntosVip7: '" + $("#txtPuntosVip7").val()
        + "', MulPuntosVip: '" + $("#txtMulPuntosVip").val()
        + "', MulPuntosVip2: '" + $("#txtMulPuntosVip2").val()
        + "', MulPuntosVip3: '" + $("#txtMulPuntosVip3").val()
        + "', MulPuntosVip4: '" + $("#txtMulPuntosVip4").val()
        + "', MulPuntosVip5: '" + $("#txtMulPuntosVip5").val()
        + "', MulPuntosVip6: '" + $("#txtMulPuntosVip6").val()
        + "', MulPuntosVip7: '" + $("#txtMulPuntosVip7").val()
        + "', ModalidadDebitoVip: " + $("#chkModalidadDebitoVip").is(':checked')
        + ", ModalidadCreditoVip: " + $("#chkModalidadCreditoVip").is(':checked')
        + ", ModalidadEfectivoVip: " + $("#chkModalidadEfectivoVip").is(':checked')
        + ", DescripcionDescuentoVip: '" + $("#txtDescuentoDescripcionVip").val()
       + "', POSArancel: '" + $("#txtArancel").val()
        + "', Arancel2: '" + $("#txtArancel2").val()
        + "', Arancel3: '" + $("#txtArancel3").val()
        + "', Arancel4: '" + $("#txtArancel4").val()
        + "', Arancel5: '" + $("#txtArancel5").val()
        + "', Arancel6: '" + $("#txtArancel6").val()
        + "', Arancel7: '" + $("#txtArancel7").val()
        + "', POSPuntos: '" + $("#txtPuntos").val()
        + "', Puntos2: '" + $("#txtPuntos2").val()
        + "', Puntos3: '" + $("#txtPuntos3").val()
        + "', Puntos4: '" + $("#txtPuntos4").val()
        + "', Puntos5: '" + $("#txtPuntos5").val()
        + "', Puntos6: '" + $("#txtPuntos6").val()
        + "', Puntos7: '" + $("#txtPuntos7").val()
        + "', CobrarUsoRed: " + $("#chkCobrarUsoRed").is(':checked')
        + ", MultPuntos1: " + $("#txtMulPuntos1").val()
        + ", MultPuntos2: " + $("#txtMulPuntos2").val()
        + ", MultPuntos3: " + $("#txtMulPuntos3").val()
        + ", MultPuntos4: " + $("#txtMulPuntos4").val()
        + ", MultPuntos5: " + $("#txtMulPuntos5").val()
        + ", MultPuntos6: " + $("#txtMulPuntos6").val()
        + ", MultPuntos7: " + $("#txtMulPuntos7").val()
        + ", Affinity: '" + $("#txtAffinity").val()

        /*DOMICILIO */ // 11 Campos
        + "', Pais: '" + $("#ddlPais2 option:selected").html()
        + "', Provincia: '" + $("#ddlProvincia2").val()
        + "', Ciudad: '" + $("#ddlCiudad2").val()
        + "', Domicilio: '" + $("#txtDomicilio2").val()
        + "', CodigoPostal: '" + $("#txtCodigoPostal2").val()
        + "', TelefonoDom: '" + $("#txtTelefonoDom2").val()
        + "', Fax: '" + $("#txtFax2").val()
        + "', PisoDepto: '" + $("#txtPisoDepto2").val()
        + "', Lat: '" + $("#txtLatitud").val()
        + "', Long: '" + $("#txtLongitud").val()
        + "', IDZona: " + idZona

          /*DATOS PRINCIPALES*/ //19 campos
        + ", POSTipo: '" + $("#ddlPOSTipo").val()
        + "', POSSistema: '" + $("#ddlPOSSistema").val()
        + "', POSTerminal: '" + $("#txtPOSTerminal").val()
        + "', POSEstablecimiento: '" + $("#txtPOSEstablecimiento").val()
        + "', POSMarca: '" + $("#txtPOSMarca").val()
        + "', ModeloPOS: '" + $("#txtModeloPOS").val()
        + "', SimcardPOS: '" + $("#txtSimcardPOS").val()
        + "', EmpresaCelularPOS: '" + $("#ddlCelularEMpresaPOS").val()
        + "', POSObservaciones: '" + $("#txtPOSObservaciones").val()
        + "', POSFechaActivacion: '" + $("#txtPOSFechaActivacion").val()
        + "', POSFechaReprogramacion: '" + $("#txtPOSFechaReprogramacion").val()
        + "', Activo: '" + $("#chkActivo").is(':checked')
        + "', Reprogramado: '" + $("#chkPOSReprogramado").is(':checked')
        + "', Invalido: '" + $("#chkInvalido").is(':checked')
        + "', FidelyUsuario: '" + $("#txtFidelyUsuario").val()
        + "', FidelyPwd: '" + $("#txtFidelyPwd").val()
        + "', FidelyClaveLicencia: '" + $("#txtFidelyLicencia").val()
        + "', posWeb: " + $("#chkPosWeb").is(':checked')
        + ", costoPosWeb: '" + $("#txtCostoPosWeb").val()

        /*GIFTCARD*/
        + "', giftcardCarga: '" + $("#txtGiftcardCarga").val()
        + "', giftcardDescarga: '" + $("#txtGiftcardDescarga").val()
        + "', giftcardCobrarUsored: " + $("#chkGiftcardCobrarUsored").is(':checked')
        + ", giftcardPosWeb: " + $("#chkGiftcardPosWeb").is(':checked')
        + ", giftcardGeneraPuntos: " + $("#chkGiftcardGeneraPuntos").is(':checked')
        + ", giftcardCostoPosWeb: '" + $("#txtGiftcardCostoPosWeb").val()

        + "', cuponInArancel: '" + $("#txtCuponINArancel").val()
        + "', cuponInCobrarUsored: " + $("#chkCuponINCobrarUsoRed").is(':checked')
        + ", CostoPOSPropio: '" + $("#txtCostoPOSPropio").val()
        + "'}";

        $.ajax({
            type: "POST",
            url: "Terminalese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#hfIDTerminal").val(data.d);

                $("#litTitulo").html("Edición de terminal " + $("#txtPOSTerminal").val());

                toggleTabs();
                filter();
                //window.location.href = "Terminalese.aspx?IDTerminal=" + $("#hfIDTerminal").val();

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function toggleTabs() {
    if ($("#hfIDTerminal").val() != "0") {
        $($("#Tabs ul li a")[8]).removeClass("hide");
        $($("#Tabs ul li a")[10]).removeClass("hide");
        $($("#Tabs ul li a")[13]).removeClass("hide");
        $("#divUploadLogo").show();
        $("#divUploadFicha").show();
    }
    else {
        $($("#Tabs ul li a")[8]).addClass("hide");
        $($("#Tabs ul li a")[10]).addClass("hide");
        $($("#Tabs ul li a")[13]).addClass("hide");
        $($("#Tabs ul li a")[14]).addClass("hide");
    }

    toggleButtons();
}

function toggleButtons() {
    $($("#Tabs ul li a")[0]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[1]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[2]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[3]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[4]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[5]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[6]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[7]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[8]).click(function () {
        $("#formButtons").hide();
    });
    $($("#Tabs ul li a")[9]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[10]).click(function () {
        $("#formButtons").hide();
    });
    $($("#Tabs ul li a")[11]).click(function () {
        $("#formButtons").show();
    });
    $($("#Tabs ul li a")[13]).click(function () {
        $("#formButtons").hide();
    });
    $($("#Tabs ul li a")[14]).click(function () {
        $("#formButtons").hide();
        $("#errorImg").hide();

    });
}

function configControls() {
    configDatePicker();

    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtSDS, #txtNroDocumento, #txtNumEst, #txtComercioID, #txtEstado, #txtMulPuntos1, #txtMulPuntos2, #txtMulPuntos3, #txtMulPuntos4, #txtMulPuntos5, #txtMulPuntos6, #txtMulPuntos7").numeric();
    $("#txtFormaPago_NroCuenta, #txtFormaPago_CBU, #txtFormaPago_NroTarjeta, #txtFormaPago_FechaVto, #txtFormaPago_CodigoSeg").numeric();
    $("#txtContacto_Documento").numeric();
    $("#txtDescuento, #txtDescuento2, #txtDescuento3, #txtDescuento4, #txtDescuento5, #txtDescuento6, #txtDescuento7, #txtCostoFijo").numeric();
    $("#txtDescuentoVip, #txtDescuentoVip2, #txtDescuentoVip3, #txtDescuentoVip4, #txtDescuentoVip5, #txtDescuentoVip6, #txtDescuentoVip7").numeric();
    $("#txtMulPuntosVip, #txtMulPuntosVip2, #txtMulPuntosVip3, #txtMulPuntosVip4, #txtMulPuntosVip5, #txtMulPuntosVip6, #txtMulPuntosVip7").numeric();
    $("#txtPuntosVip1, #txtPuntosVip2, #txtPuntosVip3, #txtPuntosVip4, #txtPuntosVip5, #txtPuntosVip6, #txtPuntosVip7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    $("#txtGiftcardCarga, #txtGiftcardDescarga, #txtCuponINArancel").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtPuntoVenta").numeric();
    $("#txtMulPuntos1,#txtMulPuntos2,#txtMulPuntos3,#txtMulPuntos4,#txtMulPuntos5,#txtMulPuntos6,#txtMulPuntos7").numeric();

    $("#txtPuntos, #txtPuntos2, #txtPuntos3, #txtPuntos4, #txtPuntos5, #txtPuntos6, #txtPuntos7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtArancel, #txtArancel2, #txtArancel3, #txtArancel4, #txtArancel5, #txtArancel6, #txtArancel7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtCostoPosWeb, #txtGiftcardCostoPosWeb").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    jQuery.validator.addMethod("url", function (value, element) {
        return this.optional(element) || /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:\w]))?)/.test(value);
    }, "Debe ingresar una url válida");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Terminalese.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idTerminal: parseInt($("#hfIDTerminal").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmailUsuarioComercio").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin")
            $("#ddlTipoUsuario").val("A");
        else
            $("#ddlTipoUsuario").val("B");
        $("#btnAgregarUsuario").html("Actualizar");
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Terminalese.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var usuario = dataItem.Usuario;
        var pwd = dataItem.Pwd;
        var info = "{ usuario: '" + usuario
            + "', pwd: '" + pwd
            + "'}";

        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginComercioAutomatico",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/comercios/home.aspx";


            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });


    //GRILLA PUNTOS DE VENTA

    $("#gridPuntos").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDPuntoVenta: { type: "integer" },
                        IDComercio: { type: "integer" },
                        Punto: { type: "integer" },
                        FechaAlta: { type: "date" },
                        EsDefault: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Terminalese.aspx/GetListaGrillaPuntos", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtPuntoVenta").val("");
                        $("#hfIDPuntoVenta").val("0");
                        $("#btnAgregarPuntoVenta").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idComercio: parseInt($("#hfIDComercio").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDPuntoVenta", title: "ID", width: "50px" },
            { field: "Punto", title: "Punto", width: "100px" },
            { field: "FechaAlta", title: "Fecha de alta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "EsDefault", title: "EsDefault", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridPuntos").delegate(".editColumn", "click", function (e) {
        var grid = $("#gridPuntos").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.EsDefault == "Si") {
            $('#chkDefault').attr('checked', true);
        }
        else {
            $('#chkDefault').attr('checked', false);
        }

        $("#hfIDPuntoVenta").val(dataItem.IDPuntoVenta);
        $("#txtPuntoVenta").val(dataItem.Punto);
        $("#btnAgregarPuntoVenta").html("Actualizar");
    });

    $("#gridPuntos").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridPuntos").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Terminalese.aspx/DeletePunto",
                data: "{ id: " + dataItem.IDPuntoVenta + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterPuntos();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    //GRILLA PRUEBAS POS

    $("#gridPruebas").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        FechaPrueba: { type: "date" },
                        IDVerificacionPOS: { type: "integer" },
                        Calco: { type: "string" },
                        Display: { type: "string" },
                        CalcoPuerta: { type: "string" },
                        EstadoCanje: { type: "string" },
                        EstadoCompras: { type: "string" },
                        EstadoGift: { type: "string" },
                        Folletos: { type: "string" },
                        ObservacionesGenerales: { type: "string" },
                        UsuarioPrueba: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Terminalese.aspx/GetListaGrillaPruebas",
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmailUsuarioComercio").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");
                        data = $.extend({ sort: null, filter: null, idComercio: parseInt($("#hfIDComercio").val()), desde: $("#txtFechaDesdePruebas").val(), hasta: $("#txtFechaHastaPruebas").val() }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        sortable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "FechaPrueba", title: "Fecha prueba", width: "100px", format: "{0:dd/MM/yyyy}" },
            { field: "UsuarioPrueba", title: "Usuario", width: "100px" },
            { field: "Calco", title: "Calco", width: "100px" },
            { field: "Display", title: "Display", width: "100px" },
            { field: "CalcoPuerta", title: "Calco puerta", width: "100px" },
            { field: "EstadoCanjes", title: "Estado Canjes", width: "100px" },
            { field: "EstadoCompras", title: "Estado Compras", width: "100px" },
            { field: "EstadoGift", title: "Estado Gift", width: "100px" },
            { field: "Folletos", title: "Folletos", width: "100px" },
            { field: "ObservacionesGenerales", title: "Observaciones", width: "100px" },
            { field: "IDVerificacionPOS", title: "ID", width: "50px", hidden: true },
            /*
            { field: "PuntosCanjes", title: "Puntos canjes", width: "100px" },
            { field: "PuntosCompras", title: "Puntos gift", width: "100px" },
            { field: "PuntosGift", title: "Puntos compras", width: "100px" },

            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
            */
        ]
    });

    $("#gridPruebas").delegate(".editColumn", "click", function (e) {
        var grid = $("#gridPruebas").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hdnIDVerificacionPOS").val(dataItem.IDPruebaPOS);
        $("#cmbEstadoCanjes").val(dataItem.EstadoCanjes);
        $("#cmbEstadoGift").val(dataItem.EstadoGift);
        $("#cmbEstadoCompras").val(dataItem.EstadoCompras);

        var fecha = dataItem.FechaPrueba;
        day = fecha.getDate(),
        month = fecha.getMonth() + 1,
        year = fecha.getFullYear();
        var mes =
        $("#txtFechaPrueba").val(day + "/" + month + "/" + year);

        $("#txtUsuarioPrueba").val(dataItem.UsuarioPrueba);

        $("#txtPuntosPosCanjes").val(dataItem.PuntosCanjes);
        $("#txtObservacionesPosCanjes").val(dataItem.ObservacionesCanjes);

        $("#txtPuntosPosGift").val(dataItem.PuntosGift);
        $("#txtObservacionesPosGift").val(dataItem.ObservacionesGift);

        $("#txtPuntosPosCompras").val(dataItem.PuntosCompras);
        $("#txtObservacionesPosCompras").val(dataItem.ObservacionesCompras);

        $("#txtObsPrueba").val(dataItem.ObservacionesGenerales);

        $("#modalPruebaPOS").modal("show");
    });

    $("#gridPruebas").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridPruebas").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Terminalese.aspx/DeletePrueba",
                data: "{ id: " + dataItem.IDVerificacionPOS + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterPruebas();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    /*if ($("#hfGrabarContacto").val() == "0")
        $($("#Tabs ul li a")[4]).attr("disabled", "disabled");
    else
        $($("#Tabs ul li a")[4]).removeAttr("disabled");
    if ($("#hfGrabarDomicilio").val() == "0") {
        $($("#Tabs ul li a")[5]).attr("disabled", "disabled");
        $($("#Tabs ul li a")[6]).attr("disabled", "disabled");
    }
    else {
        $($("#Tabs ul li a")[5]).removeAttr("disabled");
        $($("#Tabs ul li a")[6]).removeAttr("disabled");
    }*/

    //$($("#Tabs ul li a")[2]).click(function () {
    //    if ($("#rdbFormaPago_Debito")[0].checked == false && $("#rdbFormaPago_Tarjeta")[0].checked == false)
    //        $("#rdbFormaPago_Debito")[0].checked = true;
    //});


}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function provinciasByPaises2() {

    var idPais = 1;
    if ($("#ddlPais2").val() != "")
        idPais = parseInt($("#ddlPais2").val());

    $.ajax({
        type: "POST",
        url: "Comerciose.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia2").append($("<option/>").val(this.ID).text(this.Nombre));
                });

                $("#ddlProvincia2").val("1");
                if (parseInt($("#ddlProvincia2").val()) > 0) {
                    idProv = parseInt($("#ddlProvincia2").val());
                }
                LoadCiudades2(idProv, 'ddlCiudad2');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function LoadCiudades2(provincia, control) {

    $('#' + control).html("<option value=''></option>").trigger('liszt:updated');
    $.ajax({
        type: "POST",
        url: "/modulos/common.aspx/LoadCiudades",
        data: "{idProvincia: " + provincia + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            $(data.d).each(function () {
                $("#" + control).append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $('#' + control).trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}