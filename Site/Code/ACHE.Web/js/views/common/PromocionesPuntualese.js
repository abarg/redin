﻿$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtPuntos, #txtArancel, #txtArancelVip,#txtPuntosVip").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtMulPuntosVip, #txtMulPuntos, #txtDescuento, #txtDescuentoVip").numeric();

    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más comercios"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
    cargarComercios();

    if (parseInt($("#hdnIDMarca").val()) > 0)
        $('.admin').hide();

});

function cargarComercios() {
    $.ajax({
        type: "POST",
        url: "PromocionesPuntualese.aspx/getComercios",
        data: "{ idpromocion: " + $("#hdnIDPromocionPuntual").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            var values = new Array();
            $(data.d).each(function (index) {
                $('#searchable option[value="' + data.d[index] + '"]').attr('selected', true)

            });

            $('#searchable').multiSelect(values);
        },
        error: function (response) {

        }
    });
}

function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var comercios = "";

    if ($("#searchable").val() != null)
        comercios = $("#searchable").val();

    if ($('#formEdicion').valid()) {
        var info = "{ idPromocion: " + $("#hdnIDPromocionPuntual").val()
            + ", comercios: '" + comercios
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', arancel: '" + $("#txtArancel").val()
            + "', arancelVip: '" + $("#txtArancelVip").val()
            + "', puntos: '" + $("#txtPuntos").val()
            + "', mulPuntos: '" + $("#txtMulPuntos").val()
            + "', descuento: '" + $("#txtDescuento").val()
            + "', descuentoVip: '" + $("#txtDescuentoVip").val()
            + "', puntosVip: '" + $("#txtPuntosVip").val()
            + "', mulPuntosVip: '" + $("#txtMulPuntosVip").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "PromocionesPuntualese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "PromocionesPuntuales.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}