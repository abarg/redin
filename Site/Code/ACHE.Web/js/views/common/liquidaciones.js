﻿var oTable = null;
$(document).ready(function () {
    configControls();
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
});
function configControls() {
    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    var myColumns = [
            { title: "Descargar comprobantes", template: "#= renderOptions(data) #", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Ver Detalle' class='editColumn'/></div>" }, title: "Detalle", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "Franquicia", title: "Franquicia", width: "80px" },
            { field: "FechaGeneracion", title: "Fecha Generacion", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaDesde", title: "Fecha Desde", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaHasta", title: "Fecha Hasta", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "Estado", title: "Estado", width: "80px" },
            { field: "IDLiquidacion", title: "IDDealer", width: "50px", hidden: "hidden" },
            { field: "ImporteTotal", title: "Importe Total", width: "50px" }

    ];
    if ($("#hdnIDFranquicia").val() != "0") {
        myColumns.splice(2, 1)
    }

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDLiquidacion: { type: "integer" },
                        IDFranquicia: { type: "string" },
                        Estado: { type: "string" },
                        FechaGeneracion: { type: "date" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        Franquicia: { type: "decimal" },
                        ImporteTotal: { type: "decimal" },

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Liquidaciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: myColumns
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "LiquidacionesDetalle.aspx?IDLiquidacion=" + dataItem.IDLiquidacion;
    });


    $("#grid").delegate(".viewPdfFactura", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        $("#titComprobantes").html("Comprobantes");// + ", " + dataItem.Nombre);

        $.ajax({
            type: "POST",
            url: "Liquidaciones.aspx/GetComprobantes",
            data: "{ id: " + dataItem.IDLiquidacion + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d.Factura != null) {
                    window.open("/files/facturas/" + data.d.Factura);
                } else {
                    alert("No hay ninguna factura cargada");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Liquidaciones.aspx/Delete",
                data: "{ id: " + dataItem.IDLiquidacion + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });

    $("#grid").delegate(".viewPdfTransferencia", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        $("#titComprobantes").html("Comprobantes");// + ", " + dataItem.Nombre);

        $.ajax({
            type: "POST",
            url: "Liquidaciones.aspx/GetComprobantes",
            data: "{ id: " + dataItem.IDLiquidacion + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d.Transferencia != null) {
                    window.open("/files/transferencias/" + data.d.Transferencia);
                } else {
                    alert("No hay comprobanrte de transferencia cargado");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });


}
function renderOptions(data) {
    var html = "";
    if (data.Estado == "Finalizada") {
        html = "<img src='../../img/grid/gridPdf.gif'  style='cursor:pointer' title='Descargar Transferencia' class='viewPdfTransferencia'/>";
        html += "<img src='../../img/grid/gridPdf.gif'  style='cursor:pointer' title='Descargar Factura' class='viewPdfFactura'/>";
    }else if (data.Estado == "Confirmada") {
        html = "<img src='../../img/grid/gridPdf.gif'  style='cursor:pointer' title='Descargar Factura' class='viewPdfFactura'/>";
    }
    return html;
}




function renderOptionsTrans(data) {
    var html = "";
    if (data.Estado == "Finalizada") {
        html = "<img src='../../img/grid/gridPdf.gif'  style='cursor:pointer' title='Descargar Transferencia' class='viewPdfTransferencia'/>";
    }
    return html;
}

function renderOptionsFact(data) {
    var html = "";
    if (data.Estado == "Confirmada" || data.Estado == "Finalizada") {
        html = "<img src='../../img/grid/gridPdf.gif'  style='cursor:pointer' title='Descargar Factura' class='viewPdfFactura'/>";
    }

    return html;
}



function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    if ($("#hdnIDFranquicia").val() == "0") {
        if ($("#ddlFranquicias").val() != "")
            var franquicia = $("#ddlFranquicias").val();
    } else {
        var franquicia = $("#hdnIDFranquicia").val()
    }


    var info = "{ idFranquicia: '" + franquicia
    + "', fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()
        + "'}";


    $.ajax({
        type: "POST",
        url: "Liquidaciones.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}



function filter() {

    $("#imgLoading").hide();
    $("#lnkDownload").hide();


    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();


    if ($("#hdnIDFranquicia").val() == "0") {
        var franquicia = $("#ddlFranquicias").val();
        if (franquicia != "0")
            $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }


    grid.dataSource.filter($filter);

}

