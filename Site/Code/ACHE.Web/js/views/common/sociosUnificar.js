﻿$(document).ready(function () {

    $("#txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });


    $("#tablaSocios").hide();
});

function buscar() {

    $("#divOk").hide();
    $("#divError").hide();

    

    var documento = $("#txtDocumento").val();
    if (documento == "") {
        $("#divError").html("El campo de Nro. de Documento no puede estar vacio");
        $("#divError").show();
    }
    else {
        var info = "{ DNI: '" + $("#txtDocumento").val() +"'}";
    }
        $.ajax({
            type: "POST",
            url: "Socios-Unificar.aspx/generarTabla",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodySociosDetalle").html(data.d);
                    $("#tablaSocios").show();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    
}


function unificarTarjetas(IDSocio,NroDocumento) {

    var info = "{ IDSocio: '" + IDSocio
        + "', NroDocumento: '" + NroDocumento
        + "'}";

    $.ajax({
        type: "POST",
        url: "Socios-Unificar.aspx/UnificarTarjetas",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {            
            $("#tablaSocios").hide();
            $("#txtDocumento").val("")
            $("#divOk").show();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            //$("#imgLoading").hide();
        }
    });

}