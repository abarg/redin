﻿// domicilio fiscal
if ($('#ddlPais').length) {
    $('#ddlPais').on('change', function () {
        $('#nomPais').val($(this).children("option").filter(":selected").text());
        if ($("#rdbFem").length === 0)
            nomenclaturas(1, 0);
        else
            nomenclaturas(1, 1);
    }).trigger('change');
}


// ddlPais2 = domicilio comercial
if ($('#ddlPais2').length) {
    $('#ddlPais2').on('change', function () {
        $('#nomPais').val($(this).children("option").filter(":selected").text());
        if ($("#rdbFem").length === 0)
            nomenclaturas(2, 0);
        else
            nomenclaturas(2, 1);
    }).trigger('change');
}

function nomenclaturas(ddl, socios) {


    if (socios === 1 && $('#nomPais').val() !== 'ARGENTINA') {
        $("#fgMonedero").css('display', 'none');
        $("#fgSube").css('display', 'none');
    }
    else if (socios === 1 && $("#nomPais").val() === 'ARGENTINA') {
        $("#fgMonedero").css('display', '');
        $("#fgSube").css('display', '');
    }

    switch ($('#nomPais').val()) {

        case 'ARGENTINA':
            $("#labelRazon").html(" <span class='f_req'>*</span> Razón Social ");
            $("#labelProvincia").html("<span class='f_req'>*</span> Provincia");
            $("#labelCiudad").html("Ciudad");
            if (ddl === 1) {
             $("#labelProvincia2").html("<span class='f_req'>*</span> Provincia");
             $("#labelCiudad2").html("Ciudad");
            }
            if (socios === 0) {
                $('#listCuit').text('CUIT');

                $('#listDNI').css('display', 'none');
            }
            else {
                $('#listCuit').text('CUIL');

                $('#listDNI').css('display', '');

                $('#listCuit').css('display', '');

                $('#listCedula').css('display', 'none');

            }

            break;

        case 'PANAMA':
            $("#labelRazon").html(" <span class='f_req'>*</span> Razón Social ");
            $("#labelProvincia").html("<span class='f_req'>*</span> Provincia");
            $("#labelCiudad").html("Distrito");
            if (ddl === 1) {
             $("#labelProvincia2").html("<span class='f_req'>*</span> Provincia");
             $("#labelCiudad2").html("Distrito");
            }
            if (socios === 0) {
                $('#listCuit').text('RUT con DV');
                $('#listDNI').css('display', '');

                $('#listDNI').text('NIT');
            }
            else {
                $('#listDNI').css('display', 'none');

                $('#listCuit').css('display', 'none');

                $('#listCedula').css('display', '');

            }
            break;

        case 'URUGUAY':
            $("#labelRazon").html(" <span class='f_req'>*</span> Razón Social ");
            $("#labelProvincia").html("<span class='f_req'>*</span> Departamento");
            $("#labelCiudad").html("Municipio");
            if (ddl === 1) {
             $("#labelProvincia2").html("<span class='f_req'>*</span> Departamento");
             $("#labelCiudad2").html("Municipio");
            }
            if (socios === 0) {
                $('#listCuit').text('RUT');

                $('#listDNI').css('display', 'none');
                }
            else {
                $('#listDNI').css('display', 'none');

                $('#listCuit').css('display', 'none');

                $('#listCedula').css('display', '');

            }

            break;

        case 'PARAGUAY':
            $("#labelRazon").html(" <span class='f_req'>*</span> Razón Social ");
            $("#labelProvincia").html("<span class='f_req'>*</span> Departamento");
            $("#labelCiudad").html("Distrito");
            if (ddl === 1) {
             $("#labelProvincia2").html("<span class='f_req'>*</span> Departamento");
             $("#labelCiudad2").html("Distrito");
            }
            if (socios === 0) {
                $('#listCuit').text('RUC');

                $('#listDNI').css('display', 'none');
            }
            else {
                $('#listDNI').css('display', 'none');

                $('#listCuit').css('display', 'none');

                $('#listCedula').css('display', '');

            }

        case 'CHILE':
            $("#labelRazon").html(" <span class='f_req'>*</span> Razón Social ");
            $("#labelProvincia").html("<span class='f_req'>*</span> Region");
            $("#labelCiudad").html("Comuna");
            if (ddl === 1) {
                $("#labelProvincia2").html("<span class='f_req'>*</span> Region");
                $("#labelCiudad2").html("Comuna");
            }
            if (socios === 0) {
                $('#listCuit').text('RUT');

                $('#listDNI').css('display', 'none');
            }
            else {
                $('#listDNI').css('display', 'none');

                $('#listCuit').css('display', 'none');

                $('#listCedula').css('display', '');

            }

            break;



        default:
            //console.log($(this).val())

    }


}