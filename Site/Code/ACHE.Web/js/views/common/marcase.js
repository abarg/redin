﻿/**LOGO**/
function deleteLogo(file, div) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "marcase.aspx/eliminarLogo",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
            $("#imgLogo").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 1000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "gif") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    //alert("1-" + args.get_errorMessage());
    //$("#hdnAttachment").val("");
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}


function ShowUploadError(msg) {
        //alert("2-" + msg);
     //$("#hdnAttachment").val("");
     $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({scrollTop: 0 }, 'slow');
}



/**FIN LOGO**/

function mostrarMensaje(tipo) {
    switch (tipo) {
        case "B":
            if ($('#chkSMSBienvenida').is(':checked'))
                $("#divBienvenida").show();
            else
                $("#divBienvenida").hide();
            break;
        case "C":
            if ($('#chkSMSCumpleanios').is(':checked'))
                $("#divCumpleanios").show();
            else
                $("#divCumpleanios").hide();
            break;
        case "S":
            if ($('#chkSMS').is(':checked'))
                $(".sms").show();
            else
                $(".sms").hide();
            break;
    }
}

function mostrarMensajeEmail(tipo) {
    switch (tipo) {
        case "BS":
            if ($('#chkEmailRegistroASocio').is(':checked'))
                $("#divBienvenidaASocio").show();
            else
                $("#divBienvenidaASocio").hide();
            break;
        case "C":
            if ($('#chkEmailCumpleanios').is(':checked'))
                $("#divEmailCumpleanios").show();
            else
                $("#divEmailCumpleanios").hide();
            break;
        case "BC":
            if ($('#chkEmailAComercio').is(':checked'))
                $("#divBienvenidaComercio").show();
            else
                $("#divBienvenidaComercio").hide();
            break;

    }
}

function agregarUsuario() {
    $("#divErrorUsuario").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#txtUsuario").val() == "" || $("#txtEmail").val() == "" || $("#txtPwd").val() == "") {
        $("#divErrorUsuario").html("Por favor,complete todos los datos");
        $("#divErrorUsuario").show();
    }
    else {
        var info = "{ IDMarca: " + parseInt($("#hdnID").val())
           + ", IDUsuario: " + parseInt($("#hfIDUsuario").val())
           + ", usuario: '" + $("#txtUsuario").val()
           + "', email: '" + $("#txtEmail").val()
           + "', pwd: '" + $("#txtPwd").val()
           + "', tipo: '" + $("#ddlTipoUsuario").val()
           + "'}";


        $.ajax({
            type: "POST",
            url: "marcase.aspx/procesarUsuario",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtUsuario").val("");
                $("#txtEmail").val("");
                $("#txtPwd").val("");
                $("#hfIDUsuario").val("0");
                $("#btnAgregarUsuario").html("Agregar");
                filter();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorUsuario").html(r.Message);
                $("#divErrorUsuario").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function agregarToken() {
    $("#divErrorToken").hide();
    $("#divOk").hide();
    $("#divError").hide();

    if ($("#publicKey").val() == "" || $("#privateKey").val() == "" || $("#ddlComercios").val() == "") {
        $("#divErrorToken").html("Por favor,complete todos los datos");
        $("#divErrorToken").show();
    }
    else {
        var info = "{ IDMarca: " + parseInt($("#hdnID").val())
            + ", IDComercio: " + parseInt($("#ddlComercios").val())
            + ", publicKey: '" + $("#publicKey").val()
            + "', privateKey: '" + $("#privateKey").val()
            + "', paymentGateaway: '" + $("#gateaway").find('option:selected').val()
            + "'}";


        $.ajax({
            type: "POST",
            url: "marcase.aspx/procesarToken",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#publicKey").val("");
                $("#privateKey").val("");
                loadTokensKendo();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorToken").html(r.Message);
                $("#divErrorToken").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}


function grabar() {

    $("#divError").hide();
    $("#divErrorUsuario").hide();
    $("#divErrorComercio").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var isValid = true;
    if($("#txtEmailAlerta").val() != "" && !EmailsAreValid($("#txtEmailAlerta").val())){
        $("#divError").html("Un email es invalido");
        $("#divError").show();
        isValid = false;
    }

        if ($('#formEdicion').valid() && isValid) {
        var idComercioSMS = 0;

        if ($("#cmbComercioSMS").val() != "" && $("#cmbComercioSMS").val() != null)
            idComercioSMS = parseInt($("#cmbComercioSMS").val());

        var htmlBienvenida = $("#txtEmailBienvenidaASocio").val();
        
        var fechasCompletadas = true;

        var fechaCadu = "";
        if ($("#txtFechaCaducidad").val() != null)
            fechaCadu = $("#txtFechaCaducidad").val()

        if ($("#txtFechaTopeCanje").val() != null)
            fechaTope = $("#txtFechaTopeCanje").val();

        var prov="";
        if ($("#ddlProvincia").val() != null)
            prov = $("#ddlProvincia").val();

        if (fechasCompletadas) {
            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", nombre: '" + $("#txtNombre").val()
                + "', prefijo: '" + $("#txtPrefijo").val()
                + "', affinity: '" + $("#txtAffinity").val()
                + "', arancel: '" + $("#txtArancel").val()
                + "', color: '" + $("#ddlColor").val()
                + "', mostrarSoloPOSPropios: " + $("#chkMostrarSoloPOSPropios").is(':checked')
                + ", mostrarSoloTarjetasPropias: " + $("#chkMostrarSoloTarjetasPropias").is(':checked')
                + ", posWeb: " + $("#chkPosWeb").is(':checked')
                + ", idFranquicia: '" + $("#ddlFranquicias").val()
                + "', tipoCatalogo: '" + $("#ddlCatalogo").val()
                + "', codigo: '" + $("#txtCodigo").val()
                + "', giftcardWeb: " + $("#chkGiftcardWeb").is(':checked')
                + ", cuponInWeb: " + $("#chkCuponIN").is(':checked')
                + ", productos: " + $("#chkProductos").is(':checked')
                + ", sms: " + $("#chkSMS").is(':checked')
                + ", costoSMS: '" + $("#txtCostoSMS").val()
                + "', smsBienvenida: " + $("#chkSMSBienvenida").is(':checked')
                + ", mensajeBienvenida: '" + $("#txtSMSBienvenida").val()
                + "', smsCumpleanios: " + $("#chkSMSCumpleanios").is(':checked')
                + ", mensajeCumpleanios: '" + $("#txtSMSCumpleanios").val()
                + "', idComercioSMS: " + idComercioSMS
                + ", envioEmailRegistroSocio: " + $("#chkEmailRegistroASocio").is(':checked')
                + ", emailBienvenidaSocio: '" + htmlBienvenida
                + "', envioEmailCumpleanios: " + $("#chkEmailCumpleanios").is(':checked')
                + ", emailCumpleanios: '" + $("#txtEmailCumpleanios").val()
                + "', envioEmailRegistroComercio: " + $("#chkEmailAComercio").is(':checked')
                + ", emailBienvenidoComercio: '" + $("#txtEmailAComercio").val()
                + "', emailAlertas: '" + $("#txtEmailAlerta").val()
                + "', celularAlertas: '" + $("#txtCelularAlerta").val()
                + "', celularEmpresaAlertas: '" + $("#txtCelularEmpresaAlerta").val()
                + "', razonSocial: '" + $("#txtRazonSocial").val()
                + "', condicionIVA: '" + $("#ddlIVA option:selected").val()
                + "', tipoDoc: '" + $("#ddlTipoDoc option:selected").val()
                + "', nroDoc: '" + $("#txtNroDoc").val()
                + "', Pais: '" + $("#ddlPais option:selected").text()
                + "', Provincia: '" + prov
                + "', Ciudad: '" + $("#ddlCiudad").val()
                + "', Moneda: '" + $("#ddlMoneda option:selected").val()
                + "', Domicilio: '" + $("#txtDomicilio").val()
                + "', PisoDepto: '" + $("#txtPisoDepto").val()
                + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
                + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
                + "', puntoDeVenta: '" + $("#txtPuntoDeVenta").val()
                + "', tipoComprobante: '" + $("#cmbTipoComprobante").val()
                + "', nroComprobante: '" + $("#txtNroComprobante").val()
                + "', costoTransaccional: '" + $("#txtCostoTransaccional").val()
                + "', costoSeguro: '" + $("#txtCostoSeguro").val()
                + "', costoPlusin: '" + $("#txtCostoPlusin").val()
                + "', costoSMS2: '" + $("#txtCostoSMS2").val()
                + "', fechaCaducidad: '" + fechaCadu
                + "', fechaTopeCanje: '" + fechaTope
                + "', tipoTarjeta: '" + $("#ddlTipoTarjeta").val()
                + "', chkFormaPago: '" + $("#chkFormaPago").is(':checked')
                + "', chkNroTicket: '" + $("#chkNroTicket").is(':checked')
                + "', footer1: '" + $("#txtFooter1").val()
                + "', footer2: '" + $("#txtFooter2").val()
                + "', footer3: '" + $("#txtFooter3").val()
                + "', footer4: '" + $("#txtFooter4").val()
                + "', chkFidelidad: '" + $("#chkFidelidad").is(':checked')
                + "', chkMenuGift: '" + $("#chkMenuGift").is(':checked')
                + "', chkChargeGift: '" + $("#chkChargeGift").is(':checked')
                + "', chkLogo: '" + $("#chkLogo").is(':checked')
                + "', chkSeFactura: '" + $("#chkSeFactura").is(':checked')
                + "', cuitEmisor: '" + $("#txtCuitEmisor").val()
                + "', ordenMenu: '" + $("#sortable").sortable('serialize')
                + "', appColor: '" + $("#appColor").val()
                + "', allowSMS: '" + $("#allowSMS").is(':checked')
                + "', allowEmail: '" + $("#allowEmail").is(':checked')
                + "', promptSurvey: '" + $("#promptSurvey").is(':checked')
                + "', urlGestion: '" + $("#urlGestion").val()
                + "', urlLandingPage: '" + $("#urlLandingPage").val()
                + "'}";


            if (parseInt($("#hdnID").val()) == 0) {
                if (confirm("¿Esta seguro que seleccionar el tipo de tarjeta " + $("#ddlTipoTarjeta").val() + " ?")) {
                    $.ajax({
                        type: "POST",
                        url: "marcase.aspx/grabar",
                        data: info,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data, text) {
                            $('#divOK').show();
                            $('html, body').animate({ scrollTop: 0 }, 'slow');

                            $("#hdnID").val(data.d);
                            $("#litTitulo").html("Edición de " + $("#txtNombre").val());

                            $("#divUploadLogo").show();
                            toggleTabs();
                            filter();
                            //filterComercios();
                        },
                        error: function (response) {
                            var r = jQuery.parseJSON(response.responseText);
                            $("#divError").html(r.Message);
                            $("#divError").show();
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }
                    });
                }
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "marcase.aspx/grabar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        $('#divOK').show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');

                        $("#hdnID").val(data.d);
                        $("#litTitulo").html("Edición de " + $("#txtNombre").val());

                        $("#divUploadLogo").show();
                        toggleTabs();
                        filter();
                        //filterComercios();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        }
        else {            
            $("#divError").html('Debe completar tanto la "Fecha de Caducidad" como la de "Tope de Canje" para poder guardar los cambios');
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function filter() {
    $("#divErrorUsuario").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}
/*
function filterComercios() {
    $("#divErrorComercio").hide();
    var grid = $("#gridComercios").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}*/

function toggleTabs() {
    if ($("#hdnID").val() != "0") {
        $($("#Tabs ul li a")[1]).removeClass("hide");
        //$($("#Tabs ul li a")[2]).removeClass("hide");
    }
    else {
        $($("#Tabs ul li a")[1]).addClass("hide");
        //$($("#Tabs ul li a")[2]).addClass("hide");
    }
}

function configControls() {
    $("#txtArancel").numeric();
    $("#txtCostoSMS").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    mostrarMensajeEmail('BS');
    mostrarMensajeEmail('C');
    mostrarMensajeEmail('BC');

    configDatePicker();
    configFechasDesdeHasta2("txtFechaTopeCanje","txtFechaCaducidad");

    $('.htmlEditor').redactor({
        buttons: ['html', 'formatting', 'bold', 'italic', 'deleted',
            'unorderedlist', 'orderedlist', 'outdent', 'indent',
            'image', 'file', 'link', 'alignment', 'horizontalrule'],
        //lang: 'es',
        minHeight: 300 // pixels
    });

    if ($('#chkSMS').is(':checked')) {
        if ($('#chkSMSBienvenida').is(':checked'))
            $("#divBienvenida").show();
        else
            $("#divBienvenida").hide();

        if ($('#chkSMSCumpleanios').is(':checked'))
            $("#divCumpleanios").show();
        else
            $("#divCumpleanios").hide();
        $(".sms").show();
    }
    else {
        $(".sms").hide();
    }

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 150,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9 ñ\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, numeros, puntos, guiones medios y espacios en blanco.");

    loadUsersKendo();

    loadTokensKendo();

    gridMultimarcas();

    //LANDING PAGE
    loadLandingPageKendo();

    //LoadComercios("../common.aspx/LoadComercios", "ddlComercio");
    //LoadComercios("../common.aspx/LoadComercios", "cmbComercioSMS");
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
}

function loadTokensKendo() {

    $("#gridTokens").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDToken: { type: "integer" },
                        publicKey: { type: "string" },
                        privateKey: { type: "string" },
                        Pasarela: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaTokens", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmail").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDToken", title: "ID", width: "50px" },
            { field: "publicKey", title: "publicKey", width: "100px" },
            { field: "privateKey", title: "privateKey", width: "100px" },
            { field: "Pasarela", title: "Pasarela", width: "100px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#gridTokens").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmail").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin") {
            $("#ddlTipoUsuario").val("A");
        } else {
            $("#ddlTipoUsuario").val("B");
            $("#btnAgregarUsuario").html("Actualizar");
        }
    });


    $("#gridTokens").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function loadUsersKendo() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDUsuario: { type: "integer" },
                        Usuario: { type: "string" },
                        Email: { type: "string" },
                        Nombre: { type: "string" },
                        Pwd: { type: "string" },
                        Tipo: { type: "string" },
                        Activo: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmail").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDUsuario", title: "ID", width: "50px" },
            { field: "Usuario", title: "Usuario", width: "100px" },
            { field: "Pwd", title: "Contraseña", width: "100px" },
            { field: "Tipo", title: "Tipo", width: "100px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "Activo", title: "Activo", width: "50px", attributes: { class: "colCenter" } },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#hfIDUsuario").val(dataItem.IDUsuario);
        $("#txtUsuario").val(dataItem.Usuario);
        $("#txtEmail").val(dataItem.Email);
        $("#txtPwd").val(dataItem.Pwd);
        if (dataItem.Tipo == "Admin") {
            $("#ddlTipoUsuario").val("A");
        } else {
            $("#ddlTipoUsuario").val("B");
            $("#btnAgregarUsuario").html("Actualizar");
        }
    });


    $("#grid").delegate(".loginColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var usuario = dataItem.Usuario;
        var pwd = dataItem.Pwd;
        var info = "{ usuario: '" + usuario
            + "', pwd: '" + pwd
            + "'}";

        $.ajax({
            type: "POST",
            url: "/loginAutomatico.aspx/loginMarcaAutomatico",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/marcas/home.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    });


    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/Delete",
                data: "{ id: " + dataItem.IDUsuario + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

$(document).ready(function () {
    configControls();
    $("#txtCostoTransaccional, #txtCostoSeguro,#txtCostoSMS, #txtCostoPlusin,#txtCostoSMS2").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });

    toggleTabs();

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
        $("#divUploadLogo").show();
        provinciasByPaises();
    }
    else
        $("#litTitulo").html("Alta de Marca");


    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        $('#ddlCiudad').html("<option value=''></option>").trigger('list:updated');
        provinciasByPaises();
    });


    $("#sortable").sortable({
        axis: 'y',
        cursor: "move"
    });

    $("#paymentGateaways").on('click', function () {
        loadPaymentGateawaysConfig();
        console.log('click');
    })

});

function paymentGateawayControls() {

    var privateKeyInput = $("#privateKeyField");

    $("#gateaway").on('change', function () {

        var gateaway = $(this).find('option:selected').text();

        switch (gateaway) {
            case ('MercadoPago'):

                $("#privateKey").val("");
                privateKeyInput.show();

                break;
            case ('Bitcoin'):

                $("#privateKey").val("-");
                privateKeyInput.hide();

                break;
        }

    })

}

function loadPaymentGateawaysConfig() {

    paymentGateawayControls();

    var idMarca = parseInt($("#hdnID").val());

    if (idMarca > 0) {
        $.ajax({
            type: "POST",
            url: "Marcase.aspx/LoadComercios",
            data: "{ idMarca: " + idMarca
                + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d != "" && data.d != null) {
                    $.each(data.d, function () {
                        $("#ddlComercios").append($("<option/>").val(this.ID).text(this.Nombre));
                    });

                }
                else {
                    $("#errorBlock").html("La marca no tiene comercios adheridos");

                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }

}

function configFechasDesdeHasta2(date, end) {

    $.validator.addMethod("greaterThan", function () {
        var valid = true;
        var desde = $("#" + date).val();
        var hasta = $("#" + end).val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        return valid;
    }, 'La "fecha tope canje" debe ser menor o igual a la "fecha caducidad"');
    $('form').validate({
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
    //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

    $('#' + date).change(cleanErrors);
    $('#' + end).change(cleanErrors);
}

function gridMultimarcas() {

    $("#gridMultimarcas").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDMarca: { type: "integer" },
                        IDMultimarca: { type: "integer" },
                        Marca: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaGrillaMultimarcas", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        $("#txtUsuario").val("");
                        $("#txtEmail").val("");
                        $("#txtPwd").val("");
                        $("#hfIDUsuario").val("0");
                        $("#btnAgregarUsuario").html("Agregar");

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "20px" },
            //{ field: "IDMultimarca", title: "IDMultimarca", width: "50px"},
            { field: "IDMarca", title: "ID", width: "20px" },
            { field: "Marca", title: "Marca asociada", width: "100px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Acceder' class='loginColumn'/></div>" }, title: "Acceder", width: "50px" },
        ]
    });


    $("#gridMultimarcas").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#gridMultimarcas").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/DeleteMultimarcas",
                data: "{ id: " + dataItem.IDMultimarca + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filterMultimarcas();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorMultimarcas").html(r.Message);
                    $("#divErrorMultimarcas").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });


    //$("#gridMultimarcas").delegate(".loginColumn", "click", function (e) {
    //    var grid = $("#gridMultimarcas").data("kendoGrid");
    //    var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
    //    var usuario = dataItem.Usuario;
    //    var pwd = dataItem.Pwd;
    //    var info = "{ usuario: '" + usuario
    //        + "', pwd: '" + pwd
    //        + "'}";

    //    $.ajax({
    //        type: "POST",
    //        url: "/loginAutomatico.aspx/loginMarcaAutomatico",
    //        data: info,
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (data, text) {
    //            window.location.href = "/marcas/home.aspx";

    //        },
    //        error: function (response) {
    //            var r = jQuery.parseJSON(response.responseText);
    //            $("#divOk").hide();
    //            $("#divError").html(r.Message);
    //            $("#divError").show();
    //            $('html, body').animate({ scrollTop: 0 }, 'slow');
    //        }
    //    });
    //});




}

function filterMultimarcas() {
        $("#divErrorMultimarcas").hide();
        var grid = $("#gridMultimarcas").data("kendoGrid");
        var $filter = new Array();
        grid.dataSource.filter($filter);

}

function asociarMarca() {

    idMarca = 0;

    if ($("#cmbMarcas").val() > 0)
        idMarca = $("#cmbMarcas").val();


    $.ajax({
        type: "POST",
        url: "marcase.aspx/AsociarMarca",
        data: "{ idMarca1: " + parseInt($("#hdnID").val()) 
                +",idMarca2:"+ idMarca + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            filterMultimarcas();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorMultimarcas").html(r.Message);
            $("#divErrorMultimarcas").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

/*** VALIDATIONS ***/
function validateEmail(field) {
    var regex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;
    return (regex.test(field)) ? true : false;
}

function EmailsAreValid(value) {
    var result = value.split(",");
    for (var i = 0; i < result.length; i++)
        if (!validateEmail(result[i]))
            return false;
    return true;
}

function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPais").val() != "")
        idPais = parseInt($("#ddlPais").val());

    $.ajax({
        type: "POST",
        url: "Comerciose.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                $("#ddlProvincia").val("1");
                if (parseInt($("#ddlProvincia").val()) > 0) {
                    idProv = parseInt($("#ddlProvincia").val());
                    LoadCiudades2(idProv, 'ddlCiudad');
                }
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

/*** TAB LANDING PAGE ***/
function loadLandingPageKendo() {
    $("#gridLandingPage").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Habilitado: { type: "bool" },
                        IDLandingControl: { type: "integer" },
                        Campo: { type: "string" },
                        Seccion: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "marcase.aspx/GetListaLandingPage", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {

                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idMarca: parseInt($("#hdnID").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 300,
        //scrollable: true,
        sortable: true,
        persistSelection: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Habilitado", title: "Habilitado", width: "30px", template: "#if(Habilitado == 'True'){#<div class='text-center'><input  type='checkbox'  class='habilitadoColumn' checked/></div>#}else{#<div class='text-center'><input  type='checkbox'  class='habilitadoColumn'/></div>#}#" },
            { field: "IDLandingControl", title: "ID", width: "30px" },
            { field: "Campo", title: "Campo", width: "150px" },
            { field: "Seccion", title: "Seccion", width: "150px" }
        ]
    });

    $("#gridLandingPage").delegate(".habilitadoColumn", "click", function (e) {
        var grid = $("#gridLandingPage").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea habilitar el campo seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "marcase.aspx/EnableLandingPageControl",
                data: "{ id: " + dataItem.IDLandingControl + ", idMarca:" + parseInt($("#hdnID").val()) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divErrorUsuario").html(r.Message);
                    $("#divErrorUsuario").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else
            return false;
    });
}

/**HEADER**/
function deleteHeaderLanding(file, div) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "'}";

    $.ajax({
        type: "POST",
        url: "marcase.aspx/eliminarHeaderLanding",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
            $("#imgHeaderLanding").attr("src", "http://www.placehold.it/180x120/EFEFEF/AAAAAA");
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}
