﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    //    $('#formEdicion').validate();

    var idMarca = 0
    if ($("#hdnIDMarca").val() != null && parseInt($("#hdnIDMarca").val()) > 0)
        idMarca = parseInt($("#hdnIDMarca").val());
     else if ($("#cmbMarcas").val() != null && $("#cmbMarcas").val() != "")
        idMarca = $("#cmbMarcas").val();
     else
         $("#divError").html("Seleccione una marca");

    if ($('#formPromo').valid()) {
        var idPromocion = parseInt($("#hdnIDPromocion").val());
        var info = "{ idPromocion: " + idPromocion
            + ", idMarca: '" + idMarca
            + "', Titulo: '" + $("#txtTitulo").val()
            + "', Msj1: '" + $("#txtMsj1").val()
            + "', Msj2: '" + $("#txtMsj2").val()
            + "', Msj3: '" + $("#txtMsj3").val()
            + "', Msj4: '" + $("#txtMsj4").val()
            + "', tipoCodigo: '" + $("#txtTipoCodigo").val()
            + "', infoCodificar: '" + $("#txtInfoCodificar").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', activo: " + $("#chkActivo").is(':checked')
            + ", Sexo: '" + $("#ddlSexo").val()
            + "', ImporteDesde: '" + $("#txtImporteDesde").val()
            + "', ImporteHasta: '" + $("#txtImporteHasta").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "'}";


        $.ajax({
            type: "POST",
            url: "Promocionese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Promociones.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}


$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtTipoCodigo,#txtImporteDesde,#txtImporteHasta").numeric();

    if (parseInt($("#hdnIDPromocion").val()) == 0) {
        provinciasByPaises();
    }

    $("#ddlPais").change(function () {
        $('#ddlProvincia').html('');
        provinciasByPaises();
    });
});

function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPais").val() != "")
        idPais = parseInt($("#ddlPais").val());

    $('#ddlCiudad').html("<option value=''></option>").trigger('liszt:updated');

    $.ajax({
        type: "POST",
        url: "Sociose.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvincia").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                var idProv = parseInt($("#ddlProvincia").val());
                LoadCiudades2(idProv, 'ddlCiudad');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}