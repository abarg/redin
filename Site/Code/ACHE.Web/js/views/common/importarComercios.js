﻿function verErrores()
{
    $.ajax({
        type: "POST",
        url: "importarComercios.aspx/obtenerErrores",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}