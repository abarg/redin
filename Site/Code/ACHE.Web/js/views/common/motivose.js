﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var idMotivo = 0
    if ($("#hdnIDMotivo").val() != "" && $("#hdnIDMotivo").val() != null)
        idMotivo = parseInt($("#hdnIDMotivo").val())

    if ($('#formCiudad').valid()) {
        var info = "{ idMotivo: " + idMotivo
            + ", idMarca: " + $("#ddlMarcas").val()
            + ", nombre: '" + $("#txtNombre").val()
            + "', desc: '" + $("#txtDesc").val()
             + "', sumaPuntos: " + $("#chkSumaPuntos").is(':checked')
            + "}";


        $.ajax({
            type: "POST",
            url: "Motivose.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {


                window.location.href = "Motivos.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}


