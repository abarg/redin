﻿$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
   configControls();
    //buscar();
});


function filter() {

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    var franquicia = parseInt($("#ddlFranquicias").val());
    if (franquicia != "") {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: franquicia });
    }

    
    grid.dataSource.filter($filter);
}

function configControls() {

    $("#txtFechaDesde, #txtFechaHasta, #ddlFranquicias").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    var idFranq = parseInt($("#hdnIDFranquicia").val());
   
    var myColumns = [
        { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "ID", title: "Nro Formulario", width: "70px" },
            { field: "Estado", title: "Estado", width: "80px" },
            { title: "Counter", template: "#= renderOptions(data) #", width: "80px" },
            { field: "Cliente", title: "Cliente", width: "80px" },
            { field: "FechaPedido", title: "Fecha Pedido", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaEnvioProveedor", title: "Fecha Envio", width: "95px", format: "{0:dd/MM/yyyy}" },
            { field: "Franquicia", title: "Franquicia", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "UnidadNegocio", title: "Unidad", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "CantTarjetas", title: "Cant Tarjetas", width: "80px" }
            
    ];
    if (idFranq > 0) {
        myColumns.splice(4, 1);
        $(".franquicia").hide();
    }

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        FechaPedido: { type: "date" },
                        Franquicia: { type: "string" },
                        UnidadNegocio: { type: "string" },
                        ID: { type: "integer" },
                        FechaEnvioProveedor: { type: "date" },
                        Estado: { type: "string" },
                        Counter: { type: "string" },
                        Cliente: { type: "string" },
                        CantTarjetas: { type: "integer" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "FormularioAprobacionTarjetas.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: myColumns
           

        
    });
    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "FormularioAprobacionTarjetase.aspx?IDFormulario=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "FormularioAprobacionTarjetas.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
   
}

function Nuevo() {
    window.location.href = "FormularioAprobacionTarjetase.aspx";
}

function renderOptions(data) {
    var html = "";
    if (data.FechaEnvioProveedor == "" || data.FechaEnvioProveedor == null) {
        html = "";
    }
    else {
        if (data.Counter <= 20) {
            html = "<span class='label label-verde'>" + data.Counter + "</span>";
        }
        else if (data.Counter >= 21 && data.Counter <= 30) {
            html = "<span class='label label-amarillo'>" + data.Counter + "</span>";
        }
        else if (data.Counter > 30) {
            html = "<span class='label label-rojo'>" + data.Counter + "</span>";
        }
        
    }
    return html;
}

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    if ($("#hdnIDFranquicia").val() == "0") {
        if ($("#ddlFranquicias").val() != "")
            var franquicia = $("#ddlFranquicias").val();
    } else {
        var franquicia = $("#hdnIDFranquicia").val()
    }

    var info = "{idFranquicia: '" + franquicia
        + "', fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()

        + "'}";

    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetas.aspx/Exportar",
        contentType: "application/json; charset=utf-8",
        data: info,
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}