﻿var oTable = null;  

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = $("#hdnIDMarca").val();

    if (marca == "" || marca == null)
        marca = "0"

    var franquicia = $("#hdnIDFranquicia").val();

    if (franquicia == "" || franquicia == null)
        franquicia = "0";  


    var info = "{ nombre: '" + $("#txtNombre").val()
        + "', apellido: '" + $("#txtApellido").val()
        + "', documento: '" + $("#txtDocumento").val()
        + "', actividad: '" + $("#ddlActividades").val()
        + "', ciudad: '" + $("#localidades").val()
        + "', sede: '" + $("#ddlSedes").val()
        + "', horarios: '" + $("#ddlHorarios").val()
        + "', fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()

        + "'}";



    $.ajax({
        type: "POST",
        url: "ListadoBeneficiarios.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function exportarTr() {
    $("#divErrorTr").hide();
    $("#lnkDownloadTr").hide();
    $("#imgLoadingTr").show();
    var btnexportar = $("#btnExportarTr").attr("disabled", true);


    $.ajax({
        type: "POST",
        url: "Socios.aspx/ExportarTr",
        data: "{ idSocio: " + parseInt($("#hdnIDSocio").val()) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divErrorTr").hide();
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").show();
                $("#lnkDownloadTr").attr("href", data.d);
                $("#lnkDownloadTr").attr("download", data.d);
                $("#btnExportarTr").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTr").html(r.Message);
            $("#divErrorTr").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTr").hide();
            $("#lnkDownloadTr").hide();
            $("#btnExportarTr").attr("disabled", false);
        }
    });
}

function configControls() {

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    configFechasDesdeHasta("txtFechaDesdeGenerar", "txtFechaHastaGenerar");

    $("#txtDocumento").numeric();
    $("#txtTarjeta").numeric();
    $("#txtCuenta").numeric();
    var idmarca = parseInt($("#hdnIDMarca").val());
    if (idmarca > 0) {
        $(".marca").hide();
    }

    var myColumns = [
        { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
        { field: "Nombre", title: "Nombre", width: "150px" },
        { field: "Apellido", title: "Apellido", width: "150px" },
        { field: "Actividad", title: "Actividad", width: "200px" },
        { field: "Nivel", title: "Nivel", width: "150px" },
        { field: "Horario", title: "Horario", width: "150px" },
        { field: "SubCategoria", title: "SubCategoria", width: "200px" },
        { field: "Categoria", title: "Categoria", width: "200px" },
        { field: "Secretaria", title: "Secretaria", width: "200px" },
        { field: "NumDoc", title: "Documento", width: "80px" },
        { field: "Ciudad", title: "Localidad", width: "100px" },
        { field: "Sede", title: "Sede", width: "100px" },
        { field: "Email", title: "Email", width: "205px" },
        { field: "Celular", title: "Celular", width: "100px" },
        { field: "Telefono", title: "Telefono", width: "100px" },
        { field: "FechaNacimiento", title: "Fecha Nacimiento", format: "{0:dd/MM/yyyy}", width: "100px" },
        { field: "FechaAlta", title: "Fecha Alta", format: "{0:dd/MM/yyyy}", width: "130px" },
        { field: "FechaInscripcion", title: "Fecha Inscripcion", format: "{0:dd/MM/yyyy}", width: "130px" },
    ];
  
    if (idmarca > 0) {
        myColumns.splice(3, 1)
    }

    $("#txtApellido, #txtDocumento, #txtTarjeta, #txtCuenta, #txtEmail,#ddlMarcas,#ddlFranquicias").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Nombre: { type: "string" },
                        Apellido: { type: "string" },
                        Email: { type: "string" },
                        Celular: { type: "string" },
                        Telefono: { type: "string" },
                        FechaNacimiento: { type: "date" },
                        NumDoc: { type: "string" },
                        Actividad: { type: "string" },
                        Nivel: { type: "string" },
                        Horario: { type: "string" },
                        SubCategoria: { type: "string" },
                        Categoria: { type: "string" },
                        Secretaria: { type: "string" },
                        Ciudad: { type: "string" },
                        Sede: { type: "string" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        FechaAlta: { type: "date" },
                        FechaInscripcion: { type: "date" }
                        
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "ListadoBeneficiarios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },

        columns: myColumns

    });


      

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Sociose.aspx?IDSocio=" + dataItem.IDSocio;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Socios.aspx/Delete",
                data: "{ id: " + dataItem.IDSocio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        //$('#tableDetalleTr').fixedHeaderTable('destroy');
        $("#titDetalleTr").html("Detalle de Transacciones " + dataItem.Apellido);// + ", " + dataItem.Nombre);


        $.ajax({
            type: "POST",
            url: "Socios.aspx/GetBySocio",
            data: "{ id: " + dataItem.IDSocio + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#bodyDetalleTr").html("");

                if (data != null && data.d.length > 0) {

                    if (oTable != null) {

                        oTable.fnClearTable();
                        oTable.fnDestroy();
                    }

                    oTable = $('#tableDetalleTr').dataTable({
                        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                        "paging": true,
                        "bLengthChange": false,
                        "iDisplayLength": 10,
                        "ordering": false,
                        "bSort": false,
                        "info": false,
                        //"bDestroy": true,
                        "searching": false,
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                        "fnDrawCallback": function () {
                            var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                            if (pageCount == 1) {
                                $('.dataTables_paginate').first().hide();
                            } else {
                                $('.dataTables_paginate').first().show();
                            }
                        }
                    });

                 
                    $("#tableDetalleTr_info").parent().remove();
                    $("#tableDetalleTr").css("width", "100%");
                    $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                    $(".dataTables_paginate").first().parent().addClass("col-sm-12");
                }
                else
                    $("#bodyDetalleTr").html("<tr><td colspan='11'>No hay un detalle disponible</td></tr>");

                $("#hdnIDSocio").val(dataItem.IDSocio);
                $("#divErrorTr").hide();
                $('#modalDetalleTr').modal('show');
                $("#lnkDownloadTr").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });
}

function renderOptions(data) {
    var html = "";
    html = "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/>&nbsp;";
    html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";

    return html;
}

function renderDoc(data) {
    return data.TipoDocumento + " " + data.NroDocumento;
}

function Nuevo() {
    window.location.href = "Sociose.aspx";
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#hdnIDFranquicia").val("0");
    $("#hdnIDMarca").val("0");

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();


    var actividades = $("#ddlActividades").val();
    if (actividades != null) {
        var filterMulti = { logic: "or", filters: [] };
        $.each(actividades, function (i, v) {
            filterMulti.filters.push({ field: "Actividad", operator: "equals", value: v });
        });
        $filter.push(filterMulti);
    }

    var horarios = $("#ddlHorarios").val();
    if (horarios != null) {
        var filterMulti = { logic: "or", filters: [] };
        $.each(horarios, function (i, v) {
            filterMulti.filters.push({ field: "Horario", operator: "equals", value: v });
        });
        $filter.push(filterMulti);
    }



    var sedes = $("#ddlSedes").val();
    if (sedes != null) {
        var filterMulti = { logic: "or", filters: [] };
        $.each(sedes, function (i, v) {
            filterMulti.filters.push({ field: "Sede", operator: "equals", value: v });
        });
        $filter.push(filterMulti);
    }


    var localidades = $("#localidades").val();
    if (localidades != null) {
        var filterMulti = { logic: "or", filters: [] };
        $.each(localidades, function (i, v) {
            filterMulti.filters.push({ field: "Ciudad", operator: "equals", value: v.toUpperCase() });
        });
        $filter.push(filterMulti);
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "Nombre", operator: "contains", value: nombre.toUpperCase() });
    }

    var apellido = $("#txtApellido").val();
    if (apellido != "") {
        $filter.push({ field: "Apellido", operator: "contains", value: apellido.toUpperCase() });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NumDoc", operator: "contains", value: documento });
    }

    //var tarjeta = $("#txtTarjeta").val();
    //if (tarjeta != "") {
    //    $filter.push({ field: "Tarjeta", operator: "contains", value: tarjeta });
    //}

    //var marca = $("#ddlMarcas").val();
    //if (marca != null && marca != "") {
    //    $("#hdnIDMarca").val(marca);
    //    $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
    //}
    //var franquicia = $("#ddlFranquicias").val();
    //if (franquicia != null && franquicia != "") {
    //    $("#hdnIDFranquicia").val(franquicia);
    //    $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    //}


 
    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    var idFranquicia = $("#hdnIDFranquicia").val();
    if (idFranquicia > 0) {
        $(".franquicias").show();
    }

    $('#formSocio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });


    $("#ddlActividades").chosen({ max_selected_options: 10 });

    $("#ddlSedes").chosen({ max_selected_options: 10 });

    $("#localidades").chosen({ max_selected_options: 10 });

    $("#ddlHorarios").chosen({ max_selected_options: 10 });



    sedesChangeController();

    localidadesChangeController();

    actividadesChangeController();


});



function actividadesChangeController() {

    $("#ddlActividades").change(function () {

        var actividades = '';
        if ($("#ddlActividades").val() != null)
            actividades = $("#ddlActividades").val();


        var info = "{ actividades: '" + actividades
            + "', none: '" + "none"
            + "'}";


        $.ajax({
            type: "POST",
            url: "ListadoBeneficiarios.aspx/horariosPorActividades",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlHorarios').empty().trigger("liszt:updated"); //remove all child nodes


                if (Object.keys(data.d).length > 0) {
                    $.each(data.d, function (k, v) {
                        $("#ddlHorarios").append($("<option/>").val(v.Nombre).text(v.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);


                    $("#ddlHorarios").trigger("liszt:updated");

                }



                else {
                    $("#ddlHorarios").empty().trigger('liszt:updated');
                    console.log("nothing")
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}



function sedesChangeController(){

    $("#ddlSedes").change(function () {

        $('#ddlMarcas').html('');

        var sedes = '';
        if ($("#ddlSedes").val() != null)
            sedes = $("#ddlSedes").val();


        var info = "{ sedes: '" + sedes
            + "', Actividades: '" + "none"
            + "'}";


        $.ajax({
            type: "POST",
            url: "ListadoBeneficiarios.aspx/actividadesPorSedes",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlActividades').empty().trigger("liszt:updated"); //remove all child nodes

                    
                if (Object.keys(data.d).length > 0) {
                    $.each(data.d, function (k, v) {
                        $("#ddlActividades").append($("<option/>").val(v.Nombre).text(v.Nombre));
                    });

                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);


                    $("#ddlActividades").trigger("liszt:updated");

                }



                else {
                    $("#ddlActividades").empty().trigger('liszt:updated');
                    console.log("nothing")
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}


function localidadesChangeController(){

    $("#localidades").change(function () {
        
        var localidades = '';
        if ($("#localidades").val() != null)
            localidades = $("#localidades").val();


        var info = "{ localidades: '" + localidades
            + "', Actividades: '" + "none"
            + "'}";


        $.ajax({
            type: "POST",
            url: "ListadoBeneficiarios.aspx/sedesPorLocalidades",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $('#ddlSedes').empty().trigger("liszt:updated"); //remove all child nodes


                if (Object.keys(data.d).length > 0) {
                    $.each(data.d, function (k, v) {
                        $("#ddlSedes").append($("<option/>").val(v.Nombre).text(v.Nombre));
                    });


                    $("#divErrorTr").hide();
                    $("#imgLoadingTr").hide();
                    $("#lnkDownloadTr").show();
                    $("#lnkDownloadTr").attr("href", data.d);
                    $("#lnkDownloadTr").attr("download", data.d);
                    $("#btnExportarTr").attr("disabled", false);

                    $("#ddlSedes").trigger("liszt:updated");

                }
                else {
                    $("#ddlSedes").empty().trigger('liszt:updated');
                    console.log("nothing")
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTr").html(r.Message);
                $("#divErrorTr").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").hide();
                $("#btnExportarTr").attr("disabled", false);
            }
        });
    });


}