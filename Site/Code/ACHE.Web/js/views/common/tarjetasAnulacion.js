﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formSusTarjeta').validate();

    var rdbTipo = "A";
    if ($("#rdbSustitucion")[0].checked == true)
        rdbTipo = "S";
    if ($("#rdbDesvinculacion")[0].checked == true)
        rdbTipo = "D";

    var idSocio = 0;
    if ($("#hfIDSocio").val() != "")
        idSocio = parseInt($("#hfIDSocio").val());

    if ($('#formSusTarjeta').valid()) {
          //+ "', Marca: " + parseInt($("#ddlMarcas").val())
        var info = "{ Tipo: '" + rdbTipo
            + "', Numero: '" + $("#ddlTrTarjeta option:selected").html()          
            + "', Motivo: '" + $("#ddlMotivo option:selected").html()
            + "', NumeroNuevo: '" + $("#txtNumeroNuevo").val()
            + "', IDSocio: " + idSocio
            + "}";

        $.ajax({
            type: "POST",
            url: "TarjetasAnulacion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                volver();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function grabarMasiva() {

    $("#imgLoading").show();

    $("#divError").hide();
    $("#divOk").hide();

    var rdbTipo = "A";

    if ($("#rdbDesvinculacion")[0].checked == true)
        rdbTipo = "D";


    if ($('#txtMasiva').val().length) {

        var valoresRaw = $("#txtMasiva").val().split(",");

        var valores = "";

        valoresRaw.forEach(function (e) {
            var v = e.trim();
            valores += v + ",";
        })

        valores = valores.replace(/,\s*$/, "");

        var info = "{ Tipo: '" + rdbTipo
            + "', Numeros: '" + valores
            + "', Motivo: '" + $("#ddlMotivoMasiva option:selected").html()
            + "'}";

        $.ajax({
            type: "POST",
            url: "TarjetasAnulacion.aspx/grabarMasiva",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                volver();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }

}

function volver() {
    if ($("#hfIDSocio").val() != "")
        window.location.href = "/common/Sociose.aspx?IDSocio=" + $("#hfIDSocio").val();
    else if ($("#hfIDTarjeta").val() != "")
        window.location.href = "TarjetasEdicion.aspx?IDTarjeta=" + $("#hfIDTarjeta").val();
    else {
        window.location.href = "TarjetasAnulacion.aspx";
    }
}

$(document).ready(function () {

    configControls();

    $("#txtNumero").numeric();
    $("#txtNumeroNuevo").numeric();

    $("#txtValorSocio,#ddlTrBuscarSocio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarSocios();
            return false;
        }
    });


    $("#rdbAnulacion, #rdbDesvinculacion,#rdbSustitucion").click(function () {
        limpiarBusqueda();
    });


    $('#formSusTarjeta').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

function buscarSocios() {
    $("#divError").hide();

    console.log($('#metodo').val())

    if ($('#metodo').val() == 'masiva')
        busquedaMasiva();
    else
        busquedaIndividual();

}

function buscarTarjetas() {
    if ($("#ddlTrSocio").val() != "") {

        if ($("#ddlTrSocio").val() == "0") {
            $("#ddlTrTarjeta").html("<option value='" + $("#ddlTrSocio option:selected").text() + "'>" + $("#ddlTrSocio option:selected").text() + "</option>");
        }
        else {
            $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");

            var info = "{ socio: " + $("#ddlTrSocio").val() + "}";

            $.ajax({
                type: "POST",
                url: "TarjetasAnulacion.aspx/buscarTarjetas",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data != null) {
                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrTarjeta"));
                        }
                        if (data.d.length == 1) {
                            $("#ddlTrTarjeta").val(data.d[0].ID);
                            $("#ddlMarcas").val(data.d[0].ID);
                        }
                        if ($("#rdbSustitucion")[0].checked == true) {
                            $("#divSustitucion").show();
                            //seleccionarMarca(data.d[0].Nombre);
                        }

                        $("#imgLoading").hide();
                        $("#hfIDSocio").val($("#ddlTrSocio").val());
                    }

                },
                error: function (response) {
                    $("#imgLoading").hide();
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });

        }
    }
    else
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
}

function limpiarBusqueda() {
    $(".datos2").hide();
    $("#divSustitucion").hide();
    $("#divError").hide();
    $("#divOk").hide();
    $("#ddlTrTarjeta").html("");
    $("#ddlTrSocio").html("");
    $("#txtNumeroNuevo").val("");
    $("#hfIDSocio").val("");

}

function buscarMarca() {
    if ($("#ddlTrTarjeta").val() != "") {
        $("#ddlMarcas").val($("#ddlTrTarjeta").val());

    }
}

function configControls() {

    var inputIndividual = $("#inputIndividual");
    var inputMasivo = $("#inputMasivo");
    var buscarPor = $("#buscarPor");
    var buscarPorTarjeta = $("#buscarPorTarjeta");

    var datos2 = $(".datos2");

    $('#metodo').change(function () {

        console.log(this.value)

        if (this.value == 'masiva') {


            inputMasivo.show();
            inputIndividual.hide();

            buscarPor.hide();
            buscarPorTarjeta.show();

            datos2.hide();

            $("#rdbSustitucion").attr('disabled', true);
        }
        else if (this.value == 'individual') {
            inputMasivo.hide();
            inputIndividual.show();

            buscarPor.show();
            buscarPorTarjeta.hide();

            if ($("#ddlTrSocio").val() > 0)
                datos2.show();

            $("#rdbSustitucion").removeAttr('disabled');

        }
    });

}

function busquedaMasiva() {

    $("#imgLoading").show();
    $(".datosMasiva").hide();


    if ($("#txtMasiva").val() == "") {
        $("#divError").html("Por favor, ingrese los valores a buscar");
        $("#divError").show();
    }
    else {


        var valoresRaw = $("#txtMasiva").val().split(",");

        var valores = "";

        valoresRaw.forEach(function (e) {
            var v = e.trim();
            valores += v + ",";
        })

        // removemos la ultima coma
        valores = valores.replace(/,\s*$/, "");

        var info = "{ tipo: '" + 'T'
            + "', valores: '" + valores
            + "'}";

        $.ajax({
            type: "POST",
            url: "TarjetasAnulacion.aspx/buscarSociosMasivo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (data, text) {
                $("#imgLoading").hide();
                $(".datosMasiva").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();

            }
        });
    }

}

function busquedaIndividual() {

    if ($("#txtValorSocio").val() == "") {
        $("#divError").html("Por favor, ingrese el valor a buscar");
        $("#divError").show();

        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("<option value=''>Seleccione una tarjeta</option>");
    }
    else {
        $(".datos2").hide();
        $("#divSustitucion").hide();
        $("#imgLoading").show();
        $("#divError").hide();
        $("#ddlTrSocio").html("<option value=''>Seleccione un socio</option>");
        $("#ddlTrTarjeta").html("");

        var info = "{ tipo: '" + $("#ddlTrBuscarSocio").val()
            + "', valor: '" + $("#txtValorSocio").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "TarjetasAnulacion.aspx/buscarSocios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTrSocio"));
                    }

                    if (data.d.length == 0) {

                        if ($("#ddlTrBuscarSocio").val() == "Tarjeta")
                            $("#divError").html("No se encontraron tarjetas");
                        else
                            $("#divError").html("No se encontraron socios");

                        $("#divError").show();
                        $("#imgLoading").hide();
                    }
                    else if (data.d.length >= 1) {
                        $(".datos2").show();
                        $("#ddlTrSocio").val(data.d[0].ID);

                        if ($("#ddlTrSocio").val() == "0") {//Cuando trae resultados buscando por tarjeta
                            $("#ddlTrTarjeta").html("<option value='" + data.d[0].Nombre + "'>" + data.d[0].Nombre + "</option>");
                            $("#imgLoading").hide();
                        }
                        else {//Cuando trae resultados por DNI o por SOCIO
                            buscarTarjetas();
                        }
                    }
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

            }
        });
    }

}