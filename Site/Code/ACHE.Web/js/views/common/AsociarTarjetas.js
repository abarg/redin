﻿function buscar() {
    $("#divOk").hide();
    $("#divSocioEncontrado").hide();
    $("#divSocioNoEncontrado").hide();
    $("#divError").hide();
    $("#hdnIDSocio").val("0");
    $("#divAsociar").hide();

    if ($("#txtDoc").val() != "") {
        $("#imgLoading2").show();
        $("#btnBuscar").attr("disabled", true);

        var info = "{ documento: '" + $("#txtDocAsociado").val() + "'}";

        $.ajax({
            type: "POST",
            url: "Sociose.aspx/Buscar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#divError").hide();
                    $("#imgLoading2").hide();
                    $("#btnBuscar").attr("disabled", false);
                    $("#lblNombre").html(data.d.Nombre);
                    $("#lblApellido").html(data.d.Apellido);
                    $("#lblSexo").html(data.d.Sexo);
                    $("#lblEmail").html(data.d.Email);
                    $("#lblFechaNac").html(data.d.NroCuenta);
                    $("#hdnIDSocioAsociado").val(data.d.IDSocio);
                    $("#divSocioEncontrado").show();
                    if ($("#divSocioEncontrado").is(":visible") == true && $("#divSocioEncontradoResp").is(":visible") == true) {
                        $("#divAsociar").show();
                    }
                }
                else {
                    $("#imgLoading2").hide();
                    $("#btnBuscar").attr("disabled", false);
                    $("#divSocioNoEncontrado").show();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading2").hide();
                $("#btnBuscar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
    }
}

function buscarResponsable() {
    $("#divOk").hide();
    $("#divSocioEncontradoResp").hide();
    $("#divSocioNoEncontradoResp").hide();
    $("#divError").hide();
    $("#hdnIDSocio").val("0");
    $("#divAsociar").hide();
    if ($("#txtDocResponsable").val() != "") {
        $("#imgLoading1").show();
        $("#btnBuscarResponsable").attr("disabled", true);

        var info = "{ documento: '" + $("#txtDocResponsable").val() + "'}";

        $.ajax({
            type: "POST",
            url: "AsociarTarjetas.aspx/Buscar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#divError").hide();
                    $("#imgLoading1").hide();
                    $("#btnBuscarResponsable").attr("disabled", false);
                    $("#lblNombreResp").html(data.d.Nombre);
                    $("#lblApellidoResp").html(data.d.Apellido);
                    $("#lblSexoResp").html(data.d.Sexo);
                    $("#lblEmailResp").html(data.d.Email);
                    $("#lblFechaNacResp").html(data.d.NroCuenta);
                    $("#hdnIDSocioResponsable").val(data.d.IDSocio);
                    $("#divSocioEncontradoResp").show();
                    $(".asociado").show();
                    $("#txtDocAsociado").val("");
                    $("#divSocioEncontrado").hide();
                    $("#divSocioNoEncontrado").hide();

                    /*
                    if ($("#divSocioEncontrado").is(":visible") == true && $("#divSocioEncontradoResp").is(":visible") == true) {
                        $("#divAsociar").show();
                    }
                    */
                }
                else {
                    $("#txtDocAsociado").val("");
                    $("#divSocioEncontrado").hide();
                    $("#divSocioNoEncontrado").hide();

                    $("#imgLoading1").hide();
                    $("#btnBuscarResponsable").attr("disabled", false);
                    $("#divSocioNoEncontradoResp").show();
                    $(".asociado").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading2").hide();
                $("#btnBuscarResponsable").attr("disabled", false);
            }
        });
    }
    else {
        return false;
    }
}


$(document).ready(function () {
    $("#txtDocResponsable, #txtDocAsociado").numeric();
    $("#txtDocResponsable").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarResponsable();
            return false;
        }
    });
    $("#txtDocAsociado").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });
});

function asociar() {
    if (!($("#txtDocAsociado").val() == $("#txtDocResponsable").val())) {
        var info = "{ idSocioEncontrado: " + parseInt($("#hdnIDSocioAsociado").val())
            + ", idSocio: " + parseInt($("#hdnIDSocioResponsable").val()) + "}";
        $.ajax({
            type: "POST",
            url: "Sociose.aspx/Asociar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#divError").hide();
                $("#divOk").show();
                $("#divAsociar").hide();
                $("#divSocioEncontradoResp").hide();
                $("#divSocioEncontrado").hide();
                $("#txtDocAsociado").val("");
                $("#txtDocResponsable").val("");
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $(".asociado").hide();


            },
            error: function (response) {
                $("#divError").show();
                $("#divOk").hide();
            }
        });
    } else {
        $("#divError").html("debe ingresar socios diferentes");
        $("#divError").show();
        $("#divOk").hide();
    }
}