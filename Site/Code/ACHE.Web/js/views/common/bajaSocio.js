﻿$(document).ready(function () {
    //    configControls();
    $("#txtDocumento,#txtNroSocio2").numeric();
    $("#lblNro2").val("");
    $("#tablaSocios").hide();
    $("#txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });
    $("#txtNroSocio2").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar2();
            return false;
        }
    });

});

function buscar() {
    $("#divOk").hide();
    $("#divError").hide();

    var documento = $("#txtDocumento").val();
    if (documento == "") {
        error("El campo de Nro. de Documento");
    }
    else {
        var info = "{ DNI: '" + $("#txtDocumento").val() + "'}";
        $("#imgLoadingBuscar").show();
        $.ajax({
            type: "POST",
            url: "BajaSocio.aspx/buscarDatosSocio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    $("#hdnID").val(data.d.IDSocio);
                    $("#txtNombre").html(data.d.Nombre);
                    $("#txtApellido").html(data.d.Apellido);
                    //$("#txtPuntos").html(data.d.Puntos);
                    $("#datosSocio").show();
                    $("#lblNro").hide();
                    $(".step2").show();
                }
                else {
                    error("No existe ningun socio asociado a ese Nro Documento")
                }
                $("#imgLoadingBuscar").hide();
            }
        });

    }
}


function error(mensaje) {
    $("#divOk").hide();
    $("#divError").html(mensaje);
    $("#divError").show();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}


function Baja() {

    var idSocio = "";

    if ($("#hdnID").val() != "0" && $("#hdnID").val() != null)
        idSocio = parseInt($("#hdnID").val());

    var info = "{ idSocio: " + idSocio + "}";

    $.ajax({
        type: "POST",
        url: "BajaSocio.aspx/baja",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            return data.d;
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTrSocio").html(r.Message);
            $("#divErrorTrSocio").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

