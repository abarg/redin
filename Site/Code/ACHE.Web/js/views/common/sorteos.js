﻿

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtTitulo, #txtFechaDesde, #txtFechaHasta, #cmbEstado,#cmbMarcas").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDSorteo: { type: "int" },
                        Marca: { type: "string" },
                        Titulo: { type: "string" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        Estado: { type: "string" },
                        ImporteDesde: { type: "int" },
                        ImporteHasta: { type: "int" },
                        Pais: { type: "string" },
                        Provincia: { type: "string" },
                        Ciudad: { type: "string" },
                        Domicilio: { type: "string" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Sorteos.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Opciones", width: "50px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "Titulo", title: "Titulo", width: "100px" },
            { field: "FechaDesde", title: "Fecha Desde", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "FechaHasta", title: "Fecha Hasta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "ImporteDesde", title: "Importe Desde", width: "100px" },
            { field: "ImporteHasta", title: "Importe Hasta", width: "100px" },
            { field: "Pais", title: "Pais", width: "100px" },
            { field: "Provincia", title: "Provincia", width: "100px" },
            { field: "Ciudad", title: "Ciudad", width: "100px" },
            { field: "Domicilio", title: "Domicilio", width: "100px" },
            { field: "Estado", title: "Estado", width: "80px" },
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Sorteose.aspx?IDSorteo=" + dataItem.IDSorteo;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Sorteos.aspx/Eliminar",
                data: "{ id: " + dataItem.IDSorteo + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function Nuevo() {
    window.location.href = "sorteose.aspx";
}


function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var Titulo = $("#txtTitulo").val();
    if (Titulo != "") {
        $filter.push({ field: "Titulo", operator: "contains", value: Titulo });
    }
    var Estado = $("#cmbEstado").val();
    if (Estado != "") {
        $filter.push({ field: "Estado", operator: "equal", value: Estado });
    }
    var Marca = $("#cmbMarcas").val();
    if (Marca != "") {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(Marca) });
    }
    grid.dataSource.filter($filter);
}

function renderOptions(data) {
    var html = "<div align='center'>";
    html += "<img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/>";
    html += "</div>";

    return html;
}

$(document).ready(function () {
    configControls();
});
