﻿function configControls() {

    $("#txtNroReferencia, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    var myColumns = [
           { title: "Eliminar", template: "#= renderOptions(data) #", width: "80px" },
            { field: "Fecha", title: "Fecha", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "NumeroPlanilla", title: "Nro Ref", width: "80px" },
            { field: "Franquicia", title: "Franquicia", width: "80px" },
            { field: "Provincia", title: "Provincia", width: "80px" },
            { field: "Localidad", title: "Localidad", width: "80px" },
            { field: "Zona", title: "Zona", width: "80px" },
            { field: "CantidadComerciosSeleccionados", title: "Cant. comercios", width: "80px" },
            { field: "Estado", title: "Estado", width: "80px" }
    ];
    if ($("#hdnIDFranquicia").val() != "0") {
        myColumns.splice(2,1)
    }
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Estado: { type: "string" },
                        NumeroPlanilla: { type: "string" },
                        Provincia: { type: "string" },
                        Localidad: { type: "string" },
                        Zona: { type: "string" },
                        CantidadComerciosSeleccionados: { type: "int" },
                        Franquicia: { type: "string" },
                        Fecha: { type: "string" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Reporte-Verificaciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns:myColumns
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "reporte-verificaciones.aspx/Delete",
                data: "{ nroReferencia: " + dataItem.NumeroPlanilla + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        window.location.href = "verificacionesPOSe.aspx?NroRef=" + dataItem.NumeroPlanilla
    });


    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function renderOptions(data) {
    var html = "";
    if (data.Estado == "Pendiente") {
        html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
        html += "<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    } else if (data.Estado == "EnProceso") {
        html = "<img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>";
    }
   
    return html;
}
function filter() {

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

   
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();


    if ($("#hdnIDFranquicia").val() == "0") {
        var franquicia = $("#ddlFranquicias").val();
         if (franquicia != "0") 
              $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }
    var nroReferencia = $("#txtNroReferencia").val();
    if (nroReferencia != "") {
        $filter.push({ field: "NumeroPlanilla", operator: "contains", value: nroReferencia });
    }


    grid.dataSource.filter($filter);
    
}


$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configControls();
});



