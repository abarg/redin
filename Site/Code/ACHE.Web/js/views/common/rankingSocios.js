﻿$(document).ready(function () {
    configControls();
});


function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();


    var socio = $("#txtApellido").val().toUpperCase();
    if (socio != "") {
        $filter.push({ field: "Socio", operator: "contains", value: socio });
    }

    var idMarca = 0
    if ($("#hdnIDMarca").val() != null && parseInt($("#hdnIDMarca").val()) > 0)
        idMarca = $("#hdnIDMarca").val();
    else if ($("#cmbMarcas").val() != null && $("#cmbMarcas").val() != "")
        idMarca = $("#cmbMarcas").val();
    if (idMarca != "") {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(idMarca) });
    }
    grid.dataSource.filter($filter);
}

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    $("#txtApellido, #txtFechaDesde, #txtFechaHasta,#cmbMarcas").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Socio: { type: "string" },
                        //Marca: { type: "string" },
                        IDFranquicia: { type: "int" },
                        IDMarca: { type: "int" },
                        //FechaDesde: { type: "date" },
                        //FechaHasta: { type: "date" },
                        Tarjeta: { type: "string" },
                        CantTr: { type: "int" },
                        Importe: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "RankingSocios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Opciones", width: "50px" },
            { field: "Socio", title: "Socio", width: "120px" },
            //{ field: "Marca", title: "Marca", width: "100px" },
            { field: "Tarjeta", title: "Tarjeta", width: "80px" },
            //{ field: "FechaDesde", title: "Fecha Desde", format: "{0:dd/MM/yyyy}", width: "100px" },
            //{ field: "FechaHasta", title: "Fecha Hasta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "CantTr", title: "Cantidad Tr", width: "80px" },
            { field: "Importe", title: "Importe", width: "80px" }
        ]
    });

}

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();

    $("#btnExportar").attr("disabled", true);

    var idMarca = 0
    if ($("#hdnIDMarca").val() != null && parseInt($("#hdnIDMarca").val()) > 0)
        idMarca = parseInt($("#hdnIDMarca").val());

    else if ($("#cmbMarcas").val() != null && $("#cmbMarcas").val() != "")
        idMarca = parseInt($("#cmbMarcas").val());

    var idFranq = 0
    if ($("#hdnIDFranquicia").val() != null && parseInt($("#hdnIDFranquicia").val()) > 0)
        idFranq = parseInt($("#hdnIDFranquicia").val());

    var socio = ""
    if ($("#txtApellido").val() != null && $("#txtApellido").val() != "")
        socio = $("#txtApellido").val();

    var info = "{socio: '" + socio
        + "',idFranq: " + idFranq
       + ", idMarca: " + idMarca
       + ", fechaDesde: '" + $("#txtFechaDesde").val()
       + "', fechaHasta: '" + $("#txtFechaHasta").val()
       + "'}";

    $.ajax({
        type: "POST",
        url: "RankingSocios.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();

                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });

}