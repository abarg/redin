﻿$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde");
    configControls();
});


function configControls() {  

    $("#txtFechaDesde, #txtEdadDesde, #txtEdadHasta, #ddlSexo, #ddlFranquicias, #ddlMarcas,#txtCantidadTransacciones").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    var myColumns = [
    { field: "Nombre", title: "Nombre", width: "80px" },
    { field: "Apellido", title: "Apellido", width: "80px" },
    { field: "Sexo", title: "Sexo", width: "40px" },
    { field: "Edad", title: "Edad", width: "40px" },
    { field: "Franquicia", title: "Franquicia", width: "100px" },
    { field: "Marca", title: "Marca", width: "100px" },
    { field: "CantTrans", title: "Cantidad", width: "60px" },
    ];

    var idmarca = parseInt($("#hdnIDMarcas").val());
    if (idmarca > 0) {
        myColumns.splice(5, 1)
        myColumns.splice(4, 1)
    }

    var idFranquicia = parseInt($("#hdnIDFranquicias").val());
    if (idFranquicia > 0) {
        myColumns.splice(4, 1)
    }

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Nombre: { type: "string" },
                        Apellido: { type: "string" },
                        CantTrans: { type: "integer" },
                        IDFranquicia: { type: "integer" },
                        Franquicia: { type: "string" },
                        IDMarca: { type: "integer" },
                        Marca: { type: "string" },
                        Sexo: { type: "string" },
                        Edad: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "UsuariosDormidos.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        var idMarca = 0;
                        if ($("#ddlMarcas").val() != "" && $("#ddlMarcas").val() != 0 && $("#ddlMarcas").val() != null && !isNaN($("#ddlMarcas").val()))
                            idMarca = parseInt($("#ddlMarcas").val());

                        var idFranquicia = 0;
                        if ($("#ddlFranquicias").val() != 0 && $("#ddlFranquicias").val() != null && !isNaN($("#ddlFranquicias").val()))
                            idFranquicia = parseInt($("#ddlFranquicias").val());

                        var fechaDesde = $("#txtFechaDesde").val();

                        var edadDesde = 0
                        if ($("#txtEdadDesde").val() != null && $("#txtEdadDesde").val() != "" && !isNaN($("#txtEdadDesde").val()))
                            edadDesde = parseInt($("#txtEdadDesde").val());

                        var edadHasta = 0
                        if ($("#txtEdadDesde").val() != null && $("#txtEdadHasta").val() != "" && !isNaN($("#txtEdadHasta").val()))
                            edadHasta = parseInt($("#txtEdadHasta").val());

                        data = $.extend({ sort: null, filter: null, IDFranquicia: idFranquicia, IDMarca: idMarca, FechaDesde: fechaDesde, Sexo: $("#ddlSexo").val(), EdadDesde: edadDesde, EdadHasta: edadHasta, CantTrans: parseInt($("#txtCantidadTransacciones").val()) }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: myColumns
    });



    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}


$("#ddlFranquicias").change(function () {

    $('#ddlMarcas').html('');

    var idFranquicia = 0;
    if ($("#ddlFranquicias").val() != "")
        idFranquicia = parseInt($("#ddlFranquicias").val());

    $.ajax({
        type: "POST",
        url: "UsuariosDormidos.aspx/marcasByFranquicias",
        data: "{ idFranquicia: " + idFranquicia
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlMarcas").append($("<option/>").val(this.ID).text(this.Nombre));
                });

                $("#divErrorTr").hide();
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").show();
                $("#lnkDownloadTr").attr("href", data.d);
                $("#lnkDownloadTr").attr("download", data.d);
                $("#btnExportarTr").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTr").html(r.Message);
            $("#divErrorTr").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTr").hide();
            $("#lnkDownloadTr").hide();
            $("#btnExportarTr").attr("disabled", false);
        }
    });
});

function filter() {

    reiniciarHdns();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var marca = $("#ddlMarcas").val();
    if (marca != "" && marca != null && marca != "0") {
        $("#hdnIDMarcas").val(marca);
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
    }

    var franquicia = $("#ddlFranquicias").val();
    if (franquicia != "" && franquicia != null) {
        $("#hdnIDFranquicias").val(franquicia);
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }

    var sexo = $("#ddlSexo").val();
    if (sexo != "") {
        $("#hdnSexo").val(sexo);
        $filter.push({ field: "Sexo", operator: "equal", value: sexo });
    }

    var edadDesde = $("#txtEdadDesde").val();
    if (edadDesde != "") {
        $("#hdnEdadDsd").val(edadDesde);
        $filter.push({ field: "Edad", operator: "gte", value: parseInt(edadDesde) });
    }

    var edadHasta = $("#txtEdadHasta").val();
    if (edadHasta != "") {
        $("#hdnEdadHst").val(edadHasta);
        $filter.push({ field: "Edad", operator: "lte", value: parseInt(edadHasta) });
    }
    var cantTrans = $("#txtCantidadTransacciones").val();
    if (cantTrans != "") {
        $("#hdnCantTrans").val(cantTrans);
        $filter.push({ field: "CantTrans", operator: "lte", value: parseInt(cantTrans) });
    }


    grid.dataSource.filter($filter);

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

}

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = 0;
    if ($("#hdnIDMarcas").val() != "0") {
        marca = $("#hdnIDMarcas").val();
    }

    var franquicia = 0;
    if ($("#hdnIDFranquicias").val() != "0") {
        franquicia = $("#hdnIDFranquicias").val();
    }

    var sexo = "";
    if ($("#hdnSexo").val() != "") {
        sexo = $("#hdnSexo").val();
    }

    var edadDesde = 0;
    if ($("#hdnEdadDsd").val() != "0") {
        edadDesde = $("#hdnEdadDsd").val();
    }

    var edadHasta = 0;
    if ($("#hdnEdadHst").val() != "0") {
        edadHasta = $("#hdnEdadHst").val();
    }

    var cantTrans = 0;
    if ($("#hdnCantTrans").val() != "0") {
        cantTrans = $("#hdnCantTrans").val()
    }

    var info = "{ IDFranquicia: '" + parseInt(franquicia)
            + "', IDMarca: '" + parseInt(marca)
            + "', FechaDesde: '" + $("#txtFechaDesde").val()
            + "', Sexo: '" + sexo
            + "', EdadDesde: " + parseInt(edadDesde)
            + ", EdadHasta: " + parseInt(edadHasta)
            + ", CantTrans: " + parseInt(cantTrans)
            + "}";

    $.ajax({
        type: "POST",
        url: "UsuariosDormidos.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function reiniciarHdns() {
    $("#hdnIDFranquicias").val("0");
    $("#hdnIDMarcas").val("0");
    $("#hdnSexo").val("");
    $("#hdnEdadDsd").val("0");
    $("#hdnEdadDsd").val("0");
    $("#hdnCantTrans").val("0");
}