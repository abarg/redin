﻿function asociarTarjeta() {
    $("#divErrorTarjetas").hide();
    $("#divError").hide();
    $("#btnAsociarTarjeta").attr("disabled", true);

    if ($("#ddlTarjetas").val() == "") {
        $("#divErrorTarjetas").html("Seleccione una tarjeta");
        $("#divErrorTarjetas").show();
        $("#btnAsociarTarjeta").attr("disabled", false);
    }
    else {
        var info = "{ IDSocio: " + parseInt($("#hdnIDSocio").val())
            + ", Tarjeta: '" + $("#ddlTarjetas").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "Sociose.aspx/asociarTarjeta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtNuevaTarjeta").val("");
                window.location.href = "Sociose.aspx?IDSocio=" + $("#hdnIDSocio").val();
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTarjetas").html(r.Message);
                $("#divErrorTarjetas").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#btnAsociarTarjeta").attr("disabled", false);
            }
        });
    }
}

function buscarTarjetas() {
    $("#divErrorTarjetas").hide();
    $("#divError").hide();
    $("#divResultado1").hide();
    $("#divResultado2").hide();

    if ($("#ddlMarcas").val() == "") {
        $("#divErrorTarjetas").html("Selecciona una marca");
        $("#divErrorTarjetas").show();
    }
    else if ($("#txtNuevaTarjeta").val() == "" || $("#txtNuevaTarjeta").val().length < 5) {
        $("#divErrorTarjetas").html("Ingrese una terminación de una tarjeta");
        $("#divErrorTarjetas").show();
    }
    else {
        $("#ddlTarjetas").html("<option value=''>Seleccione una tarjeta</option>");

        var info = "{ idMarca: " + parseInt($("#ddlMarcas").val()) + ", tarjeta: '" + $("#txtNuevaTarjeta").val() + "'}";
        $.ajax({
            type: "POST",
            url: "Sociose.aspx/buscarTarjetas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data != null) {
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlTarjetas"));
                    }
                    if (data.d.length > 0) {
                        $("#divResultado1").show();
                        $("#divResultado2").show();
                    }
                    else {
                        $("#divErrorTarjetas").html("No se encontraron resultados");
                        $("#divErrorTarjetas").show();
                    }
                    //$('#ddlEmpleado').trigger('liszt:updated');
                }
            },
            error: function (response) {

                var r = jQuery.parseJSON(response.responseText);
                $("#divErrorTarjetas").html(r.Message);
                $("#divErrorTarjetas").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function buscar() {
    $("#divSocioEncontrado").hide();
    $("#divSocioNoEncontrado").hide();
    $("#divError").hide();
    $("#hdnIDSocio").val("0");
    $("#txtNuevaTarjeta").val("");

    if ($('#formAlta').valid()) {
        $("#imgLoading").show();
        $("#btnBuscar").attr("disabled", true);

        var info = "{ documento: '" + $("#txtNroDoc").val() + "'}";

        $.ajax({
            type: "POST",
            url: "Socios-alta.aspx/Buscar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#btnBuscar").attr("disabled", false);

                    console.log(data.d.Nombre);

                    $("#lblNombresito").html(data.d.Nombre);
                    $("#lblApellido").html(data.d.Apellido);
                    $("#lblSexo").html(data.d.Sexo);
                    $("#lblEmail").html(data.d.Email);
                    $("#lblFechaNac").html(data.d.NroCuenta);
                    $("#hdnCelular").val(data.d.Celular);
                    $("#hdnIDSocio").val(data.d.IDSocio);

                    $("#divSocioEncontrado").show();
                }
                else {
                    $("#imgLoading").hide();
                    $("#btnBuscar").attr("disabled", false);
                    $("#divSocioNoEncontrado").show();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#btnBuscar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
    }
}


$(document).ready(function () {

    $("#txtNroDoc,#txtNuevaTarjeta").numeric();
    $("#txtNroDoc").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });
    $("#txtNuevaTarjeta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscarTarjetas();
            return false;
        }
    });
    if ($("#hdnIDMarca").val() != "0") {
        $("#ddlMarcas").hide();
    }

    $('#formAlta').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore"
    });
});