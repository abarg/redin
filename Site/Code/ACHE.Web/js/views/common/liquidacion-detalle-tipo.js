﻿var oTable = null;

$(document).ready(function () {
    $('#formAddItem').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    if ($("#hdfConcepto").val() == "TRANSACCIONES") 
        GenerarTablaTR();
    else
        GenerarTabla();
    
    $("#txtImporteTotal").numeric();
    $("#txtImporteOriginal").numeric();
});
function irAlDetalle() {
    window.location.href = "LiquidacionesDetalle.aspx?IDLiquidacion=" + $("#hdfIDLiquidacion").val();

}
function agregarItem() {
    $('#formAddItem').validate();
    if ($('#formAddItem').valid()) {

        var info = "{ importeOriginal: '" + $("#txtImporteOriginal").val()
          + "', idDetalle: '" + $("#hfIDLiquidacionDetalle").val()
          + "', subConcepto: '" + $("#txtSubConcepto").val()
          +"'}";
        $.ajax({
            type: "POST",
            url: "LiquidacionDetallePorTipo.aspx/AgregarItem",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#divError2").hide();
                $("#divError").hide();
                $("#divOk").hide();
                $("#txtImporteOriginal").val("");
                $("#txtSubConcepto").val("");
                GenerarTabla();
                $("#divOK2").show();

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").hide();
                $("#divOk").hide();
                $("#divOK2").hide();
                $("#divError2").html(r.Message);
                $("#divError2").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}


function verModalTR(id) {
    //$('#tableDetalleTr').fixedHeaderTable('destroy');
    $("#titDetalleTr").html("Detalle de Transacciones");// + ", " + dataItem.Nombre);
    
    $.ajax({
        type: "POST",
        url: "LiquidacionDetallePorTipo.aspx/GetTransacciones",
        data: "{ idLiquidacion: " + id + " }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#bodyDetalleTr").html("");

            if (data != null && data.d.length > 0) {

                if (oTable != null) {

                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalleTr').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].Fecha,
                        data.d[i].Hora,
                        data.d[i].Operacion,
                        data.d[i].SDS,
                        data.d[i].ImporteOriginal,
                        data.d[i].ImporteAhorro,
                        data.d[i].Puntos]
                      );
                }

                $("#tableDetalleTr_info").parent().remove();
                $("#tableDetalleTr").css("width", "100%");
                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else
                $("#bodyDetalleTr").html("<tr><td colspan='9'>No hay un detalle disponible</td></tr>");

            $("#hdnIDLiq").val(id);
            $('#modalDetalleTr').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ":" + thrownError);
        }
    });
}

function exportarTR() {
    $("#divErrorTR").hide();
    $("#divOkTR").hide();
    $("#lnkDownloadTR").hide();
    $("#imgLoadingTR").show();
    $("#btnExportarTR").attr("disabled", true);


    var info = "{ idLiquidacion: " + $("#hdnIDLiq").val()
              + "}";

    $.ajax({
        type: "POST",
        url: "LiquidacionDetallePorTipo.aspx/ExportarTR",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divErrorTR").hide();
                $("#imgLoadingTR").hide();
                $("#lnkDownloadTR").show();
                $("#lnkDownloadTR").attr("href", data.d);
                $("#lnkDownloadTR").attr("download", data.d);
                $("#btnExportarTR").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTR").html(r.Message);
            $("#divErrorTR").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTR").hide();
            $("#lnkDownloadTR").hide();
            $("#btnExportarTR").attr("disabled", false);
        }
    });
}

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);
    $("#divError2").hide();
    $("#divOK2").hide();
    var info = "{ idLiquidacion: '" + $("#hfIDLiquidacionDetalle").val()
              + "'}";

    $.ajax({
        type: "POST",
        url: "LiquidacionDetallePorTipo.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function GenerarTablaTR() {


    var info = "{ idLiquidacion: '" + $("#hfIDLiquidacionDetalle").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "LiquidacionDetallePorTipo.aspx/generarTabla",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyLiquidacionDetalle").html(data.d);
            }
        }
    });
}




function confirmarLiquidacion() {

    $("#divError").hide();
    $("#divOk").hide();
    $("#divError2").hide();
    $("#divOK2").hide();


    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var valid = true;
    if ($("#flpFactura").val() == "") {
        $("#divError").html("debe adjuntar una factura");
        $("#divError").show();
        valid = false;
    }
    if (valid) {
        var info = "{ idLiquidacion: '" + $("#hfIDLiquidacionDetalle").val()
                   + "', archivo: '" + $("#flpFactura").val()
                 + "'}";
        $.ajax({
            type: "POST",
            url: "LiquidacionDetallePorTipo.aspx/ConfirmarLiquidacion",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                window.location.href = "Liquidaciones.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
            }
        });
    }
}



function GenerarTabla() {

    $("#divError").hide();
    $("#divOk").hide();
    $("#divError2").hide();
    $("#divOK2").hide();

    var info = "{ idLiquidacion: '" + $("#hfIDLiquidacionDetalle").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "LiquidacionDetallePorTipo.aspx/generarTablaConceptos",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyLiquidacionDetalle").html(data.d);
                if ($("#trSinItems").is(":visible")) {
                    $("#pnlPaso3").hide();
                }
            }
        }
    });
}

function grabar() {

    $("#divError").hide();
    $("#divOk").hide();
    $("#divError2").hide();
    $("#divOK2").hide();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    var msg;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
        
        info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
    });

    info += "', concepto: '" + $("#hdfConcepto").val()
        +"'}";
   
        $.ajax({
            type: "POST",
            url: "LiquidacionDetallePorTipo.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divError").hide();
                $("#divOk").show();
                window.location.href = "LiquidacionesDetalle.aspx?IDLiquidacion=" + $("#hdfIDLiquidacion").val();

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
            }
        });
  
}

