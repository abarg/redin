﻿/**LOGO**/


function deleteLogo(file, div) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", archivo: '" + file + "', div: '" + div + "'}";

    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetase.aspx/eliminarImagen",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#" + div).hide();
        },
        error: function (response) {
            //alert(response);
        }
    });

    return false;
}

function IsValidEmail(email) {
    email = email.toLowerCase();
    var regExp = /^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.)[\w]{2,4}|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$/
    if (regExp.test(email)) {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function () {
    configDatePicker();
    GenerarTablaCostos();
    GenerarTablaEspecificaciones();
    
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
    
});

function GenerarTablaCostos() {

    var info = "{ idFormulario: '" + $("#hdnID").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetase.aspx/generarTablaCostos",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyCostos").html(data.d);
                calcularTotal();
            }
        }
    });
}

function grabar() {
    /*
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {

        info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
    });

    info += "', concepto: '" + $("#hdfConcepto").val()
        + "'}";
        */
    var validEmail=true;
    if (!IsValidEmail($('#txtEmailContacto').val()) && ($('#txtEmailContacto').val()) !="") {
        $("#divErrorFormatoMail1").show();
        $("#divErrorFormatoMail2").hide();
        validEmail = false;

    }
    if (!IsValidEmail($('#txtEmail').val()) && ($('#txtEmail').val()) != "") {
        $("#divErrorFormatoMail2").show();
        $("#divErrorFormatoMail1").hide();
        validEmail = false;

    }    

    $('#formEdicion').validate();

    if ($('#formEdicion').valid()&& validEmail) {

        var info = "{ idformulario: " + $("#hdnID").val()
          + ", fecha: '" + $("#txtFecha").val()
          + "', idFranquicia: " + $("#ddlFranquicias").val()
          + ", unidadNegocio: '" + $("#ddlUnidadNegocio").val()
          + "', estado: '" + $("#ddlEstado").val()
          + "', costos: '";
        $(".selectCostos").each(function () {
            info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
        });
        info += "', cliente: '" + $("#txtCliente").val()
        + "', descripcion: '" + $("#txtDescripcion").val()
        + "', obsAnverso: '" + $("#txtObservacionesAnverso").val()
        + "', obsReverso: '" + $("#txtObservacionesReverso").val()
        + "', especificaciones: '";
        $(".selectEspecificaciones").each(function () {

            info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
        });
        info += "', imprimirSimulacion: '" + $("#chkImprimirSegunSimulacion").is(':checked')
      + "', corregirConSimulacion: '" + $("#chkCorregirConSimulacion").is(':checked')
      + "', corregirSinSimulacion: '" + $("#chkCorregirSinSimulacion").is(':checked')
      + "', obs: '" + $("#txtObservaciones").val()
      + "', razonSocial: '" + $("#txtRazonSocial").val()
      + "', paisFact: '" + $("#ddlPaisFact").val()
      + "', provinciaFact: '" + $("#ddlProvinciaFact").val()
      + "', ciudadFact: '" + $("#ddlCiudadFact").val()
      + "', domFiscal: '" + $("#txtDomFiscal").val()
      + "', cuit: '" + $("#txtCuit").val()
      + "', condIVA: '" + $("#ddlIVA").val()
      + "', contacto: '" + $("#txtContacto").val()
      + "', telContacto: '" + $("#txtTelContacto").val()
      + "', emailContacto: '" + $("#txtEmailContacto").val()
      + "', obsDatosFact: '" + $("#txtObservacionesDatosFacturacion").val()
      + "', formaPago: '" + $("#ddlFormaDePago").val()
      + "', fechaEnvioProv: '" + $("#txtFechaEnvioProv").val()
      + "', nombre: '" + $("#txtNombre").val()
      + "', dni: '" + $("#txtDNI").val()
      + "', domicilio: '" + $("#txtDomicilio").val()
      + "', localidad: '" + $("#txtLocalidad").val()
      + "', cp: '" + $("#txtCP").val()
      + "', provincia: '" + $("#ddlProvincia").val()
      + "', fechaEnvio: '" + $("#txtFechaEnvio").val()
      + "', empresa: '" + $("#txtEmpresa").val()
      + "', nroGuia: '" + $("#txtNroGuia").val()
      + "', formaEnvio: '" + $("#ddlFormaEnvio").val()
      + "', telefono: '" + $("#txtTelefono").val()
      + "', paginaWeb: '" + $("#txtPaginaWeb").val()
      + "', email: '" + $("#txtEmail").val()
      + "', facebook: '" + $("#txtFacebook").val()
      + "', basesycondiciones: '" + $("#txtBasesYCondiciones").val()
      + "', logoDatosReverso: '" + $("#chkLogo").is(':checked')
      + "'}";

        $.ajax({
            type: "POST",
            url: "FormularioAprobacionTarjetase.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                window.location.href = "/common/FormularioAprobacionTarjetas.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function GenerarTablaEspecificaciones() {

    var info = "{ idFormulario: '" + $("#hdnID").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetase.aspx/generarTablaEspecificaciones",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyEspecificaciones").html(data.d);
            }
        }
    });
}

function UploadError(sender) {
    $("#divError").html("Error al actualizar los datos");
    $("#divError").show();
    $("#divOK").hide();
}

function provinciasByPaises() {

    var idPais = 1;
    if ($("#ddlPaisFact").val() != "")
        idPais = parseInt($("#ddlPaisFact").val());

    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetase.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlProvinciaFact").append($("<option/>").val(this.ID).text(this.Nombre));
                });

                $("#ddlProvinciaFact").val("1");
                if (parseInt($("#ddlProvinciaFact").val()) > 0) {
                    idProv = parseInt($("#ddlProvinciaFact").val());
                }
                LoadCiudades(idProv, 'ddlCiudadFact');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function LoadCiudades(provincia, control) {

    $('#' + control).html("<option value=''></option>").trigger('liszt:updated');
    $.ajax({
        type: "POST",
        url: "/modulos/common.aspx/LoadCiudades",
        data: "{idProvincia: " + provincia + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            $(data.d).each(function () {
                $("#" + control).append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $('#' + control).trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}

function provinciasByPaises2() {

    var idPais = 1;
    if ($("#ddlPaisFact").val() != "")
        idPais = parseInt($("#ddlPaisFact").val());

    $.ajax({
        type: "POST",
        url: "FormularioAprobacionTarjetase.aspx/provinciasByPaises",
        data: "{ idPais: " + idPais
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlPaisFact").append($("<option/>").val(this.ID).text(this.Nombre));
                });

                $("#ddlPaisFact").val("1");
                if (parseInt($("#ddlPaisFact").val()) > 0) {
                    idPais = parseInt($("#ddlPaisFact").val());
                }
                LoadProvincias(idPais, 'ddlProvinciaFact');
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function LoadProvincias(pais, control) {
    $('#' + control).html("<option value=''></option>").trigger('liszt:updated');
    $.ajax({
        type: "POST",
        url: "/modulos/common.aspx/LoadProvincias",
        data: "{idPais: " + pais + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            $(data.d).each(function () {
                $("#" + control).append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $('#' + control).trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}

function calcularTotal(id) {

    var unitario = $("#txtUnitario_" + id).val();
    var cantidad = $("#txtCantidad_" + id).val();
  
    if (unitario != null && cantidad != null) {
        var total = cantidad * unitario;
        $("#txtTotal_" + id).val(total);
      
    }
    
    var totalTabla = 0;
    $(".totales").each(function () {
     
        if (!isNaN($(this).attr("value")) && $(this).attr("value") != null && $(this).attr("value") != "") {
            totalTabla = totalTabla + parseFloat($(this).attr("value"));
           
        }
    });
    
    
      $("#lblTotal").html(parseFloat(totalTabla));
    
}

function ActualizarTotalKeyPress(event, i) {

    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        calcularTotal(i);
        return false;
    }
}
