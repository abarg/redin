var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);


$(document).ready(function () {
    setTimeout("ObtenerPromedioTicket()", 1000);
    //setTimeout("ObtenerTotalTasaUsoMensual()", 1000);
    setTimeout("ObtenerTotalTR()", 1000);
    setTimeout("ObtenerTotalSociosSexo()", 1000);
    //setTimeout("ObtenerTotalArancelMensual()", 1000);
    //setTimeout("ObtenerTotalPuntosMensual()", 1000);
    setTimeout("ObtenerTotalFacturacion()", 1000);

    setTimeout("gebo_charts.fl_puntos_otorgados()", 2000);
    setTimeout("gebo_charts.fl_facturacion_ahorro()", 2000);
    setTimeout("gebo_charts.fl_facturacion_ahorro_detalle()", 2000);

    setTimeout("ObtenerTop10Socios()", 1500);
    setTimeout("ObtenerTop10Comercios()", 1500);
});

//* charts
gebo_charts = {
    fl_puntos_otorgados: function () {
        var elem = $('#fl_puntos_otorgados');
        //var data = ObtenerPuntosOtorgados();

        var otorgados = ObtenerPuntosOtorgados();
        var canjeados = ObtenerPuntosCanjeados();

        var data = [
            { label: "Otorgado", data: otorgados },
            { label: "Canjeado", data: canjeados }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    fl_facturacion_ahorro: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro');

        var pagado = ObtenerImportePagado();
        var ahorro = ObtenerImporteAhorro();
        var original = ObtenerImporteOriginal();

        var data = [
            { label: "Pagado", data: pagado },
            { label: "Ahorro", data: ahorro },
            { label: "Original", data: original }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_facturacion_ahorro_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro_detalle');
        var pagado = ObtenerImportePagadoDiario();
        var ahorro = ObtenerImporteAhorroDiario();
        var original = ObtenerImporteOriginalDiario();

        for (var i = 0; i < pagado.length; ++i) { pagado[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < ahorro.length; ++i) { ahorro[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < original.length; ++i) { original[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Pagado", data: pagado },
                { label: "Ahorro", data: ahorro },
                { label: "Original", data: original }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = $" + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    }

};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerPuntosOtorgados() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPuntosOtorgados",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerPuntosCanjeados() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPuntosCanjeados",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Facturaci�n & Ahorro, 4 meses*/
function ObtenerImporteAhorro() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteAhorro",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagado() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImportePagado",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginal() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteOriginal",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Facturaci�n & Ahorro, 30 d�as*/
function ObtenerImporteAhorroDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteAhorroDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginalDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteOriginalDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {;
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
            /*ddata.push([Date.UTC(2014, 0, 21, 08, 0, 0), 1180]);
            ddata.push([Date.UTC(2014, 0, 22, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 23, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 24, 08, 0, 0), 1212]);
            ddata.push([Date.UTC(2014, 0, 25, 08, 0, 0), 9922]);
            ddata.push([Date.UTC(2014, 0, 26, 08, 0, 0), 1888]);*/

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagadoDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImportePagadoDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

/*METODOS DE OBTENCION DE DATOS DE LOS ICONOS*/

function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}


function ObtenerTotalSociosSexo() {
    //$("#resultado_sexo").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalSociosPorSexo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var aux = data.d.split(",");
                $("#resultado_sexo").html(aux[0]);
                $("#totalSexo").html(aux[1]);
            }
        }
    });
}

function ObtenerTotalTR() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTR",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tr_mensual").html(data.d);
            }
        }
    });
}

function ObtenerPromedioTicket() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioTicket",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_ticketpromedio_mensual").html(data.d);
            }
        }
    });
}

function ObtenerTotalFacturacion() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerFacturacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_facturacion").html(data.d);
            }
        }
    });
}


