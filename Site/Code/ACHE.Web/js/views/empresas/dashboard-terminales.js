﻿var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);



$(document).ready(function () {



    setTimeout("gebo_charts.fl_estado_terminales()", 2000);
});

//* charts
gebo_charts = {


    fl_estado_terminales: function () {
        // Setup the placeholder reference
        var elem = $('#fl_estado_terminales');

        var data = ObtenerEstadoTerminales();
        var colors = ["#000", "#F5AA1A", "#70A415", "#f00", "#f8f412", "#0066FF", "#ccc", "#a47e3c"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    }

};

function ObtenerEstadoTerminales() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard-terminales.aspx/obtenerEstadoTerminales",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblEstadoTerminales").html("Estado de las terminales (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}




function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

function verDetalleTerminales(estado) {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Terminales");

    $.ajax({
        type: "POST",
        url: "dashboard-terminales.aspx/obtenerDetalleTerminales",
        data: "{estado: '" + estado + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }

            $('#modalDetalle').modal('show');
        }
    });
}
