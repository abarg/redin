﻿function detalleAlerta(id) {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Alertas");
    $("#headDetalle").html("<tr><th>Fecha</th><th>Origen</th><th>Operacion</th><th>Importe</th><th>Tarjeta</th><th>Socio</th></tr>");
    $.ajax({
        type: "POST",
        url: "alertas.aspx/obtenerDetalleAlerta",
        data: "{id: " + parseInt(id) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}
