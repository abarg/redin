﻿function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var tarjetaDesde = '';
        var tarjetaHasta = '';
        if ($("#txtTarjetasDesde").val() != null)
            tarjetaDesde = $("#txtTarjetasDesde").val();
        if ($("#txtTarjetasHasta").val() != null)
            tarjetaHasta = $("#txtTarjetasHasta").val();

        var info = "{ id: " + parseInt($("#hdnID").val())
            + ", codigo: '" + $("#txtCodigo").val()
            + "', idComercio: " + parseInt($("#ddlComercio").val())
            + ", fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', respuesta: '" + $("#txtRespuesta").val()
            //+ "', stock: " + parseInt($("#txtStock").val())
            + "', activo: " + $("#chkActivo").is(':checked')
            + ", respuestaSinStock: '" + $("#txtRespuestaSinStock").val()
            + "', respuestaVencida: '" + $("#txtRespuestaVencido").val()
            + "', tarjetasDesde: '" + tarjetaDesde
            + "', tarjetasHasta: '" + tarjetaHasta
             + "', estado: '" + $("#ddlEstado").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "keywordse.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                if(!data.d)
                    window.location.href = "keywords.aspx?Envio=false";
            },
            error: function (response) {                
                var r = jQuery.parseJSON(response.responseText);
                if (r.Message != "Error al enviar correo.") {
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
                else
                    window.location.href = "keywords.aspx?Envio=false";
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

$(document).ready(function () {
    configControls();
});


function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $('.rtaMaxTar').jqEasyCounter({
        'maxChars': 144,
        'maxCharsWarning': 130,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 145,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });



    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    configDatePicker();

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, numeros, puntos, guiones medios y espacios en blanco.");

    $("#txtStock").numeric();

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtCodigo").val());
        $("#divUploadFoto").show();
    }
    else
        $("#litTitulo").html("Alta de Keyword");
}