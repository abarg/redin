﻿function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ comercio: '" + $("#txtComercio").val()
            + "', codigo: '" + $("#txtCodigo").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "keywords.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    configDatePicker();

    $("#txtComercio, #txtCodigo, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDKeyword: { type: "integer" },
                        Comercio: { type: "string" },
                        Codigo: { type: "string" },
                        Stock: { type: "integer" },
                        FechaInicio: { type: "date" },
                        FechaFin: { type: "date" },
                        Activo: { type: "string" },
                        Estado: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "keywords.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDKeyword", title: "ID", width: "50px" },
            { field: "Comercio", title: "Comercio", width: "200px" },
            { field: "Codigo", title: "Codigo", width: "100px" },
            //{ field: "Stock", title: "Stock", width: "70px", attributes: { class: "colCenter" } },
            { field: "FechaInicio", title: "Vigencia desde", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "FechaFin", title: "Vigencia hasta", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "Activo", title: "Activo", width: "100px" },
            { field: "Estado", title: "Estado", width: "100px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        
        window.location.href = "keywordse.aspx?ID=" + dataItem.IDKeyword;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "keywords.aspx/Delete",
                data: "{ id: " + dataItem.IDKeyword + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function Nuevo() {
    window.location.href = "keywordse.aspx";
}

function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var comercio = $("#txtComercio").val();
    if (comercio != "") {
        $filter.push({ field: "Comercio", operator: "contains", value: comercio });
    }

    var codigo = $("#txtCodigo").val();
    if (codigo != "") {
        $filter.push({ field: "Codigo", operator: "contains", value: codigo });
    }
    var estado = $("#ddlEstado").val();
    if (estado != "") {
        $filter.push({ field: "Estado", operator: "equals", value: estado });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls()
});