﻿function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ keyword: '" + $("#ddlKeyword").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "consumos.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    configDatePicker();

    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDConsumo: { type: "integer" },
                        Keyword: { type: "string" },
                        PhoneNumber: { type: "string" },
                        Valido: { type: "string" },
                        Fecha: { type: "date" },
                        TarjetaUsada: { type: "string" },
                        Observaciones: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "consumos.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, keyword:$("#ddlKeyword").val(), fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Keyword", title: "Keyword", width: "100px" },
            { field: "PhoneNumber", title: "Celular", width: "100px" },
            { field: "Valido", title: "Valido", width: "80px" },
            { field: "Fecha", title: "Fecha", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "TarjetaUsada", title: "Tarjeta Usada", width: "100px" },
            { field: "Observaciones", title: "Observaciones", width: "200px" }
        ]
    });

}

function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    /*var key = $("#ddlKeyword").val();
    if (key != "") {
        $filter.push({ field: "Comercio", operator: "contains", value: key });
    }

    var codigo = $("#txtCodigo").val();
    if (codigo != "") {
        $filter.push({ field: "Codigo", operator: "contains", value: codigo });
    }*/

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls()
});