﻿var oTable = null;
function solucionarArancelYPuntos() {
    $('#spnCantTr').html("");
    $("#titSolucionar").html("Solucionar Alertas");
    var msg = "¿Desea continuar?";
    smoke.confirm(msg, function (e) {
        if (e) {
            $('#modalSolucion').modal('show');
            $("#imgLoading3").show();
            $.ajax({
                type: "POST",
                url: "home.aspx/solucionarArancelYPuntos",
                contentType: "application/json; charset=utf-8",
                dataType: "json",

                success: function (data) {
                    var cantTrAnt = $('#spnArancel').text();
                    var totalTr = cantTrAnt - data.d
                    $('#spnCantTr').html("<h5>Se han solucionado " + totalTr + " transacciones </h5>");

                    $('#spnArancel').html("<strong class='act-danger'>" + data.d + "</strong>");
                    $("#imgLoading3").hide();


                }
            });
        } else {
            //smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}
function verDetalle(tipo) {
    $("#hdnTipo").val(tipo);

    $("#btnExportar2").show();
    $("#imgLoading2").hide();
    $("#lnkDownload2").hide();

    $("#bodyDetalle2").html();

    $("#titDetalle2").html("Detalle de Errores");
    if (tipo == "info")
        $("#headDetalle2").html("<tr><th>Fecha</th><th>Origen</th><th>Operacion</th><th>Tarjeta</th><th>Importe</th><th>Establecimiento</th><th>Terminal</th></tr>");

    if (tipo == "tr" || tipo == "establecimientos")
        $("#headDetalle2").html("<tr><th>Fecha</th><th>Origen</th><th>Operacion</th><th>Tarjeta</th><th>Importe</th><th>Establecimiento</th><th>Terminal</th></tr>");

    else if (tipo == "terminales")
        $("#headDetalle2").html("<tr><th>Comercio</th><th>Tipo POS</th><th>Fecha</th><th>Origen</th><th>Operacion</th><th>Tarjeta</th><th>Importe</th><th>Establecimiento</th><th>Terminal</th></tr>");

    else if (tipo == "arancel")
        $("#headDetalle2").html("<tr><th>Comercio</th><th>Tipo POS</th><th>Fecha</th><th>Origen</th><th>Operacion</th><th>Tarjeta</th><th>Importe</th><th>Establecimiento</th><th>Terminal</th></tr>");

    else if (tipo != "repro" && tipo != "tar" && tipo != "sinTrans")
        $("#headDetalle2").html("<th>Fecha</th><th>Origen</th><th>Operacion</th><th>Tarjeta</th><th>Importe</th><th>Establecimiento</th><th>Terminal</th></tr>");
    
    //else if (tipo == "masDe1Trans")
    //    $("#headDetalle2").html("<tr><th>Comercio</th><th>Franquicia</th><th>Domicilio</th></tr>");

    else if (tipo == "repro" || tipo == "sinTrans")
        $("#headDetalle2").html("<tr><th>Comercio</th><th>Tipo POS</th><th>Terminal</th><th>Establecimiento</th><th>Franquicia</th><th>Domicilio</th></tr>");

    else if (tipo == "tar")
        $("#headDetalle2").html("<tr><th>Comercio</th><th>Tarjeta</th><th>Marca</th><th>Socio</th><th>Puntos</th><th>Fecha Ult TR</th></tr>");
    else if (tipo = "sociosSMS") {
        $("#headDetalle2").html("<tr><th>Socio</th><th>Numero Telefono</th><th>Empresa Telefono</th><th>Pais</th><th>Ciudad</th></tr>");
    }
    else
        $("#headDetalle2").html("<tr><th>Tarjeta</th><th>Marcas</th><th>Socio</th><th>Puntos</th><th>Fecha Ult TR</th></tr>");

    $.ajax({
        type: "POST",
        url: "home.aspx/obtenerDetalle",
        data: "{tipo: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle2").html(data.d);
            }

            $('#modalDetalle2').modal('show');
        }
    });
}

function verDetalleComercios(tipo) {

    $("#hdnTipo").val(tipo);

    $("#btnExportar").show();
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Errores");

    if (tipo == "info")
        $("#headDetalle").html("<tr><th>Comercio</th><th>Franquicia</th><th>Pendientes</th></tr>");
    else if (tipo == "sinTrans" || tipo == "masDe1Trans")
        $("#headDetalle").html("<tr><th>Comercio</th><th>Franquicia</th><th>Domicilio</th></tr>");
    else
        $("#headDetalle").html("<tr><th>Comercio</th><th>Tipo POS</th><th>Terminal</th><th>Establecimiento</th><th>Franquicia</th><th>Domicilio</th></tr>");



    $.ajax({
        type: "POST",
        url: "home.aspx/obtenerDetalleComercio",
        data: "{tipo: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyDetalle").html("");

                if (oTable != null) {

                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tableDetalle').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    if (tipo == "info") {
                        oTable.fnAddData([
                            data.d[i].NombreComercio,
                            data.d[i].NombreFranquicia,
                            data.d[i].Pendientes]
                          );
                    }
                    else if (tipo == "sinTrans")
                    {
                        oTable.fnAddData([
                        data.d[i].NombreComercio,
                        data.d[i].NombreFranquicia,
                        data.d[i].Domicilio]
                      );
                    }
                    else if (tipo == "masDe1Trans")
                    {
                        oTable.fnAddData([
                        data.d[i].NombreComercio,
                        data.d[i].NombreFranquicia,
                        data.d[i].Domicilio]
                      );
                    }
                    else {
                        oTable.fnAddData([
                        data.d[i].NombreComercio,
                        data.d[i].POSTipo,
                        data.d[i].POSTerminal,
                        data.d[i].NumEst,
                        data.d[i].NombreFranquicia,
                        data.d[i].Domicilio]
                      );
                    }
                }

                $("#tableDetalle_info").parent().remove();
                $("#tableDetalle").css("width", "100%");


                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyDetalle").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }

            $('#modalDetalle').modal('show');
        }
    });
}

function exportar(alerta) {
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "home.aspx/Exportar",
        data: "{ tipo: '" + $("#hdnTipo").val() + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function exportar2(alerta) {
    $("#lnkDownload2").hide();
    $("#imgLoading2").show();
    $("#btnExportar2").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "home.aspx/Exportar",
        data: "{ tipo: '" + $("#hdnTipo").val() + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoading2").hide();
                $("#lnkDownload2").show();
                $("#lnkDownload2").attr("href", data.d);
                $("#lnkDownload2").attr("download", data.d);
                $("#btnExportar2").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading2").hide();
            $("#lnkDownload2").hide();
            $("#btnExportar2").attr("disabled", false);
        }
    });
}