﻿var error = false;

function descargar() {
    $("#divError").html("");
    $("#divOK").hide();
    $("#divError").hide();
    $("#imgLoading").show();
    $(".download").hide();
    $("#btnDescargar").attr("disabled", true);

    exportar('AMEX');
    exportar('LAPOS');
    exportar('POSNET');

    if (error) {
        $("#divError").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
    else {
        $("#divError").hide();
        //$("#btnActualizarFecha").show();
    }
    $("#btnActualizarFecha").show();
    $("#imgLoading").hide();
    $("#btnDescargar").attr("disabled", false);
}

function actualizarFecha()
{
    var msg = "La fecha de reprogramación de los comercios será guardada y los mismos no aparecerán la próxima vez que genere éstas planillas.<br /> ¿Desea continuar?";
    smoke.confirm(msg, function (e) {
        if (e) {
            $("#btnActualizarFecha").attr("disabled", true);
            guardarFecha();
        } else {
            //smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}

function exportar(tipo) {
    $.ajax({
        type: "POST",
        url: "Reprogramaciones.aspx/Exportar",
        data: "{ tipo: '" + tipo + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, text) {
            if (data.d != "") {
                switch (tipo) {
                    case "AMEX":
                        $("#lnkDownload").show();
                        $("#lnkDownload").attr("href", data.d);
                        $("#lnkDownload").attr("download", data.d);
                        break;
                    case "LAPOS":
                        $("#lnkDownload2").show();
                        $("#lnkDownload2").attr("href", data.d);
                        $("#lnkDownload2").attr("download", data.d);
                        break;
                    case "POSNET":
                        $("#lnkDownload3").show();
                        $("#lnkDownload3").attr("href", data.d);
                        $("#lnkDownload3").attr("download", data.d);
                        break;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var err = eval("(" + xhr.responseText + ")");
            $("#divError").append(tipo + ": " + err.Message + "<br />");
            error = true;
        }
    });
}

function guardarFecha()
{
    $.ajax({
        type: "POST",
        url: "reprogramaciones.aspx/actualizarComercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data, text) {
            if (data.d != "") {
                $("#divOK").show();
                $("#btnActualizarFecha").attr("disabled", false);
                $("#btnActualizarFecha").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var err = eval("(" + xhr.responseText + ")");
            $("#divError").append(tipo + ": " + err.Message + "<br />");
            error = true;
        }
    });
}