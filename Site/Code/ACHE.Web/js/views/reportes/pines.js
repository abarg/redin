﻿function buscar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formReportePines').validate();
    $("#divGrid").html("");

    var idEmpresa = $("#cmbEmpresa").val();
    if (idEmpresa == "")
        idEmpresa = 0;
    var info = "{ idEmpresa: " + idEmpresa + " }";

    $.ajax({
        type: "POST",
        url: "pines.aspx/buscar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divGrid").html(data.d);
                $("#grid").kendoGrid({
                    height: 500,
                    scrollable: true,
                    sortable: true,
                });
                $("#imgLoading").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
        }
    });
}

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $("#btnExportar").attr("disabled", true);

    var idEmpresa = $("#cmbEmpresa").val();
    if (idEmpresa == "")
        idEmpresa = 0;

    var info = "{ idEmpresa: " + idEmpresa + " }";

    $.ajax({
        type: "POST",
        url: "pines.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();

                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function validar(valor) {
    if (valor == "")
        return false;
    else
        return true;
}

function verTodos() {
    $("#cmbEmpresa").val("");
    buscar();
}