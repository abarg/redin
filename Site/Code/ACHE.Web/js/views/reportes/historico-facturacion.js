﻿

function exportar() {

    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $("#btnExportar").attr("disabled", true);


    $.ajax({
        type: "POST",
        url: "HistoricoFacturacion.aspx/Exportar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();

                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function showimg() {
    $("#imgLoading").show();
    return true;
}

function CrearTabla() {
    var eTable = "<table><tbody>"
    var data= ObtenerCantidadTarjetas();
    //alert(data[5]);
}

function mostrarReporte(id) {
    if (id > 0)
        window.location.href = "HistoricoFacturacion.aspx?IDFranquicia=" + id;
    else 
        window.location.href = "HistoricoFacturacion.aspx";

    

}