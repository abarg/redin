﻿function exportAExcel() {
    $("#divError").hide();
    $("#lnkDownload").hide();

    $("#imgLoading").show();

    var info = "{ idComercio: '" + $("#ddlComercio").val() + "',  estado: '" + $("#ddlEstado").val() + "'}";

    $.ajax({
        type: "POST",
        url: "cc-general.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                //window.location.href = window.location.href.split("/bankInformation.aspx").join(data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
        }
    });

}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    LoadComercios("../common.aspx/LoadCasasMatrices", "ddlComercio");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Comercio: { type: "string" },
                        Importe: { type: "decimal" },
                        Abonado: { type: "decimal" },
                        Saldo: { type: "decimal" },
                        FechaUltimoPago: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: false,
            transport: {
                read: {
                    url: "cc-general.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idComercio: $("#ddlComercio").val(), estado: $("#ddlEstado").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Comercio", title: "Comercio", width: "300px" },
            { field: "Importe", title: "Importe", width: "100px", format: "{0:c}" },
            { field: "Abonado", title: "Abonado", width: "100px", format: "{0:c}" },
            { field: "Saldo", title: "Saldo", width: "100px", format: "{0:c}" },
            { field: "FechaUltimoPago", title: "Fecha Ult Pago", width: "80px" },
            { title: "Estado", template: "#= renderEstado(data) #", width: "80px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/></div>" }, title: "Ver", width: "50px" }
        ]
    });


    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#bodyDetalle").html();

        $("#titDetalle").html("Detalle " + dataItem.Empresa);
        $("#headDetalle").html("<tr><th>Detalle</th><th>Fecha</th><th>Importe</th></tr>");

        $.ajax({
            type: "POST",
            url: "cc-general.aspx/obtenerDetalle",
            data: "{idComercio: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalDetalle').modal('show');
            }
        });
    });
}

function renderEstado(data) {
    var html = "";
    if (data.Saldo > 1)
        html = "<div align='center'><span class='label label-danger'>Deudor</span></div>";
    else if (data.Saldo >= -1 && data.Saldo <= 1)
        html = "<div align='center'><span class='label label-success'>Al dia</span></div>";
    if (data.Saldo < -1)
        html = "<div align='center'><span class='label label-warning'>A favor</span></div>";

    return html;
}

function filtrar() {
    $("#lnkDownload").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});