﻿function buscar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formReporteComercios').validate();
    $("#divGrid").html("");

    var info = "{estado: '" + $("#ddlEstado").val()
        + "', sds: '" + $("#txtSDS").val()
        + "', nombre: '" + $("#txtNumeroEst").val()
        + "', marca: " + parseInt($("#ddlMarcas").val())
        + ", franquicia: " + parseInt($("#ddlFranquicias").val())
        + ", fechaDesde: '" + $("#txtFechaDesde").val()
        + "', fechaHasta: '" + $("#txtFechaHasta").val()
        + "', tipoPos: '" + $("#txtTipoPos").val()
        + "'}";

    //alert(info);

    $.ajax({
        type: "POST",
        url: "Terminales.aspx/buscar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divGrid").html(data.d);
                $("#grid").kendoGrid({
                    height: 500,
                    scrollable: true,
                    sortable: true,
                });
                $("#imgLoading").hide();
                //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
        }
    });
}

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();

    $("#btnExportar").attr("disabled", true);

    //var info = "{estado: '" + $("#ddlEstado").val()
    //   + "', sds: '" + $("#txtSDS").val()
    //   + "', nombre: '" + $("#txtNumeroEst").val()
    //   + "'}";

    var info = "{estado: '" + $("#ddlEstado").val()
       + "', sds: '" + $("#txtSDS").val()
       + "', nombre: '" + $("#txtNumeroEst").val()
       + "', marca: " + parseInt($("#ddlMarcas").val())
       + ", franquicia: " + parseInt($("#ddlFranquicias").val())
       + ", fechaDesde: '" + $("#txtFechaDesde").val()
       + "', fechaHasta: '" + $("#txtFechaHasta").val()
        + "', tipoPos: '" + $("#txtTipoPos").val()
       + "'}";

    $.ajax({
        type: "POST",
        url: "Terminales.aspx/exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });

}

$(document).ready(function () {
    $("#txtSDS").numeric();

    $("#txtSDS, #txtNumeroEst, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
});