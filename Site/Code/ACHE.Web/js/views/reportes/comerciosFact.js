﻿function buscar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formReporteComercios').validate();
    $("#divGrid").html("");
    var rdbTipo = "A"; if ($("#rdbDetallado")[0].checked == true) rdbTipo = "D";

    if ($('#formReporteComercios').valid()) {
        var info = "{ Tipo: '" + rdbTipo
            + "', FechaHasta: '" + $("#txtFechaHasta").val()
            + "', FechaDesde: '" + $("#txtFechaDesde").val()
            + "', SDS: '" + $("#txtSDS").val()
            + "', Nombre: '" + $("#txtComercio").val()
            + "', CantOper: '" + $("#txtCantOper").val()
            + "', IDMarca: " + parseInt($("#ddlMarcas").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "ComerciosFact.aspx/buscar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divGrid").html(data.d);
                    $("#grid").kendoGrid({
                        height: 500,
                        scrollable: true,
                        sortable: true,
                    });
                    $("#imgLoading").hide();
                    //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
            }
        });
    }
    else {
        return false;
        $("#imgLoading").hide();
    }
}

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $('#formReporteComercios').validate();
    $("#btnExportar").attr("disabled", true);

    var rdbTipo = "A"; if ($("#rdbDetallado")[0].checked == true) rdbTipo = "D";

    if ($('#formReporteComercios').valid()) {
        var info = "{ Tipo: '" + rdbTipo
            + "', FechaHasta: '" + $("#txtFechaHasta").val()
            + "', FechaDesde: '" + $("#txtFechaDesde").val()
            + "', SDS: '" + $("#txtSDS").val()
            + "', Nombre: '" + $("#txtComercio").val()
            + "', CantOper: '" + $("#txtCantOper").val()
            + "', IDMarca: " + parseInt($("#ddlMarcas").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "ComerciosFact.aspx/exportar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                    $("#btnExportar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#btnExportar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
        $("#imgLoading").hide();
        $("#lnkDownload").hide();
        $("#btnExportar").attr("disabled", false);
    }
}

$(document).ready(function () {
    $("#txtSDS").numeric();
    $("#txtCantOper").numeric();

    $("#txtSDS, #txtCantOper, #txtComercio, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            buscar();
            return false;
        }
    });

    //configGrid();
    configDatePicker();
    
    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
});