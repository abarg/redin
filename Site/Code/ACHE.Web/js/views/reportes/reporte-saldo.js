﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "reporte-saldo.aspx/Exportar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDComercio: { type: "integer" },
                        Comercio: { type: "string" },
                        PeriodoDesde: { type: "date" },
                        PeriodoHasta: { type: "date" },
                        Saldo: { type: "decimal" },
                        CantFacturas: { type: "integer" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "reporte-saldo.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "IDComercio", title: "IDComercio", width: "100px", hidden: true },
            { title: "Ver facturas", template: "#= renderOptions() #", width: "30px" },
            { field: "Comercio", title: "Comercio", width: "100px" },
            { field: "CantFacturas", title: "Cantidad de Facturas", width: "50px" },
            { field: "PeriodoDesde", title: "Periodo Desde", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "PeriodoHasta", title: "Periodo Hasta", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "Saldo", title: "Saldo", width: "100px", format: "{0:c}" },
        ]
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#bodyDetalle").html();

        $("#titDetalle").html("Facturas");

        $("#headDetalle").html("<tr><th>Período desde</th><th>Período hasta</th><th>Importe</th></tr>");

        $.ajax({
            type: "POST",
            url: "reporte-saldo.aspx/obtenerFacturas",
            data: "{ idComercio: " + parseInt(dataItem.IDComercio) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalDetalle').modal('show');
            }
        });
    });
}

function renderOptions() {
    return "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/></div>";
}

$(document).ready(function () {
    configControls();
});