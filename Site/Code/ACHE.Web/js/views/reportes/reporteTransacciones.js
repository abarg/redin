﻿function configControls() {

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtFechaDesde, #txtFechaHasta, #ddlMarcas, #ddlFranquicias, #txtComercio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        PeriodoDesde: { type: "date" },
                        PeriodoHasta: { type: "date" },
                        Mes: { type: "string" },
                        NombreMes: { type: "string" },
                        Año: { type: "string" },
                        CantTrans: { type: "int" },
                        Comercio: { type: "string" },
                        Franquicia: { type: "string" },
                        Marca: { type: "string" },
                        DomicilioComercio: { type: "string" },
                        IDMarca: { type: "integer" },
                        IDFranquicia: { type: "integer" },
                        IDComercio: { type: "integer" },
                        IDDomicilio: { type: "integer" }
                    //    PESOS: { type: "decimal" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "ReporteTransacciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, PeriodoDesde: $("#txtFechaDesde").val(), PeriodoHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Año", title: "Año", width: "80px" },
            { field: "NombreMes", title: "Mes", width: "80px" },
            { field: "Franquicia", title: "Franquicia", width: "80px" },
            { field: "Marca", title: "Marca", width: "80px" },
            { field: "Comercio", title: "Comercio", width: "110px" },
            { field: "DomicilioComercio", title: "Domicilio Comercio", width: "110px" },
            { field: "CantTrans", title: "Cantidad", width: "40px" },


        ]
    });
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    
    var comercio = $("#txtComercio").val();
    if (comercio != "") {
        $filter.push({ field: "Comercio", operator: "contains", value: comercio });
    }

    var marca = $("#ddlMarcas").val();
    if (marca != "0") {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
    }

    var franquicia = $("#ddlFranquicias").val();
    if (franquicia != "0") {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
    }
    grid.dataSource.filter($filter);
}

$(document).ready(function () {

    configControls();
});

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $('#formReporte').validate();
    $("#btnExportar").attr("disabled", true);


    var marca = 0;
    if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "") {
        marca = $("#ddlMarcas").val();
    }

    var franquicia = 0
    if ($("#ddlFranquicias").val() != null && $("#ddlFranquicias").val() != "") {
        franquicia = $("#ddlFranquicias").val();
    }

    var comercio = ""
    if ($("#txtComercio").val() != null && $("#txtComercio").val() != "") {
        comercio = $("#txtComercio").val();
    }


    if ($('#formReporte').valid()) {
        var info = "{ PeriodoDesde: '" + $("#txtFechaDesde").val()
            + "', PeriodoHasta: '" + $("#txtFechaHasta").val()
            + "', idFranquicia: " + parseInt(franquicia)
            + ", idMarca: " + parseInt(marca)
            + ", comercio: '" + comercio
            + "'}";

        $.ajax({
            type: "POST",
            url: "ReporteTransacciones.aspx/exportar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                    $("#btnExportar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#btnExportar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
        $("#imgLoading").hide();
        $("#lnkDownload").hide();
        $("#btnExportar").attr("disabled", false);
    }
}


