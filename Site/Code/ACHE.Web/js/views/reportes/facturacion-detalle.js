﻿function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $('#formReporte').validate();
    $("#btnExportar").attr("disabled", true);

    if ($('#formReporte').valid()) {
        var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', sds: '" + $("#txtSDS").val()
            + "', nombre: '" + $("#txtComercio").val()
            + "', idMarca: " + $("#ddlMarcas").val()
            + ", cant: '" + $("#txtCantOper").val()
            + "', operacion: '" + $("#ddlOperacion").val()
            + "', idFranquicia: " + $("#ddlFranquicias").val()
            + "}";

        $.ajax({
            type: "POST",
            url: "facturacion-detalle.aspx/exportar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                    $("#btnExportar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#btnExportar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
        $("#imgLoading").hide();
        $("#lnkDownload").hide();
        $("#btnExportar").attr("disabled", false);
    }
}

function configControls() {
    $("#txtSDS, #txtCantOper").numeric();

    $("#txtSDS, #txtComercio, #txtFechaDesde, #txtFechaHasta, #txtCantOper").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Fecha: { type: "date" }, 
                        Origen: { type: "string" },
                        POSTipo: { type: "string" },
                        POSSistema: { type: "string" },
                        POSTerminal: { type: "string" },
                        Operacion: { type: "string" },
                        SDS: { type: "string" },
                        NombreFantasia: { type: "string" },
                        FranqComercio: { type: "string" },
                        Tarjeta: { type: "string" },
                        Socio: { type: "string" },
                        NroDocumento: { type: "string" },
                        NumEst: { type: "string" },
                        Marca: { type: "string" },
                        IDMarca: { type: "integer" },
                        ImporteOriginal: { type: "number" },
                        ImporteAhorro: { type: "number" },
                        ImportePagado: { type: "number" },
                        CostoRedIn: { type: "number" },
                        Ticket: { type: "number" },
                        Arancel: { type: "number" },
                        Puntos: { type: "number" },
                        NetoGrabado: { type: "number" },
                        IVA: { type: "number" },
                        TotalFactura: { type: "number" },
                        IIBB: { type: "number" },
                        Tish: { type: "number" },
                        CostoPuntos: { type: "number" },
                        CostoPos: { type: "number" },
                        NetoTax: { type: "number" },
                        Franquicia: { type: "number" },
                        IDFranquicia: { type: "integer" }
                        //CantOperaciones: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "facturacion-detalle.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, idFranquicia: $("#ddlFranquicias").val(), fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Fecha", title: "Fecha", width: "80px", format: "{0:dd/MM/yyyy}" },
                        { field: "Fecha", title: "Horario", width: "80px", format: "{0:HH:MM:ss}" },

            { field: "Origen", title: "Origen", width: "60px" },
            { field: "POSTipo", title: "Tipo", width: "60px" },
            { field: "POSSistema", title: "Tipo Term", width: "80px" },
            { field: "POSTerminal", title: "Terminal", width: "80px" },
            { field: "Operacion", title: "Operacion", width: "80px" },
            { field: "SDS", title: "SDS", width: "75px" },
            { field: "NombreFantasia", title: "Empresa", width: "150px" },
            { field: "Tarjeta", title: "Tarjeta", width: "140px" },
            //{ field: "CantOperaciones", title: "Cant", width: "50px" },
            { field: "NroDocumento", title: "CUIT", width: "100px" },
            { field: "NumEst", title: "Establecimiento", width: "120px" },
            { field: "FranqComercio", title: "Franq Comercio", width: "100px" },
            { field: "Marca", title: "Marca", width: "100px" },
            { field: "Socio", title: "Socio", width: "150px" },
            { field: "ImporteOriginal", title: "Importe", width: "80px", format: "{0:c}" },
            { field: "ImporteAhorro", title: "Ahorro", width: "80px", format: "{0:c}" },
            { field: "ImportePagado", title: "Pagado", width: "80px", format: "{0:c}" },
            { field: "CostoRedIn", title: "Costo Red", width: "80px", format: "{0:c}" },
            { field: "Ticket", title: "Ticket", width: "80px", format: "{0:c}" },
            { field: "Arancel", title: "Arancel", width: "80px", format: "{0:c}" },
            { field: "Puntos", title: "Puntos", width: "80px", format: "{0:c}" },
            { field: "NetoGrabado", title: "Neto Grab", width: "80px", format: "{0:c}" },
            { field: "IVA", title: "IVA", width: "80px", format: "{0:c}" },
            { field: "TotalFactura", title: "Total Fact", width: "80px", format: "{0:c}" },
            { field: "IIBB", title: "IIBB", width: "80px", format: "{0:c}" },
            { field: "Tish", title: "Tish", width: "80px", format: "{0:c}" },
            { field: "CostoPuntos", title: "Costo Puntos", width: "90px", format: "{0:c}" },
            { field: "CostoPos", title: "Costo Pos", width: "80px", format: "{0:c}" },
            { field: "NetoTax", title: "Neto Tax", width: "80px", format: "{0:c}" },
            { field: "Franquicia", title: "Franquicia", width: "80px", format: "{0:c}" }
        ]
    });

    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formReporte').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();

        /*var Tarjeta = $("#txtTarjeta").val();
        if (Tarjeta != "") {
            $filter.push({ field: "Tarjeta", operator: "contains", value: Tarjeta });
        }*/

        var sds = $("#txtSDS").val();
        if (sds != "") {
            $filter.push({ field: "SDS", operator: "contains", value: sds });
        }

        var comercio = $("#txtComercio").val();
        if (comercio != "") {
            $filter.push({ field: "NombreFantasia", operator: "contains", value: comercio.toUpperCase() });
        }

        var marca = $("#ddlMarcas").val();
        if (marca != "0") {
            $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
        }

        var cant = $("#txtCantOper").val();
        if (cant != "") {
            $filter.push({ field: "CantOperaciones", operator: "gte", value: parseInt(cant) });
        }

        var ope = $("#ddlOperacion").val();
        if (ope != "") {
            $filter.push({ field: "Operacion", operator: "equal", value: ope });
        }

        var franquicia = $("#ddlFranquicias").val();
        if (franquicia != "0") {
            $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
        }

        grid.dataSource.filter($filter);
    }
}

$(document).ready(function () {
    configControls();
});