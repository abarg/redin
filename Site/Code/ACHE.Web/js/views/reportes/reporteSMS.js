﻿function configControls() {
 
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDMarca: { type: "integer" },
                        Nombre: { type: "string" },
                        cantEnviados: { type: "int" },
                        cantNoenviados: { type: "int" },
                        cant: { type: "int" },
                        Costo: { type: "decimal" },
                        Tipo: { type: "string" },
                        //Enviadas: { type: "int" },
                        //NoEnviadas: { type: "int" },
                        //Leidas: { type: "int" },
                        PeriodoDesde: { type: "string" },
                        PeriodoHasta: { type: "string" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "reporteSMS.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val(), IDMarca: $("#ddlMarcas").val(), mensaje: $("#ddlMensaje").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Nombre", title: "Nombre", width: "80px" },
            { title: "Enviadas", template: "#= renderEnviadas(data) #", width: "80px" },
            //{ title: "No Enviadas", template: "#= renderNoEnviadas(data) #", width: "80px" },
            //{ field: "cantEnviados", title: "Enviados", width: "60px" },
            { field: "cantNoenviados", title: "No Enviados", width: "60px" },
            { field: "Costo", title: "Costo", width: "60px" },
            { field: "Tipo", title: "Tipo Mensaje", width: "60px" }
            //{ title: "Leidas", template: "#= renderLeidas(data) #", width: "80px" }
        ]
    });

    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function filter() {
   
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formReporte').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();
       

        var marca = $("#ddlMarcas").val();
        if (marca != "0") {
            $filter.push({ field: "idMarca", operator: "equal", value: parseInt(marca) });
        }
        grid.dataSource.filter($filter);
    }
}


$(document).ready(function () {

    configControls();
});

function renderEnviadas(data) {
    var html = "<div align='center'>";
    html += "<a href=\"javascript:showDetalle('E','" + data.PeriodoDesde + "','" + data.PeriodoHasta + "','" + data.IDMarca + "','" + data.Tipo + "','" + data.Costo + "');\">" + data.cantEnviados + "</a>";
    html += "</div>";

    return html;
}

//function renderNoEnviadas(data) {
//    var html = "<div align='center'>";
//    html += "<a href=\"javascript:showDetalle('N','" + data.PeriodoDesde + "','" + data.PeriodoHasta + "','" + $("#ddlMarcas").val() + "','" + $("#ddlMensaje").val() + "','" + data.Costo + "');\">" + data.cantNoenviados + "</a>";
//    html += "</div>";

//    return html;
//}

var gbl_enviado = "";
var gbl_desde = "";
var gbl_hasta = "";
var gbl_idMarca = 0;
var gbl_mensaje = "";
var gbl_costo = "";

function showDetalle(enviado, desde, hasta, idMarca, mensaje, costo) {
    $("#lnkDownload").hide();
    gbl_enviado = enviado;
    gbl_desde = desde;
    gbl_hasta = hasta;
    gbl_idMarca = idMarca;
    gbl_mensaje = mensaje;
    gbl_costo = costo;


    $("#bodyDetalle").html();
    if (enviado == "E")
        $("#titDetalle").html("Detalle de enviados");       
    //else if (enviado == "N")
    //    $("#titDetalle").html("Detalle de NO enviadas");
    //else if (enviado == "L")
    //    $("#titDetalle").html("Detalle de Leidas");
    var info = "{ desde: '" + desde
           + "', hasta: '" + hasta
           + "', idMarca: " + parseInt(idMarca)
           + ", mensaje: '" + mensaje
           + "', costo: '" + costo
           + "'}";
    //alert(info);
    $.ajax({
        type: "POST",
        url: "reporteSMS.aspx/obtenerDetalle",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //alert(data);
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}


function exportar() {
    $("#btnExportar").attr("disabled", true);
    $("#lnkDownload").hide();

    var info = "{ desde: '" + gbl_desde
           + "', hasta: '" + gbl_hasta
           + "', idMarca: " + parseInt(gbl_idMarca)
           + ", mensaje: '" + gbl_mensaje
           + "', costo: '" + gbl_costo
           + "'}";

    $.ajax({
        type: "POST",
        url: "reporteSMS.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}
