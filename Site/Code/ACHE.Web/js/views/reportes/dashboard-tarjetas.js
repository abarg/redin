var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);

$(document).ready(function () {
    setTimeout("gebo_charts.fl_tarjetas_activas()", 2000);
    //setTimeout("gebo_charts.fl_tarjetas_marca()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_asignadas()", 2000);
        
});

//* charts
gebo_charts = {
    fl_tarjetas_activas: function () {

        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_activas');

        var activas = ObtenerTarjetasActivas();
        var inactivas = ObtenerTarjetasInactivas();

        var data = [
            { label: "Activas", data: activas },
            { label: "Inactivas", data: inactivas }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_marca: function () {
        ObtenerTarjetasMarca2();
        //// Setup the placeholder reference
        //var elem = $('#fl_tarjetas_marca');

        //var data = ObtenerTarjetasMarca();
        ////var colors = [ "#eadac8", "#dcc1a3", "#cea97e", "#c09059", "#a8763f", "#835c31", "#5e4223", "#392815" ];

        //// Setup the flot chart using our data
        //function a_plotWithColors() {
        //    fl_a_plot = $.plot(elem, data,
        //        {
        //            //label: "Visitors by Location",
        //            series: {
        //                pie: {
        //                    show: true,
        //                    innerRadius: 0.5,
        //                    highlight: {
        //                        opacity: 0.2
        //                    }
        //                }
        //            },
        //            legend: {
        //                noColumns: 3,
        //                labelBoxBorderColor: "#858585",
        //                position: "sw",
        //                //margin: [0, 100],
        //                container: $("#fl_tarjetas_marca_legend")

        //            },
        //            grid: {
        //                hoverable: true,
        //                clickable: true
        //            }/*,
        //            colors: colors*/
        //        }
        //    );
        //}

        //a_plotWithColors();

        //// Create a tooltip on our chart
        //elem.qtip({
        //    prerender: true,
        //    content: 'Loading...', // Use a loading message primarily
        //    position: {
        //        viewport: $(window), // Keep it visible within the window if possible
        //        target: 'mouse', // Position it in relation to the mouse
        //        adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
        //    },
        //    show: false, // We'll show it programatically, so no show event is needed
        //    style: {
        //        classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
        //        tip: false // Remove the default tip.
        //    }
        //});

        //// Bind the plot hover
        //elem.on('plothover', function (event, coords, item) {
        //    // Grab the API reference
        //    var self = $(this),
        //        api = $(this).qtip(),
        //        previousPoint, content,

        //    // Setup a visually pleasing rounding function
        //    round = function (x) { return Math.round(x * 1000) / 1000; };

        //    // If we weren't passed the item object, hide the tooltip and remove cached point data
        //    if (!item) {
        //        api.cache.point = false;
        //        return api.hide(event);
        //    }

        //    // Proceed only if the data point has changed
        //    previousPoint = api.cache.point;
        //    if (previousPoint !== item.seriesIndex) {
        //        percent = parseFloat(item.series.percent).toFixed(2);
        //        // Update the cached point data
        //        api.cache.point = item.seriesIndex;

        //        // Setup new content
        //        content = item.series.label + ' ' + percent + '%';

        //        // Update the tooltip content
        //        api.set('content.text', content);

        //        // Make sure we don't get problems with animations
        //        api.elements.tooltip.stop(1, 1);

        //        // Show the tooltip, passing the coordinates
        //        api.show(coords);
        //    }
        //});
    },

    fl_tarjetas_asignadas: function () {
        var elem = $('#fl_tarjetas_asignadas');

        var data = ObtenerTarjetasAsignadas();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    }

    

};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerTarjetasImpresas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasEmitidasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasLabels() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([i, data[i].label]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasMarca() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasPorMarca",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push(data[i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasAsignadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasAsignadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}



function ObtenerTarjetasMarca2() {
    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/ObtenerTarjetasPorMarca",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyMarcas").html("");

                if (oTable != null) {
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                }

                oTable = $('#tablaMarcas').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 10,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ning�n dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "�ltimo",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable.fnAddData([
                        data.d[i].Marca,
                        data.d[i].Tarjetas]
                      );
                }

                //$("#tableDetalle_info").parent().remove();
                $("#tablaMarcas").css("width", "100%");

                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyMarcas").html("<tr><td colspan='2'>No hay un detalle disponible</td></tr>");
            }
        }
    });
}

function ExportarTarjetasMarca() {
    $("#lnkDownloadMarca").hide();
    $("#imgLoadingMarca").show();
    $("#btnExportarMarca").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/ExportarTarjetasPorMarca",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoadingMarca").hide();
                $("#lnkDownloadMarca").show();
                $("#lnkDownloadMarca").attr("href", data.d);
                $("#lnkDownloadMarca").attr("download", data.d);
                $("#btnExportarMarca").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#imgLoadingAsignadas").hide();
            $("#imgLoadingMarca").hide();
            $("#btnExportarMarca").attr("disabled", false);
        }
    });
}



function ExportarTarjetasImpresas() {
    $("#lnkDownloadEmitidas").hide();
    $("#imgLoadingEmitidas").show();
    $("#btnExportarEmitidas").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/ExportarTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoadingEmitidas").hide();
                $("#lnkDownloadEmitidas").show();
                $("#lnkDownloadEmitidas").attr("href", data.d);
                $("#lnkDownloadEmitidas").attr("download", data.d);
                $("#btnExportarEmitidas").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingEmitidas").hide();
            $("#lnkDownloadEmitidas").hide();
            $("#btnExportarEmitidas").attr("disabled", false);
        }
    });
}

/*Tarjetas activas & inactivas, 4 meses*/
function ObtenerTarjetasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasInactivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard-tarjetas.aspx/obtenerTarjetasInactivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}