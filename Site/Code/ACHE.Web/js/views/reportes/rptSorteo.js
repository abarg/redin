﻿function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();
    $('#formReporte').validate();
    $("#btnExportar").attr("disabled", true);

    if ($('#formReporte').valid()) {
        var info = "{ idSorteo: '" + $("#ddlSorteos").val()
            + "',socio: '" + $("#txtSocio").val()
            + "',tarjeta: '" + $("#txtTarjeta").val()
            + "',fechaDesde: '" + $("#txtFechaDesde").val()
            + "',fechaHasta: '" + $("#txtFechaHasta").val()

            + "'}";

        $.ajax({
            type: "POST",
            url: "rptSorteos.aspx/exportar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                    $("#btnExportar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#lnkDownload").hide();
                $("#btnExportar").attr("disabled", false);
            }
        });
    }
    else {
        return false;
        $("#imgLoading").hide();
        $("#lnkDownload").hide();
        $("#btnExportar").attr("disabled", false);
    }
}

function verTodos() {

    $("#ddlSorteos,#txtTarjeta,#ddlSorteos,#txtSocio,#txtFechaDesde,#txtFechaHasta").val("");
    filter();
}

function configControls() {

    $("#ddlSorteos,#txtTarjeta,#ddlSorteos,#txtSocio,#txtFechaDesde,#txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Fecha: { type: "date" },
                        Sorteo: { type: "string" },
                        Tarjeta: { type: "string" },
                        Socio: { type: "string" },
                        DNI: { type: "string" },
                        Email: { type: "string" },
                        Celular: { type: "string" },
                        Latitud: { type: "string" },
                        Longitud: { type: "string" },
                        Domicilio: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "rptSorteos.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Mapa", width: "30px" },
            { field: "Fecha", title: "Fecha", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "Sorteo", title: "Sorteo", width: "100px" },
            { field: "Socio", title: "Socio", width: "100px" },
            { field: "DNI", title: "DNI", width: "100px" },
            { field: "Email", title: "Email", width: "100px" },
            { field: "Celular", title: "Celular", width: "100px" },
            { field: "Tarjeta", title: "Tarjeta", width: "100px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

       showMap(dataItem.Longitud,dataItem.Latitud,dataItem.Domicilio);
    });

    $('.form').validate({
        onkeyup: true,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formReporte').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();


        var sorteo = $("#ddlSorteos").val();
        if (sorteo != "") {
            $filter.push({ field: "IDSorteo", operator: "equal", value: parseInt(sorteo) });
        }
        var Tarjeta = $("#txtTarjeta").val();
        if (Tarjeta != "") {
            $filter.push({ field: "Tarjeta", operator: "contains", value: Tarjeta });
        }
        var socio = $("#txtSocio").val();
        if (socio != "") {
            $filter.push({ field: "Socio", operator: "contains", value: socio.toUpperCase() });
        }

        grid.dataSource.filter($filter);
    }
}

$(document).ready(function () {
    configControls();
});


function computepos(point) {
    $("#txtLatitud").val(point.lat().toFixed(6));
    $("#txtLongitud").val(point.lng().toFixed(6));
}

function showMap(latitud,longitud,domicilio) {
    var embAddr = domicilio;//$("#txtDomicilio").val() + ',' + $("#ddlCiudad option:selected").text() + ',' + $("#ddlProvincia option:selected").text();
    $("#modalTitle").html(embAddr);

    if (longitud == "" && latitud == "") {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': embAddr }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();

                drawMap(latitude, longitude);
            }
            else {
                $("#map-canvas").html('Could not find this location from the address given.<p>' + embAddr + '</p>');
            }
        });
    }
    else {
        drawMap(longitud, latitud);
    }



    function drawMap(latitude, longitude) {
        var drag = false;
        var infowindow;

        var point = new google.maps.LatLng(latitude, longitude);

        var mapOptions = {
            zoom: 14,
            center: point,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: point
        })
        marker.setMap(map);
        map.setCenter(point);

        google.maps.event.addListener(map, 'click', function (event) {
            if (drag) { return; }
            if (map.getZoom() < 10) { map.setZoom(10); }
            map.panTo(event.latLng);
            computepos(event.latLng);
        });

        google.maps.event.addListener(marker, 'click', function () {
            var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "</p></div>";

            infowindow = new google.maps.InfoWindow({ content: html });
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(marker, 'dragstart', function () { if (infowindow) { infowindow.close(); } });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            //if (map.getZoom() < 10) { map.setZoom(10); }
            map.setCenter(event.latLng);
            computepos(event.latLng);
            drag = true;
            setTimeout(function () { drag = false; }, 250);
        });

        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(point);
        });

        $("#myMapModal").modal("show");
    }


};