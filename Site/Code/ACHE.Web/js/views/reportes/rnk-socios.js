﻿function buscar() {
    $("#imgLoading").show();
    $("#divError").hide();
    $("#lnkDownload").hide();
    $('#formReporte').validate();
    $("#divGrid").html("");

    var info = "{marca: " + parseInt($("#ddlMarcas").val())
        + ", franquicia: " + parseInt($("#ddlFranquicias").val())
        //+ ", fechaDesde: '" + $("#txtFechaDesde").val()
        //+ "', fechaHasta: '" + $("#txtFechaHasta").val()
        + "}";

    //alert(info);

    $.ajax({
        type: "POST",
        url: "rnk-socios.aspx/buscar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divGrid").html(data.d);
                $("#grid").kendoGrid({
                    height: 500,
                    scrollable: true,
                    sortable: true,
                });
                $("#imgLoading").hide();
                //window.location.href = window.location.href.split("/ReporteComercios.aspx").join(data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
        }
    });
}

function exportar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#lnkDownload").hide();

    $("#btnExportar").attr("disabled", true);

    //var info = "{estado: '" + $("#ddlEstado").val()
    //   + "', sds: '" + $("#txtSDS").val()
    //   + "', nombre: '" + $("#txtNumeroEst").val()
    //   + "'}";

    var info = "{marca: " + parseInt($("#ddlMarcas").val())
       + ", franquicia: " + parseInt($("#ddlFranquicias").val())
       //+ ", fechaDesde: '" + $("#txtFechaDesde").val()
       //+ "', fechaHasta: '" + $("#txtFechaHasta").val()
       + "}";

    $.ajax({
        type: "POST",
        url: "rnk-socios.aspx/exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();

                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });

}

function filter() {
    $("#divError").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var franquicias = $("#ddlFranquicias").val();
    if (franquicias != "0") {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicias) });
    }

    var marcas = $("#ddlMarcas").val();
    if (marcas != "0") {
        $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marcas) });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {

    /*$("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });*/

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        Franquicia: { type: "string" },
                        Marca: { type: "string" },
                        Socio: { type: "string" },
                        Tarjeta: { type: "string" },
                        NroDocumento: { type: "string" },
                        CantTr: { type: "integer" },
                        Importe: { type: "number" },
                        ImporteAhorro: { type: "number" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "rnk-socios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);//, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Franquicia", title: "Franquicia", width: "120px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "Tarjeta", title: "Nro. de Tarjeta", width: "120px" },
            { field: "Socio", title: "Socio", width: "170px" },
            { field: "NroDocumento", title: "Documento", width: "90px" },
            { field: "CantTr", title: "Cant", width: "80px" },
            { field: "Importe", title: "Importe", width: "100px", format: "{0:c}" },
            { field: "ImporteAhorro", title: "Ahorro", width: "100px", format: "{0:c}" },
        ]
    });

    //configDatePicker();
    //configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
});