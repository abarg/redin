
$(document).ready(function () {
    $("#txtDesdeTarjeta, #txtHastaTarjeta").numeric();
    setTimeout("gebo_charts.fl_tarjetas_impresas()", 2000);
    $("#txtDesdeTarjeta, #txtHastaTarjeta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            ObtenerTarjetasImpresas2();
            return false;
        }
    });

});

//* charts
gebo_charts = {

    fl_tarjetas_impresas: function () {
        ObtenerTarjetasImpresas2();
    }

};

var oTable, oTable2 = null;
/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/


function ObtenerTarjetasImpresas2() {

    var desde = 0;
    var hasta = 0;
    if ($("#txtDesdeTarjeta").val() != "")
        desde = $("#txtDesdeTarjeta").val();
    if ($("#txtHastaTarjeta").val() != "")
        hasta = $("#txtHastaTarjeta").val();
    $.ajax({
        type: "POST",
        url: "rptTarjetasImpresas.aspx/ObtenerTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        data: "{ desde: " + desde + ", hasta: " + hasta + "}",
        dataType: "json",
        success: function (data) {
            if (data != null && data.d.length > 0) {
                $("#bodyImpresas").html("");

                if (oTable2 != null) {
                    oTable2.fnClearTable();
                    oTable2.fnDestroy();
                }

                oTable2 = $('#tablaImpresas').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "paging": true,
                    "bLengthChange": false,
                    "iDisplayLength": 20,
                    "ordering": false,
                    "bSort": false,
                    "info": false,
                    //"bDestroy": true,
                    "searching": false,
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ning�n dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "�ltimo",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "fnDrawCallback": function () {
                        var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                        if (pageCount == 1) {
                            $('.dataTables_paginate').first().hide();
                        } else {
                            $('.dataTables_paginate').first().show();
                        }
                    }
                });

                for (var i = 0; i < data.d.length; i++) {
                    oTable2.fnAddData([
                        data.d[i].Marca,
                        data.d[i].Activas,
                        data.d[i].Inactivas,
                        data.d[i].Total,
                    data.d[i].Desde,
                    data.d[i].Hasta
                    ]
                      );
                }

                //$("#tableDetalle_info").parent().remove();
                $("#tablaImpresas").css("width", "100%");

                $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                $(".dataTables_paginate").first().parent().addClass("col-sm-12");
            }
            else {
                $("#bodyImpresas").html("<tr><td colspan='4'>No hay un detalle disponible</td></tr>");
            }
        }
    });
}

function ExportarTarjetasImpresas() {
    var desde = 0;
    var hasta = 0;
    if ($("#txtDesdeTarjeta").val() != "")
        desde = $("#txtDesdeTarjeta").val();
    if ($("#txtHastaTarjeta").val() != "")
        hasta = $("#txtHastaTarjeta").val();
    $("#lnkDownloadEmitidas").hide();
    $("#imgLoadingEmitidas").show();
    $("#btnExportarEmitidas").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "rptTarjetasImpresas.aspx/ExportarTarjetasEmitidas",
        contentType: "application/json; charset=utf-8",
        data: "{ desde: " + desde + ", hasta: " + hasta + "}",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#imgLoadingEmitidas").hide();
                $("#lnkDownloadEmitidas").show();
                $("#lnkDownloadEmitidas").attr("href", data.d);
                $("#lnkDownloadEmitidas").attr("download", data.d);
                $("#btnExportarEmitidas").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingEmitidas").hide();
            $("#lnkDownloadEmitidas").hide();
            $("#btnExportarEmitidas").attr("disabled", false);
        }
    });
}
