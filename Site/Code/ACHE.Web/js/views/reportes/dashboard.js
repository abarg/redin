var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);

$(document).ready(function () {
    setTimeout("ObtenerPromedioTicketMensual()", 1000);
    setTimeout("ObtenerTotalTasaUsoMensual()", 1000);
    setTimeout("ObtenerTotalTRMensual()", 1000);
    setTimeout("ObtenerTotalSociosSexo()", 1000);
    setTimeout("ObtenerTotalArancelMensual()", 1000);
    setTimeout("ObtenerTotalPuntosMensual()", 1000);
    setTimeout("ObtenerTotalEmails()", 1000);
    setTimeout("ObtenerTotalCelulares()", 1000);
    setTimeout("ObtenerTotalTarjetasActivas()", 1000);
    setTimeout("ObtenerTotalFacturado()", 1000);

    /*setTimeout("gebo_charts.fl_puntos_otorgados()", 2000);
    setTimeout("gebo_charts.fl_facturacion_ahorro()", 2000);
    setTimeout("gebo_charts.fl_facturacion_ahorro_detalle()", 2000);
    setTimeout("gebo_charts.fl_arancel_puntos_fac_detalle()", 2000);*/
    setTimeout("gebo_charts.fl_terminales_activas()", 2000);
    /*setTimeout("gebo_charts.fl_tarjetas_activas()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_marca()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_asignadas()", 2000);
    setTimeout("gebo_charts.fl_tarjetas_impresas()", 2000);*/
    setTimeout("gebo_charts.fl_estado_terminales()", 2000);


    setTimeout("ObtenerTop10SociosMensual()", 1500);
    setTimeout("ObtenerTop10ComerciosMensual()", 1500);
    setTimeout("ObtenerTop10Socios()", 1500);
    setTimeout("ObtenerTop10Comercios()", 1500);

});

//* charts
gebo_charts = {
    fl_terminales_activas: function () {
        // Setup the placeholder reference
        var elem = $('#fl_terminales_activas');

        var data = ObtenerTerminalesActivas();
        //var colors = [ "#eadac8", "#dcc1a3", "#cea97e", "#c09059", "#a8763f", "#835c31", "#5e4223", "#392815" ];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    }/*,
                    colors: colors*/
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_puntos_otorgados: function () {
        var elem = $('#fl_puntos_otorgados');

        var data = ObtenerPuntosOtorgados();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    fl_facturacion_ahorro: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro');


        //So we map "app4", "app5" and etc with the integer we selected according to the above mapping.(instead of "app4" we put 1, for "app5" we put 2 and so on ) Hence the data that we will be passing to flot will take the following format.
        /* var build_pass_data = [
                         [1, 4],
                         [2, 1],
                         [3, 1],
                         [4, 4],
                         [5, 3]
                     ];
         var build_fail_data = [
                         [1, 2],
                         [2, 3],
                         [3, 3],
                         [4, 3],
                         [5, 6]
                     ];
         
         var data = [
                 { label: "PASS", data: build_pass_data, bars: { fillColor: "#336600" }, color: "#336600" },
                 { label: "FAIL", data: build_fail_data, bars: { fillColor: "#E41B17" }, color: "#E41B17" }
         ];*/

        var pagado = ObtenerImportePagado();
        var ahorro = ObtenerImporteAhorro();
        var original = ObtenerImporteOriginal();

        var data = [
            { label: "Pagado", data: pagado },
            { label: "Ahorro", data: ahorro },
            { label: "Original", data: original }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_facturacion_ahorro_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_facturacion_ahorro_detalle');
        var pagado = ObtenerImportePagadoDiario();
        var ahorro = ObtenerImporteAhorroDiario();
        var original = ObtenerImporteOriginalDiario();

        for (var i = 0; i < pagado.length; ++i) { pagado[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < ahorro.length; ++i) { ahorro[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < original.length; ++i) { original[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Pagado", data: pagado },
                { label: "Ahorro", data: ahorro },
                { label: "Original", data: original }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    /*var originalPoint;

                    if (item.datapoint[0] == item.series.data[0][3]) {
                        originalPoint = item.series.data[0][0];
                    } else if (item.datapoint[0] == item.series.data[1][3]) {
                        originalPoint = item.series.data[1][0];
                    } else if (item.datapoint[0] == item.series.data[2][3]) {
                        originalPoint = item.series.data[2][0];
                    } else if (item.datapoint[0] == item.series.data[3][3]) {
                        originalPoint = item.series.data[3][0];
                    }

                    //var x = getMonthName(originalPoint);*/
                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = $" + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    fl_arancel_puntos_fac_detalle: function () {

        // Setup the placeholder reference
        var elem = $('#fl_arancel_puntos_fac_detalle');
        var arancel = ObtenerArancelDiario();
        var puntos = ObtenerPuntosDiario();
        var facturado = ObtenerFacturadoDiario();

        for (var i = 0; i < arancel.length; ++i) { arancel[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < puntos.length; ++i) { puntos[i][0] += 60 * 120 * 1000 };
        for (var i = 0; i < facturado.length; ++i) { facturado[i][0] += 60 * 120 * 1000 };

        $.plot(elem,
            [
                { label: "Arancel", data: arancel },
                { label: "Puntos", data: puntos },
                { label: "Facturado", data: facturado }
            ],
            {
                lines: {
                    show: true
                },
                points: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    //timeformat: "%d/%m/%Y",
                    minTickSize: [1, "day"],
                    //autoscaleMargin: 0.10,
                    tickLength: 10
                },
                series: {
                    curvedLines: { active: true }
                },
                grid: {
                    backgroundColor: { colors: ["#fff", "#eee"] },
                    hoverable: true,
                    borderWidth: 1
                },

            }
        );
        //Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = $" + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    fl_tarjetas_activas: function () {

        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_activas');

        var activas = ObtenerTarjetasActivas();
        var inactivas = ObtenerTarjetasInactivas();

        var data = [
            { label: "Activas", data: activas },
            { label: "Inactivas", data: inactivas }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 5,
                mode: null,
                ticks: [
                    [1, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0,
                //axisLabel: "App",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }/*, yaxis: {
                //axisLabel: "No of builds",
                tickDecimals: 0,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            }*/, grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }, legend: {
                labelBoxBorderColor: "none",
                position: "top"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.2,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_marca: function () {
        // Setup the placeholder reference
        var elem = $('#fl_tarjetas_marca');

        var data = ObtenerTarjetasMarca();
        //var colors = [ "#eadac8", "#dcc1a3", "#cea97e", "#c09059", "#a8763f", "#835c31", "#5e4223", "#392815" ];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    legend: {
                        noColumns: 3,
                        labelBoxBorderColor: "#858585",
                        position: "sw",
                        //margin: [0, 100],
                        container: $("#fl_tarjetas_marca_legend")

                    },
                    grid: {
                        hoverable: true,
                        clickable: true
                    }/*,
                    colors: colors*/
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });

        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    },

    fl_tarjetas_asignadas: function () {
        var elem = $('#fl_tarjetas_asignadas');

        var data = ObtenerTarjetasAsignadas();
        //var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];

        $.plot(elem, [data], {
            series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.6,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0, // hide gridlines
                //axisLabel: 'Mes',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            }
        });

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_tarjetas_impresas: function () {
        var elem = $('#fl_tarjetas_impresas');

        //******* Precious Metal Price - HORIZONTAL BAR CHART
        var rawDataImpresas = ObtenerTarjetasImpresas();
        var rawDataActivas = ObtenerTarjetasImpresasActivas();
        var dataSet = [{ label: "Marcas", data: rawDataImpresas }, { data: rawDataActivas }];
        var ticks = ObtenerTarjetasImpresasLabels();

        var options = {
            series: {
                shadowSize: 1,
                stack: true,
                bars: {
                    show: true,
                    fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] }
                }
            },
            bars: {
                align: "center",
                barWidth: 0.5,
                //barWidth:24 * 60 * 60 * 600,
                horizontal: true,
                //fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                lineWidth: 1
            },
            xaxis: {
                axisLabel: "Cantidad",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5,
                tickFormatter: function (v, axis) {
                    return v;//$.formatNumber(v, { format: "#,###", locale: "us" });
                }
            },
            yaxis: {
                axisLabel: "Marcas",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                ticks: ticks,
                tickLength: 0
            },
            grid: {
                backgroundColor: { colors: ["#fff", "#eee"] },
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                show: false
            }
        };

        $.plot(elem, dataSet, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[0];
                    z = item.series.color;

                    showTooltip(item.pageX, item.pageY,
                    "<b>Cantidad</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });
    },

    fl_estado_terminales: function () {
        // Setup the placeholder reference
        var elem = $('#fl_estado_terminales');

        var data = ObtenerEstadoTerminales();
        var colors = ["#000", "#F5AA1A", "#70A415", "#f00", "#f8f412", "#0066FF", "#ccc", "#a47e3c"];

        // Setup the flot chart using our data
        function a_plotWithColors() {
            fl_a_plot = $.plot(elem, data,
                {
                    //label: "Visitors by Location",
                    series: {
                        pie: {
                            show: true,
                            //tilt: 0.5,
                            innerRadius: 0.5,
                            highlight: {
                                opacity: 0.2
                            }
                        }
                    },
                    /*combine: {
                        color: '#999',
                        threshold: 0.1
                    },*/
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    colors: colors
                }
            );
        }

        a_plotWithColors();

        // Create a tooltip on our chart
        elem.qtip({
            prerender: true,
            content: 'Loading...', // Use a loading message primarily
            position: {
                viewport: $(window), // Keep it visible within the window if possible
                target: 'mouse', // Position it in relation to the mouse
                adjust: { x: 7 } // ...but adjust it a bit so it doesn't overlap it.
            },
            show: false, // We'll show it programatically, so no show event is needed
            style: {
                classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                tip: false // Remove the default tip.
            }
        });


        // Bind the plot hover
        elem.on('plothover', function (event, coords, item) {
            // Grab the API reference
            var self = $(this),
                api = $(this).qtip(),
                previousPoint, content,

            // Setup a visually pleasing rounding function
            round = function (x) { return Math.round(x * 1000) / 1000; };

            // If we weren't passed the item object, hide the tooltip and remove cached point data
            if (!item) {
                api.cache.point = false;
                return api.hide(event);
            }

            // Proceed only if the data point has changed
            previousPoint = api.cache.point;
            if (previousPoint !== item.seriesIndex) {
                percent = parseFloat(item.series.percent).toFixed(2);
                // Update the cached point data
                api.cache.point = item.seriesIndex;

                // Setup new content
                content = item.series.label + ' ' + percent + '%';

                // Update the tooltip content
                api.set('content.text', content);

                // Make sure we don't get problems with animations
                api.elements.tooltip.stop(1, 1);

                // Show the tooltip, passing the coordinates
                api.show(coords);
            }
        });
    }

};

function getMonthName(newTimestamp) {
    var d = new Date(newTimestamp);

    var numericMonth = d.getMonth();
    var monthArray = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    var alphaMonth = monthArray[numericMonth];

    return alphaMonth;
}

function showTooltip(x, y, contents, z) {
    $('<div id="flot-tooltip">' + contents + '</div>').css({
        top: y - 20,
        left: x - 90,
        'border-color': z,
    }).appendTo("body").show();
}

/*METODOS DE OBTENCION DE DATOS DE LOS GRAFICOS*/

function ObtenerTerminalesActivas() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTerminalesPorPOS",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblTerminalesPorPOS").html("Terminales por pos (" + totalTerminales + ")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasEmitidasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].data, i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasImpresasLabels() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasEmitidas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([i, data[i].label]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerPuntosOtorgados() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPuntosOtorgados",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasMarca() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasPorMarca",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push(data[i]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

function ObtenerTarjetasAsignadas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasAsignadas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerEstadoTerminales() {
    var ddata = [];
    var totalTerminales = 0;

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerEstadoTerminales",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                totalTerminales += parseInt(data[i].data);
                ddata.push(data[i]);
            }

            $("#lblEstadoTerminales").html("Estado de las terminales ("+totalTerminales+")");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    return ddata;
}

/*Tarjetas activas & inactivas, 4 meses*/
function ObtenerTarjetasActivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasActivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerTarjetasInactivas() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTarjetasInactivas",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Facturaci�n & Ahorro, 4 meses*/
function ObtenerImporteAhorro() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteAhorro",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagado() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImportePagado",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginal() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteOriginal",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                ddata.push([data[i].label, data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Facturaci�n & Ahorro, 30 d�as*/
function ObtenerImporteAhorroDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteAhorroDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImporteOriginalDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImporteOriginalDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {;
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
            /*ddata.push([Date.UTC(2014, 0, 21, 08, 0, 0), 1180]);
            ddata.push([Date.UTC(2014, 0, 22, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 23, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 24, 08, 0, 0), 1212]);
            ddata.push([Date.UTC(2014, 0, 25, 08, 0, 0), 9922]);
            ddata.push([Date.UTC(2014, 0, 26, 08, 0, 0), 1888]);*/

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerImportePagadoDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerImportePagadoDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

/*Arancel, Puntos & Total Facturado*/
function ObtenerArancelDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerArancelDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerPuntosDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPuntosDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {;
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
            /*ddata.push([Date.UTC(2014, 0, 21, 08, 0, 0), 1180]);
            ddata.push([Date.UTC(2014, 0, 22, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 23, 08, 0, 0), 0]);
            ddata.push([Date.UTC(2014, 0, 24, 08, 0, 0), 1212]);
            ddata.push([Date.UTC(2014, 0, 25, 08, 0, 0), 9922]);
            ddata.push([Date.UTC(2014, 0, 26, 08, 0, 0), 1888]);*/

        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}

function ObtenerFacturadoDiario() {
    var ddata = [];

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerFacturadoDiario",
        //data: info,
        async: false,//wait for result
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, text) {
            var data = msg.d;
            for (i = 0; i < data.length; i++) {
                var fecha = data[i].label.split(",");
                //alert(Date.UTC(fecha[0],fecha[1],fecha[2]));
                ddata.push([Date.UTC(fecha[0], fecha[1], fecha[2]), data[i].data]);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    return ddata;
}


function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

/*METODOS DE OBTENCION DE DATOS DE LOS ICONOS*/

function ObtenerTop10Socios() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Socios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySocios").html(data.d);
            }
        }
    });
}

function ObtenerTop10Comercios() {
    //$("#bodyComercios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10Comercios",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComercios").html(data.d);
            }
        }
    });
}

function ObtenerTop10SociosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10SociosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySociosMes").html(data.d);
            }
        }
    });
}


function ObtenerTop10ComerciosMensual() {
    //$("#bodySocios").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTop10ComerciosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyComerciosMes").html(data.d);
            }
        }
    });
}
function ObtenerTotalSociosSexo() {
    //$("#resultado_sexo").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalSociosPorSexo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var aux = data.d.split(",");
                $("#resultado_sexo").html(aux[0]);
                $("#totalSexo").html(aux[1]);
            }
        }
    });
}

function ObtenerTotalPuntosMensual() {
    //$("#resultado_puntos_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalPuntosMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_puntos_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalArancelMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalArancelMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_arancel_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalTRMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTRMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tr_mensual").html(data.d);
            }
        }
    });
}

function ObtenerTotalTasaUsoMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTasaUsoMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_tasauso_mensual").html(data.d);
            }
        }
    });
}

function ObtenerPromedioTicketMensual() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerPromedioTicketMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_ticketpromedio_mensual").html("$ " + data.d);
            }
        }
    });
}

function ObtenerTotalEmails() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalEmails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_mails").html(data.d);
            }
        }
    });
}

function ObtenerTotalCelulares() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalCelulares",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_cel").html(data.d);
            }
        }
    });
}

function ObtenerTotalTarjetasActivas() {
    //$("#resultado_arancel_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalTarjetasActivas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_total_tarjetas_activas").html(data.d);
            }
        }
    });
}

function ObtenerTotalFacturado() {
    //$("#resultado_facturacion_mensual").html();

    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerTotalFacturadoMensual",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#resultado_facturacion_mensual").html(data.d);
            }
        }
    });
}

/*** METODOS AUXILIARES **/
function verDetalleTerminales(estado)
{
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Terminales");
    
    $.ajax({
        type: "POST",
        url: "dashboard.aspx/obtenerDetalleTerminales",
        data: "{estado: '" + estado + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }

            $('#modalDetalle').modal('show');
        }
    });
}


function mostrarSociosGeneral() {
    $("#tblSociosMensual").hide();
    $("#tblSocios").show();
}
function mostrarComerciosGeneral() {
    $("#tblComerciosMensual").hide();
    $("#tblComercios").show();
}

function mostrarComerciosMensual() {
    $("#tblComerciosMensual").show();
    $("#tblComercios").hide();
}
function mostrarSociosMensual() {
    $("#tblSociosMensual").show();
    $("#tblSocios").hide();
}