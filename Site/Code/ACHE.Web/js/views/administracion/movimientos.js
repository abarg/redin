﻿function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    LoadComercios("../common.aspx/LoadCasasMatrices", "ddlComercio");

    $("#txtFechaHasta, #txtFechaDesde, #txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });

}

function nuevo() {
    window.location.href = "movimientose.aspx";
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        IDEntidad: { type: "integer" },
                        Nombre: { type: "string" },
                        Fecha: { type: "date" },
                        Importe: { type: "decimal" },
                        Concepto: { type: "string" },
                        CUIT: { type: "string" },
                        Facturada: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "movimientos.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "Nombre", title: "Nombre", width: "200px" },
            { field: "Fecha", title: "Fecha", width: "70px", format: "{0:dd/MM/yyyy}" },
            { field: "Importe", title: "Importe", width: "100px", format: "{0:c}" },
            { field: "Facturada", title: "Facturada", width: "100px", attributes: { class: "colCenter" } },
            { field: "Concepto", title: "Concepto", width: "200px" }

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "movimientose.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "movimientos.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function filtrar() {
    configGrid();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var comercio = $("#ddlComercio").val();
    if (comercio != "") {
        $filter.push({ field: "IDEntidad", operator: "equals", value: parseInt(comercio) });
    }

    var cuit = $("#txtDocumento").val();
    if (cuit != "") {
        $filter.push({ field: "CUIT", operator: "contains", value: cuit });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});