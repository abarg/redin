﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "facturas-prov.aspx/Exportar",
        data: "{ fechaDesde: '" + $("#txtFechaDesde").val() + "', fechaHasta: '" + $("#txtFechaHasta").val() + "', proveedor: '" + $("#ddlProveedor").val() + "', cuit: '" + $("#txtDocumento").val() + "', fc: '" + $("#txtNumero").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function confirmarCAE()
{
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#divError").hide();
    $('#modalObtenerCAE').modal('show');
}

function verFcErrores() {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Errores");
    $("#headDetalle").html("<tr><th>Proveedor</th><th>Período</th><th>Fecha</th><th>Error</th></tr>");
    $.ajax({
        type: "POST",
        url: "facturas-prov.aspx/obtenerErrores",
        //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}

function generarCAE() {
    if ($("#txtImporteCAE").val() != "") {

        $("#modalObtenerCAEbody").removeClass("f_error");
        $('#modalObtenerCAE').modal('hide');
        $("#btnFE").attr("disabled", true);
        $("#divError").hide();
        $("#imgLoading").show();
        $("#divOK").hide();

        $.ajax({
            type: "POST",
            url: "facturas-prov.aspx/ObtenerCAE",
            data: "{importe: " + parseInt($("#txtImporteCAE").val()) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    filtrar();
                    if (data.d.indexOf("Se han generado") != -1) {
                        $("#divError").html(data.d);
                        $("#divError").show();
                    }
                    else {

                        $("#divOK").html(data.d);
                        $("#divOK").show();
                    }
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoading").hide();
                    $("#btnFE").attr("disabled", false);
                    //window.location.href = window.location.href.split("/bankInformation.aspx").join(data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#btnFE").attr("disabled", false);
                //$("#lnkDownload").hide();
            }
        });
    }
    else {
        $("#modalObtenerCAEbody").addClass("f_error");
        $("#divErrorCAE").show();
    }
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    LoadProveedores("../common.aspx/LoadProveedores", "ddlProveedor");

    $("#txtImporteCAE, #txtDocumento").numeric();

    $("#txtFechaHasta, #txtFechaDesde, #txtNumero, #txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Nombre: { type: "string" },
                        Numero: { type: "string" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        Subtotal: { type: "decimal" },
                        Iva: { type: "decimal" },
                        Total: { type: "decimal" },
                        CAE: { type: "string" },
                        CUIT: { type: "string" },
                        Enviada: { type: "string" },
                        Descargada: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "facturas-prov.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "Nombre", title: "Proveedor", width: "200px" },
            { field: "FechaDesde", title: "Fecha Desde", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaHasta", title: "Fecha Hasta", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "Numero", title: "Numero", width: "100px" },
            { field: "Subtotal", title: "Subtotal", width: "100px", format: "{0:c}" },
            { field: "Iva", title: "Iva", width: "100px", format: "{0:c}" },
            { field: "Total", title: "Total", width: "100px", format: "{0:c}" },
            { field: "CAE", title: "CAE", width: "100px" },
            { field: "Enviada", title: "Enviada", width: "50px", attributes: { class: "colCenter" } },
            { field: "Descargada", title: "Descargada", width: "50px", attributes: { class: "colCenter" } }
            /*,
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/></div>" }, title: "Ver", width: "50px" },
            { title: "Eliminar", template: "#= renderDelete(data) #", width: "50px" },*/
        ]
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#bodyDetalle").html();

        if (dataItem.CAE == null || dataItem.CAE == "")
            $("#titDetalle").html("Factura NO informado a la AFIP.");
        else
            $("#titDetalle").html("Factura Número " + dataItem.Numero);
        $("#headDetalle").html("<tr><th>Concepto</th><th>Precio</th><th>IVA</th></tr>");
        
        $.ajax({
            type: "POST",
            url: "facturas-prov.aspx/obtenerDetalle",
            data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalDetalle').modal('show');
            }
        });
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "facturas-prov.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    /*$("#grid").delegate(".pdfColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (dataItem.Numero != "") {
            window.open("/facturas/" + dataItem.Numero + ".pdf");
        }
        else
            alert("La factura seleccionada no tiene un CAE generado");
    });*/
}

function renderOptions(data) {
    var html = "";
    if (data.Numero == null || data.Numero == "")
        html = "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/>&nbsp;<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    else
        html = "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/>&nbsp;<img src='../../img/grid/gridPdf.gif' style='cursor:pointer' onclick=\"descargarPdf('" + data.Numero + "');\" title='Descargar Fc' class='viewPdf'/></div>";

    return html;
}

/*
function renderDelete(data)
{
    if (data.Numero == null || data.Numero == "")
        return "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    else
        return "";
}

function renderPdfIcon(data) {  
    if (data.Numero == null || data.Numero == "")
        return "";
    else
        return "<div align='center'><img src='../../img/grid/gridPdf.gif' style='cursor:pointer' onclick=\"descargarPdf('" + data.Numero + "');\" title='Descargar Fc' class='viewPdf'/></div>";
}*/

function descargarPdf(numero)
{
    window.open("/files/facturas/" + $.trim(numero) + ".pdf");
}

function nuevo() {
    window.location.href = "facturase.aspx?Modo=P";
}

function filtrar() {
    $("#divError").hide();
    $("#divOK").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var prov = $("#ddlProveedor").val();
    if (prov != "") {
        $filter.push({ field: "IDEntidad", operator: "equals", value: parseInt(prov) });
    }

    var numero = $("#txtNumero").val();
    if (numero != "") {
        $filter.push({ field: "Numero", operator: "contains", value: numero });
    }

    var cuit = $("#txtDocumento").val();
    if (cuit != "") {
        $filter.push({ field: "CUIT", operator: "contains", value: cuit });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});