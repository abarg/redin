﻿$(document).ready(function () {
    GenerarTabla();
});

function GenerarTabla() {
    $.ajax({
        type: "POST",
        url: "seccion-manual-ayuda.aspx/generarTabla",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodySecciones").html(data.d);
            }
        }
    });
}


function eliminar(id) {

    $.ajax({
        type: "POST",
        url: "seccion-manual-ayuda.aspx/eliminar",
        data: "{id: "+id+"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#divOk').show();
            $("#divError").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            GenerarTabla();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

