﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var id = 0;
    if ($("#hfIDSeccion").val() != "")
        id = $("#hfIDSeccion").val();

    if ($('#formEdicion').valid()) {
        var info = "{ idSeccion: " + id
            + ", titulo: '" + $("#txtTitulo").val()
            + "', orden: '" + $("#txtOrden").val()
            + "', perfil: '" + $("#ddlPerfil").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "seccion-manual-ayudae.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "seccion-manual-ayuda.aspx";

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function nuevo() {
    window.location.href = "articulo-manual-ayudae.aspx?IDSeccion="+$("#hfIDSeccion").val();

}
$(document).ready(function () {
    toggleTabs();
    $('#txtOrden').numeric();
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
});
function toggleTabs() {
    if ($("#hfIDSeccion").val() != "0") {
        $($("#Tabs ul li a")[1]).removeClass("hide");
        GenerarTabla();
    }
    else {
        $($("#Tabs ul li a")[1]).addClass("hide");
    }
}


function GenerarTabla() {
    $.ajax({
        type: "POST",
        url: "seccion-manual-ayudae.aspx/generarTabla",
        data: "{id: " + $("#hfIDSeccion").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyArticulos").html(data.d);
            }
        }
    });
}

function eliminar(id) {

    $.ajax({
        type: "POST",
        url: "seccion-manual-ayudae.aspx/eliminar",
        data: "{id: " + id + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#divOk').show();
            $("#divError").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            GenerarTabla();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}