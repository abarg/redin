﻿function guardar() {
    $("#divError").hide();
    $("#divOK").hide();
    $("#imgLoading").show();
    $("#btnGrabar").attr("disabled", true);

    if ($("#txtFechaDesde").val() != "" && $("#txtFechaHasta").val() != "") {

        if ($("#searchable").val() != "") {
            $('#formEdicion').validate();



            if ($('#formEdicion').valid()) {
                var info = "{ ids: '" + $("#searchable").val()
                    + "', fechaDesde: '" + $("#txtFechaDesde").val()
                    + "', fechaHasta: '" + $("#txtFechaHasta").val()
                    + "', concepto: '" + $("#ddlConcepto").val() + " " + $("#txtConcepto").val()
                    + "', modo: '" + $("#hdnModo").val()
                    + "'}";

                $.ajax({
                    type: "POST",
                    url: "facturase.aspx/guardar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        $('#divOK').show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        $("#btnGrabar").attr("disabled", false);
                        $("#imgLoading").hide();

                        if ($("#hdnModo").val() == "R")
                            window.location.href = "facturas.aspx";
                        else if ($("#hdnModo").val() == "P")
                            window.location.href = "facturas-prov.aspx";
                        else
                            window.location.href = "facturas-manual.aspx";
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        $("#btnGrabar").attr("disabled", false);
                        $("#imgLoading").hide();
                    }
                });
            }
            else {
                $("#btnGrabar").attr("disabled", false);
                $("#imgLoading").hide();
                return false;
            }
        }
        else {
            $("#btnGrabar").attr("disabled", false);
            if ($("#hdnModo").val() == "P")
                $("#divError").html("Por favor, seleccione una o más proveedores.");
            else
                $("#divError").html("Por favor, seleccione una o más empresas.");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
        }
    }
    else {
        $("#divError").html("Las fechas son obligatorias.");
        $("#divError").show();
        $("#imgLoading").hide();
        $("#btnGrabar").attr("disabled", false);
    }
    
}

$(document).ready(function () {
    if ($("#hdnModo").val() == "R") {
        $("#litTitulo").html("Generación de facturas recurrentes");
        $("#lnkCancelar").attr("href", "facturas.aspx");
        $("#lblTipo").html('<span class="f_req">*</span> Comercio');
    }
    else if ($("#hdnModo").val() == "P") {
        $("#litTitulo").html("Generación de facturas de proveedores");
        $("#lnkCancelar").attr("href", "facturas-prov.aspx");
        $("#lblTipo").html('<span class="f_req">*</span> Proveedor');
    }
    else {
        $("#litTitulo").html("Generación de facturas manuales");
        $("#lnkCancelar").attr("href", "facturas-manual.aspx");
        $("#lblTipo").html('<span class="f_req">*</span> Comercio');
    }

    configControls();


});


function configControls() {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    //$(".chzn_b").chosen({ allow_single_deselect: true });

    //$("#ddlEmpresas").keypress(function (event) {
    //    var keycode = (event.keyCode ? event.keyCode : event.which);
    //    if (keycode == '13') {
    //        url: "facturase.aspx/CargarFacturas",
    //        return false;
    //    }
    //});

    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más opciones"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
}




