﻿
$(document).ready(function () {
    configControls();
    $("#divErrorModal").hide();
});


function configControls() {

    $("#txtFactDesde, #txtFactHasta").numeric();

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        FacturaDesde: { type: "integer" },
                        FacturaHasta: { type: "integer" },
                        Imagen: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "facturas-PiePagina.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "FacturaDesde", title: "Factura Desde", width: "80px"},
            { field: "FacturaHasta", title: "Factura Hasta", width: "80px"},
            { field: "Imagen", title: "Imagen", width: "100px" },
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" }
        ]
    });
    $("#grid").delegate(".deleteColumn", "click", function (e) {
        $("#divOk").hide();
        $("#divError").hide();
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "facturas-PiePagina.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        $("#divOk").hide();
        $("#divError").hide();
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (dataItem != null)
        $('#modalGenerarPiePag').modal('show');
        $.ajax({
            type: "POST",
            url: "facturas-PiePagina.aspx/cargarFacturasPiePagina",
            data: "{id: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    //if (validForm(data.d)) {
                        $("#hdnIDFacturaPiePagina").val(data.d.IDFacturaPiePagina)
                        $("#txtFactDesde").val(data.d.FacturaDesde);
                        $("#txtFactHasta").val(data.d.FacturaHasta);
                        $("#divDescarga").html("<a download href=" + data.d.ImagenPie + ">Descargar</a>");
                        $("#divDescarga").show();
                    //}
                    //$("#divDescarga").html("<a href=" + data.d.ImagenPie + "download>Descargar</a>");
                    //$("#flpArch").html(" <a href=/files/facturas/Pie de pagina/ download>Descargar</a>");
                }
                $('#modalGenerarPiePag').modal('show');
            }
        });
    });
}

function filtrar() { 
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    grid.dataSource.filter($filter);
}

function nuevo() {
    $("#divErrorModal").hide();
    $("#hdnIDFacturaPiePagina").val("0");
    $("#divDescarga").hide();
    $("#flpArch").val("");
    $("#txtFactHasta").val("");
    $("#txtFactDesde").val("");
    $("#divOk").hide();
    $("#divError").hide();
    $('#modalGenerarPiePag').modal('show');
}


function renderOptions(data) {
    $("#divErrorModal").hide();
    var html = "<div align='center'>";
    html += "<img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>";
    html += "&nbsp<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/>";

    return html;
}


function validarForm() {
    var valid=false;

    var FacturaDesde=0;
    var FacturaHasta=0;
    if(parseInt($("#txtFactDesde").val())>0)
        FacturaDesde = parseInt($("#txtFactDesde").val());

    if(parseInt($("#txtFactHasta").val())>0)
        FacturaHasta = parseInt($("#txtFactHasta").val());

    var ImagenPie = $("#flpArch").val();
    if (FacturaDesde == 0 ||  FacturaHasta == 0) {
        $("#divErrorModal").html("Los campos Factura Desde y Factura Hasta no pueden estar vacios");
        $("#divErrorModal").show();
        return false;
    }
    if (FacturaDesde > FacturaHasta) {
        $("#divErrorModal").html("El campo Factura Desde no puede ser mayor que el de Factura Hasta");
        $("#divErrorModal").show();
        return false;
    }
    var ext=jQuery.trim(ImagenPie).substr(-4).toLowerCase();
    if (ext != ".jpg" && ext != ".png" && ext != ".pdf" && $("#hdnIDFacturaPiePagina").val()=="0") {
        $("#divErrorModal").html("Debe agregar un archivo jpg, pdf, png");
        $("#divErrorModal").show();
        return false;
    }
    else {
        var id = $("#hdnIDFacturaPiePagina").val();
        var info = "{ id: '" + id + "',factDsd:" + FacturaDesde + ",factHst:" + FacturaHasta + "}";
        $.ajax({
            type: "POST",
            url: "facturas-PiePagina.aspx/validarFacturaPiePaginaExistente",
            async:false,
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (!data.d) {
                    $("#divErrorModal").html("Una de esas facturas de ese rango ya tiene un archivo asignado");
                    $("#divErrorModal").show();
                    valid= false;
                }
                else {
                    valid= true;
                }
            },
            error: function (response) {
                $("#divErrorModal").html("Los datos no se actualizaron correctamente");
                $("#divErrorModal").show();
                valid= false;
            }
        });
        if (!valid)
            return false;
        else
            return true;
    }
}
