﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($("#ddlTipoDoc").val() == "CUIT") {
        if (CuitEsValido($("#txtNroDocumento").val()) == false) {
            $("#divError").html("CUIT Inválido");
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            return false;
        }
    }

    if ($('#formEdicion').valid()) {

        var info = "{ ID: " + $("#hdnID").val()
            + ", RazonSocial: '" + $("#txtRazonSocial").val()
            + "', TipoDoc: '" + $("#ddlTipoDoc option:selected").html()
            + "', NroDocumento: '" + $("#txtNroDocumento").val()
            + "', Telefono: '" + $("#txtTelefono").val()
            + "', Celular: '" + $("#txtCelular").val()
            + "', NombreFantasia: '" + $("#txtNombreFantasia").val()
            + "', IVA: '" + $("#ddlIVA option:selected").val()
            + "', Web: '" + $("#txtWeb").val()
            + "', Email: '" + $("#txtEmail").val()
            + "', Observaciones: '" + $("#txtObservaciones").val()
            + "', Pais: '" + $("#ddlPais option:selected").html()
            + "', Provincia: '" + $("#ddlProvincia").val()
            + "', Ciudad: '" + $("#ddlCiudad").val()
            + "', Domicilio: '" + $("#txtDomicilio").val()
            + "', CodigoPostal: '" + $("#txtCodigoPostal").val()
            + "', TelefonoDom: '" + $("#txtTelefonoDom").val()
            + "', Fax: '" + $("#txtFax").val()
            + "', PisoDepto: '" + $("#txtPisoDepto").val()
            + "', Lat: '" + $("#txtLatitud").val()
            + "', Long: '" + $("#txtLongitud").val()
            + "', EnvioMailsFc: '" + $("#txtEnvioFc").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "proveedorese.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "proveedores.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function configControls() {
    configDatePicker();
    $(".chzn_b").chosen({ allow_single_deselect: true });

    $("#txtNroDocumento").numeric();
}

$(document).ready(function () {
    configControls();

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });

    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombreFantasia").val());
    }
    else
        $("#litTitulo").html("Alta de Proveedor");
});