﻿function UploadCompleted(sender, args) {
    $("#divError").hide();
    //alert($("#hdnAttachment").val());
}

function UploadStarted(sender, args) {
    if (sender._inputFile.files[0].size >= 3000000) {
        var err = new Error();
        err.name = "Upload Error";
        err.message = "El archivo es demasiado grande.";
        throw (err);

        return false;
    }
    else {
        var fileName = args.get_fileName();
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        if (extension == "jpg" || extension == "png" || extension == "pdf") {
            return true;
        } else {
            //To cancel the upload, throw an error, it will fire OnClientUploadError 
            var err = new Error();
            err.name = "Upload Error";
            err.message = "Extension inválida";
            throw (err);

            return false;
        }
    }
}

function UploadError(sender, args) {
    $("#divError").html(args.get_errorMessage());
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function ShowUploadError(msg) {
    $("#divError").html(msg);
    $("#divError").show();
    $("#divOK").hide();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function enviar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkVolver").hide();
    $("#btnEnviar").attr("disabled", true);
    $("#imgLoading").show();
    $("#bodyDetalle").html("");

    if ($("#searchable").val() != "") {
        $('#formEdicion').validate();

        if ($('#formEdicion').valid()) {
            var info = "{ ids: '" + $("#searchable").val()
                + "', items: '" + $("#searchable").text()
                + "', asunto: '" + $("#txtAsunto").val()
                + "', mensaje: '" + $("#txtMensaje").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "facturasenvio-prov.aspx/enviar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != null) {
                        if (data.d.CantErrors > 0) {
                            $("#divError").html(data.d.Mensaje);
                            $("#divError").show();
                        }
                        else {
                            $("#divOK").html("Se han enviado correctamente " + data.d.CantEnvios + " mails");
                            $("#divOK").show();
                        }
                    }
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoading").hide();
                    $("#btnEnviar").attr("disabled", false);
                    $("#lnkVolver").show();
                    //window.location.href = "facturas.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $("#lnkVolver").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#btnEnviar").attr("disabled", false);
                    $("#imgLoading").hide();
                }
            });
        }
        else {
            $("#btnEnviar").attr("disabled", false);
            $("#imgLoading").hide();
            $("#lnkVolver").show();
            return false;
        }
    }
    else {
        $("#btnEnviar").attr("disabled", false);
        $("#divError").html("Por favor, seleccione una o más empresas.");
        $("#divError").show();
        $("#lnkVolver").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $("#imgLoading").hide();
    }
}

function verEnvioErrores(detalle)
{
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Errores");
    $("#headDetalle").html("<tr><th>Errores</th></tr>");
   
    $("#bodyDetalle").html(detalle);
    $('#modalDetalle').modal('show');
}

function configControls() {


    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtFechaHasta, #txtFechaDesde").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });

    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más proveedores"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
}

$(document).ready(function () {
    configControls();
});

//function filtrar() {
//    $("#divError").hide();
//    $("#divOK").hide();

//    grid.dataSource.filter($filter);
//}