﻿function configControls() {

    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        PeriodoDesde: { type: "date" },
                        PeriodoHasta: { type: "date" },
                        Enviadas: { type: "int" },
                        NoEnviadas: { type: "int" },
                        Leidas: { type: "int" },
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "reporteDeEnvios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, PeriodoDesde: $("#txtFechaDesde").val(), PeriodoHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "PeriodoDesde", title: "Periodo Desde", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "PeriodoHasta", title: "Periodo Hasta", format: "{0:dd/MM/yyyy}", width: "80px" },
            { title: "Enviadas", template: "#= renderEnviadas(data) #", width: "80px" },
            { title: "No Enviadas", template: "#= renderNoEnviadas(data) #", width: "80px" },
            { title: "Leidas", template: "#= renderLeidas(data) #", width: "80px" }
        ]
    });
}

function renderEnviadas(data) {
    var html = "<div align='center'>";
    html += "<a href=\"javascript:showDetalle('E','" + data.PeriodoDesde + "','" + data.PeriodoHasta + "');\">" + data.Enviadas + "</a>";
    html += "</div>";

    return html;
}

function renderNoEnviadas(data) {
    var html = "<div align='center'>";
    html += "<a href=\"javascript:showDetalle('N','" + data.PeriodoDesde + "','" + data.PeriodoHasta + "');\">" + data.NoEnviadas + "</a>";
    html += "</div>";

    return html;
}

function renderLeidas(data) {
    var html = "<div align='center'>";
    html += "<a href=\"javascript:showDetalle('L','" + data.PeriodoDesde + "','" + data.PeriodoHasta + "');\">" + data.Leidas + "</a>";
    html += "</div>";

    return html;
}

function filter() {

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formReporte').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();

        grid.dataSource.filter($filter);
    }
}


$(document).ready(function () {

    configControls();
});


function showDetalle(tipo, desde, hasta) {

    $("#bodyDetalle").html();
    if (tipo == "N")
        $("#titDetalle").html("Detalle de NO enviadas");
    else if (tipo == "E")
        $("#titDetalle").html("Detalle de enviados");
    else if (tipo == "L")
        $("#titDetalle").html("Detalle de Leidas");
    var info = "{ tipo: '" + tipo
           + "', desde: '" + desde
           + "', hasta: '" + hasta
           + "'}";
    //alert(info);
    $.ajax({
        type: "POST",
        url: "reporteDeEnvios.aspx/obtenerDetalle",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //alert(data);
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}

