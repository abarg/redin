﻿function pasarAContabilium() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnContabilium").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "pagos.aspx/pasarAContabilium",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#btnContabilium").attr("disabled", false);

                filtrar();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnContabilium").attr("disabled", false);
        }
    });
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    LoadComercios("../common.aspx/LoadCasasMatrices", "ddlComercio");

    $("#txtFechaHasta, #txtFechaDesde, #txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });
      
}

function configGrid() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        IDEntidad: { type: "integer" },
                        Nombre: { type: "string" },
                        Fecha: { type: "date" },
                        Importe: { type: "decimal" },
                        FormaDePago: { type: "string" },
                        Observaciones: { type: "string" },
                        CUIT: { type: "string" },
                        NroComprobante: { type: "string" },
                        EnContabilium: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "pagos.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "Nombre", title: "Comercio", width: "150px" },
            { field: "Fecha", title: "Fecha", width: "70px", format: "{0:dd/MM/yyyy}" },
            { field: "Importe", title: "Importe", width: "80px", format: "{0:c}" },
            { field: "FormaDePago", title: "Forma De Pago", width: "100px" },
            { field: "NroComprobante", title: "Nro Factura", width: "100px" },
            { field: "EnContabilium", title: "En Contabilium", width: "70px", attributes: { class: "colCenter" } },
            { field: "Observaciones", title: "Observaciones", width: "200px" }

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "pagose.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "pagos.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function nuevo() {
    window.location.href = "pagose.aspx";
}

function filtrar() {
    configGri();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var comercio = $("#ddlComercio").val();
    if (comercio != "") {
        $filter.push({ field: "IDEntidad", operator: "equals", value: parseInt(comercio) });
    }

    var cuit = $("#txtDocumento").val();
    if (cuit != "") {
        $filter.push({ field: "CUIT", operator: "contains", value: cuit });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});

function exportar() {

    $("#btnExportar").attr("disabled", true);
    $("#imgLoading").show();

    var FechaDesde = ""
    if ($("#txtFechaDesde").val() != null && $("#txtFechaDesde").val() != "") {
        FechaDesde = $("#txtFechaDesde").val();
    }
    var FechaHasta = ""
    if ($("#txtFechaHasta").val() != null && $("#txtFechaHasta").val() != "") {
        FechaHasta = $("#txtFechaHasta").val();
    }

    var Comercio = ""
    if ($("#ddlComercio").val() != null && $("#ddlComercio").val() != "") {
        Comercio = $("#ddlComercio").val();
    }
    var Cuit = ""
    if ($("#txtDocumento").val() != null && $("#txtDocumento").val() != "") {
        Cuit = $("#txtDocumento").val();
    }


    var info = "{ FechaDesde: '" + FechaDesde
            + "', FechaHasta: '" + FechaHasta
            + "', Comercio: '" + Comercio
            + "', Cuit: '" + Cuit
            + "'}";

    $.ajax({
        type: "POST",
        url: "Pagos.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}