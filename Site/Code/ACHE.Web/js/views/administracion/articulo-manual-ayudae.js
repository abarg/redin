﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    var id = 0;
    if ($("#hfIDArticulo").val() != "")
        id = $("#hfIDArticulo").val();

    if ($('#formEdicion').valid()) {
        var info = "{ idArticulo: " + id
            + ", idseccion: '" + $("#ddlSeccion").val()
            + "', titulo: '" + $("#txtTitulo").val()
            + "', articulo: '" + $("#txtArticulo").val()
            + "', tags: '" + $("#txtTags").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "articulo-manual-ayudae.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "seccion-manual-ayudae.aspx?IDSeccion="+data.d;

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}
function volver() {
    window.location.href = "seccion-manual-ayudae.aspx?IDSeccion=" + $("#hfIDSeccion").val();

}

$(document).ready(function () {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        },
        ignore: ".ignore",
        invalidHandler: function (e, validator) {
            if (validator.errorList.length)
                $('#Tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
});