﻿function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        if ($("#ddlComercio").val() == "" || $("#ddlProveedor").val() == "") {
            if ($("#hdnTipo").val() == "P")
                $("#divError").html("Por favor, seleccione un proveedor");
            else
                $("#divError").html("Por favor, seleccione un comercio");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else {

            var idEntidad = $("#ddlComercio").val();
            if ($("#hdnTipo").val() == "P")
                idEntidad = $("#ddlProveedor").val();

            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", fecha: '" + $("#txtFecha").val()
                + "', concepto: '" + $("#txtConcepto").val()
                + "', idEntidad: " + idEntidad
                + ", precio: '" + $("#txtImporte").val()
                + "', tipo: '" + $("#ddlTipo").val()
                + "', tipoMov: '" + $("#hdnTipo").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "notase.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    
                    if ($("#hdnTipo").val() == "P")
                        window.location.href = "notas-prov.aspx";
                    else
                        window.location.href = "notas.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

$(document).ready(function () {
    configControls();
});


function configControls() {
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    configDatePicker();
    $("#txtImporte").maskMoney({ thousands: '.', decimal: ',', allowNegative: true, allowZero: false, prefix: '$ ' });
    $(".chzn_b").chosen({ allow_single_deselect: true });

    if ($("#hdnTipo").val() == "P") {
        $("#lnkTitulo").attr("href", "notas-prov.aspx");
        $("#lnkCancelar").attr("href", "notas-prov.aspx");
    }
    else {
        $("#lnkTitulo").attr("href", "notas.aspx");
        $("#lnkCancelar").attr("href", "notas.aspx");
    }
}