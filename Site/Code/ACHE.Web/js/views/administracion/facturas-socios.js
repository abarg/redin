﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "facturas-socios.aspx/Exportar",
        data: "{ fechaDesde: '" + $("#txtFechaDesde").val() + "', fechaHasta: '" + $("#txtFechaHasta").val() + "', cuit: '" + $("#txtDocumento").val() + "', fc: '" + $("#txtNumero").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function generar() {
    $("#txtFechaDesdeGenerar").val("");
    $("#txtFechaHastaGenerar").val("");
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#divError").hide();
    $('#modalGenerar').modal('show');
    //$('#flpArch').val('');
}

function generarFacturas() {
    if ($("#txtFechaDesdeGenerar").val() != "" && $("#txtFechaHastaGenerar").val() != "") {

        $("#modalGenerarbody").removeClass("f_error");
        $('#modalGenerar').modal('hide');
        $("#btnFE").attr("disabled", true);
        $("#divError").hide();
        $("#imgLoading").show();
        $("#divOK").hide();

        $.ajax({
            type: "POST",
            url: "facturas-socios.aspx/Generar",
            data: "{fechaDesde: '" + $("#txtFechaDesdeGenerar").val() + "', fechaHasta:'" + $("#txtFechaHastaGenerar").val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    filtrar();
                    //if (data.d.indexOf("Se han generado") != -1) {
                    //    $("#divError").html(data.d);
                    //    $("#divError").show();
                    //}
                    //else {

                    $("#divOK").html("Las Notas de Venta y Liq. Producto se han generado correctamente");
                    $("#divOK").show();
                    //}
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoading").hide();
                    $("#btnFE").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#btnFE").attr("disabled", false);
                //$("#lnkDownload").hide();
            }
        });
    }
    else {
        $("#modalGenerarbody").addClass("f_error");
        $("#divErrorGenerar").show();
    }
}


function verFcErrores() {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Errores");
    $("#headDetalle").html("<tr><th>Proveedor</th><th>Período</th><th>Fecha</th><th>Error</th></tr>");
    $.ajax({
        type: "POST",
        url: "facturas-prov.aspx/obtenerErrores",
        //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    configFechasDesdeHasta("txtFechaDesdeGenerar", "txtFechaHastaGenerar");
    

    $("#txtDocumento").numeric();

    $("#txtFechaHasta, #txtFechaDesde, #txtNumero, #txtDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Nombre: { type: "string" },
                        Numero: { type: "string" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        Subtotal: { type: "decimal" },
                        Iva: { type: "decimal" },
                        Total: { type: "decimal" },
                        CUIT: { type: "string" },
                        EnContabilium: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "facturas-socios.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "80px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "Nombre", title: "Socio", width: "200px" },
            { field: "FechaDesde", title: "Fecha Desde", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaHasta", title: "Fecha Hasta", width: "80px", format: "{0:dd/MM/yyyy}" },
            { field: "Numero", title: "Numero", width: "100px" },
            //{ field: "Subtotal", title: "Subtotal", width: "100px", format: "{0:c}" },
            //{ field: "Iva", title: "Iva", width: "100px", format: "{0:c}" },
            { field: "Total", title: "Total", width: "100px", format: "{0:c}" },
            { field: "EnContabilium", title: "En Contabilium", width: "50px", attributes: { class: "colCenter" } }
            /*,
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/></div>" }, title: "Ver", width: "50px" },
            { title: "Eliminar", template: "#= renderDelete(data) #", width: "50px" },*/
        ]
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        $("#bodyDetalle").html();

        $("#titDetalle").html("Factura Número " + dataItem.Numero);
        $("#headDetalle").html("<tr><th>Concepto</th><th>Precio</th><th>IVA</th><th>Total</th></tr>");
        
        $.ajax({
            type: "POST",
            url: "facturas-socios.aspx/obtenerDetalle",
            data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalDetalle').modal('show');
            }
        });
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "facturas-socios.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    /*$("#grid").delegate(".pdfColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (dataItem.Numero != "") {
            window.open("/facturas/" + dataItem.Numero + ".pdf");
        }
        else
            alert("La factura seleccionada no tiene un CAE generado");
    });*/
}

function renderOptions(data) {
    var html = "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/>&nbsp;<img src='../../img/grid/gridPdf.gif' style='cursor:pointer' onclick=\"descargarPdf(" + data.ID + ",'" + data.Numero + "');\" title='Descargar Fc' class='viewPdf'/>&nbsp;<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    
    return html;
}

function descargarPdf(id,numero)
{
    //window.open("/files/facturas/" + $.trim(numero) + ".pdf");
    $.ajax({
        type: "POST",
        url: "facturas-socios.aspx/Descargar",
        data: "{ id: " + id + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            window.open("/files/liqProducto/" + numero + ".pdf");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function nuevo() {
    window.location.href = "facturase.aspx?Modo=P";
}

function filtrar() {
    $("#divError").hide();
    $("#divOK").hide();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var numero = $("#txtNumero").val();
    if (numero != "") {
        $filter.push({ field: "Numero", operator: "contains", value: numero });
    }

    var cuit = $("#txtDocumento").val();
    if (cuit != "") {
        $filter.push({ field: "CUIT", operator: "contains", value: cuit });
    }

    var fechaDesde = $("#txtFechaDesde").val();
    if (cuit != "") {
        $filter.push({ field: "fechaDesde", operator: "contains", value: fechaDesde });
    }

    var fechaHasta = $("#txtFechaHasta").val();
    if (cuit != "") {
        $filter.push({ field: "fechaHasta", operator: "contains", value: fechaHasta });
    }
    grid.dataSource.filter($filter);
}



$(document).ready(function () {
    configControls();
});


//function UploadCompleted(sender, args) {
//    $("#divError").hide();
//    //alert($("#hdnAttachment").val());
//}

//function UploadStarted(sender, args) {
//    if (sender._inputFile.files[0].size >= 1000000) {
//        var err = new Error();
//        err.name = "Upload Error";
//        err.message = "El archivo es demasiado grande.";
//        throw (err);

//        return false;
//    }
//    else {
//        var fileName = args.get_fileName();
//        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

//        if (extension == "jpg" || extension == "png" || extension == "gif") {
//            return true;
//        } else {
//            //To cancel the upload, throw an error, it will fire OnClientUploadError 
//            var err = new Error();
//            err.name = "Upload Error";
//            err.message = "Extension inválida";
//            throw (err);

//            return false;
//        }
//    }
//}

//function UploadError(sender, args) {
//    //alert("1-" + args.get_errorMessage());
//    //$("#hdnAttachment").val("");
//    $("#divError").html(args.get_errorMessage());
//    $("#divError").show();
//    $("#divOK").hide();
//    $('html, body').animate({ scrollTop: 0 }, 'slow');
//}
