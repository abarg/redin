﻿function pasarAContabilium() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnContabilium").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "facturas.aspx/pasarAContabilium",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#btnContabilium").attr("disabled", false);

                filtrar();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnContabilium").attr("disabled", false);
        }
    });
}

function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: "facturas.aspx/Exportar",
        data: "{ fechaDesde: '" + $("#txtFechaDesde").val() + "', fechaHasta: '" + $("#txtFechaHasta").val() + "', comercio: '" + $("#ddlComercio").val() + "', cuit: '" + $("#txtDocumento").val() + "', fc: '" + $("#txtNumero").val() + "', cbu: '" + $("#txtCBU").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function prueba() {

    $.ajax({
        type: "POST",
        url: "facturas.aspx/obtenerFacturasSinCAE",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (index, item) {
                GuardarComprobante(data.d[index]);
            });
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}


function confirmarCAE() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    $("#divError").hide();

    $("#modalPeriodo").html("Periodo: " + $("#txtFechaDesde").val() + " - " + $("#txtFechaHasta").val());
    $("#modalTarjeta").html("Tarjeta: " + $("#ddlTipoTarjeta").val());

    $('#modalObtenerCAE').modal('show');
}

function verFcErrores() {
    $("#bodyDetalle").html();

    $("#titDetalle").html("Detalle de Errores");
    $("#headDetalle").html("<tr><th>Comercio</th><th>Período</th><th>Fecha</th><th>Error</th></tr>");
    $.ajax({
        type: "POST",
        url: "facturas.aspx/obtenerErrores",
        //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalDetalle').modal('show');
        }
    });
}

function generarCAE() {
    if ($("#txtImporteCAE").val() != "") {

        $("#modalObtenerCAEbody").removeClass("f_error");
        $('#modalObtenerCAE').modal('hide');
        $("#btnFE").attr("disabled", true);
        $("#divError").hide();
        $("#imgLoading").show();
        $("#divOK").hide();

        $.ajax({
            type: "POST",
            url: "facturas.aspx/ObtenerCAE",
            data: "{importe: " + parseInt($("#txtImporteCAE").val()) + ", fechaDesde: '" + $("#txtFechaDesde").val() + "',fechaHasta: '" + $("#txtFechaHasta").val() + "', tipo: " + $("#ddlTipoTarjeta").val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    filtrar();
                    if (data.d.indexOf("Se han generado") != -1) {
                        $("#divError").html(data.d);
                        $("#divError").show();
                    }
                    else {

                        $("#divOK").html(data.d);
                        $("#divOK").show();
                    }
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgLoading").hide();
                    $("#btnFE").attr("disabled", false);
                    //window.location.href = window.location.href.split("/bankInformation.aspx").join(data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoading").hide();
                $("#btnFE").attr("disabled", false);
                //$("#lnkDownload").hide();
            }
        });
    }
    else {
        $("#modalObtenerCAEbody").addClass("f_error");
        $("#divErrorCAE").show();
    }
}

function configControls() {
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    LoadComercios("../common.aspx/LoadCasasMatrices", "ddlComercio");
    $("#divErrorRet").hide();
    $("#divOkRet").hide();

    $("#txtImporteCAE, #txtDocumento", "#txtIIBB", "#txtNroRetIIBB", "#txtIVA", "#txtNroRetIVA", "#txtGANANCIAS", "#txtNroRetGAN").numeric();

    $("#txtFechaHasta, #txtFechaDesde, #txtNumero, #txtDocumento, #txtCBU").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filtrar();
            return false;
        }
    });
}

function configGrid() {
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Tipo: { type: "string" },
                        Nombre: { type: "string" },
                        Numero: { type: "string" },
                        FechaDesde: { type: "date" },
                        FechaHasta: { type: "date" },
                        Subtotal: { type: "decimal" },
                        Iva: { type: "decimal" },
                        Total: { type: "decimal" },
                        CAE: { type: "string" },
                        CUIT: { type: "string" },
                        Enviada: { type: "string" },
                        Descargada: { type: "string" },
                        ArchivoDetalle: { type: "string" },
                        EnContabilium: { type: "string" },
                        CBU: { type: "string" },

                        ImporteDebitar: { type: "decimal" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "facturas.aspx/Buscar", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val(), CBU: $("#txtCBU").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "100px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Retenciones' class='editColumn'/></div>" }, title: "Retenciones", width: "80px" },
            { field: "ID", title: "ID", width: "55px" },
            { field: "Tipo", title: "Tipo", width: "80px" },
            { field: "Nombre", title: "Comercio", width: "200px" },
            { field: "FechaDesde", title: "Desde", width: "150px", format: "{0:dd/MM/yyyy}" },
            { field: "FechaHasta", title: "Hasta", width: "150px", format: "{0:dd/MM/yyyy}" },
            { field: "Numero", title: "Numero", width: "100px" },
            { field: "Subtotal", title: "Subtotal", width: "100px", format: "{0:c}" },
            { field: "Iva", title: "Iva", width: "100px", format: "{0:c}" },
            { field: "Total", title: "Total", width: "100px", format: "{0:c}" },
            { field: "ImporteDebitar", title: "Importe a debitar", width: "110px", format: "{0:c}" },
            { field: "CAE", title: "CAE", width: "100px" },
            { field: "CBU", title: "CBU", width: "100px" },
            { field: "EnContabilium", title: "En Contabilium", width: "70px", attributes: { class: "colCenter" } },
            { field: "Enviada", title: "Enviada", width: "70px", attributes: { class: "colCenter" } },
            { field: "Descargada", title: "Descargada", width: "50px", attributes: { class: "colCenter" } }
            /*,
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/></div>" }, title: "Ver", width: "50px" },
            { title: "Eliminar", template: "#= renderDelete(data) #", width: "50px" },*/
        ]
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        $("#bodyDetalle").html();

        if (dataItem.CAE == null || dataItem.CAE == "")
            $("#titDetalle").html("Factura NO informado a la AFIP.");
        else
            $("#titDetalle").html("Factura Número " + dataItem.Numero);
        $("#headDetalle").html("<tr><th>Concepto</th><th>Precio</th><th>IVA</th></tr>");

        $.ajax({
            type: "POST",
            url: "facturas.aspx/obtenerDetalle",
            data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalDetalle').modal('show');
            }
        });
    });

    $("#grid").delegate(".procesarColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea facturar electrónicamente el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "facturas.aspx/procesarFC",
                data: "{ idFactura: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Está seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "facturas.aspx/Eliminar",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        $("#divErrorRet").hide();
        $("#divOkRet").hide();
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        $("#hdnIDFacturas").val(dataItem.ID);
        $("#bodyRetenciones").html();

        $("#titRetenciones").html("Factura Número " + dataItem.Numero);
        //var tit = "<label> Número Retencion </label>";
        //tit += "<input id=" + "'txtNroRetencion'" + "maxlength=" + "'10'" + "class=" + "' form-control validDate greaterThan'" + "/><br>";
        // tit += "<tr><th>Tipo Retencion</th><th>Importe</th></tr>"
        $("#headRetenciones").html("<tr><th>Tipo Retencion</th><th>Nro Retención</th><th>Importe</th></tr>");
        //$("#headRetenciones").html("<tr><th>Tipo Retencion</th><th>Importe</th></tr>");

        $.ajax({
            type: "POST",
            url: "facturas.aspx/obtenerRetenciones",
            data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyRetenciones").html(data.d);
                }
                $('#modalRetenciones').modal('show');
            }
        });
    });
}

function guardarRetencion() {

    var idFactura = $("#hdnIDFacturas").val();

    var info = "{ idFactura: " + parseInt(idFactura)
            + ", importeIIBB: '" + $("#txtIIBB").val()
            + "', nroIIBB: '" + $("#txtNroRetIIBB").val()
            + "', importeIVA: '" + $("#txtIVA").val()
            + "', nroIVA: '" + $("#txtNroRetIVA").val()
            + "', importeGanancias: '" + $("#txtGANANCIAS").val()
            + "', nroGanancias: '" + $("#txtNroRetGAN").val()
            + "', importeOtros: '" + $("#txtOTROS").val()
            + "', nroOtros: '" + $("#txtNroRetOTR").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "facturas.aspx/guardarRetencion",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //var r = jQuery.parseJSON(response.responseText);
            $('#modalRetenciones').modal('hide');
            filtrar();
            $("#divErrorRet").hide();
            $("#divOkRet").show();

        },
        error: function (response) {
            $("#divOkRet").hide();
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorRet").html(r.Message);
            $("#divErrorRet").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });

}

function renderOptions(data) {
    var html = "<div align='center'>";
    if (data.Numero == null || data.Numero == "")
        html += "<img src='../../img/grid/gridUpload.gif' style='cursor:pointer' title='Obtener CAE' class='procesarColumn'/>&nbsp;<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/>&nbsp;<img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/>";
    else
        html += "<img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/>&nbsp;<a download href='/files/facturas/" + $.trim(data.Numero) + ".pdf'><img src='../../img/grid/gridPdf.gif' title='Descargar Fc' class='viewPdf'/></a>";

    if (data.ArchivoDetalle != null && data.ArchivoDetalle != "")
        html += "&nbsp;<a download href='/files/fc-detalles/" + data.ArchivoDetalle + "'><img src='../../img/grid/gridDetails.gif' style='cursor:pointer' title='Descargar detalle' class='detColumn'/></a>";

    if (data.CAE != null && data.CAE != "")
        html += "&nbsp<img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Retenciones' class='editColumn'/>";
    html += "</div>";

    return html;
}

/*
function renderDelete(data)
{
    if (data.Numero == null || data.Numero == "")
        return "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>";
    else
        return "";
}

function renderPdfIcon(data) {  
    if (data.Numero == null || data.Numero == "")
        return "";
    else
        return "<div align='center'><img src='../../img/grid/gridPdf.gif' style='cursor:pointer' onclick=\"descargarPdf('" + data.Numero + "');\" title='Descargar Fc' class='viewPdf'/></div>";
}*/

function descargarPdf(numero) {
    window.open("/files/facturas/" + $.trim(numero) + ".pdf");
}

function nuevoManual() {
    window.location.href = "facturase.aspx?Modo=M";
}

function nuevoRecurrente() {
    window.location.href = "facturase.aspx?Modo=R";
}

function filtrar() {
    $("#divError").hide();
    $("#divOK").hide();

    configGrid();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var comercio = $("#ddlComercio").val();
    if (comercio != "") {
        $filter.push({ field: "IDEntidad", operator: "equals", value: parseInt(comercio) });
    }

    var numero = $("#txtNumero").val();
    if (numero != "") {
        $filter.push({ field: "Numero", operator: "contains", value: numero });
    }

    var cuit = $("#txtDocumento").val();
    if (cuit != "") {
        $filter.push({ field: "CUIT", operator: "contains", value: cuit });
    }
    var cbu = $("#txtCBU").val();
    if (cbu != "") {
        $filter.push({ field: "CBU", operator: "contains", value: cbu });
    }
    var tipo = $("#ddlTipoTarjeta").val();
    if (tipo != "") {
        $filter.push({ field: "Tipo", operator: "contains", value: tipo });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();
});