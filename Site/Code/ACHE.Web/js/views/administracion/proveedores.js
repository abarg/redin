﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ RazonSocial: '" + $("#txtRazonSocial").val()
            + "', NroDocumento: '" + $("#txtDocumento").val()
            + "', Nombre: '" + $("#txtNombre").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "proveedores.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function configControls() {
    $("#txtDocumento").numeric();
    
    $("#txtDocumento, #txtRazonSocial, #txtNombre").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        NombreFantasia: { type: "string" },
                        RazonSocial: { type: "string" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "integer" },
                        Telefono: { type: "string" },
                        Celular: { type: "string" },
                        CondicionIva: { type: "string" },
                        Email: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "proveedores.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "ID", title: "ID", width: "50px" },
            { field: "NombreFantasia", title: "Nombre Fantasía", width: "150px" },
            { field: "RazonSocial", title: "Razón social", width: "150px" },
            { field: "TipoDocumento", title: "Tipo Doc.", width: "75px" },
            { field: "NroDocumento", title: "Nro. Doc", width: "100px" },
            { field: "CondicionIva", title: "Condicion Iva", width: "100px" },
            { field: "Telefono", title: "Teléfono", width: "100px" },
            { field: "Email", title: "Email", width: "200px" }

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "proveedorese.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "proveedores.aspx/Delete",
                data: "{ id: " + dataItem.IDComercio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });

}

function Nuevo() {
    window.location.href = "proveedorese.aspx";
}

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var RazonSocial = $("#txtRazonSocial").val();
    if (RazonSocial != "") {
        $filter.push({ field: "RazonSocial", operator: "contains", value: RazonSocial });
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "NombreFantasia", operator: "contains", value: nombre });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

});