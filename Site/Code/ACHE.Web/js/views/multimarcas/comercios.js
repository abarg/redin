﻿

$(document).ready(function () {

    configControls();

    $('#formComercio').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();
    configGrid();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var SDS = $("#txtSDS").val();
    if (SDS != "") {
        $filter.push({ field: "SDS", operator: "contains", value: SDS });
    }

    var documento = $("#txtDocumento").val();
    if (documento != "") {
        $filter.push({ field: "NroDocumento", operator: "contains", value: documento });
    }

    var RazonSocial = $("#txtRazonSocial").val();
    if (RazonSocial != "") {
        $filter.push({ field: "RazonSocial", operator: "contains", value: RazonSocial });
    }

    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "NombreFantasia", operator: "contains", value: nombre });
    }

    var Establecimiento = $("#txtEstablecimiento").val();
    if (Establecimiento != "") {
        $filter.push({ field: "NumEst", operator: "contains", value: Establecimiento });
    }

    var PosTerminal = $("#txtPosTerminal").val();
    if (PosTerminal != "") {
        $filter.push({ field: "PosTerminal", operator: "contains", value: PosTerminal });
    }

    var descuento = $("#txtDescuento").val();
    if (descuento != "") {
        $filter.push({ field: "Descuento", operator: "contains", value: descuento });
    }

    var idMarca = 0;
    if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "") {
        $filter.push({ field: "IDMarca", operator: "equals", value: parseInt($("#ddlMarcas").val()) });
    }

    grid.dataSource.filter($filter);
}

function configControls() {
    $("#txtDocumento").numeric();
    $("#txtSDS").numeric();
    $("#txtCBU").numeric();
    $("#txtDescuento").numeric();

    $("#txtSDS, #txtDocumento, #txtRazonSocial, #txtEstablecimiento, #txtPosTerminal, #txtNombre, #txtDealer, #txtDescuento,#txtCBU").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDComercio: { type: "integer" },
                        IDTerminal: { type: "integer" },
                        SDS: { type: "integer" },
                        NombreFantasia: { type: "string" },
                        RazonSocial: { type: "string" },
                        TipoDocumento: { type: "string" },
                        NroDocumento: { type: "integer" },
                        Telefono: { type: "string" },
                        Celular: { type: "string" },
                        Responsable: { type: "string" },
                        Email: { type: "string" },
                        POSSistema: { type: "string" },
                        POSTerminal: { type: "integer" },
                        NumEst: { type: "string" },
                        IDMarca: { type: "integer" },
                        Marca: { type: "string" },
                        IDFranquicia: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Comercios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { field: "IDTerminal", title: "ID", width: "50px" },
            { field: "SDS", title: "SDS", width: "75px" },
            { field: "NombreFantasia", title: "Nombre Fantasía", width: "150px" },
            { field: "RazonSocial", title: "Razón social", width: "150px" },
            { field: "Marca", title: "Marca", width: "100px" },
            { field: "TipoDocumento", title: "Tipo Doc.", width: "75px" },
            { field: "NroDocumento", title: "Nro. Doc", width: "100px" },
            { field: "Telefono", title: "Teléfono", width: "100px" },
            { field: "Responsable", title: "Responsable", width: "150px" },
            { field: "Email", title: "Email", width: "200px" },
            { field: "POSSistema", title: "POS Sistema", width: "100px" },
            { field: "POSTerminal", title: "POS Terminal", width: "100px" },
            { field: "NumEst", title: "Nro de Est.", width: "150px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Comerciose.aspx?IDTerminal=" + dataItem.IDTerminal;
    });

}