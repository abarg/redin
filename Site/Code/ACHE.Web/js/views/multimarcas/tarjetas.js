﻿
function configControls() {

    $("#txtTarjeta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    //GRILLA ASOCIADOS
    var myColumns = [
        { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "12px" },
        { field: "IDSocio", title: "ID", width: "50px" },
        { field: "NroDocumento", title: "Nro Documento", width: "100px" },
        { field: "Apellido", title: "Apellido", template: "<a class='apellidoColulmn'>#=Apellido#</a>", width: "100px" }
    ];
    var Responsable = parseInt($("#hdnResponsable").val());
    if (Responsable == 1) {
        myColumns.splice(0, 1)
    }
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDTarjeta: { type: "integer" },
                        Socio: { type: "string" },
                        Marca: { type: "string" },
                        Numero: { type: "string" },
                        Estado: { type: "string" },
                        IDSocio: { type: "integer" },
                        MotivoBaja: { type: "string" },
                        FechaBaja: { type: "date" },
                        FechaAsignacion: { type: "date" },
                        FechaVencimiento: { type: "date" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "tarjetas.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "Marca", title: "Marca", width: "100px" },
            { title: "", template: "#= renderOptions(data) #", width: "30px" },
            { field: "Socio", title: "Socio", width: "120px" },
            { field: "Numero", title: "Numero", width: "120px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "FechaAsignacion", title: "Fecha Asig", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "FechaVencimiento", title: "Fecha Venc", format: "{0:dd/MM/yyyy}", width: "80px" },
            { field: "Estado", title: "Estado", width: "80px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "Credito", title: "Crédito $", width: "80px", format: "{0:c}" },
            { field: "Giftcard", title: "Giftcard $", width: "80px", format: "{0:c}" },
            { field: "Total", title: "Total $", width: "80px", format: "{0:c}" },
            { field: "POS", title: "POS", width: "80px" },
            { field: "MotivoBaja", title: "Motivo Baja", width: "120px" },
            { field: "FechaBaja", title: "Fecha de Baja", format: "{0:dd/MM/yyyy}", width: "80px" }
        ]
    });


    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var fecha = dataItem.FechaVencimiento;
        day = fecha.getDate(),
            month = fecha.getMonth() + 1,
            year = fecha.getFullYear();
        $("#txtFechaVencimiento").val(day + "/" + month + "/" + year);
        $("#hdnID").val(dataItem.IDTarjeta);
        $("#hdnNumero").val(dataItem.Numero);
        $("#modalCargarFechaVencimiento").modal("show");
    });

}

function verTodos() {
    $("#txtTarjeta").val("");
    filter();
}

function filter() {
    configGrid();
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var Tarjeta = $("#txtTarjeta").val();
    if (Tarjeta != "") {
        $filter.push({ field: "Numero", operator: "contains", value: Tarjeta });
    }

    var idMarca = 0;
    if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "") {
        $filter.push({ field: "IDMarca", operator: "equals", value: parseInt($("#ddlMarcas").val()) });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls();

    $('#formTarjeta').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

function renderOptions(data) {
    var html = "";
    html = "<div align='left'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/>&nbsp;";
    return html;
}

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var idMarca = 0;
    if ($("#ddlMarcas").val() != null && $("#ddlMarcas").val() != "")
        idMarca = parseInt($("#ddlMarcas").val());

    var info = "{ numero: '" + $("#txtTarjeta").val()
            + "',  idMarca: " + idMarca
            + "}";

    //var info = "{ numero: '" + $("#txtTarjeta").val() + "',  idMarca: " + idMarca + "}";

    $.ajax({
        type: "POST",
        url: "tarjetas.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#btnExportar").attr("disabled", false);
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", "/files/tarjetas/tarjetas.csv");
                var fecha = new Date();
                var dia = fecha.getDate();
                var mes = (fecha.getMonth() + 1)
                var anio = fecha.getFullYear();
                $("#lnkDownload").attr("download", "Tarjetas_" + dia + mes + anio + ".csv");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function guardarFechaVencimiento() {
    $('#formEdicion').validate();
    if ($('#formEdicion').valid()) {

        var info = "{ id: " + parseInt($("#hdnID").val())
          + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
          + "'}";

        $.ajax({
            type: "POST",
            url: "tarjetas.aspx/guardarFechaVenc",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divError").hide();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#modalCargarFechaVencimiento").modal("hide");
                filter();
                $("#divOk").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}
