﻿function changeCheck() {
    if ($("#chk0").is(':checked')) {
        $("#divTodos").hide();
    }
    else {
        $("#divTodos").show();
    }
}

function configControls() {
    configDatePicker();

    if ($("#ddlTransacciones").val() != "") {
        $("#divFechaTrans").show()
    }

    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 145,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9 ñ\-\., ]*$/i.test(value);
    }, "El mensaje sólo puede contener letras, números, puntos, guiones medios y espacios en blanco.");

    //jQuery.validator.addMethod("telefonos", function (value, element) {
    //    return this.optional(element) || /^[0-9;]+$/i.test(value);
    //}, "El mensaje sólo puede contener números y punto y coma como separador.");

    jQuery.validator.addMethod("telefonos", function (value, element) {
        return this.optional(element) || /^[0-9\-\ a-z ;]+$/i.test(value);
    }, "El mensaje sólo puede contener letras, números, guiones medios y punto y coma como separador.");

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de Campaña SMS " + $("#txtNombre").val());
    }
    else
        $("#litTitulo").html("Alta de Campaña SMS");
}

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var sexo = "";
        var edades = "";
        var precioSMS = $("#hdnPrecio").val();
        var tipo = $("#hdnTipo").val();
        var idProfesion = 0;
        var idProvincia = 0;
        var idMarca = 0;
        var idCiudad = 0;
        var fechaDesdeTrans = "";
        var tipoTrans = "";

        if (tipo == "I") {

            if ($("#ddlProfesiones").val() != "")
                idProfesion = $("#ddlProfesiones").val();

            if ($("#ddlProvincias").val() != "")
                idProvincia = $("#ddlProvincias").val();

            if ($("#ddlCiudades").val() != "" && $("#ddlCiudades").val() != null)
                idCiudad = $("#ddlCiudades").val();

            tipoTrans = $("#ddlTransacciones").val();
            fechaDesdeTrans = $("#txtFechaDesde").val();

            if ($("#rdbTodos").is(':checked'))
                sexo = "T";
            else if ($("#rdbFemenino").is(':checked'))
                sexo = "F";
            else if ($("#rdbMasculino").is(':checked'))
                sexo = "M";

            if ($("#chk1").is(':checked'))
                edades += "0-18;";
            if ($("#chk2").is(':checked'))
                edades += "19-25;";
            if ($("#chk3").is(':checked'))
                edades += "26-30;";
            if ($("#chk4").is(':checked'))
                edades += "31-40;";
            if ($("#chk5").is(':checked'))
                edades += "41-50;";
            if ($("#chk6").is(':checked'))
                edades += "51-60;";
            if ($("#chk7").is(':checked'))
                edades += "61-200;";
            if ($("#chk0").is(':checked'))
                edades = "Todas";

            if ($("#chk1").is(':checked') && $("#chk2").is(':checked') && $("#chk3").is(':checked') && $("#chk4").is(':checked') && $("#chk5").is(':checked') && $("#chk6").is(':checked') && $("#chk7").is(':checked'))
                edades = "Todas";
        }

        var telefonos = $("#txtTelefonos").val();
        var lastChar = telefonos[telefonos.length - 1];
        if (lastChar == ";")
            telefonos = telefonos.substring(0, telefonos.length - 1);
        var valid = true;
        if (telefonos != "") {
            var array = telefonos.split(';');
            var cant = array.length;
            for (var i = 0; i < cant; i++) {
                if (array[i].length < 10)
                    valid = false;
            }
        }

        if (edades == "" && tipo == "I") {
            $("#lblError").show();
            $("#lblError").html("Debe seleccionar un rango de edades, o todas");
        }
        else if (!valid && tipo == "E") {
            $("#lblError").show();
            $("#lblError").html("Todos los teléfonos deben ser mayores a 10 caracteres");
        }
        else {
            
            if ($("#ddlMarcas").val() != "" && $("#ddlMarcas").val() != null)
                idMarca = $("#ddlMarcas").val();


            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", tipo: '" + $("#hdnTipo").val()
                + "', nombre: '" + $("#txtNombre").val()
                + "', sexo: '" + sexo
                + "', edad: '" + edades
                + "', mensaje: '" + $("#txtMensaje").val()
                + "', fechaEnvio: '" + $("#txtFechaEnvio").val()
                + "', telefonos: '" + telefonos
                + "', idProvincia: '" + parseInt(idProvincia)
                + "', idCiudad: '" + parseInt(idCiudad)
                + "', tipoTrans: '" + tipoTrans
                + "', fechaDesdeTrans: '" + fechaDesdeTrans
                + "', idProfesion: '" + idProfesion
                + "', idMarca: " + parseInt(idMarca)
                + " }";

            $.ajax({
                type: "POST",
                url: "smse-int-ext.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOK').show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $("#hdnID").val(data.d);

                    //   realizarActivacion();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}


function calcular() {
    var sexo = "";
    var edades = "";
    var precioSMS = "0";
    var prov = "";
    var ciudad = "";
    var fechaDesde = "";
    var tipoTrans = "";
    var profesion = "";

    if ($("#rdbTodos").is(':checked'))
        sexo = "T";
    else if ($("#rdbFemenino").is(':checked'))
        sexo = "F";
    else if ($("#rdbMasculino").is(':checked'))
        sexo = "M";

    if ($("#chk1").is(':checked'))
        edades += "0-18;";
    if ($("#chk2").is(':checked'))
        edades += "19-25;";
    if ($("#chk3").is(':checked'))
        edades += "26-30;";
    if ($("#chk4").is(':checked'))
        edades += "31-40;";
    if ($("#chk5").is(':checked'))
        edades += "40-50;";
    if ($("#chk6").is(':checked'))
        edades += "51-60;";
    if ($("#chk7").is(':checked'))
        edades += "61-200;";
    if ($("#chk0").is(':checked'))
        edades = "Todas";

    if ($("#chk1").is(':checked') && $("#chk2").is(':checked') && $("#chk3").is(':checked') && $("#chk4").is(':checked') && $("#chk5").is(':checked') && $("#chk6").is(':checked') && $("#chk7").is(':checked'))
        edades = "Todas";

    if ($("#ddlProvincias").val() != "")
        prov = $("#ddlProvincias").val();

    if ($("#ddlCiudades").val() != "" && $("#ddlCiudades").val() != null)
        ciudad = $("#ddlCiudades").val();

    if ($("#ddlTransacciones").val() != "")
        tipoTrans = $("#ddlTransacciones").val();

    if ($("#fechaDesde").val() != "")
        fechaDesde = $("#txtFechaDesde").val();

    if ($("#ddlProfesiones").val() != "")
        profesion = $("#ddlProfesiones").val();

    if (edades != "") {
        $("#lblError").hide();

        var info = "{ edad: '" + edades
            + "', sexo: '" + sexo
            + "', precioSMS: '" + precioSMS
            + "', prov: '" + prov
            + "', ciudad: '" + ciudad
            + "', tipoTrans: '" + tipoTrans
            + "', fechaDesde: '" + fechaDesde
            + "', profesion: '" + profesion
            + "', idMarca: '" + $("#ddlMarcas").val()
            + "' }";

        $.ajax({
            type: "POST",
            url: "smse-int-ext.aspx/Calcular",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#spnCantidad").text(data.d[1]);
                    $("#spnCantidad").show();
                    $(".calculo").show();
                    $("#divError").hide();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#spnCantidad").hide();
            }
        });
    }
    else {
        $("#lblError").show();
        $("#lblError").html("Debe seleccionar un rango de edades, o todas");
    }
}


$(document).ready(function () {
    $(".calculo").hide();
    var tipo = $("#hdnTipo").val();
    if (tipo == "I") {
        $(".externa").hide();
        $(".interna").show();
    }
    else if (tipo == "E") {
        $(".interna").hide();
        $(".externa").show();
    }

    configControls();
    changeCheck();
});


$("#ddlProvincias").change(function () {

    $('#ddlCiudades').html('');
    $('#ddlCiudades').text('');

    var idProvincia = 0;
    if ($("#ddlProvincias").val() != "")
        idProvincia = parseInt($("#ddlProvincias").val());

    $.ajax({
        type: "POST",
        url: "smse-int-ext.aspx/ciudadesByProvincias",
        data: "{ idProvincia: " + idProvincia
            + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (data.d != "" && data.d != null) {
                $.each(data.d, function () {
                    $("#ddlCiudades").append($("<option/>").val(this.ID).text(this.Nombre));
                });
                $("#divError").hide();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#spnCantidad").hide();
        }
    });
});


$("#ddlTransacciones").change(function () {

    if ($("#ddlTransacciones").val() != "") {
        //$("#txtFechaDesde").prop('disabled', false);
        $("#divFechaTrans").show();
    }
    else {
        //$("#txtFechaDesde").prop('disabled', true);
        $("#divFechaTrans").hide();
        $("#txtFechaDesde").val("");
    }
});