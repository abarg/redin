﻿function realizarCambio(estado) {
    var info = "{ id: " + parseInt($("#hdnID").val()) + ", estado: '" + estado + "', mensaje: '" + $("#txtMensaje").val() + "'}";

    $.ajax({
        type: "POST",
        url: "smse.aspx/cambiarEstado",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $('#divOK').show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            window.location.href = "sms.aspx";
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function cambiarEstado(estado) {
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {

        var estadoDesc = estado == "A" ? "aprobada" : "rechazada";
        var msg = "La campaña " + $("#txtNombre").val() + " será " + estadoDesc + ". <br />¿Desea continuar?";
        smoke.confirm(msg, function (e) {
            if (e) {
                $("#divError").hide();
                $("#divOk").hide();
                realizarCambio(estado);

            } else {
                window.location.href = "sms.aspx";
            }
        }, { cancel: "Cancelar", ok: "Aceptar" });

    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}

function configControls() {
    $('.rtaMax').jqEasyCounter({
        'maxChars': 160,
        'maxCharsWarning': 145,
        'msgFontColor': '#000',
        'msgWarningColor': '#F00'
    });

    jQuery.validator.addMethod("telefonos", function (value, element) {
        return this.optional(element) || /^[0-9;]+$/i.test(value);
    }, "El mensaje sólo puede contener números y punto y coma como separador.");

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
    if ($("#hdnID").val() != "0") {
        $("#litTitulo").html("Edición de Campaña SMS " + $("#txtNombre").val());
    }
    else
        $("#litTitulo").html("Alta de Campaña SMS");
}

$(document).ready(function () {
    var tipo = $("#hdnTipo").val();
    if (tipo == "I") {
        $(".externa").hide();
        $(".interna").show();
    }
    else if (tipo == "E") {
        $(".interna").hide();
        $(".externa").show();
    }

    configControls();
});