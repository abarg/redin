﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formTarjeta').validate();

    if ($("#txtPuntosDisponibles").numeric().val() == "") $("#txtPuntosDisponibles").val("0");
    if ($("#txtPuntosTotales").numeric().val() == "") $("#txtPuntosTotales").val("0");

    if ($('#formTarjeta').valid()) {

        var info = "{ IDTarjeta: " + parseInt($("#hfIDTarjeta").val())
            + ", Numero: '" + $("#txtNumero").val()
            + "', Marca: " + parseInt($("#ddlMarcas").val())
            + ", Franquicia: " + parseInt($("#ddlFranquicias").val())
            + ", FechaAlta: '" + $("#txtFechaAlta").val()
            + "', FechaEmision: '" + $("#txtFechaEmision").val()
            + "', FechaVto: '" + $("#txtFechaVto").val()
            + "', PuntosDisponibles: '" + $("#txtPuntosDisponibles").val()
            + "', PuntosTotales: '" + $("#txtPuntosTotales").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "TarjetasEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "tarjetas.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function irTarjetasSustitucion() {
    window.location.href = "TarjetasSustitucion.aspx?IDTarjeta=" + $("#hfIDTarjeta").val();
}

$(document).ready(function () {
    $("#txtNumero").numeric();
    $("#txtPuntosTotales").numeric();
    $("#txtPuntosDisponibles").numeric();

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,
        changeYear: true,
        yearRange: "2000:2049"
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $("#txtFechaAlta").datepicker();
    $("#txtFechaEmision").datepicker();
    $("#txtFechaVto").datepicker();

    /*$("#dialog").dialog({
        autoOpen: false,
        modal: true,
        height: 680,
        width: 600,
        open: function (ev, ui) {
            $("#frameTarjetasSus").attr('src', 'TarjetasSustitucion.aspx');
        }
    });
    $("#linkTarjetasSust").click(function (e) {
        $("#dialog").show();
        e.preventDefault();
        $("#dialog").dialog('open');
    });*/

    $('#formTarjeta').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});