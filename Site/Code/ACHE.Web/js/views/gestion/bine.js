﻿
function grabar() {
    $("#divError").hide();
    $("#divErrorRubro").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hdnIDBIN").val())
            + ", nombre: '" + $("#txtNombre").val()
            + "', numero: " + parseInt($("#txtNumero").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "bine.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#hdnIDBIN").val(data.d);
                $("#litTitulo").html("Edición de " + $("#txtNombre").val());

                filter();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
}


$(document).ready(function () {

    $("#txtNumero").numeric();

    if ($("#hdnIDBIN").val() != "0") {
        $("#litTitulo").html("Edición de " + $("#txtNombre").val());
    }
    else {
        $("#litTitulo").html("Alta de BIN");
    }

});