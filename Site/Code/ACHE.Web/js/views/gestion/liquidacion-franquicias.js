﻿$(document).ready(function () {
    configDatePicker();
    $(".chzn_b").chosen({ allow_single_deselect: true });
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
});

function generar() {
  
    $("#divError").hide();
 
    $("#formLiquidacionFranq").validate();
    $("#btnGenerar").attr("disabled", true);

    if ($('#formLiquidacionFranq').valid()) {
        var idFranq = $("#ddlFranquicias").val();
     

        var info = "{ idFranquicia: " + idFranq
            + ", fechaDesde: '" + $("#txtFechaDesde").val()
           + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "' }";

        $.ajax({
            type: "POST",
            url: "Liquidacion-Franquicias.aspx/GenerarLiquidacion",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/common/Liquidaciones.aspx";
             
              
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#btnGenerar").attr("disabled", false);
            }
        });
    }
    else {
        $("#btnGenerar").attr("disabled", false);

        return false;
    }
}

