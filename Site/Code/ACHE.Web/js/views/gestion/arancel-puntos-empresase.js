﻿$(document).ready(function () {
    $("#txtCostoFijo").numeric();
    $("#txtDescuento, #txtDescuento2, #txtDescuento3, #txtDescuento4, #txtDescuento5, #txtDescuento6, #txtDescuento7").numeric();
    $("#txtDescuentoVip, #txtDescuentoVip2, #txtDescuentoVip3, #txtDescuentoVip4, #txtDescuentoVip5, #txtDescuentoVip6, #txtDescuentoVip7").numeric();
    $("#txtMulPuntosVip, #txtMulPuntosVip2, #txtMulPuntosVip3, #txtMulPuntosVip4, #txtMulPuntosVip5, #txtMulPuntosVip6, #txtMulPuntosVip7").numeric();
    $("#txtPuntosVip1, #txtPuntosVip2, #txtPuntosVip3, #txtPuntosVip4, #txtPuntosVip5, #txtPuntosVip6, #txtPuntosVip7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtMulPuntos1,#txtMulPuntos2,#txtMulPuntos3,#txtMulPuntos4,#txtMulPuntos5,#txtMulPuntos6,#txtMulPuntos7").numeric();
    $("#txtPuntos, #txtPuntos2, #txtPuntos3, #txtPuntos4, #txtPuntos5, #txtPuntos6, #txtPuntos7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    $("#txtArancel, #txtArancel2, #txtArancel3, #txtArancel4, #txtArancel5, #txtArancel6, #txtArancel7").maskMoney({ thousands: '.', decimal: ',', allowNegative: false, allowZero: true, prefix: '' });
    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más comercios"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
    $("#ddlEmpresas").val($("#hdnIDEmpresa").val());
    $(".chosen").chosen();

});


function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ comercios: '" + $("#searchable").val()
            + "', POSArancel: '" + $("#txtArancel").val()
            + "', Arancel2: '" + $("#txtArancel2").val()
            + "', Arancel3: '" + $("#txtArancel3").val()
            + "', Arancel4: '" + $("#txtArancel4").val()
            + "', Arancel5: '" + $("#txtArancel5").val()
            + "', Arancel6: '" + $("#txtArancel6").val()
            + "', Arancel7: '" + $("#txtArancel7").val()
            + "', POSPuntos: '" + $("#txtPuntos").val()
            + "', Puntos2: '" + $("#txtPuntos2").val()
            + "', Puntos3: '" + $("#txtPuntos3").val()
            + "', Puntos4: '" + $("#txtPuntos4").val()
            + "', Puntos5: '" + $("#txtPuntos5").val()
            + "', Puntos6: '" + $("#txtPuntos6").val()
            + "', Puntos7: '" + $("#txtPuntos7").val()
            + "', MultPuntos1: '" + $("#txtMulPuntos1").val()
            + "', MultPuntos2: '" + $("#txtMulPuntos2").val()
            + "', MultPuntos3: '" + $("#txtMulPuntos3").val()
            + "', MultPuntos4: '" + $("#txtMulPuntos4").val()
            + "', MultPuntos5: '" + $("#txtMulPuntos5").val()
            + "', MultPuntos6: '" + $("#txtMulPuntos6").val()
            + "', MultPuntos7: '" + $("#txtMulPuntos7").val()
            + "', Descuento: '" + $("#txtDescuento").val()
            + "', Descuento2: '" + $("#txtDescuento2").val()
            + "', Descuento3: '" + $("#txtDescuento3").val()
            + "', Descuento4: '" + $("#txtDescuento4").val()
            + "', Descuento5: '" + $("#txtDescuento5").val()
            + "', Descuento6: '" + $("#txtDescuento6").val()
            + "', Descuento7: '" + $("#txtDescuento7").val()
            + "', DescuentoVip: '" + $("#txtDescuentoVip").val()
            + "', DescuentoVip2: '" + $("#txtDescuentoVip2").val()
            + "', DescuentoVip3: '" + $("#txtDescuentoVip3").val()
            + "', DescuentoVip4: '" + $("#txtDescuentoVip4").val()
            + "', DescuentoVip5: '" + $("#txtDescuentoVip5").val()
            + "', DescuentoVip6: '" + $("#txtDescuentoVip6").val()
            + "', DescuentoVip7: '" + $("#txtDescuentoVip7").val()
            + "',PuntosVip: '" + $("#txtPuntosVip1").val()
            + "',PuntosVip2: '" + $("#txtPuntosVip2").val()
            + "',PuntosVip3: '" + $("#txtPuntosVip3").val()
            + "',PuntosVip4: '" + $("#txtPuntosVip4").val()
            + "',PuntosVip5: '" + $("#txtPuntosVip5").val()
            + "',PuntosVip6: '" + $("#txtPuntosVip6").val()
            + "',PuntosVip7: '" + $("#txtPuntosVip7").val()
            + "', MulPuntosVip: '" + $("#txtMulPuntosVip").val()
            + "', MulPuntosVip2: '" + $("#txtMulPuntosVip2").val()
            + "', MulPuntosVip3: '" + $("#txtMulPuntosVip3").val()
            + "', MulPuntosVip4: '" + $("#txtMulPuntosVip4").val()
            + "', MulPuntosVip5: '" + $("#txtMulPuntosVip5").val()
            + "', MulPuntosVip6: '" + $("#txtMulPuntosVip6").val()
            + "', MulPuntosVip7: '" + $("#txtMulPuntosVip7").val()
            + "', CostoFijo: '" + $("#txtCostoFijo").val()
            + "', UsoRed: " + $('#chkCobrarUsoRed').is(':checked')
            + "}";

        $.ajax({
            type: "POST",
            url: "Arancel-puntos-empresase.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                $("#txtDescuento, #txtDescuento2, #txtDescuento3, #txtDescuento4, #txtDescuento5, #txtDescuento6, #txtDescuento7").val("");
                $("#txtDescuentoVip, #txtDescuentoVip2, #txtDescuentoVip3, #txtDescuentoVip4, #txtDescuentoVip5, #txtDescuentoVip6, #txtDescuentoVip7").val("");
                $("#txtMulPuntosVip, #txtMulPuntosVip2, #txtMulPuntosVip3, #txtMulPuntosVip4, #txtMulPuntosVip5, #txtMulPuntosVip6, #txtMulPuntosVip7").val("");
                $("#txtPuntosVip1, #txtPuntosVip2, #txtPuntosVip3, #txtPuntosVip4, #txtPuntosVip5, #txtPuntosVip6, #txtPuntosVip7").val("");
                $("#txtMulPuntos1,#txtMulPuntos2,#txtMulPuntos3,#txtMulPuntos4,#txtMulPuntos5,#txtMulPuntos6,#txtMulPuntos7").val("");
                $("#txtPuntos, #txtPuntos2, #txtPuntos3, #txtPuntos4, #txtPuntos5, #txtPuntos6, #txtPuntos7").val("");
                $("#txtArancel, #txtArancel2, #txtArancel3, #txtArancel4, #txtArancel5, #txtArancel6, #txtArancel7").val("");
                $('#pnlpaso2').hide();
                //  window.location.href = "PromocionesPuntuales.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

function validar() {
    var isValid = true;
    var msg = "";

    if ($("#ddlEmpresas").val() == "" || $("#ddlEmpresas").val() == null) {
    }
    else {
        guardarEmpresa();
    }


    return isValid;
}


function guardarEmpresa() {
    $("#hdnIDEmpresa").val($("#ddlEmpresas").val());

}