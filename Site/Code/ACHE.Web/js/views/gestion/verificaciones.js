﻿function configControls() {
    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más comercios"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }
    $(".chosen").chosen();
}

function cargarCiudades() {
    var idProvincia = parseInt($("#cmbProvincia").val());
    if (idProvincia > 0) {
        var info = "{ idProvincia: " + idProvincia + " }";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/CargarCiudades",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null && data.d.length > 0) {
                    $("#cmbCiudad").empty();
                    $("#cmbCiudad").append("<option value=''></option>");
                    $("#cmbZona").empty();
                    $("#cmbZona").append("<option value=''></option>");
                    for (var i = 0; i < data.d.length; i++)
                        $("#cmbCiudad").append("<option value='" + data.d[i].ID+ "'>" + data.d[i].Nombre + "</option>");
                    $('#cmbCiudad, #cmbZona').trigger("liszt:updated");
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
            }
        });
    }
}

function cargarZonas() {
    var idProvincia = parseInt($("#cmbProvincia").val());
    var idCiudad = parseInt($("#cmbCiudad").val());
    if (idProvincia > 0 && idCiudad > 0) {
        var info = "{ idProvincia: " + idProvincia + ", idCiudad: " + idCiudad + " }";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/CargarZonas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#cmbZona").empty();
                $("#cmbZona").append("<option value=''></option>");
                if (data.d != null && data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++)
                        $("#cmbZona").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
                }
                $('#cmbZona').trigger("liszt:updated");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
            }
        });
    }
}

function validar() {
    var isValid = true;
    var msg = "";

    if ($("#cmbProvincia").val() == "" || $("#cmbProvincia").val() == "0" || $("#cmbProvincia").val() == null) {
        isValid = false;
        $("#spnProvincia").show();
    }
    else {
        guardarProvincia();
        $("#spnProvincia").hide();
    }

    if ($("#cmbCiudad").val() == "" || $("#cmbCiudad").val() == "0" || $("#cmbCiudad").val() == null) {
        isValid = false;
        $("#spnCiudad").show();
    }
    else {
        guardarCiudad();
        $("#spnCiudad").hide();
    }

    if ($("#cmbZona").val() == "" || $("#cmbZona").val() == "0" || $("#cmbZona").val() == null) {
        isValid = false;
        $("#spnZona").show();
    }
    else {
        guardarZona();
        $("#spnZona").hide();
    }

    return isValid;
}

function generar() {
    $("#divError").hide();
    $("#imgLoading").show();
    $("#btnGenerar").attr("disabled", true);
    $("#btnDescargar").hide();
    if ($("#searchable").val() != "") {
        var info = "{ comercios: '" + $("#searchable").val() + "', zona: '" + $("#hdnZona").val() + "'}";

        $.ajax({
            type: "POST",
            url: "verificacionesPOS.aspx/GenerarPlanillas",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null && data.d != "") {
                    $("#imgLoading").hide();
                    $("#btnDescargar").show();
                    $("#btnDescargar").show();
                    $("#btnDescargar").attr("href", data.d);
                    $("#btnDescargar").attr("download", data.d);
                    $("#btnGenerar").attr("disabled", false);
                }
                else {
                    $("#imgLoading").hide();
                    $("#btnDescargar").hide();
                    $("#btnGenerar").attr("disabled", false);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#imgLoading").hide();
                $("#btnDescargar").hide();
                $("#btnGenerar").attr("disabled", false);
            }
        });
    }
    else {
        $("#divError").html("Debe seleccionar al menos un comercio");
        $("#divError").show();
    }
}

function guardarProvincia() {
    $("#hdnIDProvincia").val($("#cmbProvincia").val());
}

function guardarCiudad() {
    $("#hdnIDCiudad").val($("#cmbCiudad").val());
}

function guardarZona() {
    $("#hdnIDZona").val($("#cmbZona").val());
    $("#hdnZona").val($("#cmbZona option:selected").text());
}

$(document).ready(function () {
    configControls();
});