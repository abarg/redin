﻿var oTable = null;

function configControls() {
    $("#txtTarjeta").numeric();

    $("#txtTarjeta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDTarjeta: { type: "integer" },
                        IDMarca: { type: "integer" },
                        Marca: { type: "string" },
                        IDFranquicia: { type: "integer" },
                        Franquicia: { type: "string" },
                        Numero: { type: "string" },
                        FechaAlta: { type: "date" },
                        FechaEmision: { type: "date" },
                        FechaVencimiento: { type: "date" },
                        FechaBaja: { type: "date" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        Total: { type: "number" },
                        POS: { type: "integer" },
                        Tipo: { type: "string" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "tarjetas.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            //{ command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "IDTarjeta", title: "ID", width: "50px" },
            { field: "Franquicia", title: "Franquicia", width: "100px" },
            { field: "Marca", title: "Marca", width: "120px" },
            { field: "Numero", title: "Numero", width: "150px" },
            { field: "FechaAlta", title: "Fecha de alta", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "FechaEmision", title: "Emisión", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "POS", title: "POS", width: "80px" },
            { field: "Tipo", title: "Tipo", width: "100px" },

            { field: "FechaBaja", title: "FechaBaja", format: "{0:dd/MM/yyyy}", width: "100px" }

            //{ field: "PuntosTotales", title: "Puntos Totales", width: "100px" },
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        $('#hdnIDTarjeta').val(dataItem.IDTarjeta);
        $('#ddlTipo').val(dataItem.Tipo);

        $('#titEditarTipo').text("Edicion de la tarjeta: " + dataItem.Numero);
        $('#modalTipoTarjetas').modal('show');

        //  window.location.href = "tarjetasedicion.aspx?IDTarjeta=" + dataItem.IDTarjeta;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "tarjetas.aspx/Delete",
                data: "{ id: " + dataItem.IDTarjeta + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        }
    });

    $("#grid").delegate(".activateColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea activar la tarjeta seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "tarjetas.aspx/Activar",
                data: "{ id: " + dataItem.IDTarjeta + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        }
    });

    $("#grid").delegate(".viewColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        //$('#tableDetalleTr').fixedHeaderTable('destroy');
        $("#titDetalleTr").html("Detalle de Transacciones tarjeta Nro " + dataItem.Numero);// + ", " + dataItem.Nombre);


        $.ajax({
            type: "POST",
            url: "transacciones.aspx/GetByTarjeta",
            data: "{ tarjeta: '" + dataItem.Numero + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#bodyDetalleTr").html("");

                if (data != null && data.d.length > 0) {

                    if (oTable != null) {

                        oTable.fnClearTable();
                        oTable.fnDestroy();
                    }

                    oTable = $('#tableDetalleTr').dataTable({
                        "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
                        "paging": true,
                        "bLengthChange": false,
                        "iDisplayLength": 10,
                        "ordering": false,
                        "bSort": false,
                        "info": false,
                        //"bDestroy": true,
                        "searching": false,
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                        "fnDrawCallback": function () {
                            var pageCount = Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength);
                            if (pageCount == 1) {
                                $('.dataTables_paginate').first().hide();
                            } else {
                                $('.dataTables_paginate').first().show();
                            }
                        }
                    });

                    for (var i = 0; i < data.d.length; i++) {
                        oTable.fnAddData([
                            data.d[i].Fecha,
                            data.d[i].Hora,
                            data.d[i].Operacion,
                            data.d[i].SDS,
                            data.d[i].Comercio,
                            data.d[i].NroEstablecimiento,
                            data.d[i].ImporteOriginal,
                            data.d[i].ImporteAhorro,
                            data.d[i].Puntos]
                        );
                    }

                    $("#tableDetalleTr_info").parent().remove();
                    $("#tableDetalleTr").css("width", "100%");
                    $(".dataTables_paginate").first().parent().removeClass("col-sm-7");
                    $(".dataTables_paginate").first().parent().addClass("col-sm-12");
                }
                else
                    $("#bodyDetalleTr").html("<tr><td colspan='9'>No hay un detalle disponible</td></tr>");

                $('#modalDetalleTr').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });
    });
}


function renderOptions(data) {
    var html = "";
    html += "<div align='center'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver Transacciones' class='viewColumn'/>";
    if(data.FechaBaja != null)
        html += "&nbsp;&nbsp;<img src='../../img/grid/gridRoles.gif' style='cursor:pointer' title='Dar de alta' class='activateColumn'/></div>";
    html += "</div>";

    return html;
}

function renderDoc(data) {
    return data.TipoDocumento + " " + data.NroDocumento;
}

function Nuevo() {
    window.location.href = "tarjetasedicion.aspx";
}
function guardarTipo() {
    $.ajax({
        type: "POST",
        url: "tarjetas.aspx/CambiarTipo",
        data: "{id: " + $("#hdnIDTarjeta").val() + ", tipo: '" + $("#ddlTipo").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                filter();
                $("#divError").hide();
                $('#modalTipoTarjetas').modal('hide');
            
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        
        }
    });
}
function verTodos() {
    $("#txtTarjeta").val("");
    $("#cmbTipo").val("");
    $("#ddlMarcas").val("0");
    $("#ddlFranquicias").val("0");
    filter();
}

function filter() {
    configGrid();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();

    var Tarjeta = $("#txtTarjeta").val();
    if (Tarjeta != "") {
        $filter.push({ field: "Numero", operator: "contains", value: Tarjeta });
    }

    var marca = parseInt($("#ddlMarcas").val());
    if (marca > 0) {
        $filter.push({ field: "IDMarca", operator: "equal", value: marca });
    }

    var franquicia = parseInt($("#ddlFranquicias").val());
    if (franquicia > 0) {
        $filter.push({ field: "IDFranquicia", operator: "equal", value: franquicia });
    }
    var tipo = $("#cmbTipo").val();
    if (tipo != "") {
        $filter.push({ field: "Tipo", operator: "equal", value: tipo });
    }
    grid.dataSource.filter($filter);
}

function exportar() {
    $("#divError").hide();
    $("#divOk").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ numero: '" + $("#txtTarjeta").val()
            + "', idMarca: " + $("#ddlMarcas").val()
            + ", idFranquicia: " + $("#ddlFranquicias").val()
                        + ", tipo: '" + $("#cmbTipo").val()

            + "' }";

    $.ajax({
        type: "POST",
        url: "tarjetas.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#btnExportar").attr("disabled", false);
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", "/files/tarjetas/tarjetas.csv");
                var fecha = new Date();
                var dia = fecha.getDate();
                var mes = (fecha.getMonth() + 1)
                var anio = fecha.getFullYear();
                $("#lnkDownload").attr("download", "Tarjetas_" + dia + mes + anio + ".csv");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}


function exportarCalcupan() {
    $("#divError").hide();
    $("#divOk").hide();
    
    if ($("#ddlMarcas").val() == "0") {
        $("#divError").html("Seleccione una marca por favor");
        $("#divError").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
    else {
        $("#lnkDownloadCalcupan").hide();
        $("#imgLoadingCalcupan").show();
        $("#btnExportarCalcupan").attr("disabled", true);

        var info = "{ numero: '" + $("#txtTarjeta").val()
                + "', idMarca: " + $("#ddlMarcas").val()
                + ", idFranquicia: " + $("#ddlFranquicias").val()
                                        + ", tipo: '" + $("#cmbTipo").val()

                + "' }";

        $.ajax({
            type: "POST",
            url: "tarjetas.aspx/ExportarCalcupan",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoadingCalcupan").hide();
                    $("#btnExportarCalcupan").attr("disabled", false);
                    $("#lnkDownloadCalcupan").show();
                    $("#lnkDownloadCalcupan").attr("href", data.d);
                    $("#lnkDownloadCalcupan").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgLoadingCalcupan").hide();
                $("#lnkDownloadCalcupan").hide();
                $("#btnExportarCalcupan").attr("disabled", false);
            }
        });
    }
}


$(document).ready(function () {
    configControls();

    $('#formTarjeta').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});