﻿function actualizar() {

    console.log("QUE MIERDA")

    console.log($("#ddlComercio").count);

    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    $("#btnGrabar").attr("disabled", true);
    $("#imgLoading").show();

    if ($('#formEdicion').valid()) {
        var idComercio =""
        if ($("#ddlComercio").val() != "") {
            idComercio = parseInt($("#ddlComercio").val());
        }
        var comercio = $("#ddlComercio option:selected").text();
        var fechaDesde = $("#txtDesde").val();
        var fechaHasta = $("#txtHasta").val();
        var puntos = $("#txtPuntos").val();
        var arancel = $("#txtArancel").val();
        var marca = $("#ddlMarcas option:selected").text();

        var info = "{ idComercio: '" + idComercio
            + "', fechaDesde: '" + fechaDesde
            + "', fechaHasta: '" + fechaHasta
            + "', puntos: '" + puntos
            + "', arancel: '" + arancel
            + "', idMarca: " + parseInt($("#ddlMarcas").val())
            + ", operacion: '" + $("#ddlOperacion").val()
            + "', costoRed: '" + $("#txtCostoRed").val()
            + "', origen: '" + $("#ddlOrigen").val()
             + "', puntosVip: '" + $("#txtPuntosVip").val()
            + "', multPuntos: " + $("#txtMulPuntos").val()
            + ", multPuntosVip: " + $("#txtMulPuntosVip").val()
            + ", arancelVip: '" + $("#txtArancelVip").val()
            + "'}";

        var msg = "¿Desea actualizar los puntos y aranceles que se encuentran entre las fechas: " + fechaDesde + " y " + fechaHasta;

        if (comercio != "")
            msg += " del comercio: " + comercio;
        if (marca != "")
            msg += " de la marca: " + marca;

        msg += " ?"
        
        smoke.confirm(msg, function (e) {
            if (e) {
                $("#divError").hide();
                $("#divOk").hide();
                $.ajax({
                    type: "POST",
                    url: "ModificacionPuntosArancel.aspx/actualizarPuntosArancel",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        $('#divOK').show();
                        $('#divOK').html("Se han actualizado correctamente las transacciones");
                        $("#btnGrabar").attr("disabled", false);
                        $("#imgLoading").hide();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');

                        $("#btnGrabar").attr("disabled", false);
                        $("#imgLoading").hide();
                    }
                });
                //smoke.alert('"yeah it is" pressed', { ok: "close" });
            } else {
                //smoke.alert('"no way" pressed', { ok: "close" });
                $("#btnGrabar").attr("disabled", false);
                $("#imgLoading").hide();

            }
        }, { ok: "Aceptar", cancel: "Cancelar" });
    }
    else {
        $("#btnGrabar").attr("disabled", false);
        $("#imgLoading").hide();

        return false;
    }
}

$(document).ready(function () {
    $(".numeric").numeric();
    LoadComercios("../common.aspx/LoadCasasMatrices", "ddlComercio");

    $(".chzn_b").chosen({ allow_single_deselect: true });
    configDatePicker();
    configFechasDesdeHasta("txtDesde", "txtHasta");

    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

