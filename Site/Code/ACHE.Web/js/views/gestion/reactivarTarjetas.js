﻿function verErrores() {
    $.ajax({
        type: "POST",
        url: "ReactivarTarjetas.aspx/obtenerErrores",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }

            $('#modalDetalle').modal('show');
        }
    });
}



$(document).ready(function () {
    $(".chosen").chosen();
});


function descargarEjemplo() {
    $.ajax({
        type: "POST",
        url: "ReactivarTarjetas.aspx/descargarEjemplo",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        }
    });
}