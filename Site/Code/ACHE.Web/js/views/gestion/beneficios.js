﻿
$(document).ready(function () {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
    configControls();
    $("#btnExportar").css("display", 'none');
});


function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    /*
    var com = $("#ddlComercios").val();
    if (com != "") {
        $filter.push({ field: "IDComercio", operator: "equal", value: parseInt(com) });
    }
    */

    grid.dataSource.filter($filter);
}

function Nuevo() {
    window.location.href = "Beneficiose.aspx";
}

function configControls() {
    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        IDBeneficio: { type: "integer" },
                        mulPuntos: { type: "integer" }, 
                        Nombre: { type: "string" },
                        Sexo: { type: "string" },
                        Edad: { type: "string" },
                        fechaDesde: { type: "date" },
                        fechaHasta: { type: "date" },

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Beneficios.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            //{ field: "IDZona", title: "ID", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "fechaDesde", title: "Fecha Desde", width: "50px", format: "{0:dd/MM/yyyy}" },
            { field: "fechaHasta", title: "Fecha Hasta", width: "50px", format: "{0:dd/MM/yyyy}" },
            { field: "Nombre", title: "Nombre", width: "50px" },
            { field: "Sexo", title: "Sexo", width: "50px" },
            { field: "Edad", title: "Edad", width: "50px" },
            { field: "mulPuntos", title: "Mult. Puntos", width: "50px" },
            { field: "IDBeneficio", title: "IDBeneficio", width: "50px", hidden: "hidden" }

            //{ title: "Opciones", template: "#= renderOptions(data) #", width: "50px" },

        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

        window.location.href = "Beneficiose.aspx?ID=" + dataItem.IDBeneficio;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "Beneficios.aspx/Delete",
                data: "{ id: " + dataItem.IDBeneficio + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var err = eval("(" + xhr.responseText + ")");
                    $("#divError").html(err.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}
