﻿function generarCambio() {
    var info = "{ tipo: '" + $("#ddlTipo").val()
            + "', idTerminal: " + parseInt($("#ddlComercio").val())
            + ", anterior: '" + $("#txtAnterior").val()
            + "', nuevo: '" + $("#txtNuevo").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "historialTermEstabe.aspx/generarCambio",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $('#divOK').show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            smoke.alert("Se han actualizado " + data.d + " transacciones", { ok: "<a href='historialTermEstab.aspx' style='color:#fff'>cerrar</a>" });
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}

function realizarCambio() {
    var msg = "Los cambios fueron realizados correctamente. <br />¿Desea actualizar las transacciones con el nro de " + $("#ddlTipo option:selected").text() + " " + $("#txtAnterior").val() + " por el nro " + $("#txtAnterior").val() + "?";
    smoke.confirm(msg, function (e) {
        if (e) {
            $("#divError").hide();
            $("#divOk").hide();
            generarCambio();
            //smoke.alert('"yeah it is" pressed', { ok: "close" });
        } else {
            window.location.href = "historialTermEstab.aspx";
            //smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}

function grabar() {
    
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var info = "{ id: " + parseInt($("#hfID").val())
            + ", idTerminal: " + parseInt($("#ddlComercio").val())
            + ", tipo: '" + $("#ddlTipo").val()
            + "', anterior: '" + $("#txtAnterior").val()
            + "', nuevo: '" + $("#txtNuevo").val()
            + "', observaciones: '" + $("#txtObservaciones").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "historialTermEstabe.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOK').show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                //window.location.href = "historialTermEstab.aspx";
                realizarCambio();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    //LoadComercios("../common.aspx/LoadComercios", "ddlComercio");
    //$(".chzn_a").chosen({ allow_single_deselect: false });
    $(".chzn_b").chosen({ allow_single_deselect: true });


    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});
