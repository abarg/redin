﻿function nueva() {
    window.location.href = "posweb.aspx";
}

function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var marca = $("#ddlMarcas").val();
    var franquicia = $("#ddlFranquicias").val();

    if (marca == "")
        marca = 0;
    if (franquicia == "")
        franquicia = 0;

    var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "', NumTerminal: '" + $("#txtTerminal").val()
            + "', NumEst: '" + $("#txtNumeroEst").val()
            + "', documento: '" + $("#txtDocumento").val()
            + "', comercio: '" + $("#txtComercio").val()
            + "', idMarca: " + parseInt(marca)
            + ", idFranquicia: " + parseInt(franquicia)
            + ", origen: '" + $("#ddlOrigen").val()
            + "', operacion: '" + $("#ddlOperacion").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "transacciones.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtNumeroEst, #txtTarjeta, #txtDocumento").numeric();

    $("#txtNumeroEst, #txtFechaDesde, #txtFechaHasta, #txtTarjeta, #txtDocumento, #txtComercio, #txtTerminal").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    //LoadComercios("../common.aspx/LoadComercios", "ddlTrComercio");
    //$(".chzn_a").chosen({ allow_single_deselect: false });
    //$(".chzn_b").chosen({ allow_single_deselect: true });

    //Buscador
    $('#formTransacciones').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

function configGrid() {

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        Fecha: { type: "string" },
                        Hora: { type: "string" },
                        Operacion: { type: "string" },
                        SDS: { type: "string" },
                        Comercio: { type: "string" },
                        NroEstablecimiento: { type: "string" },
                        Tarjeta: { type: "string" },
                        NumTerminal: { type: "string" },
                        Socio: { type: "string" },
                        ImporteOriginal: { type: "number" },
                        ImporteAhorro: { type: "number" },
                        Puntos: { type: "integer" },
                        PuntosTotales: { type: "integer" },
                        IDMarca: { type: "integer" },
                        Marca: { type: "string" },
                        IDFranquicia: { type: "integer" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Transacciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val(), NumTerminal: $("#txtTerminal").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" },
            { field: "ID", title: "ID", width: "70px" },
            { field: "Fecha", title: "Fecha", width: "80px" },
            { field: "Hora", title: "Hora", width: "70px" },
            { field: "Operacion", title: "Operacion", width: "80px" },
            { field: "SDS", title: "SDS", width: "75px" },
            { field: "Comercio", title: "Comercio", width: "150px" },
            { field: "Tarjeta", title: "Tarjeta", width: "140px" },
            { field: "NumTerminal", title: "Terminal", width: "140px" },
            { field: "Marca", title: "Marca", width: "100px" },
            { field: "NroEstablecimiento", title: "Nro Est.", width: "120px" },
            { field: "Socio", title: "Socio", width: "150px" },
            { field: "ImporteOriginal", title: "Importe", width: "70px" },
            { field: "ImporteAhorro", title: "Ahorro", width: "70px" },
            { field: "Puntos", title: "Puntos", width: "70px" },
            { field: "PuntosTotales", title: "Total", width: "70px" }
        ]
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar la TR seleccionada? Una vez eliminada, no podra ser recuperada.")) {
            $.ajax({
                type: "POST",
                url: "transacciones.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });
}

function filter() {
    configGrid();

    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formTransacciones').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();

        var Tarjeta = $("#txtTarjeta").val();
        if (Tarjeta != "") {
            $filter.push({ field: "Tarjeta", operator: "contains", value: Tarjeta });
        }

        var NumEst = $("#txtNumeroEst").val();
        if (NumEst != "") {
            $filter.push({ field: "NroEstablecimiento", operator: "contains", value: NumEst });
        }

        var Documento = $("#txtDocumento").val();
        if (Documento != "") {
            $filter.push({ field: "NroDocumentoSocio", operator: "contains", value: Documento });
        }

        var origen = $("#ddlOrigen").val();
        if (origen != "") {
            $filter.push({ field: "Origen", operator: "equal", value: origen });
        }

        var comercio = $("#txtComercio").val();
        if (comercio != "") {
            $filter.push({ field: "Comercio", operator: "contains", value: comercio });
        }

        var marca = $("#ddlMarcas").val();
        if (marca != "") {
            $filter.push({ field: "IDMarca", operator: "equal", value: parseInt(marca) });
        }

        var franquicia = $("#ddlFranquicias").val();
        if (franquicia != "") {
            $filter.push({ field: "IDFranquicia", operator: "equal", value: parseInt(franquicia) });
        }

        var operacion = $("#ddlOperacion").val();
        if (operacion != "") {
            $filter.push({ field: "Operacion", operator: "equal", value: operacion });
        }

        var terminal = $("#txtTerminal").val();
        if (terminal != "") {
            $filter.push({ field: "NumTerminal", operator: "equal", value: terminal });
        }

        grid.dataSource.filter($filter);
    }
}

$(document).ready(function () {
    configControls();
});