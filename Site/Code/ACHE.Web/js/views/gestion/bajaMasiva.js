﻿function ejecutar() {
    $("#divOK").hide();
    $("#divError").hide();
    $('#formBajaMasiva').validate();
    $("#btnEjecutar").attr("disabled", true);
    $("#imgLoading").show();

    if ($('#formBajaMasiva').valid()) {
        var info = "{ desde: " + parseInt($("#txtDesde").val())
            + ", hasta: " + parseInt($("#txtHasta").val())
            + ", idMarca: " + $("#ddlMarcas").val()
            + ", idFranquicia: " + $("#ddlFranquicias").val()
            + ", tipo: '" + $("#ddlTipo").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "BajaMasiva.aspx/EjecutarBaja",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null && (data.d[1] > 0 || data.d[0] > 0)) {
                    var tarjetasPuntos = data.d[0];
                    var bajasSinPuntos = data.d[1];
                    if (tarjetasPuntos > 0) {
                        var msg = "Se han encontrado " + tarjetasPuntos + " tarjetas con puntos, ¿desea dar de baja estas?";
                        smoke.confirm(msg, function (e) {
                            if (e) {
                                $.ajax({
                                    type: "POST",
                                    url: "BajaMasiva.aspx/EjecutarBajaPuntos",
                                    data: info,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data, text) {
                                        if (data.d != null && data.d > 0) {
                                            $("#divError").hide();
                                            $("#divOK").html("Se han dado de baja " + bajasSinPuntos + " tarjetas sin puntos y " + data.d + " con puntos");
                                            $("#divOK").show();
                                            $("#btnEjecutar").attr("disabled", false);
                                            $("#imgLoading").hide();
                                            $("#txtDesde").val("");
                                            $("#txtHasta").val("");
                                        }
                                    },
                                    error: function (response) {
                                        var r = jQuery.parseJSON(response.responseText);
                                        $("#divOK").hide();
                                        $("#divError").html(r.Message);
                                        $("#divError").show();
                                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                                        $("#btnEjecutar").attr("disabled", false);
                                        $("#imgLoading").hide();
                                    }
                                });
                            } else {
                                $("#divError").hide();
                                $("#divOK").html("Se han dado de baja " + bajasSinPuntos + " tarjetas sin puntos.");
                                $("#divOK").show();
                                $("#btnEjecutar").attr("disabled", false);
                                $("#imgLoading").hide();
                                $("#txtDesde").val("");
                                $("#txtHasta").val("");
                            }
                        }, { ok: "Aceptar", cancel: "Cancelar" });
                    }
                    else {
                        $("#divError").hide();
                        $("#divOK").html("Se han dado de baja " + bajasSinPuntos + " tarjetas sin puntos.");
                        $("#divOK").show();
                        $("#btnEjecutar").attr("disabled", false);
                        $("#imgLoading").hide();
                        $("#txtDesde").val("");
                        $("#txtHasta").val("");
                    }
                }
                else {
                    $("#divError").hide();
                    $("#divOK").html("No se han encontrado tarjetas con los filtros ingresados para dar de baja");
                    $("#divOK").show();
                    $("#btnEjecutar").attr("disabled", false);
                    $("#imgLoading").hide();
                    $("#txtDesde").val("");
                    $("#txtHasta").val("");
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divOK").hide();
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#btnEjecutar").attr("disabled", false);
                $("#imgLoading").hide();
            }
        });
    }
    else {
        $("#btnEjecutar").attr("disabled", false);
        $("#imgLoading").hide();
        return false;
    }
}

function getMarcas() {
    var info = "{ idFranquicia: " + $("#ddlFranquicias").val() + " }";

    $.ajax({
        type: "POST",
        url: "BajaMasiva.aspx/GetMarcas",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#ddlMarcas").empty();
                $("#ddlMarcas").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#ddlMarcas").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //alert(r.Message);
        }
    });
}

$(document).ready(function () {
    $('#formBajaMasiva').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});