﻿$(document).ready(function () {
    configControls();
})

function configControls() {
    terminalesControls();
    marcasControls();
    persistNotificationsListener();
}

function persistNotificationsListener() {

    $("#btnEnviar").on("click", function () {
        persistNotifications();
    })
}

function persistNotifications() {

    var terminales = $("#searchable").val();

    console.log("hey", terminales)

    var info = { 'terminales': terminales, 'mensaje': $('#txtMensaje').val(), 'titulo': $('#txtTitulo').val() };

    $.ajax({
        type: "POST",
        url: "msjPushe.aspx/persistirNotificaciones",
        data: "{'terminales':'" + info.terminales + "','mensaje': '" + info.mensaje + "','titulo': '" + info.titulo + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            console.log('cool')
            $("#divOK").toggle();
            window.scrollTo(0, 0);
        },
        error: function (response) {
    
        }
    });

}

function marcasControls() {

    $("#ddlMarcas").on("change", function () {
        terminalesPorMarcas();
    });

    $('#ddlMarcas').multiSelect({
        selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search-marcas" autocomplete="off" placeholder="Selecciona 1 o más opciones"></div>',
        selectionHeader: "<div class='search-selected'></div>"
    });

    if ($('#ms-search-marcas').length) {
        $('#ms-search-marcas').quicksearch($('.ms-elem-selectable', '#ms-searchable-marcas')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable-marcas').focus();
                return false;
            }
        })
    }
}

function terminalesPorMarcas() {

    var marcas = '';
    if ($("#ddlMarcas").val() != null)
        marcas = $("#ddlMarcas").val();

    var info = "{ marcas: '" + marcas
        + "', Actividades: '" + "none"
        + "'}";

    $.ajax({
        type: "POST",
        url: "msjPushe.aspx/getTerminalesPorMarcas",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            $('#searchable').empty().multiSelect("refresh"); //remove all child nodes

            if (Object.keys(data.d).length > 0) {
                $.each(data.d, function (k, v) {

                    console.log(v)

                    $("#searchable").append($("<option/>").val(v.ID).text(v.Nombre));
                });

                $("#divErrorTr").hide();
                $("#imgLoadingTr").hide();
                $("#lnkDownloadTr").show();
                $("#lnkDownloadTr").attr("href", data.d);
                $("#lnkDownloadTr").attr("download", data.d);
                $("#btnExportarTr").attr("disabled", false);

                $("#searchable").multiSelect("refresh");

            }
            else {
                $("#searchable").empty().trigger('liszt:updated');
                console.log("nothing")
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divErrorTr").html(r.Message);
            $("#divErrorTr").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoadingTr").hide();
            $("#lnkDownloadTr").hide();
            $("#btnExportarTr").attr("disabled", false);
        }
    });

}

function terminalesControls() {

    if ($('#searchable').length) {
        //* searchable
        $('#searchable').multiSelect({
            selectableHeader: '<div class="search-header"><input type="text" class="form-control" id="ms-search" autocomplete="off" placeholder="Selecciona 1 o más opciones"></div>',
            selectionHeader: "<div class='search-selected'></div>"
        });

        $('#select-all').click(function () {
            $('#searchable').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#searchable').multiSelect('deselect_all');
            return false;
        });
    }
    if ($('#ms-search').length) {
        $('#ms-search').quicksearch($('.ms-elem-selectable', '#ms-searchable')).on('keydown', function (e) {
            if (e.keyCode == 40) {
                $(this).trigger('focusout');
                $('#ms-searchable').focus();
                return false;
            }
        })
    }

}