﻿function generar() {
    $("#divError").hide();
    $('#formAltaMasiva').validate();
  

    if ($('#formAltaMasiva').valid()) {


        var msg = "Esta por dar de alta tarjetas de tipo " + $("#ddlTipo option:selected").text(); + ".<br /> ¿Desea continuar?";
        smoke.confirm(msg, function (e) {
            if (e) {
                $("#btnGenerar").attr("disabled", true);
                $("#imgLoading").show();
                var info = "{ PanDesde: '" + $("#ddlBIN").val()
                    + "', PanHasta: '" + $("#txtPanHasta").val()
                    + "', Desde: '" + $("#txtDesde").val()
                    + "', Hasta: '" + $("#txtHasta").val()
                    + "', Marca: " + $("#ddlMarcas").val()
                    + ", Franquicia: " + $("#ddlFranquicias").val()
                    + ", tipo: '" + $("#ddlTipo").val()
                    + "'}";

                $.ajax({
                    type: "POST",
                    url: "AltaMasiva.aspx/generar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        $("#divError").hide();
                        $("#divOK").show();
                        $("#btnGenerar").attr("disabled", false);
                        $("#imgLoading").hide();
                        $("#txtDesde").val("");
                        $("#txtHasta").val("");
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divOK").hide();
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        $("#btnGenerar").attr("disabled", false);
                        $("#imgLoading").hide();
                    }
                });
            } else {
                $("#imgLoading").hide();
                $("#btnGenerar").attr("disabled", false);

            }
        }, { ok: "Aceptar", cancel: "Cancelar" });








    }
    else {
        $("#btnGenerar").attr("disabled", false);
        $("#imgLoading").hide();
        return false;
    }
}

function getMarcas() {
    var info = "{ idFranquicia: " + $("#ddlFranquicias").val() + " }";

    $.ajax({
        type: "POST",
        url: "AltaMasiva.aspx/GetMarcas",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d.length > 0) {
                $("#ddlMarcas").empty();
                $("#ddlMarcas").append("<option value=''></option>");
                for (var i = 0; i < data.d.length; i++)
                    $("#ddlMarcas").append("<option value='" + data.d[i].ID + "'>" + data.d[i].Nombre + "</option>");
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //alert(r.Message);
        }
    });
}
function getDatosMarca() {
    var info = "{ idMarca: " + $("#ddlMarcas").val() + " }";

    $.ajax({
        type: "POST",
        url: "AltaMasiva.aspx/GetDatosMarca",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null && data.d!="") {
                $("#txtDesde").val(data.d.split('#')[0]);
                $("#txtPanHasta").val(data.d.split('#')[1]);

            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            //alert(r.Message);
        }
    });
}
$(document).ready(function () {

    $('#formAltaMasiva').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});