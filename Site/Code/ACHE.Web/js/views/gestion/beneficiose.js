﻿function changeCheck() {
    if ($("#chk0").is(':checked')) {
        $("#divTodos").hide();
    }
    else if ($("#chkSegmentado").is(':checked')) {
        $("#divTodos").show();
    }

    if ($("#chkEmpresa").is(':checked')) {
        $("#ddlEmpresas").show();
        $("#ddlMarcas").hide();
        $("#ddlComercios").hide();
    } else if ($("#chkMarca").is(':checked')) {
        $("#ddlMarcas").show();
        $("#ddlEmpresas").hide();
        $("#ddlComercios").hide();
    } else if ($("#chkComercio").is(':checked')) {
        $("#ddlComercios").show();
        $("#ddlMarcas").hide();
        $("#ddlEmpresas").hide();
    }
}

function configControls() {
    configDatePicker();
    $('#formEdicion').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
   
}

function guardar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formEdicion').valid()) {
        var sexo = "";
        var edades = "";
      
        if ($("#rdbTodos").is(':checked'))
            sexo = "T";
        else if ($("#rdbFemenino").is(':checked'))
            sexo = "F";
        else if ($("#rdbMasculino").is(':checked'))
            sexo = "M";

        if ($("#chk1").is(':checked'))
            edades += "0-18;";
        if ($("#chk2").is(':checked'))
            edades += "19-25;";
        if ($("#chk3").is(':checked'))
            edades += "26-30;";
        if ($("#chk4").is(':checked'))
            edades += "31-40;";
        if ($("#chk5").is(':checked'))
            edades += "41-50;";
        if ($("#chk6").is(':checked'))
            edades += "51-60;";
        if ($("#chk7").is(':checked'))
            edades += "61+;";
        if ($("#chk0").is(':checked'))
            edades = "Todas";

        if ($("#chk1").is(':checked') && $("#chk2").is(':checked') && $("#chk3").is(':checked') && $("#chk4").is(':checked') && $("#chk5").is(':checked') && $("#chk6").is(':checked') && $("#chk7").is(':checked'))
            edades = "Todas";

        var tipo = "comercio";
        var id =   $("#ddlComercios").val();
        if ($("#chkEmpresa").is(':checked')) {
            tipo = "empresa";
            id = $("#ddlEmpresas").val();
        } else if ($("#chkMarca").is(':checked')) {
            tipo = "marca";
            id = $("#ddlMarcas").val();
        }
        if (edades == "" ) {
            $("#lblError").show();
            $("#lblError").html("Debe seleccionar un rango de edades, o todas");
        }
        else {
            $("#lblError").hide();

            var info = "{ id: " + parseInt($("#hdnID").val())
                + ", tipo: '" + tipo
                + "', idTipo: " + id
                + ", fechaDesde: '" + $("#txtFechaDesde").val()
                + "', fechaHasta: '" + $("#txtFechaHasta").val()
                + "', sexo: '" + sexo
                + "', edad: '" + edades
                + "', multPuntos: " + parseInt($("#txtMultPuntos").val())
                + "}";

            $.ajax({
                type: "POST",
                url: "Beneficiose.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    window.location.href = "Beneficios.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
    else {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    
}


$(document).ready(function () {
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtMultPuntos").numeric();

    configControls();
    changeCheck();
});
