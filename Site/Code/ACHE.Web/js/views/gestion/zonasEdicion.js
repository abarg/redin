﻿function grabar() {
    $("#divError").hide();
    $("#divOk").hide();
    $('#formEdicion').validate();

    if ($('#formZona').valid()) {
        var info = "{ IDZona: '" + $("#hdnIDZona").val()
            + "', Nombre: '" + $("#txtNombre").val()

            + "', IDCiudad: " + parseInt($("#cmbCiudad").val())
                + ", IDProvincia: " + parseInt($("#cmbProvincia").val())
            + "}";

        $.ajax({
            type: "POST",
            url: "zonasEdicion.aspx/grabar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //$('#divOk').show();
                //$("#divError").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

                window.location.href = "Zonas.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    $(".chosen").chosen();
});

