﻿function configControls() {
    $("#txtSDS, #txtNombre, #txtTerminal").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });
    
    
    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ID: { type: "integer" },
                        IDComercio: { type: "integer" },
                        FechaBaja: { type: "date" },
                        NombreFantasia: { type: "string" },
                        Tipo: { type: "string" },
                        Anterior: { type: "string" },
                        Nuevo: { type: "string" },
                        RazonSocial: { type: "string" },
                        Observaciones: { type: "string" }
                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "historialTermEstab.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "ID", title: "ID", width: "50px" },
            { field: "NombreFantasia", title: "Comercio", width: "200px" },
            { field: "FechaBaja", title: "Fecha Cambio", format: "{0:dd/MM/yyyy}", width: "100px" },
            { field: "Tipo", title: "Cambio", width: "100px" },
            { field: "Anterior", title: "Anterior", width: "100px" },
            { field: "Nuevo", title: "Nuevo", width: "100px" },
            { field: "Observaciones", title: "Observaciones", width: "200px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Editar' class='editColumn'/></div>" }, title: "Editar", width: "50px" },
            { command: { text: "", template: "<div align='center'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/></div>" }, title: "Eliminar", width: "50px" }
        ]
    });

    $("#grid").delegate(".editColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        
        window.location.href = "historialTermEstabe.aspx?ID=" + dataItem.ID;
    });

    $("#grid").delegate(".deleteColumn", "click", function (e) {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        if (confirm("¿Esta seguro que desea eliminar el item seleccionado?")) {
            $.ajax({
                type: "POST",
                url: "historialTermEstab.aspx/Delete",
                data: "{ id: " + dataItem.ID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filter();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                }
            });
        }
    });
}

function Nuevo() {
    window.location.href = "historialTermEstabe.aspx";
}

function filter() {
    var grid = $("#grid").data("kendoGrid");
    var $filter = new Array();
    
    var nombre = $("#txtNombre").val();
    if (nombre != "") {
        $filter.push({ field: "NombreFantasia", operator: "contains", value: nombre });
    }

    var sds = $("#txtSDS").val();
    if (sds != "") {
        $filter.push({ field: "SDS", operator: "contains", value: sds });
    }

    var terminal = $("#txtTerminal").val();
    if (terminal != "") {
        $filter.push({ field: "Anterior", operator: "contains", value: terminal });
    }

    grid.dataSource.filter($filter);
}

$(document).ready(function () {
    configControls()
});

function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var nombre = ""
    if ($("#txtNombre").val() != null && $("#txtNombre").val() != "") {
        var nombre = $("#txtNombre").val();
    }

    var sds = "";
    if ($("#txtSDS").val() != null && $("#txtSDS").val() != "") {
        sds = $("#txtSDS").val();
    }
    var terminal = "";
    if ($("#txtTerminal").val() != null && $("#txtTerminal").val() != "") {
        terminal = $("#txtTerminal").val();
    }

    var info = "{ SDS: '" + sds
            + "', Nombre: '" + nombre
            + "', Terminal: '" + terminal
            + "'}";

    $.ajax({
        type: "POST",
        url: "HistorialTermEstab.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}