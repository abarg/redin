﻿function cargarMarcas(idFranquicia) {
    $.ajax({
        type: "POST",
        url: "ReasignacionTarjetas.aspx/CargarMarcas",
        data: "{ idFranquicia: " + idFranquicia + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#ddlMarcas").empty();
            $('#ddlMarcas').append($('<option></option>').val("0").html(""));

            $(data.d).each(function () {
                $("#ddlMarcas").append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ":" + thrownError);
        }
    });
}

function reasignar(numeroDesde, numeroHasta, idFranquicia, idMarca, tipo) {
    $("#btnGenerar").attr("disabled", true);
    $("#imgLoading").show();

    var info = "{ numeroDesde: '" + numeroDesde
    + "', numeroHasta: '" + numeroHasta
    + "', idFranquicia: " + idFranquicia
    + ", idMarca: " + idMarca
    + ", tipo: '" + tipo
    + "' }";

    //alert(info);

    var msg = "¿Desea reasignar las tarjetas con número desde " + numeroDesde + " hasta " + numeroHasta + " a la franquicia " + $("#ddlFranquicias option:selected").text() + " y la marca " + $("#ddlMarcas option:selected").text() + " con el tipo " + (tipo == "B" ? "Beneficio" : (tipo == "G" ? "Giftcard" : "CuponIN")) + "?";
    smoke.confirm(msg, function (e) {
        if (e) {
            $("#divError").hide();
            $("#divOk").hide();
            $.ajax({
                type: "POST",
                url: "ReasignacionTarjetas.aspx/ReasignarTarjetas",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#divOK").show();

                    $("#btnGenerar").attr("disabled", false);
                    $("#imgLoading").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);

                    $("#btnGenerar").attr("disabled", false);
                    $("#imgLoading").hide();
                }
            });
            //smoke.alert('"yeah it is" pressed', { ok: "close" });
        } else {
            //smoke.alert('"no way" pressed', { ok: "close" });
        }
    }, { ok: "Aceptar", cancel: "Cancelar" });
}

function validar() {
    var numeroDesde = $("#txtNumeroDesde").val();
    var numeroHasta = $("#txtNumeroHasta").val();
    var idFranquicia = $("#ddlFranquicias").val();
    var idMarca = $("#ddlMarcas").val();
    var tipo = $("#ddlTipo").val();

    var isValid = true;
    var msg = "";

    if (numeroDesde == "") {
        isValid = false;
        msg = "Debe ingresar un numero de tarjeta desde";
    }
    else if (numeroHasta == "") {
        isValid = false;
        msg = "Debe ingresar un numero de tarjeta hasta";
    }
    else if (parseInt(numeroDesde) > parseInt(numeroHasta)) {
        isValid = false;
        msg = "El número desde debe ser menor al número hasta";
    }
    else if (idFranquicia == "0") {
        isValid = false;
        msg = "Debe seleccionar una franquicia";
    }
    else if (idMarca == "0") {
        isValid = false;
        msg = "Debe seleccionar una marca";
    }

    if (!isValid) {
        $("#divError").html(msg + "<br />");
        $("#divError").show();
        return false;
    }
    else {
        $("#divError").hide();
        $("#divError").html("");
        reasignar(numeroDesde, numeroHasta, idFranquicia, idMarca, tipo);
    }
}

$(document).ready(function () {
    $(".numeric").numeric();

    $("#ddlFranquicias").change(function () {
        cargarMarcas($("#ddlFranquicias").val());
    });

    $('#formAltaMasiva').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});