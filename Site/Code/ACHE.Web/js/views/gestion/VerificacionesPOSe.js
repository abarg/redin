﻿function mostrarTablaVerificaciones() {
    if ($("#formVerifPos").valid()) {
        var info = "{ nroReferencia: '" + $("#txtNroDeReferencia").val()
                 + "'}";
        $.ajax({
            type: "POST",
            url: "VerificacionesPOSe.aspx/verificar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == true) {
                    $("#divError").html("El nro de referencia no es correcto");
                    $("#divError").show();
                    $("#divOk").hide();
                    $("#tblVerificacionesPOS").hide();
                } else {
                    $("#divError").hide();
                    $("#divOk").hide();
                    $("#tblVerificacionesPOS").show();
                    GenerarTabla();
                }
            }
        });
    }
}
function guardar() {
    var isValid = true;
    var msg;
    var info = "{ info: '";
    $(".selectVerificaciones").each(function () {
        if ($(this).attr("id") == "cmbEstadoCompras" && ($(this).attr("value") == "")) {
            msg = "El campo Compras es obligatorio"
            isValid = false;
        } else if ($(this).attr("id") == "cmbEstadoGift" && ($(this).attr("value") == "")) {
            msg="El campo Gift es obligatorio"
            isValid=false;
        } else if ($(this).attr("id") == "cmbEstadoCanjes" && ($(this).attr("value") == "")) {
            msg = "El campo Canjes es obligatorio"
            isValid=false;
        } 
        if (!($(this).attr("id") == "chkCalco") && !($(this).attr("id") == "chkDisplay") && !($(this).attr("id") == "chkCalcoPuerta") && !($(this).attr("id") == "chkFolletos") ) {
            info += "-" + $(this).attr("id") + "#" + $(this).attr("value");
        } else {
            info += "-" + $(this).attr("id") + "#" + $(this).is(":checked");
        }

    });

   
    info += "', nroReferencia: '" + $("#txtNroDeReferencia").val()
     + "'}";
    if(isValid){
        $.ajax({
            type: "POST",
            url: "VerificacionesPOSe.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divOk").show();
                $("#tblVerificacionesPOS").hide();
                $("#txtNroDeReferencia").val("");

        },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").hide();
                $("#divOk").hide();
        }
        });
    } else {
        $("#divError").html(msg);
        $("#divError").show();
        $("#divOk").hide();
    }
}

function GenerarTabla() {
 
    var info = "{ nroReferencia: '" + $("#txtNroDeReferencia").val()
                + "'}";
    $.ajax({
        type: "POST",
        url: "VerificacionesPOSe.aspx/generarTabla",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyVerificacionesPOS").html(data.d);
            }
        }
    });
}
$(document).ready(function () {

    $('#formVerifPos').validate({
  
            onkeyup: false,
            errorClass: 'error',
            validClass: 'valid',
            highlight: function (element) {
                $(element).closest('div').addClass("f_error");
            },
            unhighlight: function (element) {
                $(element).closest('div').removeClass("f_error");
            },
            errorPlacement: function (error, element) {
                $(element).closest('div').append(error);
            }
    });

    configDatePicker();
});
