﻿

$(document).ready(function () {
    $(".chosen").chosen();
    $("#txtSDS").numeric();
});

function buscar() {
    limpiarPantalla();
    var comer = $("#ddlComercios").val();
    if (comer == "") {
        $("#divErrorEmptySearch").show();
    }
    else {
        var info = "{ idComer: '" + $("#ddlComercios").val() + "'}";
        $("#hdnIDComercio").val($("#ddlComercios").val());
//        limpiarPantalla();
        $.ajax({
            type: "POST",
            url: "DuplicarComercio.aspx/buscarDatosComercio",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    //generarTablaTarjetas(info);
                    $("#lblSDS").html(data.d.SDS);
                    $("#lblRazonSocial").html(data.d.RazonSocial);
                    $("#lblFantasia").html(data.d.NombreFantasia);
                    $("#lblNomEst").html(data.d.NombreEstablecimiento);
                    //$("#lblNroEst").html(data.d.NroEstablecimiento);
                    $("#lblFranq").html(data.d.Franquicia);
                    $("#lblMarca").html(data.d.Marca);
                    $("#lblTipoDoc").html(data.d.TipoDocumento);
                    $("#lblNroDoc").html(data.d.NroDocumento);
                    $("#lblRubro").html(data.d.Rubro);
                    $("#lblSubRubro").html(data.d.SubRubro);
                    $("#lblTel").html(data.d.Telefono);
                    $("#sds").show();
                    $("#btnDupliComer").show();
                    $("#nombreFantasia").show();
                    $("#datosComercio").show();
                    $("#lblNro").hide();
                    buscarSDS();
                }
                else {
                    $("#divError").html("No existe un comercio con ese nombre");
                    $("#divError").show();
                }
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function buscarSDS() {

    $.ajax({
        type: "POST",
        url: "DuplicarComercio.aspx/buscarSDS",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "")
                $("#txtSDS").val(data.d)
            else
                $("#divError").html("El SDS automatizado es un valor incorrecto");
        }
    });
}

function duplicarComercio() {
    limpiarPantalla();
    var comer = $("#hdnIDComercio").val()
    if (comer != "0") {
        if ( $("#txtNombreFantasia").val() == "") {
            $("#divError").html("El campo Nombre Fantasía no pueden estar vacio");
            $("#divError").show();
        }
        else {

            var info = "{ idComer: '" + comer
            + "', nombreFantasia: '" + $("#txtNombreFantasia").val()
            + "', sds: '" + $("#txtSDS").val()
            + "'}";
            $.ajax({
                type: "POST",
                url: "DuplicarComercio.aspx/duplicarComercio",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    $("#divOk").show();
                    $("#txtNombreFantasia").val("");
                    $("#txtSDS").val("");
                    //$("#txtPOS").val("");
                    $("#hdnIDComercio").val("0");
                    $("#datosComercio").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $("#sds").hide();
                    $("#btnDupliComer").hide();
                    $("#nombreFantasia").hide();
                    $("#datosComercio").hide();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    }
}

function limpiarPantalla() {
    //$("#txtSDS").val("");
    //$("#txtPOS").val("");
    $("#divOk").hide();
    $("#divError").hide();
    $("#divErrorEmptySearch").hide();

}