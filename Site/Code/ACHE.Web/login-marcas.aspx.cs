﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections.Specialized;

public partial class login_marcas : PaginaMarcasBase
{
    protected static string mensaje = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //LogOut
            if (!String.IsNullOrEmpty(Request.QueryString["logOut"]))
            {
                if (Request.QueryString["logOut"].Equals("true"))
                {
                    Session.Remove("CurrentMarcasUser");
                    Session.Remove("Detalle_Fc");
                }
            }
        }
    }

    [WebMethod(true)]
    public static void ingresar(string usuario, string pwd)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosMarcas.Include("Marcas").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
            if (usu != null)
            {
                usu.FechaUltLogin = DateTime.Now;
                dbContext.SaveChanges();
                HttpContext.Current.Session.Remove("CurrentUser");            
                HttpContext.Current.Session["CurrentMarcasUser"] = new WebMarcasUser(
                    usu.IDUsuario, usu.Email, usu.Marcas.Nombre, usu.IDMarca, usu.Usuario, usu.Tipo,
                    usu.Marcas.Color, usu.Marcas.MostrarSoloPOSPropios,
                    usu.Marcas.MostrarSoloTarjetasPropias, usu.Marcas.HabilitarPOSWeb,
                    usu.Marcas.TipoCatalogo == "C", usu.Marcas.HabilitarSMS,
                    usu.Marcas.EnvioMsjBienvenida, usu.Marcas.MsjBienvenida,
                    usu.Marcas.EnvioMsjCumpleanios, usu.Marcas.MsjCumpleanios,
                    usu.Marcas.CostoSMS, usu.Marcas.EnvioEmailRegistroSocio, usu.Marcas.EnvioEmailCumpleanios,usu.Marcas.EnvioEmailRegistroComercio, usu.Marcas.EmailRegistroSocio, usu.Marcas.EmailCumpleanios, usu.Marcas.EmailRegistroComercio);
            }
            else
                throw new Exception("Usuario y/o contraseña incorrecta.");
        }
    }

    [WebMethod(true)]
    public static void RecuperarDatos(string email)
    {

        if (!email.IsValidEmailAddress())
            throw new Exception("Email incorrect.");

        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosMarcas.Where(x => x.Email == email && x.Activo).FirstOrDefault();
            if (usu != null)
            {
                string newPwd = string.Empty;
                newPwd = newPwd.GenerateRandom(6);

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", usu.Usuario);
                replacements.Add("<PASSWORD>", newPwd);

                bool send = EmailHelper.SendMessage(EmailTemplate.RecuperoPwd, replacements, usu.Email, "RedIN: Recupero de contraseña");
                if (!send)
                    throw new Exception("El email con su nueva contraseña no pudo ser enviado.");
                else
                {
                    usu.Pwd = newPwd;
                    dbContext.SaveChanges();
                }
            }
            else
                throw new Exception("El email es inexistente.");
        }
    }
}