﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login-franquicias.aspx.cs" Inherits="login_franquicias" %>

<!DOCTYPE html>
<html lang="en" class="login_page">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>RedIN :: Acceso Franquicias</title>
    
        <link rel="stylesheet" href="<%= ResolveUrl("~/bootstrap/css/bootstrap.min.css") %>" />
        <link rel="stylesheet" href="<%= ResolveUrl("~/css/blue.css") %>" />   
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/qtip2/jquery.qtip.min.css") %>" />
        <link rel="stylesheet" href="<%= ResolveUrl("~/css/style.css") %>" />  
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/keyboard/keyboard.css") %>" />   
        <!--[if lt IE 9]>
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/login.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.actual.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/validation/jquery.validate.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/bootstrap/js/bootstrap.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/keyboard/keyboard.js") %>" charset="UTF-8"></script>
    </head>
<body>
    <div class="login_box">
        <form id="login_form">
            <div class="top_b">RedIN :: Acceso Franquicias</div>
            
            <div align="center">
                <br />
<%--                <img src="img/redinlogo.jpg" alt="RedIN" />--%>
            </div>

            <div class="alert alert-danger alert-login alert-dismissable" id="divError" style="display:none">Usuario y/o contraseña incorrecta.</div> 
            <div class="cnt_b">
                
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-user"></i></span>
                        <input class="form-control input-sm keyboardInput" type="text" id="Usuario" name="Usuario" placeholder="Usuario" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-lock"></i></span>
                        <input class="form-control input-sm keyboardInput" type="Password" ID="Password" name="Password" placeholder="Contraseña" value="" />
                    </div>
                </div>
                <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
            </div>
            <div class="btm_b clearfix">
                <button name="btnLogin_form" class="btn btn-default btn-sm pull-right" type="button" onclick="Login();">Entrar</button>
            </div>
        </form>

        <form id="pass_form" style="display: none">
            <div class="top_b">¿No pudiste ingresar?</div>
            <div class="alert alert-info alert-login" id="divTituloRecuperarDatos">
                Por favor ingrese su email para recibir su usuario y contraseña en su casilla.
            </div>
            <div class="alert alert-danger alert-login alert-dismissable" id="divError2" style="display:none">El email es inexistente.</div> 
            
            <div class="cnt_b">

                <div class="formRow clearfix">
                    <div class="input-group">
                        <span class="input-group-addon input-sm">@</span>
                        <input type="text" id="p_Email" name="p_Email" placeholder="email" class="form-control input-sm" />
                    </div>
                </div>
                <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingPwd" style="display:none" />
            </div>
            <div class="btm_b tac">
                <button name="btnPass_form" class="btn btn-default" type="button" onclick="Recuperar();">Aceptar</button>
            </div>
        </form>

        <div class="links_b links_btm clearfix">
            <span class="linkform"><a href="#pass_form">¿Olvidaste la contraseña?</a></span>
            <span class="linkform" style="display: none"><a href="#login_form" id="lnkVolver">Volver a la pantalla de Login</a></span>
        </div>
    </div>

    <script>
        (function ($) {
            $(document).ready(function () {
                $('body').keyboard({ keyboard: 'azerty', plugin: 'form' });
            })
        })(jQuery);
	</script>
</body>
</html>

