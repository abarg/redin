﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPageFranquicias : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentFranquiciasUser"] != null)
        {
            var oUsuario = (WebFranquiciasUser)Session["CurrentFranquiciasUser"];
            link_theme.Attributes.Add("href", "/css/" + oUsuario.Theme + ".css");
        }
        else
            link_theme.Attributes.Add("href", "/css/blue.css");
            //link_theme.Href = "~/css/blue.css";
    }
}
