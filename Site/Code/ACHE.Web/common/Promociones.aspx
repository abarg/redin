﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Promociones.aspx.cs" Inherits="common_Promociones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Cupón check</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Cupón check</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">El ticket se ha creado correctamente.</div></asp:Literal>
            <asp:Literal runat="server" id="litErrorMail" Visible="false"><div class="alert alert-danger alert-dismissable">El mail no se ha enviado correctamente</div></asp:Literal>
            <form id="formPromo" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Titulo</label>
                            <input type="text" id="txtTitulo" value="" maxlength="12" class="form-control" />                        
                       </div>
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                          <div class="col-sm-2">
                            <label>Estado</label>
                            <asp:DropDownList runat="server" class="form-control" ID="cmbEstado">
                                <asp:ListItem Text="Todos" Value=""  Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Activo" Value="Activo"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="Inactivo"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div runat="server" id="divMarcas"  class="col-sm-4">
                            <label for="cmbMarcas" class="col-lg-2"> Marca</label>
                            <asp:DropDownList runat="server" ID="cmbMarcas" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-sm-md-8">
                             <br>
                                <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                                <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">        
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
       <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/promociones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

