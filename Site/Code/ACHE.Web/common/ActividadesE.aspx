﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ActividadesE.aspx.cs" Inherits="common_ActividadesE" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/actividades.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

   <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/common/Actividades.aspx") %>">Actividades</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Beneficios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

          <form runat="server" id="formActividad" class="form-horizontal" role="form">
                <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
                <br />
                <div class="form-group">
                    <label for="cmbCategorias" class="col-lg-2 control-label"><span class="f_req">*</span>Categoria</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbCategorias" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label for="cmbSubCategorias" class="col-lg-2 control-label"><span class="f_req">*</span>Sub-Categoria</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbSubCategorias" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label for="txtSubCategoria" class="col-lg-2 control-label"><span class="f_req">*</span>Actividad</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtActividad" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>

              <div class="row">


                  <%--NIVELES--%>
                  <div class="col-md-4">
                    <div class="form-group">
                        <label for="addInputNivel" class="col-lg-2 control-label">Niveles</label>
                        <div class="col-lg-4">
                            <a ID="addInputNivel" class="btn btn-primary" >Agregar Nivel</a>
                        </div>
                    </div>


                    <div id="wrapperNiveles"></div>
                    </div>


                  <%--HORARIOS--%>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label for="addInputNivel" class="col-lg-2 control-label">Horarios</label>
                        <div class="col-lg-4">
                            <a ID="addInputHorario" class="btn btn-primary" >Agregar Horario</a>
                        </div>
                 </div>


                <div id="wrapperHorarios"></div>
                </div>



               <%--SEDES--%>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="addInputNivel" class="col-lg-2 control-label">Sedes</label>
                        <div class="col-lg-4">
                            <a ID="addInputSede" class="btn btn-primary" >Agregar Sede</a>
                        </div>
                    </div>


                <div id="wrapperSedes"></div>
                </div>

              </div>   

       


         
       
                   <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdnIDActividad" Value="0" />

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a href="BeneficiosSubCategorias.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

 </asp:Content>
