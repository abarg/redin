﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model.EntityData;
using System.Collections.Specialized;
using System.Globalization;

public partial class common_importarActividades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                cargarCombos();
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    hdnIDFranquicias.Value = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault().IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos()
    {
        try
        {
            bFranquicia bFranquicia = new bFranquicia();
            this.ddlFranquicias.DataSource = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    protected void ImportarActividades(object sender, EventArgs e)
    {

        int cantErrores = 0;
        List<string> errores = new List<string>();

        int idFranquicia = int.Parse(hdnIDFranquicias.Value);

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try { 
            if (flpActividades.HasFile)
            {
                if (flpActividades.FileName.Split('.')[flpActividades.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {
                    #region read&savefile
                    path = Server.MapPath("~/files//importaciones//") + flpActividades.FileName;
                    flpActividades.SaveAs(path);

                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        #endregion

                        #region entidades
                        ActividadSecretariaCategoria ASCEntity;
                        ActividadCategorias ACATEntity;
                        ActividadSubCategorias ASUBEntity;
                        Actividades ACTEntity;
                        ActividadComercio ACEntity;
                        ActividadNiveles ANEntity;
                        ActividadHorarios AHEntity;
                        #endregion

                        using (var dbContext = new ACHEEntities())
                        {

                            for (int i = 0; i < rows.Length; i++)
                            {

                                #region tmpId's
                                int secretariaID = 0;
                                int categoriaID = 0;
                                int subcategoriaID = 0;
                                int actividadID = 0;
                                int nivelID = 0;
                                int horarioID = 0;
                                int sedeID = 0;
                                #endregion

                                if (i > 0)
                                {
                                    string[] dr = rows[i].Split(new char[] { ';' });

                                    if (dr.Length > 6)
                                    {

                                        #region tmpStrings
                                        string tmpSec = dr[0].Trim();
                                        string tmpCat = dr[1].Trim();
                                        string tmpSub = dr[2].Trim();
                                        string tmpAct = dr[3].Trim();
                                        string tmpNiv = dr[4].Trim();
                                        string tmpHor = dr[5].Trim();
                                        string tmpCom = dr[6].Trim();
                                        #endregion

                                        #region secretaria
                                        // SECRETARIA
                                        if (dr[0].Trim() != "")
                                        {

                                            var aux = dbContext.ActividadSecretariaCategoria.Where(x => x.Nombre == tmpSec).FirstOrDefault();
                                            if (aux != null)
                                                secretariaID = aux.IDSecretaria;
                                            else
                                            {

                                                ASCEntity = new ActividadSecretariaCategoria();

                                                ASCEntity.Nombre = tmpSec;

                                                dbContext.ActividadSecretariaCategoria.Add(ASCEntity);

                                                dbContext.SaveChanges();

                                                secretariaID = ASCEntity.IDSecretaria;

                                            }

                                        }
                                        #endregion

                                        // TODO: FIFU SECRETARIA

                                        #region categoria
                                        // CATEGORIA
                                        if (dr[1].Trim() != "")
                                        {


                                            var aux = dbContext.ActividadCategorias.Where(x => x.Categoria == tmpCat).FirstOrDefault();

                                            if (aux != null)
                                                categoriaID = aux.IDCategoria;
                                            else
                                            {

                                                ACATEntity = new ActividadCategorias();

                                                ACATEntity.Categoria = tmpCat;

                                                ACATEntity.IDSecretaria = secretariaID;

                                                dbContext.ActividadCategorias.Add(ACATEntity);

                                                dbContext.SaveChanges();

                                                categoriaID = ACATEntity.IDCategoria;

                                            }

                                        }
                                        #endregion

                                        #region subcategoria
                                        // SUBCATEGORIA
                                        if (dr[2].Trim() != "")
                                        {


                                            var aux = dbContext.ActividadSubCategorias.Where(x => x.SubCategoria == tmpSub).FirstOrDefault();

                                            if (aux != null)
                                                subcategoriaID = aux.IDSubCategoria;
                                            else
                                            {

                                                ASUBEntity = new ActividadSubCategorias();

                                                ASUBEntity.SubCategoria = tmpSub;

                                                ASUBEntity.IDCategoria = categoriaID;

                                                dbContext.ActividadSubCategorias.Add(ASUBEntity);

                                                dbContext.SaveChanges();

                                                subcategoriaID = ASUBEntity.IDSubCategoria;

                                            }

                                        }
                                        #endregion

                                        #region actividad
                                        // Actividad
                                        if (dr[3].Trim() != "")
                                        {


                                            var aux = dbContext.Actividades.Where(x => x.Actividad == tmpAct).FirstOrDefault();

                                            if (aux != null)
                                                actividadID = aux.IDActividad;
                                            else
                                            {
                                                ACTEntity = new Actividades();

                                                ACTEntity.Actividad = tmpAct;

                                                ACTEntity.IDSubCategoria = subcategoriaID;

                                                dbContext.Actividades.Add(ACTEntity);

                                                dbContext.SaveChanges();

                                                actividadID = ACTEntity.IDActividad;


                                            }

                                        }
                                        #endregion

                                        #region nivel
                                        // Nivel

                                        if (dr[4].Trim() != "")
                                        {

                                            var aux = dbContext.ActividadNiveles.Include("Actividades").Where(x => x.Actividades.Actividad == tmpAct && x.Nivel == tmpNiv).FirstOrDefault();

                                            if (aux != null)
                                                nivelID = aux.IDNivel;
                                            else
                                            {
                                                ANEntity = new ActividadNiveles();

                                                ANEntity.Nivel = tmpNiv;

                                                ANEntity.IDActividad = actividadID;

                                                dbContext.ActividadNiveles.Add(ANEntity);

                                                dbContext.SaveChanges();

                                                nivelID = ANEntity.IDNivel;

                                            }

                                        }
                                        #endregion

                                        #region horario
                                        // Horario
                                        if (dr[5].Trim() != "")
                                        {


                                            var aux = dbContext.ActividadHorarios.Include("Actividades").Where(x => x.Actividades.Actividad == tmpAct && x.Horario == tmpHor).FirstOrDefault();
                                            if (aux != null)
                                                horarioID = aux.IDHorario;
                                            else
                                            {

                                                AHEntity = new ActividadHorarios();

                                                AHEntity.Horario = tmpHor;

                                                AHEntity.Dia = "-";

                                                AHEntity.IDActividad = actividadID;

                                                dbContext.ActividadHorarios.Add(AHEntity);

                                                dbContext.SaveChanges();

                                                horarioID = AHEntity.IDHorario;
                                            }

                                        }
                                        #endregion

                                        #region comercio
                                        // Comercio
                                        if (dr[6].Trim() != "")
                                        {

                                            var comercio = dbContext.Comercios.Where(x => x.NombreFantasia == tmpCom).FirstOrDefault();

                                            if (comercio != null)
                                            {

                                                var aux = dbContext.ActividadComercio.Include("Actividades").Where(x => x.Actividades.Actividad == tmpAct && x.IDComercio == comercio.IDComercio).FirstOrDefault();

                                                if (aux != null)
                                                    sedeID = aux.IDComercio;
                                                else
                                                {
                                                    ACEntity = new ActividadComercio();

                                                    ACEntity.IDActividad = actividadID;

                                                    ACEntity.IDComercio = comercio.IDComercio;

                                                    dbContext.ActividadComercio.Add(ACEntity);

                                                    dbContext.SaveChanges();
                                                }


                                            }
                                            else
                                            {

                                                Comercios com = dbContext.Comercios.Where(x => x.IDFranquicia == idFranquicia).FirstOrDefault();

                                                if (com != null)
                                                {

                                                    com.NombreFantasia = tmpCom;


                                                    int idcomercioviejo = com.IDComercio;
                                                    com = duplicarDatosForaneos(com);
                                                    dbContext.Comercios.Add(com);
                                                    dbContext.SaveChanges();
                                                    duplicarPuntoDeVenta(idcomercioviejo, com.IDComercio);
                                                    duplicarAlertas(idcomercioviejo, com.IDComercio);


                                                    cantErrores++;

                                                    errores.Add("No se encontro el comercio " + dr[6] + ". Correspondiente a la actividad " + dr[3] + "por lo que creamos un comercio con este nuevo nombre, recuerde modificar los datos de la direccion");

                                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No se encontro el comercio:", tmpCom);

                                                }
                                                else
                                                {
                                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No se encontro ningun comercio perteneciente a la franquicia:", idFranquicia.ToString());

                                                }
                                            }
                                        }
                                        #endregion


                                    }
                                }
                            }
                        }

                        int totalImportados = rows.Length - cantErrores;

                        if (cantErrores < 1)
                            ShowOk("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                        else
                        {
                            ShowError("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                            devolverErroresAsignacion(errores);
                        }
                    }

                    File.Delete(path);

                }   
            }
            else
            {
                File.Delete(path);
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "First Column?:", "fail");

            }


        }
        catch (Exception ex) {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

    }

    protected void Importar(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";

        if (flpAsignacion.HasFile)
        {
            if (flpAsignacion.FileName.Split('.')[flpAsignacion.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpAsignacion.FileName;
                    flpAsignacion.SaveAs(path);

                    #region datatable
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Actividad");
                    dt.Columns.Add("Nivel");
                    dt.Columns.Add("Horario");
                    dt.Columns.Add("DNI");
                    dt.Columns.Add("Sede");
                    dt.Columns.Add("FechaInscripcion");
                    #endregion

                    // ISO 8859 CON STREAMREADER PARA QUE FUNCIONEN CARACTERES ESPECIALES (Ñ, TILDES, ETC)
                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                    // Use stream
                     StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                     string csvContentStr = sr.ReadToEnd();

                    //string csvContentStr = File.ReadAllText(path);

                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        int errores = 0;
                        int total = 0;

                        #region Armo Datatable

                        for (int i = 0; i < rows.Length; i++)
                        {
                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length > 4)
                                {
                                    #region Armo DataRow
                                    if (dr[1].Trim() != "")
                                    {
                                        total++;
                                        try
                                        {
                                        
                                            DataRow drNew = dt.NewRow();
                                            // si viene un string vacio lo reemplazamos con un guion para encontrar la entidad
                                            drNew[0] = dr[0].Trim(); // actividad
                                            drNew[1] = (dr[1].Trim().Length > 1) ? dr[1].Trim() : "-"; // nivel
                                            drNew[2] = (dr[2].Trim().Length > 1) ? dr[2].Trim() : "-"; // horario
                                            drNew[3] = (dr[3].Trim().Length > 1) ? dr[3].Trim() : "-"; // dni-cuil-cuit..etc
                                            drNew[4] = (dr[4].Trim().Length > 1) ? dr[4].Trim() : "-"; // sede


                                        if (dr[5].Trim().Length == 8) //fecha inscripcion longitud de 8 es valida, 
                                        {
                                            // mm/dd/yyyy to dd/mm/yyyy para DB SAN MARTIN
                                            string tmpFecha = DateTime.ParseExact(dr[5].Trim(), "MM/dd/yy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");

                                            drNew[5] = DateTime.Parse(tmpFecha);

                                        }
                                        else // asignamos año 1900
                                        {
                                            drNew[5] = DateTime.Parse("01/01/1900");
                                        }
                                   


                                        dt.Rows.Add(drNew);
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                            errores++;                                   
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion

                        bool ok = true;

                        #region Importar datatable

                        if (dt.Rows.Count > 0)
                        {
                            EliminarTmp("beneficiarios");
                            try
                            {
                                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                                {
                                    sqlConnection.Open();

                                    // Run the Bulk Insert
                                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                                    {
                                        //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                        bulkCopy.DestinationTableName = " [dbo].[BeneficiariosTmp]";

                                        foreach (DataColumn col in dt.Columns)
                                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                        bulkCopy.WriteToServer(dt);
                                    }
                                }
                                ProcesarTmp("beneficiarios");
                            }
                            catch (Exception ex)
                            {
                                ok = false;
                                ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                            }
                        }


                        if (ok)
                        {

                            int totalOk = dt.Rows.Count - errores;

                            if (errores > 0)
                            {
                                ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }

                            int cantErrores = 0;

                            using (var dbContext = new ACHEEntities())
                            {
                                cantErrores = dbContext.BeneficiariosTmp.Where(x => x.Error.Length > 0).Count();
                            }

                            if (cantErrores > 0)
                            {
                                totalOk = totalOk - cantErrores;
                                var msg = ". Se encontraron <a href=" + "javascript:exportarErrores(" + "'beneficiarios'" + ");" + ">" + cantErrores + " errores</a> en la importación.";

                                ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);

                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }
                            else
                                ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());

                        }
                    }

                    File.Delete(path);

                    #endregion
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    private void EliminarTmp(string tipo)
    {
        using (var dbContext = new ACHEEntities())
        {

            if (tipo == "beneficiarios")
             dbContext.Database.ExecuteSqlCommand("DELETE BeneficiariosTmp", new object[] { });
            else if(tipo == "socios")
             dbContext.Database.ExecuteSqlCommand("DELETE SociosTmp", new object[] { });

        }
    }

    private void ProcesarTmp(string tipo)
    {

        var user = HttpContext.Current.Session["CurrentUser"];

        var idFranquicia = (user != null) ? ddlFranquicias.SelectedValue : hdnIDFranquicias.Value;

        using (var dbContext = new ACHEEntities())
        {
          
            dbContext.Database.CommandTimeout = 1000;

            if (tipo == "beneficiarios")
                dbContext.Database.ExecuteSqlCommand("exec ProcesarBeneficiariosTmp " + int.Parse(idFranquicia), new object[] { });
            else if (tipo == "socios")
                dbContext.Database.ExecuteSqlCommand("exec ProcesarSociosTmp " + int.Parse(idFranquicia), new object[] { });

        }
    }

    protected void ImportarSocios(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";

        if (flpArchivo.HasFile)
        {
            if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    //Obra Social	Apellido y nombres	Cuil	Parentesco	Tipo doc.	N° doc.	Fecha nac.	Sexo
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Affinity");
                    dt.Columns.Add("NroTarjeta");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Apellido");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("FechaNacimiento", typeof(DateTime));
                    dt.Columns.Add("Sexo");
                    dt.Columns.Add("TipoDocumento");
                    dt.Columns.Add("NroDocumento");
                    dt.Columns.Add("Telefono");
                    dt.Columns.Add("Celular");
                    dt.Columns.Add("Empresa");
                    dt.Columns.Add("Observaciones");
                    dt.Columns.Add("Pais");
                    dt.Columns.Add("Provincia");
                    dt.Columns.Add("Ciudad");
                    dt.Columns.Add("Domicilio");
                    dt.Columns.Add("CodigoPostal");
                    dt.Columns.Add("TelefonoDom");
                    dt.Columns.Add("Fax");
                    dt.Columns.Add("PisoDepto");
                    dt.Columns.Add("IDSocio", typeof(Int32));
                    dt.Columns.Add("IDDomicilio", typeof(Int32));
                    dt.Columns.Add("IDTarjeta", typeof(Int32));
                    dt.Columns.Add("IDProvincia", typeof(Int32));
                    dt.Columns.Add("Error");
                    dt.Columns.Add("IDCiudad");
                    dt.Columns.Add("Puntos", typeof(Int32));
                    dt.Columns.Add("latitud");
                    dt.Columns.Add("longitud");

                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        int errores = 0;
                        int total = 0;

                        #region Armo Datatable

                        for (int i = 0; i < rows.Length; i++)
                        {
                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length > 15)
                                {
                                    #region Armo DataRow
                                    if (dr[1].Trim() != "")
                                    {
                                        total++;
                                        try
                                        {

                                            DataRow drNew = dt.NewRow();
                                            if (dr[1].Trim().Length == 3)
                                                drNew[0] = "0" + dr[1].Trim();
                                            else
                                                drNew[0] = dr[1].Trim();
                                            drNew[1] = dr[0].Trim();
                                            drNew[2] = dr[2].Trim();
                                            drNew[3] = dr[3].Trim();
                                            drNew[4] = dr[4].Trim();
                                            if (dr[5] != string.Empty)
                                                try
                                                {
                                                    drNew[5] = DateTime.Parse(dr[5].Trim());
                                                    if (DateTime.Parse(dr[5].Trim()) < DateTime.Parse("01/01/1900"))
                                                        drNew[5] = DateTime.Parse("01/01/1900");
                                                    else if (DateTime.Parse(dr[5].Trim()) > DateTime.Parse("01/01/2100"))
                                                        drNew[5] = DateTime.Parse("01/01/1900");
                                                }
                                                catch (Exception ex)
                                                {
                                                    drNew[5] = DateTime.Parse("01/01/1900");
                                                }
                                            else
                                                drNew[5] = DateTime.Parse("01/01/1900");
                                            if (dr[6].Trim().ToLower().StartsWith("f"))
                                                drNew[6] = "F";
                                            else if (dr[6].Trim().ToLower().StartsWith("m"))
                                                drNew[6] = "M";
                                            else
                                                drNew[6] = "I";
                                            //drNew[6] = dr[6].Trim();

                                            if (dr[7].Trim().Length <= 3)
                                                drNew[7] = dr[7].Trim();
                                            else
                                                drNew[7] = "DNI";

                                            if (dr[8].Trim().Length <= 20)
                                                drNew[8] = dr[8].Trim();
                                            else
                                                drNew[8] = "";
                                            drNew[9] = dr[9].Trim();
                                            drNew[10] = dr[10].Trim();
                                            drNew[11] = dr[11].Trim();
                                            drNew[12] = dr[12].Trim();
                                            drNew[13] = dr[13].Trim();
                                            drNew[14] = dr[14].Trim();
                                            drNew[15] = dr[15].Trim();
                                            //drNew[16] = dr[16].Trim();
                                            if (dr[16].Trim().Length <= 100)//Domicilio
                                                drNew[16] = dr[16].Trim();
                                            else
                                                drNew[16] = dr[16].Trim().Substring(0, 100);

                                            if (dr[17].Trim().Length <= 10)//CP
                                                drNew[17] = dr[17].Trim();
                                            else
                                                drNew[17] = dr[17].Trim().Substring(0, 10);

                                            if (dr[18].Trim().Length <= 50)//tel
                                                drNew[18] = dr[18].Trim();
                                            else
                                                drNew[18] = dr[18].Trim().Substring(0, 50);

                                            if (dr[19].Trim().Length <= 50)//fax
                                                drNew[19] = dr[19].Trim();
                                            else
                                                drNew[19] = dr[19].Trim().Substring(0, 50);

                                            if (dr.Length >= 20)
                                            {

                                                if (dr[20].Trim().Length <= 10)
                                                    drNew[20] = dr[20].Trim();
                                                else
                                                    drNew[20] = "";
                                            }
                                            else
                                                drNew[20] = "";
                                            drNew[21] = 0;
                                            drNew[22] = 0;
                                            drNew[23] = 0;
                                            drNew[24] = 0;
                                            drNew[25] = "";
                                            drNew[26] = "0";
                                            if (!string.IsNullOrEmpty(dr[21].Trim()))
                                                drNew[27] = (int)decimal.Parse(dr[21].Trim());
                                            else
                                                drNew[27] = "0";
                                            if (!string.IsNullOrEmpty(dr[22].Trim()))
                                            {
                                                drNew[28] = dr[22].Trim();
                                                drNew[29] = dr[23].Trim();
                                            }
                                            else
                                            {
                                                drNew[28] = "";
                                                drNew[29] = "";
                                            }

                                            dt.Rows.Add(drNew);
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                            errores++;
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion

                        bool ok = true;

                        #region Importar datatable

                        if (dt.Rows.Count > 0)
                        {
                            EliminarTmp("socios");
                            try
                            {
                                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                                {
                                    sqlConnection.Open();

                                    // Run the Bulk Insert
                                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                                    {
                                        //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                        bulkCopy.DestinationTableName = " [dbo].[SociosTmp]";

                                        foreach (DataColumn col in dt.Columns)
                                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                        bulkCopy.WriteToServer(dt);
                                    }
                                }
                                ProcesarTmp("socios");
                            }
                            catch (Exception ex)
                            {
                                ok = false;
                                ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                            }
                        }


                        if (ok)
                        {

                            int totalOk = dt.Rows.Count - errores;

                            if (errores > 0)
                            {
                                ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }

                            int cantErrores = 0;

                            using (var dbContext = new ACHEEntities())
                            {
                                cantErrores = dbContext.SociosTmp.Where(x => x.Error.Length > 0).Count();
                            }

                            if (cantErrores > 0)
                            {
                                totalOk = totalOk - cantErrores;
                                var msg = ". Se encontraron <a href="+"javascript:exportarErrores("+"'socios'"+");"+">" + cantErrores + " errores</a> en la importación.";

                                ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);

                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }
                            else
                                ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());

                        }

                    }

                    File.Delete(path);


                    #endregion
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    protected void ImportarAsignacionSimple(object sender, EventArgs e)
    {

        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpAsignacion.HasFile)
            {
                if (flpAsignacion.FileName.Split('.')[flpAsignacion.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpAsignacion.FileName;
                    flpAsignacion.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


                    using (var dbContext = new ACHEEntities())
                    {


                        for (int i = 0; i < rows.Length; i++)
                        {


                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });


                                if (dr.Length == 5)
                                {

                                    // columna "3" = DNI
                                    if (dr[3].Trim() != "")
                                    {

                                        string tmpAct = dr[0].Trim(); // ACTIVIDAD
                                        string tmpNiv = dr[1].Trim().Length > 0 ? dr[1].Trim() : "-"; // NIVEL
                                        string tmpHorario = dr[2].Trim(); // HORARIO
                                        string tmpSocioDNI = dr[3].Trim(); // NUMERO DOCUMENTO SOCIO
                                        string tmpSede = dr[4].Trim(); // SEDE
                                          

                                        // BUSCO NIVEL Y ACTIVIDAD
                                        var aux = dbContext.ActividadNiveles.Include("Actividades").Where(x => x.Actividades.Actividad == tmpAct && x.Nivel == tmpNiv).FirstOrDefault();



                                        if (aux != null)
                                        {
                                            // BUSCO HORARIO CORRESPONDIENTE A LA ACTIVIDAD
                                            var auxHorario = dbContext.ActividadHorarios.Where(x => x.IDActividad == aux.IDActividad && x.Horario == tmpHorario).FirstOrDefault();


                                            if (auxHorario != null) {

                                                // BUSCO SEDE CORRESPONDIENTE

                                                var auxComercio = dbContext.Comercios.Where(x => x.NombreFantasia == tmpSede).FirstOrDefault();

                                                if (auxComercio != null)
                                                {

                                                    // BUSCO SOCIO

                                                    Socios Socio;

                                                    Socio = dbContext.Socios.Where(x => x.NroDocumento == tmpSocioDNI).FirstOrDefault();

                                                    if (Socio != null)
                                                    {

                                                        var auxActNiv = dbContext.ActividadSocioNivel.Where(x => x.IDHorario == auxHorario.IDHorario).FirstOrDefault();

                                                        if (auxActNiv == null)
                                                        {

                                                            ActividadSocioNivel actNiv = new ActividadSocioNivel();
                                                            actNiv.IDSocio = Socio.IDSocio;
                                                            actNiv.IDActividad = aux.IDActividad;
                                                            actNiv.IDNivel = aux.IDNivel;
                                                            actNiv.IDHorario = auxHorario.IDHorario;
                                                            actNiv.IDComercio = auxComercio.IDComercio;

                                                            dbContext.ActividadSocioNivel.Add(actNiv);

                                                        }
                                                        else
                                                        {
                                                            error++;

                                                            errores.Add("El socio " + tmpSocioDNI + " ya tenia la misma actividad, nivel y horario asignados.");

                                                        }



                                                    }
                                                    else
                                                    {
                                                        error++;

                                                        errores.Add("No se encontro el socio DNI:" + tmpSocioDNI);

                                                    }


                                                }
                                                else
                                                {
                                                    error++;

                                                    errores.Add("No se encontro la sede "+ tmpSede);

                                                }
                                            }
                                            else
                                            {

                                                error++;

                                                errores.Add("No se encontro el horario " + tmpHorario + ". Correspondiente a la actividad " + tmpAct);

                                            }

                                        }


                                        else
                                        {

                                            error++;

                                            errores.Add("No se encontro el nivel " + tmpNiv + ". Correspondiente a la actividad " + tmpAct);


                                        }


                                    }


                                   
                                }
                                else
                                {

                                    error++;

                                    errores.Add("La estructura del archivo es incorrecta, debe tener 4 columnas");

                                    ShowError("La estructura del archivo es incorrecta, debe tener 4 columnas");
                                }   
                        

                            }
                        }


                        dbContext.SaveChanges();


                    }

                    int totalImportados = rows.Length - error;

                    if (error < 1)
                        ShowOk("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                    else { 
                        ShowError("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                        devolverErroresAsignacion(errores);
                    }
                    File.Delete(path);

                }

                File.Delete(path);


            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }


        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }

    protected void ImportarReparacionFechasNacimiento(object sender, EventArgs e)
    {

        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpReparacion.HasFile)
            {
                if (flpReparacion.FileName.Split('.')[flpReparacion.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpReparacion.FileName;
                    flpReparacion.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


                    using (var dbContext = new ACHEEntities())
                    {


                        for (int i = 0; i < rows.Length; i++)
                        {


                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });


                                if (dr.Length == 2)
                                {
                                    string tmpFechaNacimiento = dr[0].Trim(); // FechaNacimiento
                                    string tmpSocioDNI = dr[1].Trim(); // NUMERO DOCUMENTO SOCIO

                                    // Fecha Nac
                                    if (dr[0].Trim() != "")
                                    {

                            
                                        // BUSCO SOCIO
                                        Socios Socio = dbContext.Socios.Where(x => x.NroDocumento == tmpSocioDNI).FirstOrDefault();


                                        if (Socio != null)
                                            {
                                                try
                                                {
                                                    // mm/dd/yyyy to dd/mm/yyyy para DB SAN MARTIN
                                                    tmpFechaNacimiento = DateTime.ParseExact(tmpFechaNacimiento, "MM/dd/yy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");

                                                    Socio.FechaNacimiento = DateTime.Parse(tmpFechaNacimiento);
                                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "fecha valida:", tmpFechaNacimiento);
                                                    dbContext.SaveChanges();

                                                }
                                               catch (Exception ex)
                                                {

                                                error++;

                                                errores.Add("El socio " + tmpSocioDNI + " tiene asignada una fecha de nacimiento invalida: " + dr[0].Trim());
                                            }


                                          

                                            }
                                            else
                                            {
                                                error++;

                                                errores.Add("No se encontro el socio " + tmpSocioDNI);

                                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "domicilio invalida:", "No se encontro el socio " + tmpSocioDNI);
                                            }

                                    }
                                    else
                                    {

                                        error++;

                                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "socio invalida:", "No se encontro el socio " + tmpSocioDNI);


                                    }


                                }



                            }
                            else
                            {

                                error++;


                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "DNI?:", "fail");


                            }


                        }
                   }


                 

           }

        File.Delete(path);

            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

        devolverErroresAsignacion(errores);

        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }

    protected void ImportarReparacionDomicilios(object sender, EventArgs e)
    {

        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpReparacionCiudades.HasFile)
            {
                if (flpReparacionCiudades.FileName.Split('.')[flpReparacionCiudades.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpReparacionCiudades.FileName;
                    flpReparacionCiudades.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    using (var dbContext = new ACHEEntities())
                    {

                        for (int i = 0; i < rows.Length; i++)
                        {

                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length == 2)
                                {
                                    string tmpCiudad = dr[1].Trim();
                                    string tmpSocioDNI = dr[0].Trim(); // NUMERO DOCUMENTO SOCIO

                                    // DNI
                                    if (tmpSocioDNI != "")
                                    {

                                        // BUSCO SOCIO
                                        Socios Socio = dbContext.Socios.Where(x => x.NroDocumento == tmpSocioDNI).FirstOrDefault();

                                        if (Socio != null)
                                        {
                                            try
                                            {

                                                if (tmpCiudad != "")
                                                {
                                                    // busco ciudad
                                                    var ciudad = dbContext.Ciudades.Where(x => x.Nombre == tmpCiudad).FirstOrDefault();

                                                    if (ciudad != null)
                                                    {

                                                        Socio.Domicilios.Ciudad = ciudad.IDCiudad;
                                                        dbContext.SaveChanges();

                                                    }
                                                    else // no existe la ciudad buscada
                                                    {
                                                        error++;

                                                        errores.Add("La ciudad " + tmpCiudad + " no se encontro en la base de datos");
                                                    }
                                                }
                                                else
                                                {
                                                    Socio.Domicilios.Ciudad = 27887;
                                                }

                                            }
                                            catch (Exception ex)
                                            {

                                                error++;

                                                errores.Add("El socio " + tmpSocioDNI + " tiene asignada una fecha de nacimiento invalida: " + dr[0].Trim());
                                            }

                                        }
                                        else
                                        {
                                            error++;

                                            errores.Add("No se encontro el socio " + tmpSocioDNI);

                                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "domicilio invalida:", "No se encontro el socio " + tmpSocioDNI);
                                        }

                                    }
                                    else
                                    {

                                        // campo dni vacio

                                    }

                                }

                            }
                            else
                            {

                                error++;

                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "DNI?:", "fail");

                            }

                        }
                    }

                }

                File.Delete(path);

            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");
            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

        devolverErroresAsignacion(errores);

        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }

    //protected void ImportarAsignacionActividadSede(object sender, EventArgs e)
    //{

    //    int error = 0;

    //    List<string> errores = new List<string>();

    //    string path = string.Empty;
    //    pnlError.Visible = pnlOK.Visible = false;
    //    //lblResultados.Text = "";
    //    try
    //    {
    //        if (flpAsignacionSedes.HasFile)
    //        {
    //            if (flpAsignacionSedes.FileName.Split('.')[flpAsignacionSedes.FileName.Split('.').Length - 1].ToUpper() != "CSV")
    //                ShowError("Formato incorrecto. El archivo debe ser CSV.");
    //            else
    //            {

    //                path = Server.MapPath("~/files//importaciones//") + flpAsignacionSedes.FileName;
    //                flpAsignacionSedes.SaveAs(path);

    //                string csvContentStr = File.ReadAllText(path);
    //                string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


    //                using (var dbContext = new ACHEEntities())
    //                {


    //                    for (int i = 0; i < rows.Length; i++)
    //                    {


    //                        //Split the tab separated fields (comma separated split commented)
    //                        if (i > 0)
    //                        {
    //                            string[] dr = rows[i].Split(new char[] { ';' });


    //                            if (dr.Length == 2)
    //                            {

    //                                // Sede
    //                                if (dr[1].Trim() != "")
    //                                {


    //                                    string tmpAct = dr[0].Trim(); // Actividad
    //                                    string tmpSede = dr[1].Trim().Length > 0 ? dr[1].Trim() : "-"; // Sede


    //                                    // BUSCO ACTIVIDAD
    //                                    ActividadComercio AC;
    //                                    var actividad = dbContext.Actividades.Where(x => x.Actividad == tmpAct).FirstOrDefault();


    //                                    if (actividad != null)
    //                                    {

    //                                        Comercios Comercio;

    //                                        // BUSCO COMERCIO
    //                                        Comercio = dbContext.Comercios.Where(x => x.RazonSocial == tmpSede).FirstOrDefault();

    //                                        if (Comercio != null)
    //                                        {

    //                                            AC = new ActividadComercio();

    //                                            AC.IDComercio = Comercio.IDComercio;

    //                                            AC.IDActividad = actividad.IDActividad;

    //                                            dbContext.ActividadComercio.Add(AC);

    //                                            dbContext.SaveChanges();


    //                                        }
    //                                        else
    //                                        {
    //                                            error++;

    //                                            errores.Add("No se encontro el comercio " + tmpSede);

    //                                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "domicilio invalida:", "No se encontro el comercio " + tmpSede);
    //                                        }

    //                                    }
    //                                    else
    //                                    {

    //                                        error++;

    //                                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "socio invalida:", "No se encontro la actividad " + tmpAct);


    //                                    }


    //                                }



    //                            }
    //                            else
    //                            {

    //                                error++;


    //                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Length:", "fail");


    //                            }


    //                        }
    //                    }


    //                }

    //            }

    //            File.Delete(path);

    //        }
    //        else
    //        {
    //            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
    //        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
    //        throw ex;

    //    }

    //    devolverErroresAsignacion(errores);

    //    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    //}

    protected void ImportarAsignacionDomicilioSede(object sender, EventArgs e)
    {

        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpAsignacionDomicilio.HasFile)
            {
                if (flpAsignacionDomicilio.FileName.Split('.')[flpAsignacionDomicilio.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpAsignacionDomicilio.FileName;
                    flpAsignacionDomicilio.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


                    using (var dbContext = new ACHEEntities())
                    {


                        for (int i = 0; i < rows.Length; i++)
                        {


                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });



                                if (dr.Length > 4)
                                {

                                    // Sede
                                    if (dr[0].Trim() != "")
                                    {


                                        string tmpSede = dr[0].Trim(); // Sede
                                        string tmpDir = dr[1].Trim().Length > 0 ? dr[1].Trim() : "-"; // Direccion
                                        string tmpLoc = dr[2].Trim(); // Localidad
                                        string tmpLat = dr[3].Trim(); // Latitud
                                        string tmpLon = dr[4].Trim(); // Longitud
                                                                      // string tmpCuit = dr[5].Trim(); // TODO CREAR COMERCIOS

                                        // BUSCO COMERCIO
                                        var Comercio = dbContext.Comercios.Where(x => x.NombreFantasia == tmpSede).FirstOrDefault();


                                        if (Comercio != null)
                                        {

                                            Domicilios Domicilio;

                                            // BUSCO COMERCIO
                                            Domicilio = dbContext.Domicilios.Where(x => x.IDDomicilio == Comercio.IDDomicilio).FirstOrDefault();

                                            if (Domicilio != null)
                                            {

                                                // BUSCO CIUDAD
                                                var Ciudad = dbContext.Ciudades.Where(x => x.Nombre.Contains(tmpLoc)).FirstOrDefault();

                                                if (Ciudad != null)
                                                {


                                                    Domicilio.Domicilio = tmpDir;

                                                    Domicilio.Ciudad = Ciudad.IDCiudad;

                                                    Domicilio.Latitud = tmpLat;

                                                    Domicilio.Longitud = tmpLon;

                                                    dbContext.SaveChanges();


                                                }
                                                else
                                                {
                                                    error++;

                                                    errores.Add("No se encontro la ciudad" + tmpLoc);

                                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "domicilio invalida:", "No se encontro la Ciudad " + tmpLoc);
                                                }





                                            }
                                            else
                                            {
                                                error++;

                                                errores.Add("No se encontro el domicilio " + tmpSede);

                                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "domicilio invalida:", "No se encontro el Domicilio " + tmpSede);
                                            }

                                        }
                                        else
                                        {

                                            error++;

                                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "sde invalida:", "No se encontro la sede " + tmpSede);


                                        }


                                    }



                                }
                                else
                                {

                                    error++;


                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Length:", "fail");


                                }


                            }
                        }


                    }

                }

                File.Delete(path);

            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

        devolverErroresAsignacion(errores);

        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }

    protected void ImportarSociosConAsignacion(object sender, EventArgs e)
    {

        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpAsignacion.HasFile)
            {
                if (flpAsignacion.FileName.Split('.')[flpAsignacion.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpAsignacion.FileName;
                    flpAsignacion.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    ActividadSocioNivel ASNEntity;
                    Socios Socio;

                    using (var dbContext = new ACHEEntities())
                    {


                        // SE RECUPERA IDMARCA PARA BUSCAR TARJETAS CORRESPONDIENTES
                        int idFranquicia = int.Parse(ddlFranquicias.SelectedValue);
                        int idMarca = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia).FirstOrDefault().IDMarca;


                        for (int i = 0; i < rows.Length; i++)
                        {

                            List<ActividadSocioNivel> listActividades = new List<ActividadSocioNivel>();


                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });


                                if (dr.Length > 23)
                                {

                                    // ACTIVIDAD - NIVEL
                                    if (dr[24].Trim() != "")
                                    {


                                        string tmpActividad = dr[24].Trim();

                                        string[] actividadNiveles;
                                        actividadNiveles = tmpActividad.Split(",");
                                        foreach (var actividadNivel in actividadNiveles)
                                        {

                                            ActividadSocioNivel actNiv = new ActividadSocioNivel();

                                            string[] AN = actividadNivel.Split("-");

                                            string tmpAct = AN[0].Trim();
                                            string tmpNiv = AN[1].Trim().Length > 0 ? AN[1].Trim() : "-";


                                            var aux = dbContext.ActividadNiveles.Include("Actividades").Where(x => x.Actividades.Actividad == tmpAct && x.Nivel == tmpNiv).FirstOrDefault();

                                            if (aux != null)
                                            {

                                                actNiv.IDActividad = aux.IDActividad;
                                                actNiv.IDNivel = aux.IDNivel;
                                                listActividades.Add(actNiv);
                                            }
                                            else
                                            {

                                                error++;

                                                errores.Add("No se encontro el nivel " + AN[1] + ". Correspondiente a la actividad " + AN[0]);

                                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "nivel invalida:", tmpNiv.ToString());


                                            }
                                        }

                                    }


                                    // Busco y Asigno Socio Beneficio 
                                    if (dr[8].Trim() != "#?REF!" && dr.Length > 23)
                                    {

                                        string tmpNombre = dr[2].Trim();


                                        string tmpSocioDNI = dr[8].Trim();

                                        Socio = dbContext.Socios.Where(x => x.NroDocumento == tmpSocioDNI).FirstOrDefault();
                                        if (Socio != null)
                                        {
                                            if (listActividades.Any())
                                            {
                                                dbContext.Database.ExecuteSqlCommand("delete from ActividadSocioNivel where IDSocio = " + Socio.IDSocio);

                                                foreach(var LA in listActividades)
                                                {
                                                    ASNEntity = new ActividadSocioNivel();
                                                    ASNEntity.IDActividad = LA.IDActividad;
                                                    ASNEntity.IDNivel = LA.IDNivel;
                                                    ASNEntity.IDSocio = Socio.IDSocio;

                                                    dbContext.ActividadSocioNivel.Add(ASNEntity);

                                                    dbContext.SaveChanges();

                                                }

                                            }
                                        }
                                           
                                        // Intento crear Socio
                                        else if(dr.Length > 23)
                                        {

                                            // Tarjeta
                                            var tmpAffinity = dr[1].Trim();
                                            var tmpNum = dr[0].Trim();
                                            string tmpNumTarjeta = 637117 + tmpAffinity + tmpNum;
                                            // Datos Socio
                                            string tmpApellido = dr[3].Trim();
                                            string tmpEmail = dr[4].Trim();
                                            string tmpCelular = dr[10].Trim();
                                            string tmpLat = dr[17].Trim();
                                            string tmpLon = dr[18].Trim();


                                            // valido y recupero tarjeta
                                            var auxTarjeta = dbContext.Tarjetas.Where(x => x.IDMarca == idMarca && x.Numero.Substring(0, 15).Equals(tmpNumTarjeta)).FirstOrDefault();

                                            // CREO SOCIO SI EXISTE LA TARJETA
                                            if(auxTarjeta != null) { 

                                                Socio = new Socios();

                                                Socio.FechaAlta = DateTime.Now;
                                                Socio.Nombre = tmpNombre;
                                                Socio.Apellido = tmpApellido;
                                                Socio.Email = tmpEmail;

                                                // FECHA NACIMIENTO
                                                if (dr[5] != string.Empty)
                                                    try
                                                    {
                                                        Socio.FechaNacimiento = DateTime.Parse(dr[5].Trim());
                                                        if (DateTime.Parse(dr[5].Trim()) < DateTime.Parse("01/01/1900"))
                                                            Socio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                        else if (DateTime.Parse(dr[5].Trim()) > DateTime.Parse("01/01/2100"))
                                                            Socio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Socio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                    }
                                                else
                                                    Socio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                // SEXO
                                                if (dr[6].Trim().ToLower().StartsWith("f"))
                                                   Socio.Sexo = "F";
                                                else if (dr[6].Trim().ToLower().StartsWith("m"))
                                                   Socio.Sexo = "M";
                                                else
                                                   Socio.Sexo = "I";
                                                Socio.TipoDocumento = "DNI";
                                                Socio.NroDocumento = tmpSocioDNI;
                                                Socio.Celular = tmpCelular;
                                                Socio.EmpresaCelular = "Movistar";



                                                // DOMICILIO
                                                Socio.Domicilios = new Domicilios();

                                                Socio.Domicilios.FechaAlta = DateTime.Now;

                                                Socio.Domicilios.TipoDomicilio = "S";
                                                Socio.Domicilios.Estado = "A";
                                                Socio.Domicilios.Pais = "ARGENTINA";

                                                // Busco Ciudad

                                                var tmpCity = dr[15].Trim();
                                                var auxCity = dbContext.Ciudades.Where(x => x.Nombre == tmpCity).FirstOrDefault();

                                                if(auxCity != null)
                                                {
                                                    Socio.Domicilios.Provincia = 1;
                                                    Socio.Domicilios.Ciudad = auxCity.IDCiudad;
                                                }
                                                else
                                                {
                                                    error++;

                                                    errores.Add("No se encontro la ciudad " + tmpCity);


                                                }


                                                Socio.Domicilios.Domicilio = null;
                                                Socio.Domicilios.CodigoPostal = null;
                                                Socio.Domicilios.Telefono = null;
                                                Socio.Domicilios.Fax = null;
                                                Socio.Domicilios.PisoDepto = null;
                                                Socio.Domicilios.Entidad = "S";
                                                Socio.Domicilios.Latitud = tmpLat;
                                                Socio.Domicilios.Longitud = tmpLon;


                                                if (listActividades.Any())
                                                {
                                                    Socio.ActividadSocioNivel = listActividades;
                                                }


                                                dbContext.Socios.Add(Socio);


                                                dbContext.SaveChanges();


                                                // ASIGNO  TARJETA                

                                                auxTarjeta.IDSocio = Socio.IDSocio;

                                                dbContext.SaveChanges();


                                            }
                                            else
                                            {

                                                error++;

                                                errores.Add("El numero de tarjeta "+ tmpNum + "es invalido. De la marca id "+idMarca);


                                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "tarjeta invalida:", tmpNumTarjeta.ToString() + idMarca.ToString());

                                            }

                                        }
                                    }
                                    else
                                    {

                                        error++;

                                        errores.Add("El socio " + dr[3] + " " + dr[4] + " no tiene numero de DNI asignado");


                                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "DNI?:", "fail");


                                    }




                                }


                            }
                        }


                    }

                }

            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "First Column?:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

        devolverErroresAsignacion(errores);

        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }

    public static Comercios duplicarDatosForaneos(Comercios comer)
    {

        comer = duplicarDomicilio(comer);
        comer = duplicarDomicilioFiscal(comer);
        return comer;
    }

    public static void duplicarPuntoDeVenta(int idViejo, int idNuevo)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usuComer = dbContext.PuntosDeVenta.Where(x => x.IDComercio == idViejo).ToList();
            foreach (var usu in usuComer)
            {
                usu.IDComercio = idNuevo;
                dbContext.PuntosDeVenta.Add(usu);
            }
            dbContext.SaveChanges();
        }
    }

    public static void duplicarAlertas(int idViejo, int idNuevo)
    {
        //using (var dbContext = new ACHEEntities())
        //{
        //    var usuComer = dbContext.Alertas.Where(x => x.IDComercio == idViejo).ToList();
        //    foreach (var usu in usuComer)
        //    {
        //        usu.IDComercio = idNuevo;
        //        dbContext.Alertas.Add(usu);
        //    }
        //    dbContext.SaveChanges();
        //}
    }

    public static Comercios duplicarDomicilio(Comercios comer)
    {
        using (var dbContext = new ACHEEntities())
        {
            var domicilio = dbContext.Domicilios.Where(x => x.IDDomicilio == comer.IDDomicilio).FirstOrDefault();
            if (domicilio != null)
            {
                dbContext.Domicilios.Add(domicilio);
                dbContext.SaveChanges();
                comer.IDDomicilio = domicilio.IDDomicilio;
            }
            return comer;
        }
    }

    public static Comercios duplicarDomicilioFiscal(Comercios comer)
    {
        using (var dbContext = new ACHEEntities())
        {
            var domicilioFiscal = dbContext.Domicilios.Where(x => x.IDDomicilio == comer.IDDomicilioFiscal).FirstOrDefault();
            if (domicilioFiscal != null)
            {
                dbContext.Domicilios.Add(domicilioFiscal);
                dbContext.SaveChanges();
                comer.IDDomicilioFiscal = domicilioFiscal.IDDomicilio;
            }
            return comer;
        }
    }

    [WebMethod(true)]
    public static string obtenerErrores(string tipo)
    {
        var html = string.Empty;
   
        using (var dbContext = new ACHEEntities())
        {
          
            if(tipo == "beneficiarios")
            { 
               var list = dbContext.BeneficiariosTmp.Where(x => x.Error.Length > 0).ToList();

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.DNI + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";

            }
            else if(tipo == "socios")
            { 
                var list = dbContext.SociosTmp.Where(x => x.Error.Length > 0).ToList();

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.NroDocumento + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                 html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";

            }

        }      

        return html;
    }

    [WebMethod(true)]
    public static string devolverErroresAsignacion(List<string> errores)
    {

        var html = string.Empty;

        if (errores.Any())
        {
            foreach (var detalle in errores)
            {
                html += "<tr>";
                html += "<td>" + detalle + "</td>";
                html += "</tr>";
            }
        }
        else
            html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";


        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", html.ToString());

        return html;
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }

    [WebMethod(true)]
    public static string exportarErrores(string tipo)
    {

        string fileName = "errores " + tipo;
        string path = "/tmp/";

        try
        {
            DataTable dt = new DataTable();
            using (var dbContext = new ACHEEntities())
            {

                if (tipo == "beneficiarios")
                {
                    IEnumerable<BeneficiariosTmp> info;

                    info = dbContext.BeneficiariosTmp.Where(x => x.Error.Length > 0).AsEnumerable();

                    dt = info.Select(x => new
                    {
                        Ciudadano = x.DNI,
                        Error = x.Error,
                    }).ToList().ToDataTable();

                }
                else if( tipo == "socios")
                {

                    IEnumerable<SociosTmp> info;

                    info = dbContext.SociosTmp.Where(x => x.Error.Length > 0).AsEnumerable();

                    dt = info.Select(x => new
                    {
                        Ciudadano = x.NroDocumento,
                        Error = x.Error,
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }        

    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new ClosedXML.Excel.XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}