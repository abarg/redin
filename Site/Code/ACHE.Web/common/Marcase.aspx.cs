﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;  

public partial class common_Marcase : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            this.txtArancel.Text = "3";
            Session["CurrentLogo"] = null;
            Session["CurrentLogoApp"] = null;
            Session["CurrentLogoTicket"] = null;
            Session["CurrentHeaderLanding"] = null;

            CargarPaises();
            CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["IDMarca"])) {
                this.hdnID.Value = Request.QueryString["IDMarca"];

                if (!this.hdnID.Value.Equals("0")) {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDMarca"]));
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void CargarPaises()
    {

        var paises = Common.LoadPaises();
        if (paises != null)
        {
            ddlPais.DataSource = paises;
            ddlPais.DataTextField = "Nombre";
            ddlPais.DataValueField = "ID";
            ddlPais.DataBind();

        }


    }

    private void CargarCombos() {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
            ddlFranquicias.Enabled = false;
        }
        else {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            ddlProvincia.DataSource = Common.LoadProvincias();
            ddlProvincia.DataTextField = "Nombre";
            ddlProvincia.DataValueField = "ID";
            ddlProvincia.DataBind();
        }
        //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));"
    }

    public static List<Combo2ViewModel> LoadCiudades(int idProvincia) {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        using (var dbContext = new ACHEEntities()) {
            entities = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia)
                .Select(x => new Combo2ViewModel() {
                    ID = x.IDCiudad,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }
        return entities;
    }
    
    [WebMethod(true)]
    public static List<Combo2ViewModel> LoadComercios(int idMarca)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Comercios.Where(x => x.IDMarca == idMarca)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDComercio,
                    Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio
                }).OrderBy(x => x.Nombre).ToList();
        }
        return entities;
    }

    private void cargarDatos(int id) {
        try {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.Marcas.Where(x => x.IDMarca == id).FirstOrDefault();

                if (entity != null) {
                    var domicilio = dbContext.Domicilios.Where(x => x.IDDomicilio == entity.IDDomicilio).FirstOrDefault();
                    this.txtNombre.Text = entity.Nombre;
                    this.txtPrefijo.Text = entity.Prefijo;
                    this.txtCodigo.Text = entity.Codigo;
                    this.txtAffinity.Text = entity.Affinity;
                    this.txtArancel.Text = entity.POSArancel.ToString().Replace(",", ".");
                    this.ddlColor.SelectedValue = entity.Color;
                    this.ddlMoneda.SelectedValue = entity.Moneda;
                    this.chkMostrarSoloPOSPropios.Checked = entity.MostrarSoloPOSPropios;
                    this.chkMostrarSoloTarjetasPropias.Checked = entity.MostrarSoloTarjetasPropias;
                    this.chkPosWeb.Checked = entity.HabilitarPOSWeb;
                    this.chkGiftcardWeb.Checked = entity.HabilitarGiftcard;
                    this.chkCuponIN.Checked = entity.HabilitarCuponIN;
                    this.chkProductos.Checked = entity.MostrarProductos;
                    this.chkSMS.Checked = entity.HabilitarSMS;
                    this.txtCostoSMS.Text = entity.CostoSMS.ToString();
                    this.chkSMSBienvenida.Checked = entity.EnvioMsjBienvenida;
                    this.txtSMSBienvenida.Text = entity.MsjBienvenida;
                    this.chkSMSCumpleanios.Checked = entity.EnvioMsjCumpleanios;
                    this.txtSMSCumpleanios.Text = entity.MsjCumpleanios;
                    this.txtFechaTopeCanje.Text = entity.fechaTopeCanje != null ? entity.fechaTopeCanje.Value.ToString("dd/MM/yyyy") : "";
                    this.txtFechaCaducidad.Text = entity.fechaCaducidad != null ? entity.fechaCaducidad.Value.ToString("dd/MM/yyyy") : "";
                    this.txtCuitEmisor.Text = entity.CuitEmisor;
                    this.chkSeFactura.Checked = entity.SeFactura;

                    if (entity.EnvioEmailRegistroSocio) {
                        this.chkEmailRegistroASocio.Checked = true;
                        this.txtEmailBienvenidaASocio.Text = entity.EmailRegistroSocio;
                    }

                    if (entity.EnvioEmailCumpleanios) {
                        this.chkEmailCumpleanios.Checked = true;
                        this.txtEmailCumpleanios.Text = entity.EmailCumpleanios;
                    }

                    if (entity.EnvioEmailRegistroComercio) {
                        this.chkEmailAComercio.Checked = true;
                        this.txtEmailAComercio.Text = entity.EmailRegistroComercio;
                    }

                    this.txtEmailAlerta.Text = entity.EmailAlertas;
                    this.txtCelularAlerta.Text = entity.CelularAlertas;
                    this.txtCelularEmpresaAlerta.SelectedValue = entity.EmpresaCelularAlertas;
                    this.txtRazonSocial.Text = entity.RazonSocial;
                    this.ddlIVA.SelectedValue = entity.CondicionIva;
                    this.ddlTipoDoc.SelectedValue = entity.TipoDocumento;
                    this.txtNroDoc.Text = entity.NroDocumento;
                    this.txtNroComprobante.Text = entity.NroComprobante;
                    this.txtPuntoDeVenta.Text = entity.PuntoDeVenta;
                    this.cmbTipoComprobante.SelectedValue = entity.TipoComprobante;
                    if (domicilio != null) {
                        this.ddlProvincia.SelectedValue = domicilio.Provincia.ToString();
                        this.txtDomicilio.Text = domicilio.Domicilio;
                        ddlCiudad.DataSource = Common.LoadCiudades(domicilio.Provincia);
                        ddlCiudad.DataTextField = "Nombre";
                        ddlCiudad.DataValueField = "ID";
                        ddlCiudad.DataBind();
                        ddlCiudad.Items.Insert(0, new ListItem("", ""));
                        this.ddlCiudad.SelectedValue = domicilio.Ciudad.ToString();
                        this.txtPisoDepto.Text = domicilio.PisoDepto;
                        this.txtCodigoPostal.Text = domicilio.CodigoPostal;
                        this.txtTelefonoDom.Text = domicilio.Telefono;
                    }

                    var comercios = dbContext.Comercios
                        .Include("Domicilios")
                        .Where(x => x.IDMarca == id)
                    .Select(x => new Combo2ViewModel() {
                        ID = x.IDComercio,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio
                    }).OrderBy(x => x.Nombre).ToList();
                    this.cmbComercioSMS.DataSource = comercios;
                    this.cmbComercioSMS.DataValueField = "ID";
                    this.cmbComercioSMS.DataTextField = "Nombre";
                    this.cmbComercioSMS.DataBind();

                    this.cmbComercioSMS.Items.Insert(0, new ListItem("", ""));
                    this.cmbComercioSMS.SelectedValue = entity.IDComercioFacturanteSMS.ToString() ?? "";
                    this.txtCostoPlusin.Text = entity.CostoPlusin.ToString();
                    this.txtCostoSeguro.Text = entity.CostoSeguro.ToString();
                    this.txtCostoTransaccional.Text = entity.CostoTransaccional.ToString();
                    this.txtCostoSMS2.Text = entity.CostoSMS2.ToString();
                    if (entity.IDFranquicia.HasValue)
                        this.ddlFranquicias.SelectedValue = entity.IDFranquicia.Value.ToString();
                    this.ddlCatalogo.SelectedValue = entity.TipoCatalogo;
                    this.ddlTipoTarjeta.SelectedValue = entity.TipoTarjeta != null? entity.TipoTarjeta :"REDIN" ;
                    if (!string.IsNullOrEmpty(entity.Logo)) {
                        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=franquicias&file=" + entity.Logo;
                        else
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.Logo;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.Logo + "', 'divLogo')");
                        divLogo.Visible = true;

                        imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                    }
                    else
                        imgLogo.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";


                    

                    //POS
                    this.chkFormaPago.Checked = entity.POSMostrarFormaPago;
                    this.chkNroTicket.Checked = entity.POSMostrarNumeroTicket;

                    this.txtFooter1.Text = entity.POSFooter1;
                    this.txtFooter2.Text = entity.POSFooter2;
                    this.txtFooter3.Text = entity.POSFooter3;
                    this.txtFooter4.Text = entity.POSFooter4;

                    this.chkFidelidad.Checked = entity.POSMostrarMenuFidelidad;
                    this.chkMenuGift.Checked = entity.POSMostrarMenuGift;
                    this.chkLogo.Checked = entity.POSMostrarLOGO;
                    this.chkChargeGift.Checked = entity.POSMostrarChargeGift;
                    this.allowSMS.Checked = entity.notiShowSMS ?? false;
                    this.allowEmail.Checked = entity.notiShowEmail ?? false;
                    this.promptSurvey.Checked = entity.showPoll ?? false;
                    this.urlGestion.Text = entity.urlGestion;


                    if (!string.IsNullOrEmpty(entity.rutaLogoApp))
                    {
                        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=franquicias&file=" + entity.Logo;
                        else
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.rutaLogoApp;
                        deleteLogoApp.NavigateUrl = "javascript: void(0)";
                        deleteLogoApp.Attributes.Add("onclick", "return deleteLogo('" + entity.rutaLogoApp + "', 'divLogo')");
                        divLogo.Visible = true;

                        logoApp.ImageUrl = entity.rutaLogoApp;
                    }
                    else
                        logoApp.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";


                    if (!string.IsNullOrEmpty(entity.rutaLogoTicket))
                    {
                        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=franquicias&file=" + entity.rutaLogoTicket;
                        else
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.rutaLogoTicket;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.rutaLogoTicket + "', 'divLogo')");
                        divLogo.Visible = true;

                        logoTicket.ImageUrl = entity.rutaLogoTicket;
                    }
                    else
                        logoTicket.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
                    
                    //LANDING PAGE
                    var landing = dbContext.LandingAdmin.Where(x => x.IdMarca == id).FirstOrDefault();

                    if (landing != null )
                    {
                        if (landing.URL != "")
                        {
                            urlLandingPage.Text = landing.URL;
                        }
                    }
                    else
                    {
                        landing = new LandingAdmin();
                        urlLandingPage.Text = "/" + entity.Nombre.Replace(" ","").ToLower();
                    }

                    if (!string.IsNullOrEmpty(landing.Header))
                    {
                        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                            lnkHeaderLanding.NavigateUrl = "/fileHandler.ashx?type=headers&module=franquicias&file=" + landing.Header;
                        else
                            lnkHeaderLanding.NavigateUrl = "/fileHandler.ashx?type=header&module=admin&file=" + landing.Header;
                        lnkHeaderLandingDelete.NavigateUrl = "javascript: void(0)";
                        lnkHeaderLandingDelete.Attributes.Add("onclick", "return deleteHeaderLanding('" + landing.Header + "', 'divHeaderLanding')");
                        divHeaderLandingActual.Visible = true;

                        imgHeaderLanding.ImageUrl = "/files/Headers/" + landing.Header;
                    }
                    else
                        imgHeaderLanding.ImageUrl = "http://www.placehold.it/800x98/EFEFEF/AAAAAA";

                    //Domicilio Fiscal
                    if (entity.IDDomicilio != null)
                    {
                        try {

                            Domicilios oDomicilioF = entity.Domicilios;
                 

                            var idPais = 0;
                            string pais = oDomicilioF.Pais.ToString().ToLower();

                            if (pais.Length > 0) {

                                var aux = dbContext.Paises.Where(x => x.Nombre.ToLower() == pais).FirstOrDefault();


                                if (aux != null)
                                {
                                    idPais = aux.IDPais;
                                    if (idPais > 0)
                                    {

                                        this.ddlPais.SelectedValue = idPais.ToString();

                                        List<ComboViewModel> listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
                                        ddlProvincia.DataSource = listProvincias;
                                        ddlProvincia.DataTextField = "Nombre";
                                        ddlProvincia.DataValueField = "ID";
                                        ddlProvincia.DataBind();
                                        this.ddlProvincia.SelectedValue = oDomicilioF.Provincia.ToString();
                                    }
                                }

                                this.ddlProvincia.SelectedValue = oDomicilioF.Provincia.ToString();
                                ddlCiudad.DataSource = Common.LoadCiudades(oDomicilioF.Provincia);
                                ddlCiudad.DataTextField = "Nombre";
                                ddlCiudad.DataValueField = "ID";
                                ddlCiudad.DataBind();
                                ddlCiudad.Items.Insert(0, new ListItem("", ""));

                                if (oDomicilioF.Ciudad.HasValue)
                                    ddlCiudad.SelectedValue = oDomicilioF.Ciudad.Value.ToString();
                                this.txtDomicilio.Text = oDomicilioF.Domicilio;
                                this.txtCodigoPostal.Text = oDomicilioF.CodigoPostal;
                                this.txtTelefonoDom.Text = oDomicilioF.Telefono;
                                this.txtPisoDepto.Text = oDomicilioF.PisoDepto;
                            }
                        }
                        catch(Exception e)
                        {
                            var msg = "error";
                            var dom = "seteando valores domicilio de marca";

                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, dom);
                        }
                    }
                    

                }
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int id, string nombre, string prefijo, string affinity, string arancel, string color,
        bool mostrarSoloPOSPropios, bool mostrarSoloTarjetasPropias, bool posWeb, string idFranquicia, string tipoCatalogo, string codigo,
        bool giftcardWeb, bool cuponInWeb, bool productos, bool sms, string costoSMS, bool smsBienvenida, string mensajeBienvenida, bool smsCumpleanios,
        string mensajeCumpleanios, int idComercioSMS, bool envioEmailRegistroSocio, string emailBienvenidaSocio,
        bool envioEmailCumpleanios, string emailCumpleanios, bool envioEmailRegistroComercio, string emailBienvenidoComercio, string emailAlertas, string celularAlertas,
        string celularEmpresaAlertas, string razonSocial, string condicionIVA, string tipoDoc, string nroDoc,
        string puntoDeVenta, string tipoComprobante, string nroComprobante, string costoTransaccional, string costoSeguro,
        /* DOMICILIO */ string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string PisoDepto,
        string costoPlusin, string costoSMS2, string fechaCaducidad, string fechaTopeCanje,string tipoTarjeta,
        bool chkFormaPago, bool chkNroTicket, string footer1, string footer2, string footer3, string footer4, bool chkFidelidad,
        bool chkMenuGift, bool chkChargeGift, bool chkLogo, bool chkSeFactura, string cuitEmisor, string Moneda,
        /* CUSTOM POS */ string ordenMenu, string appColor, bool allowSMS, bool allowEmail, string urlGestion, bool promptSurvey,/*LANDING PAGE*/string urlLandingPage)
    {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            try {
                using (var dbContext = new ACHEEntities()) {

                    Marcas entity;
                    if (id > 0)
                        entity = dbContext.Marcas.Where(x => x.IDMarca == id).FirstOrDefault();
                    else {
                        entity = new Marcas();
                        entity.FechaAlta = DateTime.Now;
                        entity.IDMarca = dbContext.Marcas.Max(x => x.IDMarca) + 1;
                    }
                    if(fechaTopeCanje!="")
                    entity.fechaTopeCanje = DateTime.Parse(fechaTopeCanje);
                    if (fechaCaducidad != "")
                        entity.fechaCaducidad = DateTime.Parse(fechaCaducidad);

                    entity.CuitEmisor = cuitEmisor;
                    entity.SeFactura = chkSeFactura;
                    entity.Prefijo = prefijo;
                    entity.Affinity = affinity;
                    entity.Codigo = codigo;
                    entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper() : "";
                    entity.POSArancel =decimal.Parse(arancel.Replace(".",","));
                    entity.Color = color;
                    entity.MostrarSoloPOSPropios = mostrarSoloPOSPropios;
                    entity.MostrarSoloTarjetasPropias = mostrarSoloTarjetasPropias;
                    entity.HabilitarPOSWeb = posWeb;
                    entity.HabilitarGiftcard = giftcardWeb;
                    entity.HabilitarCuponIN = cuponInWeb;
                    entity.MostrarProductos = productos;
                    entity.HabilitarSMS = sms;
                    entity.EnvioMsjBienvenida = smsBienvenida;
                    entity.MsjBienvenida = mensajeBienvenida;
                    entity.EnvioMsjCumpleanios = smsCumpleanios;
                    entity.MsjCumpleanios = mensajeCumpleanios;

                    entity.EnvioEmailRegistroComercio = envioEmailRegistroComercio;
                    entity.EmailRegistroSocio = emailBienvenidaSocio;
                    entity.EnvioEmailCumpleanios = envioEmailCumpleanios;
                    entity.EmailCumpleanios = emailCumpleanios;
                    entity.EnvioEmailRegistroSocio = envioEmailRegistroSocio;
                    entity.EmailRegistroComercio = emailBienvenidoComercio;

                    //Facturacion
                    entity.RazonSocial = razonSocial != null && razonSocial != "" ? razonSocial.ToUpper() : "";
                    entity.CondicionIva = condicionIVA;

                    var msg = "Moneda";
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, Moneda);

                    entity.TipoDocumento = tipoDoc;
                    entity.NroDocumento = nroDoc;
                    entity.TipoTarjeta = tipoTarjeta;

                    entity.Moneda = Moneda;


                    //POS
                    entity.POSMostrarFormaPago = chkFormaPago;
                    entity.POSMostrarNumeroTicket = chkNroTicket;
                    entity.POSFooter1 = footer1;
                    entity.POSFooter2 = footer2;
                    entity.POSFooter3 = footer3;
                    entity.POSFooter4 = footer4;
                    entity.POSMostrarMenuFidelidad = chkFidelidad;
                    entity.POSMostrarMenuGift = chkMenuGift;
                    entity.POSMostrarChargeGift = chkChargeGift;
                    entity.POSMostrarLOGO = chkLogo;
                    entity.NroComprobante = nroComprobante;
                    entity.TipoComprobante = tipoComprobante;
                    entity.PuntoDeVenta = puntoDeVenta;
                    entity.notiShowSMS = allowSMS;
                    entity.notiShowEmail = allowEmail;
                    entity.showPoll = promptSurvey;
                    entity.urlGestion = urlGestion;


                    // DOMICILIO FISCAL
                    if (!string.IsNullOrEmpty(Provincia)) {

                        if (entity.IDDomicilio == null) {
                            entity.Domicilios = new Domicilios();
                            entity.Domicilios.FechaAlta = DateTime.Now;
                            entity.Domicilios.Estado = "A";
                        }

                        entity.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : ""; ;
                        entity.Domicilios.TipoDomicilio = "F";
                        entity.Domicilios.Entidad = "M";
                        entity.Domicilios.Provincia = int.Parse(Provincia);
                        if (Ciudad != string.Empty)
                            entity.Domicilios.Ciudad = int.Parse(Ciudad);
                        else
                            entity.Domicilios.Ciudad = null;

                        entity.Domicilios.Domicilio = Domicilio != null && razonSocial != "" ? razonSocial.ToUpper() : "";
                        entity.Domicilios.CodigoPostal = CodigoPostal;
                        entity.Domicilios.Telefono = TelefonoDom;
                        entity.Domicilios.PisoDepto = PisoDepto;
                    }

                    //Costos
                    if (costoTransaccional != "")
                        entity.CostoTransaccional = decimal.Parse(costoTransaccional);
                    else
                        entity.CostoTransaccional = 0;

                    if (costoSeguro != "")
                        entity.CostoSeguro = decimal.Parse(costoSeguro);
                    else
                        entity.CostoSeguro = 0;

                    if (costoPlusin != "")
                        entity.CostoPlusin = decimal.Parse(costoPlusin);
                    else
                        entity.CostoPlusin = 0;
                    if (costoSMS2 != "")
                        entity.CostoSMS2 = decimal.Parse(costoSMS2);
                    else
                        entity.CostoSMS2 = 0;

                    //Alertas
                    entity.EmailAlertas = emailAlertas;
                    entity.CelularAlertas = celularAlertas;
                    entity.EmpresaCelularAlertas = celularEmpresaAlertas;



                    if (idComercioSMS != 0)
                        entity.IDComercioFacturanteSMS = idComercioSMS;
                    else
                        entity.IDComercioFacturanteSMS = null;

                    if (costoSMS != "")
                        entity.CostoSMS = decimal.Parse(costoSMS);
                    else
                        entity.CostoSMS = 0;

                    if (idFranquicia != string.Empty)
                        entity.IDFranquicia = int.Parse(idFranquicia);
                    else
                        entity.IDFranquicia = null;
                    entity.TipoCatalogo = tipoCatalogo;


                    if (appColor != string.Empty)
                        entity.colorApp = appColor;

                    //Landing Page
                    LandingAdmin landing;

                    landing = dbContext.LandingAdmin.Where(x => x.IdMarca == id).FirstOrDefault();

                    if (landing == null)
                    { 
                        landing = new LandingAdmin();
                        dbContext.LandingAdmin.Add(landing);
                    }
                    landing.IdMarca = id;
                    landing.URL = urlLandingPage.Replace(" ","").ToLower();

                    if (HttpContext.Current.Session["CurrentHeaderLanding"] != null && HttpContext.Current.Session["CurrentHeaderLanding"] != "")
                        landing.Header = HttpContext.Current.Session["CurrentHeaderLanding"].ToString();



                    // Orden Menu

                    if (id > 0 && entity.MarcasMenu.Any())
                    {
                        var menu = entity.MarcasMenu.ToList();
                        dbContext.MarcasMenu.RemoveRange(menu);
                    }

                    int posicion = 1;

                    List<MarcasMenu> listMenu = new List<MarcasMenu>();
                    if (ordenMenu != "")
                    {

                        var items = ordenMenu.Split('&');


                        foreach (var i in items)
                        {

                            string menuItem = String.Join("", i.Split('[', ']', '1', '2', '3', '4', '='));


                            MarcasMenu menu = new MarcasMenu();

                            menu.posicion = posicion;
                            menu.Nombre = menuItem;
                            if (id > 0)
                            {
                                menu.IDMarca = id;

                                entity.MarcasMenu.Add(menu);
                            }
                            else
                            {
                                listMenu.Add(menu);
                            }

                            posicion++;
                        }
                    }
                    if (listMenu.Count() > 0)
                    {
                        entity.MarcasMenu = listMenu;
                    }



                    if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                        entity.Logo = HttpContext.Current.Session["CurrentLogo"].ToString();

                    if (HttpContext.Current.Session["CurrentLogoApp"] != null && HttpContext.Current.Session["CurrentLogoApp"] != "")
                        entity.rutaLogoApp = "/files/logos/" + HttpContext.Current.Session["CurrentLogoApp"].ToString();

                    if (HttpContext.Current.Session["CurrentLogoTicket"] != null && HttpContext.Current.Session["CurrentLogoTicket"] != "")
                        entity.rutaLogoTicket = "/files/logos/" + HttpContext.Current.Session["CurrentLogoTicket"].ToString();


                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        //creo una alerta 
                        Alertas alerta = new Alertas();
                        alerta.IDMarca = entity.IDMarca;
                        alerta.CantTR = 1;
                        alerta.Activa = true;
                        alerta.Prioridad = "A";
                        alerta.Tipo = "Por cantidad";
                        alerta.Restringido = false;
                        alerta.FechaDesde = 1;
                        alerta.Nombre = "default marca";
                        dbContext.Alertas.Add(alerta);

                        dbContext.Marcas.Add(entity);
                        
                        dbContext.SaveChanges();
                    }

                    return entity.IDMarca.ToString();
                }

            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";

    }

    [WebMethod(true)]
    public static void eliminarLogo(int id, string archivo) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var file = "//files//logos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Marcas.Where(x => x.IDMarca == id).FirstOrDefault();
                        if (entity != null) {
                            entity.Logo = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadLogo(object sender, EventArgs e) {
        try {
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif") {

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "marca_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpLogo.SaveAs(path);

                Session["CurrentLogo"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex) {
            Session["CurrentLogo"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    protected void uploadLogoApp(object sender, EventArgs e)
    {
        try
        {
            var extension = flpApp.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpApp.FileName);
                string uniqueName = "marca_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpApp.SaveAs(path);

                Session["CurrentLogoApp"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentLogoApp"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    protected void uploadLogoTicket(object sender, EventArgs e)
    {
        try
        {
            var extension = flpTicket.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpTicket.FileName);
                string uniqueName = "marca_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpTicket.SaveAs(path);

                Session["CurrentLogoTicket"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentLogoTicket"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }


    [WebMethod(true)]
    public static void login() {

    }

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMarca) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.UsuariosMarcas
                    .Where(x => x.IDMarca == idMarca)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel() {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : (x.Tipo == "B" ? "Backoffice" : "Dataentry"),
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaTokens(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMarca)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {


                return dbContext.Database.SqlQuery<PaymentGateawayTokensView>(@"select PGT.IDTokens as IDToken, PGT.PublicKey as publicKey, PGT.PrivateKey as privateKey, PG.Nombre as Pasarela from PaymentGateawayTokens as PGT
                                                                join PaymentGateaways as PG on PG.IDPaymentGateaway = PGT.IDPaymentGateaway
                                                                join Comercios as C on C.IDComercio = PGT.IDComercio
                                                                join Marcas as M on M.IDMarca = C.IDMarca
                                                                where M.IDMarca = " + idMarca + ";").AsQueryable().ToDataSourceResult(take, skip, sort, filter);

            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.UsuariosMarcas.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null) {

                        dbContext.UsuariosMarcas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDMarca, int IDUsuario, string usuario, string email, string pwd, string tipo) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.UsuariosMarcas.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosMarcas.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosMarcas entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosMarcas();
                    else
                        entity = dbContext.UsuariosMarcas.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDMarca = IDMarca;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosMarcas.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarToken(int IDMarca, int IDComercio, string publicKey, string privateKey, string paymentGateaway)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {

                    PaymentGateawayTokens entity;

                    entity = new PaymentGateawayTokens();

                    entity.PublicKey = publicKey;

                    entity.IDComercio = IDComercio;

                    // se hardcodea a mercadolibre temporalmente
                    entity.IDPaymentGateaway = int.Parse(paymentGateaway);

                    // no necesitamos private key en caso de ser crypto
                    if (paymentGateaway == "3")
                        entity.PrivateKey = "-";
                    else
                        entity.PrivateKey = privateKey;

                    dbContext.PaymentGateawayTokens.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Comercios

    /*[WebMethod(true)]
    public static DataSourceResult GetListaGrillaComercios(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMarca) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.MarcasComercios.Include("Comercios").Include("Domicilios")
                    .Where(x => x.IDMarca == idMarca)
                    .OrderBy(x => x.Comercios.NombreFantasia)
                    .Select(x => new {
                        ID = x.IDMarcaComercio,
                        Nombre = x.Comercios.NombreFantasia,
                        NroDocumento = x.Comercios.NroDocumento,
                        CasaMatriz = x.Comercios.CasaMatriz ? "Si" : "No",
                        Domicilio = x.Comercios.Domicilios.Domicilio
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeleteComercio(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.MarcasComercios.Where(x => x.IDMarcaComercio == id).FirstOrDefault();
                    if (entity != null) {

                        dbContext.MarcasComercios.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void agregarComercio(int IDMarca, int IDComercio) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.MarcasComercios.Any(x => x.IDComercio == IDComercio && x.IDMarca == IDMarca))
                        throw new Exception("Ya se encuentra asignado el comercio seleccionado");

                    MarcasComercios entity = new MarcasComercios();
                    entity.IDMarca = IDMarca;
                    entity.IDComercio = IDComercio;

                    dbContext.MarcasComercios.Add(entity);
                    dbContext.SaveChanges();

                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }*/

    #endregion

    #region LandingPage
    [WebMethod(true)]
    public static DataSourceResult GetListaLandingPage(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMarca)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Database.SqlQuery<LandingPageView>(@"select LC.Id as IDLandingControl, LC.Nombre as Campo,LS.Seccion as Seccion, 'True' as Habilitado from LandingCampos as LC
                                                                    inner join LandingMarcasPage as LMP on LC.Id = LMP.IdLandingControl inner join LandingSecciones as LS on LS.Id = LC.IdSeccion
                                                                    where LMP.IdMarca = " + idMarca + 
                                                                    " and LC.Activo = 1 UNION select LC.Id as IDLandingControl, LC.Nombre as Campo, LS.Seccion as Seccion, 'False' as Habilitado from LandingCampos as LC " +
                                                                    " inner join LandingSecciones as LS on LS.Id = LC.IdSeccion where LC.Id not in (select LC.Id from LandingCampos as LC inner join LandingMarcasPage as LMP on LC.Id = LMP.IdLandingControl  where LMP.IdMarca = "+idMarca+" and LC.Activo = 1) and LC.Activo = 1;").AsQueryable().ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void EnableLandingPageControl(int id, int idMarca)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    LandingMarcasPage entity;

                    entity = dbContext.LandingMarcasPage.Where(x => x.IdLandingControl == id).Where(x => x.IdMarca == idMarca).FirstOrDefault();

                    if (entity != null)
                    {
                        dbContext.LandingMarcasPage.Remove(entity);
                    }
                    else
                    {
                        entity = new LandingMarcasPage();
                        entity.IdLandingControl = id;
                        entity.IdMarca = idMarca;
                        entity.UsaSeccion = true;
                        dbContext.LandingMarcasPage.Add(entity);
                    }

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    protected void uploadHeaderLanding(object sender, EventArgs e)
    {
        try
        {
            var extension = flpHeaderLanding.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpHeaderLanding.FileName);
                string uniqueName = "headerLanding_" + this.hdnID.Value + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/headers/"), uniqueName);

                flpHeaderLanding.SaveAs(path);

                Session["CurrentHeaderLanding"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentHeaderLanding"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    [WebMethod(true)]
    public static void eliminarHeaderLanding(int id, string archivo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var file = "//files//headers//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.LandingAdmin.Where(x => x.IdMarca == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Header = null;
                            HttpContext.Current.Session["CurrentHeader"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }

    }
    #endregion
}