﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using ACHE.Model.EntityData;
using ACHE.Model.ViewModels;


public partial class common_LiquidacionesDetalle : System.Web.UI.Page
{
    static int idLiquidacion;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["IDLiquidacion"]))
        {
            this.hfIDLiquidacion.Value = Request.QueryString["IDLiquidacion"];
            idLiquidacion = int.Parse(Request.QueryString["IDLiquidacion"]);
            // cargarConceptos2();
            mostrar();
        }
    }

    private void mostrar()
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
            litTitulo.Text = "Liquidación #" + liq.IDLiquidacion.ToString("#000000");
            litEstado.Text = "Estado: " + liq.Estado;
            litPeriodo.Text = "Desde: " + liq.FechaDesde.ToString("dd/M/yyyy") + " Hasta: " + liq.FechaHasta.ToString("dd/M/yyyy");
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                if (liq.Estado == "EnFranquicia")
                {
                    pnlPaso2.Visible = true;
                }

            }

            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                litFranquicia.Text = "Franquicia: " + liq.Franquicias.NombreFantasia;
                if (liq.Estado == "CasaMatriz")
                {
                    pnlFormLiqDetalle.Visible = true;
                    pnlPaso3.Visible = true;
                }
                if (liq.Estado == "Confirmada")
                {
                    pnlPaso5.Visible = true;
                }

            }

        }
    }

    [WebMethod(true)]
    public static List<string> cargarConceptos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
            if ((HttpContext.Current.Session["CurrentUser"] != null && liq.Estado == "CasaMatriz"))
            {
                List<string> conceptoslist = new List<string>();
                string[] conceptos = new string[5];
                conceptos[0] = "TARJETAS";
                conceptos[1] = "CAPITAS";
                conceptos[2] = "VIATICOS";
                conceptos[3] = "ENCOMIENDAS";
                conceptos[4] = "OTROS";
                var tipos = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacion == idLiquidacion).Select(x => x.Tipo).ToList();
                int i = 0;
                bool hayConceptos = false;
                foreach (var concepto in conceptos)
                {
                    if (!(tipos.Contains(concepto)))
                    {
                        i++;
                        hayConceptos = true;
                        conceptoslist.Add(concepto);
                    }
                }
                if (hayConceptos == false)
                {
                    return null;
                }
                else
                {
                    return conceptoslist;
                }
            }
            else
            {
                return null;
            }
        }

    }

    [System.Web.Services.WebMethod(true)]
    public static void AgregarConcepto(string tipo, int idLiquidacion)
    {
        using (var dbContext = new ACHEEntities())
        {
            LiquidacionDetalle liq = new LiquidacionDetalle();
            liq.IDLiquidacion = idLiquidacion;
            liq.Tipo = tipo;
            liq.SubTotal = 0;
            dbContext.LiquidacionDetalle.Add(liq);
            dbContext.SaveChanges();

        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                var result = dbContext.LiquidacionDetalle
                        .Where(x => x.IDLiquidacion == idLiquidacion).OrderBy(x => x.Tipo)
                        .Select(x => new LiquidacionDetalleViewModel
                        {
                            IDLiquidacionDetalle = x.IDLiquidacionDetalle,
                            Concepto = x.Tipo,
                            SubTotal = x.SubTotal ?? 0
                        }).ToDataSourceResult(take, skip, sort, filter);
                return result;
            }
        }
        else
            return null;
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

    }

    protected void AdjuntarComprobante(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        int idLiquidacion = int.Parse(hfIDLiquidacion.Value);
        if (flpComprobante.HasFile)
        {
            if (flpComprobante.FileName.Split('.')[flpComprobante.FileName.Split('.').Length - 1].ToUpper() != "PDF")
                ShowError("Formato incorrecto. El archivo debe ser pdf.");
            else
            {

                try
                {
                    string ext = System.IO.Path.GetExtension(flpComprobante.FileName);
                    string uniqueName = "transferencias_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                    path = System.IO.Path.Combine(Server.MapPath("~/files/transferencias/"), uniqueName);
                    flpComprobante.SaveAs(path);
                    using (var dbContext = new ACHEEntities())
                    {
                        var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
                        liq.ComprobanteTransferencia = uniqueName;
                        liq.Estado = "Finalizada";
                        dbContext.SaveChanges();
                        Response.Redirect("Liquidaciones.aspx");
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    protected void ConfirmarLiquidacion(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        int idLiquidacion = int.Parse(hfIDLiquidacion.Value);
        if (flpFactura.HasFile)
        {
            if (flpFactura.FileName.Split('.')[flpFactura.FileName.Split('.').Length - 1].ToUpper() != "PDF")
                ShowError("Formato incorrecto. El archivo debe ser pdf.");
            else
            {

                try
                {
                    string ext = System.IO.Path.GetExtension(flpFactura.FileName);
                    string uniqueName = "factura_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                    path = System.IO.Path.Combine(Server.MapPath("~/files/facturas/"), uniqueName);
                    flpFactura.SaveAs(path);
                    using (var dbContext = new ACHEEntities())
                    {
                        var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
                        liq.Factura = uniqueName;
                        liq.Estado = "Confirmada";
                        dbContext.SaveChanges();
                        Response.Redirect("Liquidaciones.aspx");
                    }
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTabla()
    {
        var html = string.Empty;
        using (var dbContext = new ACHEEntities())
        {
            var list = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacion == idLiquidacion).OrderBy(x => x.Tipo).ToList();
            if (list.Any())
            {
                int n = 1;
                foreach (var item in list)
                {

                    html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='irHistorialObs(" + item.IDLiquidacionDetalle + ");'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/><a/></div></td> ";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='irDetalle(" + item.IDLiquidacionDetalle + ");'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Ver' class='editColumn'/><a/></div></td> ";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='EliminarDetalle(" + item.IDLiquidacionDetalle + ");'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='deleteColumn'/><a/></div></td> ";
                    html += "<td>" + item.Tipo + "</td> ";
                    html += "<td>" + (item.SubTotal.HasValue ? item.SubTotal.Value.ToString("N2") : "0") + "</td> ";
                    html += "</tr>";
                    n++;
                }
                html += " <tr><td colspan='4'>&nbsp;</td><td><b>Total: $" + list.Sum(x => x.SubTotal).Value.ToString("N2") + "</b></td>";

            }
            else
            {
                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static void enviarACasaMatriz()
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
            liq.Estado = "CasaMatriz";
            dbContext.SaveChanges();
        }

    }
    [System.Web.Services.WebMethod(true)]
    public static void enviarAlFranquiciado()
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == idLiquidacion).FirstOrDefault();
            liq.Estado = "EnFranquicia";
            dbContext.SaveChanges();
        }
    }
    [System.Web.Services.WebMethod(true)]
    public static void EliminarDetalle(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == id).FirstOrDefault();
            if (liq.LiquidacionDetalleTipo.Any())
            {
                var list = liq.LiquidacionDetalleTipo.ToList();
                dbContext.LiquidacionDetalleTipo.RemoveRange(list);
            }
            if (liq.LiquidacionesObservaciones.Any())
            {
                var list = liq.LiquidacionesObservaciones.ToList();
                dbContext.LiquidacionesObservaciones.RemoveRange(list);
            }
            dbContext.LiquidacionDetalle.Remove(liq);
            dbContext.SaveChanges();
        }
    }

}