﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ListadoBeneficiarios.aspx.cs" Inherits="common_ListadoBeneficiarios" %>


<asp:Content ID="Content4" ContentPlaceHolderID="HeadContent" Runat="Server">
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li class="last">Socios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de Benefeciarios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form id="formSocio" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMultimarca" Value="0" />
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">

                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>
					    </div>


                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>
					    </div>

					    <div class="col-sm-3">
						    <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control"/>
					    </div>

                       <div class="col-sm-3">
						    <label>Apellido</label>
                            <input type="text" id="txtApellido" value="" maxlength="100" class="form-control"/>
					    </div>

					    <div class="col-sm-3">
						    <label>Nro. de Documento</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
					    </div>

                        <div id="div1" runat="server" class="col-sm-3 marca">
                            <label>Localidad</label>
                            <asp:DropDownList data-placeholder="Localidades - Escribi aqui para buscar" multiple="multiple" runat="server" value="" class="form-control chosen" ID="localidades" />
                        </div>

                        <div class="col-sm-3 marca">
                            <label>Sede</label>
                            <asp:DropDownList data-placeholder="Sedes - Escribi aqui para buscar"  runat="server" value="" class="form-control chosen" ID="ddlSedes" />
                        </div>

                        <div class="col-sm-3 marca">
                            <label>Actividad</label>
                            <asp:DropDownList data-placeholder="Actividades - Escribi aqui para buscar" runat="server" value="" class="form-control chosen-select" multiple="multiple" ID="ddlActividades" />
                        </div>

                        
                        <div class="col-sm-3 marca">
                            <label>Horario</label>
                            <asp:DropDownList data-placeholder="Horarios - Escribi aqui para buscar" runat="server" value="" class="form-control chosen-select" multiple="multiple" ID="ddlHorarios" />
                        </div>


				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <div class="btn-group" id="btnMasAccciones" runat="server">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a href="<%= ResolveUrl("~/common/AsociarTarjetas.aspx") %>">Asociar tarjetas</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/Socios-Unificar.aspx") %>">Unificar</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/TransferirPuntos.aspx") %>">Transferir puntos</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/importarSocios.aspx") %>">Importar</a></li>
                                    
						        </ul>
					        </div>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Socios" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>

    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog  modal-lg">
			<div class="modal-content">
				<div class="modal-header">
                 
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
                    <input type="hidden" id="hdnIDSocio" value="0" />
				</div>
                <div class="alert alert-danger alert-dismissable" id="divErrorTr" style="display: none"></div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha Alta</th> 
                                <th>ID</th> 
                                <th>Nombre</th> 
                                <th>Apellido</th> 
                                <th>Email</th> 
                                <th>Beneficio</th> 
                                <th>Nivel</th> 
                                <th>Sub-Categoria</th> 
                                <th>Categoria</th> 
                                <th>Secretaria</th> 
                                <th>Fecha Nacimiento</th> 
                             
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
							
						</tbody>
					</table>
				</div>
         
				<div class="modal-footer">
                      <button class="btn btn-success" type="button" id="btnExportarTr" onclick="exportarTr();">Exportar a Excel</button>
                      <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingTr" style="display:none" />
                      <a href="" id="lnkDownloadTr" download="TrSocios" style="display:none">Descargar</a>
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/beneficiarios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
   <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>