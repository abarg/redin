﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sorteos.aspx.cs" Inherits="common_Sorteos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Sorteos</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Sorteos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <asp:Literal runat="server" ID="litOk" Visible="false"><div class="alert alert-success alert-dismissable"></div></asp:Literal>
            <form id="formSorteo" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-4" id="divMarca" runat="server">
                            <label for="cmbMarcas" class="col-lg-2">Marca</label>
                            <asp:DropDownList runat="server" ID="cmbMarcas" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Titulo</label>
                            <input type="text" id="txtTitulo" value="" maxlength="12" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-sm-2">
                            <label>Estado</label>
                            <asp:DropDownList runat="server" class="form-control" ID="cmbEstado">
                                <asp:ListItem Text="Todos" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Activo" Value="Activo"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="Inactivo"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                   </div> 
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <br>
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/sorteos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
