﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UsuariosDormidos.aspx.cs" Inherits="common_UsuariosDormidos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
            <li><a href="<%= ResolveUrl("~/Home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Usuarios Dormdios</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Usuarios Dormidos</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formComercio" runat="server">
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
            <asp:HiddenField runat="server" ID="hdnIDMarcas" Value="0" />
            <asp:HiddenField runat="server" ID="hdnSexo" Value="" />
            <asp:HiddenField runat="server" ID="hdnEdadDsd" Value="0" />
            <asp:HiddenField runat="server" ID="hdnEdadHst" Value="0" /> 
            <asp:HiddenField runat="server" ID="hdnCantTrans" Value="0" /> 
                                      
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row"> 
                        <div runat="server" id="divFranquicias" class="col-sm-3">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" >
                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                             </asp:DropDownList>
                        </div>
                        <div runat="server" id="divMarcas" class="col-sm-3">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas">
                                <asp:ListItem Text="" Value="0"></asp:ListItem>
                             </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate required" MaxLength="10" />
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-sm-1">
                            <label>Sexo</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlSexo" >
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="M" Value="M"></asp:ListItem>
                                <asp:ListItem Text="F" Value="F"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-1">
                             <label>Edad Desde</label>
                             <asp:TextBox runat="server" ID="txtEdadDesde" Value="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-sm-1">
                             <label>Edad Hasta</label>
                             <asp:TextBox runat="server" ID="txtEdadHasta" Value="" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <label> Cantidad Transacciones Máx.</label>
                            <asp:TextBox runat="server" ID="txtCantidadTransacciones" Value="0" CssClass="form-control" MaxLength="8" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/comercios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/UsuariosDormidos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
