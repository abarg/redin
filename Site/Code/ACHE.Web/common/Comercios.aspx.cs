﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_Comercios : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                btnMasAccciones.Visible = false;
            }
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
                cargarCombos();
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        if (idFranquicia > 0 || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Comercios.Include("Domicilios")
                    .Where(x => idFranquicia == 0 || x.IDFranquicia == idFranquicia)
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new ComerciosViewModel()
                    {
                        IDComercio = x.IDComercio,
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Domicilio = x.Domicilios.Domicilio != null ? x.Domicilios.Domicilio : "",
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        Responsable = x.Responsable,
                        Email = x.Email,
                        IDMarca = x.IDMarca ?? 0,
                        IDFranquicia = x.IDFranquicia,
                        CBU = x.FormaPago_CBU
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
            }
            else
            {
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataSource = listFranquicias;
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();

                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
               listMarcas = bMarca.getMarcas();
            }
       
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            try
            {
                bComercio bComercio = new bComercio();
                bool aux = bComercio.deleteComercio(id);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string SDS, string NombreFantasia, string RazonSocial, string NroDocumento, string CBU, int idMarca, int idFranquicia)
    {
        string fileName = "Comercios";
        string path = "/tmp/";
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.Comercios
                        .Include("Franquicias").Include("Domicilios").Include("DomiciliosFiscal").Include("Marcas")
                        .OrderBy(x => x.NombreFantasia).AsEnumerable();

                    if (NombreFantasia != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(NombreFantasia.ToLower()));
                    if (SDS != "")
                        info = info.Where(x => x.SDS.ToLower().Contains(SDS.ToLower()));
                    if (RazonSocial != "")
                        info = info.Where(x => x.RazonSocial.ToLower().Contains(RazonSocial.ToLower()));
                    if (NroDocumento != "")
                        info = info.Where(x => x.NroDocumento != null && x.NroDocumento.ToLower().Contains(NroDocumento.ToLower()));
                    if (CBU != "")
                        info = info.Where(x => x.FormaPago_CBU != null && x.FormaPago_CBU.ToLower().Contains(CBU.ToLower()));
                    if (idMarca > 0)
                        info = info.Where(x => x.IDMarca.HasValue && x.IDMarca == idMarca);
                    if (idFranquicia > 0)
                        info = info.Where(x => x.IDFranquicia.HasValue && x.IDFranquicia == idFranquicia);

                    dt = info.ToList().Select(x => new
                    {
                        Franquicia = x.IDFranquicia.HasValue ? x.Franquicias.NombreFantasia : "",
                     //   Marca = x.IDMarca.HasValue ? x.Marcas.Nombre : "",
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        Responsable = x.Responsable,
                        Cargo = x.Cargo,
                        CondicionIva = x.CondicionIva,
                        Web = x.Web,
                        Facebook = x.Url,
                        Twitter = x.twitter,
                        Email = x.Email,
                        FechaAlta = x.FechaAlta.ToShortDateString(),
                        Observaciones = x.Observaciones,
                        NombreEst = x.NombreEst,
                        Rubro = x.Rubro.HasValue ? x.Rubros.Nombre : "",
                        SubRubro = x.IDSubRubro.HasValue ? x.SubRubros.Nombre : "",
                        CodigoDealer = x.IDDealer.HasValue ? x.Dealer.Codigo : "",

                        Activo = (x.Activo ? "Si" : "No"),
                        FormaPago = x.FormaPago,
                        FormaPago_Banco = x.FormaPago_Banco,
                        FormaPago_TipoCuenta = x.FormaPago_TipoCuenta,
                        FormaPago_NroCuenta = x.FormaPago_NroCuenta,
                        FormaPago_CBU = x.FormaPago_CBU,
                        FormaPago_Tarjeta = x.FormaPago_Tarjeta,
                        FormaPago_BancoEmisor = x.FormaPago_BancoEmisor,
                        FormaPago_NroTarjeta = x.FormaPago_NroTarjeta,
                        FormaPago_FechaVto = x.FormaPago_FechaVto,
                        FormaPago_CodigoSeguridad = x.FormaPago_CodigoSeg,

                        Zona = x.IDZona.HasValue ? x.Zonas.Nombre : "",
                        Pais = x.Domicilios != null ? x.Domicilios.Pais : "",
                        Provincia = x.Domicilios != null ? x.Domicilios.Provincias.Nombre : "",
                        Ciudad = x.Domicilios != null && x.Domicilios.Ciudad.HasValue ? x.Domicilios.Ciudades.Nombre : "",
                        Domicilio = x.Domicilios != null ? x.Domicilios.Domicilio : "",
                        PisoDepto = x.Domicilios != null ? x.Domicilios.PisoDepto : "",
                        CodigoPostal = x.Domicilios != null ? x.Domicilios.CodigoPostal : "",
                        TelefonoDom = x.Domicilios != null ? x.Domicilios.Telefono : "",
                        Fax = x.Domicilios != null ? x.Domicilios.Fax : "",
                        //PaisFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.Pais : "",
                        //ProvinciaFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.Provincias.Nombre : "",
                        //CiudadFiscal = x.DomiciliosFiscal != null && x.DomiciliosFiscal.Ciudad.HasValue ? x.DomiciliosFiscal.Ciudades.Nombre : "",
                        //DomicilioFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.Domicilio : "",
                        //PisoDeptoFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.PisoDepto : "",
                        //CodigoPostalFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.CodigoPostal : "",
                        //TelefonoDomFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.Telefono : "",
                        //FaxFiscal = x.DomiciliosFiscal != null ? x.DomiciliosFiscal.Fax : "",

                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}