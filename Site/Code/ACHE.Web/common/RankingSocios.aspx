﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="RankingSocios.aspx.cs" Inherits="common_RankingSocios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Ranking Socios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Top Socios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">El ticket se ha creado correctamente.</div></asp:Literal>
            <asp:Literal runat="server" id="litErrorMail" Visible="false"><div class="alert alert-danger alert-dismissable">El mail no se ha enviado correctamente</div></asp:Literal>
            <form id="formPromo" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
					    <div class="col-sm-3">
						    <label>Socio</label>
                            <input type="text" id="txtApellido" value="" maxlength="20" class="form-control"/>
					    </div>
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div runat="server" id="divMarcas"  class="col-sm-3">
                            <label for="cmbMarcas" class="col-lg-2"> Marca</label>
                            <asp:DropDownList runat="server" ID="cmbMarcas" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-sm-md-8">
                             <br>
                                <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                                <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                                <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar</button>
                                <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                                <a href="" id="lnkDownload" download="Terminales" style="display:none">Descargar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">        
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
<%--       <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/promociones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
           <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/rankingSocios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

