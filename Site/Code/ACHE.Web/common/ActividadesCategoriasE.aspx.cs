﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ActividadesCategoriase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        this.cargarSecretarias();

        if (!String.IsNullOrEmpty(Request.QueryString["IDCategoria"]))
        {
            try
            {
                int idCategoria = int.Parse(Request.QueryString["IDCategoria"]);
                if (idCategoria > 0)
                {
                    this.hdnIDCategoria.Value = idCategoria.ToString();
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDCategoria"]));
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ActividadesCategorias.aspx");
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.ActividadCategorias.Where(x => x.IDCategoria == id).FirstOrDefault();
                if (entity != null)
                {

                    this.txtCategoria.Text = entity.Categoria;
                    this.ddlSecretarias.SelectedValue = entity.IDSecretaria.ToString();

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private void cargarSecretarias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var secretarias = dbContext.ActividadSecretariaCategoria.OrderBy(x => x.Nombre).ToList();
            if (secretarias != null)
            {
                ddlSecretarias.DataSource = secretarias;
                ddlSecretarias.DataTextField = "Nombre";
                ddlSecretarias.DataValueField = "IDSecretaria";
                ddlSecretarias.DataBind();
                ddlSecretarias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }


    [WebMethod(true)]
    public static string grabar(int idCategoria, string Categoria, int idSecretaria)
    {
        try
        {

            //Categoria Beneficio
            ActividadCategorias entity;
            using (var dbContext = new ACHEEntities())
            {
                if (idCategoria > 0)
                    entity = dbContext.ActividadCategorias.FirstOrDefault(s => s.IDCategoria == idCategoria);
                else
                    entity = new ActividadCategorias();

                entity.Categoria = Categoria != null && Categoria != "" ? Categoria.Trim().ToUpper() : "";
                entity.IDSecretaria = idSecretaria;

                if (idCategoria > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ActividadCategorias.Add(entity);
                    dbContext.SaveChanges();
                }

            }

            return entity.IDCategoria.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


}