﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Comercios.aspx.cs" Inherits="common_Comercios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Comercios</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formComercio" runat="server">
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
                <div class="formSep col-sm-12 col-md-12">
                   <div class="row">
                        <div class="col-sm-3">
                            <label>Nombre Fantasía</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-sm-3">
                            <label>Razón social</label>
                            <input type="text" id="txtRazonSocial" value="" maxlength="128" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. de Documento</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">
                            <label>SDS</label>
                            <input type="text" id="txtSDS" value="" maxlength="50" class="form-control number" />
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="row"> 

                        <div class="col-sm-3">
                            <label>CBU</label>
                            <input type="text" id="txtCBU" value="" maxlength="30" class="form-control number" />
                        </div>
                        <div class="col-sm-2" style="display:none">
                            <label>Descuento</label>
                            <input type="text" id="txtDescuento" value="" maxlength="3" class="form-control number" />
                        </div>
                        <div class="col-sm-3">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                        </div>
                        <div class="col-sm-3">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <div class="btn-group" id="btnMasAccciones" runat="server">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a href="<%= ResolveUrl("~/modulos/gestion/DuplicarComercio.aspx") %>">Duplicar Comercio</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>">Promociones</a></li>
                                    <li><a href="<%= ResolveUrl("~/modulos/gestion/ArancelPuntosMarcase.aspx") %>">Actualizacion de arancel, puntos y descuentos por marcas</a></li>
                                    <li><a href="<%= ResolveUrl("~/modulos/gestion/Arancel-puntos-empresase.aspx") %>">Actualizacion de arancel, puntos y descuentos por empresas</a></li>
                                    <li><a href="<%= ResolveUrl("~/modulos/gestion/Beneficios.aspx") %>">Beneficios por edad/sexo</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/importarComercios.aspx") %>">Importar</a></li>
						        </ul>
					        </div>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/comercios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>