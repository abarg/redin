﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="importarBeneficiarios.aspx.cs" Inherits="common_importarActividades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <style>
        .form-group:hover{
        background: #cbe49b;
        }
    </style>
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li>Importación de socios-actividades-beneficiarios</li>
            </ul>
        </div>
    </nav>
    <div class="row">

        <%-- INFO --%>
        <div class="alert alert-danger alert-dismissable" id="pnlError" runat="server" visible="false">
            <asp:Literal runat="server" ID="litError"></asp:Literal>
            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
            <a href="" id="lnkDownload" download="Socios" style="display:none">Descargar</a>
        </div>
        <div class="alert alert-success alert-dismissable" id="pnlOK" runat="server" visible="false">
              <asp:Literal runat="server" ID="litOk"></asp:Literal>
        </div>
        <div class="alert alert-success alert-dismissable" id="rowsInfo" hidden>
            <div id="rowsInfoContainer">
            </div>
        </div>

        <form runat="server" id="formImportar" class="form-horizontal" role="form">
            <div runat="server" ID="divFranquicia" class="form-group groupHover">
                <asp:Label Text="Franquicia" runat="server" />
                <asp:DropDownList class="" runat="server" ID="ddlFranquicias"></asp:DropDownList>
            </div>
            <%--SOCIOS--%>
            <h2 class="heading">Paso n.1 - Importacion de socios  &nbsp; &nbsp; <small>En caso de que se necesite cargar nuevos socios/ciudadanos a la base</small></h2>


            <div class="col-md-12" style="margin-bottom: 75px;">
                <asp:HiddenField runat="server" ID="HiddenField1" Value="0" />
                <div class="form-group groupHover">
                    &nbsp; &nbsp; &nbsp; &nbsp;Ejemplo CSV Socios <a href="/files/importarsocios.csv" download>Descargar </a>  
                    <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV Socios</label>

                    <div class="col-lg-3">
                        <asp:FileUpload runat="server" ID="flpArchivo" />
                    </div>
                    <br/><br/>
                    <div class="col-sm-8 col-sm-offset-2">
                        <asp:Button runat="server" ID="btnSocios" CssClass="btn btn-success" Text="Importar Socios" OnClick="ImportarSocios" disabled/>
                    </div>
                    <div class="col-lg-10 pull-right" runat="server" id="Div1" style="padding-left: 135px;">                                                                     
                    </div>
                </div>
            </div>
            <%--  Actividades --%>
            <h2 class="heading">Paso n.2 - Importacion de Actividades  &nbsp; &nbsp; <small>En caso de que se necesite cargar nuevas actividades/beneficios a la base</small></h2>
            <div class="col-md-12" style="margin-bottom: 75px;">
                <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
                <div class="form-group groupHover">
                    &nbsp; &nbsp; &nbsp; &nbsp;Ejemplo CSV Actividades <a href="/files/importarActividades.csv" download>Descargar </a>  
                    <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV Actividades     </label>
                    <div class="col-lg-3">
                        <asp:FileUpload runat="server" ID="flpActividades" />
                    </div>
                    <br/><br/>
                    <div class="col-sm-8 col-sm-offset-2">
                        <asp:Button runat="server" ID="btnUploadActividades" CssClass="btn btn-success" Text="Importar Actividades" OnClick="ImportarActividades" disabled/>
                    </div>
                    <div class="col-lg-10 pull-right" runat="server" id="lnkPremiosPrueba" style="padding-left: 135px;">                                                                     
                    </div>
                </div>
            </div>

            <%--  Asignacion socio-Beneficio --%>
            <h2 class="heading">Paso n.3 - Importacion de Asignacion Socio-Beneficio </h2>
            <div class="form-group groupHover">
                <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV Asignacion beneficiarios     </label>
                &nbsp; &nbsp; &nbsp; &nbsp;Ejemplo CSV Asignacion <a href="/files/importarBeneficiarios.csv" download>Descargar </a>  
                <div class="col-lg-3">
                    <asp:FileUpload runat="server" ID="flpAsignacion" />
                </div>
                <br/><br/>
                <div class="col-sm-8 col-sm-offset-2">
                    <asp:Button runat="server" ID="btnAsignacion" CssClass="btn btn-success" Text="Importar Asignacion Socio-Beneficio" OnClick="Importar" disabled/>
                </div>
                <div class="col-lg-10 pull-right" runat="server" id="Div2" style="padding-left: 135px;">                                                                     
                </div>
            </div>

            <%--TEMPORALES--%>
            <%--   asignacion domicilios    --%>
            <div class="form-group groupHover" style="display: none;" runat="server">
                <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV Asignacion Domicilios Sedes (temporal)</label>
                <div class="col-lg-3">
                    <asp:FileUpload runat="server" ID="flpAsignacionDomicilio" />
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <asp:Button runat="server" ID="Button5" CssClass="btn btn-success" Text="Importar Asignacion Sede-Domicilio" OnClick="ImportarAsignacionDomicilioSede" />
                </div>
            </div>
            <%--reparacion localidades--%>
            <div style="display: none;">
                <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV reparacion localidades</label>
                <div class="col-lg-2">
                    <asp:FileUpload runat="server" ID="flpReparacionCiudades" />
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <asp:Button runat="server" ID="Button3" CssClass="btn btn-success" Text="Importar" OnClick="ImportarReparacionDomicilios" />
                </div>
            </div>
            <%--reparacion fechas de nacimiento--%>
            <div style="display: none;">
                <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV reparacion fechas de nacimiento (temporal)</label>
                <div class="col-lg-2">
                    <asp:FileUpload runat="server" ID="flpReparacion" />
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <asp:Button runat="server" ID="Button1" CssClass="btn btn-success" Text="Importar Reparacion Fechas de nac." OnClick="ImportarReparacionFechasNacimiento" />
                </div>
            </div>
            <%--FIN TEMPORALES--%>
        </form>
    </div>
    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle">Detalle de errores</h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>Ciudadano DNI</th>
                                <th>Error</th>
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/importarBeneficiarios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>