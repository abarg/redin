﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;  
using System.IO;
using System.Windows.Forms;  

public partial class common_VerificacionesPOS : System.Web.UI.Page
{
    private int idProvincia, idCiudad, idZona, idFranq;
    private static string rutaMapa;
    private static bool insertarMapa;

    protected void Page_Load(object sender, EventArgs e)
    {

        int idfranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idfranquicia = usu.IDFranquicia;
            hdnIDFranquicia.Value = idfranquicia.ToString();
            pnlFranquicias.Visible = false;

            cargarProvincias2(idfranquicia);
        }
        else if (HttpContext.Current.Session["CurrentUser"] != null)
            cargarFranquicias();
        else
            Response.Redirect("/login.aspx");

    }

    private void cargarProvincias2(int idFranq)
    {
        using (var dbContext = new ACHEEntities())
        {
            var provincias = dbContext.Comercios.Include("Domicilios").Include("Provincias").Where(x => x.IDFranquicia == idFranq).Select(x => x.Domicilios.Provincias).Distinct().ToList();
            if (provincias != null && provincias.Count() > 0)
            {
                cmbProvincia.DataSource = provincias;
                cmbProvincia.DataValueField = "IDProvincia";
                cmbProvincia.DataTextField = "Nombre";
                cmbProvincia.DataBind();
                cmbProvincia.Items.Insert(0, new ListItem("", "0"));
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarProvincias(int idFranquicia)
    {
        List<ComboViewModel> result = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            var provincias = dbContext.Comercios.Include("Domicilios").Include("Provincias").Where(x => x.IDFranquicia == idFranquicia).OrderBy(x => x.Domicilios.Provincias.Nombre)
                .Select(x => new ComboViewModel()
                {
                    ID = x.Domicilios.Provincia.ToString(),
                    Nombre = x.Domicilios.Provincias.Nombre,
                }).Distinct().ToList();
            if (provincias != null && provincias.Count() > 0)
                result = provincias;
        }
        return result;
    }

    private void cargarFranquicias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var franquicias = dbContext.Franquicias.ToList();
            if (franquicias != null && franquicias.Count() > 0)
            {
                cmbFranquicia.DataSource = franquicias;
                cmbFranquicia.DataValueField = "IDFranquicia";
                cmbFranquicia.DataTextField = "NombreFantasia";
                cmbFranquicia.DataBind();
                cmbFranquicia.Items.Insert(0, new ListItem("", "0"));
            }
        }
    }

    private void CargarComercios(int idZona, int idFranq, string estado)
    {

        using (var dbContext = new ACHEEntities())
        {
            //var com = dbContext.Comercios.Where(x => x.IDFranquicia == idFranq && x.Activo && x.Estado != "DE").AsQueryable();
            //if (estado != "")
            //    com = com.Where(x => x.EstadoCompras == estado);

            var com = dbContext.Comercios.Where(x => x.IDFranquicia == idFranq && x.Activo).AsQueryable();
            if (idZona > 0)
                com = com.Where(x => x.IDZona == idZona);

            var comercios = com.ToList().Select(x => new ComboViewModel { ID = x.IDComercio.ToString(), Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio }).OrderBy(x => x.Nombre).ToArray();

            if(estado != "TODOS")
            {
                var aux = comercios.Select(x => int.Parse(x.ID)).ToArray();
                var terminales = dbContext.Terminales.Where(x => aux.Contains(x.IDComercio) && x.EstadoCompras == estado).GroupBy(i => i.IDComercio).Select(x => new ComboViewModel { ID = x.FirstOrDefault().IDComercio.ToString(), Nombre = x.FirstOrDefault().Comercios.NombreFantasia + " - " + x.FirstOrDefault().Comercios.Domicilios.Domicilio }).ToList();

                if (terminales != null && terminales.Count() > 0)
                {
                    searchable.DataSource = terminales;
                    searchable.DataBind();
                    return;
                }
                else { 
                    searchable.Items.Clear();
                    return;
                }
            }

            if (comercios != null && comercios.Count() > 0)
            {
                searchable.DataSource = comercios;
                searchable.DataBind();
            }
            else
                searchable.Items.Clear();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        idProvincia = int.Parse(hdnIDProvincia.Value);
        idCiudad = int.Parse(hdnIDCiudad.Value);
        idZona = int.Parse(hdnIDZona.Value);

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranq = usu.IDFranquicia;
        }
        else
            idFranq = int.Parse(hdnIDFranquicia.Value);

        CargarComercios(idZona, idFranq, hdnEstado.Value);
        divPaso2.Visible = divPaso3.Visible = true;

        cmbFranquicia.SelectedValue = idFranq.ToString();


        var provincias = cargarProvincias(idFranq);
        if (provincias != null)
        {

            cmbProvincia.DataSource = provincias;
            cmbProvincia.DataValueField = "ID";
            cmbProvincia.DataTextField = "Nombre";
            cmbProvincia.DataBind();

            cmbProvincia.SelectedValue = idProvincia.ToString();
        }

        var ciudades = CargarCiudades(idProvincia);
        if (ciudades != null)
        {

            cmbCiudad.DataSource = ciudades;
            cmbCiudad.DataValueField = "ID";
            cmbCiudad.DataTextField = "Nombre";
            cmbCiudad.DataBind();

            cmbCiudad.SelectedValue = idCiudad.ToString();
        }
        var zonas = CargarZonas(idProvincia, idCiudad);
        if (zonas != null)
        {
            cmbZona.DataSource = zonas;
            cmbZona.DataValueField = "ID";
            cmbZona.DataTextField = "Nombre";
            cmbZona.DataBind();
            cmbZona.Items.Insert(0, new ListItem("", "0"));

            cmbZona.SelectedValue = idZona.ToString();    
        }

    }

    [WebMethod(true)]
    public static List<ComboViewModel> CargarCiudades(int idProvincia)
    {
        List<ComboViewModel> result = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            var ciudades = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).OrderBy(x => x.Nombre)
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDCiudad.ToString(),
                    Nombre = x.Nombre,
                }).ToList();
            if (ciudades != null && ciudades.Count() > 0)
                result = ciudades;
        }
        return result;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> CargarZonas(int idProvincia, int idCiudad)
    {
        List<ComboViewModel> result = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            var zonas = dbContext.Zonas.Where(x => x.IDProvincia == idProvincia && x.IDCiudad == idCiudad)
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDZona.ToString(),
                    Nombre = x.Nombre,
                }).ToList();
            if (zonas != null && zonas.Count() > 0)
                result = zonas;
        }
        return result;
    }

    [WebMethod(true)]
    public static string GenerarPlanillas(string comercios, string zona, bool insertMap, string mapRoute)
    {
        string[] ids;
        ids = comercios.Split(",");



        if(insertMap != true)
        {
            mapRoute = "";
        }

        if (ids.Count() > 0 && ids[0] != "null")
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    HttpContext context = HttpContext.Current;
                    string path = Common.CrearPlanillaVerificacion(dbContext, context, ids, zona, mapRoute);

                    return path;
                }
            }
            catch (Exception ex)
            {
                var error = ex.InnerException;
            }

            return "";
        }
        else throw new Exception("Debe seleccionar al menos un comercio");
    }

    [WebMethod(true)]
    public static List<MarkersViewModel> GetMarkers(string idsComercios)
    {
        var list = new List<MarkersViewModel>();
        string[] ids;
        ids = idsComercios.Split(",");
        if (ids.Count() > 0 && ids[0] != "null")
        {

            if (HttpContext.Current.Session["CurrentUser"] != null)
            {

                try
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                        string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                        string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
                        string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                        var trMensual = string.Empty;
                        var trAnual = string.Empty;
                        var promMensual = string.Empty;
                        var promAnual = string.Empty;

                        foreach (var id in ids)
                        {
                            int idcom = int.Parse(id);
                            var com = dbContext.Comercios.Where(x => x.IDComercio == idcom).FirstOrDefault();
                            var marker = new MarkersViewModel();
                            marker.Nombre = com.NombreFantasia;
                            marker.Direccion = com.Domicilios.Domicilio;
                            marker.Lat = com.Domicilios.Latitud;
                            marker.Lng = com.Domicilios.Longitud;
                            marker.Categoria = com.Rubro.HasValue ? com.Rubro.Value.ToString() : "0";
                            marker.Icono = com.Rubro.HasValue ? com.Rubros.Icono : "";
                            marker.Ficha = "/common/Comerciose.aspx?IDComercio=" + com.IDComercio;

                            if (!string.IsNullOrEmpty(com.Logo))
                                marker.Foto = "/files/logos/" + com.Logo;
                            else
                                marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                            list.Add(marker);
                        }
                    }
                    return list;
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
            else
                return list;
        }
        else
        {
            throw new Exception("Debe seleccionar al menos un comercio");
         
        }

    }
    
    protected void subirMapa(object sender, EventArgs e)
    {
        if (Path.GetExtension(this.fu.FileName).Equals(".jpg") || Path.GetExtension(this.fu.FileName).Equals(".JPG"))
        {
            string nombreArchivoRuta = Server.MapPath("/files/mapas/") + Path.GetFileName(this.fu.FileName);
            this.fu.SaveAs(nombreArchivoRuta);
            rutaMapa = nombreArchivoRuta;
        }
        else
        {
            throw new Exception("La extension debe ser jpg");

        }
    }
    
    private void limpiarControles_Click(object sender, EventArgs e)
    {
        this.fu.ClearFileFromPersistedStore();
    }

}