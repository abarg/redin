﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Data.Entity.Infrastructure;
using System.Text;

public partial class common_VerificacionesPOSe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] == null && HttpContext.Current.Session["CurrentUser"] == null)
            Response.Redirect("/login.aspx");
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [System.Web.Services.WebMethod(true)]
    public static void verificar(string nroReferencia)
    {
        int idfranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idfranquicia = usu.IDFranquicia;
        }
        using (var dbContext = new ACHEEntities())
        {
            //   var verifPOSCom = dbContext.VerificacionesPOS.Where(x => x.NroReferencia == nroReferencia);
            var list = dbContext.VerificacionesPOSComercios.Include("Comercios").Where(x => x.NroReferencia == nroReferencia).ToList();
            if (idfranquicia > 0)
            {
                foreach (var item in list)
                {
                    if (item.Comercios.IDFranquicia != idfranquicia)
                        throw new Exception("Nro de referencia incorrecto");
                }
            }
            if (list.Count() == 0 || list[0].Estado == "Finalizado")
                throw new Exception("Nro de referencia incorrecto");
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void guardar(string info, string nroReferencia, bool completo)
    {
        borrarVerificaciones(nroReferencia);
        var filas = info.Split("%").ToList();
        //int idComercio = 0;
        int idTerminal = 0;
        using (var dbContext = new ACHEEntities())
        {
            for (int i = 1; i < filas.Count(); i++)
            {
                Terminales ter = null;
                VerificacionesPOS verifPOS = new VerificacionesPOS();
                var fila = filas[i].Split("-").ToList();
                for (int j = 0; j < fila.Count(); j++)
                {
                    var valor = fila[j].Split("#").ToList();
                    switch (valor[0])
                    {
                        //case "idComercio":
                        //    verifPOS.IDComercio = Convert.ToInt32(valor[1]);
                        //    idComercio = Convert.ToInt32(valor[1]);
                        //    ter = dbContext.Terminales.Include("Comercios").Where(x => x.IDComercio == idComercio).FirstOrDefault();
                        //    break;
                        case "idTerminal":
                            idTerminal = Convert.ToInt32(valor[1]);

                            ter = dbContext.Terminales.Include("Comercios").Where(x => x.IDTerminal == idTerminal).FirstOrDefault();

                            if(ter != null) { 
                              verifPOS.IDTerminal = 3773;
                              verifPOS.IDComercio = ter.IDComercio;
                            }
                            else
                            {
                                return;
                            }

                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), ter.IDTerminal.ToString(), ter.IDComercio.ToString());


                            break;
                        case "cmbEstadoCompras":
                            verifPOS.EstadoCompras = valor[1];
                            if (valor[1] != string.Empty)
                                ter.EstadoCompras = valor[1];
                            break;
                        case "cmbEstadoCanjes":
                            verifPOS.EstadoCanjes = valor[1];
                            if (valor[1] != string.Empty)
                                ter.EstadoCanjes = valor[1];
                            break;
                        case "cmbEstadoGift":
                            verifPOS.EstadoGift = valor[1];
                            if (valor[1] != string.Empty)
                                ter.EstadoGift = valor[1];
                            break;
                        case "chkCalco":
                            verifPOS.Calco = Convert.ToBoolean(valor[1]);
                            break;
                        case "chkDisplay":
                            verifPOS.Display = Convert.ToBoolean(valor[1]);
                            break;
                        case "chkCalcoPuerta":
                            verifPOS.CalcoPuerta = Convert.ToBoolean(valor[1]);
                            break;
                        case "txtVerifCompras":
                            verifPOS.Puntos_POSCompras = (valor[1]);
                            break;
                        case "txtVerifCanjes":
                            verifPOS.Puntos_POSCanjes = valor[1];
                            break;
                        case "txtVerifGift":
                            verifPOS.Puntos_POSGift = valor[1];
                            break;
                        case "chkFolletos":
                            verifPOS.Folletos = Convert.ToBoolean(valor[1]);
                            break;
                        case "txtObservacionesTerminales":
                            verifPOS.ObservacionesGenerales = valor[1];
                            break;
                    }
                }
                verifPOS.NroReferencia = nroReferencia;
                verifPOS.FechaPrueba = DateTime.Now;
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    verifPOS.IDUsuario = usu.IDUsuario;
                }
                else
                {
                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    verifPOS.IDUsuario = usu.IDUsuario;
                }

                dbContext.VerificacionesPOS.Add(verifPOS);
            }
            #region Estado
            var verificaciones = dbContext.VerificacionesPOSComercios.Where(x => x.NroReferencia == nroReferencia).ToList();
            foreach (var item in verificaciones)
            {
                if (completo)
                    item.Estado = "Finalizado";
                else
                    item.Estado = "EnProceso";
            }
            #endregion

            try {
                dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {

                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName + " -- " + ve.ErrorMessage);

                    }
                }
                throw;
            }
            catch (DbUpdateException dbu)
            {
                var exception = HandleDbUpdateException(dbu);
                throw;
            }
        }
    }

    private static Exception HandleDbUpdateException(DbUpdateException dbu)
    {
        var builder = new StringBuilder("A DbUpdateException was caught while saving changes. ");

        try
        {
            foreach (var result in dbu.Entries)
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]),"Type: {0} was part of the problem. ", result.Entity.GetType().Name);
            }
        }
        catch (Exception e)
        {
            builder.Append("Error parsing DbUpdateException: " + e.ToString());
        }

        string message = builder.ToString();
        return new Exception(message, dbu);
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTabla(string nroReferencia)
    {
        var html = string.Empty;
        using (var dbContext = new ACHEEntities())
        {
            var verifPOSCom = dbContext.VerificacionesPOS.Include("Terminales").Include("Comercios").Where(x => x.NroReferencia == nroReferencia).ToList();
            if (verifPOSCom.Any())
            {

                int n = 1;
                foreach (var item in verifPOSCom)
                {

                    html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                    html += "<td id=\"idTerminal\"   value=\"" + item.IDTerminal + "\" class=\"selectVerificaciones\">" + n + "</td>";
                    html += "<td > " + (item != null ? item.EstadoCompras : "") + " </td>";
                    html += "<td> " + (item != null ? item.EstadoCanjes : "N0") + " </td>";
                    html += "<td> " + (item != null ? item.EstadoGift : "N0") + " </td>";
                    html += "<td> " + item.Comercios.NombreFantasia + "</td> ";
                    html += "<td> " + item.Comercios.Domicilios.Domicilio + " </td>";
                    html += "<td value=\"" + (item != null ? item.Terminales.POSTerminal : "") + "\"> " + (item != null ? item.Terminales.POSTerminal : "") + "</td> ";
                    html += "<td value=\"" + (item != null ? item.Comercios.SDS : "") + "\"> " + (item != null ? item.Comercios.SDS : "") + "</td> ";
                    html += "<td value=\"" + (item != null ? item.Terminales.NumEst : "") + "\"> " + (item != null ? item.Terminales.NumEst : "") + "</td> ";

                    if (item.Calco == true)
                        html += "<td> <input id=\"chkCalco\" class=\"selectVerificaciones\"  checked=\"checked\" type=\"checkbox\"/> </td>";
                    else
                        html += "<td> <input id=\"chkCalco\" class=\"selectVerificaciones\" type=\"checkbox\"/> </td>";

                    if (item.Display == true)
                        html += "<td> <input id=\"chkDisplay\" class=\"selectVerificaciones\"  checked=\"checked\" type=\"checkbox\" /> </td>";
                    else
                        html += "<td> <input id=\"chkDisplay\" class=\"selectVerificaciones\" type=\"checkbox\" /> </td>";
                    if (item.CalcoPuerta == true)
                        html += "<td> <input id=\"chkCalcoPuerta\"  class=\"selectVerificaciones\" checked=\"checked\" type=\"checkbox\" /> </td>";
                    else
                        html += "<td> <input id=\"chkCalcoPuerta\"  class=\"selectVerificaciones\" type=\"checkbox\" /> </td>";
                    switch (item.EstadoCompras)
                    {
                        case "NARANJA":
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option selected value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option> </select> </td>";
                            break;
                        case "VERDE":
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option selected value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option> </select> </td>";
                            break;
                        case "AMARILLO":
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option selected value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option> </select> </td>";
                            break;
                        case "ROJO":
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option selected value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option></select> </td>";
                            break;
                        case "NUEVO":
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option selected value=\"NUEVO\">NUEVO</option> </select> </td>";
                            break;
                        default:
                            html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option> </select> </td>";
                            break;
                    }
                    switch (item.EstadoCanjes)
                    {
                        case "SI":
                            html += "<td> <select id=\"cmbEstadoCanjes\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option selected value=\"SI\">SI</option><option value=\"NO\">NO</option></select> </td>";
                            break;
                        case "NO":
                            html += "<td> <select id=\"cmbEstadoCanjes\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"SI\">SI</option><option selected value=\"NO\">NO</option></select> </td>";
                            break;
                        default:
                            html += "<td> <select id=\"cmbEstadoCanjes\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select> </td>";
                            break;
                    }
                    switch (item.EstadoGift)
                    {
                        case "SI":
                            html += "<td> <select id=\"cmbEstadoGift\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option selected value=\"SI\">SI</option><option value=\"NO\">NO</option></select> </td>";
                            break;
                        case "NO":
                            html += "<td> <select id=\"cmbEstadoGift\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"SI\">SI</option><option selected value=\"NO\">NO</option></select> </td>";
                            break;
                        default:
                            html += "<td> <select id=\"cmbEstadoGift\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option>  <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select>  </td>";
                            break;
                    }

                    if (item.Folletos == true)
                        html += "<td> <input id=\"chkFolletos\" class=\"selectVerificaciones\" checked=\"checked\"  type=\"checkbox\" /> </td>";
                    else
                        html += "<td> <input id=\"chkFolletos\" class=\"selectVerificaciones\"  type=\"checkbox\" /> </td>";

                    html += "<td> <input id=\"txtObservacionesTerminales\" class=\"selectVerificaciones\" value=\"" + item.ObservacionesGenerales + "\" type=\"textbox\" /></td>";

                    html += "</tr>";
                    n++;
                }
            }
            else
            {
                var list = dbContext.VerificacionesPOSComercios.Include("Comercios").Include("Terminales").Where(x => x.NroReferencia == nroReferencia).ToList();

                if (list.Any())
                {
                    //   if()
                    int n = 1;
                    foreach (var item in list)
                    {

                        html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                        html += "<td hidden id=\"idComercio\"   value=\"" + item.IDComercio + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        html += "<td id=\"idTerminal\"   value=\"" + item.IDTerminal + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        var verif = item.Comercios.VerificacionesPOS.OrderByDescending(x => x.NroReferencia == item.NroReferencia).FirstOrDefault();
                        html += "<td> " + (item.Terminales.EstadoCompras != null ? item.Terminales.EstadoCompras : "") + " </td>";
                        html += "<td> " + (item.Terminales.EstadoCanjes != null ? item.Terminales.EstadoCanjes : "NO") + " </td>";
                        html += "<td> " + (item.Terminales.EstadoGift != null ? item.Terminales.EstadoGift : "NO") + " </td>";
                        html += "<td> " + item.Comercios.NombreFantasia + "</td> ";
                        html += "<td> " + item.Terminales.Domicilios.Domicilio + " </td>";
                        html += "<td> " + (item != null ? item.Terminales.POSTerminal : "") + "</td> ";
                        html += "<td> " + (item != null ? item.Comercios.SDS : "") + "</td> ";
                        html += "<td value=\"" + (item != null ? item.Terminales.NumEst : "") + "\"> " + (item != null ? item.Terminales.NumEst : "") + "</td> ";


                        html += "<td> <input id=\"chkCalco\" class=\"selectVerificaciones\"  type=\"checkbox\"/> </td>";
                        html += "<td> <input id=\"chkDisplay\" class=\"selectVerificaciones\"  type=\"checkbox\" /> </td>";
                        html += "<td> <input id=\"chkCalcoPuerta\"  class=\"selectVerificaciones\"  type=\"checkbox\" /> </td>";
                        html += "<td> <select id=\"cmbEstadoCompras\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"NARANJA\">NARANJA</option><option value=\"VERDE\">VERDE</option><option value=\"AMARILLO\">AMARILLO</option> <option value=\"ROJO\">ROJO</option> <option value=\"NUEVO\">NUEVO</option> </select> </td>";
                        html += "<td> <select id=\"cmbEstadoCanjes\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option> <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select> </td>";
                        html += "<td> <select id=\"cmbEstadoGift\" class=\"form-control selectVerificaciones\" ><option value=\"\"></option>  <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select>  </td>";
                        html += "<td> <input id=\"chkFolletos\" class=\"selectVerificaciones\"  type=\"checkbox\" /> </td>";
                        html += "<td> <input id=\"txtObservacionesTerminales\" class=\"selectVerificaciones\"  type=\"textbox\" /></td>";

                        html += "</tr>";
                        n++;
                    }
                }
                else
                    html += "<tr><td colspan=34'>No hay un detalle disponible</td></tr>";
            }

        }
        return html;
    }

    private static void borrarVerificaciones(string nroReferencia)
    {
        using (var dbContext = new ACHEEntities())
        {
            var verifPOSCom = dbContext.VerificacionesPOS.Include("Comercios").Where(x => x.NroReferencia == nroReferencia).ToList();
            if (verifPOSCom.Any())
            {
                dbContext.VerificacionesPOS.RemoveRange(verifPOSCom);
            }
            dbContext.SaveChanges();

        }
    }


}