﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using ACHE.Model.EntityData;

public partial class common_LiquidacionDetallePorTipo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["IDLiquidacionDetalle"]))
        {
            this.hfIDLiquidacionDetalle.Value = Request.QueryString["IDLiquidacionDetalle"];
            int idLiquidacionDetalle = int.Parse(Request.QueryString["IDLiquidacionDetalle"]);
            using (var dbContext = new ACHEEntities())
            {
                var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiquidacionDetalle).FirstOrDefault();
                this.hdfIDLiquidacion.Value = liq.IDLiquidacion.ToString();
                hdfConcepto.Value = liq.Tipo;

                litConcepto.Text = "Liquidación #" + liq.IDLiquidacion.ToString("#000000") + " - " + "Concepto: " + liq.Tipo;
                if (liq.Tipo == "TRANSACCIONES")
                {
                    pnlComercio.Visible = true;
                    pnlVerTR.Visible = true;
                }
                else
                {
                    pnlSubConcepto.Visible = true;
                }
                if ((HttpContext.Current.Session["CurrentUser"] != null && liq.Liquidaciones.Estado == "CasaMatriz") || (HttpContext.Current.Session["CurrentFranquiciasUser"] != null && liq.Liquidaciones.Estado == "EnFranquicia"))
                {
                    pnlGrabar.Visible = true;
                }
                if (HttpContext.Current.Session["CurrentUser"] != null && liq.Liquidaciones.Estado == "CasaMatriz" && liq.Tipo != "TRANSACCIONES")
                {
                    pnlFormLiqDetalle.Visible = true;
                }
            }

        }
    }

    [System.Web.Services.WebMethod]
    public static void AgregarItem(string importeOriginal, string idDetalle, string subConcepto)
    {
        using (var dbContext = new ACHEEntities())
        {
            LiquidacionDetalleTipo liq = new LiquidacionDetalleTipo();
            importeOriginal = importeOriginal.Replace(".", ",");
            liq.ImporteTotalOriginal = decimal.Parse(importeOriginal);
            liq.IDLiquidacionDetalle = int.Parse(idDetalle);
            liq.SubConcepto = subConcepto;
            dbContext.LiquidacionDetalleTipo.Add(liq);
            dbContext.SaveChanges();

        }
        int id = int.Parse(idDetalle);
        ActualizarSubTotal(id);
    }

    private static void ActualizarSubTotal(int idLiqDetalle)
    {
        using (var dbContext = new ACHEEntities())
        {
            var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiqDetalle).FirstOrDefault();
            liq.SubTotal = liq.LiquidacionDetalleTipo.Sum(x => x.ImporteTotal.HasValue ? x.ImporteTotal : x.ImporteTotalOriginal);
            dbContext.SaveChanges();
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTabla(int idLiquidacion)
    {
        var html = string.Empty;
        var Perfil = "";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            Perfil = "Franquicia";

        if (HttpContext.Current.Session["CurrentUser"] != null)
            Perfil = "Admin";

        using (var dbContext = new ACHEEntities())
        {
            //  int idliqdet = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacion == idLiquidacion && x.Tipo == "TRANSACCIONES").FirstOrDefault().IDLiquidacionDetalle;
            var list = dbContext.LiquidacionDetalleTipo.Include("Comercios").Where(x => x.IDLiquidacionDetalle == idLiquidacion).ToList();
            if (list.Any())
            {
                var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiquidacion && x.Tipo == "TRANSACCIONES").FirstOrDefault();
                if (liq.Liquidaciones.Estado == "Confirmada" || liq.Liquidaciones.Estado == "Finalizada" || (liq.Liquidaciones.Estado == "CasaMatriz" && Perfil == "Franquicia") || (liq.Liquidaciones.Estado == "EnFranquicia" && Perfil == "Admin") || (liq.Liquidaciones.Estado == "Paso4"))
                {
                    int n = 1;
                    foreach (var item in list.OrderBy(x => x.Comercios.NombreFantasia))
                    {

                        html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                        html += "<td id=\"idLiquidacionDetalle\"   value=\"" + item.IDLiquidacionDetalleTipo + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        html += "<td>" + item.Comercios.NombreFantasia + "</td> ";
                        html += "<td>" + item.ImporteTotalOriginal + "</td> ";
                        html += "<td>" + item.ImporteTotal + "</td> ";
                        html += "<td><div align='center'><a onclick='verModalTR(" + item.IDLiquidacionDetalleTipo + ");'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/><a/></div></td> ";
                        //   html += "<td> " + item.Observaciones + "</td> ";
                        html += "</tr>";
                        n++;
                    }
                }
                else
                {
                    int n = 1;
                    foreach (var item in list.OrderBy(x => x.Comercios.NombreFantasia))
                    {
                        html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                        html += "<td id=\"idLiquidacionDetalle\"   value=\"" + item.IDLiquidacionDetalleTipo + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        html += "<td>" + item.Comercios.NombreFantasia + "</td> ";
                        html += "<td>" + item.ImporteTotalOriginal + "</td> ";
                        html += "<td><input type=\"number\" min=\"0.01\" step=\"0.01\" value=\"" + item.ImporteTotal.ToString().Replace(",", ".") + "\"  id=\"txtImporteTotal\" class=\"selectVerificaciones\"/></td>";
                        html += "<td><div align='center'><a onclick='verModalTR(" + item.IDLiquidacionDetalleTipo + ");'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/><a/></div></td> ";

                        //  html += "<td> <input id=\"txtObservaciones\" class=\"selectVerificaciones\"  value=\"" + item.Observaciones + "\"  type=\"textbox\" /></td>";
                        html += "</tr>";
                        n++;
                    }
                }
            }
            else
            {
                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetTransacciones(int idLiquidacion)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null || (HttpContext.Current.Session["CurrentFranquiciasUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                var liq = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalleTipo == idLiquidacion).FirstOrDefault();
                var aux = dbContext.TransaccionesView.Where(x => x.IDComercio == liq.IDComercio && x.FechaTransaccion >= liq.LiquidacionDetalle.Liquidaciones.FechaDesde && x.FechaTransaccion <= liq.LiquidacionDetalle.Liquidaciones.FechaHasta)
                     .OrderBy(x => x.FechaTransaccion)
                     .Select(x => new TransaccionesViewModel
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0
                     }).ToList();
                list = aux.ToList();

            }
        }
        return list;
    }

    /*
     [System.Web.Services.WebMethod(true)]
     public static void enviarObs(string info)
     {
         int id = 0;
         var filas = info.Split("%").ToList();
         using (var dbContext = new ACHEEntities())
         {
             LiquidacionDetalleTipo entity = null;
             for (int i = 1; i < filas.Count(); i++)
             {
                 var fila = filas[i].Split("-").ToList();
                 int idliquidacion = 0;
                 string importeTotal = "";
               //  string observaciones = "";
                 for (int j = 0; j < fila.Count(); j++)
                 {
                     var valor = fila[j].Split("#").ToList();

                     switch (valor[0])
                     {
                         case "idLiquidacionDetalle":
                             idliquidacion = Convert.ToInt32(valor[1]);
                             break;
                         case "txtImporteTotal":
                             if (valor[1] != "")
                                 importeTotal = valor[1];
                             break;
                         //      case "txtObservaciones":
                         //      observaciones = valor[1];
                         //      break;
                     }
                     if (idliquidacion > 0 && j ==2)
                     {
                         entity = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalleTipo == idliquidacion).FirstOrDefault();
                         if (importeTotal != "")
                         {
                             importeTotal = importeTotal.Replace(".", ",");
                             entity.ImporteTotal = decimal.Parse(importeTotal);
                         }
                         //  entity.Observaciones = observaciones;
                     }
                 }
             }
             entity.LiquidacionDetalle.Liquidaciones.Estado = "CasaMatriz";
             id = entity.IDLiquidacionDetalle;
             dbContext.SaveChanges();
         }
         ActualizarSubTotal(id);

     }

     [System.Web.Services.WebMethod(true)]
     public static void enviarAlFranquiciado(string info)
     {

         var filas = info.Split("%").ToList();
         int id = 0;
         using (var dbContext = new ACHEEntities())
         {
             LiquidacionDetalleTipo entity = null;
             for (int i = 1; i < filas.Count(); i++)
             {
                 var fila = filas[i].Split("-").ToList();
                 int idliquidacion = 0;
                 string importeTotal = "";
                // string observaciones = "";
                 for (int j = 0; j < fila.Count(); j++)
                 {
                     var valor = fila[j].Split("#").ToList();

                     switch (valor[0])
                     {
                         case "idLiquidacionDetalle":
                             idliquidacion = Convert.ToInt32(valor[1]);
                             break;
                         case "txtImporteTotal":
                             if (valor[1] != "")
                                 importeTotal = valor[1];
                             break;
                         //   case "txtObservaciones":
                         //      observaciones = valor[1];
                         //       break;
                     }
                     if (idliquidacion > 0 && j == 2)
                     {
                         entity = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalleTipo == idliquidacion).FirstOrDefault();
                         if (importeTotal != "")
                         {
                             importeTotal = importeTotal.Replace(".", ",");
                             entity.ImporteTotal = decimal.Parse(importeTotal);
                         }
                         //    entity.Observaciones = observaciones;
                     }
                 }
             }
             entity.LiquidacionDetalle.Liquidaciones.Estado = "EnFranquicia";
             id = entity.IDLiquidacionDetalle;
             dbContext.SaveChanges();
         }
         ActualizarSubTotal(id);
     }
     */

    [System.Web.Services.WebMethod(true)]
    public static void grabar(string info, string concepto)
    {
        int cantCampos = 2;
        if (concepto != "TRANSACCIONES")
            cantCampos = 3;
        var filas = info.Split("%").ToList();
        int id = 0;
        var isvalid = false;
        using (var dbContext = new ACHEEntities())
        {
            LiquidacionDetalleTipo entity = null;
            for (int i = 1; i < filas.Count(); i++)
            {
                var fila = filas[i].Split("-").ToList();
                int idliquidacion = 0;
                string subConcepto = "";
                string importeTotal = "";
                for (int j = 0; j < fila.Count(); j++)
                {
                    var valor = fila[j].Split("#").ToList();

                    switch (valor[0])
                    {
                        case "idLiquidacionDetalle":
                            idliquidacion = Convert.ToInt32(valor[1]);
                            break;
                        case "txtImporteTotal":
                            if (valor[1] != "")
                            {
                                importeTotal = valor[1];
                                isvalid = true;
                            }
                            break;
                        case "txtSubConcepto":
                            subConcepto = valor[1];
                            if (valor[1] != "")
                            {
                                isvalid = true;
                            }
                            break;
                    }
                    if (idliquidacion > 0 && j == cantCampos)
                    {
                        entity = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalleTipo == idliquidacion).FirstOrDefault();
                        if (importeTotal != "")
                        {
                            importeTotal = importeTotal.Replace(".", ",");
                            entity.ImporteTotal = decimal.Parse(importeTotal);
                        }
                        else
                            entity.ImporteTotal = null;

                        if (subConcepto != "")
                            entity.SubConcepto = subConcepto;
                        else
                            entity.SubConcepto = null;
                    }
                }
            }
            id = entity.IDLiquidacionDetalle;
            if (isvalid)
                dbContext.SaveChanges();
            else
                throw new Exception("Debe ingresar algun valor");

        }
        ActualizarSubTotal(id);
    }

    [WebMethod(true)]
    public static string ExportarTR(int idLiquidacion)
    {
        string fileName = "LiquidacionDetalleTR";
        string path = "/tmp/";
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var liq = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalleTipo == idLiquidacion).FirstOrDefault();
                    var aux = dbContext.TransaccionesView.Where(x => x.IDComercio == liq.IDComercio && x.FechaTransaccion >= liq.LiquidacionDetalle.Liquidaciones.FechaDesde && x.FechaTransaccion <= liq.LiquidacionDetalle.Liquidaciones.FechaHasta)
                    .OrderBy(x => x.FechaTransaccion).AsQueryable();

                    dt = aux.Select(x => new TransaccionesLiquidacionViewModel
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    [WebMethod(true)]
    public static string Exportar(int idLiquidacion)
    {
        string fileName = "LiquidacionDetalle";
        string path = "/tmp/";
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            string concepto = "";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    //int idliqdet = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacion == idLiquidacion && x.Tipo == "TRANSACCIONES").FirstOrDefault().IDLiquidacionDetalle;
                    var info = dbContext.LiquidacionDetalleTipo.Include("Comercios").Where(x => x.IDLiquidacionDetalle == idLiquidacion).AsEnumerable();

                    var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiquidacion).FirstOrDefault();
                    concepto = liq.Tipo;
                    dt = info.OrderBy(x => x.Comercios.NombreFantasia).ToList().Select(x => new
                    {
                        Comercio = (x.IDComercio.HasValue) ? x.Comercios.NombreFantasia : "",
                        SubConcepto = x.SubConcepto,
                        ImporteTotalOriginal = x.ImporteTotalOriginal ?? 0,
                        ImporteTotal = x.ImporteTotal ?? 0,
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    if (concepto == "TRANSACCIONES")
                        dt.Columns.RemoveAt(1);
                    else
                        dt.Columns.RemoveAt(0);

                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTablaConceptos(int idLiquidacion)
    {
        var html = string.Empty;
        var Perfil = "";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            Perfil = "Franquicia";

        if (HttpContext.Current.Session["CurrentUser"] != null)
            Perfil = "Admin";

        using (var dbContext = new ACHEEntities())
        {
            var list = dbContext.LiquidacionDetalleTipo.Where(x => x.IDLiquidacionDetalle == idLiquidacion).ToList();
            if (list.Any())
            {
                var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiquidacion).FirstOrDefault();
                if (liq.Liquidaciones.Estado == "Confirmada" || liq.Liquidaciones.Estado == "Finalizada" || (liq.Liquidaciones.Estado == "CasaMatriz" && Perfil == "Franquicia") || (liq.Liquidaciones.Estado == "EnFranquicia" && Perfil == "Admin") || (liq.Liquidaciones.Estado == "Paso4"))
                {
                    int n = 1;
                    foreach (var item in list)
                    {
                        html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                        html += "<td id=\"idLiquidacionDetalle\"   value=\"" + item.IDLiquidacionDetalleTipo + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        html += "<td>" + item.SubConcepto + "</td> ";
                        html += "<td>" + item.ImporteTotalOriginal + "</td> ";
                        html += "<td>" + item.ImporteTotal + "</td> ";
                        html += "</tr>";
                        n++;
                    }
                }
                else
                {
                    int n = 1;
                    foreach (var item in list)
                    {
                        html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                        html += "<td id=\"idLiquidacionDetalle\"   value=\"" + item.IDLiquidacionDetalleTipo + "\" class=\"selectVerificaciones\">" + n + "</td>";
                        html += "<td><input type=\"text\"  value=\"" + item.SubConcepto + "\"  id=\"txtSubConcepto\" class=\"selectVerificaciones\"/></td>";
                        html += "<td>" + item.ImporteTotalOriginal + "</td> ";
                        html += "<td><input type=\"number\" min=\"0.01\" step=\"0.01\" value=\"" + item.ImporteTotal.ToString().Replace(",", ".") + "\"  id=\"txtImporteTotal\" class=\"selectVerificaciones\"/></td>";
                        html += "</tr>";
                        n++;
                    }
                }
            }
            else
                html += "<tr id='trSinItems'><td colspan='4'>No hay un detalle disponible</td></tr>";

        }

        return html;
    }
}