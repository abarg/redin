﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.Data;
using System.Web.Services;
using System.IO;


public partial class common_Multimarcase : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        CargarCombos();
        if (!IsPostBack) {
            this.txtArancel.Text = "3";
            Session["CurrentLogo"] = null;

            if (!String.IsNullOrEmpty(Request.QueryString["IDMultimarca"])) {
                this.hdnID.Value = Request.QueryString["IDMultimarca"];
                
                if (!this.hdnID.Value.Equals("0")) {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDMultimarca"]));
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void CargarCombos() {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
            ddlFranquicias.Enabled = false;
        }
        else {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            ddlProvincia.DataSource = Common.LoadProvincias();
            ddlProvincia.DataTextField = "Nombre";
            ddlProvincia.DataValueField = "ID";
            ddlProvincia.DataBind();
        }
        cargarMarcas();
        //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));"
    }

    private void cargarMarcas() {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
            if (!String.IsNullOrEmpty(Request.QueryString["IDMultimarca"])) 
                this.hdnID.Value = Request.QueryString["IDMultimarca"];

                int idMultimarca = int.Parse(hdnID.Value);
                if (idMultimarca > 0) {
                    var multimarca = dbContext.Multimarcas.Where(x => x.IDMultimarca == idMultimarca).FirstOrDefault();
                    bMarca bMarca = new bMarca();
                    var listMarcas = bMarca.getMarcasByFranquicia(multimarca.IDFranquicia??0);
                    this.cmbMarcas.DataSource = listMarcas;
                    this.cmbMarcas.DataValueField = "IDMarca";
                    this.cmbMarcas.DataTextField = "Nombre";
                    this.cmbMarcas.DataBind();
                    //this.cmbMarcas.Items.Insert(0, new ListItem("", ""));
                }
            }
        }        
    }

    public static List<Combo2ViewModel> LoadCiudades(int idProvincia) {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        using (var dbContext = new ACHEEntities()) {
            entities = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia)
                .Select(x => new Combo2ViewModel() {
                    ID = x.IDCiudad,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }
        return entities;
    }

    private void cargarDatos(int id) {
        try {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.Multimarcas.Where(x => x.IDMultimarca == id).FirstOrDefault();

                if (entity != null) {
                    var domicilio = dbContext.Domicilios.Where(x => x.IDDomicilio == entity.IDDomicilio).FirstOrDefault();
                    this.txtNombre.Text = entity.Nombre;
                    this.txtPrefijo.Text = entity.Prefijo;
                    this.txtCodigo.Text = entity.Codigo;
                    this.txtAffinity.Text = entity.Affinity;
                    this.txtArancel.Text = entity.POSArancel.ToString().Replace(",", ".");
                    this.ddlColor.SelectedValue = entity.Color;
                    this.chkMostrarSoloPOSPropios.Checked = entity.MostrarSoloPOSPropios;
                    this.chkMostrarSoloTarjetasPropias.Checked = entity.MostrarSoloTarjetasPropias;
                    this.chkPosWeb.Checked = entity.HabilitarPOSWeb;
                    this.chkGiftcardWeb.Checked = entity.HabilitarGiftcard;
                    this.chkCuponIN.Checked = entity.HabilitarCuponIN;
                    this.chkProductos.Checked = entity.MostrarProductos;
                    this.chkSMS.Checked = entity.HabilitarSMS;
                    this.txtCostoSMS.Text = entity.CostoSMS.ToString();
                    this.chkSMSBienvenida.Checked = entity.EnvioMsjBienvenida;
                    this.txtSMSBienvenida.Text = entity.MsjBienvenida;
                    this.chkSMSCumpleanios.Checked = entity.EnvioMsjCumpleanios;
                    this.txtSMSCumpleanios.Text = entity.MsjCumpleanios;
                    this.txtFechaTopeCanje.Text = entity.fechaTopeCanje != null ? entity.fechaTopeCanje.Value.ToString("dd/MM/yyyy") : "";
                    this.txtFechaCaducidad.Text = entity.fechaCaducidad != null ? entity.fechaCaducidad.Value.ToString("dd/MM/yyyy") : "";

                    if (entity.EnvioEmailRegistroSocio) {
                        this.chkEmailRegistroASocio.Checked = true;
                        this.txtEmailBienvenidaASocio.Text = entity.EmailRegistroSocio;
                    }

                    if (entity.EnvioEmailCumpleanios) {
                        this.chkEmailCumpleanios.Checked = true;
                        this.txtEmailCumpleanios.Text = entity.EmailCumpleanios;
                    }

                    if (entity.EnvioEmailRegistroComercio) {
                        this.chkEmailAComercio.Checked = true;
                        this.txtEmailAComercio.Text = entity.EmailRegistroComercio;
                    }

                    this.txtEmailAlerta.Text = entity.EmailAlertas;
                    this.txtCelularAlerta.Text = entity.CelularAlertas;
                    this.txtCelularEmpresaAlerta.SelectedValue = entity.EmpresaCelularAlertas;
                    this.txtRazonSocial.Text = entity.RazonSocial;
                    this.ddlIVA.SelectedValue = entity.CondicionIva;
                    this.ddlTipoDoc.SelectedValue = entity.TipoDocumento;
                    this.txtNroDoc.Text = entity.NroDocumento;
                    if (domicilio != null) {
                        this.ddlProvincia.SelectedValue = domicilio.Provincia.ToString();
                        this.txtDomicilio.Text = domicilio.Domicilio;
                        ddlCiudad.DataSource = Common.LoadCiudades(domicilio.Provincia);
                        ddlCiudad.DataTextField = "Nombre";
                        ddlCiudad.DataValueField = "ID";
                        ddlCiudad.DataBind();
                        ddlCiudad.Items.Insert(0, new ListItem("", ""));
                        this.ddlCiudad.SelectedValue = domicilio.Ciudad.ToString();
                        this.txtPisoDepto.Text = domicilio.PisoDepto;
                        this.txtCodigoPostal.Text = domicilio.CodigoPostal;
                        this.txtTelefonoDom.Text = domicilio.Telefono;
                    }

                    var comercios = dbContext.Comercios
                        .Include("Domicilios")
                        .Where(x => x.IDMarca == id)
                    .Select(x => new Combo2ViewModel() {
                        ID = x.IDComercio,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio
                    }).OrderBy(x => x.Nombre).ToList();
                    this.cmbComercioSMS.DataSource = comercios;
                    this.cmbComercioSMS.DataValueField = "ID";
                    this.cmbComercioSMS.DataTextField = "Nombre";
                    this.cmbComercioSMS.DataBind();

                    this.cmbComercioSMS.Items.Insert(0, new ListItem("", ""));
                    this.cmbComercioSMS.SelectedValue = entity.IDComercioFacturanteSMS.ToString() ?? "";
                    this.txtCostoPlusin.Text = entity.CostoPlusin.ToString();
                    this.txtCostoSeguro.Text = entity.CostoSeguro.ToString();
                    this.txtCostoTransaccional.Text = entity.CostoTransaccional.ToString();
                    this.txtCostoSMS2.Text = entity.CostoSMS2.ToString();
                    if (entity.IDFranquicia.HasValue) {
                        this.ddlFranquicias.SelectedValue = entity.IDFranquicia.Value.ToString();
                        this.ddlFranquicias.Enabled = false;
                    }
                    this.ddlCatalogo.SelectedValue = entity.TipoCatalogo;
                    this.ddlTipoTarjeta.SelectedValue = entity.TipoTarjeta != null ? entity.TipoTarjeta : "REDIN";
                    if (!string.IsNullOrEmpty(entity.Logo)) {
                        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=franquicias&file=" + entity.Logo;
                        else
                            lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.Logo;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.Logo + "', 'divLogo')");
                        divLogo.Visible = true;

                        imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                    }
                    else
                        imgLogo.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";


                    //POS
                    this.chkFormaPago.Checked = entity.POSMostrarFormaPago;
                    this.chkNroTicket.Checked = entity.POSMostrarNumeroTicket;

                    this.txtFooter1.Text = entity.POSFooter1;
                    this.txtFooter2.Text = entity.POSFooter2;
                    this.txtFooter3.Text = entity.POSFooter3;
                    this.txtFooter4.Text = entity.POSFooter4;

                    this.chkFidelidad.Checked = entity.POSMostrarMenuFidelidad;
                    this.chkMenuGift.Checked = entity.POSMostrarMenuGift;
                    this.chkLogo.Checked = entity.POSMostrarMenuGift;
                    this.chkChargeGift.Checked = entity.POSMostrarChargeGift;
                }
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int id, string nombre, string prefijo, string affinity, string arancel, string color,
        bool mostrarSoloPOSPropios, bool mostrarSoloTarjetasPropias, bool posWeb, string idFranquicia, string tipoCatalogo, string codigo,
        bool giftcardWeb, bool cuponInWeb, bool productos, bool sms, string costoSMS, bool smsBienvenida, string mensajeBienvenida, bool smsCumpleanios,
        string mensajeCumpleanios, int idComercioSMS, bool envioEmailRegistroSocio, string emailBienvenidaSocio,
        bool envioEmailCumpleanios, string emailCumpleanios, bool envioEmailRegistroComercio, string emailBienvenidoComercio, string emailAlertas, string celularAlertas,
        string celularEmpresaAlertas, string razonSocial, string condicionIVA, string tipoDoc, string nroDoc, string provincia, string ciudad, string domicilio,
        string pisoDepto, string codigoPostal, string telefono, string costoTransaccional, string costoSeguro, string costoPlusin, string costoSMS2, string fechaCaducidad, string fechaTopeCanje, string tipoTarjeta,
        bool chkFormaPago, bool chkNroTicket, string footer1, string footer2, string footer3, string footer4, bool chkFidelidad, bool chkMenuGift, bool chkChargeGift, bool chkLogo) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            try {
                using (var dbContext = new ACHEEntities()) {

                    Multimarcas entity;
                    if (id > 0)
                        entity = dbContext.Multimarcas.Where(x => x.IDMultimarca == id).FirstOrDefault();
                    else {
                        entity = new Multimarcas();
                        entity.FechaAlta = DateTime.Now;
                        //entity.IDMultimarca = dbContext.Multimarcas.Max(x => x.IDMultimarca) > 0 ? dbContext.Multimarcas.Max(x => x.IDMultimarca + 1) : 1;
                    }
                    if (fechaTopeCanje != "")
                        entity.fechaTopeCanje = DateTime.Parse(fechaTopeCanje);
                    if (fechaCaducidad != "")
                        entity.fechaCaducidad = DateTime.Parse(fechaCaducidad);

                    entity.Prefijo = prefijo;
                    entity.Affinity = affinity;
                    entity.Codigo = codigo;
                    entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper() : "";
                    entity.POSArancel = decimal.Parse(arancel.Replace(".", ","));
                    entity.Color = color;
                    entity.MostrarSoloPOSPropios = mostrarSoloPOSPropios;
                    entity.MostrarSoloTarjetasPropias = mostrarSoloTarjetasPropias;
                    entity.HabilitarPOSWeb = posWeb;
                    entity.HabilitarGiftcard = giftcardWeb;
                    entity.HabilitarCuponIN = cuponInWeb;
                    entity.MostrarProductos = productos;
                    entity.HabilitarSMS = sms;
                    entity.EnvioMsjBienvenida = smsBienvenida;
                    entity.MsjBienvenida = mensajeBienvenida;
                    entity.EnvioMsjCumpleanios = smsCumpleanios;
                    entity.MsjCumpleanios = mensajeCumpleanios;

                    entity.EnvioEmailRegistroComercio = envioEmailRegistroComercio;
                    entity.EmailRegistroSocio = emailBienvenidaSocio;
                    entity.EnvioEmailCumpleanios = envioEmailCumpleanios;
                    entity.EmailCumpleanios = emailCumpleanios;
                    entity.EnvioEmailRegistroSocio = envioEmailRegistroSocio;
                    entity.EmailRegistroComercio = emailBienvenidoComercio;

                    //Facturacion
                    entity.RazonSocial = razonSocial != null && razonSocial != "" ? razonSocial.ToUpper() : "";
                    entity.CondicionIva = condicionIVA;
                    entity.TipoDocumento = tipoDoc;
                    entity.NroDocumento = nroDoc;
                    entity.TipoTarjeta = tipoTarjeta;

                    //POS
                    entity.POSMostrarFormaPago = chkFormaPago;
                    entity.POSMostrarNumeroTicket = chkNroTicket;
                    entity.POSFooter1 = footer1;
                    entity.POSFooter2 = footer2;
                    entity.POSFooter3 = footer3;
                    entity.POSFooter4 = footer4;
                    entity.POSMostrarMenuFidelidad = chkFidelidad;
                    entity.POSMostrarMenuGift = chkMenuGift;
                    entity.POSMostrarChargeGift = chkChargeGift;
                    entity.POSMostrarLOGO = chkLogo;

                    if (!string.IsNullOrEmpty(provincia)) {
                        if (!entity.IDDomicilio.HasValue) {
                            entity.Domicilios = new Domicilios();
                            entity.Domicilios.FechaAlta = DateTime.Now;
                            entity.Domicilios.Estado = "A";
                            entity.Domicilios.Pais = "Argentina";
                        }
                        entity.Domicilios.TipoDomicilio = "F";
                        entity.Domicilios.Entidad = "M";
                        entity.Domicilios.Provincia = int.Parse(provincia);
                        if (ciudad != string.Empty)
                            entity.Domicilios.Ciudad = int.Parse(ciudad);
                        else
                            entity.Domicilios.Ciudad = null;

                        entity.Domicilios.Domicilio = domicilio != null && razonSocial != "" ? razonSocial.ToUpper() : "";
                        entity.Domicilios.CodigoPostal = codigoPostal;
                        entity.Domicilios.Telefono = telefono;
                        entity.Domicilios.PisoDepto = pisoDepto;
                    }

                    //Costos
                    if (costoTransaccional != "")
                        entity.CostoTransaccional = decimal.Parse(costoTransaccional);
                    else
                        entity.CostoTransaccional = 0;

                    if (costoSeguro != "")
                        entity.CostoSeguro = decimal.Parse(costoSeguro);
                    else
                        entity.CostoSeguro = 0;

                    if (costoPlusin != "")
                        entity.CostoPlusin = decimal.Parse(costoPlusin);
                    else
                        entity.CostoPlusin = 0;
                    if (costoSMS2 != "")
                        entity.CostoSMS2 = decimal.Parse(costoSMS2);
                    else
                        entity.CostoSMS2 = 0;

                    //Alertas
                    entity.EmailAlertas = emailAlertas;
                    entity.CelularAlertas = celularAlertas;
                    entity.EmpresaCelularAlertas = celularEmpresaAlertas;



                    if (idComercioSMS != 0)
                        entity.IDComercioFacturanteSMS = idComercioSMS;
                    else
                        entity.IDComercioFacturanteSMS = null;

                    if (costoSMS != "")
                        entity.CostoSMS = decimal.Parse(costoSMS);
                    else
                        entity.CostoSMS = 0;

                    if (idFranquicia != string.Empty)
                        entity.IDFranquicia = int.Parse(idFranquicia);
                    else
                        entity.IDFranquicia = null;
                    entity.TipoCatalogo = tipoCatalogo;

                    if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                        entity.Logo = HttpContext.Current.Session["CurrentLogo"].ToString();

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        //////creo una alerta 
                        ////Alertas alerta = new Alertas();
                        ////alerta.IDMarca = entity.IDMarca;
                        ////alerta.CantTR = 1;
                        ////alerta.Activa = true;
                        ////alerta.Prioridad = "A";
                        ////alerta.Tipo = "Por cantidad";
                        ////alerta.Restringido = false;
                        ////alerta.FechaDesde = 1;
                        ////alerta.Nombre = "default marca";
                        //dbContext.Alertas.Add(alerta);

                        dbContext.Multimarcas.Add(entity);
                        dbContext.SaveChanges();
                    }

                    return entity.IDMultimarca.ToString();
                }

            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";

    }

    [WebMethod(true)]
    public static void eliminarLogo(int id, string archivo) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var file = "//files//logos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Multimarcas.Where(x => x.IDMultimarca == id).FirstOrDefault();
                        if (entity != null) {
                            entity.Logo = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadLogo(object sender, EventArgs e) {
        try {
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif") {

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "marca_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpLogo.SaveAs(path);

                Session["CurrentLogo"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex) {
            Session["CurrentLogo"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }



    #region Multimarcas

    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaMarcasAsociadas(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMultimarca) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.MarcasAsociadas.Include("Multimarcas").Include("Marcas")
                    .Where(x => x.IDMultimarca == idMultimarca)
                    .OrderBy(x => x.IDMultimarca)
                    .Select(x => new {
                        IDMarcaAsociada = x.IDMarcaAsociada,
                        IDMultimarca = x.IDMultimarca,
                        IDMarca = x.IDMarca,
                        Marca = x.Marcas.Nombre
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeleteMarcasAsociadas(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.MarcasAsociadas.Where(x => (x.IDMarcaAsociada == id)).FirstOrDefault();

                    if (entity != null) {
                        dbContext.MarcasAsociadas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                    else {
                        throw new Exception("La asociacion de la marca no existe");
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void AsociarMarca(int idMultimarca, int idMarca) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {

                    if (idMultimarca > 0) {
                        var multimarca = dbContext.Multimarcas.Where(x => x.IDMultimarca == idMultimarca).FirstOrDefault();
                        if (multimarca != null) {
                            if (dbContext.MarcasAsociadas.Any(x => (x.IDMultimarca == idMultimarca && x.IDMarca == idMarca)))
                                throw new Exception("Ya se encuentra asignado la marca seleccionado");

                            else {
                                MarcasAsociadas entity = new MarcasAsociadas();
                                entity.IDMultimarca = idMultimarca;
                                entity.IDMarca = idMarca;
                                dbContext.MarcasAsociadas.Add(entity);
                                dbContext.SaveChanges();
                            }
                        }
                        else
                            throw new Exception("La multimarca no existe");
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
    #endregion

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idMarca) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.UsuariosMultimarcas
                    .Where(x => x.IDMultimarca == idMarca)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel() {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        //Tipo = x.Tipo == "A" ? "Admin" : (x.Tipo == "B" ? "Backoffice" : "Dataentry"),
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.UsuariosMultimarcas.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null) {

                        dbContext.UsuariosMultimarcas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDMarca, int IDUsuario, string usuario, string email, string pwd, string tipo) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.UsuariosMultimarcas.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosMultimarcas.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosMultimarcas entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosMultimarcas();
                    else
                        entity = dbContext.UsuariosMultimarcas.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDMultimarca = IDMarca;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    //entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosMultimarcas.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    [WebMethod(true)]
    public static List<ComboViewModel> marcasByFranquicias(int idFranquicia) {
        List<ComboViewModel> listMarcas = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {

            listMarcas = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia).Select(x => new ComboViewModel { ID = x.IDMarca.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listMarcas.Insert(0, todas);

        }
        return listMarcas;
    }
}