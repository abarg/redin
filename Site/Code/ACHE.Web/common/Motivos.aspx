﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Motivos.aspx.cs" Inherits="common_Motivos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Configuración</a></li>
            <li class="last">Motivos</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Motivos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formSocio" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Motivo</label>
                            <input type="text" id="txtNombre" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-sm-3 marca">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" value="" class="form-control" ID="ddlMarcas" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Ciudades" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/motivos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>