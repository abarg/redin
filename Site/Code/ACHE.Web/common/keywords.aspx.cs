﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
public partial class common_keywords : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

             if (Request.QueryString["Envio"] == "false")
                divError.Visible = true;
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    //[System.Web.Services.WebMethod]
    //public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
    //    if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
    //        using (var dbContext = new ACHEEntities()) {
    //            var result = dbContext.Keywords.Include("Comercios")
    //                .OrderBy(x => x.Codigo)
    //                .Select(x => new KeywordsViewModel() {
    //                    IDKeyword = x.IDKeyword,
    //                    IDComercio = x.IDComercio,
    //                    IDMarca = x.Comercios.IDMarca == null ? 0 : (int)x.Comercios.IDMarca,
    //                    Comercio = x.Comercios.NombreFantasia,
    //                    Codigo = x.Codigo,
    //                    Stock = x.Stock,
    //                    FechaInicio = x.FechaInicio,
    //                    FechaFin = x.FechaFin,
    //                    Activo = x.Activo ?"Si":"No",
    //                    Estado = x.Estado
    //                });
    //            if (HttpContext.Current.Session["CurrentMarcasUser"] != null){
    //               WebMarcasUser marca= (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
    //               result = result.Where(x => x.IDMarca == marca.IDMarca);                    
    //                }
    //            if (fechaDesde != string.Empty) {
    //                DateTime dtDesde = DateTime.Parse(fechaDesde);
    //                result = result.Where(x => x.FechaInicio >= dtDesde);
    //            }
    //            if (fechaHasta != string.Empty) {
    //                DateTime dtHasta = DateTime.Parse(fechaHasta);
    //                result = result.Where(x => !x.FechaFin.HasValue || (x.FechaFin.HasValue && x.FechaFin.Value <= dtHasta));
    //            }

    //            return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
    //        }
    //    }
    //    else
    //        return null;
    //}

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Keywords.Where(x => x.IDKeyword == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Keywords.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string Exportar(string comercio, string codigo, string fechaDesde, string fechaHasta) {
        string fileName = "Keywords";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {

                    var info = dbContext.Keywords.Include("Comercios")
                    .OrderBy(x => x.Codigo)
                    .Select(x => new KeywordsViewModel() {
                        IDKeyword = x.IDKeyword,
                        IDComercio = x.IDComercio,
                        IDMarca = x.Comercios.IDMarca == null ? 0 : (int)x.Comercios.IDMarca,
                        Comercio = x.Comercios.NombreFantasia,
                        Codigo = x.Codigo,
                        Stock = x.Stock,
                        FechaInicio = x.FechaInicio,
                        FechaFin = x.FechaFin,
                        Activo = x.Activo ? "Si" : "No",
                        Estado = x.Estado
                    }).AsEnumerable();

                    if (comercio != "")
                        info = info.Where(x => x.Comercio.ToLower().Contains(comercio.ToLower()));

                    if (codigo != "")
                        info = info.Where(x => x.Codigo.ToLower().Contains(codigo.ToLower()));

                    if (fechaDesde != string.Empty) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaInicio >= dtDesde);
                    }
                    if (fechaHasta != string.Empty) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => !x.FechaFin.HasValue || (x.FechaFin.HasValue && x.FechaFin.Value <= dtHasta));
                    }

                    if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                        WebMarcasUser marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                        info = info.Where(x => x.IDMarca == marca.IDMarca);
                    }


                    dt = info.Select(x => new {
                        Comercio = x.Comercio,
                        Codigo = x.Codigo,
                        Stock = x.Stock,
                        FechaInicio = x.FechaInicio.ToString("dd/MM/yyyy"),
                        FechaFin = x.FechaFin.HasValue ? x.FechaFin.Value.ToString("dd/MM/yyyy") : "",
                        Activo = x.Activo,
                        Estado = x.Estado
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}