﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using ClosedXML.Excel;
using ACHE.Business;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using ClosedXML.Excel;

public partial class common_RankingSocios : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");

            int idMarca = 0;
            int idFran = 0;
            int idAdmin = 0;
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                idMarca = usuMarca.IDMarca;
                hdnIDMarca.Value = idMarca.ToString();
            }
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usuFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFran = usuFran.IDFranquicia;
                hdnIDFranquicia.Value = idFran.ToString();
            }
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usuAdmin = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                idAdmin = usuAdmin.IDUsuario;
            }
            cargarMarcas(idFran, idMarca, idAdmin);
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarMarcas(int idFran, int idMarca, int idAdmin) {
        using (var dbContext = new ACHEEntities()) {
            if (idMarca > 0) {
                //marcas = marcas.Where(x => x.IDMarca == idMarca).OrderBy(x => x.Nombre).ToList();
                this.divMarcas.Visible = false;
            }
            else if (idFran > 0 || idAdmin > 0) {
                var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
                if (idFran > 0) {
                    marcas = marcas.Where(x => x.IDFranquicia == idFran).OrderBy(x => x.Nombre).ToList();
                }
                if (marcas != null) {
                    cmbMarcas.DataSource = marcas;
                    cmbMarcas.DataTextField = "Nombre";
                    cmbMarcas.DataValueField = "IDMarca";
                    cmbMarcas.DataBind();
                    cmbMarcas.Items.Insert(0, new ListItem("", ""));
                }
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
        int idFranq = 0;
        int idMarca = 0;

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usuFranq = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranq = usuFranq.IDFranquicia;
        }

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
        }

        if (HttpContext.Current.Session["CurrentUser"] != null || idFranq > 0 || idMarca > 0) {

            using (var dbContext = new ACHEEntities()) {
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

                fechaDesde = (DateTime.Parse(fechaDesde)).ToString(formato);
                fechaHasta = (DateTime.Parse(fechaHasta)).ToString(formato);
                string sql = "exec Rpt_RankingSociosPorFecha " + idFranq + "," + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'";
                var list = dbContext.Database.SqlQuery<RptRankingSociosViewModel>(sql, new object[] { }).ToList();

                var result = list.ToList()
                         .Select(x => new {
                             Socio = x.Socio,
                             Tarjeta = x.Tarjeta,
                             CantTr = x.CantTr,
                             Importe = x.Importe,
                             IDMarca = x.IDMarca,
                             IDFranquicia = x.IDFranquicia
                         }).ToList().AsQueryable();

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string Exportar(string socio, int idFranq, int idMarca, string fechaDesde, string fechaHasta) {
        string fileName = "RankingSocios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    //var info = dbContext.RankingSocios.AsEnumerable();
                    string sql = "exec Rpt_RankingSociosPorFecha " + idFranq + "," + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'";
                    var list = dbContext.Database.SqlQuery<RptRankingSociosViewModel>(sql, new object[] { }).ToList();

                    if (socio != "" && socio != null)
                        list = list.Where(x => x.Socio.ToLower().Contains(socio.ToLower())).ToList();

                    dt = list.Select(x => new  {
                        Socio = x.Socio,
                        Tarjeta = x.Tarjeta,
                        CantTr = x.CantTr,
                        Importe = x.Importe,
                    }).OrderByDescending(x => x.Importe).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}