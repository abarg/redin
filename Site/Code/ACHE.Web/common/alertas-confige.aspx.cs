﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class common_alertas_confige : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentUser"] == null && HttpContext.Current.Session["CurrentFranquiciasUser"] == null)
                Response.Redirect("~/home.aspx");

            else if (HttpContext.Current.Session["CurrentUser"] != null) {
                this.cargarCombos();
            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                this.hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                this.cargarCombos();
            }

            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }
        }
    }


    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

    }

    private void cargarCombos() {
        try { 
            this.cargarMarcas();
            //this.cargarFranquicias();
            this.cargarEmpresas();
            this.cargarComercios();
        }
        catch(Exception e)
        {
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]),"error en aleras-confige common", e.ToString());
        }
    }

    private void cargarMarcas() {
        try {
            int idFranquicia = 0;
            if (int.Parse(hdnIDFranquicias.Value) > 0)
                idFranquicia = int.Parse(hdnIDFranquicias.Value);
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            listMarcas = listMarcas.Where(x => x.IDFranquicia == idFranquicia || idFranquicia == 0).ToList();                           
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            //this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    //private void cargarFranquicias() {
    //    try {
    //        bFranquicia bFranquicia = new bFranquicia();
    //        List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
    //        this.ddlFranquicias.DataSource = listFranquicias;
    //        this.ddlFranquicias.DataValueField = "IDFranquicia";
    //        this.ddlFranquicias.DataTextField = "NombreFantasia";
    //        this.ddlFranquicias.DataBind();

    //        this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
    //    }
    //    catch (Exception ex) {
    //        throw ex;
    //    }
    //}

    private void cargarComercios() {
        using (var dbContext = new ACHEEntities()) {
            int idFranquicia = 0;
            if (int.Parse(hdnIDFranquicias.Value) > 0)
                idFranquicia = int.Parse(hdnIDFranquicias.Value);
            var comercios = dbContext.Comercios.Where(x => x.IDFranquicia == idFranquicia || idFranquicia == 0).OrderBy(x => x.NombreFantasia).Select(x=> new {NombreFantasia = x.NombreFantasia + "-" + x.NroDocumento, IDComercio = x.IDComercio,IDFranquicia = x.IDFranquicia}).ToList();
            if (int.Parse(hdnIDFranquicias.Value) > 0)
                comercios = comercios.Where(x => x.IDFranquicia == int.Parse(hdnIDFranquicias.Value)).ToList();        
            if (comercios != null) {

                searchable.DataSource = comercios;
                searchable.DataTextField = "NombreFantasia";
                searchable.DataValueField = "IDComercio";
                searchable.DataBind();
            }
        }
    }

    private void cargarEmpresas() {
        try {
            using (var dbContext = new ACHEEntities()) {
                int idFranquicia = 0;
                if (int.Parse(hdnIDFranquicias.Value) > 0)
                idFranquicia=int.Parse(hdnIDFranquicias.Value);
                var listEmpresas = dbContext.EmpresasComerciosFranquiciasView.Where(x => x.IDFranquicia == idFranquicia || idFranquicia == 0).OrderBy(x => x.Empresa).Select(x => new { IDEmpresa = x.IDEmpresa, Nombre = x.Empresa }).Distinct().ToList();
                this.ddlEmpresas.DataSource = listEmpresas;
                this.ddlEmpresas.DataValueField = "IDEmpresa";
                this.ddlEmpresas.DataTextField = "Nombre";
                this.ddlEmpresas.DataBind();

                //this.ddlEmpresas.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex) {
            throw ex;
        }

    }

    [WebMethod(true)]
    public static void guardar(int id, string nombre, string entidad, int idEntidad, string prioridad, string tipo, string montoDesde,
        string montoHasta, string cantidad, int fecha, bool activa, bool restringida) {
        try {
            int idFranquicia = 0;

            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }

            if (HttpContext.Current.Session["CurrentUser"] != null || idFranquicia > 0) {
                using (var dbContext = new ACHEEntities()) {
                    Alertas entity;
                    if (id > 0)
                        entity = dbContext.Alertas.Where(x => x.IDAlerta == id).FirstOrDefault();
                    else {
                        entity = new Alertas();
                        if(idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                    }

                    if (entidad.ToLower() == "marca"){
                        entity.IDMarca = idEntidad;
                        idFranquicia = dbContext.Marcas.Where(x => x.IDMarca == idEntidad).FirstOrDefault().IDFranquicia ?? 0;
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }
                    else if (entidad.ToLower() == "comercio"){
                        entity.IDComercio = idEntidad;
                        idFranquicia =dbContext.Comercios.Where(x=> x.IDComercio == idEntidad).FirstOrDefault().IDFranquicia ?? 0;
                        if(idFranquicia >0 )
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }
                    else if (entidad.ToLower() == "empresa"){
                        entity.IDEmpresa = idEntidad;
                        idFranquicia = dbContext.EmpresasComerciosFranquiciasView.Where(x=> x.IDEmpresa == x.IDEmpresa).FirstOrDefault().IDFranquicia;
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }

                    entity.Nombre = nombre;
                    entity.Prioridad = prioridad;
                    entity.Tipo = tipo;
                    if (montoDesde != string.Empty)
                        entity.MontoDesde = int.Parse(montoDesde);
                    else
                        entity.MontoDesde = null;
                    if (montoDesde != string.Empty)
                        entity.MontoHasta = int.Parse(montoHasta);
                    else
                        entity.MontoHasta = null;
                    if (cantidad != string.Empty)
                        entity.CantTR = int.Parse(cantidad);
                    else
                        entity.CantTR = null;
                    if (fecha > 0)
                        entity.FechaDesde = fecha;
                    else
                        entity.FechaDesde = null;

                    entity.Activa = activa;
                    entity.Restringido = restringida;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.Alertas.Add(entity);
                        dbContext.SaveChanges();

                    }
                }
            }

        }
        catch (Exception ex) {
            var msg = ex.Message;
            throw ex;
        }
    }

    [WebMethod(true)]
    public static void guardarAlertasComercios(int id, string nombre, string entidad, int idEntidad, string prioridad, string tipo, string montoDesde,
        string montoHasta, string cantidad, int fecha, bool activa, bool restringida) {
        try {
            int idFranquicia = 0;

            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }

            if (HttpContext.Current.Session["CurrentUser"] != null || idFranquicia > 0) {
                using (var dbContext = new ACHEEntities()) {
                    Alertas entity;
                    if (id > 0)
                        entity = dbContext.Alertas.Where(x => x.IDAlerta == id).FirstOrDefault();
                    else {
                        entity = new Alertas();
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                    }

                    if (entidad.ToLower() == "marca") {
                        entity.IDMarca = idEntidad;
                        idFranquicia = dbContext.Marcas.Where(x => x.IDMarca == idEntidad).FirstOrDefault().IDFranquicia ?? 0;
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }
                    else if (entidad.ToLower() == "comercio") {
                        entity.IDComercio = idEntidad;
                        idFranquicia = dbContext.Comercios.Where(x => x.IDComercio == idEntidad).FirstOrDefault().IDFranquicia ?? 0;
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }
                    else if (entidad.ToLower() == "empresa") {
                        entity.IDEmpresa = idEntidad;
                        idFranquicia = dbContext.EmpresasComerciosFranquiciasView.Where(x => x.IDEmpresa == x.IDEmpresa).FirstOrDefault().IDFranquicia;
                        if (idFranquicia > 0)
                            entity.IDFranquicia = idFranquicia;
                        else
                            throw new Exception("La entidad no tiene una franquicia asociada");
                    }

                    entity.Nombre = nombre;
                    entity.Prioridad = prioridad;
                    entity.Tipo = tipo;
                    if (montoDesde != string.Empty)
                        entity.MontoDesde = int.Parse(montoDesde);
                    else
                        entity.MontoDesde = null;
                    if (montoDesde != string.Empty)
                        entity.MontoHasta = int.Parse(montoHasta);
                    else
                        entity.MontoHasta = null;
                    if (cantidad != string.Empty)
                        entity.CantTR = int.Parse(cantidad);
                    else
                        entity.CantTR = null;
                    if (fecha > 0)
                        entity.FechaDesde = fecha;
                    else
                        entity.FechaDesde = null;

                    entity.Activa = activa;
                    entity.Restringido = restringida;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.Alertas.Add(entity);
                        dbContext.SaveChanges();

                    }
                }
            }

        }
        catch (Exception ex) {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Alertas.Where(x => x.IDAlerta == id).FirstOrDefault();
            if (entity != null) {


                ddlEntidad.Enabled =false;
                if (entity.IDMarca != null) {
                    hdnEntidad.Value = "marca";
                    ddlEntidad.SelectedValue = "2";
                    ddlMarcas.SelectedValue = entity.IDMarca.ToString();
                    ddlMarcas.Enabled = false;
 
                }
                else if (entity.IDComercio != null) {

                    hdnEntidad.Value = "comercio";
                    ddlEntidad.SelectedValue = "3";
                    //ddlComercios.SelectedValue = entity.IDComercio.ToString();
                    //ddlComercios.Enabled = false;
                    searchable.SelectedValue = entity.IDComercio.ToString();
                    searchable.Enabled = false;

                }
                else if (entity.IDEmpresa != null) {

                    hdnEntidad.Value = "empresa";
                    ddlEntidad.SelectedValue = "4";
                    ddlEmpresas.SelectedValue = entity.IDEmpresa.ToString();
                    ddlMarcas.Enabled = false;

                }

                txtNombre.Text = entity.Nombre;
                ddlTipo.Text = entity.Tipo;
                ddlPrioridad.Text = entity.Prioridad;
                txtCantTR.Text = entity.CantTR.HasValue ? entity.CantTR.Value.ToString() : "";
                txtMontoDesde.Text = entity.MontoDesde.HasValue ? entity.MontoDesde.Value.ToString() : "";
                txtMontoHasta.Text = entity.MontoHasta.HasValue ? entity.MontoHasta.Value.ToString() : "";
                ddlFecha.SelectedValue = entity.FechaDesde.HasValue ? entity.FechaDesde.Value.ToString() : "";

                chkActiva.Checked = entity.Activa;
                if (entity.Restringido) {
                    rdbDeterminadas.Checked = true;
                    rdbTodas.Checked = false;
                }
                else {
                    rdbDeterminadas.Checked = false;
                    rdbTodas.Checked = true;
                }
            }
        }
    }
}