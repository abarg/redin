﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VerificacionesPOSe.aspx.cs" Inherits="common_VerificacionesPOSe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
      <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }

        .contentImg{
            float: left;
            width: 150px;
            margin-right: 10px;
            text-align: center;
        }
        .contentImg a{
            color: #369;
            font-size: 12px;
        }
        .contentTxt{
            float: left;
            width: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li class="last">Verificaciones POS edición</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Cargar resultados de verificaciones del POS</h3>
             <form id="formVerifPos" class="form-horizontal" role="form" runat="server">
                <div class=" form-group col-sm-12 col-md-12">
                    <div class="row">
                        <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
                        <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                        <div class="col-sm-3">
                            <label><span class="f_req">*</span> Nro de referencia</label>
                            <asp:TextBox runat="server" id="txtNroDeReferencia"  CssClass="form-control required"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="mostrarTablaVerificaciones();">Buscar</button>
                            <br/>
                        </div>
                    </div>
                </div>
             </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12" id="tblVerificacionesPOS" style="display: none">
               <table class="table table-striped table-bordered mediaTable" >
				<thead>
					<tr>
						<th class="essential persist">Nº</th>
						<th class="optional">Compras</th>
						<th class="optional">Canjes</th>
						<th class="essential">Gift</th>
                        <th class="essential">Nombre de fantasía</th>
                        <th class="essential">Domicilio</th>
						<th class="essential">Nº Terminal</th>
                        <th class="essential">SDS</th>
                        <th class="essential">Nº Estab</th>                        
                        <th class="optional">Calco</th>
						<th class="optional">Display</th>
						<th class="essential">Calco puerta</th>
                        <th class="essential">Verificación compra</th>
                        <th class="essential">Verificación canje &nbsp&nbsp </th>
                        <th class="essential">Verificación gift &nbsp&nbsp</th>

                        <th class="essential">Folletos</th>
                        <%--<th class="essential">Entrega tarjetas</th>--%>
                        <th class="essential">Observaciones terminal</th>
					</tr>
				</thead>
				<tbody id="bodyVerificacionesPOS">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>
             <div class="row">
                <div class="col-sm-8 col-sm-md-8">
                    <button class="btn btn-success" type="button" id="btnGuardar" onclick="guardar();">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/verificacionesPOSe.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

