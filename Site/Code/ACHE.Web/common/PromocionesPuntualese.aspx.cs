﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;

public partial class common_PromocionesPuntualese : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            cargarComercios();
            if (!String.IsNullOrEmpty(Request.QueryString["IDPromocion"])) {
                try {
                    int idPromocion = int.Parse(Request.QueryString["IDPromocion"]);
                    if (idPromocion > 0) {
                        this.hdnIDPromocionPuntual.Value = idPromocion.ToString();
                        cargarDatos(idPromocion);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("PromocionesPuntuales.aspx");
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            MasterPageFile = "~/MasterPageComercios.master";
    }

    private void cargarComercios() {
        int idMarca = 0;
        int idComer = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
            hdnIDMarca.Value = idMarca.ToString();
        }
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null) {
            var usuComer = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            divComercios.Visible = false;
            idComer = usuComer.IDComercio;
        }

        using (var dbContext = new ACHEEntities()) {
            var com = dbContext.Comercios.OrderBy(x => x.NombreFantasia).ToList();
            //var domicilio = dbContext.Domicilios.OrderBy(x => x.Domicilio).ToList();
            if (idMarca > 0)
            {
                com = dbContext.Comercios.Where(x => x.IDMarca == idMarca).OrderBy(x => x.NombreFantasia).ToList();
               
              
            }
            if (idComer > 0)
            {
                com = dbContext.Comercios.Where(x => x.IDComercio == idComer).OrderBy(x => x.NombreFantasia).ToList();
                
            }


            var comercios = com.ToList().Select(x => new  { IDComercio= x.IDComercio.ToString(), NombreFantasia = x.NombreFantasia + " - " + x.Domicilios.Domicilio }).ToList();
            if (comercios != null && comercios.Count() > 0)
            {
                searchable.DataSource = comercios;
                searchable.DataBind();
            }
            else
                searchable.Items.Clear();

        }
    }
    private void cargarDatos(int idpromocion) {
        using (var dbContext = new ACHEEntities()) {
            var prom = dbContext.PromocionesPuntuales.Where(x => x.IDPromocionesPuntuales == idpromocion).FirstOrDefault();
            if (prom != null) {

                this.txtFechaDesde.Text = prom.FechaDesde.ToString("dd/MM/yyyy");
                this.txtFechaHasta.Text = prom.FechaHasta.ToString("dd/MM/yyyy");
                this.txtPuntos.Text = prom.Puntos.ToString();
                this.txtArancel.Text = prom.Arancel.ToString();
               // this.txtArancelVip.Text = (prom.ArancelVip != null) ? prom.ArancelVip.ToString() : "";
                this.txtDescuento.Text = prom.Descuento.ToString();
                this.txtMulPuntos.Text = prom.MultiplicaPuntos.ToString();
                this.txtMulPuntosVip.Text = (prom.MultiplicaPuntosVip != null) ? prom.MultiplicaPuntosVip.ToString() : "";
                this.txtPuntosVip.Text = (prom.PuntosVip != null) ? prom.PuntosVip.ToString() : "";
                this.txtDescuentoVip.Text = (prom.DescuentoVip != null) ? prom.DescuentoVip.ToString() : "";


            }
        }
    }
    [System.Web.Services.WebMethod(true)]
    public static List<int> getComercios(int idpromocion) {
        List<int> ids = new List<int>();
        using (var dbContext = new ACHEEntities()) {
            var comercios = dbContext.PromocionesPorComercio.Where(x => x.IDPromocionesPuntuales == idpromocion).ToList();
            foreach (var comercio in comercios) {
                ids.Add(comercio.IDComercio);
            }
        }
        return ids;
    }

    [System.Web.Services.WebMethod(true)]
    public static void grabar(int idPromocion, string comercios, string fechaDesde, string fechaHasta, string arancel, string arancelVip,string puntos, string mulPuntos, string descuento, string descuentoVip, string puntosVip, string mulPuntosVip) {

        try {
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            string[] ids;
            ids = comercios.Split(",");
            if (ids.Count() > 0 && ids[0] != "null") {
                using (var dbContext = new ACHEEntities()) {
                    PromocionesPuntuales entity;
                    if (idPromocion > 0) {
                        entity = dbContext.PromocionesPuntuales.FirstOrDefault(x => x.IDPromocionesPuntuales == idPromocion);
                        var promPorComercios = dbContext.PromocionesPorComercio.Where(x => x.IDPromocionesPuntuales == idPromocion);
                        dbContext.PromocionesPorComercio.RemoveRange(promPorComercios);
                    }
                    else
                        entity = new PromocionesPuntuales();

                    if (fechaDesde != "")
                        entity.FechaDesde = DateTime.Parse(fechaDesde);

                    if (fechaHasta != "")
                        entity.FechaHasta = DateTime.Parse(fechaHasta).Date.AddTicks(-1);

                    if (arancel != "")
                    entity.Arancel = decimal.Parse(arancel);
                    else
                        entity.Arancel = 0;

                    if (puntos != "")
                    entity.Puntos = decimal.Parse(puntos);
                    else
                        entity.Puntos = 0;

                    if (mulPuntos != "")
                    entity.MultiplicaPuntos = int.Parse(mulPuntos);
                    else
                        entity.MultiplicaPuntos = 1;

                    if (descuento != "")
                    entity.Descuento = int.Parse(descuento);
                    else
                        entity.Descuento = 0;

                    if (descuentoVip != "")
                        entity.DescuentoVip = int.Parse(descuentoVip);
                    else
                        entity.DescuentoVip = null;


                    //if (arancelVip != "")
                    //    entity.ArancelVip = decimal.Parse(arancelVip);
                    //else
                    //    entity.ArancelVip = null;

                    if (puntosVip != "")
                        entity.PuntosVip = decimal.Parse(puntosVip);
                    else
                        entity.PuntosVip = null;

                    if (mulPuntosVip != "")
                        entity.MultiplicaPuntosVip = int.Parse(mulPuntosVip);
                    else
                        entity.MultiplicaPuntosVip = null;

                    entity.PromocionesPorComercio = new List<PromocionesPorComercio>();
                    if (comercios != "") {
                        foreach (var idComercio in ids) {
                            PromocionesPorComercio prom = new PromocionesPorComercio();
                            prom.IDComercio = int.Parse(idComercio);
                            entity.PromocionesPorComercio.Add(prom);
                        }
                    }
                    else if(HttpContext.Current.Session["CurrentComerciosUser"] != null){
                        var usuComer = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                        PromocionesPorComercio prom = new PromocionesPorComercio();
                        prom.IDComercio = usuComer.IDComercio;
                        entity.PromocionesPorComercio.Add(prom);
                    }

                    if (idPromocion > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.PromocionesPuntuales.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            else throw new Exception("Debe seleccionar al menos un comercio");
            }

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
       
    }
}