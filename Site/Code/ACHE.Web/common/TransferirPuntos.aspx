﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TransferirPuntos.aspx.cs" Inherits="common_TransferirPuntos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/transferirPuntos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <form id="formSocio" runat="server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/common/socios.aspx") %>">Socios</a></li>
                <li>Transferir Puntos</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Transferencia de Puntos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none">No se ha podido realizar la transferencia de puntos</div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>		    
			<div class="col-sm-12 col-md-12">
				<div class="row">
					<div class="col-sm-4">
						<label><span class="f_req">* </span>Nro.Documento o Nro.Tarjeta</label>
						<input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />

					</div>
                    <div class="col-sm-3">
                        <br />
                        <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingBuscar" style="display:none" />
                    </div>
				</div>
                <div class="row">
                    <div class="col-sm-8 col-sm-md-8">
                        <label id="lblNro" style="color:green">Ingrese un número de documento o número de tarjeta  para transferir puntos</label>
                    </div>
                </div>
            </div>
            <div  id="datosSocio"  style="display:none">
                <div class="vcard">
                    <ul  style="margin: 0 0 0 -30px;">
                        <li class="v-heading">
                        Datos del socio
						</li>
                         <li>
                            <span class="item-key">Nombre</span>
					        <div class="vcard-item" id="txtNombre"></div>
                        </li>
                         <li>
                            <span class="item-key">Apellido</span>
					        <div class="vcard-item" id="txtApellido"></div>
                        </li>
                         <li>
                            <span class="item-key">Puntos</span>
					        <div class="vcard-item" id="txtPuntos"></div>
                        </li>                 
                    </ul>
                </div>
            </div>                       
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12" id="tblTarjetasDetalle">
            <table id="tablaTarjetas" class="table table-striped table-bordered mediaTable" style="display:none" >
				<thead>
					<tr>
                        <th class="optional" style="width:100px">Transferir</th>
                        <th class="essential" style="width:100px">Numero Tarjeta</th>
                        <th class="essential" style="width:100px">Puntos Disponibles</th>
                        <th class="essential" style="width:100px">Puntos A Transferir</th>
					</tr>
				</thead>
				<tbody id="bodySociosDetalle">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
    </div>
    <div class="row" id="divBuscar2" style="display:none"> 
		<div class="alert alert-danger alert-dismissable" id="divError2" style="display: none"></div>
        <div class="col-sm-4">
			<label><span class="f_req">* </span>Nro.Documento o Nro.Tarjeta de Destino</label>
            <asp:TextBox runat="server" ID="txtNroSocio2" CssClass="form-control" MaxLength="20"></asp:TextBox>
		</div>
        <div class="col-sm-3">
            <br />
            <button class="btn" type="button" id="btnBuscar2" onclick="buscar2();">Buscar</button>
            <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingBuscar2" style="display:none" />
        </div>               
        <%--<div class="row">
            <div class="col-sm-12">
                <label style="margin: 0 0 0 15px;color:green;" id="lblNro2">Ingrese un número de documento o número de tarjeta  para transferir puntos</label>
                <label style="margin: 0 0 0 15px;color:red;display:none" id="errorSocioNro2">No se encontraron tarjetas asociadas con ese Nro.Documento ó Nro.Tarjeta</label>
            </div>
       </div>--%>
        <div id="datosSocioDestino" style="display:none">
            <div class="vcard">
                <ul  style="margin: 20px 0 0 -30px;">
                    <li class="v-heading">
                    Datos del socio Destino
				    </li>
                        <li>
                        <span class="item-key">Nombre</span>
					    <div class="vcard-item" id="txtNombreDestino"></div>
                    </li>
                        <li>
                        <span class="item-key">Apellido</span>
					    <div class="vcard-item" id="txtApellidoDestino"></div>
                    </li>           
                </ul>
            </div>
        </div>
        <div class="col-sm-3" id="divddlTarjeta" style="display:none">
             <br />
            <label><span class="f_req">*</span> Tarjeta Destino</label>
            <asp:DropDownList runat="server" ID="ddlTrTarjeta" CssClass="form-control required"></asp:DropDownList>
            <br />                                        
        </div>
        <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="col-sm-8 col-sm-md-8" id="transPtos" style="display:none"> 
                    <br />
                    <br />                                        
                    <button class="btn btn-success" type="button" onclick="transferirPuntos();">Transferir Puntos</button>
                </div>
            </div>
        </div>
            <div class="col-sm-12">
                <label style="margin: 0 0 0 5px;color:red;display:none" id="lblTransPtos">Seleccione una tarjeta de destino para realizar la transferencia</label>                
            </div>
    </div>
    </form>
</asp:Content>

