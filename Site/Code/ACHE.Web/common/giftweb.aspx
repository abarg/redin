﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="giftweb.aspx.cs" Inherits="common_giftweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />   
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/giftweb.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Gift Web</li>
        </ul>
    </div>

     <div class="row">
        <div class="col-sm-10 col-md-10">
            <h1 class="invoice_heading">Gift Web</h1>
            
            <br /><br />
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

			<form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="formSep" style="border-bottom:0px">
				    
                    <h3 class="heading" style="color:#428bca">1. Seleccione la operación</h3>
                    <div id="divPaso1">
                        <div class="row">
					        <div class="col-lg-3">
						        <label><span class="f_req">*</span> Operación</label>
                                <select id="ddlTrTipo" class="form-control required" onchange="changeTipoOperacion(this.value);">
                                    <option value="Carga">Carga</option>
                                    <option value="Descarga">Descarga</option>
                                </select>                                         
					        </div>
                            <div class="col-lg-4">
                                
                            </div>
				        </div>
                    </div>
                    <h3 class="heading" style="margin-top: 20px;color:#428bca">2. Seleccione el socio</h3>
                    <div class="alert alert-danger alert-dismissable" id="divErrorTrSocio" style="display: none"></div>
                    <div id="divPaso2">
                        <div class="row"> 
                            <div class="col-lg-3">
                                <label><span class="f_req">*</span> Buscar por</label>
                                <select id="ddlTrBuscarSocio" class="form-control">
                                    <option value="Tarjeta">Nro Tarjeta</option>
                                    <option value="Nombre">Nombre y apellido</option>
                                    <option value="DNI">DNI</option>
                                </select>                                         
                            </div>
                            <div class="col-lg-4">
                                <label><span class="f_req">*</span> Valor a buscar</label>
                                <input type="text" id="txtValorSocio" class="form-control" MaxLength="20"/>
                            </div>
                            <div class="col-lg-2">
                                <label>&nbsp;</label>
                                <button id="btnBuscarSocio" class="btn" type="button" onclick="buscarSocios();">Buscar</button>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-lg-4">
                                <label><span class="f_req">*</span> Socio</label>
                                <select id="ddlTrSocio" class="form-control required" onchange="buscarTarjetas();">
                                            
                                </select>                                         
                            </div>
                            <div class="col-lg-4" id="divResultado1"> <%--style="display:none"--%>
                                <label><span class="f_req">*</span> Tarjeta</label>
                                <select id="ddlTrTarjeta" class="form-control required" onchange="getSaldo(this.value);">
                                            
                                </select>                                         
                            </div>
                            <%--<div class="col-lg-2">
                                <label>&nbsp;</label>
                                <a href="#" onclick="validarSocio();">Validar socio</a>
                            </div>--%>
                        </div>
                        <div class="row"> 
                            <div class="col-lg-4">
                                <label id="lblDni" style="font-weight:bold;"></label>
                            </div>
                            <div class="col-lg-4">
                                <label id="lblSaldo" style="font-weight:bold;"></label>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-lg-3">
                                <img src="http://www.placehold.it/180x120/EFEFEF/AAAAAA" style="max-width:180px; max-height:120px" id="imgSocio" />
                                
                            </div>
                            <div class="col-lg-6">
                                <label>Ultimas transacciones</label> 
                                <table class="table" id="tbDetalle">
			                        <thead>
				                        <tr>
					                        <th>Fecha</th> 
                                            <th>Hora</th> 
                                            <th>Operacion</th> 
                                            <th>Comercio</th> 
                                            <th>Importe original</th>
                                            <th>Importe ahorro</th>
                                            <th>Puntos</th>
				                        </tr>
			                        </thead>
			                        <tbody id="tBodyHistorial">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <h3 class="heading" style="margin-top: 20px;color:#428bca">3. Seleccione un comercio</h3>
                    <div class="alert alert-danger alert-dismissable" id="divErrorTrComercio" style="display: none"></div>
                    <div id="divPaso3">
                        <div class="row"> 
                            <div class="col-lg-3">
                                <label><span class="f_req">*</span> Buscar comercio por</label>
                                <select id="ddlTrBuscarComercio" class="form-control required">
                                    <option value="ID">ID</option>
                                    <option value="Nombre">Nombre</option>
                                    <option value="SDS">SDS</option>
                                    <option value="Terminal">Terminal</option>
                                    <option value="CUIT">CUIT</option>
                                </select>                                         
                            </div>
                            <div class="col-lg-4">
                                <label><span class="f_req">*</span> Valor a buscar</label>
                                <input type="text" id="txtValorComercio" class="form-control" MaxLength="20"/>
                            </div>
                            <div class="col-lg-2">
                                <label>&nbsp;</label>
                                <button id="btnBuscarComercios" class="btn" type="button" onclick="buscarComercios();">Buscar</button>
                            </div>
                        </div>
			            <div class="row">
                            <div class="col-sm-8">
						        <label><span class="f_req">*</span> Comercio</label>
                                <asp:DropDownList runat="server" ID="ddlTrComercio" class="form-control chzn_b"
                                    data-placeholder="Seleccione un comercio">

                                </asp:DropDownList>
                                <%--<select ID="ddlTrComercio" class="form-control chzn_b" data-placeholder="Seleccione un comercio" style="width:100% important!">
                                    <option value=""></option>
                                </select>--%>
					        </div>
                        </div>
                        <div class="row"> 
                            <div class="col-lg-2">
                                <label>Punto de Venta</label>
                                 <input type="text" id="txtPuntoVenta" class="form-control" MaxLength="4"/>                                    
                            </div>
                            <div class="col-lg-3">
                                <label>Tipo de Comprobante </label>
                                <select id="ddlTipoComprobante" class="form-control required">
                                    <option value=""></option>
                                    <option value="Ticket A">Ticket A</option>
                                    <option selected="selected" value="Ticket B">Ticket B</option>
                                    <option value="Ticket C">Ticket C</option>
                                </select>   
                            </div>
                            <div class="col-lg-3">
                                <label>N° de Comprobante</label>
                                <input type="text" id="txtNroComprobante" class="form-control" MaxLength="10"/>
                            </div>
                           
                        </div>
                    </div>
                    <h3 class="heading" style="margin-top: 20px;color:#428bca">4. Detalle el monto</h3>
                    <div id="divPaso4">
                        <div class="row">
                            <div class="col-sm-3">
                                <label><span class="f_req">*</span> Importe</label>
						        <input type="text" id="txtTrImporte" class="form-control required" />
					        </div>
                            <div class="col-sm-4">
						        <label>Descripción</label>
						        <input type="text" id="txtTrDescripcion" class="form-control" maxlength="100" />
					        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8" style="margin-top: 20px;">						    
                            <button id="btnGrabar" class="btn btn-success" type="button" onclick="confirmarOperacion(false);">Aceptar y ver comprobante</button>
                            <button style="margin-left: 25px;" id="btnGrabarYCargarOtra" class="btn" type="button" onclick="confirmarOperacion(true);">Aceptar y realizar otra operacion</button>
                        </div>
					</div>
                </div>
            </form>

        </div>
    </div>
</asp:Content>

