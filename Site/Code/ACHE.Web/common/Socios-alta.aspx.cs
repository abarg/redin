﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class common_Socios_alta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarMarcas();
          if (HttpContext.Current.Session["CurrentMarcasUser"] != null){
              var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
              hdnIDMarca.Value = usu.IDMarca.ToString();

          }
    }
    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }
    [WebMethod(true)]
    public static SociosViewModel buscar(string documento)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Socios.Where(x => x.NroDocumento == documento).FirstOrDefault();
                    if (aux != null)
                    {
                        return new SociosViewModel()
                        {
                            Nombre = aux.Nombre,
                            Apellido = aux.Apellido,
                            NroCuenta = aux.FechaNacimiento.HasValue ? aux.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "",
                            Sexo = aux.Sexo,
                            Email = aux.Email,
                            Celular = aux.Celular,
                            IDSocio = aux.IDSocio
                        };
                    }
                    else
                        return null;
                }
            }
            else
                return null;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void cargarMarcas()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
            }
            else
                listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}