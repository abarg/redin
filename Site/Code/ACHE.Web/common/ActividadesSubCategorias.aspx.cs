﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ActividadesSubCategorias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCategorias();
            cargarSubCategorias();
        }

    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.ActividadCategorias.OrderBy(x => x.Categoria).Select(x => new { IDCategoria = x.IDCategoria, Categoria = x.Categoria.ToUpper() }).ToList();
            if (categorias != null)
            {
                cmbCategorias.DataSource = categorias;
                cmbCategorias.DataTextField = "Categoria";
                cmbCategorias.DataValueField = "IDCategoria";
                cmbCategorias.DataBind();
                cmbCategorias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarSubCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var subcategorias = dbContext.ActividadSubCategorias.OrderBy(x => x.SubCategoria).ToList();
            if (subcategorias != null)
            {
                cmbSubCategorias.DataSource = subcategorias;
                cmbSubCategorias.DataTextField = "SubCategoria";
                cmbSubCategorias.DataValueField = "IDSubCategoria";
                cmbSubCategorias.DataBind();
                cmbSubCategorias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }



    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {

        using (var dbContext = new ACHEEntities())
                    {
                        var sql = @"SELECT COUNT(ASN.IDActividad) as CantidadBeneficiarios, AC.Categoria as Categoria, ASUB.SubCategoria AS SubCategoria, ASCC.Nombre as Secretaria FROM ActividadSecretariaCategoria AS ASCC
                                    inner JOIN ActividadCategorias AS AC ON ASCC.IDSecretaria = AC.IDSecretaria  
                                    inner JOIN ActividadSubCategorias AS ASUB ON ASUB.IDCategoria = AC.IDCategoria
                                    inner JOIN Actividades AS A ON A.IDSubCategoria = ASUB.IDSubCategoria
                                    join ActividadSocioNivel as ASN ON ASN.IDActividad = A.IDActividad
                                    group by AC.Categoria, ASUB.SubCategoria, ASCC.Nombre";


                        var result = dbContext.Database.SqlQuery<ActividadSubCategoriasView>(sql).AsQueryable();


                        return result.ToDataSourceResult(take, skip, sort, filter);
                }

    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var subcategoria = dbContext.ActividadSubCategorias.Where(x => x.IDSubCategoria == id).FirstOrDefault();
                    if (subcategoria != null)
                    {
                        throw new Exception("No se puede eliminar ya que la ciudad pertenece a un comercio y/o socio");
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string txtCategoria)
    {
        string fileName = "Categorias Beneficios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.ActividadSubCategorias.Include("ActividadCategorias").OrderBy(x => x.IDSubCategoria)
                        .Select(x => new {
                            IDSubCategoria = x.IDSubCategoria,
                            SubCategoria = x.SubCategoria,
                            Categoria = x.ActividadCategorias.Categoria.ToUpper(),
                            IDCategoria = x.IDCategoria,

                        }).AsEnumerable();


                    if (!string.IsNullOrEmpty(txtCategoria))
                        info = info.Where(x => x.Categoria.ToLower().Contains(txtCategoria.ToLower()));

                    dt = info.Select(x => new
                    {
                        IDSubCategoria = x.IDSubCategoria,
                        SubCategoria = x.SubCategoria,
                        Categoria = x.Categoria.ToUpper(),
                        IDCategoria = x.IDCategoria,

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }


    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}