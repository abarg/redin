﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;
using System.Diagnostics;

public partial class common_Comerciose : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
            Session["CurrentImagen"] = null;
            Session["CurrentLogo"] = null;
            Session["CurrentFicha"] = null;
            this.txtFechaAlta.Text = DateTime.Now.ToString("dd/MM/yyyy");

            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
                cargarCombos();

            if (!String.IsNullOrEmpty(Request.QueryString["IDComercio"]))
            {
                this.hfIDComercio.Value = Request.QueryString["IDComercio"];
                if (!this.hfIDComercio.Value.Equals("0"))
                {
                    this.cargarDatosComercio(Convert.ToInt32(Request.QueryString["IDComercio"]));
                }
            }
            else {
                using (var dbContext = new ACHEEntities()) {
                    string aux = dbContext.Comercios.OrderByDescending(x => x.SDS.Length).ThenByDescending(x => x.SDS).FirstOrDefault().SDS;
                    txtSDS.Text = (int.Parse(aux) + 1).ToString();
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void cargarCombos()
    {
        try
        {
            int idfranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idfranquicia = usu.IDFranquicia;
            }
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            if (idfranquicia > 0)
                listMarcas = bMarca.getMarcasByFranquicia(idfranquicia);
            else
                listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = bMarca.getMarcas();
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

            bRubros bRubros = new bRubros();
            this.ddlRubro.DataSource = bRubros.getRubros();
            this.ddlRubro.DataValueField = "IDRubro";
            this.ddlRubro.DataTextField = "Nombre";
            this.ddlRubro.DataBind();
            this.ddlRubro.Items.Insert(0, new ListItem("", ""));

            if (idfranquicia == 0)
            {
                bFranquicia bFranquicia = new bFranquicia();
                this.ddlFranquicias.DataSource = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();
                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
            }
            else
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];

                this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
            }
            ddlProvincia.DataSource = Common.LoadProvincias();
            ddlProvincia.DataTextField = "Nombre";
            ddlProvincia.DataValueField = "ID";
            ddlProvincia.DataBind();

            ddlProvincia2.DataSource = Common.LoadProvincias();
            ddlProvincia2.DataTextField = "Nombre";
            ddlProvincia2.DataValueField = "ID";
            ddlProvincia2.DataBind();

            var paises = Common.LoadPaises();
            if (paises != null) {
                ddlPais.DataSource = paises;
                ddlPais.DataTextField = "Nombre";
                ddlPais.DataValueField = "ID";
                ddlPais.DataBind();

                ddlPais2.DataSource = paises;
                ddlPais2.DataTextField = "Nombre";
                ddlPais2.DataValueField = "ID";
                ddlPais2.DataBind();
            }

            using (var dbContext = new ACHEEntities())
            {
                var dealers = dbContext.Dealer.Where(x => x.Activo).OrderBy(x => x.Nombre).ToList();
                this.ddlDealer.DataSource = dealers;
                this.ddlDealer.DataTextField = "Nombre";
                this.ddlDealer.DataValueField = "IDDealer";
                this.ddlDealer.DataBind();
                this.ddlDealer.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void cargarImagenes(int IDComercio) {
        using (var dbContext = new ACHEEntities()) {
            var imagenes = dbContext.ImagenesComercios.Where(x => x.IDComercio == IDComercio)
            .Select(x => new ImagenesComerciosViewModel {
                URL =  x.URL,
                IDImagenComercio = x.IDImagenComercio,
                IDComercio = x.IDComercio
            }).ToList();
            if (imagenes.Count() > 0) {
                rptImagenes.DataSource = imagenes;
                rptImagenes.DataBind();
            }
        }    
    }

    protected void btnSubirFoto_Click(object sender, EventArgs e) {
        litImgErrorFormato.Visible = false ;
        if (IsValid == true) {
            string ext = System.IO.Path.GetExtension(flpFoto.FileName).ToLower();
            if (ext == ".jpg" || ext == ".gif" || ext == ".png") {
                string uniqueName = "ImagenesComercio_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/comercios/"), uniqueName);
                //save the file to our local path
                flpFoto.SaveAs(path);
                using (var dbContext = new ACHEEntities()) {
                    ImagenesComercios imagen = new ImagenesComercios();
                    imagen.URL = uniqueName;
                    imagen.IDComercio = int.Parse(hfIDComercio.Value);
                    dbContext.ImagenesComercios.Add(imagen);
                    dbContext.SaveChanges();

                    Response.Redirect("Comerciose.aspx?IDComercio=" + hfIDComercio.Value + "&modo=I");
                }
            }
            else litImgErrorFormato.Visible = true;

        }
    }

    [System.Web.Services.WebMethod]
    public static void DeleteImagen(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.ImagenesComercios.Where(x => x.IDImagenComercio == id).FirstOrDefault();
            if (entity != null) {
                dbContext.ImagenesComercios.Remove(entity);
                dbContext.SaveChanges();
            }
        }
    }

    private void cargarDatosComercio(int IDComercio)
    {
        try
        {
            cargarImagenes(IDComercio);
            //Comercio
            bComercio bComercio = new bComercio();
            Comercios oComercio = bComercio.getComercio(IDComercio);            

            this.txtFechaAlta.Text = oComercio.FechaAlta.ToString("dd/MM/yyyy");
            this.txtSDS.Text = oComercio.SDS;
            this.txtRazonSocial.Text = oComercio.RazonSocial;
            this.chkCasaMatriz.Checked = oComercio.CasaMatriz;
            this.ddlTipoDoc.SelectedValue = oComercio.TipoDocumento;
            this.txtNroDocumento.Text = oComercio.NroDocumento;
            this.txtNombreEst.Text = oComercio.NombreEst;

            this.txtTelefono.Text = oComercio.Telefono;
            this.txtCelular.Text = oComercio.Celular;
            this.txtResponsable.Text = oComercio.Responsable;
            this.txtCargo.Text = oComercio.Cargo;
            this.txtNombreFantasia.Text = oComercio.NombreFantasia;
            if (oComercio.IDMarca.HasValue)
                this.ddlMarcas.SelectedValue = oComercio.IDMarca.Value.ToString();
            else
                this.ddlMarcas.SelectedValue = "";
            if (oComercio.IDFranquicia.HasValue)
                this.ddlFranquicias.SelectedValue = oComercio.IDFranquicia.Value.ToString();
            else
                this.ddlFranquicias.SelectedValue = "";
            this.txtWeb.Text = oComercio.Web;
            this.txtEmail.Text = oComercio.Email;
            this.txtUrl.Text = oComercio.Url;
            this.txtTwitter.Text = oComercio.twitter;
            this.chkActivo.Checked = oComercio.Activo;
            this.txtObservaciones.Text = oComercio.Observaciones;
            this.ddlRubro.SelectedValue = oComercio.Rubro.ToString();
            int idRubro = int.Parse(ddlRubro.SelectedValue);
            if (idRubro > 0)
            {
                ddlSubRubro.DataSource = cargarSubRubros(idRubro);
                this.ddlSubRubro.DataValueField = "ID";
                this.ddlSubRubro.DataTextField = "Nombre";
                this.ddlSubRubro.DataBind();
                //this.ddlSubRubro.DataValueField = oComercio.SubRubro.ToString();
                if (oComercio.IDSubRubro.HasValue)
                    this.ddlSubRubro.SelectedValue = oComercio.IDSubRubro.ToString();

                //this.ddlSubRubro.DataTextField = cargarSubRubro(oComercio.SubRubro.ToString());
                //this.ddlRubro.DataBind();
            }
            if (oComercio.IDDealer.HasValue)
                this.ddlDealer.SelectedValue = oComercio.IDDealer.Value.ToString();
            else
                this.ddlDealer.SelectedValue = "";

            if (oComercio.FechaAltaDealer.HasValue)
                txtFechaAltaDealer.Text = oComercio.FechaAltaDealer.Value.ToString("dd/MM/yyyy");

            if (!string.IsNullOrEmpty(oComercio.Logo))
            {
                lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + oComercio.Logo;
                lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                lnkLogoDelete.Attributes.Add("onclick", "return deleteUpload('" + oComercio.Logo + "', 'Logo')");
                divLogo.Visible = true;

                imgLogo.ImageUrl = "/files/logos/" + oComercio.Logo;
            }
            else
                imgLogo.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";

            if (!string.IsNullOrEmpty(oComercio.FichaAlta))
            {
                lnkFicha.NavigateUrl = "/fileHandler.ashx?type=fichas&module=admin&file=" + oComercio.FichaAlta;
                lnkFichaDelete.NavigateUrl = "javascript: void(0)";
                lnkFichaDelete.Attributes.Add("onclick", "return deleteUpload('" + oComercio.FichaAlta + "', 'Ficha')");
                divFicha.Visible = true;

                imgFicha.ImageUrl = "/files/fichas/" + oComercio.FichaAlta;
            }
            else
                imgFicha.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";


            //Formas Pago
            if (oComercio.FormaPago != null) {
                if (oComercio.FormaPago.Equals("D")) {
                    this.rdbFormaPago_Debito.Checked = true;
                    this.rdbFormaPago_Tarjeta.Checked = false;
                    this.rdbFormaPago_MercadoPago.Checked = false;
                    this.txtFormaPago_Banco.Text = oComercio.FormaPago_Banco;
                    if (oComercio.FormaPago_TipoCuenta != null) {
                        if (oComercio.FormaPago_TipoCuenta.Equals("1")) this.ddlFormaPago_TipoCuenta.SelectedValue = "1";
                        else if (oComercio.FormaPago_TipoCuenta.Equals("2")) this.ddlFormaPago_TipoCuenta.SelectedValue = "2";
                    }

                    this.txtFormaPago_NroCuenta.Text = oComercio.FormaPago_NroCuenta;
                    this.txtFormaPago_CBU.Text = oComercio.FormaPago_CBU;
                    this.txtFormaPago_CBU_Rep.Text = oComercio.FormaPago_CBU;
                }
                else if (oComercio.FormaPago.Equals("T")) {
                    this.rdbFormaPago_Debito.Checked = false;
                    this.rdbFormaPago_Tarjeta.Checked = true;
                    this.rdbFormaPago_MercadoPago.Checked = false;

                    this.txtFormaPago_Tarjeta.Text = oComercio.FormaPago_Tarjeta;
                    this.txtFormaPago_BancoEmisor.Text = oComercio.FormaPago_BancoEmisor;
                    this.txtFormaPago_NroTarjeta.Text = oComercio.FormaPago_NroTarjeta;
                    this.txtFormaPago_FechaVto.Text = oComercio.FormaPago_FechaVto;
                    this.txtFormaPago_CodigoSeg.Text = oComercio.FormaPago_CodigoSeg;
                }
                else if (oComercio.FormaPago.Equals("M")) {
                    this.rdbFormaPago_MercadoPago.Checked = true;
                    this.rdbFormaPago_Debito.Checked = false;
                    this.rdbFormaPago_Tarjeta.Checked = false;
                }
            }

            //Contacto
            if (oComercio.IDContacto != 0)
            {
                Contactos oContacto = oComercio.Contactos;

                this.txtContacto_Nombre.Text = oContacto.Nombre;
                this.txtContacto_Apellido.Text = oContacto.Apellido;
                this.txtContacto_Cargo.Text = oContacto.Cargo;
                this.txtContacto_Telefono.Text = oContacto.Telefono;
                this.txtContacto_Celular.Text = oContacto.Celular;
                this.txtContacto_Email.Text = oContacto.Email;
                this.txtContacto_Observaciones.Text = oContacto.Observaciones;
                this.txtContacto_Documento.Text = oContacto.NroDocumento;
            }

            //Domicilio Fiscal
            if (oComercio.IDDomicilioFiscal != 0)
            {
                Domicilios oDomicilioF = oComercio.Domicilios;

                var idPais = 0;
                string pais = oDomicilioF.Pais.ToString().ToLower();
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Paises.Where(x => x.Nombre.ToLower() == pais).FirstOrDefault();
                    if (aux != null)
                    {
                        idPais = aux.IDPais;
                        if (idPais > 0)
                        {

                            this.ddlPais.SelectedValue = idPais.ToString();
                            
                            List<ComboViewModel> listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
                            ddlProvincia.DataSource = listProvincias;
                            ddlProvincia.DataTextField = "Nombre";
                            ddlProvincia.DataValueField = "ID";
                            ddlProvincia.DataBind();
                            this.ddlProvincia.SelectedValue = oDomicilioF.Provincia.ToString();
                        }
                    }
                }
                this.ddlProvincia.SelectedValue = oDomicilioF.Provincia.ToString();
                ddlCiudad.DataSource = Common.LoadCiudades(oDomicilioF.Provincia);
                ddlCiudad.DataTextField = "Nombre";
                ddlCiudad.DataValueField = "ID";
                ddlCiudad.DataBind();
                ddlCiudad.Items.Insert(0, new ListItem("", ""));

                if (oDomicilioF.Ciudad.HasValue)
                    ddlCiudad.SelectedValue = oDomicilioF.Ciudad.Value.ToString();
                this.txtDomicilio.Text = oDomicilioF.Domicilio;
                this.txtCodigoPostal.Text = oDomicilioF.CodigoPostal;
                this.txtTelefonoDom.Text = oDomicilioF.Telefono;
                this.txtFax.Text = oDomicilioF.Fax;
                this.txtPisoDepto.Text = oDomicilioF.PisoDepto;

            }

            //Domicilio Comercial
            if (oComercio.IDDomicilio != 0)
            {
                Domicilios oDomicilioC = oComercio.Domicilios;

                using (var dbContext = new ACHEEntities()) {
                    var pais = dbContext.Paises.Where(x => x.Nombre == oDomicilioC.Pais).FirstOrDefault();
                    if (pais!=null){
                        var idPais= pais.IDPais;

                    if (idPais > 0) {
                        this.ddlPais2.SelectedValue = idPais.ToString();                   
                            List<ComboViewModel> listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
                        ddlProvincia2.DataSource = listProvincias;                  
                        ddlProvincia2.DataTextField = "Nombre";
                        ddlProvincia2.DataValueField = "ID";
                        ddlProvincia2.DataBind();
                        this.ddlProvincia2.SelectedValue = oDomicilioC.Provincia.ToString();

                        }
                    }
                }

                ddlCiudad2.DataSource = Common.LoadCiudades(oDomicilioC.Provincia);
                ddlCiudad2.DataTextField = "Nombre";
                ddlCiudad2.DataValueField = "ID";
                ddlCiudad2.DataBind();
                ddlCiudad2.Items.Insert(0, new ListItem("", ""));

                if (oDomicilioC.Ciudad.HasValue)
                    ddlCiudad2.SelectedValue = oDomicilioC.Ciudad.Value.ToString();
                this.txtDomicilio2.Text = oDomicilioC.Domicilio;
                this.txtCodigoPostal2.Text = oDomicilioC.CodigoPostal;
                this.txtTelefonoDom2.Text = oDomicilioC.Telefono;
                this.txtFax2.Text = oDomicilioC.Fax;
                this.txtPisoDepto2.Text = oDomicilioC.PisoDepto;
                this.txtLatitud.Text = oDomicilioC.Latitud;
                this.txtLongitud.Text = oDomicilioC.Longitud;


                if (oDomicilioC.Ciudad.HasValue)
                {
                    cmbZona.DataSource = Common.LoadZonas(oDomicilioC.Ciudad.Value);
                    cmbZona.DataTextField = "Nombre";
                    cmbZona.DataValueField = "ID";
                    cmbZona.DataBind();
                    cmbZona.Items.Insert(0, new ListItem("", ""));

                    if (oComercio.IDZona.HasValue)
                        cmbZona.SelectedValue = oComercio.IDZona.Value.ToString();
                }

            }

            //Facturacion
            this.ddlIVA.SelectedValue = oComercio.CondicionIva;
            this.txtEnvioFc.Text = oComercio.EmailsEnvioFc;
            if (oComercio.CostoFijo.HasValue)
                this.txtCostoFijo.Text = oComercio.CostoFijo.Value.ToString();
            if (oComercio.TopeVenta.HasValue)
                this.txtTopeVenta.Text = oComercio.TopeVenta.Value.ToString();
            if (oComercio.ConRetenciones)
                this.chkRetencion.Checked = true;

            //Alertas
            this.txtEmailAlerta.Text = oComercio.EmailAlertas;
            this.txtCelularAlerta.Text = oComercio.CelularAlertas;
            if (oComercio.EmpresaCelularAlertas != null)
                this.txtCelularEmpresaAlerta.SelectedValue = oComercio.EmpresaCelularAlertas;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(/*DATOS PRINCIPALES*/int IDComercio, string SDS, string RazonSocial, string TipoDoc, string NroDocumento, string NumEst, string NombreEst, string Rubro, string SubRubro, string Telefono, string Celular
        , string Responsable, string Cargo, string NombreFantasia, int IDMarca, int IDFranquicia, string IVA, string Web, string Email, string Url,bool Activo, string Observaciones, int IDDealer, string FechaAltaDealer
        /*DOMICILIO COMERCIAL*/, string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax, string PisoDepto, string Lat, string Long, int IDZona,
        /*DOMICILIO FISCAL*/string Pais2, string Provincia2, string Ciudad2, string Domicilio2, string CodigoPostal2, string TelefonoDom2, string Fax2, string PisoDepto2
        /*FORMA DE PAGO*/, string FormaPago, string FormaPago_Banco, string FormaPago_TipoCuenta, string FormaPago_NroCuenta, string FormaPago_CBU, string FormaPago_Tarjeta, string FormaPago_BancoEmisor, string FormaPago_NroTarjeta, string FormaPago_FechaVto, string FormaPago_CodigoSeg ,
        /*CONTACTO*/string Contacto_Nombre, string Contacto_Apellido, string Contacto_Cargo, string Contacto_Telefono, string Contacto_Celular, string Contacto_Email, string Contacto_Observaciones, string Contacto_Documento
        /*MAS DATOS PRINCIPALES*/, string EnvioMailsFc, bool CasaMatriz, string emailAlertas, string celularAlertas, string celularEmpresaAlertas, string CostoFijo,string TopeVenta, string Twitter, string CostoPOSPropio, bool ConRetenciones)
    {
        try
        {

            //Comercio
            Comercios entity;
            using (var dbContext = new ACHEEntities())
            {

                if (SDS != string.Empty)
                {
                    var _oComercio = dbContext.Comercios.FirstOrDefault(c => c.SDS == SDS);

                    var aux = dbContext.Comercios.FirstOrDefault(s => s.SDS == SDS);
                    if (aux != null && aux.IDComercio != IDComercio)
                        throw new Exception("Ya existe un Comercio con el Número de SDS ingresado");
                }


                if (IDComercio > 0)
                    entity = dbContext.Comercios.FirstOrDefault(s => s.IDComercio == IDComercio);
                else
                {
                    entity = new Comercios();
                    entity.FechaAlta = DateTime.Now;
                }

                entity.SDS = SDS;
                entity.RazonSocial = RazonSocial != null && RazonSocial != "" ? RazonSocial.ToUpper() : "";
                entity.CasaMatriz = CasaMatriz;
                entity.TipoDocumento = TipoDoc;

                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "de tipo", TipoDoc);


                entity.NroDocumento = NroDocumento;

                entity.NombreEst = NombreEst != null && NombreEst != ""? NombreEst.ToUpper() : "";

                entity.Telefono = Telefono;
                entity.Celular = corregirCelular(Celular);
                entity.Responsable = Responsable != null && Responsable != "" ? Responsable.ToUpper() : "";
                entity.Cargo = Cargo != null && Cargo != "" ? Cargo.ToUpper() : "";
                entity.NombreFantasia = NombreFantasia != null && NombreFantasia != "" ? NombreFantasia.ToUpper() : "";
                if (IDMarca > 0)
                    entity.IDMarca = IDMarca;
                else
                    entity.IDMarca = null;
                if (IDFranquicia > 0)
                    entity.IDFranquicia = IDFranquicia;
                else
                    entity.IDFranquicia = null;
                entity.Web = Web;
                entity.Email = corregirEmail(Email);
                entity.Url = Url;
                entity.twitter = Twitter != null? Twitter : "";
                entity.Activo = Activo;
                entity.Observaciones = Observaciones;
                entity.Rubro = int.Parse(Rubro);
                if (SubRubro != null && SubRubro != "null" && SubRubro != "" && SubRubro != "0")
                    entity.IDSubRubro = int.Parse(SubRubro);
                else
                    entity.IDSubRubro = null;
                if (IDDealer > 0)
                    entity.IDDealer = IDDealer;
                else
                    entity.IDDealer = null;

                if (FechaAltaDealer != null && FechaAltaDealer != "")
                    entity.FechaAltaDealer = DateTime.Parse(FechaAltaDealer);
                else
                    entity.FechaAltaDealer = null;

                if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                    entity.Logo = HttpContext.Current.Session["CurrentLogo"].ToString();
                if (HttpContext.Current.Session["CurrentFicha"] != null && HttpContext.Current.Session["CurrentFicha"] != "")
                    entity.FichaAlta = HttpContext.Current.Session["CurrentFicha"].ToString();       
        


                //Formas de Pago
                if (FormaPago != "")
                {
                    if (FormaPago.Equals("D"))
                    {
                        entity.FormaPago_Banco = FormaPago_Banco != null && FormaPago_Banco != "" ? FormaPago_Banco.ToUpper() : "";
                        entity.FormaPago_TipoCuenta = FormaPago_TipoCuenta;
                        entity.FormaPago_NroCuenta = FormaPago_NroCuenta;
                        entity.FormaPago_CBU = FormaPago_CBU;
                    }
                    else if (FormaPago.Equals("T"))
                    {
                        entity.FormaPago_Tarjeta = FormaPago_Tarjeta != null && FormaPago_Tarjeta != "" ? FormaPago_Tarjeta.ToUpper() : ""; 
                        entity.FormaPago_BancoEmisor = FormaPago_BancoEmisor != null && FormaPago_BancoEmisor != "" ? FormaPago_BancoEmisor.ToUpper() : "";
                        entity.FormaPago_NroTarjeta = FormaPago_NroTarjeta;
                        entity.FormaPago_FechaVto = FormaPago_FechaVto;
                        entity.FormaPago_CodigoSeg = FormaPago_CodigoSeg;
                    }
                    entity.FormaPago = FormaPago;
                }

                //Contacto
                if (!entity.IDContacto.HasValue)
                {
                    entity.Contactos = new Contactos();
                    entity.Contactos.FechaAlta = DateTime.Now;
                    entity.Contactos.Estado = "A";
                    entity.Contactos.TipoContacto = "C";
                }
                entity.Contactos.Nombre = Contacto_Nombre;
                entity.Contactos.Apellido = Contacto_Apellido;
                entity.Contactos.Cargo = Contacto_Cargo;
                entity.Contactos.Telefono = Contacto_Telefono;
                entity.Contactos.Celular = Contacto_Celular;
                entity.Contactos.Email = Contacto_Email;
                entity.Contactos.Observaciones = Contacto_Observaciones;
                entity.Contactos.NroDocumento = Contacto_Documento;


                //Domicilio Fiscal
                if (entity.IDDomicilioFiscal == 0)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    entity.Domicilios.Estado = "A";
                }
                entity.Domicilios.TipoDomicilio = "F";
                entity.Domicilios.Entidad = "C";
                entity.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                entity.Domicilios.Provincia = int.Parse(Provincia);
                if (Ciudad != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : ""; 
                entity.Domicilios.CodigoPostal = CodigoPostal;
                entity.Domicilios.Telefono = TelefonoDom;
                entity.Domicilios.Fax = Fax;
                entity.Domicilios.PisoDepto = PisoDepto;

                if (entity.IDDomicilio == 0)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    entity.Domicilios.Estado = "A";
                }
                entity.Domicilios.TipoDomicilio = "C";
                entity.Domicilios.Entidad = "C";
                entity.Domicilios.Pais = Pais2 != null && Pais2 != "" ? Pais2.ToUpper() : "";
                entity.Domicilios.Provincia = int.Parse(Provincia2);
                if (Ciudad2 != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad2);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio2 != null && Domicilio2 != "" ? Domicilio2.ToUpper() : "";
                entity.Domicilios.CodigoPostal = CodigoPostal2;
                entity.Domicilios.Telefono = TelefonoDom2;
                entity.Domicilios.Fax = Fax2;
                entity.Domicilios.PisoDepto = PisoDepto2;
                entity.Domicilios.Latitud = Lat;
                entity.Domicilios.Longitud = Long;

                if (IDZona > 0)
                    entity.IDZona = IDZona;
                else
                    entity.IDZona = null;

                //Facturacion
                entity.CondicionIva = IVA;
                entity.EmailsEnvioFc = EnvioMailsFc;
                if (CostoFijo != "")
                    entity.CostoFijo = decimal.Parse(CostoFijo);
                else
                    entity.CostoFijo = null;

                if (TopeVenta != "")
                    entity.TopeVenta = decimal.Parse(TopeVenta);
                else
                    entity.TopeVenta = null;

                if (ConRetenciones)
                    entity.ConRetenciones = true;
                else 
                    entity.ConRetenciones = false;


                //Alertas
                entity.EmailAlertas = emailAlertas;
                entity.CelularAlertas = celularAlertas;
                entity.EmpresaCelularAlertas = celularEmpresaAlertas;

                //Imagenes Comercios

                if (IDComercio > 0)
                    dbContext.SaveChanges();
                else
                {
                    //creo una alerta 
                    Alertas alerta = new Alertas();
                    alerta.IDComercio = entity.IDComercio;
                    alerta.CantTR = 1;
                    alerta.Activa = true;
                    alerta.Prioridad = "A";
                    alerta.Tipo = "Por cantidad";
                    alerta.Restringido = false;
                    alerta.FechaDesde = 1;
                    alerta.Nombre = "default comercio";
                    dbContext.Alertas.Add(alerta);

                    dbContext.Comercios.Add(entity);

                    entity.UsuariosComercios = new System.Data.Entity.Core.Objects.DataClasses.EntityCollection<UsuariosComercios>();

                    dbContext.SaveChanges();

                    //Creo usuario de base
                    UsuariosComercios usu = new UsuariosComercios();
                    usu.IDComercio = entity.IDComercio;
                    usu.Tipo = "A";
                    usu.Activo = true;
                    usu.Email = entity.Email;
                    usu.Pwd = entity.Email;
                    usu.Usuario = entity.IDComercio.ToString();
                    entity.UsuariosComercios.Add(usu);

                    dbContext.SaveChanges();

                    //Envio mail de alta comercio
                    var marca = dbContext.Marcas.Where(x => x.IDMarca == IDMarca).FirstOrDefault();
                    if (marca != null && marca.EnvioEmailRegistroComercio && entity.Email != string.Empty && !string.IsNullOrEmpty(marca.EmailRegistroComercio))
                    {
                        ListDictionary mensaje = new ListDictionary();
                        string mailHtml = marca.EmailRegistroComercio.Replace("XNOMBREX", entity.NombreFantasia);
                        mensaje.Add("<MENSAJE>", mailHtml);
                        bool send = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, mensaje, entity.Email, "Bienvenido a RedIN");
                    }


                }
            }
            return entity.IDComercio.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void eliminarLogo(int id, string archivo)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            var file = "//files//logos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Logo = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadLogo(object sender, EventArgs e)
    {
        try
        {
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "comercio_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpLogo.SaveAs(path);

                Session["CurrentLogo"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentLogo"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    [WebMethod(true)]
    public static void eliminarFicha(int id, string archivo)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            var file = "//files//fichas//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.FichaAlta = null;
                            HttpContext.Current.Session["CurrentFicha"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadFicha(object sender, EventArgs e)
    {
        try
        {

            var extension = flpFichaAlta.FileName.Split('.')[1].ToLower();

            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpFichaAlta.FileName);
                string uniqueName = "comercio_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/fichas/"), uniqueName);

                flpFichaAlta.SaveAs(path);

                Session["CurrentFicha"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentFicha"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    #region Pruebas POS
    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPruebas(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio, string desde, string hasta)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                DateTime fDesde, fHasta;
                var result = dbContext.VerificacionesPOS
                    .Where(x => x.IDComercio == idComercio)
                    .OrderByDescending(x => x.FechaPrueba)
                    .Select(x => new VerificacionPOSViewModel()
                    {
                        FechaPrueba = x.FechaPrueba,
                        IDVerificacionPOS = x.IDVerificacionPOS,
                        EstadoCanjes = x.EstadoCanjes,
                        EstadoCompras = x.EstadoCompras,
                        EstadoGift = x.EstadoGift,
                        Calco = x.Calco == true ? "Si" : "No",
                        CalcoPuerta = x.CalcoPuerta == true ? "Si" : "No",
                        Display = x.Display == true ? "Si" : "No",
                        Folletos = x.Folletos == true ? "Si" : "No",
                        ObservacionesGenerales = x.ObservacionesGenerales,
                        //UsuarioPrueba = x.Usuarios.Usuario

                    }).AsQueryable();

                if (!string.IsNullOrEmpty(desde))
                {
                    fDesde = DateTime.Parse(desde).Date;
                    result = result.Where(x => x.FechaPrueba >= fDesde).AsQueryable();
                }

                if (!string.IsNullOrEmpty(hasta))
                {
                    fHasta = DateTime.Parse(hasta).Date.AddDays(1).AddTicks(-1);
                    result = result.Where(x => x.FechaPrueba <= fHasta).AsQueryable();
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePrueba(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.VerificacionesPOS.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void ProcesarPrueba(int idComercio, int IDVerificacionPOS, string estadoCanjes, string estadoGift, string estadoCompras, string observacionesCanjes, string observacionesGift, string observacionesCompras
        , string usuario, string obs, string puntosCanjes, string puntosGift, string puntosCompras, string fechaPrueba)
    {
        try
        {
            int idusu = 0;
            bool logueado = false;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                logueado = true;
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idusu = usu.IDUsuario;
            }
            else
            {
                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    logueado = true;
                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    idusu = usu.IDUsuario;
                }
            }
            if (logueado)
            {
                using (var dbContext = new ACHEEntities())
                {
                    VerificacionesPOS entity;
                    if (IDVerificacionPOS == 0)
                        entity = new VerificacionesPOS();
                    else
                        entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == IDVerificacionPOS).FirstOrDefault();


                    entity.IDUsuario = idusu;

                    entity.IDComercio = idComercio;
                    entity.EstadoCanjes = estadoCanjes;
                    entity.EstadoCompras = estadoCompras;
                    entity.EstadoGift = estadoGift;

                    entity.Puntos_POSCanjes = puntosCanjes;
                    entity.Puntos_POSCompras = puntosCompras;
                    entity.Puntos_POSGift = puntosGift;

                    entity.Observaciones_POSCanjes = observacionesCanjes;
                    entity.Observaciones_POSCompras = observacionesCompras;
                    entity.Observaciones_POSGift = observacionesGift;
                    entity.FechaPrueba = DateTime.Parse(fechaPrueba);

                    entity.UsuarioPrueba = usuario;
                    entity.ObservacionesGenerales = obs;

                    if (IDVerificacionPOS == 0)
                        dbContext.VerificacionesPOS.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Puntos de venta

    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPuntos(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.PuntosDeVenta
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Numero)
                    .Select(x => new PuntoDeVentaViewModel()
                    {
                        IDPuntoVenta = x.IDPuntoVenta,
                        IDComercio = x.IDComercio,
                        Punto = x.Numero,
                        FechaAlta = x.FechaAlta,
                        EsDefault = x.EsDefault ? "Si" : "No",
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePunto(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.PuntosDeVenta.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarPunto(int IDComercio, int IDPuntoVenta, int numero, DateTime fechaAlta, bool esDefault)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.PuntosDeVenta.Any(x => x.Numero == numero && x.IDComercio == IDComercio && x.IDPuntoVenta != IDPuntoVenta))
                        throw new Exception("Ya existe un punto de venta con el numero ingresado para este comercio");

                    PuntosDeVenta entity;
                    if (IDPuntoVenta == 0)
                        entity = new PuntosDeVenta();
                    else
                        entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == IDPuntoVenta).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Numero = numero;
                    entity.FechaAlta = DateTime.Now;

                    #region default
                    var algunoDefault = dbContext.PuntosDeVenta.Any(x => x.IDComercio == IDComercio && x.EsDefault);
                    if (algunoDefault && esDefault)
                        throw new Exception("Solo puede haber un punto de venta como default");
                    else
                        entity.EsDefault = esDefault;
                    #endregion

                    if (IDPuntoVenta == 0)
                        dbContext.PuntosDeVenta.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.UsuariosComercios
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.UsuariosComercios.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDComercio, int IDUsuario, string usuario, string email, string pwd, string tipo)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.UsuariosComercios.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosComercios.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosComercios entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosComercios();
                    else
                        entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosComercios.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    public static string corregirCelular(string Celular)
    {
        if (Celular == null) Celular = "";
        if (Celular.Length > 6)
        {
            Celular = Celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            Celular = Celular.Trim();
            if (Celular[0] == '0') Celular = Celular.Remove(0, 1);
            if (Celular[1] == '0') Celular = Celular.Remove(1, 1);
            if (Celular[2] == '0') Celular = Celular.Remove(2, 1);
            if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
            if (Celular[1] != '4') Celular = Celular.Insert(1, "4");
        }
        return Celular;
    }

    public static string corregirEmail(string Email)
    {
        if (Email == null) Email = "";
        if (Email != "")
        {
            Email = Email.Replace("(", "").Replace(")", "").Replace("emal", "email").Replace("email.com.ar", "email.com.ar").Replace("hotmai.com", "hotmail.com").Replace("hotmal", "hotmail").Replace("yaho.com", "yahoo.com").Replace(" ", "").Replace("?", "").Replace("¿", "").Replace("¡", "").Replace("!", "").Replace("&", "").Replace("}", "").Replace("{", "").Replace(",", "").Replace("#", "");
            Email = Email.Trim();
        }
        return Email;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubRubros(int IDRubroPadre)
    {

        using (var dbContext = new ACHEEntities())
        {
            List<ComboViewModel> result = new List<ComboViewModel>();

            var subRubros = dbContext.Rubros.Where(x => x.IDRubroPadre == IDRubroPadre).OrderBy(x => x.IDRubro).Select(x => new ComboViewModel()
            {
                ID = x.IDRubro.ToString(),
                Nombre = x.Nombre
            }).ToList();

            if (subRubros != null && subRubros.Count() > 0)
                result = subRubros;

            result.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });

            return result;
        }

    }

    private static void asociarComercioAEmpresa(int idComercio, int idEmpresa)
    {
        using (var dbContext = new ACHEEntities())
        {
            EmpresasComercios empresaComercioNueva = new EmpresasComercios();
            empresaComercioNueva.IDComercio = idComercio;
            empresaComercioNueva.IDEmpresa = idEmpresa;
            dbContext.EmpresasComercios.Add(empresaComercioNueva);
            dbContext.SaveChanges();
        }

    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais) {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }

}

