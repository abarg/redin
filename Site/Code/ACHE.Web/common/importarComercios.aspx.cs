﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using System.Globalization;
using System.Text;

public partial class common_importarComercios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                cargarCombos();
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    hdnIDFranquicias.Value = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault().IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos()
    {
        try
        {
            int idfranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idfranquicia = usu.IDFranquicia;
            }
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            if (idfranquicia > 0)
                listMarcas = bMarca.getMarcasByFranquicia(idfranquicia);
            else
                listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = bMarca.getMarcas();
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));



            if (idfranquicia == 0)
            {
                bFranquicia bFranquicia = new bFranquicia();
                this.ddlFranquicias.DataSource = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();
                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
            }
            else
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];

                this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



     


    private DataTable TComercios()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Franquicia");
        dt.Columns.Add("Marca");
        dt.Columns.Add("Dealer");
        dt.Columns.Add("FechaAltaDealer");
        dt.Columns.Add("RazonSocial");
        dt.Columns.Add("NombreFantasia");
        dt.Columns.Add("NombreEst");
        dt.Columns.Add("Telefono");
        dt.Columns.Add("Responsable");
        dt.Columns.Add("Cargo");
        dt.Columns.Add("Rubro");
        dt.Columns.Add("SubRubro");
        dt.Columns.Add("TipoDocumento");
        dt.Columns.Add("NroDocumento");
        dt.Columns.Add("CondicionIva");
        dt.Columns.Add("ConRetenciones");
        dt.Columns.Add("Web");
        dt.Columns.Add("Email");
        dt.Columns.Add("PaisComercial");
        dt.Columns.Add("PaisFiscal");
        dt.Columns.Add("ProvinciaComercial");
        dt.Columns.Add("ProvinciaFiscal");
        dt.Columns.Add("CiudadComercial");
        dt.Columns.Add("CiudadFiscal");
        dt.Columns.Add("DomicilioComercial");
        dt.Columns.Add("DomicilioFiscal");
        dt.Columns.Add("TelefonoComercial");
        dt.Columns.Add("TelefonoFiscal");
        dt.Columns.Add("FaxComercial");
        dt.Columns.Add("FaxFiscal");
        dt.Columns.Add("Piso_Depto_Comercial");
        dt.Columns.Add("Piso_Depto_Fiscal");
        dt.Columns.Add("CodigoPostalComercial");
        dt.Columns.Add("CodigoPostalFiscal");
        dt.Columns.Add("Celular");
        dt.Columns.Add("Observaciones");
        dt.Columns.Add("Actividad");
        dt.Columns.Add("EmailsEnvioFc");
        dt.Columns.Add("FichaAlta");
        dt.Columns.Add("Activo");
        dt.Columns.Add("NombreContacto");
        dt.Columns.Add("ApellidoContacto");
        dt.Columns.Add("DNIContacto");
        dt.Columns.Add("CargoContacto");
        dt.Columns.Add("TelefonoContacto");
        dt.Columns.Add("CelularContacto");
        dt.Columns.Add("EmailContacto");
        dt.Columns.Add("ObservacionContacto");
        dt.Columns.Add("CasaMatriz");
        dt.Columns.Add("EmailAlertas");
        dt.Columns.Add("CelularAlertas");
        dt.Columns.Add("EmpresaCelularAlertas");
        dt.Columns.Add("CostoFijo");
        dt.Columns.Add("TopeVenta");
        dt.Columns.Add("Tipo");

        return dt;
    }



    protected void Importar(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        

        if (flpArchivo.HasFile)
        {
            if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    DataTable dt = TComercios();

                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream for extended ascii characters (ñ, á, é, etc)
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        int errores = 0;
                        int total = 0;

                        #region Armo Datatable

                        for (int i = 0; i < rows.Length; i++)
                        {
                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length > 15)
                                {
                                    #region Armo DataRow
                                    if (dr[1].Trim() != "")
                                    {
                                        total++;
                                        try
                                        {

                                            DataRow drNew = dt.NewRow();
                                            drNew[0] = ddlFranquicias.SelectedValue;
                                            drNew[1] = ddlMarcas.SelectedValue;
                                            drNew[2] = dr[0].ToString();
                                            drNew[3] = dr[1].ToString();
                                            drNew[4] = dr[2].ToString();
                                            drNew[5] = dr[3].ToString();
                                            drNew[6] = dr[4].ToString();
                                            drNew[7] = dr[5].ToString();
                                            drNew[8] = dr[6].ToString();
                                            drNew[9] = dr[7].ToString();
                                            drNew[10] = dr[8].ToString();
                                            drNew[11] = dr[9].ToString();
                                            drNew[12] = dr[10].ToString();
                                            drNew[13] = dr[11].ToString();
                                            drNew[14] = dr[12].ToString();
                                            drNew[15] = dr[13].ToString();
                                            drNew[16] = dr[14].ToString();
                                            drNew[17] = dr[15].ToString();
                                            drNew[18] = dr[16].ToString();
                                            drNew[19] = dr[17].ToString();
                                            drNew[20] = dr[18].ToString();
                                            drNew[21] = dr[19].ToString();
                                            drNew[22] = dr[20].ToString();
                                            drNew[23] = dr[21].ToString();
                                            drNew[24] = dr[22].ToString();
                                            drNew[25] = dr[23].ToString();
                                            drNew[26] = dr[24].ToString();
                                            drNew[27] = dr[25].ToString();
                                            drNew[28] = dr[26].ToString();
                                            drNew[29] = dr[27].ToString();
                                            drNew[30] = dr[28].ToString();
                                            drNew[31] = dr[29].ToString();
                                            drNew[32] = dr[30].ToString();
                                            drNew[33] = dr[31].ToString();
                                            drNew[34] = dr[32].ToString();
                                            drNew[35] = dr[33].ToString();
                                            drNew[36] = dr[34].ToString();
                                            drNew[37] = dr[35].ToString();
                                            drNew[38] = dr[36].ToString();
                                            drNew[39] = dr[37].ToString();
                                            drNew[40] = dr[38].ToString();
                                            drNew[41] = dr[39].ToString();
                                            drNew[42] = dr[40].ToString();
                                            drNew[43] = dr[41].ToString();
                                            drNew[44] = dr[42].ToString();
                                            drNew[45] = dr[43].ToString();
                                            drNew[46] = dr[44].ToString();
                                            drNew[47] = dr[45].ToString();
                                            drNew[48] = dr[46].ToString();
                                            drNew[49] = dr[47].ToString();
                                            drNew[50] = dr[48].ToString();
                                            drNew[51] = dr[49].ToString();
                                            drNew[52] = dr[50].ToString();
                                            drNew[53] = dr[51].ToString();
                                            drNew[54] = dr[52].ToString(); 

                                            dt.Rows.Add(drNew);
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                            errores++;
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion

                        bool ok = true;

                        #region Importar datatable

                        if (dt.Rows.Count > 0)
                        {
                            EliminarTmp();
                            try
                            {
                                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                                {
                                    sqlConnection.Open();

                                    // Run the Bulk Insert
                                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                                    {
                                        //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                        bulkCopy.DestinationTableName = " [dbo].[ComerciosTmp]";

                                        foreach (DataColumn col in dt.Columns)
                                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                        bulkCopy.WriteToServer(dt);
                                    }
                                    sqlConnection.Close();
                                }
                                ProcesarTmp();
                            }
                            catch (Exception ex)
                            {
                                ok = false;
                                ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                            }
                        }


                        if (ok)
                        {

                            int totalOk = dt.Rows.Count - errores;

                            if (errores > 0)
                            {
                                ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }

                            int cantErrores = 0;

<<<<<<< HEAD
                            //using (var dbContext = new ACHEEntities())
                            //{
                            //    cantErrores = dbContext.SociosTmp.Where(x => x.Error.Length > 0).Count();
                            //}
=======
                            using (var dbContext = new ACHEEntities())
                            {
                                cantErrores = dbContext.ComerciosTmp.Where(x => x.Error.Length > 0).Count();
                            }
>>>>>>> bf796eb34b8061b3ce7c08713c783b4d2e4c4fcc

                            if (cantErrores > 0)
                            {
                                totalOk = totalOk - cantErrores;
                                //var msg = ". Se encontraron <a href='javascript:verErrores();'>" + cantErrores + " errores</a> en la importación.";

                                //ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);
                                ShowError("Registros importados: " + totalOk + " de " + (total).ToString());

                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }
                            else
                                ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());

                        }

                    }

                    File.Delete(path);

                    #endregion
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    //[WebMethod(true)]
    //public static string obtenerErrores()
    //{
    //    var html = string.Empty;
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        using (var dbContext = new ACHEEntities())
    //        {
    //            var list = dbContext.ComerciosTmp.Where(x => x..Error.Length > 0).ToList();
    //            if (list.Any())
    //            {
    //                foreach (var detalle in list)
    //                {
    //                    html += "<tr>";
    //                    html += "<td>" + detalle.Affinity + "</td>";
    //                    html += "<td>" + detalle.NroTarjeta + "</td>";
    //                    html += "<td>" + detalle.Apellido + " " + detalle.Nombre + "</td>";
    //                    html += "<td>" + detalle.Error + "</td>";
    //                    html += "</tr>";
    //                }
    //            }
    //            else
    //                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
    //        }
    //    }

    //    return html;
    //}

    private void EliminarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {
            dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE ComerciosTmp", new object[] { });
        }
    }

    private void ProcesarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                //var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                dbContext.Database.CommandTimeout = 1000;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarComerciosTmp ", new object[] { });
            }
            else
            {
                dbContext.Database.CommandTimeout = 1000;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarComerciosTmp ", new object[] { });
            }
        }
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}