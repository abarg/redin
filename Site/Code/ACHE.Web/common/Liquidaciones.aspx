﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Liquidaciones.aspx.cs" Inherits="common_Liquidaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
                 <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/liquidaciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Liquidaciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Liquidaciones</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="Form1" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" ClientIDMode="Static" Value="0" />
                <div class="formSep col-sm-12 col-md-12">

                    <div class="row">
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                       <asp:Panel runat="server" ID="pnlFranquicias" class="col-sm-3">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlFranquicias" />
                        </asp:Panel>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                                     <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Liquidaciones" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
    <!--
       <div class="modal fade" id="modalComprobantes">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titComprobantes"></h3>
				</div>
				<div class="modal-body">
				< class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.
					<table class="table table-condensed table-striped" id="tableComprobantes">
						<thead id="headDetalleTr">
							<tr>
                                <th>Factura</th> 
                                <th>Transferencia</th> 
                              
                            </tr>
						</thead>
						<tbody id="bodyComprobantes">
                           
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalComprobantes').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
    -->


</asp:Content>

