﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class common_motivose : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
           
                cargarMarcas();
            
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                ddlMarcas.Enabled = false;

            }
            if (!String.IsNullOrEmpty(Request.QueryString["IDMotivo"])) {
                try {
                    int idMotivo = int.Parse(Request.QueryString["IDMotivo"]);
                    if (idMotivo > 0) {
                        this.hdnIDMotivo.Value = idMotivo.ToString();
                        cargarDatos(idMotivo);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("Motivos.aspx");
                }
            }
        }

    }
    private void cargarMarcas()
    {
        try
        {

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;

            listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void cargarDatos(int idMotivo) {
        if (idMotivo > 0) {
            using (var dbContext = new ACHEEntities()) {

                var entity = dbContext.Motivos.Where(x => x.IDMotivo == idMotivo).FirstOrDefault();
                ddlMarcas.SelectedValue = entity.IDMarca.HasValue?entity.IDMarca.ToString():"0";
                txtNombre.Text = entity.Nombre != null && entity.Nombre != "" ? entity.Nombre.Trim().ToUpper() : "";
                txtDesc.Text = entity.Descripcion != null && entity.Descripcion != "" ? entity.Descripcion : "";
                chkSumaPuntos.Checked = entity.SumaPuntos;
            }
        }
    }



    [WebMethod(true)]
    public static void grabar(int idMotivo,int idMarca, string nombre, string desc, bool sumaPuntos)
    {
          if (HttpContext.Current.Session["CurrentMarcasUser"] != null||HttpContext.Current.Session["CurrentUser"] != null)
        {
         

        try {

            Motivos entity;
            using (var dbContext = new ACHEEntities()) {
                if (idMotivo > 0)
                    entity = dbContext.Motivos.Where(x => x.IDMotivo == idMotivo).FirstOrDefault();
                else
                    entity = new Motivos();
               
                if (dbContext.Motivos.Any(x => x.IDMotivo != entity.IDMotivo &&((x.IDMarca==idMarca && idMarca>0) || (idMarca==0 && !x.IDMarca.HasValue))&& x.Nombre.ToUpper() == nombre.ToUpper()))
                    throw new Exception("Ya existe un motivo con el nombre " + nombre);
                   
                       

                       entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper() : "";
                       entity.Descripcion = desc;
                        entity.SumaPuntos = sumaPuntos;
                        if(idMarca>0)
                            entity.IDMarca = idMarca;
                        if (idMotivo > 0)
                            dbContext.SaveChanges();
                        else {
                            dbContext.Motivos.Add(entity);
                            dbContext.SaveChanges();
                        }
                    }
                
                
            
            //return entity.IDMotivo.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
          }
    }

    void Page_PreInit(object sender, EventArgs e)
    {


        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

}