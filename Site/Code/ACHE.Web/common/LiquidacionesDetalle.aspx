﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LiquidacionesDetalle.aspx.cs" Inherits="common_LiquidacionesDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/common/Liquidaciones.aspx") %>">Liquidaciones</a></li>
            <li class="last">Detalle</li>
        </ul>
    </div> 

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading"><asp:Literal runat="server" ID="litTitulo"></asp:Literal></h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h4><asp:Literal runat="server" ID="litEstado"></asp:Literal></h4>
            <h4><asp:Literal runat="server" ID="litPeriodo"></asp:Literal></h4>
            <h4><asp:Literal runat="server" ID="litFranquicia"></asp:Literal></h4>
       </div>
    </div>
    <br />
    <asp:Panel ID="pnlFormLiqDetalle" runat="server" Visible="false">
        <form id="formEdicion" > 
            <h3 class="heading">Agregar item</h3>
            <div class="alert alert-danger alert-dismissable" id="divError2" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Se ha agregado correctamente.</div>
         	<div class="formSep col-sm-12 col-md-12">
				<div class="row">
                    <div class="col-sm-2">
                        <label>Concepto</label>
                        <select runat="server" value="" class="form-control required" ID="cmbConcepto" />
                    </div>
                    <div class="col-sm-2">   <br />  
                        <button class="btn btn-success" type="button" id="btnAgregarConcepto" onclick="agregarConcepto();">Agregar</button>
                    </div>
				</div>
            </div>
        </form>
    </asp:Panel>   
    <div class="row">
        <div class="col-sm-12 col-md-12" id="tblLiquidacionDetalle">
            <table class="table table-striped table-bordered mediaTable" >
				<thead>
					<tr>
                        <th class="essential" style="width:100px">Observaciones</th>
                        <th class="essential" style="width:100px">Detalle</th>
                                                <th class="essential" style="width:100px">Eliminar</th>

                        <th class="essential">Concepto</th>
                        <th class="optional">Sub total</th>
					</tr>
				</thead>
				<tbody id="bodyLiquidacionDetalle">
                    <tr><td colspan="5">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
        <asp:Panel runat="server" ID="pnlPaso2" Visible="false"  class="row">
            <div class="col-sm-12 col-md-12" style="margin-left:15px">
                <button class="btn btn-success" type="button" id="btnEnviarObs" onclick="enviarACasaMatriz();">Enviar a casa matriz </button>
                    <button class="btn btn-success" type="button"  onclick="mostrarPaso4();">Confirmar</button>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlPaso3" Visible="false"  class="row">
            <div class="col-sm-12 col-md-12" style="margin-left:15px">
                <button class="btn btn-success" type="button" id="btnEnviarFranq" onclick="enviarAlFranquiciado();">Enviar al franquiciado</button>
            </div>
        </asp:Panel>
    </div>
    <div class="row">
        <form  runat="server" id="form1" >
             <asp:HiddenField runat="server" value="0" ID="hfIDLiquidacion" /> 

            <div class="alert alert-danger alert-dismissable" id="pnlError" runat="server" visible="false">
                <asp:Literal runat="server" ID="litError"></asp:Literal>
            </div>
            <div class="alert alert-success alert-dismissable" id="pnlOK" runat="server" visible="false">
                <asp:Literal runat="server" ID="litOk"></asp:Literal>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
            <asp:HiddenField runat="server" value="0" ID="HiddenField1" />
            <div runat="server" id="divPaso4" style="display:none"  >
                <div  class="row">
                    <div class="col-sm-8 col-sm-md-8">
                            <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Factura</label>
                            <div class="col-lg-3">
                                <asp:FileUpload runat="server" ID="flpFactura" />
                            </div>
                        </div>
                        <br/> <br/>
                        <asp:Button runat="server" ID="btnConfirmar" CssClass="btn btn-success" Text="Aceptar" OnClick="ConfirmarLiquidacion" />
                    </div>
                </div> 
            </div>
           <asp:Panel runat="server" ID="pnlPaso5" Visible="false" >
                <div  class="row">
                    <div class="col-sm-8 col-sm-md-8">
                            <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Comprobante de transferencia</label>
                            <div class="col-lg-3">
                                <asp:FileUpload runat="server" ID="flpComprobante" />
                            </div>
                        </div>
                        <br/> <br/>
                        <asp:Button runat="server" ID="Button1" CssClass="btn btn-success" Text="Adjuntar Comprobante" OnClick="AdjuntarComprobante" />
                    </div>
                </div> 
            </asp:Panel>
        </form>
    </div>

    <!--
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
    -->
       
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/liquidaciones-detalle.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

