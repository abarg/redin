﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_Socios_Unificar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string NroDocumento) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            if (NroDocumento=="")
                NroDocumento = "-1";
            using (var dbContext = new ACHEEntities()) {
                    var sociosRepetidos= dbContext.Socios
                        .Where(x=> x.NroDocumento == NroDocumento)
                            .OrderBy(x => x.Apellido)
                            .Select(x => new SociosViewModel() {
                                IDSocio = x.IDSocio,
                                NroDocumento = x.NroDocumento,
                                //Tarjeta = x.NumeroTarjeta != "" ? (x.NumeroTarjeta.Substring(0, x.NumeroTarjeta.Length - 4) + "XXXX") : x.NumeroTarjeta,
                                Apellido = x.Apellido + ", " + x.Nombre
                                //Email = x.Email,
                            });
                   
                    return sociosRepetidos.ToDataSourceResult(take, skip, sort, filter); 
                
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void UnificarTarjetas(string IDSocio,string NroDocumento) {
        using (var dbContext = new ACHEEntities()) {
           dbContext.Database.SqlQuery<ChartDecimal>("exec unificarTarjetas " + int.Parse(IDSocio) + ",'" + NroDocumento+"'", new object[] { }).ToList();
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTabla(string DNI) {
        var html = string.Empty;
        using (var dbContext = new ACHEEntities()) {
            var list = dbContext.Socios.Where(x => x.NroDocumento == DNI).ToList();
            if (list.Count() > 1) {
                int n = 1;
                foreach (var item in list) {

                    html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                    //html += "<td style='max-width:100px'><div align='center'><a onclick='irHistorialObs(" + item.IDSocio + ");'><img src='../../img/grid/gridView.gif' style='cursor:pointer' title='Ver' class='viewColumn'/><a/></div></td> ";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='unificarTarjetas(" + item.IDSocio + "," + item.NroDocumento + ");'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Unificar' class='editColumn'/><a/></div></td> ";
                    html += "<td>" + item.IDSocio + "</td> ";
                    html += "<td>" + item.NroDocumento + "</td> ";
                    html += "<td>" + item.Apellido + "</td> ";
                    //html += "<td>" + (item.SubTotal.HasValue ? item.SubTotal.Value.ToString("N2") : "0") + "</td> ";
                    html += "</tr>";
                    n++;
                }
                //html += " <tr><td colspan='3'>&nbsp;</td><td><b>Total: $" + list.Sum(x => x.SubTotal).Value.ToString("N2") + "</b></td>";
            }
            else {
                throw new Exception("No existen más de 1 socios con el Nro de Documento ingresado");
            }

        }

        return html;
    }

}
