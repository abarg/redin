﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FormularioAprobacionTarjetase.aspx.cs" Inherits="common_FormularioAprobacionTarjetase" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/common/FormularioAprobacionTarjetas.aspx") %>">Formulario aprobación tarjetas</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Formulario aprobación tarjetas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabEspecificaciones"  data-toggle="tab">Especificaciones</a></li>
                    <li><a href="#tabResultadoRevision" data-toggle="tab">Resultado de revisión</a></li>
                    <li><a href="#tabDatosFacturacion" data-toggle="tab">Datos Facturación</a></li>
                    <li><a href="#tabEnvioProv"  data-toggle="tab">Envio a provedor</a></li>
                    <li><a href="#tabDatosReverso"  data-toggle="tab">Datos Reverso</a></li>
                
                </ul>
                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                      <div class="tab-content">
                        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                        <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                             <div class="form-group">
                                <label class="col-lg-2 control-label"> Fecha </label>
                                <div class="col-lg-4" >
                                    <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control validDate" MaxLength="10" />
                                </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Estado</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                      <asp:ListItem Value="PROCESO" Text="Proceso" />
                                      <asp:ListItem Value="PEDIDO" Text="Pedido" />
                                      <asp:ListItem Value="PRODUCCION" Text="Produccion" />
                                      <asp:ListItem Value="ENVIADO" Text="Enviado" />
                                      <asp:ListItem Value="ENTREGADO" Text="Entregado" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Franquicia</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control required" ID="ddlFranquicias">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">Unidad de negocio</label>
                                    <div class="col-lg-3">
                                        <asp:DropDownList runat="server" ID="ddlUnidadNegocio" CssClass="form-control">
                                            <asp:ListItem Text="RED IN" Value="REDIN" />
                                            <asp:ListItem Text="FIDELIZACION" Value="FIDELIZACION" />
                                           <asp:ListItem Text="GIFTCARD" Value="GIFTCARD" />
                                        </asp:DropDownList>
                                    </div>
                              </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-3" id="tblCostos">
                                    <table class="table table-striped table-bordered mediaTable" >
				                        <thead>
					                        <tr>
                                                <th style="width:100px">Costos</th>
                                                <th style="width:100px">Unitario</th>
                                                <th style="width:100px">Cantidad</th>
                                                <th style="width:100px">Total</th>
                                                <th style="width:100px">Observaciones</th>
					                        </tr>
				                        </thead>
				                        <tbody id="bodyCostos">
                                            <tr><td colspan="4">Calculando...</td></tr>
				                        </tbody>
			                        </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Total</label>
                                <div class="col-lg-4">
                                    <asp:label runat="server" ID="lblTotal" ></asp:label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Cliente</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCliente" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                               <label class="col-lg-2 control-label"> Observaciones</label>
                               <div class="col-lg-4">
                                   <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control" MaxLength="50"></asp:TextBox>
                               </div>
                            </div>

                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Logo</label>
                                 <div class="col-lg-6">
                                     <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"  OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadLogo" />

                                     <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label> 
                                 </div>

                                 <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Otras imagenes</label>
                                 <div class="col-lg-6">
                                      <ajaxToolkit:AsyncFileUpload runat="server" ID="flpImagenes" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadImagenes" />     
                                             
                                    <asp:Label runat="server" ID="Label1" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divImagenes" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkImagenes" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkImagenesDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                   <label class="col-lg-1 control-label"></label>
                                    <div class="col-lg-6">
                                      <p>IMPORTANTE: Esta es solo una simulación, por lo que los colores mostrados son solamente referenciales y no representan el 
                                          color final de las tintas que se detallan junto a cada imagen.
                                          Por favor chequee detenidamente esta prueba. En caso de existir alguna observación deberá dejarla indicada. Se recomienda 
                                          enviar logos e imágenes en buena calidad.
                                          Una vez aprobada esta simulación por parte del cliente, Red In asumirá que esta conforme con la misma, por lo que no se 
                                          responsabilizara por errores encontrados una vez impresa la orden de trabajo.
                                      </p>
                                      </div>
                             </div>

                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Anverso</label>
                                 <div class="col-lg-6">
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpAnverso" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadAnverso" />             
                                     <asp:Label runat="server" ID="Label2" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divAnverso" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkAnverso" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkAnversoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Observaciones Anverso</label>
                                 <div class="col-lg-4">
                                     <asp:TextBox runat="server" ID="txtObservacionesAnverso" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Reverso</label>
                                 <div class="col-lg-6">
                                      <ajaxToolkit:AsyncFileUpload runat="server" ID="flpReverso" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadReverso" />             
                                     <asp:Label runat="server" ID="Label3" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divReverso" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkReverso" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkReversoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Observaciones Reverso</label>
                                 <div class="col-lg-4">
                                     <asp:TextBox runat="server" ID="txtObservacionesReverso" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                 </div>
                             </div>
                        </div>
                        <div class="tab-pane" id="tabEspecificaciones">
                        <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"></label>
                                <div class="col-lg-3" id="tblEspecificaciones">
                                    <table style="width: 502px;" class="table table-striped table-bordered mediaTable" >
				                        <thead>
					                        <tr>
                                                <th style="width:100px">ESPECIFICACIONES</th>
                                                <th style="width:100px">TIENE</th>
                                                <th style="width:100px">OBS.</th>
					                        </tr>
				                        </thead>
				                        <tbody id="bodyEspecificaciones">
                                            <tr><td colspan="4">Calculando...</td></tr>
				                        </tbody>
			                        </table>
                                </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tabResultadoRevision">
                        <br/>
                             <div class="form-group">
                                <label class="col-lg-2 control-label">Imprimir segun simulacion</label>
                                <div class="col-lg-4 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkImprimirSegunSimulacion" ClientIDMode="Static"  />
                                </div>
 
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Corregir según observaciones y enviar simulación</label>
                                <div class="col-lg-4 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkCorregirConSimulacion" ClientIDMode="Static"  />
                                </div>
 
                            </div> 
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Corregir e imprimir sin enviar nueva simulación</label>
                                <div class="col-lg-4 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkCorregirSinSimulacion" ClientIDMode="Static"  />
                                </div>
 
                            </div>
                           <div class="form-group">
                                 <label class="col-lg-2 control-label">Observaciones </label>
                                 <div class="col-lg-4">
                                     <asp:TextBox runat="server" ID="txtObservaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                 </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Adjuntar Factura anticipo</label>
                                 <div class="col-lg-6">
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpFacturaAnticipo" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadFacturaAnticipo" />             
                                      <asp:Label runat="server" ID="Label10" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divFacturaAnticipo" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkFacturaAnticipo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkFacturaAnticipoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                              <div class="form-group">
                                 <label class="col-lg-2 control-label">Adjuntar aprobacion firmada por el cliente</label>
                                 <div class="col-lg-6">
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpAprobacion" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadAprobacion" />             
                                      <asp:Label runat="server" ID="Label5" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divAprobacionCliente" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkAprobacionCliente" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkAprobacionClienteDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                        </div>
                        <div class="tab-pane" id="tabDatosFacturacion">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Razon social</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> País</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlPaisFact" onchange="LoadProvincias(this.value,'ddlProvinciaFact');">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlProvinciaFact" onchange="LoadCiudades(this.value,'ddlCiudadFact');">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlCiudadFact">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomFiscal" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> CUIT</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCuit" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Cond. IVA</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlIVA">
                                      <asp:ListItem Value="RI" Text="Resp. Inscripto" />
                                      <asp:ListItem Value="MONOTRIBUTISTA" Text="Monotributista" />
                                      <asp:ListItem Value="EX" Text="Exento/No Resp." />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Contacto</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Tel Contacto</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelContacto" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Email Contacto</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmailContacto" CssClass="form-control email" MaxLength="50"></asp:TextBox>
                                    <div id="divErrorFormatoMail1" style="display: none; color:#a94442"> Debe ingresar un Email válido</div>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Observaciones </label>
                                 <div class="col-lg-4">
                                     <asp:TextBox runat="server" ID="txtObservacionesDatosFacturacion" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                 </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabEnvioProv">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Forma de pago</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlFormaDePago">
                                      <asp:ListItem Value="EFECTIVO" Text="Efectivo" />
                                      <asp:ListItem Value="CHEQUE" Text="Cheque" />
                                      <asp:ListItem Value="DEPOSITO" Text="Deposito" />
                                      <asp:ListItem Value="TRANSFERENCIA" Text="Transferencia" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Comprobante</label>
                                 <div class="col-lg-6">
                                     <ajaxToolkit:AsyncFileUpload runat="server" ID="flpComprobante" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadComprobante" />             
                                     <asp:Label runat="server" ID="Label4" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divComprobante" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkComprobante" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkComprobanteDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Fecha envio proveedor </label>
                                <div class="col-lg-4" >
                                    <asp:TextBox runat="server" ID="txtFechaEnvioProv" CssClass="form-control validDate" MaxLength="10" />
                                </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> COUNTER DIAS	 </label>
                                <div class="col-lg-2" >
                                    <span runat="server" id="spnCounterDias"></span>
                                    
                                    <%--<asp:Literal runat="server" ID="litCounterDias" />--%>
                                </div>
                             </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Formulario Aprobacion</label>
                                 <div class="col-lg-6">
                                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpFormulario" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadFormulario" />             
                                     <asp:Label runat="server" ID="Label6" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divFormularioAprobacion" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkFormularioAprobacion" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkFormularioAprobacionDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-lg-2 control-label">Base tracks</label>
                                 <div class="col-lg-6">
                                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpBaseTracks" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadBaseTracks" />             
                                     <asp:Label runat="server" ID="Label7" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divBaseTracks" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkBaseTracks" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkBaseTracksDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Nombre y apellido</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> DNI</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDNI" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Localidad</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtLocalidad" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> CP.</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCP" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlProvincia">
                                    </asp:DropDownList>
                                </div>
                            </div>
                              <div class="form-group">
                                 <label class="col-lg-2 control-label">Importe factura</label>
                                 <div class="col-lg-6">
                                       <ajaxToolkit:AsyncFileUpload runat="server" ID="flpImporte" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                                OnUploadedComplete="uploadImporte" />     
                                     <asp:Label runat="server" ID="Label8" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divImporteFactura" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkImporteFactura" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkImporteFacturaDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label"> Fecha de envio </label>
                                <div class="col-lg-4" >
                                    <asp:TextBox runat="server" ID="txtFechaEnvio" CssClass="form-control validDate" MaxLength="10" />
                                </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Empresa</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmpresa" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Nro Guia</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNroGuia" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Adjuntar guia</label>
                                 <div class="col-lg-6">
                                       <ajaxToolkit:AsyncFileUpload runat="server" ID="flpGuia" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber"   OnClientUploadError="UploadError"
                                            OnUploadedComplete="uploadGuia" />             
                                     <asp:Label runat="server" ID="Label9" Style="display: none;">
                                         <img alt="" src="../../img/ajax_loader.gif" />
                                     </asp:Label>
 
                                 </div>
                                 <div class="col-sm-4" runat="server" id="divGuia" visible="false">
                                     Actual:
                                     <asp:HyperLink runat="server" ID="lnkGuia" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkGuiaDelete" Target="_blank">Eliminar</asp:HyperLink>
                                 </div>
                             </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Forma de envio</label>
                                <div class="col-lg-6">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlFormaEnvio">
                                      <asp:ListItem Value="DOMICILIO" Text="Envio a domicilio" />
                                      <asp:ListItem Value="TERMINAL" Text="Retiro en terminal" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                          <div class="tab-pane" id="tabDatosReverso">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Página web</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtPaginaWeb" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <div id="divErrorFormatoMail2" style="display: none; color:#a94442"> Debe ingresar un Email válido</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Facebook</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFacebook" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Bases y Condiciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox id="txtBasesYCondiciones" TextMode="multiline" Columns="50" Rows="5" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Logo</label>
                                <div class="col-lg-4">
                                    <asp:CheckBox runat="server" ID="chkLogo" ClientIDMode="Static"  />
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                                <button runat="server" id="Button4" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="FormularioAprobacionTarjetas.aspx" class="btn btn-link">Cancelar</a>
                            </div>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/formularioAprobacionTarjetase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

