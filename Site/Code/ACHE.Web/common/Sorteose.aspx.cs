﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;


public partial class common_Sorteose : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        int idSorteo = 0;
        if (!IsPostBack) {
            try {
                if (!String.IsNullOrEmpty(Request.QueryString["IDSorteo"])) {                
                    idSorteo = int.Parse(Request.QueryString["IDSorteo"]);
                        if (idSorteo > 0) {                      
                            this.hdnIDSorteo.Value = idSorteo.ToString();
                            cargarDatosSorteos(idSorteo);
                        }
                    }
                     cargarPaises();
                    cargarMarcas(idSorteo);
            }
            catch (Exception ex) {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "el error en marcas es: ", ex.ToString());
                Response.Redirect("Sorteos.aspx");
            }            
        }    
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarDatosSorteos(int idSorteo) {

        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                var sorteo = dbContext.Sorteos.Where(x => x.IDSorteo == idSorteo).FirstOrDefault();
                if (sorteo != null) {
                    this.txtTitulo.Text = sorteo.Titulo;
                    this.txtMsj1.Text = sorteo.Mensaje1;
                    this.txtMsj2.Text = sorteo.Mensaje2;
                    this.txtMsj3.Text = sorteo.Mensaje3;
                    this.txtMsj4.Text = sorteo.Mensaje4;
                    DateTime dtDsd = Convert.ToDateTime(sorteo.FechaDesde.ToString());
                    DateTime dtHst = Convert.ToDateTime(sorteo.FechaHasta.ToString());
                    this.txtFechaDesde.Text = dtDsd.ToString("dd/M/yyyy");
                    this.txtFechaHasta.Text = dtHst.ToString("dd/M/yyyy");
                    this.chkActivo.Checked = sorteo.Activo;
                    this.ddlSexo.SelectedValue = sorteo.Sexo;
                    this.txtImporteDesde.Text = sorteo.ImporteDesde.HasValue ? sorteo.ImporteDesde.ToString() : "0";
                    this.txtImporteHasta.Text = sorteo.ImporteHasta.HasValue ? sorteo.ImporteHasta.ToString() : "0";

                    //DOMICILIO
                    if (sorteo.Domicilios != null && sorteo.Domicilios.IDDomicilio != 0) {
                        var idPais = dbContext.Paises.Where(x => x.Nombre.ToLower() == sorteo.Domicilios.Pais.ToString().ToLower()).FirstOrDefault().IDPais;
                        this.ddlPais.SelectedValue = idPais.ToString();

                        ddlProvincia.DataSource = Common.LoadProvinciasByPais(idPais);
                        ddlProvincia.DataTextField = "Nombre";
                        ddlProvincia.DataValueField = "ID";
                        ddlProvincia.DataBind();
                        if (sorteo.Domicilios.Provincia != null) {
                            this.ddlProvincia.SelectedValue = sorteo.Domicilios.Provincia.ToString();

                            ddlCiudad.DataSource = Common.LoadCiudades(sorteo.Domicilios.Provincia);
                            ddlCiudad.DataTextField = "Nombre";
                            ddlCiudad.DataValueField = "ID";
                            ddlCiudad.DataBind();
                            ddlCiudad.Items.Insert(0, new ListItem("", ""));

                            if (sorteo.Domicilios.Ciudad != null)
                                this.ddlCiudad.SelectedValue = sorteo.Domicilios.Ciudad.ToString();
                            else
                                this.ddlCiudad.SelectedValue = "";
                        }

                        this.txtDomicilio.Text = sorteo.Domicilios.Domicilio;
                        this.txtPisoDepto.Text = sorteo.Domicilios.PisoDepto;

                    }
                   

                }
            }
        }
    }

    private void cargarMarcas(int idSorteo) {
        int idMarca = 0;
        int idFran = 0;
        int idAdmin = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
        }
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usuFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFran = usuFran.IDFranquicia;
        }
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usuAdmin = (Usuarios)HttpContext.Current.Session["CurrentUser"];
            idAdmin = usuAdmin.IDUsuario;
        }

        using (var dbContext = new ACHEEntities()) {
            var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
            if (idMarca > 0) {
                marcas = marcas.Where(x => x.IDMarca == idMarca).OrderBy(x => x.Nombre).ToList();
                this.cmbMarcas.Enabled = false;
            }
            if (idFran > 0 || idAdmin > 0 || idMarca > 0) {
                
                if (idFran > 0) {
                    marcas = marcas.Where(x => x.IDFranquicia == idFran).OrderBy(x => x.Nombre).ToList();
                }
                if (marcas != null) {
                    cmbMarcas.DataSource = marcas;
                    cmbMarcas.DataTextField = "Nombre";
                    cmbMarcas.DataValueField = "IDMarca";
                    cmbMarcas.DataBind();
                }
                if (idSorteo > 0) {
                    int idMarcaSorteo = dbContext.Sorteos.Where(x => x.IDSorteo == idSorteo).FirstOrDefault().IDMarca;
                    cmbMarcas.SelectedValue = idMarcaSorteo.ToString();
                    //cmbMarcas.Items.Insert(0, new ListItem("", ""));
                }
                else if(idMarca == 0)
                    cmbMarcas.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarPaises() {
        ddlPais.DataSource = Common.LoadPaises();
        ddlPais.DataTextField = "Nombre";
        ddlPais.DataValueField = "ID";
        ddlPais.DataBind();
        this.ddlPais.Items.Insert(0, new ListItem("", ""));

    }

    [WebMethod(true)]
    public static void grabar(int idSorteo, int idMarca, string Titulo, string Msj1, string Msj2, string Msj3, string Msj4, string fechaDesde, string fechaHasta, bool activo, string Sexo, string ImporteDesde, string ImporteHasta, string Pais, string Provincia, string Ciudad, string Domicilio, string PisoDepto) {

        try {
            Sorteos sorteo = new Sorteos();
            using (var dbContext = new ACHEEntities()) {

                if (idSorteo > 0) {
                    sorteo =dbContext.Sorteos.Where(x => x.IDSorteo == idSorteo).FirstOrDefault();
                    if (sorteo != null) {
                        sorteo.IDMarca = idMarca;
                        sorteo.Titulo = Titulo;
                        sorteo.Mensaje1 = Msj1;
                        sorteo.Mensaje2 = Msj2;
                        sorteo.Mensaje3 = Msj3;
                        sorteo.Mensaje4 = Msj4;
                        sorteo.FechaDesde = DateTime.Parse(fechaDesde);
                        sorteo.FechaHasta = DateTime.Parse(fechaHasta);
                        sorteo.Sexo = Sexo;
                        sorteo.ImporteDesde = int.Parse(ImporteDesde);
                        sorteo.ImporteHasta = int.Parse(ImporteHasta);   
                        sorteo.Activo = activo;

                        //Domicilio
                        if (sorteo.IDDomicilio == null || sorteo.IDDomicilio == 0) {
                            sorteo.Domicilios = new Domicilios();
                            sorteo.Domicilios.FechaAlta = DateTime.Now;
                            sorteo.Domicilios.Estado = "A";
                            sorteo.Domicilios.TipoDomicilio = "P"; //Promocion
                                    
                        }
                        sorteo.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                        sorteo.Domicilios.Provincia = int.Parse(Provincia);
                        if (!string.IsNullOrEmpty(Ciudad))
                            sorteo.Domicilios.Ciudad = int.Parse(Ciudad);
                        else
                            sorteo.Domicilios.Ciudad = null;
                        sorteo.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                        sorteo.Domicilios.PisoDepto = PisoDepto;

                    }
                }
                else
                {
                    sorteo.IDMarca = idMarca;
                    sorteo.Titulo = Titulo;
                    sorteo.Mensaje1 = Msj1;
                    sorteo.Mensaje2 = Msj2;
                    sorteo.Mensaje3 = Msj3;
                    sorteo.Mensaje4 = Msj4;
                    sorteo.FechaDesde = DateTime.Parse(fechaDesde);
                    sorteo.FechaHasta = DateTime.Parse(fechaHasta);
                    sorteo.Sexo = Sexo;
                    sorteo.ImporteDesde = int.Parse(ImporteDesde);
                    sorteo.ImporteHasta = int.Parse(ImporteHasta);   
                    sorteo.Activo = activo;                
                    dbContext.Sorteos.Add(sorteo);


                    if (sorteo.IDDomicilio == null || sorteo.IDDomicilio == 0) {
                        sorteo.Domicilios = new Domicilios();
                        sorteo.Domicilios.FechaAlta = DateTime.Now;
                        sorteo.Domicilios.Estado = "A";
                        sorteo.Domicilios.TipoDomicilio = "P"; //Promocion

                    }

                    sorteo.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                    sorteo.Domicilios.Provincia = int.Parse(Provincia);
                    if (Ciudad != string.Empty)
                        sorteo.Domicilios.Ciudad = int.Parse(Ciudad);
                    else
                        sorteo.Domicilios.Ciudad = null;
                    sorteo.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                    sorteo.Domicilios.PisoDepto = PisoDepto;


                    dbContext.Sorteos.Add(sorteo);
                }
                dbContext.SaveChanges();
            }
        }

        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }

}