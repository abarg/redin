﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dashboard-PlusIN.aspx.cs" Inherits="common_dashboard_PlusIN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
      <div class="row" > 
          <form id="formFranquicia" style="display:none" runat="server">
            <div class="col-sm-3" >
                <label>Franquicia</label>
                <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"  onchange="mostrarReporte(this.value);" />
            </div>
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="-1" />
            <asp:HiddenField runat="server" ID="hdnIDAdmin" Value="-1" />

          </form>
      </div>
      <div class="dashboard" >
        <div class="row admin" style="display:none">
            <div class="col-sm-12 tac">
                <h3 class="heading">Plus IN
                    <asp:Literal runat="server" ></asp:Literal></h3>
                <ul class="ov_boxes">
                      <li>
					    <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/1396123817_banker.png" style="width:48px" />
					    </div>
					    <div class="ov_text">
						    <strong id="resultado_cantSocios_plusIN">Calculando...</strong>
						    Cantidad socios 
					    </div>
				    </li>
                    <li>
					    <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/1396123817_banker.png" style="width:48px" />
					    </div>
					    <div class="ov_text">
						    <strong id="resultado_facturacion_plusIN">Calculando...</strong>
						    Facturacion <br />mes actual
					    </div>
				    </li>
                </ul>
            </div>
        </div>
        <div class="row franquicia" style="display:none">
            <div class="col-sm-12 tac">
                  <h3 class="heading">Plus IN Franquicia
                    <asp:Literal ID="Literal1" runat="server" ></asp:Literal></h3>
                <ul class="ov_boxes">
                        <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/invoice.png" style="width: 48px" />
                        </div>
                        <div class="ov_text" style="height: 70px;">
                            <strong id="resultado_cantSocios_plusIN_Franq">Calculando...</strong>
                            Cantidad socios 
                        </div>
                    </li>
                        <li>
                    <div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/invoice.png" style="width: 48px" />
                    </div>
                    <div class="ov_text" style="height: 70px;">
                        <strong id="resultado_facturacion_plusIN_Franq">Calculando...</strong>
                        Facturacion <br />mes actual
                    </div>
                </li>
                </ul>
            </div>
        </div>
 
</div>
    <!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/dashboard-PlusIN.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <!-- charts functions -->

    <!-- tablas -->
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

