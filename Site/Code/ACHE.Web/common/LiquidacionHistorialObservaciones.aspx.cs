﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using ACHE.Model.EntityData;
using ACHE.Model.ViewModels;

public partial class common_LiquidacionHistorialObservaciones : System.Web.UI.Page
{
    static int idLiquidacionDetalle;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["IDLiquidacionDetalle"]))
        {

            idLiquidacionDetalle = int.Parse(Request.QueryString["IDLiquidacionDetalle"]);
            using (var dbContext = new ACHEEntities())
            {
                var liq = dbContext.LiquidacionDetalle.Where(x => x.IDLiquidacionDetalle == idLiquidacionDetalle).FirstOrDefault();
                this.hdfIDLiquidacion.Value = liq.IDLiquidacion.ToString();
                //aca veo si mostrar el boton o no

                litTitulo.Text = "Observaciones - Liquidación #" + liq.IDLiquidacion.ToString("#000000") + " - Concepto: " + liq.Tipo;

                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    if (liq.Liquidaciones.Estado == "EnFranquicia")
                    {
                        pnlAgregarObservacion.Visible = true;
                    }

                }

                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    if (liq.Liquidaciones.Estado == "CasaMatriz")
                    {
                        pnlAgregarObservacion.Visible = true;
                    }
                }


            }

        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                var result = dbContext.LiquidacionesObservaciones
                        .Where(x => x.IDLiquidacionDetalle == idLiquidacionDetalle).OrderByDescending(x => x.FechaGeneracion)
                        .Select(x => new LiquidacionDetalleObservacionesViewModel
                        {
                            FechaGeneracion = x.FechaGeneracion,
                            Observaciones = x.Observacion,
                            Origen = x.Origen
                        }).ToDataSourceResult(take, skip, sort, filter);
                return result;
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void guardar(string observacion)
    {
        using (var dbContext = new ACHEEntities())
        {
            string Origen = "Admin";
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                Origen = "Franquicia";
            }
            LiquidacionesObservaciones liq = new LiquidacionesObservaciones();
            liq.IDLiquidacionDetalle = idLiquidacionDetalle;
            liq.Observacion = observacion;
            liq.Origen = Origen;
            liq.FechaGeneracion = DateTime.Now;
            dbContext.LiquidacionesObservaciones.Add(liq);
            dbContext.SaveChanges();
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

    }

}