﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;


public partial class common_alertas_config : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentUser"] == null && HttpContext.Current.Session["CurrentFranquiciasUser"] == null)
                Response.Redirect("~/home.aspx");
            else if (HttpContext.Current.Session["CurrentUser"] != null) {
                this.cargarCombos();
            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                this.hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
        }
    }

    private void cargarCombos() {
        this.cargarMarcas();
        this.cargarFranquicias();
        this.cargarEmpresas();
        this.cargarComercios();
    }

    private void cargarMarcas() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    private void cargarFranquicias() {
        try {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    private void cargarComercios() {
        using (var dbContext = new ACHEEntities()) {
            var comercios = dbContext.Comercios.OrderBy(x => x.NombreFantasia).Select(x=> new {NombreFantasia = x.NombreFantasia, IDComercio = x.IDComercio}).ToList();
            if (comercios != null) {
                ddlComercios.DataSource = comercios;
                ddlComercios.DataTextField = "NombreFantasia";
                ddlComercios.DataValueField = "IDComercio";
                ddlComercios.DataBind();
                ddlComercios.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarEmpresas() {
        try {
            using (var dbContext = new ACHEEntities()) {
                List<Empresas> listEmpresas = dbContext.Empresas.OrderBy(x => x.Nombre).ToList();
                this.ddlEmpresas.DataSource = listEmpresas;
                this.ddlEmpresas.DataValueField = "IDEmpresa";
                this.ddlEmpresas.DataTextField = "Nombre";
                this.ddlEmpresas.DataBind();

                this.ddlEmpresas.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex) {
            throw ex;
        }

    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
       
        int idFranquicia = 0;

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }

        if (HttpContext.Current.Session["CurrentUser"] != null ||  idFranquicia > 0) {
            using (var dbContext = new ACHEEntities()) {
                var info = dbContext.Alertas.Include("Comercios").Include("Franquicia").Include("Marca").Include("Empresa")
                    .Where(x => idFranquicia == 0 || x.IDFranquicia == idFranquicia)
                    .OrderBy(x => x.Nombre)
                    .Select(x => new {
                        ID = x.IDAlerta,
                        Nombre = x.Nombre,
                        Tipo = x.Tipo,
                        Prioridad = x.Prioridad == "A" ? "Alta" : (x.Prioridad == "M" ? "Media" : "Baja"),
                        Activa = x.Activa ? "Si" : "No",
                        Restringido = x.Restringido ? "Tarjetas en seguimiento" : "Todas las tarjetas",
                        TipoEntidad = x.IDComercio != null ? "Comercio" : (x.IDFranquicia != null ? "Franquicia" : (x.IDMarca != null ? "Marca" : (x.IDEmpresa != null ? "Empresa" : ""))),
                        NombreEntidad = x.IDComercio != null ? x.Comercios.NombreFantasia : (x.IDFranquicia != null ? x.Franquicias.NombreFantasia : (x.IDMarca != null ? x.Marcas.Nombre : (x.IDEmpresa != null ? x.Empresas.Nombre : ""))),
                        IDComercio = x.IDComercio,
                        IDMarca = x.IDMarca,
                        IDFranquicia = x.IDFranquicia,
                        IDEmpresa = x.IDEmpresa
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();

                return info;
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                int idFranquicia = usu.IDFranquicia;
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Alertas.Where(x => x.IDAlerta == id && (idFranquicia == 0 || x.IDFranquicia == idFranquicia)).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Alertas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}