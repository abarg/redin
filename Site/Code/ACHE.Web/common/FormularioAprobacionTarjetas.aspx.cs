﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.Data;
using System.IO;
using ACHE.Model.ViewModels;

public partial class common_FormularioAprobacionTarjetas : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
            }
            else
                CargarCombos();
        }
    }



    private void CargarCombos()
    {

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            Franquicias todas = new Franquicias();
            todas.NombreFantasia = "Todas";
            todas.IDFranquicia = 0;
            listFranquicias.Insert(0, todas);

            ddlFranquicias.DataSource = listFranquicias;
            ddlFranquicias.DataValueField = "IDFranquicia";
            ddlFranquicias.DataTextField = "NombreFantasia";
            ddlFranquicias.DataBind();
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            int idfranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                 idfranquicia = usu.IDFranquicia;
            }
            using (var dbContext = new ACHEEntities())
            {

                var aux = dbContext.FormularioTarjetas.Include("Franquicias").Where(x => idfranquicia == 0 || x.IDFranquicia == idfranquicia);
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    aux = aux.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    aux = aux.Where(x => x.Fecha <= dtHasta);
                }



                var today = DateTime.Today;
             
                var result = aux.OrderByDescending(x => x.IDFormularioTarjetas).Select(x => new FormularioViewModel()
                    {
                        ID = x.IDFormularioTarjetas,
                        IDFranquicia=x.IDFranquicia??0,
                        FechaPedido = x.Fecha,
                        UnidadNegocio=x.UnidadNegocio,
                        Franquicia=x.Franquicias.NombreFantasia,
                        Estado=x.Estado,
                        Cliente=x.Cliente,
                        FechaEnvioProveedor=x.FechaEnvioProveedor,
                        Counter=x.Counter??0,
                        //Counter = (x.FechaEnvioProveedor != null) ? (today - x.FechaEnvioProveedor.Value).ToString() : "",
                    });
                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }
    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var form = dbContext.FormularioTarjetas.Where(x => x.IDFormularioTarjetas == id).FirstOrDefault();
                    if (form != null)
                    {
                        var costos = form.Costos.ToList();
                        if (costos.Any())
                            dbContext.Costos.RemoveRange(costos);
                        var esp = form.Especificaciones.ToList();
                        if (esp.Any())
                            dbContext.Especificaciones.RemoveRange(esp);
                        dbContext.FormularioTarjetas.Remove(form);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }


    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string idFranquicia, string fechaDesde, string fechaHasta)
    {
        string fileName = "formAprobacionTarjetas";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {

                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    //var info = dbContext.FormularioTarjetas.Where(x => int.Parse(idFranquicia) == 0 || x.IDFranquicia == int.Parse(idFranquicia)).OrderByDescending(x => x.Fecha).AsEnumerable();
                    var info = dbContext.FormularioTarjetas.OrderByDescending(x => x.Fecha).AsEnumerable();
                    if (idFranquicia != string.Empty)
                    {
                        info = info.Where(x => int.Parse(idFranquicia) == 0 || x.IDFranquicia == int.Parse(idFranquicia));
                    }
     
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.Fecha <= dtHasta);
                    }

                    dt = info.ToList().Select(x => new
                    {
                        NumeroFormulario = x.IDFormularioTarjetas,
                        Estado = x.Estado,
                        Counter = x.Counter ?? 0,
                        Cliente = x.Cliente,
                        FechaPedido = x.Fecha != null ? x.Fecha.Value.ToString() : "",
                        FechaEnvioProveedor = x.FechaEnvioProveedor != null ? x.FechaEnvioProveedor.Value.ToString() : "",
                        Franquicia = x.Franquicias.NombreFantasia,
                        UnidadNegocio = x.UnidadNegocio,

                    }).ToList().ToDataTable();

                    if (dt.Rows.Count > 0)
                        generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    else
                        throw new Exception("No se encuentran datos para los filtros seleccionados");
                    return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }


    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}