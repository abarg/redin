﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alertas-seguimiento.aspx.cs" Inherits="common_alertas_seguimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/common/alertas.aspx") %>">Alertas</a></li>
            <li>Tarjetas en seguimiento</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Tarjetas en Seguimiento</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtTarjeta" CssClass="form-control number" MinLength="5" MaxLength="15"></asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        <button id="btnBuscarTarjetas" class="btn" type="button" onclick="buscarTarjetas();">Buscar</button>
                    </div>
                    <div class="col-lg-4" id="divResultado1" style="display:none">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTarjetas">
                            <asp:ListItem Text="Seleccione una tarjeta" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-2" id="divResultado2" style="display:none">
                        <button runat="server" id="btnAceptar" class="btn btn-success" type="button" onclick="agregar();">Aceptar</button>
                    </div>
                </div>
                <div class="row" style="margin-left:5px">
                    <table class="table" id="tbDetalle">
			            <thead>
				            <tr>
					            <th>Numero</th> 
                                <th>Socio</th> 
                                <th>Nro Documento</th>
                                <th>Eliminar</th> 
				            </tr>
			            </thead>
			            <tbody id="tBody">
                                        
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/alertas-seguimiento.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
