﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_Terminales : System.Web.UI.Page {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
                cargarCombos();
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        if (idFranquicia > 0 || (HttpContext.Current.Session["CurrentUser"] != null)) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.Terminales.Include("Domicilios")
                    .Where(x => idFranquicia == 0 || x.Comercios.IDFranquicia == idFranquicia)
                    .OrderBy(x => x.IDTerminal)
                    .Select(x => new TerminalesFullViewModel() {
                        IDComercio = x.IDComercio,
                        IDTerminal = x.IDTerminal,
                        SDS = x.Comercios.SDS,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        RazonSocial = x.Comercios.RazonSocial,
                        NroDocumento = x.Comercios.NroDocumento,
                        POSTerminal = x.POSTerminal,
                        POSSistema = (x.POSSistema != null) ? x.POSSistema : "",
                        NumEst = x.NumEst,
                        CobrarUsoRed = x.CobrarUsoRed,
                        IDMarca = x.Comercios.IDMarca ?? 0,
                        IDFranquicia = x.Comercios.IDFranquicia,
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    private void cargarCombos() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
            }
            else {
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataSource = listFranquicias;
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();

                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
                listMarcas = bMarca.getMarcas();
            }

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var terminal = dbContext.Terminales.Where(x => x.IDTerminal == id).FirstOrDefault();
                    terminal.Activo = false;
                    terminal.Estado = "DE";
                    dbContext.SaveChanges();
                }
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string SDS, string NombreFantasia, string RazonSocial, string NroDocumento,
        string NumEst,string POSTerminal, int idMarca, int idFranquicia) {
        string fileName = "Comercios";
        string path = "/tmp/";
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {

                    dbContext.Database.CommandTimeout = 1000;

                    var info = dbContext.Terminales.Include("Domicilios").Include("Comercios")
                        .OrderBy(x => x.IDTerminal).AsEnumerable();

                    if (NombreFantasia != "")
                        info = info.Where(x => x.Comercios.NombreFantasia.ToLower().Contains(NombreFantasia.ToLower()));
                    //var i =info.ToList().Count();
                    if (SDS != "")
                        info = info.Where(x => x.Comercios.SDS.ToLower().Contains(SDS.ToLower()));
                    if (RazonSocial != "")
                        info = info.Where(x => x.Comercios.RazonSocial.ToLower().Contains(RazonSocial.ToLower()));
                    if (NroDocumento != "")
                        info = info.Where(x => x.Comercios.NroDocumento != null && x.Comercios.NroDocumento.ToLower().Contains(NroDocumento.ToLower()));
                    if (POSTerminal != "")
                        info = info.Where(x => x.POSTerminal.ToLower().Contains(POSTerminal.ToLower()));
                    if (NumEst != "")
                        info = info.Where(x => x.NumEst.ToLower().Contains(NumEst.ToLower()));
                    if (idMarca > 0)
                        info = info.Where(x => x.Comercios.IDMarca.HasValue && x.Comercios.IDMarca == idMarca);
                    if (idFranquicia > 0)
                        info = info.Where(x => x.Comercios.IDFranquicia.HasValue && x.Comercios.IDFranquicia == idFranquicia);

                    dt = info.ToList().Select(x => new {

                        POSSistema = x.POSSistema,
                        POSTerminal = x.POSTerminal,
                        Tipo=x.POSTipo,
                        POSEstablecimiento = x.POSEstablecimiento,
                        Franquicia = x.Comercios.IDFranquicia.HasValue ? x.Comercios.Franquicias.NombreFantasia : "",
                    //    Marca = x.Comercios.IDMarca.HasValue ? x.comercios.Marcas.Nombre : "",
                        SDS = x.Comercios.SDS,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        RazonSocial = x.Comercios.RazonSocial,
                        TipoDocumento = x.Comercios.TipoDocumento,
                        NroDocumento = x.Comercios.NroDocumento,

                        NumEst = x.NumEst,


                        DescuentoLunes = x.Descuento,
                        DescuentoMartes = x.Descuento2,
                        DescuentoMiercoles = x.Descuento3,
                        DescuentoJueves = x.Descuento4,
                        DescuentoViernes = x.Descuento5,
                        DescuentoSabDom = x.Descuento6,

                        DescuentoDescripcion = x.DescuentoDescripcion,

                        ModalidadEfectivo = x.ModalidadEfectivo ? "Si" : "No",
                        ModalidadDebito = x.ModalidadDebito ? "Si" : "No",
                        ModalidadCredito = x.ModalidadCredito ? "Si" : "No",
                        DescuentoVipLunes = x.DescuentoVip.HasValue ? x.DescuentoVip.Value.ToString() : "",
                        DescuentoVipMartes = x.DescuentoVip2.HasValue ? x.DescuentoVip2.Value.ToString() : "",
                        DescuentoVipMiercoles = x.DescuentoVip3.HasValue ? x.DescuentoVip3.Value.ToString() : "",
                        DescuentoVipJueves = x.DescuentoVip4.HasValue ? x.DescuentoVip4.Value.ToString() : "",
                        DescuentoVipViernes = x.DescuentoVip5.HasValue ? x.DescuentoVip5.Value.ToString() : "",
                        DescuentoVipSabDom = x.DescuentoVip6.HasValue ? x.DescuentoVip6.Value.ToString() : "",

                        DescuentoDescripcionVip = x.DescuentoDescripcionVip,


                        ArancelLunes = x.POSArancel.HasValue ? x.POSArancel.Value.ToString() : "",
                        ArancelMartes = x.Arancel2.HasValue ? x.Arancel2.Value.ToString() : "",
                        ArancelMiercoles = x.Arancel3.HasValue ? x.Arancel3.Value.ToString() : "",
                        ArancelJueves = x.Arancel4.HasValue ? x.Arancel4.Value.ToString() : "",
                        ArancelViernes = x.Arancel5.HasValue ? x.Arancel5.Value.ToString() : "",
                        ArancelSabado = x.Arancel6.HasValue ? x.Arancel6.Value.ToString() : "",
                        ArancelDomingo = x.Arancel7.HasValue ? x.Arancel7.Value.ToString() : "",


                        PuntosLunes = x.POSPuntos.HasValue ? x.POSPuntos.Value.ToString() : "",
                        PuntosMartes = x.Puntos2.HasValue ? x.Puntos2.Value.ToString() : "",
                        PuntosMiercoles = x.Puntos3.HasValue ? x.Puntos3.Value.ToString() : "",
                        PuntosJueves = x.Puntos4.HasValue ? x.Puntos4.Value.ToString() : "",
                        PuntosViernes = x.Puntos5.HasValue ? x.Puntos5.Value.ToString() : "",
                        PuntosSabado = x.Puntos6.HasValue ? x.Puntos6.Value.ToString() : "",
                        PuntosDomingo = x.Puntos7.HasValue ? x.Puntos7.Value.ToString() : "",

                        MulPuntosLunes = x.MultiplicaPuntos1,
                        MulPuntosMartes = x.MultiplicaPuntos2,
                        MulPuntosMiercoles = x.MultiplicaPuntos3,
                        MulPuntosJueves = x.MultiplicaPuntos4,
                        MulPuntosViernes = x.MultiplicaPuntos5,
                        MulPuntosSabado = x.MultiplicaPuntos6,
                        MulPuntosDomingo = x.MultiplicaPuntos7,


                        PuntosVipLunes = x.PuntosVip1.HasValue ? x.PuntosVip1.Value.ToString() : "",
                        PuntosVipMartes = x.PuntosVip2.HasValue ? x.PuntosVip2.Value.ToString() : "",
                        PuntosVipMiercoles = x.PuntosVip3.HasValue ? x.PuntosVip3.Value.ToString() : "",
                        PuntosVipJueves = x.PuntosVip4.HasValue ? x.PuntosVip4.Value.ToString() : "",
                        PuntosVipViernes = x.PuntosVip5.HasValue ? x.PuntosVip5.Value.ToString() : "",
                        PuntosVipSabado = x.PuntosVip6.HasValue ? x.PuntosVip6.Value.ToString() : "",
                        PuntosVipDomingo = x.PuntosVip7.HasValue ? x.PuntosVip7.Value.ToString() : "",
                        MulPuntosVipLunes = x.MultiplicaPuntosVip1.HasValue ? x.MultiplicaPuntosVip1.Value.ToString() : "",
                        MulPuntosVipMartes = x.MultiplicaPuntosVip2.HasValue ? x.MultiplicaPuntosVip2.Value.ToString() : "",
                        MulPuntosVipMiercoles = x.MultiplicaPuntosVip3.HasValue ? x.MultiplicaPuntosVip3.Value.ToString() : "",
                        MulPuntosVipJueves = x.MultiplicaPuntosVip4.HasValue ? x.MultiplicaPuntosVip4.Value.ToString() : "",
                        MulPuntosVipViernes = x.MultiplicaPuntosVip5.HasValue ? x.MultiplicaPuntosVip5.Value.ToString() : "",
                        MulPuntosVipSabado = x.MultiplicaPuntosVip6.HasValue ? x.MultiplicaPuntosVip6.Value.ToString() : "",
                        MulPuntosVipDomingo = x.MultiplicaPuntosVip7.HasValue ? x.MultiplicaPuntosVip7.Value.ToString() : "",

                        //ModalidadDescuentoVip = x.ModalidadDescuentoVip,

                        ModalidadEfectivoVip = x.ModalidadEfectivoVip ? "Si" : "No",
                        ModalidadDebitoVip = x.ModalidadDebitoVip ? "Si" : "No",
                        ModalidadCreditoVip = x.ModalidadCreditoVip ? "Si" : "No",
                        CobrarUsoRed = x.CobrarUsoRed ? "Si" : "No",
                        AffinityBenef = x.AffinityBenef,
                        Activo = (x.Activo ? "Si" : "No"),
                        
                        POSTipo = x.POSTipo,


                        POSMarca = x.POSMarca,
                        POSReprogramado = x.POSReprogramado ? "Si" : "No",
                        POSFechaReprogramacion = x.POSFechaReprogramacion.HasValue ? x.POSFechaReprogramacion.Value.ToShortDateString() : "",
                        POSObservaciones = x.POSObservaciones,
                        POSFechaActivacion = x.POSFechaActivacion.HasValue ? x.POSFechaActivacion.Value.ToShortDateString() : "",
                        POSWeb = x.HabilitarPOSWeb ? "Si" : "No",
                        GiftWeb = x.HabilitarGifcard ? "Si" : "No",

                        FidelyUsuario = x.FidelyUsuario,
                        FidelyPwd = x.FidelyPwd,
                        FidelyClaveLicencia = x.FidelyClaveLicencia,

                        Zona = x.IDZona.HasValue ? x.Zonas.Nombre : "",
                        Pais = x.Domicilios != null ? x.Domicilios.Pais : "",
                        Provincia = x.Domicilios != null ? x.Domicilios.Provincias.Nombre : "",
                        Ciudad = x.Domicilios != null && x.Domicilios.Ciudad.HasValue ? x.Domicilios.Ciudades.Nombre : "",
                        Domicilio = x.Domicilios != null ? x.Domicilios.Domicilio : "",
                        PisoDepto = x.Domicilios != null ? x.Domicilios.PisoDepto : "",
                        CodigoPostal = x.Domicilios != null ? x.Domicilios.CodigoPostal : "",
                        TelefonoDom = x.Domicilios != null ? x.Domicilios.Telefono : "",
                        Fax = x.Domicilios != null ? x.Domicilios.Fax : "",

                        CostoPosPropio = x.CostoPOSPropio

                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}