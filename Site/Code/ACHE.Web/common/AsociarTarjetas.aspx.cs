﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;
public partial class common_AsociarTarjetas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static SociosViewModel Buscar(string documento)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Tarjetas.Include("Socios").Where(x => x.IDSocio.HasValue && x.Socios.NroDocumento == documento && (x.Socios.Responsable != false) ).FirstOrDefault();
                    if (aux != null)
                    {
                        return new SociosViewModel()
                        {
                            Nombre = aux.Socios.Nombre,
                            Apellido = aux.Socios.Apellido,
                            NroCuenta = aux.Socios.FechaNacimiento.HasValue ? aux.Socios.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "",
                            Sexo = aux.Socios.Sexo,
                            Email = aux.Socios.Email,
                            Celular = aux.Socios.Celular,
                            IDSocio = aux.Socios.IDSocio
                        };
                    }
                    else
                        return null;
                }
            }
            else
                return null;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}