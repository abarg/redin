﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reporte-Verificaciones.aspx.cs" Inherits="common_Reporte_Verificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
             <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/reporte-verificaciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Verificaciones POS</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Verificaciones realizadas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" ClientIDMode="Static" Value="0" />
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                         <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label> Nro referencia</label>
                            <asp:TextBox runat="server" ID="txtNroReferencia" CssClass="form-control" MaxLength="10" />
                        </div>
                       <asp:Panel runat="server" ID="pnlFranquicias" class="col-sm-3">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlFranquicias" />
                        </asp:Panel>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

