﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard-Actividades.aspx.cs" Inherits="common_Dashboard_actividades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }

        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }

        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }

        .ov_boxes .ov_text {
            width: 125px !important;
        }

        .modal.modal-wide .modal-dialog {
            width: 90%;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }


                .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            position: absolute;
            top: 50%;
            left: 50%;
            display: none;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="row admin" style="display:none"> 
          <form runat="server">
            <div class="col-sm-3" >
                <label>Franquicia</label>
                <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"  onchange="mostrarReporte(this.value);" />
            </div>
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="-1" />
            <asp:HiddenField runat="server" ID="hdnIDFranquiciasAdmin" Value="-1" />
          </form>
      </div>
    <div class="dashboard" style="display:none">
   
        <div class="row">
            <div class="col-sm-12 tac">
                <h3 class="heading">Estadísticas generales</h3>
                <ul class="ov_boxes">
                    <li>
                        <div class="p_bar_down p_canvas">
                            <img src="../../img/dashboard/sex.png" style="width: 48px" />
                        </div>
                        <div class="ov_text" style="padding: 3px">
                            <strong id="resultado_sexo" style="font-size: 13px; padding-top: 9px;">Calculando...</strong><%--F: 100 - M: 300--%>
						    Total Socios: <span id="totalSexo"></span>
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/email.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_mails">Calculando...</strong>
                            Cantidad de
                            <br />
                            mails
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/MetroUI_Phone.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_cel">Calculando...</strong>
                            Cantidad de
                            <br />
                            celulares
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/credit_card_yellow.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_tarjetas_activas">Calculando...</strong>
                            Cantidad de
                            <br />
                            Tarjetas activas
                        </div>
                    </li>
                  
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 tac">
                <h3 class="heading">
                    <asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
                <ul class="ov_boxes">

                    <li>
                        <div class="p_line_up p_canvas">
                            <img src="../../img/dashboard/Tracking_3D.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_actividades">Calculando...</strong>
                            Cantidad de
                            <br />
                            <a href="#">Beneficios</a> 
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/transaction.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_cantidad_socios_multiple_actividad">Calculando...</strong>
                            Cantidad de socios
                            <br />
                             con mas de un beneficio
                        </div>
                    </li>

                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/presentation.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_beneficiarios_ubicacion">Calculando...</strong>
                            Beneficiarios de<br />
                            San Martin 
                        </div>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="row">
                    <div class="col-sm-6 col-lg-6"  >
                        <div class="heading clearfix">
                            <h3 class="pull-left">Cantidad de beneficiarios por Tipo de Beneficio</h3>
                        </div>

  
                        <table id="tblActividadesPorSocio"class="table table-striped table-bordered mediaTable">
                            <thead>
                                <tr>
                                    <th class="essential persist">Beneficio</th>
                                    <th class="essential persist">Cantidad Socios</th>
                                </tr>
                            </thead>
                            <tbody id="bodySociosMes">
                                <tr>
                                    <td colspan="4">Calculando...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

               <div class="col-sm-6 col-lg-6"  >
                      <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Beneficiarios por tipo de Beneficio</h3>

                       <%--     <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>--%>
                          </div>
                          <div class="card-body">
                            <canvas id="pieXActividad" style="height:250px"></canvas>
                          </div>
                          <!-- /.card-body -->
                     </div>

              </div>


	    </div>


        <br /> <hr /> <br />


        <div class="row">


               <div class="col-sm-6 col-lg-6"  >
                        <div class="heading clearfix">
                            <h3 class="pull-left">Cantidad de beneficiarios por Secretaria</h3>
                        </div>

  
                        <table id="tblActividadesPorSecretaria"class="table table-striped table-bordered mediaTable">
                            <thead>
                                <tr>
                                    <th class="essential persist">Secretaria</th>
                                    <th class="essential persist">Cantidad Socios</th>
                                </tr>
                            </thead>
                            <tbody id="bodySociosPorSecretaria">
                                <tr>
                                    <td colspan="4">Calculando...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                   <div class="col-sm-6 col-lg-6"  >
                      <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Beneficiarios por Secretarias</h3>

                       <%--     <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>--%>
                          </div>
                          <div class="card-body">
                            <canvas id="pieXSecretaria" style="height:250px"></canvas>
                          </div>
                          <!-- /.card-body -->
                     </div>

              </div>

        </div>


    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
              
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                    <%--<a href="/modulos/reportes/facturacion.aspx">Facturación</a>--%>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success"  ID="btnExportar">Exportar a Excel</a>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="loader"></div>



    <!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/dashboard-actividades.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <!-- charts functions -->

    <!-- tablas -->
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
 
        <script src="<%= ResolveUrl("~/lib/chartjs-old/Chart.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

