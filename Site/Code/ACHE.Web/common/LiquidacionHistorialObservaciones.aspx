﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LiquidacionHistorialObservaciones.aspx.cs" Inherits="common_LiquidacionHistorialObservaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
            <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#" onclick="irAlDetalle();">Liquidación</a></li>
            <li class="last">Historial observaciones</li>
        </ul>
    </div>      
    <form runat="server" id="formEdicion" > 
        <asp:HiddenField runat="server" value="0" ID="hdfIDLiquidacion" />
    </form>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading"><asp:Literal runat="server" ID="litTitulo"></asp:Literal></h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlAgregarObservacion"  Visible="false"  class="row">
        <div class="col-sm-8 col-sm-md-8">
            <button class="btn btn-success" onclick="abrirModal();" type="button">Agregar</button>
        </div>
    </asp:Panel>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
    <div class="modal fade" id="modalAgregarObservacion">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="H1">Edición observación</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form id="formEdicionObs">
                            <div class="row">                                
                                <div class="col-sm-12">
                                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Observación</label>
                                    <div class="col-lg-10">
                                        <textarea  id="txtObservacion" rows="5" style="width:100%" class="form-control required validDate" ></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" id="btnGuardar" onclick="guardar();">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
       
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/liquidacion-historial-observaciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

