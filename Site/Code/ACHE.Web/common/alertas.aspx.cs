﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model;
using System.Web.Script.Services;

public partial class common_alertas : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                litAlertasAlta2.Visible = false;
                LoadAlertasByFranquicias();
            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                LoadAlertasByFranquicia();
            }
            else
                Response.Redirect("~/home.aspx");                
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            MasterPageFile = "~/MasterPageFranquicias.master";
        }
    }

    private void LoadAlertasByFranquicia() {
        int cant = 0;
        var html = string.Empty;
        using (var dbContext = new ACHEEntities()) {
            int idFranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }

            if (idFranquicia > 0) {
                string prioridad = "";

                var altas = dbContext.Alertas.Where(x => x.IDFranquicia == idFranquicia && x.Activa && x.Prioridad == "A").ToList();
                if (idFranquicia > 0)
                    altas.Where(x => x.IDFranquicia == idFranquicia);
                if (altas.Any()) {
                    html = string.Empty;
                    var cantAlertasMarcas = altas.Where(x => x.IDMarca != null).ToList().Count();
                    var cantAlertasComercios = altas.Where(x => x.IDComercio != null).ToList().Count();
                    var cantAlertasEmpresas = altas.Where(x => x.IDEmpresa != null).ToList().Count();
                    prioridad = "A";

                    html += "<tr>";
                    html += "<td>  Marcas </td>";
                    html += "<td>" + cantAlertasMarcas + "</td>";
                    if (cantAlertasMarcas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Marcas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Comercios </td>";
                    html += "<td>" + cantAlertasComercios + "</td>";
                    if (cantAlertasComercios > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Comercios\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Empresas </td>";
                    html += "<td>" + cantAlertasEmpresas + "</td>";
                    if (cantAlertasEmpresas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Empresas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";

                    litAlertasAlta2.Text = html;
                }
                else
                    litAlertasAlta.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

                var medias = dbContext.Alertas.Where(x => x.IDFranquicia == idFranquicia && x.Activa && x.Prioridad == "M").ToList();
                if (idFranquicia > 0)
                    medias.Where(x => x.IDFranquicia == idFranquicia);
                if (medias.Any()) {
                    html = string.Empty;
                    var cantAlertasMarcas = medias.Where(x => x.IDMarca != null).ToList().Count();
                    var cantAlertasComercios = medias.Where(x => x.IDComercio != null).ToList().Count();
                    var cantAlertasEmpresas = medias.Where(x => x.IDEmpresa != null).ToList().Count();
                    prioridad = "M";

                    html += "<tr>";
                    html += "<td>  Marcas </td>";
                    html += "<td>" + cantAlertasMarcas + "</td>";
                    if (cantAlertasMarcas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Marcas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Comercios </td>";
                    html += "<td>" + cantAlertasComercios + "</td>";
                    if (cantAlertasComercios > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Comercios\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Empresas </td>";
                    html += "<td>" + cantAlertasEmpresas + "</td>";
                    if (cantAlertasEmpresas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Empresas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";


                    litAlertasMedia2.Text = html;
                }
                else
                    litAlertasMedia.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

                var bajas = dbContext.Alertas.Where(x => x.IDFranquicia == idFranquicia && x.Activa && x.Prioridad == "B").ToList();
                if (idFranquicia > 0)
                    medias.Where(x => x.IDFranquicia == idFranquicia);
                if (bajas.Any()) {
                    html = string.Empty;
                    var cantAlertasMarcas = bajas.Where(x => x.IDMarca != null).ToList().Count();
                    var cantAlertasComercios = bajas.Where(x => x.IDComercio != null).ToList().Count();
                    var cantAlertasEmpresas = bajas.Where(x => x.IDEmpresa != null).ToList().Count();
                    prioridad = "B";

                    html += "<tr>";
                    html += "<td>  Marcas </td>";
                    html += "<td>" + cantAlertasMarcas + "</td>";
                    if (cantAlertasMarcas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Marcas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Comercios </td>";
                    html += "<td>" + cantAlertasComercios + "</td>";
                    if (cantAlertasComercios > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Comercios\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Empresas </td>";
                    html += "<td>" + cantAlertasEmpresas + "</td>";
                    if (cantAlertasEmpresas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Empresas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";


                    litAlertasBaja2.Text = html;
                }
                else
                    litAlertasBaja.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";
            }
        }
    }

    private void LoadAlertasByFranquicias() {
        int cant = 0;
        var html = string.Empty;
        using (var dbContext = new ACHEEntities()) {
            int idFranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }

            if (HttpContext.Current.Session["CurrentUser"] != null || idFranquicia > 0) {
                var altas = dbContext.Alertas.Where(x => x.IDFranquicia != null && x.Activa && x.Prioridad == "A").ToList();
                if (idFranquicia > 0)
                    altas.Where(x => x.IDFranquicia == idFranquicia);
                if (altas.Any()) {
                    html = string.Empty;
                    List<int> aux = new List<int>();
                    foreach (var alerta in altas) {
                        if (!aux.Exists(x => x == alerta.IDFranquicia)) {
                            cant = altas.Where(x => x.IDFranquicia == alerta.IDFranquicia).Count();

                            html += "<tr>";
                            html += "<td>" + alerta.Franquicias.NombreFantasia + "</td>";
                            html += "<td>" + cant + "</td>";
                            if (cant > 0)
                                html += "<td><a style='cursor:pointer;' onclick='detalleFranquicia(" + alerta.IDFranquicia + ",\"A\")'>detalle</td>";
                            else
                                html += "<td>&nbsp;</td>";
                            html += "</tr>";
                            aux.Add(alerta.IDFranquicia ?? 0);
                        }
                    }
                    litAlertasAlta.Text = html;
                }
                else
                    litAlertasAlta.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

                var medias = dbContext.Alertas.Where(x => x.IDFranquicia != null && x.Activa && x.Prioridad == "M").ToList();
                if (idFranquicia > 0)
                    medias.Where(x => x.IDFranquicia == idFranquicia);
                if (medias.Any()) {
                    List<int> aux = new List<int>();
                    html = string.Empty;
                    foreach (var alerta in medias) {
                        if (!aux.Exists(x => x == alerta.IDFranquicia)) {
                            cant = medias.Where(x => x.IDFranquicia == alerta.IDFranquicia).Count();

                            html += "<tr>";
                            html += "<td>" + alerta.Franquicias.NombreFantasia + "</td>";
                            html += "<td>" + cant + "</td>";
                            if (cant > 0)
                                html += "<td><a style='cursor:pointer;' onclick='detalleFranquicia(" + alerta.IDFranquicia + ",\"M\")'>detalle</td>";
                            else
                                html += "<td>&nbsp;</td>";
                            html += "</tr>";
                            aux.Add(alerta.IDFranquicia ?? 0);
                        }
                    }

                    litAlertasMedia.Text = html;
                }
                else
                    litAlertasMedia.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

                var bajas = dbContext.Alertas.Where(x => x.IDFranquicia != null && x.Activa && x.Prioridad == "B").ToList();
                if (idFranquicia > 0)
                    medias.Where(x => x.IDFranquicia == idFranquicia);
                if (bajas.Any()) {
                    List<int> aux = new List<int>();
                    html = string.Empty;
                    foreach (var alerta in bajas) {
                        if (!aux.Exists(x => x == alerta.IDFranquicia)) {
                            cant = bajas.Where(x => x.IDFranquicia == alerta.IDFranquicia).Count();

                            html += "<tr>";
                            html += "<td>" + alerta.Franquicias.NombreFantasia + "</td>";
                            html += "<td>" + cant + "</td>";
                            if (cant > 0)
                                html += "<td><a style='cursor:pointer;' onclick='detalleFranquicia(" + alerta.IDFranquicia + ",\"B\")'>detalle</td>";
                            else
                                html += "<td>&nbsp;</td>";
                            html += "</tr>";
                            aux.Add(alerta.IDFranquicia ?? 0);
                        }
                    }

                    litAlertasBaja.Text = html;
                }
                else
                    litAlertasBaja.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";
            }
        }
    }

    [WebMethod(true)]
    public static string obtenerDetalleFranquicia(int idFranquicia,string prioridad) {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            if(usu != null)
                idFranquicia = usu.IDFranquicia;
            using (var dbContext = new ACHEEntities()) {
                var alertas = dbContext.Alertas.Where(x => x.IDFranquicia == idFranquicia && x.Activa && x.Prioridad == prioridad).ToList();

                if (alertas != null) {
                    var AlertasMarcas = alertas.Where(x => x.IDMarca != null).ToList();
                    var AlertasComercios = alertas.Where(x => x.IDComercio != null).ToList();
                    var cantAlertasMarcas = alertas.Where(x => x.IDMarca != null).ToList().Count();
                    var cantAlertasComercios = alertas.Where(x => x.IDComercio != null).ToList().Count();
                    var cantAlertasEmpresas = alertas.Where(x => x.IDEmpresa != null).ToList().Count();

                    html += "<tr>";
                    html += "<td>  Marcas </td>";
                    html += "<td>" + cantAlertasMarcas + "</td>";
                    if (cantAlertasMarcas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Marcas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Comercios </td>";
                    html += "<td>" + cantAlertasComercios + "</td>";
                    if (cantAlertasComercios > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Comercios\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td>  Empresas </td>";
                    html += "<td>" + cantAlertasEmpresas + "</td>";
                    if (cantAlertasEmpresas > 0)
                        html += "<td><a style='cursor:pointer;' onclick='alertasByEntidad(" + idFranquicia + ",\"" + prioridad + "\",\"Empresas\")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }
        return html;
    }

    [WebMethod(true)]
    public static string obtenerAlertasByEntidad(int idFranquicia, string prioridad, string entidad) {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            if (usu != null) {
                idFranquicia = usu.IDFranquicia;
            }
            DateTime fechaDesde = DateTime.Now;

            using (var dbContext = new ACHEEntities()) {
                var alertas = dbContext.Alertas.Where(x => x.IDFranquicia == idFranquicia && x.Activa && x.Prioridad == prioridad.ToUpper()).ToList();
                string nombreEntidad = "";
                if (entidad.ToLower() == "marcas") {
                    alertas = alertas.Where(x => x.IDMarca != null).ToList();
                }
                if (entidad.ToLower() == "comercios") {
                    alertas = alertas.Where(x => x.IDComercio != null).ToList();
                }
                if (entidad.ToLower() == "empresas") {
                    alertas = alertas.Where(x => x.IDEmpresa != null).ToList();
                }

                foreach (var alerta in alertas) {
                    if (alerta != null) {

                        if (alerta.IDComercio != null) {
                            nombreEntidad = alerta.Comercios.NombreFantasia;
                        }
                        if (alerta.IDMarca != null) {
                            nombreEntidad = alerta.Marcas.Nombre;
                        }
                        if (alerta.IDEmpresa != null) {
                            nombreEntidad = alerta.Empresas.Nombre;
                        }

                        int cant = 0;
                        cant = obtenerCantidad(alerta);
                        html += "<tr>";
                        html += "<td>" + nombreEntidad + "</td>";
                        html += "<td>" + alerta.Tipo + "</td>";
                        html += "<td>" + alerta.Nombre + "</td>";
                        html += "<td>" + obtenerDetalle(alerta) + "</td>";
                        html += "<td>" + cant + "</td>";
                        if (cant > 0)
                            html += "<td><a style='cursor:pointer;' onclick='detalleAlerta(" + alerta.IDAlerta + ")'>detalle</td>";
                        else
                            html += "<td>&nbsp;</td>";
                        html += "</tr>";
                    }
                    else
                        html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                }
            }

        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerDetalleAlerta(int idAlerta) {
        var html = string.Empty;
        string tarjetas = "";

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {

            DateTime fechaDesde = DateTime.Now;

            using (var dbContext = new ACHEEntities()) {
                var alertas = dbContext.Alertas.Where(x => x.IDAlerta == idAlerta && x.Activa).ToList();

                foreach (var alerta in alertas) {
                    if (alerta != null) {
                        string idEntidad = "";
                        string entidad = "";

                        if (alerta.Restringido) {
                            //COMERCIOS
                            if (alerta.IDComercio != null) {
                                entidad = "Comercio";
                                idEntidad = alerta.IDComercio.ToString();
                                foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDComercio == alerta.IDComercio).ToList())
                                    tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                            }
                            //Marcas
                            if (alerta.IDMarca != null) {
                                entidad = "Marca";
                                idEntidad = alerta.IDMarca.ToString();
                                foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == alerta.IDMarca).ToList())
                                    tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                            }
                            //Empresas
                            if (alerta.IDEmpresa != null) {
                                entidad = "Empresa";
                                idEntidad = alerta.IDEmpresa.ToString();
                                foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDEmpresa == alerta.IDEmpresa).ToList())
                                    tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                            }

                            if (tarjetas.Length > 0)
                                tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
                        }

                        if (alerta.IDComercio != null) {
                            entidad = "Comercio";
                            idEntidad = alerta.IDComercio.ToString();
                        }
                        if (alerta.IDMarca != null) {
                            entidad = "Marca";
                            idEntidad = alerta.IDMarca.ToString();
                        }
                        if (alerta.IDEmpresa != null) {
                            entidad = "Empresa";
                            idEntidad = alerta.IDEmpresa.ToString();
                        }
                        string sql = queryAlertasByEntidad(alerta, tarjetas, entidad, idEntidad);

                        string nombreEntidad = "";
                        var list = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                        if (list.Any()) {
                            foreach (var detalle in list) {

                                if (alerta.IDComercio != null)
                                    nombreEntidad = detalle.NombreFantasia;
                                if (alerta.IDMarca != null)
                                    nombreEntidad = detalle.Empresa;
                                if (alerta.IDEmpresa != null)
                                    nombreEntidad = detalle.Marca;
                                html += "<tr>";
                                html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                html += "<td>" + nombreEntidad + "</td>";
                                html += "<td>" + detalle.Origen + "</td>";
                                html += "<td>" + detalle.Operacion + "</td>";
                                html += "<td style='text-align: right;'>" + detalle.ImporteOriginal.ToString("N2") + "</td>";
                                html += "<td>" + detalle.Numero + "</td>";
                                html += "<td>" + detalle.Socio + "</td>";
                                html += "</tr>";
                            }
                        }
                        else
                            html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                    }
                    else
                        html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                }
            }

        }

        return html;
    }

    [WebMethod(true)]
    public static int obtenerCantidad(Alertas alerta) {
        int cant = 0;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        string sql = "";
        string tarjetas = "";
        string idEntidad = "";
        string entidad = "";

        int idFranquicia = alerta.IDFranquicia ?? 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }

        if (HttpContext.Current.Session["CurrentUser"] != null || idFranquicia > 0) {
            using (var dbContext = new ACHEEntities()) {
                if (alerta.Restringido) {

                    //COMERCIOS
                    if (alerta.IDComercio != null) {
                        foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDComercio == alerta.IDComercio).ToList())
                            tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                    }
                    //Marcas
                    if (alerta.IDMarca != null) {
                        foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == alerta.IDMarca).ToList())
                            tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                    }
                    //Empresas
                    if (alerta.IDEmpresa != null) {
                        foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDEmpresa == alerta.IDEmpresa).ToList())
                            tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                    }

                    if (tarjetas.Length > 0)
                        tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
                }

                if (alerta.IDComercio != null) {
                    entidad = "Comercio";
                    idEntidad = alerta.IDComercio.ToString();
                }
                if (alerta.IDMarca != null) {
                    entidad = "Marca";
                    idEntidad = alerta.IDMarca.ToString();
                }
                if (alerta.IDEmpresa != null) {
                    entidad = "Empresa";
                    idEntidad = alerta.IDEmpresa.ToString();
                }

                DateTime fechaDesde = DateTime.Now;
                if (alerta.FechaDesde.HasValue)
                    fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);

                if (entidad.ToLower() != "comercio") {
                    if (alerta.Tipo == "Por monto") {
                        sql = "select count(Numero) from Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                    }
                    else if (alerta.Tipo == "Por cantidad") {
                        sql = "select count(cant) from (select distinct Numero as cant from  Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";

                    }
                    else if (alerta.Tipo == "Por monto y cantidad") {
                        sql = "select count(cant) from (select distinct Numero as cant from Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";
                    }
                }
                else {
                    if (alerta.Tipo == "Por monto") {
                        sql = "select count(Numero) from TransaccionesView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                    }
                    else if (alerta.Tipo == "Por cantidad") {
                        sql = "select count(cant) from (select distinct Numero as cant from  TransaccionesView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";

                    }
                    else if (alerta.Tipo == "Por monto y cantidad") {
                        sql = "select count(cant) from (select distinct Numero as cant from TransaccionesView where ID" + entidad + " =" + idEntidad;//CurrentEmpresasUser.IDEmpresa;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";
                    }
                }

                cant = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();
            }
        }
        return cant;
    }

    [WebMethod(true)]
    public static string obtenerDetalle(Alertas alerta) {
        string detalle = string.Empty;

        if (alerta.Tipo == "Por monto") {
            detalle = "Entre $" + alerta.MontoDesde.Value + " y $" + alerta.MontoHasta.Value;
        }
        else if (alerta.Tipo == "Por cantidad") {
            detalle = "Con trans >= " + alerta.CantTR.Value;
        }
        else if (alerta.Tipo == "Por monto y cantidad") {
            detalle = "Entre $" + alerta.MontoDesde.Value + " y $" + alerta.MontoHasta.Value;
            detalle += ". Con trans >= " + alerta.CantTR.Value;
        }

        if (alerta.FechaDesde.HasValue)
            detalle += ". En los últimos " + alerta.FechaDesde.Value + " días";
        if (alerta.Restringido)
            detalle += ". Sólo para tarjetas en seguimiento";

        return detalle;
    }

    [WebMethod(true)]
    public static string queryAlertasByEntidad(Alertas alerta,string tarjetas,string entidad,string idEntidad){
    var sql = string.Empty;
    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
    DateTime fechaDesde = DateTime.Now;

    if (alerta.FechaDesde.HasValue)
        fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);

    if (entidad.ToLower() != "comercio") {
        if (alerta.Tipo == "Por monto") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            sql += " order by ImporteOriginal desc, FechaTransaccion";
        }
        else if (alerta.Tipo == "Por cantidad") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;
            sql += " and Numero in (select distinct Numero as cant from Transacciones" + entidad + "sView where ID" + entidad + "=" + idEntidad;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
            sql += " order by Numero, FechaTransaccion";
        }
        else if (alerta.Tipo == "Por monto y cantidad") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesView where ID" + entidad + " =" + idEntidad;
            sql += " and Numero in (select distinct Numero as cant from TransaccionesView where ID" + entidad + "=" + idEntidad;
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            sql += " order by Numero, FechaTransaccion";
        }
    }
    else { 
            if (alerta.Tipo == "Por monto") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesView where ID" + entidad + " =" + idEntidad;
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            sql += " order by ImporteOriginal desc, FechaTransaccion";
        }
        else if (alerta.Tipo == "Por cantidad") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesView where ID" + entidad + " =" + idEntidad;
            sql += " and Numero in (select distinct Numero as cant from TransaccionesView where ID" + entidad + "=" + idEntidad;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
            sql += " order by Numero, FechaTransaccion";
        }
        else if (alerta.Tipo == "Por monto y cantidad") {
            sql = "select NombreFantasia,Empresa,Marca,FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from Transacciones" + entidad + "sView where ID" + entidad + " =" + idEntidad;
            sql += " and Numero in (select distinct Numero as cant from TransaccionesView where ID" + entidad + "=" + idEntidad;
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            sql += " order by Numero, FechaTransaccion";
        }
    }

    return sql;
}

    public class DetalleAlerta {
        public DateTime FechaTransaccion { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
        public decimal ImporteOriginal { get; set; }
        public string Socio { get; set; }
        public string Numero { get; set; }
        public string NombreFantasia { get; set; }
        public string Marca { get; set; }
        public string Empresa { get; set; }
    }

}

