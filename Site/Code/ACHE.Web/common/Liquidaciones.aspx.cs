﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_Liquidaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
            pnlFranquicias.Visible = false;
        }
        else if (HttpContext.Current.Session["CurrentUser"] != null)
            cargarFranquicias();
        else
            Response.Redirect("/login.aspx");
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void cargarFranquicias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var franquicias = dbContext.Franquicias.ToList();
            if (franquicias != null && franquicias.Count() > 0)
            {
                ddlFranquicias.DataSource = franquicias;
                ddlFranquicias.DataValueField = "IDFranquicia";
                ddlFranquicias.DataTextField = "NombreFantasia";
                ddlFranquicias.DataBind();
                ddlFranquicias.Items.Insert(0, new ListItem("", "0"));
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        if (idFranquicia > 0 || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.Liquidaciones.Include("Franquicia").Where(x => idFranquicia == 0 || x.IDFranquicia == idFranquicia).OrderByDescending(x => x.FechaGeneracion);
                if (fechaDesde != "")
                {
                    DateTime fechad = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaGeneracion >= fechad).OrderBy(x => x.FechaGeneracion);
                }

                if (fechaHasta != "")
                {
                    DateTime fechah = DateTime.Parse(fechaHasta);
                    results = results.Where(x => x.FechaGeneracion <=fechah).OrderBy(x => x.FechaGeneracion);
                }
           
                     return  results.Select(x => new LiquidacionesViewModel()
                    {
                        IDLiquidacion=x.IDLiquidacion,
                        Franquicia=x.Franquicias.NombreFantasia,
                        FechaGeneracion=x.FechaGeneracion,
                        FechaDesde = x.FechaDesde,
                        FechaHasta = x.FechaHasta,
                        Estado=x.Estado,
                        IDFranquicia = x.IDFranquicia,
                        ImporteTotal = x.LiquidacionDetalle.Sum(y => y.SubTotal) ?? 0
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
        }

    [WebMethod(true)]
    public static string Exportar(int idFranquicia,string fechaDesde, string fechaHasta)
    {
        string fileName = "Liquidaciones";
        string path = "/tmp/";

        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                 
                    var info = dbContext.Liquidaciones
                        .Include("Franquicias").Where(x => idFranquicia == 0 || x.IDFranquicia ==idFranquicia)
                        .OrderBy(x => x.FechaGeneracion).AsEnumerable();
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaGeneracion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.FechaGeneracion <= dtHasta);
                    }
                  
                    dt = info.ToList().Select(x => new
                    {
                        Franquicia = x.Franquicias.NombreFantasia,
                        FechaGeneracion = x.FechaGeneracion,
                        FechaDesde = x.FechaDesde,
                        FechaHasta = x.FechaHasta,
                        Estado = x.Estado,
                        IDFranquicia = x.IDFranquicia

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static ComprobanteLiquidacionesViewModel GetComprobantes(int id)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result= dbContext.Liquidaciones.Where(x => x.IDLiquidacion == id).Select(x => new ComprobanteLiquidacionesViewModel
                {
                    Factura=x.Factura,
                    Transferencia=x.ComprobanteTransferencia
                }).FirstOrDefault();
                return result;
            }
        }
        return null;

    }

   [System.Web.Services.WebMethod(true)]
   public static void Delete(int id)
   {
       if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
       {
           using (var dbContext = new ACHEEntities())
           {
               var entity = dbContext.Liquidaciones.Where(x => x.IDLiquidacion == id).FirstOrDefault();
               if (entity != null)
               {
                   dbContext.Liquidaciones.Remove(entity);
                   dbContext.SaveChanges();
               }
           }
       }
   }




}