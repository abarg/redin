﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="VerificacionesPOS.aspx.cs" Inherits="common_VerificacionesPOS" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <!-- multiselect -->
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li>Verificaciones POS</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Nueva planilla de verificacion</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>

            <form runat="server" id="formVerificacion" class="form-horizontal" role="form">
                <div id="divPaso1">
                     <asp:HiddenField runat="server" ID="hdnIDFranquicia" ClientIDMode="Static" Value="0" />
                    <h3 class="heading" style="color: #428bca">1. Complete los siguientes campos</h3>
                      <asp:Panel runat="server" ID="pnlFranquicias" class="form-group">
                        <label class="col-lg-3 control-label"><span class="f_req">*</span>&nbsp;Franquicia</label>
                        <div class="col-lg-6">
                            <asp:DropDownList runat="server" class="form-control chosen" ID="cmbFranquicia" data-placeholder="Seleccione una franquicia" ClientIDMode="Static" onchange="cargarProvincias();"/>
                            <span id="spnFranquicia" class="errorRequired" style="display: none;">Debe completar este campo</span>

                        </div>
                    </asp:Panel>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><span class="f_req">*</span>&nbsp;Provincia</label>
                        <div class="col-lg-6">
                            <asp:DropDownList runat="server" ID="cmbProvincia" CssClass="form-control chosen" data-placeholder="Seleccione una provincia" ClientIDMode="Static" onchange="cargarCiudades();" />
                            <asp:HiddenField runat="server" ID="hdnIDProvincia" ClientIDMode="Static" Value="0" />
                            <span id="spnProvincia" class="errorRequired" style="display: none;">Debe completar este campo</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label"><span class="f_req">*</span>&nbsp;Ciudad</label>
                        <div class="col-lg-6">
                            <asp:DropDownList runat="server" ID="cmbCiudad" CssClass="form-control chosen" data-placeholder="Seleccione una ciudad" ClientIDMode="Static" onchange="cargarZonas();" />
                            <asp:HiddenField runat="server" ID="hdnIDCiudad" ClientIDMode="Static" Value="0" />
                            <span id="spnCiudad" class="errorRequired" style="display: none;">Debe completar este campo</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Zona</label>
                        <div class="col-lg-6">
                            <asp:DropDownList runat="server" ID="cmbZona" CssClass="form-control chosen" data-placeholder="Seleccione una zona" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdnIDZona" ClientIDMode="Static" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnZona" ClientIDMode="Static" Value="" />
                            <%--<span id="spnZona" class="errorRequired" style="display: none;">Debe completar este campo</span>--%>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-lg-3 control-label">Estado Compra</label>
                        <div class="col-lg-6">
                            <asp:DropDownList runat="server" ID="cmbEstado" CssClass="form-control chosen" ClientIDMode="Static">
                                <asp:ListItem Text="Todos" Value="TODOS"></asp:ListItem>
                                <asp:ListItem Text="Nuevo" Value="NUEVO"></asp:ListItem>
                                <asp:ListItem Text="Rojo" Value="ROJO"></asp:ListItem>
                                <asp:ListItem Text="Naranja" Value="NARANJA"></asp:ListItem>
                                <asp:ListItem Text="Amarillo" Value="AMARILLO"></asp:ListItem>
                                <asp:ListItem Text="Verde" Value="VERDE"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField runat="server" ID="hdnEstado" ClientIDMode="Static" Value="" />
                        </div>
                    </div>
                    <div class="form-group" runat="server" id="divBuscar">
                        <div class="col-sm-8 col-sm-offset-3">
                            <asp:Button runat="server" ID="btnBuscar" CssClass="btn btn-success" OnClientClick="return validar();" OnClick="btnBuscar_Click" Text="Obtener Comercios" />
                        </div>
                    </div>
                </div>

                <div id="divPaso2" runat="server" visible="false">
                    <h3 class="heading" style="color: #428bca">2. Seleccione 1 o más comercios</h3>
                    <div class="form-group" id="divComercios" runat="server">
                        <label class="col-lg-3 control-label"><span class="f_req">*</span> Comercios</label>
                        <div class="col-lg-9">
                            <asp:DropDownList runat="server" ID="searchable" multiple="multiple" ClientIDMode="Static" DataTextField="Nombre" DataValueField="ID" />
                            <br />
                            <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                        </div>
                         
                          
                    </div>
                    <div class="form-group" id="divPaso3" runat="server">
                        <div class="alert alert-danger alert-dismissable" id="pnlError" runat="server" visible="false">
                            <asp:Literal runat="server" ID="litError"></asp:Literal>
                        </div>
                       
                        <div class="col-sm-8 col-sm-offset-3">
                            <a href="javascript:buscar();" id="btnMap" class="btn">Ver mapa</a>   
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><span class="f_req"></span>&nbsp;Insertar mapa en PDF?</label>
                            <div class="col-lg-6">
                                <asp:CheckBox ID="insertMap" runat="server" CssClass="form-control" />
                            </div>
                        </div>


                         <div class="col-sm-8 col-sm-offset-3">                            
                              <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar Planillas</button>
                             <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display: none" />
                            <a href="" download="Planilla" id="btnDescargar" style="display: none;">Descargar</a>
                        </div>
                    </div>
                </div>



                <%--OLD MAP INSERT--%>
                    <label class="col-lg-3 control-label" style="display: none;"><span class="f_req">*</span> Ingrese el mapa</label> 
                        <div class="col-lg-9" style="display: none;">
                           <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <ajaxToolkit:AsyncFileUpload runat="server" ID="fu" CssClass="form-control" PersistFile="true"
                                            ThrobberID="throbber" OnClientUploadComplete="UploadCompleted" OnClientUploadError="UploadError"
                                            OnUploadedComplete="subirMapa" />

                                        <asp:Label runat="server" ID="throbber" Style="display: none;">
                                            <img alt="" src="../../img/ajax_loader.gif" />
                                        </asp:Label>                                                             


                                    </div>
                                </div>
                            </div>
                        </div>


            </form>
        </div>
    </div>



    <!--mapa-->
                      


   <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle"></h4>                 
                    <span id="litResultados"></span>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
      <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/html2canvas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/verificaciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

      <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>