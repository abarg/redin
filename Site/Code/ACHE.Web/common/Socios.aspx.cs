﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_Socios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();

            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
                btnMasAccciones.Visible = false;
                divFranquicias.Visible = false;
            }
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                hdnIDMarca.Value = usu.IDMarca.ToString();
                btnMasAccciones.Visible = false;
            }
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                hdnIDMultimarca.Value = usu.IDMultimarca.ToString();
                btnMasAccciones.Visible = false;
            }
        }
    }

    private void cargarCombos()
    {
        try{

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                bMultimarca bMultimarca = new bMultimarca();
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                listMarcas = bMultimarca.getMarcas(usu.IDMultimarca);
                divFranquicias.Visible = false;
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();
                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();
                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            }             
            else {
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataSource = listFranquicias;
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();

                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

            }

            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null )
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        int idMarca = 0; 
        int idFranquicia = 0;
        int idMultimarca = 0;

        try {

            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                idMarca = usu.IDMarca; 
            }
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }

            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                idMultimarca = usu.IDMultimarca;
            }

            if (idFranquicia > 0 || idMarca > 0 || HttpContext.Current.Session["CurrentUser"] != null || idMultimarca > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var result =dbContext.SociosFullView
                            .OrderBy(x => x.Apellido)
                            .Select(x => new SociosViewModel()
                            {
                                IDSocio = x.IDSocio,
                                Tarjeta = (idMarca > 0) ? (x.NumeroTarjeta) : x.NumeroTarjeta,
                                Marca = x.Marca,
                                Franquicia = x.Franquicia,
                                Apellido = x.Apellido + ", " + x.Nombre,
                                Email = x.Email,
                                Sexo = x.Sexo,
                                NroDocumento = x.TipoDocumento + " " + x.NroDocumento,
                                Telefono = x.Telefono,
                                Celular = x.Celular,
                                EmpresaCelular = x.EmpresaCelular,
                                Observaciones = x.Observaciones,
                                NumeroTarjetaSube = x.NumeroTarjetaSube,
                                NumeroTarjetaMonedero = x.NumeroTarjetaMonedero,
                                NumeroTarjetaTransporte = x.NumeroTarjetaTransporte,
                                PatenteCoche = x.PatenteCoche,
                                IDMarca = x.IDMarca,
                                IDFranquicia = x.IDFranquicia ?? 0,
                                NroCuenta = x.NroCuenta,
                                Puntos = x.PuntosTotales,
                                Credito = x.Credito,
                                Giftcard = x.Giftcard,
                                POS = x.Credito + x.Giftcard,
                                Total = x.Credito + x.Giftcard,
                                Ciudad = x.Ciudad
                            }).AsQueryable();

                    if (idMultimarca > 0) {
                        var multimarcas = dbContext.MarcasAsociadas.Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                        if (multimarcas.Count > 0)
                            result = result.Where(x => multimarcas.Contains(x.IDMarca));
                        else
                            return null;
                    }

                    if(idFranquicia > 0)
                    {
                       result = result.Where(x => x.IDFranquicia == idFranquicia);
                    }
                    if(idMarca > 0)
                    {
                        result = result.Where(x => x.IDMarca == idMarca);
                    }

                    return result.ToDataSourceResult(take, skip, sort, filter);
                }
            }
            else
                return null;
            }
        catch(Exception e) {

            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;

        }
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                bSocio bSocio = new bSocio();
                bSocio.deleteSocio(id);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }
    [WebMethod(true)]
    public static string ExportarTr(int idSocio)
    {

        string fileName = "SociosTr";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    List<TransaccionesViewModel> info = new List<TransaccionesViewModel>();
                    if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
                        info = GetTrBySocio(idSocio, null);
                    else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                        info = GetTrBySocioMarca(idSocio, null);


                    dt = info.Select(x => new
                    {
                        Fecha = x.Fecha,
                        //      FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        //    Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.Comercio,

                        Marca = x.Marca,
                        Tarjeta = x.Tarjeta,
                        NroEstablecimiento = x.NroEstablecimiento,


                        //     Socio = x.Socio,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.Puntos
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }


    [WebMethod(true)]
    public static string Exportar(string apellido, string documento, string email, string cuenta, string tarjeta, int idMarca,int idFranquicia)
    {

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        int idMarcausu = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarcausu = usu.IDMarca;
        }
        
        int idMultimarca = 0;
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            idMultimarca = usu.IDMultimarca;
        }

        string fileName = "Socios";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || idMultimarca > 0)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.CommandTimeout = 1000;
                    var info = dbContext.SociosFullView.Where(x => (idMarca == 0 || x.IDMarca == idMarca) && (idFranquicia == 0 || x.IDFranquicia == idFranquicia)).OrderBy(x => x.Apellido).AsEnumerable();
                    if (apellido != "")
                        info = info.Where(x => x.Apellido.ToLower().Contains(apellido.ToLower()) || x.Nombre.ToLower().Contains(apellido.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumento != null && x.NroDocumento.ToLower().Contains(documento.ToLower()));
                    if (email != "")
                        info = info.Where(x => x.Email != null && x.Email.ToLower().Contains(email.ToLower()));
                    if (tarjeta != "")
                        info = info.Where(x => x.NumeroTarjeta != null && x.NumeroTarjeta.ToLower().Contains(tarjeta.ToLower()));
                    if (idMultimarca > 0) {
                        var multimarcas = dbContext.MarcasAsociadas.Include("Marcas").Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                        if (multimarcas.Count > 0)
                            info = info.Where(x => multimarcas.Contains(x.IDMarca));
                        else
                            info = null;
                    }

                   dt = info.Select(x => new
                    {
                        Franquicia = x.Franquicia,
                        Marca = x.Marca,
                        IDMarca = x.IDMarca,
                        Tarjeta = (idMarcausu > 0) ? (x.NumeroTarjeta) : x.NumeroTarjeta,
                        Apellido = x.Apellido,
                        Nombre = x.Nombre,
                        Email = x.Email,
                        FechaNacimiento = x.FechaNacimiento.HasValue ? x.FechaNacimiento.Value.ToShortDateString() : "",
                        Sexo = x.Sexo ?? "",
                        TipoDocumento = x.TipoDocumento ?? "",
                        NroDocumento = x.NroDocumento ?? "",
                        Telefono = x.Telefono ?? "",
                        Celular = x.Celular ?? "",
                        EmpresaCelular = x.EmpresaCelular ?? "",
                        Profesion=x.Profesiones ?? "", 
                        FechaAlta = x.FechaAlta.HasValue ? x.FechaAlta.Value.ToShortDateString() : "",
                        Observaciones = x.Observaciones ?? "",
                        NumeroSUBE = x.NumeroTarjetaSube ?? "",
                        NumeroMonedero = x.NumeroTarjetaMonedero ?? "",
                        NumeroTransporte = x.NumeroTarjetaTransporte ?? "",
                        Patente = x.PatenteCoche ?? "",
                        NroCuenta = x.NroCuenta ?? "",
                        POS = x.PuntosTotales,
                        Pais = x.Pais ?? "",
                        Provincia = x.Provincia ?? "",
                        Puntos = x.PuntosTotales,
                        PesosPuntos = x.Credito + x.Giftcard,
                        Ciudad = x.Ciudad ?? "",
                        Domicilio = x.Domicilio ?? "",
                        PisoDepto = x.PisoDepto ?? "",
                        CodigoPostal = x.CodigoPostal ?? "",
                        TelefonoDom = x.TelefonoDom ?? "",
                        Fax = x.Fax ?? "",
                        CostoTransaccionalConDescuento = x.CostoTransaccionalConDescuento.ToString(),
                        CostoTransaccionalSoloPuntos = x.CostoTransaccionalSoloPuntos.ToString(),
                        CostoTransaccionalCanje = x.CostoTransaccionalCanje.ToString(),
                        CostoSeguro = x.CostoSeguro.ToString(),
                        CostoPlusin = x.CostoPlusin.ToString(),
                        CostoSMS = x.CostoSMS.ToString(),

                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            return GetTrBySocio(id, null);
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            return GetTrBySocioMarca(id, null);

        return null;

    }
    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesView.Where(x => x.IDSocio.HasValue && x.IDSocio == id)
                    .OrderBy(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel()
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        Marca = x.Marca,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,

                    });

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }
    private static List<TransaccionesViewModel> GetTrBySocioMarca(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

            int idMarca = usu.IDMarca;
            var marca = usu.Marca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesMarcasView.Where(x => x.IDSocio.HasValue && x.IDSocio == id
                     && x.IDMarca == idMarca && x.ImporteOriginal > 1)
                     .OrderBy(x => x.FechaTransaccion).Select(x => new
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Marca = marca,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0,
                         ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0
                     });


                if (max.HasValue)
                    result = result.OrderByDescending(x => x.FechaTransaccion).Take(max.Value);

                list = result.Select(x => new TransaccionesViewModel
                {
                    Fecha = x.Fecha,
                    FechaTransaccion = x.FechaTransaccion,
                    Hora = x.Hora,
                    Operacion = x.Operacion,
                    Origen = x.Origen,
                    SDS = x.SDS,
                    Comercio = x.Comercio,
                    NroEstablecimiento = x.NroEstablecimiento,
                    //POSTerminal = x.POSTerminal,
                    Marca = marca,
                    Tarjeta = x.Tarjeta,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal,
                    ImporteAhorro = x.ImporteAhorro
                }).ToList();
            }
        }
        return list;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> marcasByFranquicias(int idFranquicia) {
        List<ComboViewModel> listMarcas = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {

            listMarcas = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia).Select(x => new ComboViewModel { ID = x.IDMarca.ToString() , Nombre = x.Nombre }).OrderBy(x=>x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listMarcas.Insert(0, todas);

        }
        return listMarcas;       
    }

}