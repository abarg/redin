﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;
using System.Collections.Specialized;

public partial class common_keywordse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            divTarjetasDesde.Visible = false;
            divTarjetasHasta.Visible = false;
            divStock.Visible = false;
            divActivo.Visible = false;
            divEstado.Visible = false;
            litXtarj.Visible = false;
            //ddlEstado.Enabled = false;
        }

        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");
            CargarCombos();

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];


                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    private void CargarCombos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            ddlComercio.DataSource = Common.LoadComercios();
            ddlComercio.DataBind();
        }
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                WebMarcasUser marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                ddlComercio.DataSource = dbContext.Comercios.Where(x => x.IDMarca == marca.IDMarca)
                    .Select(x => new Combo2ViewModel()
                        {
                            ID = x.IDComercio,
                            Nombre = x.NombreFantasia
                        }).OrderBy(x => x.Nombre).ToList();
                ddlComercio.DataBind();
            }
        }
    }

    //[WebMethod(true)]
    //public static bool guardar(int id, string codigo, int idComercio, string fechaDesde, string fechaHasta, string respuesta, bool activo,
    //    string respuestaSinStock, string respuestaVencida, string tarjetasDesde, string tarjetasHasta, string estado)
    //{
    //    bool send = true;
    //    bool enviarMailConfirmacionKeyword = false;
    //    try
    //    {
    //        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //        {
    //            using (var dbContext = new ACHEEntities())
    //            {
    //                var aux = dbContext.Keywords.FirstOrDefault(s => s.Codigo == codigo);
    //                if (aux != null && aux.IDKeyword != id)
    //                    throw new Exception("El código ya se encuentra disponible");
    //                string asunto = "";
    //                string mensaje = "";
    //                Keywords entity;
    //                if (id > 0)
    //                {
    //                    entity = dbContext.Keywords.Where(x => x.IDKeyword == id).FirstOrDefault();
    //                    if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //                    {
    //                        WebMarcasUser marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
    //                        asunto = "Modificacion Keyword";
    //                        mensaje = "Se ha modificado la keyword código '" + codigo + "' id: " + id + " Marca: " + marca.Marca;
    //                    }
    //                    if (estado == "Confirmado" && entity.Estado == "Pendiente" && entity.IDComercio == idComercio && entity.Comercios.IDMarca.HasValue)
    //                    {
    //                        enviarMailConfirmacionKeyword = true;
    //                    }
    //                }
    //                else
    //                {
    //                    if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //                    {
    //                        WebMarcasUser marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
    //                        activo = true;
    //                        asunto = "Alta Keyword";
    //                        mensaje = "Se ha creado una keyword con código '" + codigo + "' id: " + id + " Marca: " + marca.Marca;
    //                    }
    //                    entity = new Keywords();
    //                }

    //                if (tarjetasDesde != string.Empty)
    //                {
    //                    if (!dbContext.Tarjetas.Any(x => x.Numero == tarjetasDesde && x.TipoTarjeta == "C"))
    //                        throw new Exception("No existen tarjetas destinadas a CuponIN  en el rango " + tarjetasDesde);
    //                }
    //                if (tarjetasHasta != string.Empty)
    //                {
    //                    if (!dbContext.Tarjetas.Any(x => x.Numero == tarjetasHasta && x.TipoTarjeta == "C"))
    //                        throw new Exception("No existen tarjetas destinadas a CuponIN en el rango " + tarjetasHasta);
    //                }

    //                entity.FechaInicio = DateTime.Parse(fechaDesde);
    //                if (fechaHasta != "")
    //                    entity.FechaFin = DateTime.Parse(fechaHasta);
    //                else
    //                    entity.FechaFin = null;
    //                entity.Codigo = codigo;
    //                //entity.Stock = stock;                    
    //                entity.IDComercio = idComercio;
    //                entity.Respuesta = respuesta;
    //                entity.MensajeSinStock = respuestaSinStock;
    //                entity.MensajeVencida = respuestaVencida;
    //                entity.NumeroDesde = tarjetasDesde;
    //                entity.NumeroHasta = tarjetasHasta;
    //                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //                {
    //                    entity.Activo = entity.Activo;
    //                    entity.Estado = "Pendiente";
    //                }
    //                else
    //                {
    //                    entity.Estado = estado;
    //                    entity.Activo = activo;
    //                }

    //                if (id > 0)
    //                    dbContext.SaveChanges();
    //                else
    //                {
    //                    dbContext.Keywords.Add(entity);
    //                    dbContext.SaveChanges();
    //                }

    //                ListDictionary replacements = new ListDictionary();
    //                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //                {
    //                    replacements.Add("<MENSAJE>", mensaje);
    //                    send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, "backoffice@redin.com.ar", "RedIN :: " + asunto);
    //                }
    //                if (enviarMailConfirmacionKeyword)
    //                {

    //                    replacements.Add("<KEYWORD>", entity.Codigo);

    //                    var mail = entity.comercios.Marcas.EmailAlertas;
    //                    if (mail != "")
    //                        send = EmailHelper.SendMessage(EmailTemplate.ConfirmacionKeyword, replacements, mail, "RedIN :: Confirmación de keyword");
    //                }

    //            }
    //        }
    //        return send;
    //    }
    //    catch (Exception ex)
    //    {
    //        var msg = ex.Message + ConfigurationManager.AppSettings["Email.To"];
    //        throw ex;
    //    }
    //}

    private void CargarInfo(int id)
    {


        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Keywords.Where(x => x.IDKeyword == id).FirstOrDefault();
            if (entity != null)
            {
                txtFechaDesde.Text = entity.FechaInicio.ToShortDateString();
                if (entity.FechaFin.HasValue)
                    txtFechaHasta.Text = entity.FechaFin.Value.ToShortDateString();
                txtCodigo.Text = entity.Codigo;
                if (!string.IsNullOrEmpty(entity.NumeroDesde) && !string.IsNullOrEmpty(entity.NumeroHasta))
                {
                    string sql = "select count(IDTarjeta) from tarjetas where numero>='" + entity.NumeroDesde + "' and numero<='" + entity.NumeroHasta + "' and tipotarjeta='C'";
                    var total = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();
                    sql = "select count(IDTarjeta) from tarjetas where numero>='" + entity.NumeroDesde + "' and numero<='" + entity.NumeroHasta + "' and fechabaja is null and tipotarjeta='C'";
                    var usadas = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();

                    if (usadas != null)
                    {
                        txtStock.Text = usadas.ToString() + "/" + total.ToString();
                    }
                }

                chkActivo.Checked = entity.Activo;
                ddlEstado.SelectedValue = entity.Estado.ToLower() == "confirmado" ? "Confirmado" : "Pendiente";
                ddlComercio.SelectedValue = entity.IDComercio.ToString();
                txtRespuesta.Text = entity.Respuesta;
                txtRespuestaSinStock.Text = entity.MensajeSinStock;
                txtRespuestaVencido.Text = entity.MensajeVencida;
                txtTarjetasDesde.Text = entity.NumeroDesde;
                txtTarjetasHasta.Text = entity.NumeroHasta;
            }
        }
    }
}