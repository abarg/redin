﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_TransferirPuntos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack) {
        //    if (HttpContext.Current.Session["CurrentUser"] != null)
        //        cargarSocios();
        //}
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    [System.Web.Services.WebMethod(true)]
    public static SociosViewModel buscarDatosSocio(string DNI) {
        var html = string.Empty;
        Socios result;
        using (var dbContext = new ACHEEntities()) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = HttpContext.Current.Session["CurrentMarcasUser"] != null ? (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"] : null;
            //var result = dbContext.Socios.Where(x => x.NroDocumento == DNI).FirstOrDefault();
                if (DNI.Length < 10) {
                    if (usuMarca != null) {
                        if (dbContext.Tarjetas.Include("Socios").Include("Marcas").Any(x => x.Socios.NroDocumento == DNI && x.IDMarca == usuMarca.IDMarca && x.Marcas.TipoCatalogo == "C")) {
                            var puntos = dbContext.Tarjetas.Include("Socios").Any(x => x.Socios.NroDocumento == DNI && x.IDMarca == usuMarca.IDMarca) ? dbContext.Tarjetas.Include("Socios").Where(x => x.Socios.NroDocumento == DNI && x.IDMarca == usuMarca.IDMarca).Sum(x => x.PuntosTotales) : 0;
                            result = dbContext.Tarjetas.Include("Socios").Include("Marcas").Where(x => x.Socios.NroDocumento == DNI && x.IDMarca == usuMarca.IDMarca && x.Marcas.TipoCatalogo == "C").FirstOrDefault().Socios;
                            //result = dbContext.Socios.Include("Tarjetas").Where(x => x.NroDocumento == DNI).FirstOrDefault();
                            SociosViewModel socio = new SociosViewModel();
                            socio.Apellido = result.Apellido;
                            socio.Nombre = result.Nombre;
                            socio.Puntos = puntos;
                            return socio;
                        }
                    }
                    else if (dbContext.Socios.Any(x => x.NroDocumento == DNI) && HttpContext.Current.Session["CurrentUser"] != null) {
                        var puntos = dbContext.Tarjetas.Include("Socios").Any(x => x.Socios.NroDocumento == DNI)? dbContext.Tarjetas.Include("Socios").Where(x => x.Socios.NroDocumento == DNI).Sum(x => x.PuntosTotales) : 0;
                            result = dbContext.Socios.Where(x => x.NroDocumento == DNI).FirstOrDefault();
                            SociosViewModel socio = new SociosViewModel();
                            socio.Apellido = result.Apellido;
                            socio.Nombre = result.Nombre;
                            socio.Puntos = puntos;
                            return socio;                        
                    }
                }
                else if (dbContext.Tarjetas.Any(x => x.Numero == DNI && x.Estado == "A")) {
                    var aux = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == DNI).FirstOrDefault();
                    if (usuMarca != null) {
                        if (dbContext.Tarjetas.Include("Socios").Include("Marcas").Any(x => x.IDMarca == usuMarca.IDMarca && x.Marcas.TipoCatalogo == "C" && aux.Marcas.TipoCatalogo == "C")) {
                            SociosViewModel socio = new SociosViewModel();
                            socio.Apellido = aux.Socios.Apellido;
                            socio.Nombre = aux.Socios.Nombre;
                            socio.Puntos = aux.PuntosTotales;
                            return socio;
                        }
                        else {
                            return null;
                        }
                    }
                    else if (aux.IDSocio != null) {
                        SociosViewModel socio = new SociosViewModel();
                        socio.Apellido = aux.Socios.Apellido;
                        socio.Nombre = aux.Socios.Nombre;
                        socio.Puntos = aux.PuntosTotales;
                        return socio;
                    }
                }                
            }
         return null;
        }
    } 


    [System.Web.Services.WebMethod(true)]
    public static string generarTabla(string DNI) {
        var html = string.Empty;
        int n = 0;
        //DataHtml aux = new DataHtml();
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = HttpContext.Current.Session["CurrentMarcasUser"] != null ? (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"] : null;
            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Tarjetas.Include("Socios").Include("Marcas")
                    .Where(x => x.Socios.NroDocumento == DNI && x.Estado == "A").ToList();
                if (usuMarca != null)
                    list = list.Where(x => x.Socios.NroDocumento == DNI && x.Marcas.TipoCatalogo == "C").ToList();
                if (list.Count() > 0) {
                    foreach (var item in list) {
                        n++;
                        html += "<tr class=\"selectTarjetas\"  id=\"%\">";
                        html += "<td> <input id=\"chkTrans" + n + "\" class=\"chckbox selectTarjetas\"' type=\"checkbox\"/> </td>";
                        html += "<td> <input id=\"nroTarjeta" + n + "\" class='form-control selectTarjetas' value='" + item.Numero + "' readonly/></td> ";
                        html += "<td> <input id=\"puntosDisp" + n + "\" class='form-control selectTarjetas' value='" + item.PuntosTotales + "' readonly/></td> ";
                        html += "<td> <input class='form-control selectTarjetas' id=\"ptosATrans" + n + "\" type=\"number\" min='1' max='" + item.PuntosTotales + "'/> </td> ";
                        html += "</tr>";
                        //aux.empty = false;
                        //aux.html = html;
                    }
                }
                else if (dbContext.Tarjetas.Any(x => x.Numero == DNI && x.Estado == "A")) {                                          
                    var tarjeta = dbContext.Tarjetas.Include("Marcas").Where(x => x.Numero == DNI).FirstOrDefault();
                        if (tarjeta.Numero == null || (usuMarca != null && tarjeta.Marcas.TipoCatalogo == "A")) {                            
                            //html += "<tr><td colspan='4'>No hay tarjetas ni socios con ese Nro. Documento ó Nro.Tarjeta</td></tr>";
                            //aux.empty = true;
                            //aux.html = html;
                            return null;
                        }
                        else{
                          n++;
                        html += "<tr class=\"selectTarjetas\"  id=\"%\">";
                        html += "<td> <input id=\"chkTrans" + n + "\"class='chckbox selectTarjetas' type=\"checkbox\"/> </td>";
                        html += "<td> <input id=\"nroTarjeta" + n + "\" class='form-control selectTarjetas' value='" + tarjeta.Numero + "' readonly/></td> ";
                        html += "<td> <input id=\"puntosDisp" + n + "\" class='form-control selectTarjetas' value='" + tarjeta.PuntosTotales + "' readonly/></td> ";
                        html += "<td> <input class='form-control selectTarjetas' id=\"ptosATrans" + n + "\" type=\"number\" min='1' max='" + tarjeta.PuntosTotales + "'/> </td> ";
                        html += "</tr>";                      
                        }
                }
                else {
                    //html += "<tr><td colspan='4'>No hay tarjetas ni socios con ese Nro. Documento ó Nro.Tarjeta</td></tr>";
                    return null;
                }
            }
        }
        //aux.empty = true;
        //aux.html = html;
        return html;
    }


    //protected void cargarTarjetas() {

    //    //ddlTrTarjeta.DataSource = buscarTarjetas(); //aca va el nro de socio para que encuentre las tarjetas a las que va a transferir los ptos.
    //    ddlTrTarjeta.DataTextField = "Numero";
    //    ddlTrTarjeta.DataValueField = "ID";
    //    ddlTrTarjeta.DataBind();

    //}

    [System.Web.Services.WebMethod(true)]
    public static List<Combo2ViewModel> buscarTarjetas(string nroSocio) {
        try {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usuMarca = HttpContext.Current.Session["CurrentMarcasUser"] != null ? (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"] : null;
                if (nroSocio != "" && nroSocio != null){
                    using (var dbContext = new ACHEEntities()) {
                        if (dbContext.Tarjetas.Include("Marcas").Any(x => x.Numero == nroSocio && x.Estado == "A" && !x.FechaBaja.HasValue)) {
                           var aux = dbContext.Tarjetas.Where(x => x.Numero == nroSocio && x.Estado == "A" && !x.FechaBaja.HasValue).ToList();
                           if (usuMarca != null) {
                               aux = aux.Where(x => x.Numero == nroSocio && x.Marcas.TipoCatalogo == "C").Distinct().ToList();
                           }
                            if(aux.Count > 0){
                                list =aux.OrderBy(x => x.Numero)
                            .Select(x => new Combo2ViewModel() { ID = x.IDTarjeta, Nombre = x.Numero })
                            .Distinct().OrderBy(x => x.Nombre).ToList();
                            }
                        }
                        else if (dbContext.Tarjetas.Include("Socios").Any(x => x.Socios.NroDocumento == nroSocio)) {
                            var aux2 = dbContext.Tarjetas.Include("Marcas").Include("Socios").Where(x => x.Socios.NroDocumento == nroSocio && x.Estado == "A" && !x.FechaBaja.HasValue).ToList();
                            if (usuMarca != null)
                                aux2 = aux2.Where(x => x.Socios.NroDocumento == nroSocio && x.Marcas.TipoCatalogo == "C").Distinct().ToList();
                            if(aux2.Count > 0){
                                list=aux2.OrderBy(x => x.Numero)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTarjeta, Nombre = x.Numero })
                                .Distinct().OrderBy(x => x.Nombre).ToList();
                            }
                        }
                        else list = null;
                    }
                }
                else list = null;
            }
            return list;
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void transferirPuntos(string nroTarjetaOrigen, string IDTarjetaDestino, int puntosATrans) {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            string ptoVenta = "";
            string nroComprobante = "";
            string tipoComrpobante = "";
           // TarjetasViewModel tarjetaOrigen
            var usuAdmin = (Usuarios)HttpContext.Current.Session["CurrentUser"];
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            using (var dbContext = new ACHEEntities()) {
                int idTarjetaDest = int.Parse(IDTarjetaDestino);
                var tarjetaDestino = dbContext.Tarjetas.Where(x => x.IDTarjeta == idTarjetaDest).FirstOrDefault();
                var tarjetaOrigen = dbContext.Tarjetas.Where(x => x.Numero == nroTarjetaOrigen).FirstOrDefault();
                 //if (tarjetaOrigen != null)
                 //   tarjetaOrigen.PuntosTotales -= puntosATrans;
                 //if (tarjetaDestino != null)
                 //   tarjetaDestino.PuntosTotales += puntosATrans;
                int idComer = int.Parse(ConfigurationManager.AppSettings["Transferencia.IDComercio"]);
                if (idComer != null && dbContext.Comercios.Any(x => x.IDComercio == idComer)) {
                    if (tarjetaOrigen != null) {
                        //tarjetaOrigen.PuntosTotales -= puntosATrans;
                        string descOrigen = "Transferencia desde Tarjeta Nro: " + tarjetaOrigen.Numero;
                        if (usuAdmin != null)
                            ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idComer, "Web", "", descOrigen, puntosATrans, "", "", "", "", tarjetaOrigen.Numero, "", "Anulacion", "000000000000", "1100", ptoVenta, nroComprobante, tipoComrpobante, "M-" + usuAdmin.Usuario);
                        else
                            ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idComer, "Web", "", descOrigen, puntosATrans, "", "", "", "", tarjetaOrigen.Numero, "", "Anulacion", "000000000000", "1100", ptoVenta, nroComprobante, tipoComrpobante, "M-" + usuMarca.Usuario);
                    }
                    if (tarjetaDestino != null) {
                        //tarjetaDestino.PuntosTotales += puntosATrans;
                        string descDestino = "Transferencia desde Tarjeta Nro: " + tarjetaDestino.Numero;
                        if (usuAdmin != null)
                            ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idComer, "Web", "", descDestino, puntosATrans, "", "", "", "", tarjetaDestino.Numero, "", "Venta", "000000000000", "1100", ptoVenta, nroComprobante, tipoComrpobante, "M-" + usuAdmin.Usuario);
                        else
                            ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idComer, "Web", "", descDestino, puntosATrans, "", "", "", "", tarjetaDestino.Numero, "", "Venta", "000000000000", "1100", ptoVenta, nroComprobante, tipoComrpobante, "M-" + usuMarca.Usuario);
                    }
                }
                else throw new Exception("No existe el comercio");
                //dbContext.SaveChanges();
            }
        }
    }

    //protected void buscar2(object sender, EventArgs e) {
    //    try {
    //        List<Combo2ViewModel> list = new List<Combo2ViewModel>();
    //        if (HttpContext.Current.Session["CurrentUser"] != null) {
    //            //string nroSocio=txtNroSocio2.Text;
    //            using (var dbContext = new ACHEEntities()) {
    //                if(dbContext.Tarjetas.Any(x => x.Numero == "1" && x.Estado == "A" && !x.FechaBaja.HasValue)){
    //                list = dbContext.Tarjetas.Where(x => x.Numero == "1" && x.Estado == "A" && !x.FechaBaja.HasValue)
    //                    .OrderBy(x => x.Numero)
    //                    .Select(x => new Combo2ViewModel() { ID = x.IDTarjeta, Nombre = x.Numero })
    //                    .Distinct().OrderBy(x => x.Nombre).ToList();
    //                }
    //                else if (dbContext.Tarjetas.Include("Socios").Any(x => x.Socios.NroDocumento == "12780557")) {
    //                    list = dbContext.Tarjetas.Include("Socios").Where(x => x.Socios.NroDocumento == "12780557" && x.Estado == "A" && !x.FechaBaja.HasValue)
    //                    .OrderBy(x => x.Numero)
    //                    .Select(x => new Combo2ViewModel() { ID = x.IDTarjeta, Nombre = x.Numero })
    //                    .Distinct().OrderBy(x => x.Nombre).ToList();
    //                }
    //                else list= null;
    //            }
    //        }
    //         //return list;
    //    }
    //    catch (Exception ex) {
    //        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
    //        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
    //        throw ex;
    //    }           
    //}  
}