﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.IO;
using ACHE.Business;

public partial class common_Dashboard_actividades : System.Web.UI.Page
{
    static int idFranquicia;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnIDFranquiciasAdmin.Value = "0";
            hdnIDFranquicias.Value = "";

            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                Franquicias franquicia = new Franquicias();
                cargarCombos(listFranquicias);

                if (!String.IsNullOrEmpty(Request.QueryString["IDFranquicia"]))
                {
                    franquicia = listFranquicias[Convert.ToInt32(Request.QueryString["IDFranquicia"]) - 1];
                    hdnIDFranquiciasAdmin.Value = Request.QueryString["IDFranquicia"];
                    idFranquicia = Convert.ToInt32(Request.QueryString["IDFranquicia"]);
                    ddlFranquicias.SelectedValue = Request.QueryString["IDFranquicia"];

                }


            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                if (usu.Tipo == "B")
                    Response.Redirect("home.aspx");

                idFranquicia = usu.IDFranquicia;
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();

            }
            else
                Response.Redirect("/login.aspx");
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void cargarCombos(List<Franquicias> listFranquicias)
    {
        try
        {
            //bFranquicia bFranquicia = new bFranquicia();
            this.ddlFranquicias.DataSource = listFranquicias;// bFranquicia.getFranquicias();
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region Graficos

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTerminalesPorPOS()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TerminalesActivos " + idFranquicia, new object[] { }).ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasAsignadas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCantTransacciones()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasActivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasInactivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";


            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasEmitidas()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasEmitidas " + idFranquicia).ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerEstadoTerminales()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + "" + "','" + "" + "','" + "" + "'," + 0 + "," + idFranquicia, new object[] { }).ToList();

                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";

                    else
                        row.Estado = "Desconocido";
                }


                //var prevList = aux.ToList();
                int negro = aux.Count(x => x.Estado == "Negro");
                int naranja = aux.Count(x => x.Estado == "Naranja");
                int verde = aux.Count(x => x.Estado == "Verde");
                int rojo = aux.Count(x => x.Estado == "Rojo");
                int amarillo = aux.Count(x => x.Estado == "Amarillo");
                int azul = aux.Count(x => x.Estado == "Azul");
                int desconocido = aux.Count(x => x.Estado == "Desconocido");
                int noprobado = aux.Count(x => x.Estado == "Marron");

                list.Add(new Chart() { data = negro, label = "<a href=\"javascript:verDetalleTerminales('Negro');\">Baja (" + negro + ")</a>" });
                list.Add(new Chart() { data = naranja, label = "<a href=\"javascript:verDetalleTerminales('Naranja');\">Inválido (" + naranja + ")</a>" });
                list.Add(new Chart() { data = verde, label = "<a href=\"javascript:verDetalleTerminales('Verde');\">Activo Mayor a $1 (" + verde + ")</a>" });
                list.Add(new Chart() { data = rojo, label = "<a href=\"javascript:verDetalleTerminales('Rojo');\">No Reprogramado (" + rojo + ")</a>" });
                list.Add(new Chart() { data = amarillo, label = "<a href=\"javascript:verDetalleTerminales('Amarillo');\">Activo Menor a $1 (" + amarillo + ")</a>" });
                list.Add(new Chart() { data = azul, label = "<a href=\"javascript:verDetalleTerminales('Azul');\">Web (" + azul + ")</a>" });
                list.Add(new Chart() { data = desconocido, label = "<a href=\"javascript:verDetalleTerminales('Desconocido');\">Desconocido (" + desconocido + ")</a>" });
                list.Add(new Chart() { data = noprobado, label = "<a href=\"javascript:verDetalleTerminales('Marron');\">No Probado (" + noprobado + ")</a>" });
            }
        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Socios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Franq_TopSocios " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosPorBeneficio()
    {
        var html = string.Empty;


        using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableIntHtml>(@" SELECT Actividades.Actividad as uno, COUNT(ASN.IDActividad) AS dos, Actividades.IDActividad as tres
                                                                        FROM ActividadSocioNivel as ASN
                                                                        join actividades on ASN.IDActividad = Actividades.IDActividad
                                                                        GROUP BY Actividades.Actividad, Actividades.IDActividad ", new object[] { }).ToList();


            if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td> <a style='cursor: pointer;' onClick='verDetalle(" + detalle.tres + ")'> " + detalle.dos + " </a> </td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }



        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosPorSecretaria()
    {
        var html = string.Empty;


        using (var dbContext = new ACHEEntities())
        {
            var list = dbContext.Database.SqlQuery<TableIntHtml>(@" SELECT ASEC.Nombre as uno, count(ASN.IDActividad) as dos
		                                                            FROM ActividadSocioNivel as ASN
		                                                            join actividades as A on ASN.IDActividad = A.IDActividad
		                                                            join ActividadSubCategorias as ASUB on ASUB.IDSubCategoria =  A.IDSubCategoria
		                                                            join ActividadCategorias as ACAT on ACAT.IDCategoria = ASUB.IDCategoria
		                                                            join ActividadSecretariaCategoria as ASEC on ASEC.IDSecretaria = ACAT.IDSecretaria
		                                                            GROUP BY ASEC.Nombre  ", new object[] { }).ToList();


            if (list.Any())
            {
                foreach (var detalle in list)
                {
                    html += "<tr>";
                    html += "<td>" + detalle.uno + "</td>";
                    html += "<td>" + detalle.dos + "</td>";
                    html += "</tr>";
                }
            }
            else
                html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
        }



        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerBeneficiariosUbicacionSanMartin()
    {
        var html = string.Empty;
 
            using (var dbContext = new ACHEEntities())
            {
                var total = 0;
                var totalSociosAgrupadosPorUbicacion = dbContext.Database.SqlQuery<NullableChart>(@"SELECT DISTINCT(ASN.IDSocio), D.Ciudad as data FROM ActividadSocioNivel AS ASN
                                                                                                        JOIN Socios AS S ON ASN.IDSocio = S.IDSocio
                                                                                                        JOIN Domicilios AS D ON S.IDDomicilio = D.IDDomicilio
                                                                                                        ", new object[] { }).ToList();

            var totalBeneficiarios = totalSociosAgrupadosPorUbicacion.Count;

                if (totalSociosAgrupadosPorUbicacion.Any())
                {
                    foreach (var soc in totalSociosAgrupadosPorUbicacion)
                    {

                        if (soc.data == 4107 || soc.data > 27862 || soc.data > 16564 && soc.data < 16671)
                        {
                            total++;
                        }
                    }


                    html = total + "/" + totalBeneficiarios;
                }
            }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerCantidadTarjetas()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_CantidadTarjetasUnicas '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("");
                else
                    html = "0";
            }
        }

        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosMultipleActividad()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var totalSociosAgrupadosPorCantidadActividad = dbContext.Database.SqlQuery<int>("SELECT COUNT(*) count FROM ActividadSocioNivel GROUP BY IDSocio Having COUNT(*) > 1").Count();
                if (totalSociosAgrupadosPorCantidadActividad != 0) {
         

                    html = Math.Abs(totalSociosAgrupadosPorCantidadActividad).ToString("");

                }
                else
                    html = "0";
            }
        }

        return html;
    }


    #endregion

    #region Estadisticas Superiores

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalSociosPorSexo()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<Chart>(String.Format(@"select 
                                                                count(distinct IDSocio) as data, 'M' as label
                                                                from SociosView
                                                                where IDFranquicia = {0} AND sexo = 'M'
                                                                union all
                                                                select
                                                                count(distinct IDSocio) as data, 'F' as label
                                                                from SociosView
                                                                where IDFranquicia = {0}  AND sexo = 'F'
                                                                union all
                                                                select
                                                                count(distinct IDSocio) as data, 'I' as label
                                                                from SociosView
                                                                where IDFranquicia =  {0} AND(sexo = 'I' or sexo is null)", idFranquicia), new object[] { }).ToList();
                if (list.Any())
                {
                    html = "F: " + list[0].data + " - M: " + list[1].data + " <br/> I: " + list[2].data;// + list[3].data);
                    decimal total = list[0].data + list[1].data + list[2].data;// +list[3].data;
                    html += "," + total;
                }
                else
                    html = "Error";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalComisionMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                /*var total = 0;dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_TotalComisionMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else*/
                html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTRMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalActividades()
    {
        var html = string.Empty;

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {

  

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("SELECT COUNT(*) as data, 'Cant' as label  FROM Actividades", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicketMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                //var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_PromedioTicketMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                //if (total.Any())
                //    html = Math.Abs(total[0].data).ToString("N2");
                //else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerComisiones()
    {
        var html = string.Empty;
        var com1 = string.Empty;
        var com2 = string.Empty;
        var com3 = string.Empty;
        decimal tot = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                {
                    com1 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com1 = "0";

                total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCt','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                {
                    com2 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com2 = "0";

                total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TtCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                {
                    com3 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com3 = "0";

                //var entity = dbContext.Franquicias.Where(x => x.IDFranquicia == usu.IDFranquicia).FirstOrDefault();
                html = com1 + "_" + com2 + "_" + com3 + "_" + tot.ToString("N2") + "_" + (tot * 1.21M).ToString("N2");
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalEmails()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalEmails " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalCelulares()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>(@"select 
                                                                count(IDSocio) as data, 'Cant' as label
                                                                from SociosView
                                                                where Celular is not null AND Celular <> '' AND IDFranquicia = " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString();
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTarjetasActivas()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTarjetasActivas " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                    html = int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "") + " / " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    #endregion

    #region Otros

    //[WebMethod(true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static List<Chart> obtenerDetalleSociosPorActividad()
    //{
    //    var html = string.Empty;
   
    //        using (var dbContext = new ACHEEntities())
    //        {
    //            var list = dbContext.Database.SqlQuery<TableIntHtml>(@" SELECT Actividades.Actividad as uno, COUNT(ASN.IDActividad) AS dos
    //                                                                    FROM ActividadSocioNivel as ASN
    //                                                                    join actividades on ASN.IDActividad = Actividades.IDActividad
    //                                                                    GROUP BY Actividades.Actividad ", new object[] { }).ToList();

    //            if (list.Any())
    //            {
    //                foreach (var detalle in list)
    //                {
    //                    html += "<tr>";
    //                    html += "<td>" + detalle.uno + "</td>";
    //                    html += "<td>" + detalle.dos + "</td>";
    //                    html += "</tr>";
    //                }
    //            }
    //            else
    //                html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
    //        }

        
    //    return html;
    //}

    #endregion

    #region Tablas HTML

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<TarjetasImpresas> ObtenerTarjetasEmitidas()
    {
        List<TarjetasImpresas> result = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                result = dbContext.Database.SqlQuery<TarjetasImpresas>("exec Dashboard_Franq_TarjetasEmitidas_Listado " + idFranquicia).ToList();
                result = result.Select(x => new TarjetasImpresas()
                {
                    Marca = x.Marca,
                    Activas = x.Activas,
                    Inactivas = x.Total - x.Activas,
                    Total = x.Total
                }).ToList();
            }
        }
        return result;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string obtenerDineroDisponibleCredito()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_DineroDisponibleCredito " + idFranquicia).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string ExportarPorActividad(string idActividad)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                string fileName = "sociosPorActividad";
                string path = "/tmp/";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    string sql = @"select SociosView.Nombre, SociosView.Apellido, SociosView.Celular, SociosView.Email from SociosView
                                join ActividadSocioNivel as ASN on SociosView.IDSocio = ASN.IDSocio
                                where ASN.IDActividad = " + idActividad + ";";



                    var info = dbContext.Database.SqlQuery<DetalleSocios>(sql, new object[] { }).ToList();

                    if (info.Any())
                    {
                        dt = info.Select(x => new
                        {
                            Nombre = x.Nombre,
                            Apellido = x.Apellido,
                        }).ToList().ToDataTable();

                        if (dt.Rows.Count > 0)
                            generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                        else
                            throw new Exception("No se encuentran datos para los filtros seleccionados");
                        return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }


    [WebMethod(true)]
    public static List<DetalleSocios> obtenerDetallePorActividad(string idActividad)
    {

        List<DetalleSocios> list = new List<DetalleSocios>();
    
            using (var dbContext = new ACHEEntities())
            {

                string sql = @"select SociosView.Nombre, SociosView.Apellido, SociosView.Celular, SociosView.Email from SociosView
                                join ActividadSocioNivel as ASN on SociosView.IDSocio = ASN.IDSocio
                                where ASN.IDActividad = " + idActividad + ";";

                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "id", sql);


                try
                {
                    list = dbContext.Database.SqlQuery<DetalleSocios>(sql, new object[] { }).ToList();
                }

                catch (Exception ex)
                {
                    throw ex;
                }



             }


        return list;
    }

    public class DetalleSocios
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    #endregion

}