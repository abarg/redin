﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using System.Globalization;
using System.Text;

public partial class common_importarTransacciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                cargarCombos();
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    hdnIDFranquicias.Value = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault().IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos()
    {
        try
        {
            bFranquicia bFranquicia = new bFranquicia();
            this.ddlFranquicias.DataSource = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Importar(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;


        if (flpArchivo.HasFile)
        {
            if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    DataTable dt = TTransacciones();

                    WebMarcasUser usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream for extended ascii characters (ñ, á, é, etc)
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        int errores = 0;
                        int total = 0;
                        
                        #region Armo Datatable

                        for (int i = 0; i < rows.Length; i++)
                        {
                            //Split the tab separated fields (comma separated split commented)
                            if (rows.Length > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length > 9)
                                {
                                    #region Armo DataRow
                                    if (dr[1].Trim() != "")
                                    {
                                        total++;
                                        try
                                        {

                                            DataRow drNew = dt.NewRow();
                                            drNew[0] = "00000" + dr[0].ToString();//archivo
                                            drNew[1] = dr[1].ToString();//archivo
                                            drNew[2] = dr[2].ToString();//archivo
                                            drNew[3] = dr[3].ToString();//archivo
                                            drNew[4] = dr[4].ToString();//archivo

                                            drNew[5] = "0";
                                            drNew[6] = "0";
                                            drNew[7] = "0";

                                            drNew[8] = dr[5].ToString();//archivo

                                            drNew[9] = "0";
                                            drNew[10] = "0";
                                            drNew[11] = "0";

                                            drNew[12] = dr[6].ToString();//archivo

                                            drNew[13] = usu.IDUsuario;

                                            drNew[14] = dr[7].ToString();//archivo
                                            drNew[15] = dr[8].ToString();//archivo

                                            drNew[16] = 0.0;
                                            drNew[17] = "0";

                                            drNew[18] = dr[9].ToString();//archivo

                                            drNew[19] = 0;
                                            drNew[20] = 0;
                                            drNew[21] = 0;

                                            dt.Rows.Add(drNew);
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                            errores++;
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion

                        bool ok = true;

                        #region Importar datatable

                        if (dt.Rows.Count > 0)
                        {
                            EliminarTmp();
                            try
                            {
                                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                                {
                                    sqlConnection.Open();

                                    // Run the Bulk Insert
                                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                                    {
                                        //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                        bulkCopy.DestinationTableName = " [dbo].[TransaccionesTmp]";

                                        foreach (DataColumn col in dt.Columns)
                                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                        bulkCopy.WriteToServer(dt);
                                    }
                                }
                                ProcesarTmp();
                            }
                            catch (Exception ex)
                            {
                                ok = false;
                                ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                            }
                        }


                        if (ok)
                        {

                            int totalOk = dt.Rows.Count - errores;

                            if (errores > 0)
                            {
                                ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                            }

                            int cantErrores = 0;

                            if (cantErrores > 0)
                            {
                                totalOk = totalOk - cantErrores;
                                var msg = ". Se encontraron <a href='javascript:verErrores();'>" + cantErrores + " errores</a> en la importación.";

                                ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);
                            }
                            else
                                ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());

                        }

                    }

                    File.Delete(path);

                    #endregion
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }


    private DataTable TTransacciones()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("NumEst", typeof(string));
        dt.Columns.Add("NumTerminal", typeof(string));
        dt.Columns.Add("TipoMensaje", typeof(string));
        dt.Columns.Add("TipoTransaccion", typeof(string));
        dt.Columns.Add("FechaTransaccion", typeof(DateTime));

        dt.Columns.Add("NumCupon", typeof(string));
        dt.Columns.Add("NumReferencia", typeof(string));
        dt.Columns.Add("NumRefOriginal", typeof(string));

        dt.Columns.Add("NumTarjetaCliente", typeof(string));

        dt.Columns.Add("CodigoPremio", typeof(string));
        dt.Columns.Add("PuntosIngresados", typeof(string));
        dt.Columns.Add("PuntosDisponibles", typeof(string));

        dt.Columns.Add("NombreArchivo", typeof(string));

        dt.Columns.Add("IDUsuario", typeof(int));

        dt.Columns.Add("Origen", typeof(string));
        dt.Columns.Add("Operacion", typeof(string));

        dt.Columns.Add("ImporteAhorro", typeof(Decimal));
        dt.Columns.Add("Descripcion", typeof(string));

        dt.Columns.Add("Importe", typeof(Decimal));

        dt.Columns.Add("Descuento", typeof(int));
        dt.Columns.Add("PuntosAContabilizar", typeof(int));
        dt.Columns.Add("IDProceso", typeof(int));
        return dt;
    }
    
    private void EliminarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {
            dbContext.Database.ExecuteSqlCommand("DELETE TransaccionesTmp", new object[] { });
        }
    }

    private void ProcesarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                WebMarcasUser usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

                dbContext.Database.CommandTimeout = 2000;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarTransacciones @nombreArchivo = '" + flpArchivo.FileName + "', @idUsuario = " + usu.IDUsuario + "", new object[] { });

            }
        }
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}