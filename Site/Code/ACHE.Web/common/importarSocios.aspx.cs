﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using System.Globalization;
using System.Text;

public partial class common_importarSocios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                cargarCombos();
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    hdnIDFranquicias.Value = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault().IDFranquicia.ToString();
                    divFranquicia.Visible = false;
                }
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos()
    {
        try
        {
            bFranquicia bFranquicia = new bFranquicia();
            this.ddlFranquicias.DataSource = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Importar(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";

        if (flpArchivo.HasFile)
        {
            if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    //Obra Social	Apellido y nombres	Cuil	Parentesco	Tipo doc.	N° doc.	Fecha nac.	Sexo
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Affinity");
                    dt.Columns.Add("NroTarjeta");
                    dt.Columns.Add("Nombre");
                    dt.Columns.Add("Apellido");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("FechaNacimiento", typeof(DateTime));
                    dt.Columns.Add("Sexo");
                    dt.Columns.Add("TipoDocumento");
                    dt.Columns.Add("NroDocumento");
                    dt.Columns.Add("Telefono");
                    dt.Columns.Add("Celular");
                    dt.Columns.Add("Empresa");
                    dt.Columns.Add("Observaciones");
                    dt.Columns.Add("Pais");
                    dt.Columns.Add("Provincia");
                    dt.Columns.Add("Ciudad");
                    dt.Columns.Add("Domicilio");
                    dt.Columns.Add("CodigoPostal");
                    dt.Columns.Add("TelefonoDom");
                    dt.Columns.Add("Fax");
                    dt.Columns.Add("PisoDepto");
                    dt.Columns.Add("IDSocio", typeof(Int32));
                    dt.Columns.Add("IDDomicilio", typeof(Int32));
                    dt.Columns.Add("IDTarjeta", typeof(Int32));
                    dt.Columns.Add("IDProvincia", typeof(Int32));
                    dt.Columns.Add("Error");
                    dt.Columns.Add("IDCiudad");
                    dt.Columns.Add("Puntos", typeof(Int32));
                    dt.Columns.Add("latitud");
                    dt.Columns.Add("longitud");
                    dt.Columns.Add("nfc");

                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream for extended ascii characters (ñ, á, é, etc)
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                        int errores = 0;
                        int total = 0;

                        #region Armo Datatable
                        List<SociosTmp> tempSocios = new List<SociosTmp>();
                        var tempSocio = new SociosTmp();
                        for (int i = 0; i < rows.Length; i++)
                        {
                            tempSocio = new SociosTmp();
                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                if (dr.Length > 15)
                                {
                                    #region Armo DataRow
                                    if (dr[1].Trim() != "")
                                    {
                                        total++;
                                        try
                                        {

                                            DataRow drNew = dt.NewRow();
                                            if (dr[1].Trim().Length == 3)
                                                tempSocio.Affinity = "0" + dr[1].Trim();
                                            else
                                                tempSocio.Affinity = dr[1].Trim();
                                            tempSocio.NroTarjeta = dr[0].Trim();
                                            tempSocio.Nombre = dr[2].Trim();
                                            tempSocio.Apellido = dr[3].Trim();
                                            tempSocio.Email = dr[4].Trim();
                                            if (dr[5] != string.Empty)
                                                try
                                                {
                                                    tempSocio.FechaNacimiento = DateTime.Parse(dr[5].Trim());
                                                    if (DateTime.Parse(dr[5].Trim()) < DateTime.Parse("01/01/1900"))
                                                        tempSocio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                    else if (DateTime.Parse(dr[5].Trim()) > DateTime.Parse("01/01/2100"))
                                                        tempSocio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                }
                                                catch (Exception ex)
                                                {
                                                    tempSocio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                                }
                                            else
                                                tempSocio.FechaNacimiento = DateTime.Parse("01/01/1900");
                                            if (dr[6].Trim().ToLower().StartsWith("f"))
                                                tempSocio.Sexo = "F";
                                            else if (dr[6].Trim().ToLower().StartsWith("m"))
                                                tempSocio.Sexo = "M";
                                            else
                                                tempSocio.Sexo = "I";
                                            //drNew[6] = dr[6].Trim();

                                            if (dr[7].Trim().Length <= 3)
                                                tempSocio.TipoDocumento = dr[7].Trim();
                                            else
                                                tempSocio.TipoDocumento = "DNI";

                                            if (dr[8].Trim().Length <= 20)
                                                tempSocio.NroDocumento = dr[8].Trim();
                                            else
                                                tempSocio.NroDocumento = "";
                                            tempSocio.Telefono = dr[9].Trim();
                                            tempSocio.Celular = dr[10].Trim();
                                            tempSocio.Empresa = dr[11].Trim();
                                            tempSocio.Observaciones = dr[12].Trim();
                                            tempSocio.Pais = dr[13].Trim();
                                            tempSocio.Provincia = dr[14].Trim();
                                            tempSocio.Ciudad = dr[15].Trim();
                                            //drNew[16] = dr[16].Trim();
                                            if (dr[16].Trim().Length <= 100)//Domicilio
                                                tempSocio.Domicilio = dr[16].Trim();
                                            else
                                                tempSocio.Domicilio = dr[16].Trim().Substring(0, 100);

                                            if (dr.Length > 17)
                                            {
                                                if (dr[17].Trim().Length <= 10)//CP
                                                    tempSocio.CodigoPostal = dr[17].Trim();
                                                else
                                                    tempSocio.CodigoPostal = dr[17].Trim().Substring(0, 10);
                                            }
                                            if (dr.Length > 18)
                                            {
                                                if (dr[18].Trim().Length <= 50)//tel
                                                    tempSocio.TelefonoDom = dr[18].Trim();
                                                else
                                                    tempSocio.TelefonoDom = dr[18].Trim().Substring(0, 50);
                                            }
                                            if (dr.Length > 19)
                                            {
                                                if (dr[19].Trim().Length <= 50)//fax
                                                    tempSocio.Fax = dr[19].Trim();
                                                else
                                                    tempSocio.Fax = dr[19].Trim().Substring(0, 50);
                                            }

                                            if (dr.Length > 20)
                                            {

                                                if (dr[20].Trim().Length <= 10)
                                                    tempSocio.PisoDepto = dr[20].Trim();
                                                else
                                                    tempSocio.PisoDepto = "";
                                            }
                                            else
                                                tempSocio.PisoDepto = "";
                                            tempSocio.IDSocioTmp = 0;
                                            //drNew[22] = 0;
                                            //drNew[23] = 0;
                                            //drNew[24] = 0;
                                            //drNew[25] = "";
                                            //drNew[26] = "0";

                                            if (dr.Length > 21)
                                            {
                                                if (!string.IsNullOrEmpty(dr[21].Trim()))
                                                    tempSocio.Puntos= (int)decimal.Parse(dr[21].Trim());
                                                
                                            }

                                            if (dr.Length > 22)
                                            {
                                                if (!string.IsNullOrEmpty(dr[22].Trim()))
                                                {
                                                    tempSocio.latitud = dr[22].Trim(); // lat
                                                    tempSocio.longitud = dr[23].Trim(); // lng
                                                }
                                              
                                            }
                                            if (dr.Length > 24)
                                            {
                                                //if (!string.IsNullOrEmpty(dr[24].Trim()))
                                                //    tempSocio.nfc = dr[24].Trim(); // nfc
                                                
                                            }
                                            tempSocios.Add(tempSocio);
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                            errores++;
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion

                        bool ok = true;

                        #region Importar datatable

                        if (tempSocios.Count > 0)
                        {
                            EliminarTmp();
                            try
                            {
                                using (var dbContext = new ACHEEntities())
                                {
                                    dbContext.SociosTmp.AddRange(tempSocios);
                                    dbContext.SaveChanges();
                                }
                                //using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                                //{
                                //    sqlConnection.Open();

                                // Run the Bulk Insert

                                //using (var bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, SqlBulkCopyOptions.Default))
                                //{
                                //    //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                //    bulkCopy.DestinationTableName = " [dbo].[SociosTmp]";

                                //    foreach (DataColumn col in dt.Columns)
                                //        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                //    bulkCopy.WriteToServer(dt);
                                //}
                                //    sqlConnection.Close();
                                //}
                                ProcesarTmp();
                            }
                            catch (Exception ex)
                            {
                                ok = false;
                                ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                            }
                        }


                        if (ok)
                        {

                            int totalOk = tempSocios.Count - errores;

                            if (errores > 0)
                            {
                                ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }

                            int cantErrores = 0;

                            using (var dbContext = new ACHEEntities())
                            {
                                cantErrores = dbContext.SociosTmp.Where(x => x.Error.Length > 0).Count();
                            }

                            if (cantErrores > 0)
                            {
                                totalOk = totalOk - cantErrores;
                                var msg = ". Se encontraron <a href='javascript:verErrores();'>" + cantErrores + " errores</a> en la importación.";

                                ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);

                                //lblResultados.ForeColor = Color.Red;
                                //lblResultados.Visible = true;
                            }
                            else
                                ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());

                        }

                    }

                    File.Delete(path);

                    #endregion
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    [WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.SociosTmp.Where(x => x.Error.Length > 0).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Affinity + "</td>";
                        html += "<td>" + detalle.NroTarjeta + "</td>";
                        html += "<td>" + detalle.Apellido + " " + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    private void EliminarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {
            dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE SociosTmp", new object[] { });
        }
    }

    private void ProcesarTmp()
    {
        using (var dbContext = new ACHEEntities())
        {   
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                //var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                dbContext.Database.CommandTimeout = 1000;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarSociosTmp " + int.Parse(ddlFranquicias.SelectedValue), new object[] { });
            }
            else
            {
                dbContext.Database.CommandTimeout = 1000;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarSociosTmp " + hdnIDFranquicias.Value, new object[] { });
            }
        }
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}