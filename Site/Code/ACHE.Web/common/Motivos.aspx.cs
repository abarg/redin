﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;


public partial class common_Motivos : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
      
       cargarMarcas();
        
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            ddlMarcas.Enabled = false;

        }
    }
    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }
    private void cargarMarcas()
    {
        try
        {

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
           
            listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
             int idMarca =0;
             if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
             {
                 var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                 idMarca = usu.IDMarca;
             }
            using (var dbContext = new ACHEEntities()) {
                var result = dbContext.Motivos.Where(x => idMarca == 0 || x.IDMarca == idMarca)
                        .OrderBy(x => x.Nombre)
                        .Select(x => new {
                            IDMotivo = x.IDMotivo,
                            Nombre = x.Nombre,
                            IDMarca=x.IDMarca.HasValue?x.IDMarca.Value:0,
                            Marca=(x.IDMarca.HasValue)?x.Marcas.Nombre:"ADMIN",
                            Descripcion = x.Descripcion,
                            SumaPuntos=x.SumaPuntos?"SI":"NO"
                        }).ToDataSourceResult(take, skip, sort, filter);

                return result;
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
             if (HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null){

            using (var dbContext = new ACHEEntities()) {
                try {
                    var motivo = dbContext.Motivos.Where(x => x.IDMotivo == id).FirstOrDefault();
                    if (motivo != null) {
                        //if (dbContext.Motivos.Any(x => x.IDMotivo == id))
                        //    throw new Exception("No se puede eliminar ya ese motivo está siendo utilizado por algun proceso");
                        dbContext.Motivos.Remove(motivo);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e) {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string nombre,int idmarca) {
        string fileName = "Motivos";
        string path = "/tmp/";
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null){
               
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Motivos.Include("Marcas").Where(x => idmarca == 0 || x.IDMarca == idmarca).OrderBy(x => x.Nombre).AsEnumerable();

                    if (!string.IsNullOrEmpty(nombre))
                        info = info.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));

                    dt = info.Select(x => new {
                        Motivo = x.Nombre,
                        Marca = ( x.Marcas!=null) ? x.Marcas.Nombre : "ADMIN",
                         SumaPuntos=x.SumaPuntos?"SI":"NO",
                        Descrpicion = x.Descripcion

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }





    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}