﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AsociarTarjetas.aspx.cs" Inherits="common_AsociarTarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/AsociarTarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Asociar tarjetas</li>
            </ul>
        </div>
    </nav>
    
     <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" >Asociar tarjetas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los socios se asociaron correctamente.</div>
            <form runat="server" id="formAsociarTarjeta" class="form-horizontal" role="form">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtDocResponsable" CssClass="form-control number" MaxLength="20" MinLength="6"></asp:TextBox>
                                    <span class="help-block">Nro Documento Responsable</span>
                            </div>
                            <div class="col-lg-3">
                                <button runat="server" id="btnBuscarResponsable" class="btn" type="button" onclick="buscarResponsable();" >Buscar</button>
                                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading1" style="display:none" />
                            </div>
                        </div>
                    </div>

                    <div id="divSocioEncontradoResp" style="display:none">
                        <div class="vcard">
					    <ul style="margin: 10px 0 0 180px">
						        <li class="v-heading">
							        Socio encontrado!
						        </li>
						        <li>
							        <span class="item-key">Nombre</span>
							        <div class="vcard-item" id="lblNombreResp"></div>
						        </li>
						        <li>
							        <span class="item-key">Apellido</span>
							        <div class="vcard-item" id="lblApellidoResp"></div>
						        </li>
						        <li>
							        <span class="item-key">Email</span>
							        <div class="vcard-item" id="lblEmailResp"></div>
						        </li>
						        <li>
							        <span class="item-key">Sexo</span>
							        <div class="vcard-item" id="lblSexoResp"></div>
						        </li>
                                <li>
							        <span class="item-key">Fecha Nac.</span>
							        <div class="vcard-item" id="lblFechaNacResp"></div>
						        </li>
                                    <br />
                            </ul>

                            <asp:HiddenField runat="server" ID="hdnIDSocioResponsable" Value="0" />
                        </div>
                        <br />
                        <br />
                        <br />
                    </div>

                    <div id="divSocioNoEncontradoResp" style="display:none">
                        <div class="vcard">
					        <ul style="margin: 10px 0 0 180px">
						        <li class="v-heading">
							        Socio NO encontrado! 
						        </li>
                            </ul>
                        </div>
                        <br />
                    </div>
                    <div class ="asociado" style="display:none">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtDocAsociado" CssClass="form-control number" MaxLength="20" MinLength="6"></asp:TextBox>
                                        <span class="help-block">Nro Documento asociado</span>
                                </div>
                                <div class="col-lg-3">
                                    <button runat="server" id="btnBuscar" class="btn" type="button" onclick="buscar();" >Buscar</button>
                                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading2" style="display:none" />
                                </div>
                            </div>
                        </div>
                    
                        <div id="divSocioEncontrado" style="display:none">
                            <div class="vcard">
					        <ul style="margin: 10px 0 0 180px">
						            <li class="v-heading">
							            Socio encontrado!
						            </li>
						            <li>
							            <span class="item-key">Nombre</span>
							            <div class="vcard-item" id="lblNombre"></div>
						            </li>
						            <li>
							            <span class="item-key">Apellido</span>
							            <div class="vcard-item" id="lblApellido"></div>
						            </li>
						            <li>
							            <span class="item-key">Email</span>
							            <div class="vcard-item" id="lblEmail"></div>
						            </li>
						            <li>
							            <span class="item-key">Sexo</span>
							            <div class="vcard-item" id="lblSexo"></div>
						            </li>
                                    <li>
							            <span class="item-key">Fecha Nac.</span>
							            <div class="vcard-item" id="lblFechaNac"></div>
						            </li>
                                        <br />
                                </ul>
                                <asp:HiddenField runat="server" ID="hdnIDSocioAsociado" Value="0" />
                            </div>
                            <br />
                            <br />
                            <br />
                        </div>

                        <div id="divSocioNoEncontrado" style="display:none">
                            <div class="vcard">
					            <ul style="margin: 10px 0 0 180px">
						            <li class="v-heading">
							            Socio NO encontrado! 
						            </li>
                                </ul>
                            </div>
                            <br />
                        </div>
                        <div class ="row" id="divAsociar" style="display:none">
                             <div class="col-sm-8 col-sm-offset-3">
                                   <button runat="server" id="btnAsociado" class="btn btn-success" type="button" onclick="asociar();" >Asociar</button>
                             </div>
                        </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

