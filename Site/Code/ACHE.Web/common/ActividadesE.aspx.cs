﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ActividadesE : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.cargarCategorias();



        if (!String.IsNullOrEmpty(Request.QueryString["IDActividad"]))
        {
            try
            {
                int IDActividad = int.Parse(Request.QueryString["IDActividad"]);
                if (IDActividad > 0)
                {
                    this.hdnIDActividad.Value = IDActividad.ToString();
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDActividad"]));
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Actividades.aspx");
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Actividades.Where(x => x.IDActividad == id).FirstOrDefault();
                if (entity != null)
                {

                    this.cmbSubCategorias.SelectedValue = entity.IDSubCategoria.ToString();

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.ActividadCategorias.Include("ActividadSubCategorias").Where(x => x.ActividadSubCategorias.FirstOrDefault() != null).OrderBy(x => x.Categoria).Select(x => new { IDCategoria = x.IDCategoria, Categoria = x.Categoria.ToUpper() }).ToList();
            if (categorias != null)
            {
                cmbCategorias.DataSource = categorias;
                cmbCategorias.DataTextField = "Categoria";
                cmbCategorias.DataValueField = "IDCategoria";
                cmbCategorias.DataBind();
                cmbCategorias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarSubCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var subcategorias = dbContext.ActividadSubCategorias.OrderBy(x => x.SubCategoria).ToList();
            if (subcategorias != null)
            {
                cmbSubCategorias.DataSource = subcategorias;
                cmbSubCategorias.DataTextField = "SubCategoria";
                cmbSubCategorias.DataValueField = "IDSubCategoria";
                cmbSubCategorias.DataBind();
                cmbSubCategorias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> subCategoriasByCategorias(int idCategoria)
    {
        List<ComboViewModel> listSubCategorias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {

            listSubCategorias = dbContext.ActividadSubCategorias.Where(x => x.IDCategoria == idCategoria).Select(x => new ComboViewModel { ID = x.IDSubCategoria.ToString(), Nombre = x.SubCategoria }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listSubCategorias.Insert(0, todas);

        }
        return listSubCategorias;
    }


    [WebMethod(true)]
    public static List<ComboViewModel> comerciosByFranquicia(int idFranquicia)
    {
        List<ComboViewModel> listComercios = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {

            listComercios = dbContext.Comercios.Where(x => x.IDFranquicia == idFranquicia).Select(x => new ComboViewModel { ID = x.IDComercio.ToString(), Nombre = x.NombreFantasia }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listComercios.Insert(0, todas);

        }
        return listComercios;
    }



    [WebMethod(true)]
    public static string grabar(string idSubCategoria, string Actividad, string idActividad, string Niveles, string Horarios, string Sedes)
    {

        // parse to int parameters

        int idSub = int.Parse(idSubCategoria);
        int idAct = int.Parse(idActividad);


        try
        {
            //Actividad
            Actividades entity;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Actividades.Where(x => x.IDSubCategoria== idSub && x.Actividad == Actividad && x.IDActividad != idAct).FirstOrDefault();
                if (aux != null)
                    throw new Exception("Ya existe una actividad perteneciente a la subcategoria con el mismo nombre");

                if (idAct > 0)
                    entity = dbContext.Actividades.FirstOrDefault(s => s.IDActividad == idAct);
                else
                    entity = new Actividades();

                entity.Actividad = Actividad != null && Actividad != "" ? Actividad.Trim().ToUpper() : "";
                entity.IDSubCategoria = idSub;



                //NIVELES
                if (idAct > 0 && entity.ActividadNiveles.Any())
                {
                    var niveles = entity.ActividadNiveles.ToList();
                    dbContext.ActividadNiveles.RemoveRange(niveles);
                }

                List<ActividadNiveles> listNiveles = new List<ActividadNiveles>();
                if (Niveles != "")
                {
                    string[] niveles;
                    niveles = Niveles.Split(",");
                    foreach (var auxNivel in niveles)
                    {
                        ActividadNiveles nivel = new ActividadNiveles();
                   
                        nivel.Nivel= auxNivel;

                        if (idAct > 0)
                        {
                            nivel.IDActividad = idAct;

                            entity.ActividadNiveles.Add(nivel);
                        }
                        else
                        {
                            listNiveles.Add(nivel);
                        }
                    }
                }
                if (listNiveles.Count() > 0)
                {
                    entity.ActividadNiveles = listNiveles;
                }


                //HORARIOS
                if (idAct > 0 && entity.ActividadHorarios.Any())
                {
                    var horarios = entity.ActividadHorarios.ToList();
                    dbContext.ActividadHorarios.RemoveRange(horarios);
                }

                List<ActividadHorarios> listHorarios = new List<ActividadHorarios>();
                if (Horarios != "")
                {
                    string[] horarios;
                    horarios = Horarios.Split(",");
                    foreach (var auxHorario in horarios)
                    {
                        ActividadHorarios horario = new ActividadHorarios();

                        horario.Horario = auxHorario;

                        if (idAct > 0)
                        {
                            horario.IDActividad = idAct;

                            entity.ActividadHorarios.Add(horario);
                        }
                        else
                        {
                            listHorarios.Add(horario);
                        }
                    }
                }
                if (listHorarios.Count() > 0)
                {
                    entity.ActividadHorarios = listHorarios;
                }

                //SEDE
                if (idAct > 0 && entity.ActividadComercio.Any())
                {
                    var horarios = entity.ActividadComercio.ToList();
                    dbContext.ActividadComercio.RemoveRange(horarios);
                }

                List<ActividadComercio> listSedes = new List<ActividadComercio>();
                if (Sedes != "")
                {
                    string[] sedes;
                    sedes = Sedes.Split(",");
                    foreach (var auxSede in sedes)
                    {
                        ActividadComercio sede = new ActividadComercio();

                        sede.IDComercio = int.Parse(auxSede);

                        if (idAct > 0)
                        {
                            sede.IDActividad = idAct;

                            entity.ActividadComercio.Add(sede);
                        }
                        else
                        {
                            listSedes.Add(sede);
                        }
                    }
                }
                if (listSedes.Count() > 0)
                {
                    entity.ActividadComercio = listSedes;
                }

                if (idAct > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Actividades.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDActividad.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


}