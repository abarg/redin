﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;

public partial class common_Sociose : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        cargarSearchables();


        Session["CurrentFoto"] = null;
        //cargarProvincias();
        cargarPaises();
        cargarMarcas();
        cargarProfesion();
        cargarMotivos();

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
        }
        if (HttpContext.Current.Session["CurrentUser"] != null)
            cargarMarcas();

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            hdnIDMarca.Value = usu.IDMarca.ToString();
        }
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            hdnIDMultimarca.Value = usu.IDMultimarca.ToString();
        }
        if (!String.IsNullOrEmpty(Request.QueryString["IDSocio"]))
        {
            this.limpiarControles();
            this.hfIDSocio.Value = Request.QueryString["IDSocio"];
            Session["IDSocio"] = Convert.ToInt32(Request.QueryString["IDSocio"]);

            if (!this.hfIDSocio.Value.Equals("0"))
            {
                
                this.hfGrabarDomicilio.Value = "1";
                this.hfVerTarjetas.Value = "1";
                this.cargarDatosSocio(Convert.ToInt32(Request.QueryString["IDSocio"]));
                cargarTarjetas(Convert.ToInt32(Request.QueryString["IDSocio"]));
            }

        }
    }
    private void cargarSearchables()
    {
      
        using (var dbContext = new ACHEEntities())
        {
            var hobbies = dbContext.Hobbies.OrderBy(x => x.Nombre).ToList();
            if (hobbies != null)
            {
                searchableHobbies.DataSource = hobbies;
                searchableHobbies.DataBind();
            }


            //var sql = @"SELECT 'Actividad: ' + A.Actividad + ' Nivel: ' + AN.Nivel + ' Horario: ' + AH.Horario + ' Sede: ' + C.NombreFantasia COLLATE Modern_Spanish_CI_AS AS ActividadNivelHorario, CONVERT(varchar(10), AH.IDHorario) + ' - ' + CONVERT(varchar(10),C.IDComercio) as IDHorarioIDSede 
            //            FROM Actividades AS A 
            //            JOIN ActividadNiveles as AN ON AN.IDActividad = A.IDActividad
            //            JOIN ActividadHorarios as AH on AH.IDActividad = A.IDActividad
            //            Join ActividadComercio as AC on AC.IDActividad = A.IDActividad
            //            Join Comercios as C on C.IDComercio = AC.IDComercio";

            //var actividades = dbContext.Database.SqlQuery<ActividadesView>(sql, new object[] { }).ToList();
            //if (actividades != null)
            //{
            //    searchableActividades.Visible = false;
            //    searchableActividades.DataSource = actividades;
            //    searchableActividades.DataBind();
            //}

        }
    }


    [System.Web.Services.WebMethod(true)]
    public static List<int> getHobbies(int idsocio)
    {
        List<int> ids = new List<int>();
        using (var dbContext = new ACHEEntities())
        {
            var hobbies = dbContext.HobbiesSocios.Where(x => x.IDSocio == idsocio).ToList();
            foreach (var hobbie in hobbies)
            {
                ids.Add(hobbie.IDHobbie);
            }
        }
        return ids;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<singleID> getAsignaciones(int idsocio)
    {
        List<singleID> idList;

        using (var dbContext = new ACHEEntities())
        {


            idList = dbContext.ActividadSocioNivel.Where(x => x.IDSocio == idsocio).OrderBy(x => x.IDActividad).Select(x => new singleID { id = x.IDHorario.ToString() + " - " + x.IDComercio.ToString() }).ToList();


        }
        return idList;
    }

    public class singleID
    {
        public string id;
    }


    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";
    }

    private void cargarProvincias()
    {
        ddlProvincia.DataSource = Common.LoadProvincias();
        ddlProvincia.DataTextField = "Nombre";
        ddlProvincia.DataValueField = "ID";
        ddlProvincia.DataBind();
    }

    private void cargarPaises()
    {
        //using (var dbContext = new ACHEEntities()) {
        //    var paises = dbContext.Paises.Include("Provincias").Where(x => x.Provincias.FirstOrDefault() != null).OrderBy(x => x.Nombre).Select(x => new { IDPais = x.IDPais, Nombre = x.Nombre.ToUpper() }).ToList();
        //    if (paises != null) {
        ddlPais.DataSource = Common.LoadPaises();
        ddlPais.DataTextField = "Nombre";
        ddlPais.DataValueField = "ID";
        ddlPais.DataBind();
        //    }
        //}
    }

    private void cargarProfesion()
    {
        using (var dbContext = new ACHEEntities())
        {
            var profesion = dbContext.Profesiones.OrderBy(x => x.Nombre).Select(x => new { IDProfesion = x.IDProfesion , Nombre = x.Nombre}).ToList();
            if (profesion != null)
            {
                ddlProfesion.DataSource = profesion;
                ddlProfesion.DataValueField = "IDProfesion";
                ddlProfesion.DataTextField = "Nombre";
                ddlProfesion.DataBind();
                ddlProfesion.Items.Insert(0, new ListItem("", "0"));
            }

        }
    }

    private void cargarMarcas()
    {
        try
        {

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
            }
            else if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                    bMultimarca bMultimarca = new bMultimarca();
                    listMarcas = bMultimarca.getMarcas(usu.IDMultimarca).ToList();                
            }
            else
                listMarcas = bMarca.getMarcas();

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void cargarMotivos() {

        try {
            using (var dbContext = new ACHEEntities()) {
                var listMotivos = dbContext.Motivos.ToList();
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    listMotivos = listMotivos.Where(x =>x.IDMarca !=null &&  x.Marcas.IDFranquicia == usu.IDFranquicia).ToList();
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    listMotivos = listMotivos.Where(x => x.IDMarca != null && x.IDMarca ==usu.IDMarca).ToList();
                }
               
                this.ddlMotivos.DataSource = listMotivos;
                this.ddlMotivos.DataValueField = "IDMotivo";
                this.ddlMotivos.DataTextField = "Nombre";
                this.ddlMotivos.DataBind();

                this.ddlMotivos.Items.Insert(0, new ListItem("", ""));
            }

        }
        catch (Exception ex) {
            throw ex;
        }
    }

    private void cargarTarjetas(int IDSocio) {
        try {
            using (var dbContext = new ACHEEntities()) {
                var listTarjetas = dbContext.Tarjetas.Where(x => x.IDSocio == IDSocio).Select(x=> new {ID=x.IDTarjeta,Numero = x.Numero}).ToList();
                this.ddlTarjetas2.DataSource = listTarjetas;
                this.ddlTarjetas2.DataValueField = "ID";
                this.ddlTarjetas2.DataTextField = "Numero";
                this.ddlTarjetas2.DataBind();

                this.ddlTarjetas2.Items.Insert(0, new ListItem("", ""));
            }

        }
        catch (Exception ex) {
            throw ex;
        }

    }

    private void limpiarControles()
    {
        //Socio
        //this.txtTarjeta.Text = "";
        this.txtNroCuenta.Text = "";
        this.txtNombre.Text = "";
        this.txtApellido.Text = "";
        this.txtEmail.Text = "";
        this.txtNroDoc.Text = "";
        this.txtTelefono.Text = "";
        this.txtCelular.Text = "";
        this.ddlEmpresaCelular.SelectedValue = "";
        this.ddlProfesion.SelectedValue = "0";
        //this.txtPuntosDisponibles.Text = "";
        //this.txtPuntosTotales.Text = "";
        this.txtObservaciones.Text = "";
        //Domicilio
        //this.txtCiudad.Text = "";
        this.txtDomicilio.Text = "";
        this.txtCodigoPostal.Text = "";
        this.txtTelefonoDom.Text = "";
        this.txtFax.Text = "";
        this.txtPisoDepto.Text = "";
        this.txtNumSUBE.Text = "";
        this.txtNumMonedero.Text = "";
        this.txtNumTarjTransporte.Text = "";
        this.txtPatente.Text = "";
    }


    private void cargarDatosSocio(int IDSocio)
    {

        //Socio
        bSocio bSocio = new bSocio();
        Socios oSocio = bSocio.getSocio(IDSocio);

        try
        {
         
            this.txtImporteTope.Text = oSocio.ImporteTope != null ? oSocio.ImporteTope.ToString() : "";
            this.txtFechaTopeCanje.Text = oSocio.fechaTopeCanje != null ? oSocio.fechaTopeCanje.Value.ToString("dd/MM/yyyy") : "";
            this.txtFechaCaducidad.Text = oSocio.fechaCaducidad != null ? oSocio.fechaCaducidad.Value.ToString("dd/MM/yyyy") : "";
            this.txtNroCuenta.Text = oSocio.NroCuenta;
            this.txtNombre.Text = oSocio.Nombre != null && oSocio.Nombre != "" ? oSocio.Nombre.ToUpper() : "";
            this.txtApellido.Text = oSocio.Apellido != null && oSocio.Apellido != "" ? oSocio.Apellido.ToUpper() : "";
            this.txtEmail.Text = oSocio.Email;
            this.txtPwd.Text = oSocio.Pwd;
            if (oSocio.FechaNacimiento.HasValue)
            {
                this.ddlDia.SelectedValue = oSocio.FechaNacimiento.Value.Day.ToString();
                this.ddlMes.SelectedValue = oSocio.FechaNacimiento.Value.Month.ToString();
                this.ddlAnio.SelectedItem.Text = oSocio.FechaNacimiento.Value.Year.ToString();
            }
            if (oSocio.Sexo != null && oSocio.Sexo.Equals("F"))
                this.rdbFem.Checked = true;
            else if (oSocio.Sexo != null && oSocio.Sexo.Equals("M"))
                this.rdbMas.Checked = true;
            else
                this.rdbInd.Checked = true;


        }
        catch (Exception ex)
        {
            throw ex;
        }

        try
        {

            this.txtNroDoc.Text = oSocio.NroDocumento;
            this.ddlTipoDoc.SelectedValue = oSocio.TipoDocumento;
            this.txtTelefono.Text = oSocio.Telefono;
            this.txtCelular.Text = oSocio.Celular;
            this.ddlEmpresaCelular.SelectedValue = oSocio.EmpresaCelular;
            this.ddlProfesion.SelectedValue = oSocio.IDProfesion.ToString();
            this.txtObservaciones.Text = oSocio.Observaciones;
            this.txtNumSUBE.Text = oSocio.NumeroTarjetaSube;
            this.txtNumMonedero.Text = oSocio.NumeroTarjetaMonedero;
            this.txtNumTarjTransporte.Text = oSocio.NumeroTarjetaTransporte;
            this.txtPatente.Text = oSocio.PatenteCoche;
            this.txttwitter.Text = oSocio.Twitter;
            this.txtFacebook.Text = oSocio.Facebook;
            this.txtBanco.Text = oSocio.Banco != null && oSocio.Banco != "" ? oSocio.Banco.ToUpper() : "";
            this.txtNroCuentaPLUSIN.Text = oSocio.NroCuentaPlusIN;
            this.txtCBU.Text = oSocio.CBU;
            this.ddlTipoCuenta.SelectedValue = oSocio.TipoCuenta;
            this.txtEmailPlusIn.Text = oSocio.EmailPlusIn;
            this.txtFormaPago_CBU_Rep.Text = oSocio.CBU;
            if (oSocio.AuthTipo == "FB")
            {
                imgFoto.ImageUrl = string.Format("https://graph.facebook.com/{0}/picture?type=normal", oSocio.AuthID);
                divFoto.Visible = true;
                lnkFotoDelete.Visible = false;
                lnkFoto.Visible = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(oSocio.Foto))
                {
                    lnkFoto.NavigateUrl = "/fileHandler.ashx?type=socios&module=admin&file=" + oSocio.Foto;
                    lnkFotoDelete.NavigateUrl = "javascript: void(0)";
                    lnkFotoDelete.Attributes.Add("onclick", "return deleteUpload('" + oSocio.Foto + "', 'Foto')");
                    divFoto.Visible = true;

                    imgFoto.ImageUrl = "/files/socios/" + oSocio.Foto;
                }
                else
                    imgFoto.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";
            }


        }
        catch (Exception ex)
        {
            throw ex;
        }


        //Domicilio
        if (oSocio.IDDomicilio.HasValue)
        {
            try
            {
                var oDomicilio = oSocio.Domicilios;
                if (oDomicilio != null)
                {
                    var idPais = 0;
                    string pais = oDomicilio.Pais.ToString().ToLower();
                    using (var dbContext = new ACHEEntities())
                    {
                        idPais = dbContext.Paises.Where(x => x.Nombre.ToLower() == pais).FirstOrDefault().IDPais;

                        if (idPais > 0)
                        {
                            this.ddlPais.SelectedValue = idPais.ToString();
                            List<ComboViewModel> listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
                            ddlProvincia.DataSource = listProvincias;
                            ddlProvincia.DataTextField = "Nombre";
                            ddlProvincia.DataValueField = "ID";
                            ddlProvincia.DataBind();
                            this.ddlProvincia.SelectedValue = oDomicilio.Provincia.ToString();
                        }
                    }

                    ddlCiudad.DataSource = Common.LoadCiudades(oDomicilio.Provincia);
                    ddlCiudad.DataTextField = "Nombre";
                    ddlCiudad.DataValueField = "ID";
                    ddlCiudad.DataBind();
                    ddlCiudad.Items.Insert(0, new ListItem("", ""));

                    if (oDomicilio.Ciudad.HasValue)
                        ddlCiudad.SelectedValue = oDomicilio.Ciudad.Value.ToString();

                    //this.txtCiudad.Text = oDomicilio.Ciudad;
                    this.txtDomicilio.Text = oDomicilio.Domicilio;
                    this.txtCodigoPostal.Text = oDomicilio.CodigoPostal;
                    this.txtTelefonoDom.Text = oDomicilio.Telefono;
                    this.txtFax.Text = oDomicilio.Fax;
                    this.txtPisoDepto.Text = oDomicilio.PisoDepto;
                    this.txtLatitud.Text = oDomicilio.Latitud;
                    this.txtLongitud.Text = oDomicilio.Longitud;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        try { 
        //costos
        this.txtCostoTransaccionalSoloPuntos.Text = oSocio.CostoTransaccionalSoloPuntos.ToString();
            this.txtCostoTransaccionalCanje.Text = oSocio.CostoTransaccionalCanje.ToString();

            this.txtCostoTransaccionalConDescuento.Text = oSocio.CostoTransaccionalConDescuento.ToString();

            this.txtCostoSeguro.Text = oSocio.CostoSeguro.ToString();

            this.txtCostoSMS.Text = oSocio.CostoSMS.ToString();

            ////Transacciones
            //txtPuntos.Text = obtenerPuntosActuales(oSocio.IDSocio).Replace(".", ""); 

            //get totales
            using (var dbContext = new ACHEEntities())
            {
                int idFranquicia = 0;
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    idFranquicia = usu.IDFranquicia;
                }
                /*  var tarjetas = dbContext.Tarjetas.Where(x => x.IDSocio.HasValue && x.IDSocio.Value == oSocio.IDSocio && (idFranquicia == 0 || x.IDFranquicia == idFranquicia)
                       && !x.FechaBaja.HasValue).ToList();*/
                int idMarca = Convert.ToInt32(this.hdnIDMarca.Value);
                var tarjetas = dbContext.Tarjetas.Where(x => x.IDSocio.HasValue && (x.IDSocio.Value == oSocio.IDSocio || x.Socios.IDSocioPadre == oSocio.IDSocio)
                        && (idFranquicia == 0 || x.IDFranquicia == idFranquicia) && (idMarca == 0 || x.IDMarca == idMarca) && !x.FechaBaja.HasValue).ToList();



                var credito = tarjetas.Sum(x => x.Credito);
                var giftcard = tarjetas.Sum(x => x.Giftcard);

                litTotalPuntos.Text = tarjetas.Sum(x => x.PuntosTotales).ToString("N0");
                litTotalCredito.Text = credito.ToString("N2");
                litTotalGiftcard.Text = giftcard.ToString("N2");
                litTotal.Text = (credito + giftcard).ToString("N2");
                litTotalPos.Text = Math.Round(credito + giftcard).ToString("N0");

                divInfo.Visible = !tarjetas.Any();
                //Validar cuando se pone en 0 (cuando responsable =null o responsable=True)
                if (tarjetas.Any())
                {

                    //Responsable =0 no tiene ningun responsable asi que puede asociar tarjetas a el 
                    //Responsable=1 tiene un responsable asi que no puede asociar tarjetas a el

                    if ((((!oSocio.Responsable.HasValue) && idMarca == 0) || oSocio.Responsable == true))
                    {
                        this.hdnResponsable.Value = "0";
                    }
                    else if ((idMarca == 0 && !oSocio.Responsable.Value) || (idMarca > 0 && oSocio.Responsable == false))
                    {
                        this.hdnResponsable.Value = "1";
                        Socios socioResponsable = bSocio.getSocio(oSocio.IDSocioPadre ?? 0);
                        litNombreResponsable.Text = socioResponsable.Apellido;
                    }

                }
            }
          
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod(true)]
    public static string grabar(int IDSocio, string NroCuenta, string Nombre, string Apellido, string Email, string Dia, string Mes, string Anio, string Sexo, string TipoDoc
        , string NroDoc, string Telefono, string Celular, string EmpresaCelular, string Observaciones, string NumeroSube, string NumeroMonedero, string NumeroTransporte, string Patente
        , string Twitter, string Facebook, string GrabarDomicilio, string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax
        , string PisoDepto, string Lat, string Long, string CostoTransaccionalConDescuento, string CostoTransaccionalSoloPuntos, string CostoTransaccionalCanje, string CostoSeguro, string CostoSMS, string Banco, string TipoCuenta, string NroCuentaPLUSIN
        , string CBU, string EmailPlusIn, string fechaCaducidad, string fechaTopeCanje, string Pwd, int Profesion, string ImporteTope, string Hobbies, string Actividades)
    {
        try
        {
            //Socio
            Socios entity;
            using (var dbContext = new ACHEEntities())
            {
                if (NroDoc != string.Empty && NroDoc != "00")
                {
                    var aux = dbContext.Socios.FirstOrDefault(s => s.NroDocumento == NroDoc && s.TipoDocumento == TipoDoc);
                    if (aux != null && aux.IDSocio != IDSocio)
                        throw new Exception("Ya existe un socio llamado " + aux.Apellido + ", " + aux.Nombre + " con ese Número de Documento");
                }

                if (IDSocio > 0)
                    entity = dbContext.Socios.FirstOrDefault(s => s.IDSocio == IDSocio);
                else
                {
                    entity = new Socios();
                    entity.FechaAlta = DateTime.Now;
                    entity.Pwd = Pwd == null || Pwd == "" ? new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia)).ToString("ddMMyyyy") : Pwd;
                    entity.IDFranquiciaAlta = 5;//Pongo a MDQ como franquicia alta
                }
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
                {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    entity.IDFranquiciaAlta = usu.IDFranquicia;
                }
                if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    entity.IDFranquiciaAlta = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).First().IDFranquicia.Value;
                }

                if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                    var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                    entity.IDFranquiciaAlta = dbContext.Multimarcas.Where(x => x.IDMultimarca == usu.IDMultimarca).First().IDFranquicia.Value;
                }
                if (fechaTopeCanje != string.Empty)
                    entity.fechaTopeCanje = DateTime.Parse(fechaTopeCanje);
                else
                    entity.fechaTopeCanje = null;
                if (fechaCaducidad != string.Empty)
                    entity.fechaCaducidad = DateTime.Parse(fechaCaducidad);
                else
                    entity.fechaCaducidad = null;

                if (ImporteTope != string.Empty)
                    entity.ImporteTope = int.Parse(ImporteTope);
                else
                    entity.ImporteTope = null;

                entity.Pwd = Pwd;
                entity.TipoMov = "A";
                entity.TipoReg = "D";
                entity.Fecha = DateTime.Now;
                entity.NroCuenta = NroCuenta.ToString();
                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.ToUpper() : "";
                entity.Apellido = Apellido != null && Apellido != "" ? Apellido.ToUpper() : "";
                entity.Email = corregirEmail(Email);
                entity.FechaNacimiento = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia));
                entity.Sexo = Sexo;
                entity.TipoDocumento = TipoDoc;
                entity.NroDocumento = NroDoc.ToString();
                entity.Telefono = Telefono;
                entity.Celular = corregirCelular(Celular, Pais);
                entity.EmpresaCelular = EmpresaCelular;
                if (Profesion == 0)
                {
                    entity.IDProfesion = null;
                }
                else
                {
                    entity.IDProfesion = Profesion;
                }
                entity.Observaciones = Observaciones;
                entity.NumeroTarjetaSube = NumeroSube;
                entity.NumeroTarjetaMonedero = NumeroMonedero;
                entity.NumeroTarjetaTransporte = NumeroTransporte;
                entity.PatenteCoche = Patente;
                entity.Twitter = Twitter;
                entity.Facebook = Facebook;
                entity.Activo = true;
                if (HttpContext.Current.Session["CurrentFoto"] != null && HttpContext.Current.Session["CurrentFoto"] != "")
                    entity.Foto = HttpContext.Current.Session["CurrentFoto"].ToString();

                if (!entity.IDDomicilio.HasValue)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                }

                entity.Domicilios.TipoDomicilio = "S";
                entity.Domicilios.Estado = "A";
                entity.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                entity.Domicilios.Provincia = int.Parse(Provincia);
                if (Ciudad != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                entity.Domicilios.CodigoPostal = CodigoPostal;
                entity.Domicilios.Telefono = TelefonoDom;
                entity.Domicilios.Fax = Fax;
                entity.Domicilios.PisoDepto = PisoDepto;
                entity.Domicilios.Entidad = "S";
                entity.Domicilios.Latitud = Lat;
                entity.Domicilios.Longitud = Long;

                if (CostoTransaccionalConDescuento != "")
                    entity.CostoTransaccionalConDescuento = Convert.ToDecimal(CostoTransaccionalConDescuento);
                else
                    entity.CostoTransaccionalConDescuento = 0;

                if (CostoTransaccionalSoloPuntos != "")
                    entity.CostoTransaccionalSoloPuntos = Convert.ToDecimal(CostoTransaccionalSoloPuntos);
                else
                    entity.CostoTransaccionalSoloPuntos = 0;

                if (CostoTransaccionalCanje != "")
                    entity.CostoTransaccionalCanje = Convert.ToDecimal(CostoTransaccionalCanje);
                else
                    entity.CostoTransaccionalCanje = 0;

                if (CostoSeguro != "")
                    entity.CostoSeguro = Convert.ToDecimal(CostoSeguro);
                else
                    entity.CostoSeguro = 0;

                if (CostoSMS != "")
                    entity.CostoSMS = Convert.ToDecimal(CostoSMS);
                else
                    entity.CostoSMS = 0;



                entity.Banco = Banco != null && Banco != "" ? Banco.ToUpper() : "";
                entity.TipoCuenta = TipoCuenta;
                entity.NroCuentaPlusIN = NroCuentaPLUSIN;
                entity.CBU = CBU;
                entity.EmailPlusIn = EmailPlusIn;
                if (IDSocio > 0 && entity.HobbiesSocios.Any())
                {
                    var hobbies = entity.HobbiesSocios.ToList();
                    dbContext.HobbiesSocios.RemoveRange(hobbies);
                }

                List<HobbiesSocios> list = new List<HobbiesSocios>();

                if (Hobbies != "")
                {
                    string[] ids;
                    ids = Hobbies.Split(",");
                    foreach (var idHobbie in ids)
                    {
                        HobbiesSocios hobbie = new HobbiesSocios();
                        var auxIDHobbie = int.Parse(idHobbie);
                        hobbie.IDHobbie = auxIDHobbie;
                        if (IDSocio > 0)
                        {
                            hobbie.IDSocio = IDSocio;

                            entity.HobbiesSocios.Add(hobbie);
                        }
                        else
                        {
                            list.Add(hobbie);
                        }
                    }
                }
                if (list.Count() > 0)
                {
                    entity.HobbiesSocios = list;
                }

                if (IDSocio > 0 && entity.ActividadSocioNivel.Any())
                {
                    var actividades = entity.ActividadSocioNivel.ToList();
                    dbContext.ActividadSocioNivel.RemoveRange(actividades);
                }

                List<ActividadSocioNivel> listActividades = new List<ActividadSocioNivel>();
                if (Actividades != "")
                {
                    string[] ids;
                    ids = Actividades.Split(",");
                    foreach (var IDHorarioIDSede in ids)
                    {


                        string[] auxIds = IDHorarioIDSede.Split("-"); // indice 0 = horario, indice 1 = sede

                        ActividadSocioNivel asignacion = new ActividadSocioNivel();

                        int auxIDHorario = int.Parse(auxIds[0]);

                        int auxIDComercio = int.Parse(auxIds[1]);

                        int auxIDActividad = dbContext.ActividadHorarios.Where(x => x.IDHorario == auxIDHorario).First().IDActividad;

                        int auxIDNivel = dbContext.ActividadNiveles.Where(x => x.IDActividad == auxIDActividad).FirstOrDefault().IDNivel;

                        asignacion.IDActividad = auxIDActividad;
                        asignacion.IDNivel = auxIDNivel;
                        asignacion.IDComercio = auxIDComercio;
                        asignacion.IDHorario = auxIDHorario;

                        DateTime now = DateTime.Now;

                        asignacion.FechaInscripcion = now;

                        if (IDSocio > 0)
                        {
                            asignacion.IDSocio = IDSocio;

                            entity.ActividadSocioNivel.Add(asignacion);
                        }
                        else
                        {
                            listActividades.Add(asignacion);
                        }
                    }
                }
                if (listActividades.Count() > 0)
                {
                    entity.ActividadSocioNivel = listActividades;
                }

                if (IDSocio > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Socios.Add(entity);
                    dbContext.SaveChanges();
                }
            }
            return entity.IDSocio.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    public static string corregirCelular(string Celular,string Pais)
    {
        
        if (Celular == null) Celular = "";
        if (Celular.Length > 6) 
        {
            Celular = Celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            Celular = Celular.Trim();
            if (Celular[0] == '0') Celular = Celular.Remove(0, 1);
            if (Celular[1] == '0') Celular = Celular.Remove(1, 1);
            if (Celular[2] == '0') Celular = Celular.Remove(2, 1);
            if (Pais == "ARGENTINA")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '4') Celular = Celular.Insert(1, "4");
            }
            if (Pais == "BOLIVIA")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '9') Celular = Celular.Insert(1, "9");
                if (Celular[1] != '1') Celular = Celular.Insert(1, "1");
            }
            if (Pais == "CHILE")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '6') Celular = Celular.Insert(1, "6");
            }
            if (Pais == "ESPAÑA")
            {
                if (Celular[0] != '3') Celular = Celular.Insert(0, "3");
                if (Celular[1] != '4') Celular = Celular.Insert(1, "4");
            }
            if (Pais == "MEXICO")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '2') Celular = Celular.Insert(1, "2");
            }
            if (Pais == "PANAMA")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '0') Celular = Celular.Insert(1, "0");
                if (Celular[1] != '7') Celular = Celular.Insert(1, "7");
            }
            if (Pais == "PARAGUAY")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '9') Celular = Celular.Insert(1, "9");
                if (Celular[1] != '5') Celular = Celular.Insert(1, "5");
            }
            if (Pais == "URUGUAY")
            {
                if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
                if (Celular[1] != '9') Celular = Celular.Insert(1, "9");
                if (Celular[1] != '8') Celular = Celular.Insert(1, "8");
            }
        }
        return Celular;
    }

    public static string corregirEmail(string Email)
    {
        if (Email == null) Email = "";
        if (Email != "")
        {
            Email = Email.Replace("(", "").Replace(")", "").Replace("emal", "email").Replace("email.com.ar", "email.com.ar").Replace("hotmai.com", "hotmail.com").Replace("hotmaill.com", "hotmail.com").Replace("hotmal.com", "hotmail.com").Replace("yaho.com", "yahoo.com").Replace(" ", "").Replace("?", "").Replace("¿", "").Replace("¡", "").Replace("!", "").Replace("&", "").Replace("}", "").Replace("{", "").Replace(",", "").Replace("#", "");
            Email = Email.Trim();
        }
        return Email;
    }



    [WebMethod(true)]
    public static void eliminarFoto(int id, string archivo)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            var file = "//files//socios//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Foto = null;
                            HttpContext.Current.Session["CurrentFoto"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadFoto(object sender, EventArgs e)
    {
        try
        {
            var extension = flpFoto.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpFoto.FileName);
                string uniqueName = "foto_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/socios/"), uniqueName);

                flpFoto.SaveAs(path);

                Session["CurrentFoto"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentFoto"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    [WebMethod(true)]
    public static void asociarTarjeta(int IDSocio, string Tarjeta)
    {
        try
        {
            int idFranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idFranquicia = usu.IDFranquicia;
            }
            int idMarca = 0;
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                idMarca = usu.IDMarca;
            }

            int idMultimarca = 0;
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                idMultimarca = usu.IDMultimarca;
            }

            if (idFranquicia > 0 || HttpContext.Current.Session["CurrentMarcasUser"] != null || idMultimarca > 0 || HttpContext.Current.Session["CurrentUser"] != null)
            {

                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = null;

                oTarjeta = bTarjeta.getTarjetaPorNumero(Tarjeta, false);
                if (oTarjeta == null)
                    throw new Exception("El Número de Tarjeta no existe");
                else if (idFranquicia > 0 && oTarjeta.IDFranquicia != idFranquicia || idMarca > 0 && oTarjeta.IDMarca != idMarca)
                    throw new Exception("El Número de Tarjeta no existe");
                else if (oTarjeta.IDSocio.HasValue)
                    throw new Exception("La Tarjeta ya se encuentra asignada a un socio");
                else
                {
                    oTarjeta.IDSocio = IDSocio;
                    oTarjeta.FechaAsignacion = DateTime.Now;
                    if (idFranquicia > 0)
                        oTarjeta.IDFranquicia = idFranquicia;

                    bTarjeta.add(oTarjeta);

                    bSocio bSocio = new bSocio();
                    Socios oSocio = bSocio.getSocio(IDSocio);
                    var marca = oTarjeta.Marcas;

                    if (marca != null)
                    {
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le", marca.EnvioMsjBienvenida.ToString());

                        if (marca.EnvioMsjBienvenida && oSocio.Celular != string.Empty)
                            PlusMobile.SendSMSBienvenida(oTarjeta.IDMarca, marca.CostoSMS, marca.MsjBienvenida, oSocio.Celular, oSocio.EmpresaCelular);

                        if (marca.EnvioEmailRegistroSocio && oSocio.Email != string.Empty && !string.IsNullOrEmpty(marca.EmailRegistroSocio))
                        {
                            ListDictionary mensaje = new ListDictionary();
                            string mailHtml = marca.EmailRegistroSocio.Replace("XNOMBREX", oSocio.Nombre).Replace("XAPELLIDOX", oSocio.Apellido);
                            mensaje.Add("<MENSAJE>", mailHtml);
                            bool send = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, mensaje, oSocio.Email, "Bienvenido a " + marca.Nombre);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        int idMarca = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usu.IDMarca;
        }
        int idMultimarca = 0;
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            idMultimarca = usu.IDMultimarca;
        }
        if (idFranquicia > 0 || idMarca > 0 || idMultimarca > 0 || HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                int idSocio = (int)HttpContext.Current.Session["IDSocio"];
                var result = dbContext.Tarjetas.Where(x => x.IDSocio.HasValue && (x.IDSocio.Value == idSocio || x.Socios.IDSocioPadre == idSocio)
                    && (idFranquicia == 0 || x.IDFranquicia == idFranquicia) && (idMarca == 0 || x.IDMarca == idMarca))
                     .OrderBy(x => x.IDSocio)
                     .Select(x => new TarjetasViewModel()
                     {
                         IDTarjeta = x.IDTarjeta,
                         Socio = x.Socios.Apellido + ", " + x.Socios.Nombre,
                         Marca = x.Marcas.Nombre,
                         IDSocio = x.IDSocio.Value,
                         Numero = x.Numero,
                         IDMarca = x.IDMarca,
                         FechaAsignacion = x.FechaAsignacion,
                         FechaVencimiento = x.FechaVencimiento,
                         Estado = x.Estado == "A" ? "Activa" : "Baja",
                         MotivoBaja = x.MotivoBaja,
                         FechaBaja = x.FechaBaja,
                         Puntos = x.PuntosTotales,
                         Credito = x.Credito,
                         Giftcard = x.Giftcard,
                         POS = Math.Round(x.Credito + x.Giftcard),
                         Total = x.Credito + x.Giftcard,
                     }).AsQueryable();

                if (idMultimarca > 0) {
                    var multimarcas = dbContext.MarcasAsociadas.Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();

                    if (multimarcas.Count() > 0)
                        result = result.Where(x => multimarcas.Contains(x.IDMarca));
                    else
                        return null;
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaActividades(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
       
            using (var dbContext = new ACHEEntities())
            {

                string sql = @"SELECT A.Actividad, AN.Nivel, AH.Horario, C.NombreFantasia as Sede, CONVERT(varchar(10), AH.IDHorario) + ' - ' + CONVERT(varchar(10),C.IDComercio) as IDHorarioIDSede
                            FROM Actividades AS A 
                            JOIN ActividadNiveles as AN ON AN.IDActividad = A.IDActividad
                            JOIN ActividadHorarios as AH on AH.IDActividad = A.IDActividad
                            Join ActividadComercio as AC on AC.IDActividad = A.IDActividad
                            Join Comercios as C on C.IDComercio = AC.IDComercio";


                var actividades = dbContext.Database.SqlQuery<ActividadesView>(sql, new object[] { }).AsQueryable();


                return actividades.ToDataSourceResult(take, skip, sort, filter);


        }

    }

    [WebMethod(true)]
    public static void guardarFechaVenc(int id, string fechaVencimiento, string numero)
    {
        using (var dbContext = new ACHEEntities())
        {
            Tarjetas tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == id && x.Numero == numero).FirstOrDefault();
            tarj.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            dbContext.SaveChanges();
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> buscarTarjetas(int idMarca, string tarjeta)
    {
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }
        try
        {
            List<ComboViewModel> list = new List<ComboViewModel>();
            if (idFranquicia > 0 || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Tarjetas.Where(x => !x.IDSocio.HasValue && (idMarca == 0 || x.IDMarca == idMarca) && x.Numero.Contains(tarjeta) && (idFranquicia == 0 || x.IDFranquicia == idFranquicia))
                     .OrderBy(x => x.Numero)
                     .Select(x => new
                     {
                         Numero = x.Numero
                     }).Take(10).ToList();

                    foreach (var tar in aux)
                        list.Add(new ComboViewModel() { ID = tar.Numero, Nombre = tar.Numero });
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #region Grupo Familiar

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrillaAsociados(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            if (HttpContext.Current.Session["IDSocio"] != null)
            {
                int idSocio = (int)HttpContext.Current.Session["IDSocio"];

                using (var dbContext = new ACHEEntities())
                {
                    Socios currentSocio = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
                    if (currentSocio.IDSocioPadre.HasValue)//muestro los socios del grupo familiar de un responsbale
                    {
                        //int idSocioResponsable;
                        //idSocioResponsable = socioAs.IDSocioPadre ?? 0;

                        return dbContext.Socios
                                .OrderBy(x => x.Apellido)
                                .Where(x => x.IDSocioPadre == currentSocio.IDSocioPadre.Value)
                                .Select(x => new SociosViewModel()
                                {
                                    IDSocio = x.IDSocio,
                                    Apellido = x.Apellido + ", " + x.Nombre,
                                    NroDocumento = x.TipoDocumento,
                                }).ToDataSourceResult(take, skip, sort, filter);
                    }
                    else
                    {
                        var aux = dbContext.Socios
                                .OrderBy(x => x.Apellido)
                                .Where(x => x.IDSocioPadre.HasValue && x.IDSocioPadre.Value == idSocio)
                                .Select(x => new SociosViewModel()
                                {
                                    IDSocio = x.IDSocio,
                                    Apellido = x.Apellido + ", " + x.Nombre,
                                    NroDocumento = x.TipoDocumento,
                                }).ToDataSourceResult(take, skip, sort, filter);
                        return aux;
                    }
                }
            }
            else
                return null;
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Desasociar(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                int idSocio = (int)HttpContext.Current.Session["IDSocio"];
                var socio = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
                if (socio.Responsable == true)
                {
                    //Primero elimino socioasociado
                    Socios socioAs = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                    socioAs.IDSocioPadre = null;
                    socioAs.Responsable = null;
                    //Deja de ser responsable si no tiene ningun socio asociado 
                    var sociosAsociados = dbContext.Socios.OrderBy(x => x.Apellido).Where(x => x.IDSocioPadre == idSocio).ToList();
                    if (sociosAsociados.Count() == 1)
                    {
                        socio.Responsable = null;
                    }
                    dbContext.SaveChanges();
                }

            }
        }
    }

    [WebMethod(true)]
    public static int GetIDResponsable(int IDSocio)
    {
        bSocio bSocio = new bSocio();
        Socios oSocio = bSocio.getSocio(IDSocio);
        return oSocio.IDSocioPadre ?? 0;
    }

    [WebMethod(true)]
    public static void Asociar(int idSocioEncontrado, int idSocio)
    {
        using (var dbContext = new ACHEEntities())
        {
            //Grabo que es responsable 
            Socios socio = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
            if (socio.Responsable != true)
                socio.Responsable = true;
            //Grabo el asociado
            Socios socioAs = dbContext.Socios.Where(x => x.IDSocio == idSocioEncontrado).FirstOrDefault();
            socioAs.IDSocioPadre = idSocio;
            socioAs.Responsable = false;
            dbContext.SaveChanges();
        }
    }

    [WebMethod(true)]
    public static SociosViewModel buscar(string documento, int idSocio)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var soc = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
                    if (soc.NroDocumento != documento)
                    {
                        var aux = dbContext.Tarjetas.Include("Socios").Where(x => x.IDSocio.HasValue && x.Socios.NroDocumento == documento && (x.Socios.Responsable != true) && !(x.Socios.IDSocioPadre.HasValue)).FirstOrDefault();
                        if (aux != null)
                        {
                            return new SociosViewModel()
                            {
                                Nombre = aux.Socios.Nombre,
                                Apellido = aux.Socios.Apellido,
                                NroCuenta = aux.Socios.FechaNacimiento.HasValue ? aux.Socios.FechaNacimiento.Value.ToString("dd/MM/yyyy") : "",
                                Sexo = aux.Socios.Sexo,
                                Email = aux.Socios.Email,
                                Celular = aux.Socios.Celular,
                                IDSocio = aux.Socios.IDSocio
                            };
                        }
                        else
                            return null;
                    }
                    else
                        return null;
                }
            }
            else
                return null;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Estadisticas

    [WebMethod(true)]
    public static string obtenerPromedioTicket(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Socios_PromedioTicket " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalTasaUsoMensual(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Socios_TotalTasaUsoMensual '" + fechaDesde + "','" + fechaHasta + "'," + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("#0.00");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalTRMensual(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_TotalTRMensual '" + fechaDesde + "','" + fechaHasta + "'," + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalCanjes(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Socios_TotalCanjes " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerImporteAhorro(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_ImporteAhorro " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerImportePagado(int IDSocio)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {


            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_ImportePagado " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerSaldoActual(int IDSocio)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Socios_SaldoActual " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerPuntosActuales(int IDSocio)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_PuntosActuales " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerCantTR(int IDSocio)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_CantTR " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString();
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerCantComerciosUnicos(int IDSocio)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Socios_CantComerciosUnicos " + IDSocio, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString();
                else
                    html = "0";
            }
        }

        return html;
    }


    #endregion

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {

            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }


    [WebMethod(true)]
    public static void trPuntos(string numeroTarjeta, string idmotivo, decimal puntos,int idSocio) {
        int idTerminal = int.Parse(ConfigurationManager.AppSettings["TransaccionPuntos.IDTerminal"]);

        using (var dbContext = new ACHEEntities()) {
            int idm = int.Parse(idmotivo);
            var motivo = dbContext.Motivos.Where(x => x.IDMotivo == idm).FirstOrDefault();
            if (!motivo.SumaPuntos && puntos<0)// PORQUE TIENEN QUE IR EN POSITIVO 
                puntos = puntos*(-1); 
          
            if (motivo.SumaPuntos)
                ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idTerminal, "POS", "", "", puntos, "", "", "", "", numeroTarjeta, "", "Venta", "000000000000", "1100", "", "", "", motivo.Nombre);
            else
                ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idTerminal, "POS", "", "", puntos , "", "", "", "", numeroTarjeta, "", "Anulacion", "000000000000", "1100", "", "", "", motivo.Nombre);
        }
    }

    [WebMethod(true)]
    public static int puntosPorTarjeta(int idTarjeta) {
          int result=0;
        using (var dbContext = new ACHEEntities()) {
            result = dbContext.Tarjetas.Where(x => x.IDTarjeta == idTarjeta).ToList() != null ? dbContext.Tarjetas.Where(x => x.IDTarjeta == idTarjeta).Sum(x => x.PuntosTotales) : 0;
            
        }
        return result;

    }
}