﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sociose.aspx.cs" Inherits="common_Sociose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

     <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />    
    <style type="text/css">
        #map-canvas {
            height: 280px;
        }
        .multiSelect{
            min-width: 255px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/common/Socios.aspx") %>">Socios</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Socio</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
            <div class="alert alert-info alert-dismissable" id="divInfo" runat="server" visible="false">ATENCIÓN! El socio no tiene tarjetas asignadas</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales"  data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabDomicilio"  class="asociado" data-toggle="tab">Domicilio</a></li>
                    <li><a class="hide" href="#tabTarjetas" data-toggle="tab">Tarjetas</a></li>
                    <li><a href="#tabCostos"  class="admin-franq asociado" data-toggle="tab">Costos</a></li>
                    <li><a class="grupo" href="#tabGrupos"  data-toggle="tab">Grupo familiar</a></li>
                    <li><a href="#tabPlusIN"  data-toggle="tab">PLUS IN</a></li>
                    <li><a class="hide" href="#tabEstadisticas"  data-toggle="tab">Estadisticas</a></li>
                    <li><a  class="hide" onclick="abrirPopUpTR();" href="#tabTr"  data-toggle="tab" style="cursor:pointer">Transacciones</a></li>
                    <li><a href="#tabHobbies"  data-toggle="tab">Hobbies</a></li>
                    <li><a href="#tabProfesion"  data-toggle="tab">Profesion</a></li>
                    <li><a href="#tabActividades" onclick="loadActividades()"  data-toggle="tab">Actividades</a></li>

                </ul>
            </div>
            <form runat="server" id="formSocio" class="form-horizontal" role="form">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMultimarca" Value="0" />
                <div class="tab-content">
                    <div class="tab-pane active" id="tabDatosPrincipales">
                        <br />
                        <div class="form-group hide">
                            <label for="txtNroCuenta" class="col-lg-2 control-label">Nro. Cuenta</label>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtNroCuenta" CssClass="form-control number" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="txtPais" class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                            <div class="col-lg-3">
                                <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Apellido</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ddlDia" class="col-lg-2 control-label"><span class="f_req">*</span> Fecha de Nac.</label>

                            <div class="col-sm-2 col-md-2" style="width: 100px !important">
                                <asp:DropDownList runat="server" ID="ddlDia" CssClass="form-control required">
                                    <asp:ListItem Text="1" Value="1" />
                                    <asp:ListItem Text="2" Value="2" />
                                    <asp:ListItem Text="3" Value="3" />
                                    <asp:ListItem Text="4" Value="4" />
                                    <asp:ListItem Text="5" Value="5" />
                                    <asp:ListItem Text="6" Value="6" />
                                    <asp:ListItem Text="7" Value="7" />
                                    <asp:ListItem Text="8" Value="8" />
                                    <asp:ListItem Text="9" Value="9" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="12" Value="12" />
                                    <asp:ListItem Text="13" Value="13" />
                                    <asp:ListItem Text="14" Value="14" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="16" Value="16" />
                                    <asp:ListItem Text="17" Value="17" />
                                    <asp:ListItem Text="18" Value="18" />
                                    <asp:ListItem Text="19" Value="19" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="21" Value="21" />
                                    <asp:ListItem Text="22" Value="22" />
                                    <asp:ListItem Text="23" Value="23" />
                                    <asp:ListItem Text="24" Value="24" />
                                    <asp:ListItem Text="25" Value="25" />
                                    <asp:ListItem Text="26" Value="26" />
                                    <asp:ListItem Text="27" Value="27" />
                                    <asp:ListItem Text="28" Value="28" />
                                    <asp:ListItem Text="29" Value="29" />
                                    <asp:ListItem Text="30" Value="30" />
                                    <asp:ListItem Text="31" Value="31" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2 col-md-2" style="width: 140px !important">
                                <asp:DropDownList runat="server" ID="ddlMes" CssClass="form-control required">
                                    <asp:ListItem Text="Enero" Value="1" />
                                    <asp:ListItem Text="Febrero" Value="2" />
                                    <asp:ListItem Text="Marzo" Value="3" />
                                    <asp:ListItem Text="Abril" Value="4" />
                                    <asp:ListItem Text="Mayo" Value="5" />
                                    <asp:ListItem Text="Junio" Value="6" />
                                    <asp:ListItem Text="Julio" Value="7" />
                                    <asp:ListItem Text="Agosto" Value="8" />
                                    <asp:ListItem Text="Septiembre" Value="9" />
                                    <asp:ListItem Text="Octubre" Value="10" />
                                    <asp:ListItem Text="Noviembre" Value="11" />
                                    <asp:ListItem Text="Diciembre" Value="12" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2 col-md-2" style="width: 120px !important">
                                <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlAnio">
                                    <asp:ListItem Value="114">2013</asp:ListItem>
                                    <asp:ListItem Value="113">2012</asp:ListItem>
                                    <asp:ListItem Value="112">2011</asp:ListItem>
                                    <asp:ListItem Value="111">2010</asp:ListItem>
                                    <asp:ListItem Value="110">2009</asp:ListItem>
                                    <asp:ListItem Value="109">2008</asp:ListItem>
                                    <asp:ListItem Value="108">2007</asp:ListItem>
                                    <asp:ListItem Value="107">2006</asp:ListItem>
                                    <asp:ListItem Value="106">2005</asp:ListItem>
                                    <asp:ListItem Value="105">2004</asp:ListItem>
                                    <asp:ListItem Value="104">2003</asp:ListItem>
                                    <asp:ListItem Value="103">2002</asp:ListItem>
                                    <asp:ListItem Value="102">2001</asp:ListItem>
                                    <asp:ListItem Value="101">2000</asp:ListItem>
                                    <asp:ListItem Value="100">1999</asp:ListItem>
                                    <asp:ListItem Value="99">1998</asp:ListItem>
                                    <asp:ListItem Value="98">1997</asp:ListItem>
                                    <asp:ListItem Value="97">1996</asp:ListItem>
                                    <asp:ListItem Value="96">1995</asp:ListItem>
                                    <asp:ListItem Value="95">1994</asp:ListItem>
                                    <asp:ListItem Value="94">1993</asp:ListItem>
                                    <asp:ListItem Value="93">1992</asp:ListItem>
                                    <asp:ListItem Value="92">1991</asp:ListItem>
                                    <asp:ListItem Value="91">1990</asp:ListItem>
                                    <asp:ListItem Value="90">1989</asp:ListItem>
                                    <asp:ListItem Value="89">1988</asp:ListItem>
                                    <asp:ListItem Value="88">1987</asp:ListItem>
                                    <asp:ListItem Value="87">1986</asp:ListItem>
                                    <asp:ListItem Value="86">1985</asp:ListItem>
                                    <asp:ListItem Value="85">1984</asp:ListItem>
                                    <asp:ListItem Value="84">1983</asp:ListItem>
                                    <asp:ListItem Value="83">1982</asp:ListItem>
                                    <asp:ListItem Value="82">1981</asp:ListItem>
                                    <asp:ListItem Value="81">1980</asp:ListItem>
                                    <asp:ListItem Value="80">1979</asp:ListItem>
                                    <asp:ListItem Value="79">1978</asp:ListItem>
                                    <asp:ListItem Value="78">1977</asp:ListItem>
                                    <asp:ListItem Value="77">1976</asp:ListItem>
                                    <asp:ListItem Value="76">1975</asp:ListItem>
                                    <asp:ListItem Value="75">1974</asp:ListItem>
                                    <asp:ListItem Value="74">1973</asp:ListItem>
                                    <asp:ListItem Value="73">1972</asp:ListItem>
                                    <asp:ListItem Value="72">1971</asp:ListItem>
                                    <asp:ListItem Value="71">1970</asp:ListItem>
                                    <asp:ListItem Value="70">1969</asp:ListItem>
                                    <asp:ListItem Value="69">1968</asp:ListItem>
                                    <asp:ListItem Value="68">1967</asp:ListItem>
                                    <asp:ListItem Value="67">1966</asp:ListItem>
                                    <asp:ListItem Value="66">1965</asp:ListItem>
                                    <asp:ListItem Value="65">1964</asp:ListItem>
                                    <asp:ListItem Value="64">1963</asp:ListItem>
                                    <asp:ListItem Value="63">1962</asp:ListItem>
                                    <asp:ListItem Value="62">1961</asp:ListItem>
                                    <asp:ListItem Value="61">1960</asp:ListItem>
                                    <asp:ListItem Value="60">1959</asp:ListItem>
                                    <asp:ListItem Value="59">1958</asp:ListItem>
                                    <asp:ListItem Value="58">1957</asp:ListItem>
                                    <asp:ListItem Value="57">1956</asp:ListItem>
                                    <asp:ListItem Value="56">1955</asp:ListItem>
                                    <asp:ListItem Value="55">1954</asp:ListItem>
                                    <asp:ListItem Value="54">1953</asp:ListItem>
                                    <asp:ListItem Value="53">1952</asp:ListItem>
                                    <asp:ListItem Value="52">1951</asp:ListItem>
                                    <asp:ListItem Value="51">1950</asp:ListItem>
                                    <asp:ListItem Value="50">1949</asp:ListItem>
                                    <asp:ListItem Value="49">1948</asp:ListItem>
                                    <asp:ListItem Value="48">1947</asp:ListItem>
                                    <asp:ListItem Value="47">1946</asp:ListItem>
                                    <asp:ListItem Value="46">1945</asp:ListItem>
                                    <asp:ListItem Value="45">1944</asp:ListItem>
                                    <asp:ListItem Value="44">1943</asp:ListItem>
                                    <asp:ListItem Value="43">1942</asp:ListItem>
                                    <asp:ListItem Value="42">1941</asp:ListItem>
                                    <asp:ListItem Value="41">1940</asp:ListItem>
                                    <asp:ListItem Value="40">1939</asp:ListItem>
                                    <asp:ListItem Value="39">1938</asp:ListItem>
                                    <asp:ListItem Value="38">1937</asp:ListItem>
                                    <asp:ListItem Value="37">1936</asp:ListItem>
                                    <asp:ListItem Value="36">1935</asp:ListItem>
                                    <asp:ListItem Value="35">1934</asp:ListItem>
                                    <asp:ListItem Value="34">1933</asp:ListItem>
                                    <asp:ListItem Value="33">1932</asp:ListItem>
                                    <asp:ListItem Value="32">1931</asp:ListItem>
                                    <asp:ListItem Value="31">1930</asp:ListItem>
                                    <asp:ListItem Value="30">1929</asp:ListItem>
                                    <asp:ListItem Value="29">1928</asp:ListItem>
                                    <asp:ListItem Value="28">1927</asp:ListItem>
                                    <asp:ListItem Value="27">1926</asp:ListItem>
                                    <asp:ListItem Value="26">1925</asp:ListItem>
                                    <asp:ListItem Value="25">1924</asp:ListItem>
                                    <asp:ListItem Value="24">1923</asp:ListItem>
                                    <asp:ListItem Value="23">1922</asp:ListItem>
                                    <asp:ListItem Value="22">1921</asp:ListItem>
                                    <asp:ListItem Value="21">1920</asp:ListItem>
                                    <asp:ListItem Value="20">1919</asp:ListItem>
                                    <asp:ListItem Value="19">1918</asp:ListItem>
                                    <asp:ListItem Value="18">1917</asp:ListItem>
                                    <asp:ListItem Value="17">1916</asp:ListItem>
                                    <asp:ListItem Value="16">1915</asp:ListItem>
                                    <asp:ListItem Value="15">1914</asp:ListItem>
                                    <asp:ListItem Value="14">1913</asp:ListItem>
                                    <asp:ListItem Value="13">1912</asp:ListItem>
                                    <asp:ListItem Value="12">1911</asp:ListItem>
                                    <asp:ListItem Value="11">1910</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rdbFem" class="col-lg-2 control-label"><span class="f_req">*</span> Sexo</label>
                            <div class="col-lg-6">
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" ID="rdbFem" Checked="true" GroupName="grpSexo" Text="F" Width="20px" />
                                </label>
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" ID="rdbMas" GroupName="grpSexo" Text="M" Width="20px" />
                                </label>
                                <label class="radio-inline">
                                    <asp:RadioButton runat="server" ID="rdbInd" GroupName="grpSexo" Text="I" Width="20px" />
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ddlTipoDoc" class="col-lg-2 control-label"><span class="f_req">*</span> Tipo y Nro Identidad</label>
                            <div class="col-sm-2 col-md-2">
                                <asp:DropDownList runat="server" class="form-control" ID="ddlTipoDoc">
                                    <asp:ListItem ID="listDNI" Value="DNI" Text="DNI" />
                                    <asp:ListItem ID="listCuit" Value="CUIT" Text="CUIL" />
                                    <asp:ListItem ID="listCedula" Value="Cedula de Identidad" Text="Cedula de Identidad" />
                                    <asp:ListItem ID="listPasaporte" Value="Pasaporte" Text="Pasaporte" />

                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtNroDoc" CssClass="form-control required number" MaxLength="20"></asp:TextBox>
                                <span class="help-block">Si no existe, poner 00</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtTelefono" class="col-lg-2 control-label">Teléfono</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtCelular" class="col-lg-2 control-label">Celular</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtCelular" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtEmpresaCelular" class="col-lg-2 control-label">Empresa Cel.</label>
                            <div class="col-lg-2">
                                <asp:DropDownList runat="server" ID="ddlEmpresaCelular" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="Claro" Value="Claro" />
                                    <asp:ListItem Text="Movistar" Value="Movistar" />
                                    <asp:ListItem Text="Nextel" Value="Nextel" />
                                    <asp:ListItem Text="Personal" Value="Personal" />
                                    <asp:ListItem Text="Otro" Value="Otro" />
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Contraseña</label>
                            <div class="col-lg-4" style="margin-top: 5px;">
                                <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group" id="divUploadFoto" style="display: none">
                            <label class="col-lg-2 control-label">Foto</label>
                            <div class="col-lg-4">
                                <asp:Image runat="server" ID="imgFoto" Style="width: 180px; height: 120px;"></asp:Image>
                                <br />
                                <br />
                            <ajaxToolkit:AsyncFileUpload runat="server" ID="flpFoto" CssClass="form-control" PersistFile="true"
                                    ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                                    ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                    OnUploadedComplete="uploadFoto" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                    <img alt="" src="../../img/ajax_loader.gif" />
                                </asp:Label>
                                <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                            </div>
                            <div class="col-sm-4" runat="server" id="divFoto" visible="false">
                                Actual:
                                <asp:HyperLink runat="server" ID="lnkFoto" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkFotoDelete" Target="_blank">Eliminar</asp:HyperLink>
                            </div>
                        </div>
                            
                        <div class="form-group admin-franq" id="fgSube">
                            <label for="txtNumSUBE" class="col-lg-2 control-label ">Tarjeta SUBE</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtNumSUBE" ClientIDMode="Static" CssClass="form-control" MaxLength="16"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group admin-franq" id="fgMonedero">
                            <label for="txtNumMonedero" class="col-lg-2 control-label">Tarjeta Monedero</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtNumMonedero" ClientIDMode="Static" CssClass="form-control" MaxLength="16"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group admin-franq">
                            <label for="txtNumTarjTransporte" class="col-lg-2 control-label">Numero Transporte</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtNumTarjTransporte" ClientIDMode="Static" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group admin-franq">
                            <label for="txtPatente" class="col-lg-2 control-label">Patente</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtPatente" ClientIDMode="Static" CssClass="form-control" MaxLength="7"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txttwitter" class="col-lg-2 control-label">Twitter</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txttwitter" ClientIDMode="Static" CssClass="form-control" MaxLength="7"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtFacebook" class="col-lg-2 control-label">Facebook</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtFacebook" ClientIDMode="Static" CssClass="form-control" MaxLength="7"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtObservaciones" class="col-lg-2 control-label">Observaciones</label>
                            <div class="col-lg-6">
                                <asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"> Fecha Tope Canje</label>
                            <div class="col-lg-2" >
                            <asp:TextBox runat="server" ID="txtFechaTopeCanje" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"> Fecha Caducidad</label>
                            <div class="col-lg-2">
                            <asp:TextBox runat="server" ID="txtFechaCaducidad" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtImporteTope" class="col-lg-2 control-label">Importe Tope</label>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtImporteTope" CssClass="form-control number" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hfIDSocio" Value="0" />
                        <asp:HiddenField runat="server" ID="hfGrabarDomicilio" Value="0" />
                        <asp:HiddenField runat="server" ID="hfVerTarjetas" Value="0" />
                    </div>
                    <div class="tab-pane" id="tabDomicilio">
                        <br />
                        <div class="form-group">
                            <label for="txtPais" class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" CssClass="form-control required" ID="nomPais" Enabled="false">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group provincia">
                            <label for="txtProvincia" class="col-lg-2 control-label" id="labelProvincia"><span class="f_req">*</span> Provincia</label>
                            <div class="col-lg-3">
                                <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia" onchange="LoadCiudades2(this.value,'ddlCiudad');return false;">
                                        
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group ciudad">
                            <label for="txtCiudad" class="col-lg-2 control-label" id="labelCiudad">Ciudad</label>
                            <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                        data-placeholder="Seleccione una ciudad">
                                        
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtDomicilio" class="col-lg-2 control-label">Domicilio</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtPisoDepto" class="col-lg-2 control-label">Piso/Depto</label>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtCodigoPostal" class="col-lg-2 control-label">Código Postal</label>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtCodigoPostal" CssClass="form-control" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtTelefono" class="col-lg-2 control-label">Teléfono</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtTelefonoDom" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtFax" class="col-lg-2 control-label">Fax</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtFax" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Geolocalizar</label>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtLatitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                <span class="help-block">Latitud</span>
                            </div>
                            <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtLongitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                <span class="help-block">Longitud</span>
                            </div>
                            <a href="javascript:showMap();" id="btnMap" class="btn">Ver mapa</a>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabTarjetas">
                        <br />
                        <div class="alert alert-danger alert-dismissable" id="divErrorTarjetas" style="display: none"></div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="col-lg-2 admin" >
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNuevaTarjeta" CssClass="form-control number" MinLength="5" MaxLength="5"></asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <button id="btnBuscarTarjetas" class="btn" type="button" onclick="buscarTarjetas();">Buscar</button>
                                </div>
                                <div class="col-lg-3" id="divResultado1" style="display: none">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTarjetas">
                                        <asp:ListItem Text="Seleccione una tarjeta" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-4" id="divResultado2" style="display: none">
                                    <button runat="server" id="btnAsociarTarjeta" class="btn btn-success" type="button" onclick="asociarTarjeta();">Asociar</button>
                                    <a id="linkAnular" class="btn btn-success admin" onclick="irTarjetasSustitucion();">Anular/Sustituir Tarjetas</a>

                                </div>

                            </div>
                        </div>
                        <div class="row admin-franq">
                            <div class="col-sm-12 col-md-12">
                                <label class="col-lg-2 control-label label label-default" style="font-size: 13px; text-align: left; padding: 5px;">
                                    Total puntos:
                                    <asp:Literal runat="server" ID="litTotalPuntos"></asp:Literal></label>
                                <label class="col-lg-2 control-label label label-default" style="margin-left: 10px; font-size: 13px; text-align: left; padding: 5px;">
                                    Total crédito: $
                                    <asp:Literal runat="server" ID="litTotalCredito"></asp:Literal></label>
                                <label class="col-lg-2 control-label label label-default" style="margin-left: 10px; font-size: 13px; text-align: left; padding: 5px;">
                                    Total giftcard: $
                                    <asp:Literal runat="server" ID="litTotalGiftcard"></asp:Literal></label>
                                <label class="col-lg-2 control-label label label-default" style="margin-left: 10px; font-size: 13px; text-align: left; padding: 5px;">
                                    Total: $
                                    <asp:Literal runat="server" ID="litTotal"></asp:Literal></label>
                                <label class="col-lg-2 control-label label label-default" style="margin-left: 10px; font-size: 13px; text-align: left; padding: 5px;">
                                    Total POS: 
                                    <asp:Literal runat="server" ID="litTotalPos"></asp:Literal></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">

                                <div id="grid"></div>
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                       
                    <div class="tab-pane admin-franq" id="tabCostos">
                        <br />  
                        <div class="form-group">
                            <label for="txtCostoTransaccional" class="col-lg-2 control-label">Costo transaccional con descuento</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCostoTransaccionalConDescuento" CssClass="form-control" MaxLength="100" Value="0"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtCostoTransaccional" class="col-lg-2 control-label">Costo transaccional compra solo puntos</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCostoTransaccionalSoloPuntos" CssClass="form-control" MaxLength="100" Value="0"></asp:TextBox>

                            </div>
                        </div>
                            <div class="form-group">
                            <label for="txtCostoTransaccional" class="col-lg-2 control-label">Costo transaccional canje</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCostoTransaccionalCanje" CssClass="form-control" MaxLength="100" Value="0"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtCostoSeguro" class="col-lg-2 control-label">Costo de seguro</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCostoSeguro" CssClass="form-control" MaxLength="100"  Value="0"></asp:TextBox>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="txtCostoSMS" class="col-lg-2 control-label">Costo de sms</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCostoSMS" CssClass="form-control" MaxLength="100"  Value="0"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane"  id="tabGrupos">
                        <div class="asociado" id="divAsociarSocios">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                            <asp:HiddenField runat="server" ID="hdnResponsable" Value="-1" ClientIDMode="Static" />
                                            <asp:TextBox runat="server" ID="txtDoc" CssClass="form-control number" MaxLength="20"></asp:TextBox>
                                            <span class="help-block">Nro Documento</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnBuscar" class="btn" type="button" onclick="buscar();" >Buscar</button>
                                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                                    </div>
                                </div>
                            </div>

                            <div id="divSocioEncontrado" style="display:none">
                                <div class="vcard">
					                <ul style="margin: 10px 0 0 -30px">
						                <li class="v-heading">
							                Socio encontrado!
						                </li>
						                <li>
							                <span class="item-key">Nombre</span>
							                <div class="vcard-item" id="lblNombre"></div>
						                </li>
						                <li>
							                <span class="item-key">Apellido</span>
							                <div class="vcard-item" id="lblApellido"></div>
						                </li>
						                <li>
							                <span class="item-key">Email</span>
							                <div class="vcard-item" id="lblEmail"></div>
						                </li>
						                <li>
							                <span class="item-key">Sexo</span>
							                <div class="vcard-item" id="lblSexo"></div>
						                </li>
                                        <li>
							                <span class="item-key">Fecha Nac.</span>
							                <div class="vcard-item" id="lblFechaNac"></div>
						                </li>
                                    </ul>
                                    <br />
                                    <div class="col-sm-8 col-sm-offset-3">
                                        <button runat="server" id="btnAsociar" class="btn btn-success" type="button" onclick="asociar();" >Asociar</button>
                                    </div>
                                    <asp:HiddenField runat="server" ID="hdnIDSocio" Value="0" />
                                </div>
                                <br />
                                <br />
                                <br />
                            </div>

                            <div id="divSocioNoEncontrado" style="display:none">
                                <div class="vcard">
					                <ul style="margin: 10px 0 0 -30px">
						                <li class="v-heading">
							                Socio NO encontrado! 
						                </li>
                                    </ul>
                                </div>
                                <br />
                            </div>
                                <div class="alert alert-success alert-dismissable" id="divAsociadoOk" style="display: none"></div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group nombreResponsable">
                                    <label class="col-lg-2 control-label" style="font-size: 15px; text-align: left; padding: 5px;">
                                        Responsable:
                                        <a href="#" onclick="irAlResponsable();"> <asp:Literal runat="server" ID="litNombreResponsable"></asp:Literal></label> </a>
                                         
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-sm-12 col-md-12">

                                <div id="gridAsociados"></div>
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabPlusIN">
                        <br />
                           
                           
                        <div class="form-group">
                            <label for="txtBanco" class="col-lg-2 control-label">Banco</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtBanco" ClientIDMode="Static" CssClass="form-control" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ddlTipoCuenta" class="col-lg-2 control-label"><span class="f_req">*</span> Tipo Cuenta</label>
                            <div class="col-lg-3">
                                <asp:DropDownList ClientIDMode="Static" runat="server" CssClass="form-control required" ID="ddlTipoCuenta">
                                    <asp:ListItem Value="CuentaCorriente" Text="Cuenta Corriente"></asp:ListItem>
                                        <asp:ListItem Value="CajaDeAhorro" Text="Caja De Ahorro"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtNroCuentaPLUSIN" class="col-lg-2 control-label">Nro. Cuenta</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtNroCuentaPLUSIN" CssClass="form-control number" MaxLength="100"></asp:TextBox>
                            </div>
                        </div> <div class="form-group">
                            <label for="txtCBU" class="col-lg-2 control-label">CBU</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtCBU"  ClientIDMode="Static" CssClass="form-control" MaxLength="22"></asp:TextBox>
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Repetir CBU</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFormaPago_CBU_Rep" ClientIDMode="Static" CssClass="form-control range" equalTo="#txtCBU" MaxLength="22" Minlength="22"></asp:TextBox>
                                </div>
                            </div>
                            
                        <div class="form-group">
                            <label for="txtEmailPlusIn" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-4">
                                <asp:TextBox runat="server" ID="txtEmailPlusIn" ClientIDMode="Static" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                            </div>
                        </div>  

                    </div>
                    <div class="tab-pane" id="tabEstadisticas">
                        <br />
                            <div class="row ">
		                    <div class="col-sm-12 tac">
			                    <h3 ><asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
                                <ul class="ov_boxes">
				                    <li>
					                    <div class="p_bar_up p_canvas">
                                            <img src="../../img/dashboard/money-graph.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_ticketpromedio">Calculando...</strong>
						                    Promedio ticket <br />
					                    </div>
				                    </li>
                                        <li>
                                            <div class="p_bar_up p_canvas">
                                            <img src="../../img/dashboard/exchange.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_canjeado">Calculando...</strong>
						                    Canjeado <br />
					                    </div>
				                    </li>

				                    <li>
					                    <div class="p_line_up p_canvas">
                                                <img src="../../img/dashboard/Tracking_3D.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_tasauso_mensual">Calculando...</strong>
                                            Tasa de uso <br />  mes actual
					                    </div>
				                    </li>
				                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/transaction.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_tr_mensual">Calculando...</strong>
						                    Cantidad de trans. <br /> mes actual
					                    </div>
				                    </li>
                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/emblem-money.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_importe">Calculando...</strong>
						                    Importe <br />pagado
					                    </div>
				                    </li>
                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/billete.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_importeAhorro">Calculando...</strong>
						                    Importe <br />ahorro
					                    </div>
				                    </li>

                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/money2.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_saldoActual">Calculando...</strong>
						                    Saldo <br />actual
					                    </div>
				                    </li>

                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/coins.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_puntosActuales">Calculando...</strong>
						                    Total <br />puntos actuales
					                    </div>
				                    </li>

                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/transaction.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_cantTR">Calculando...</strong>
						                    Cantidad <br />Transacciones
					                    </div>
				                    </li>

                                    <li>
					                    <div class="p_line_down p_canvas">
                                            <img src="../../img/dashboard/1396123817_banker.png" style="width:48px" />
					                    </div>
					                    <div class="ov_text">
						                    <strong id="resultado_cantComerciosUnicos">Calculando...</strong>
						                    Cantidad <br />comercios únicos
					                    </div>
				                    </li>
			                    </ul>
		                    </div>
	                    </div>
                    </div>
                        
                    <div class="tab-pane" id="tabTr">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <button runat="server" id="Button2" class="btn btn-success" type="button"  onclick="$('#modalDetalleTr').modal('show');">Listado Transacciones</button>                             

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2" >
                                <button runat="server" id="Button1" class="btn" type="button"  onclick="$('#modalDetalleTrPuntos').modal('show');">Ajustar Puntos</button>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabHobbies">
                        <div class="form-group"  runat="server">
                            <label class="col-lg-2 control-label">Hobbies</label>
                            <div class="col-lg-9">
                                <asp:DropDownList CssClass="multiSelect" runat="server" ID="searchableHobbies" multiple="multiple" ClientIDMode="Static" DataTextField="Nombre" DataValueField="IDHobbie" />
                                <br />
                                <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabProfesion">
                       <div class="form-group">
                            <label for="txtProfesion" class="col-lg-2 control-label">Profesión</label>
                            <div class="col-lg-2">
                                <asp:DropDownList runat="server" ID="ddlProfesion" CssClass="form-control">
                                </asp:DropDownList>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane tabActividades" id="tabActividades">
                      
                        <div class="form-group">
                            <label for="txtFax" class="col-lg-2 control-label">Beneficio</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="searchBeneficio" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>

                        <div id="grillaActividades">



                        </div>
                    
                    
                    
                    
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                            <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                            <a href="Socios.aspx" class="btn btn-link">Cancelar</a>
                        </div>
                    </div>
               </div>
                <div runat="server" class="modal fade" id="modalDetalleTrPuntos">
	                <div class="modal-dialog  modal-lg">
		                <div class="modal-content">
			                <div class="modal-header">
                 
				                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                <h3 class="modal-title" id="titTrPuntos">Ajustar Puntos</h3>
                    
			                </div>
                            <div class="alert alert-danger alert-dismissable" id="div3" style="display: none"></div>
			                <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="divErrorTrPuntos" style="display: none"></div>
                                <div class="alert alert-success alert-dismissable" id="divOkTrPuntos" style="display: none">Los datos se han actualizado correctamente</div>
				                <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                                <div class="form-group">
                                    <label for="ddlTipoCuenta" class="col-lg-2 control-label"><span class="f_req">*</span> Tarjeta</label>
                                    <div class="col-lg-3">
                                        <asp:DropDownList ClientIDMode="Static" runat="server" CssClass="form-control " ID="ddlTarjetas2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ddlTipoCuenta" class="col-lg-2 control-label"><span class="f_req">*</span> Motivo</label>
                                    <div class="col-lg-3">
                                        <asp:DropDownList ClientIDMode="Static" runat="server" CssClass="form-control " ID="ddlMotivos">
                                        </asp:DropDownList>
                                    </div>
                                </div>	 
                                <div class="form-group">
                                    <label for="txtPuntos" class="col-lg-2 control-label"><span class="f_req">*</span>Puntos a transferir</label>
                                    <div class="col-lg-3">
                                        <asp:TextBox runat="server" ID="txtPuntos"  ClientIDMode="Static" CssClass="form-control   number" MaxLength="22"></asp:TextBox>
                                    </div>
                                </div>                                               	
			                </div>
         
			                <div class="modal-footer">
                                <button class="btn btn-success" id="Button3" onclick="trPuntos();return false">Guardar</button>
				                <button type="button" class="btn btn-default" onclick="$('#modalDetalleTrPuntos').modal('hide');">Cerrar</button>
			                </div>
		                </div>
	                </div>
                </div>
            </form>          
                
        </div>
    </div>

  <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog  modal-lg">
			<div class="modal-content">
				<div class="modal-header">
                 
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
                    
				</div>
                <div class="alert alert-danger alert-dismissable" id="divErrorTr" style="display: none"></div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha</th> 
                                <th>Hora</th> 
                                <th>Operacion</th> 
                                <th>SDS</th> 
                                <th>Comercio</th> 
                                <th>Marca</th> 
                                <th>Tarjeta</th> 
                                <th>Establecimiento</th> 
                                <th>$ Original</th> 
                                <th>$ Ahorro</th> 
                                <th>Puntos</th> 
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
							
						</tbody>
					</table>
				</div>
         
				<div class="modal-footer">
                      <button class="btn btn-success" type="button" id="btnExportarTr" onclick="exportarTr();">Exportar a Excel</button>
                      <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingTr" style="display:none" />
                      <a href="" id="lnkDownloadTr" download="TrSocios" style="display:none">Descargar</a>
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>



     <div class="modal fade" id="modalCargarFechaVencimiento">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Edicion FechaVencimiento</h4>
                        <input type="hidden" id="hdnID" value="0" />
                          <input type="hidden" id="hdnNumero" value="0" />
                        
                    </div>
                    <div class="modal-body">
                            <div class="container">
                                   <form id="formEdicion">
                                   <div class="row">                                
                                       <div class="col-sm-6">
                                            <label class="col-lg-4 control-label"><span class="f_req">*</span> Fecha vencimiento</label>
                                            <div class="col-lg-6">
                                                <input type="text" id="txtFechaVencimiento" class="form-control required validDate" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnGuardar" onclick="guardarFechaVencimiento();">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle">Mapa</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var drag = false;
        var infowindow;

        function computepos(point) {
            $("#txtLatitud").val(point.lat().toFixed(6));
            $("#txtLongitud").val(point.lng().toFixed(6));
        }

        function drawMap(latitude, longitude) {
            var point = new google.maps.LatLng(latitude, longitude);

            var mapOptions = {
                zoom: 14,
                center: point,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: point
            })
            marker.setMap(map);
            map.setCenter(point);

            google.maps.event.addListener(map, 'click', function (event) {
                if (drag) { return; }
                if (map.getZoom() < 10) { map.setZoom(10); }
                map.panTo(event.latLng);
                computepos(event.latLng);
            });

            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "</p></div>";

                infowindow = new google.maps.InfoWindow({ content: html });
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(marker, 'dragstart', function () { if (infowindow) { infowindow.close(); } });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                //if (map.getZoom() < 10) { map.setZoom(10); }
                map.setCenter(event.latLng);
                computepos(event.latLng);
                drag = true;
                setTimeout(function () { drag = false; }, 250);
            });

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(point);
            });

            $("#myMapModal").modal("show");
        }

        function showMap() {
            var embAddr = $("#txtDomicilio").val() + ',' + $("#ddlCiudad option:selected").text() + ',' + $("#ddlProvincia option:selected").text();
            $("#modalTitle").html(embAddr);

            if ($("#txtLatitud").val() == "" && $("#txtLongitud").val() == "") {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': embAddr }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        drawMap(latitude, longitude);

                        $("#txtLatitud").val(latitude);
                        $("#txtLongitud").val(longitude);
                    }
                    else {
                        $("#map-canvas").html('Could not find this location from the address given.<p>' + embAddr + '</p>');
                    }
                });
            }
            else {
                var latitude = $("#txtLatitud").val();
                var longitude = $("#txtLongitud").val();

                drawMap(latitude, longitude);
            }
        };
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">   
      <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/sociosEdicion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/nomenclaturas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
     <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvucvVugcM6B5-rcps4y3QAAQQOAwPivI"></script>
</asp:Content>