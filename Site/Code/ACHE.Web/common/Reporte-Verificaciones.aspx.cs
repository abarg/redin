﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;


public partial class common_Reporte_Verificaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
            pnlFranquicias.Visible = false;
        }
        else if (HttpContext.Current.Session["CurrentUser"] != null)
            cargarFranquicias();
        else
            Response.Redirect("/login.aspx");
    }
    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void cargarFranquicias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var franquicias = dbContext.Franquicias.ToList();
            if (franquicias != null && franquicias.Count() > 0)
            {
                ddlFranquicias.DataSource = franquicias;
                ddlFranquicias.DataValueField = "IDFranquicia";
                ddlFranquicias.DataTextField = "NombreFantasia";
                ddlFranquicias.DataBind();
                ddlFranquicias.Items.Insert(0, new ListItem("Todas las franquicias", "0"));
            }
        }
    }
    [System.Web.Services.WebMethod]
    public static void Delete(string nroReferencia)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var verif = dbContext.VerificacionesPOSComercios.Where(x => x.NroReferencia == nroReferencia);

                    if (verif != null)
                    {
                        dbContext.VerificacionesPOSComercios.RemoveRange(verif); 
                    }
                    dbContext.SaveChanges();
                   
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        int idfranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idfranquicia = usu.IDFranquicia;
        }
        if (HttpContext.Current.Session["CurrentUser"] != null || idfranquicia>0)
        {
            using (var dbContext = new ACHEEntities())
            {

                var results = dbContext.ReporteVerificacionesPOSView.OrderBy(x=>x.Fecha).ToList();
                if (fechaDesde != "")
                    results = results.Where(x => x.Fecha >= DateTime.Parse(fechaDesde)).OrderBy(x => x.Fecha).ToList();

                if (fechaHasta != "")
                    results = results.Where(x => x.Fecha <= DateTime.Parse(fechaHasta)).OrderBy(x => x.Fecha).ToList();

                if (idfranquicia>0){
                    results = results.Where(x => x.IDFranquicia == idfranquicia).OrderBy(x => x.Fecha).ToList();
                }

                var info = results
                        .Select(x => new ReporteVerificacionPOSViewModel()
                        {
                            Estado = x.Estado ,
                            Provincia = x.Provincia,
                            Localidad = x.Ciudad,
                            Zona = x.Zona,
                            CantidadComerciosSeleccionados = x.CantidadComercios ?? 0,
                            NumeroPlanilla = x.NroReferencia,
                            Franquicia = x.Franquicia,
                            IDFranquicia=x.IDFranquicia,
                            Fecha = x.Fecha.Value.ToString("dd/MM/yyyy")
                        }).AsQueryable().ToDataSourceResult(take, skip, sort, filter);
                return info;
              
            }
        }
        else
            return null;
    }

}