﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LiquidacionDetallePorTipo.aspx.cs" Inherits="common_LiquidacionDetallePorTipo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#" onclick="irAlDetalle();">Liquidación</a></li>
            <li class="last">Detalle</li>
        </ul>
    </div>
    <div class="row">
        <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
        <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
    </div>
      
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading"><asp:Literal runat="server" ID="litConcepto"></asp:Literal></h3>
            <br />
            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
            <a href="" id="lnkDownload" download="Liquidaciones" style="display:none">Descargar</a>
        </div>
    </div>
    <br/> 
    <form id="formAddItem" runat="server" >
          <asp:HiddenField runat="server" value="0" ID="hdfIDLiquidacion" /> 
          <asp:HiddenField runat="server" value="0" ID="hfIDLiquidacionDetalle" /> 
          <asp:HiddenField runat="server" value="" ID="hdfConcepto" />
          <asp:Panel ID="pnlFormLiqDetalle" Visible="false" runat="server">
              <h3 class="heading">Agregar item</h3>
              <div class="alert alert-danger alert-dismissable" id="divError2" style="display: none"></div>
              <div class="alert alert-success alert-dismissable" id="divOK2" style="display: none">Se ha agregado correctamente</div>
              <div class="formSep col-sm-12 col-md-12">
		 	    <div class="row">
                      <div class="col-sm-2">
                          <label><span class="f_req">*</span>Importe original</label>
                           <asp:TextBox runat="server" ID="txtImporteOriginal" CssClass="form-control required number" MaxLength="50"></asp:TextBox>
                      </div>
                      <div class="col-sm-2">
                          <label>Sub concepto</label>
                          <asp:TextBox runat="server" ID="txtSubConcepto" CssClass="form-control" MaxLength="50"></asp:TextBox>

                      </div>
                      <div class="col-sm-2">   <br />  
                          <button class="btn btn-success" type="button" id="btnAgregarItem" onclick="agregarItem();">Agregar</button>
                      </div>
		 	    </div>
              </div>
        </asp:Panel>   
    </form>
    <div class="row">
        <div class="col-sm-12 col-md-12" id="tblLiquidacionDetalle">
            <table class="table table-striped table-bordered mediaTable" >
				<thead>
					<tr>
						<th class="essential persist">Nº</th>
                        <asp:Panel runat="server" ID="pnlComercio" Visible="false">
						    <th class="optional">Comercio</th>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlSubConcepto" Visible="false">
						    <th class="optional">Sub concepto</th>
                        </asp:Panel>
                        <th class="essential">Importe Original</th>
                        <th class="optional">Importe Total</th>
                        <asp:Panel runat="server" ID="pnlVerTR" Visible="false">
                            <th class="optional">Ver</th>
                        </asp:Panel>
                        <!--   <th class="optional">Observaciones</th>-->
					</tr>
				</thead>
				<tbody id="bodyLiquidacionDetalle">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>
            <asp:Panel runat="server" ID="pnlGrabar" Visible="false"  class="row">
                <div class="col-sm-8 col-sm-md-8">
                    <button class="btn btn-success" type="button" id="btnGrabar" onclick="grabar();">Grabar </button>
                </div>
            </asp:Panel>
              
        </div>
    </div>

   
    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
                    <input type="hidden" id="hdnIDLiq" />
				</div>

				<div class="modal-body">
                    <div class="alert alert-danger alert-dismissable" id="divErrorTR" style="display: none"></div>
                    <div class="alert alert-success alert-dismissable" id="divOkTR" style="display: none">Los datos se han actualizado correctamente.</div>
                    <button class="btn btn-success" type="button" id="btnExportarTR" onclick="exportarTR();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingTR" style="display:none" />
                    <a href="" id="lnkDownloadTR" download="Liquidaciones" style="display:none">Descargar</a>
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha</th> 
                                <th>Hora</th> 
                                <th>Operacion</th> 
                                <th>SDS</th> 
                                <th>$ Original</th> 
                                <th>$ Ahorro</th> 
                                <th>Puntos</th> 
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/liquidacion-detalle-tipo.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

