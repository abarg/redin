﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ActividadesSecretariasE : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["IDSecretaria"]))
        {
            try
            {
                int idSecretaria = int.Parse(Request.QueryString["IDSecretaria"]);
                if (idSecretaria > 0)
                {
                    this.hdnIDSecretaria.Value = idSecretaria.ToString();
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDSecretaria"]));
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ActividadesSecretarias.aspx");
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.ActividadSecretariaCategoria.Where(x => x.IDSecretaria == id).FirstOrDefault();
                if (entity != null)
                {

                    this.txtSecretaria.Text = entity.Nombre;

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod(true)]
    public static string grabar(int idSecretaria, string Nombre)
    {
        try
        {

            //Categoria Beneficio
            ActividadSecretariaCategoria entity;
            using (var dbContext = new ACHEEntities())
            {
                if (idSecretaria > 0)
                    entity = dbContext.ActividadSecretariaCategoria.FirstOrDefault(s => s.IDSecretaria == idSecretaria);
                else
                    entity = new ActividadSecretariaCategoria();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";

                if (idSecretaria > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ActividadSecretariaCategoria.Add(entity);
                    dbContext.SaveChanges();
                }

            }

            return entity.IDSecretaria.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


}