﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class common_ManualAyuda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var seccion = Request.QueryString["seccion"];
        var articulo = Request.QueryString["articulo"];
        var query = Request.QueryString["search"];
        if (seccion != null){
            int idTitulo = int.Parse(seccion);
            CargarSubtitulos(idTitulo);
            pnlSubTitulos.Visible = true;
        }
        else if (articulo != null)
        {
            int idArticulo = int.Parse(articulo);
            CargarArticulo(idArticulo);
            pnlArticulo.Visible = true;
        }
        else if (query != null)
        {
            CargarBusqueda(query);
            pnlBusqueda.Visible = true;
        }
        else{
            CargarTitulos();
            pnlTitulos.Visible = true;
        }
    }
    private void CargarBusqueda(string query)
    {
        using (var dbContext = new ACHEEntities())
        {
            var perfil = getPerfil();
            var articulos = dbContext.ArticulosManualAyuda.Where(x => (perfil == "" || x.ManualAyuda.Perfil == perfil) && (x.Tags.ToLower().Contains(query.ToLower())) || (x.Titulo.ToLower().Contains(query.ToLower()))).ToList();
            var html = "";
            
            foreach(var articulo in articulos){
                html += "<div class='search_item clearfix'><div class='search_content' style='padding-left: 0px; '><h4>";
                html += "<a href='ManualAyuda.aspx?articulo=" + articulo.IDArticulosManualAyuda + "' class='sepV_a'>" + articulo.Titulo + "</a></h4>";
                var descr = articulo.HTML.CleanHtmlTags().ToString();
                if (descr.Length > 500)
                    html += "<p class='sepH_b item_description'>"+descr.Substring(1,500)+"...</p>";
                else
                    html += "<p class='sepH_b item_description'>" + descr.ToString()+ "</p>";
                var tags = articulo.Tags.Split(',');
                var htmlTags="";
                bool first = true;
                foreach(var tag in tags){
                    if (first)
                    {
                        htmlTags += " <small> <a href='ManualAyuda.aspx?search="+tag+"'>" + tag + "</a>  </small>";
                        first = false;
                    }
                    else
                        htmlTags += "  <small> -  <a href='ManualAyuda.aspx?search=" + tag + "'>" + tag + "</a>   </small>";
                }
                html += htmlTags;
                html += "</div></div>";

            }
            spnHTMLArticulos.InnerHtml = html;
            litquery.Text = query;
        }
    }

    private void CargarTitulos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var perfil = getPerfil();
            var titulos = dbContext.ManualAyuda.Where(x => perfil == "" || x.Perfil == perfil).OrderBy(x => x.Orden).ToList();
            var html="";
            foreach(var titulo in titulos){
                html+="<section class='section'> <h3>";
                html += "<a href='ManualAyuda.aspx?seccion=" + titulo.IDManualAyuda + "'>" + titulo.Titulo + "</a>";         
                html +="</h3><ul class='article-list'>";
                var htmlSub="";
                foreach(var articulo in titulo.ArticulosManualAyuda){
                   htmlSub += "<li>";
                   htmlSub += "<a href='ManualAyuda.aspx?articulo=" + articulo.IDArticulosManualAyuda + "'>" + articulo.Titulo + "</a>";
                   htmlSub += "</li>";
               }
                html+=htmlSub;
                html +="</ul></section>";
            }
            litHTMLTitulos.Text = html;
        }
    }
   
    private void CargarSubtitulos(int idTitulo)
    {
        using (var dbContext = new ACHEEntities())
        {
            var articulos = dbContext.ArticulosManualAyuda.Where(x => x.IDManualAyuda == idTitulo).ToList();
            litTitulo.Text  = (articulos.FirstOrDefault()!=null)?articulos.FirstOrDefault().ManualAyuda.Titulo:"";
            litTitulo2.Text = (articulos.FirstOrDefault()!=null)?articulos.FirstOrDefault().ManualAyuda.Titulo:"No contiene ningun articulo";

            var html = "";
            foreach (var articulo in articulos)
            {
                html += " <li >";
                html += "  <a href='ManualAyuda.aspx?articulo=" + articulo.IDArticulosManualAyuda + "'>" + articulo.Titulo+ "</a>";
                html += " </li >";
            }
            litHTMLSubtitulos.Text = html;
        }
    }

    private void CargarArticulo(int idArticulo)
    {
        using (var dbContext = new ACHEEntities())
        {
            var articulo = dbContext.ArticulosManualAyuda.Where(x => x.IDArticulosManualAyuda == idArticulo).FirstOrDefault();
            var breadcrumb = "";
            breadcrumb += "<li title='Ayuda'><a href='ManualAyuda.aspx'>Manual de ayuda</a></li>";
            breadcrumb += "<li title='Ayuda'><a href='ManualAyuda.aspx?seccion=" + articulo.IDManualAyuda + "'>"+articulo.ManualAyuda.Titulo+"</a></li>";
            breadcrumb += "<li title='Ayuda'><a href='#'>" + articulo.Titulo + "</a></li>";

            litBreadCrumbsArticulo.Text = breadcrumb;
            litUsuario.Text = articulo.Usuarios.Nombre;
            litFecha.Text = articulo.Fecha.ToString();
            litTituloArticulo.Text = articulo.Titulo;
            spnHTML.InnerHtml = articulo.HTML;
        }
    }

    private string getPerfil()
    {
        string perfil="";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            perfil = "Franquicias";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            perfil = "Marcas";

        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
            perfil = "Empresas";

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            perfil = "Comercios";
        return perfil;
    }


    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";


        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
            MasterPageFile = "~/MasterPageEmpresas.master";

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            MasterPageFile = "~/MasterPageComercios.master";
    }
}