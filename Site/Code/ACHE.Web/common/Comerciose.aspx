﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Comerciose.aspx.cs" Inherits="common_Comerciose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
  <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/common/Comercios.aspx") %>">Comercios</a></li>
               <li>Edición</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Comercios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
            <div class="tab-content" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="nav-link active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li class="nav-link"><a href="#tabFormasPago" data-toggle="tab">Forma de Pago</a></li>
                    <li class="nav-link"><a href="#tabContacto" data-toggle="tab">Contacto</a></li>
                    <li class="nav-link"><a href="#tabDomicilioC" data-toggle="tab">Dom. Comercial</a></li>
                    <li class="nav-link"><a href="#tabDomicilioF" data-toggle="tab">Dom. Fiscal</a></li>
                    <li class="nav-link"><a href="#tabFacturacion" data-toggle="tab">Facturación</a></li>
                    <li class="nav-link"><a href="#tabUsuarios" class="hide" data-toggle="tab">Usuarios</a></li>
                    <li class="nav-link"><a href="#tabAlertas" data-toggle="tab">Alertas</a></li>
                    <li class="nav-link"><a href="#tabImagenes" data-toggle="tab">Imagenes</a></li>
                </ul>
                <form runat="server" id="formComercio" class="form-horizontal" role="form">
                    <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de Carga</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFechaAlta" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req"></span> SDS</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtSDS" CssClass="form-control required number" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" id="labelRazon"><span class="f_req">*</span> Razón Social</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control required" MaxLength="128"></asp:TextBox>
                                    <asp:CheckBox runat="server" ID="chkCasaMatriz" />&nbsp;Casa Matriz
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre de Fantasia</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombreFantasia" CssClass="form-control required" MaxLength="150"></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre del Establecimiento</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombreEst" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" id="divUploadLogo" style="display: none">
                                <label class="col-lg-2 control-label">Logo</label>
                                <div class="col-lg-4">
                                    <asp:Image runat="server" ID="imgLogo" Style="width: 180px; height: 120px;"></asp:Image>
                                    <br />
                                    <br />

                                   <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                                        ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                                        ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                        OnUploadedComplete="uploadLogo" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                    <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                        <img alt="" src="../../img/ajax_loader.gif" />
                                    </asp:Label>
                                    <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                                </div>
                                <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                                    Actual:
                                    <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                </div>
                            </div>

                            <div  class="form-group" >
                                <label class="col-lg-2 control-label">Franquicia</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Marca</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas">
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Tipo y Nro Identidad </label>
                                <div class="col-sm-2 col-md-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTipoDoc">
                                        <asp:ListItem ID="listCuit" Value="CUIT" Text="CUIT" />
                                        <asp:ListItem ID="listDNI" Value="DNI" Text="DNI" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNroDocumento" CssClass="form-control required number" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Rubro</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" ID="ddlRubro" class="form-control chosen" data-placeholder="Seleccione un Rubro" ClientIDMode="Static" onchange="cargarSubRubros();">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Sub Rubro</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" ID="ddlSubRubro" class="form-control chosen">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtCelular" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Responsable</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtResponsable" CssClass="form-control" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cargo</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCargo" CssClass="form-control" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Web</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtWeb" CssClass="form-control" MaxLength="225"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email required" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Facebook</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtUrl" CssClass="form-control" MaxLength="256"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Twitter</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTwitter" CssClass="form-control" MaxLength="256"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Dealer</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlDealer">
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de Alta</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFechaAltaDealer" CssClass="form-control validDate"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" id="divUploadFicha" style="display: none">
                                <label class="col-lg-2 control-label">Ficha de Alta</label>
                                <div class="col-lg-4">
                                    <asp:Image runat="server" ID="imgFicha" Style="width: 180px; height: 120px;"></asp:Image>
                                    <br />
                                    <br />

                                      <ajaxToolkit:AsyncFileUpload runat="server" ID="flpFichaAlta" CssClass="form-control" PersistFile="true"
                                        ThrobberID="throbberFicha" OnClientUploadComplete="UploadCompleted" Width="200px"
                                        ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                        OnUploadedComplete="uploadFicha" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                    <asp:Label runat="server" ID="throbberFicha" Style="display: none;">
                                        <img alt="" src="../../img/ajax_loader.gif" />
                                    </asp:Label>
                                    <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                                </div>
                                <div class="col-sm-4" runat="server" id="divFicha" visible="false">
                                    Actual:
                                    <asp:HyperLink runat="server" ID="lnkFicha" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkFichaDelete" Target="_blank">Eliminar</asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Activo</label>
                                <div class="col-lg-4">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox runat="server" ID="chkActivo" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtObservaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>

                            <%--<asp:HiddenField runat="server" ID="hfGrabarDomicilio" Value="0" />
                            <asp:HiddenField runat="server" ID="hfGrabarContacto" Value="0" />--%>
                        </div>

                        <div class="tab-pane" id="tabFormasPago">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tipo</label>
                                <div class="col-lg-12">
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdbFormaPago_Debito" GroupName="grpFormaPago" Checked="false" Text="Débito Bancario" Width="100px" />
                                    </label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdbFormaPago_Tarjeta" GroupName="grpFormaPago" Checked="false" Text="Tarjeta: Débito/Crédito" Width="150px" />
                                    </label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdbFormaPago_MercadoPago" GroupName="grpFormaPago" Checked="false" Text="Mercado Pago" Width="150px" />
                                    </label>
                                </div>
                            </div>

                            <div runat="server" id="divFormaPago_Debito">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Banco</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_Banco" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tipo Cuenta</label>
                                    <div class="col-lg-4">
                                        <asp:DropDownList runat="server" class="form-control" ID="ddlFormaPago_TipoCuenta">
                                            <asp:ListItem Value="1" Text="Caja de Ahorro" />
                                            <asp:ListItem Value="2" Text="Cuenta Corriente" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nro. de Cuenta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_NroCuenta" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">CBU</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CBU" CssClass="form-control range" MaxLength="22" Minlength="22"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Repetir CBU</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CBU_Rep" CssClass="form-control range" equalTo="#txtFormaPago_CBU" MaxLength="22" Minlength="22"></asp:TextBox>
                                    </div>
                                </div>



                            </div>

                            <div runat="server" id="divFormaPago_Tarjeta" style="display: none;">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tarjeta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_Tarjeta" CssClass="form-control" MaxLength="23"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Banco emisor</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_BancoEmisor" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nro. Tarjeta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_NroTarjeta" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Fecha Vto. (mm/aaaa)</label>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFormaPago_FechaVto" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Cod. de Seguridad</label>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CodigoSeg" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tabContacto">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Nombre" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Apellido</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Apellido" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">DNI</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtContacto_Documento" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cargo</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Cargo" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtContacto_Telefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtContacto_Celular" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Email" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Observaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabDomicilioF">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="labelProvincia2" class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia" onchange="LoadCiudades2(this.value,'ddlCiudad');">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="labelCiudad2" class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                        data-placeholder="Seleccione una ciudad">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fax</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFax" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabDomicilioC">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                     <asp:TextBox ID="nomPais" runat="server" CssClass="form-control required" Enabled="false">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="labelProvincia" class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList  runat="server" CssClass="form-control required" ID="ddlProvincia2" onchange="LoadCiudades2(this.value,'ddlCiudad2');">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="labelCiudad" class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad2" onchange="LoadZonas(this.value,'cmbZona');"
                                        data-placeholder="Seleccione una ciudad">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio2" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto2" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal2" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom2" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fax</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFax2" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Geolocalizar</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLatitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Latitud</span>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLongitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Longitud</span>
                                </div>
                                <a href="javascript:showMap();" id="btnMap" class="btn">Ver mapa</a>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Zona</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="cmbZona" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tabFacturacion">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Condición Iva</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlIVA">
                                        <asp:ListItem Value="RI" Text="Resp. Inscripto" />
                                        <asp:ListItem Value="MONOTRIBUTISTA" Text="Monotributista" />
                                        <asp:ListItem Value="EX" Text="Exento/No Resp." />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Email para envíos de fc</label>
                                <div class="col-lg-6">
                                    <asp:TextBox runat="server" ID="txtEnvioFc" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    <span class="help-block">Puede ingresar más de una dirección separados mediante ";"</span>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-3 control-label">Costo Fijo</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtCostoFijo" CssClass="form-control"  MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-3 control-label">Tope Venta</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtTopeVenta" CssClass="form-control"  MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="form-group hide">
                                 <label class="col-lg-3 control-label">Costo POS Propio</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtCostoPOSPropio" CssClass="form-control" value="0"  MaxLength="6"></asp:TextBox>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                 <label class="col-lg-3 control-label">Con Retenciones</label>
                                <div class="col-lg-1">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox runat="server" ID="chkRetencion" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabUsuarios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorUsuario" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Usuario</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:TextBox runat="server" ID="txtEmailUsuarioComercio" CssClass="form-control email" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Email</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MinLength="3" MaxLength="10"></asp:TextBox>
                                        <span class="help-block">Contraseña</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:DropDownList runat="server" ID="ddlTipoUsuario" CssClass="form-control">
                                            <asp:ListItem Text="Admin" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Backoffice" Value="B"></asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block">Tipo</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarUsuario" class="btn" type="button" onclick="agregarUsuario();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div id="grid"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hfIDUsuario" Value="0" />
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tabAlertas">
                            <br />
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email </label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:TextBox runat="server" ID="txtEmailAlerta" CssClass="form-control email"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular </label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:TextBox runat="server" ID="txtCelularAlerta" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular empresa </label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="txtCelularEmpresaAlerta" CssClass="form-control">
                                        <asp:ListItem Text="" Value="" />
                                        <asp:ListItem Text="Claro" Value="Claro" />
                                        <asp:ListItem Text="Movistar" Value="Movistar" />
                                        <asp:ListItem Text="Nextel" Value="Nextel" />
                                        <asp:ListItem Text="Personal" Value="Personal" />
                                        <asp:ListItem Text="Otro" Value="Otro" />
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tabImagenes">
                            <asp:HiddenField runat="server" ID="hdnTabImg" Value="0" />
                            <br />
                            <div class="row">
                                <asp:Literal runat="server" id="litImgErrorFormato"  Visible="false"><div id="errorImg" class="alert alert-danger alert-dismissable">El formato de la imagen es incorrecto</div> </asp:Literal>
                                <div class="col-sm-4 col-md-4">
                                    <asp:FileUpload runat="server" ID="flpFoto" />
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <asp:Button runat="server" ID="btnSubirFoto" Text="Subir" CssClass="btn" OnClick="btnSubirFoto_Click" CausesValidation="true" ValidationGroup="foto"/>
                                    <br />
                                    <asp:RequiredFieldValidator runat="server" ID="rqvFoto" ControlToValidate="flpFoto" ValidationGroup="foto"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-sm-4 col-md-4"></div>
                            </div>
                            <div class="row">
                                <asp:ListView runat="server" ID="rptImagenes">
                                    <ItemTemplate>
                                        <div class="col-md-3 producto">

                                            <div class="imagen">
                                                <a href="#" data-toggle="modal" data-target="#<%# Eval("IDImagenComercio") %>">
                                                <img src="/files/comercios/<%# Eval("URL") %>" alt="" class="img-responsive" width="200" style="height: 100px;max-height: 100px;"/></a>
                                                <%--<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>--%>
                                                <%--<asp:LinkButton runat="server" ID="lnkEliminar" Text="Eliminar" OnClick="eliminarImagen"></asp:LinkButton>--%>
                                            </div>
                                            <br>
                                            &nbsp;&nbsp;
                                            <a href="#" class="btn btn-danger" onclick="DeleteFoto(<%# Eval("IDImagenComercio") %>); return false;" title="Eliminar">
                                                eliminar
                                            </a>
                                        </div>
                                        <div class="modal fade" tabindex="-1" id="<%# Eval("IDImagenComercio") %>" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Imagen</h4>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <center><img src="/files/comercios/<%# Eval("URL") %>" alt="" class="img-responsive"  style="max-width: 500px;" /></center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                            <br/>

                        </div>
                        <div class="form-group" id="formButtons">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="Comercios.aspx" class="btn btn-link">Cancelar</a>

                                <asp:HiddenField runat="server" ID="hfIDComercio" Value="0" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle">Mapa</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var drag = false;
        var infowindow;

        /*$('#btnMap').click(function () {
            showMap();
        });*/

        function computepos(point) {
            $("#txtLatitud").val(point.lat().toFixed(6));
            $("#txtLongitud").val(point.lng().toFixed(6));
        }

        function drawMap(latitude, longitude) {
            var point = new google.maps.LatLng(latitude, longitude);

            var mapOptions = {
                zoom: 14,
                center: point,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: point
            })
            marker.setMap(map);
            map.setCenter(point);

            google.maps.event.addListener(map, 'click', function (event) {
                if (drag) { return; }
                if (map.getZoom() < 10) { map.setZoom(10); }
                map.panTo(event.latLng);
                computepos(event.latLng);
            });

            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "</p></div>";

                infowindow = new google.maps.InfoWindow({ content: html });
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(marker, 'dragstart', function () { if (infowindow) { infowindow.close(); } });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                //if (map.getZoom() < 10) { map.setZoom(10); }
                map.setCenter(event.latLng);
                computepos(event.latLng);
                drag = true;
                setTimeout(function () { drag = false; }, 250);
            });

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(point);
            });

            $("#myMapModal").modal("show");
        }

        function showMap() {
            var embAddr = $("#txtDomicilio2").val() + ',' + $("#ddlCiudad2 option:selected").text() + ',' + $("#ddlProvincia2 option:selected").text();
            $("#modalTitle").html(embAddr);

            if ($("#txtLatitud").val() == "" && $("#txtLongitud").val() == "") {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': embAddr }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        drawMap(latitude, longitude);

                        $("#txtLatitud").val(latitude);
                        $("#txtLongitud").val(longitude);
                    }
                    else {
                        $("#map-canvas").html('Could not find this location from the address given.<p>' + embAddr + '</p>');
                    }
                });
            }
            else {
                var latitude = $("#txtLatitud").val();
                var longitude = $("#txtLongitud").val();

                drawMap(latitude, longitude);
            }
        };
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
   <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/comerciose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/nomenclaturas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvucvVugcM6B5-rcps4y3QAAQQOAwPivI"></script>
</asp:Content>
