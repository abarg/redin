﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Socios.aspx.cs" Inherits="common_Socios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li class="last">Socios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de Socios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form id="formSocio" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMultimarca" Value="0" />
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    <div class="col-sm-3">
						    <label>Socio</label>
                            <input type="text" id="txtApellido" value="" maxlength="100" class="form-control"/>
					    </div>
					    <div class="col-sm-3">
						    <label>Nro. de Documento</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
					    </div>
                         <div class="col-sm-3">
						    <label>Email</label>
                            <input type="text" id="txtEmail" value="" maxlength="100" class="form-control"/>
					    </div>
                        <div class="col-sm-3">
						    <label>Nro. de Tarjeta</label>
						    <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2" style="display:none">
                            <label>Nro. de Cuenta</label>
						    <input type="text" id="txtCuenta" value="" maxlength="20" class="form-control number" />
					    </div>
                        <div id="divFranquicias" runat="server" class="col-sm-3 marca">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" />
                        </div>
                       <div class="col-sm-3 marca">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" value="" class="form-control" ID="ddlMarcas" />
                        </div>


                        <div class="col-sm-3 marca">
                            <label>Ciudad</label>
                              
                            <select class="form-control" id="ddlCiudades">
                                    <option value=' '> </option>        
                                    <option value='GENERAL SAN MARTIN'>GENERAL SAN MARTIN</option>
                                    <option value='Barrio para Jefes y Oficiales'>Barrio para Jefes y Oficiales</option>
                                    <option value='Ciudad del Libertador General San Martin'>Ciudad del Libertador General San Martin</option>
                                    <option value='Ayacucho'>Villa Ayacucho</option>
                                    <option value='Ballester'>Villa Ballester</option>
                                    <option value='Barrio Parque F. Alcorta'>Villa Barrio Parque F. Alcorta</option>
                                    <option value='Bernardo de Monteagudo'>Villa Bernardo de Monteagudo</option>
                                    <option value='Billinghurst'>Villa Billinghurst</option>
                                    <option value='Chacabuco'>Villa Chacabuco</option>
                                    <option value='Ciudad Jardin El Libertador'>Villa Ciudad Jardin El Libertador</option>
                                    <option value='Coronel M. Zapiola'>Villa Coronel M. Zapiola</option>
                                    <option value='General A. J. de Sucre'>Villa General A. J. de Sucre</option>
                                    <option value='General Eugenio Necochea'>Villa General Eugenio Necochea</option>
                                    <option value='General Jose Tomas Guvalueo'>Villa General Jose Tomas Guvalueo</option>
                                    <option value='General Juan Gregorio de Las Heras'>Villa General Juan Gregorio de Las Heras</option>
                                    <option value='Godoy Cruz'>Villa Godoy Cruz</option>
                                    <option value='Granaderos de San Martin'>Villa Granaderos de San Martin</option>
                                    <option value='Gregoria Matorras'>Villa Gregoria Matorras</option>
                                    <option value='Jose Leon Suarez'>Villa Jose Leon Suarez</option>
                                    <option value='Juan Martin de Pueyrredon'>Villa Juan Martin de Pueyrredon</option>
                                    <option value='Libertad'>Villa Libertad</option>
                                    <option value='Lynch'>Villa Lynch</option>
                                    <option value='Ma. Irene de los Remedios de Escalada'>Villa Ma. Irene de los Remedios de Escalada</option>
                                    <option value='Maipu'>Villa Maipu</option>
                                    <option value='Marques Alejandro Ma. de Aguado'>Villa Marques Alejandro Ma. de Aguado</option>
                                    <option value='Parque San Lorenzo'>Villa Parque San Lorenzo</option>
                                    <option value='San Andres'>Villa San Andres</option>
                                </select> 

                        </div>

              

				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <div class="btn-group" id="btnMasAccciones" runat="server">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a href="<%= ResolveUrl("~/common/AsociarTarjetas.aspx") %>">Asociar tarjetas</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/Socios-Unificar.aspx") %>">Unificar</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/TransferirPuntos.aspx") %>">Transferir puntos</a></li>
                                    <li><a href="<%= ResolveUrl("~/common/importarSocios.aspx") %>">Importar</a></li>
                                    
						        </ul>
					        </div>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Socios" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>

    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog  modal-lg">
			<div class="modal-content">
				<div class="modal-header">
                 
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
                    <input type="hidden" id="hdnIDSocio" value="0" />
				</div>
                <div class="alert alert-danger alert-dismissable" id="divErrorTr" style="display: none"></div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha</th> 
                                <th>Hora</th> 
                                <th>Operacion</th> 
                                <th>SDS</th> 
                                <th>Comercio</th> 
                                <th>Marca</th> 
                                <th>Tarjeta</th> 
                                <th>Establecimiento</th> 
                                <th>$ Original</th> 
                                <th>$ Ahorro</th> 
                                <th>Puntos</th> 
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
							
						</tbody>
					</table>
				</div>
         
				<div class="modal-footer">
                      <button class="btn btn-success" type="button" id="btnExportarTr" onclick="exportarTr();">Exportar a Excel</button>
                      <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingTr" style="display:none" />
                      <a href="" id="lnkDownloadTr" download="TrSocios" style="display:none">Descargar</a>
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/socios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>