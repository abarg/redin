﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="keywords.aspx.cs" Inherits="common_keywords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">CuponIN</a></li>
            <li class="last">Keywords</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de Keywords</h3>
            <%--<div class="alert alert-danger alert-dismissable" runat="server" id="divError" style="display: none"></div>--%>
            <asp:Literal runat="server" id="divError" Visible="false"><div class="alert alert-success alert-danger">El mail no se pudo enviar correctamente</div></asp:Literal>
            <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">El mail no se pudo enviar correctamente</div></asp:Literal>

		    <form id="Form1" runat="server">
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					     <div class="col-md-4">
                            <label>Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="100" />
                        </div>
                        <div class="col-md-2">
						    <label>Código</label>
                            <input type="text" id="txtCodigo" value="" maxlength="10" class="form-control"/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Vigencia desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Vigencia hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Estado</label>
                            <div >
                                 <asp:DropDownList CssClass="form-control required"  runat="server" ID="ddlEstado">
                                     <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Confirmado" Value="Confirmado"></asp:ListItem>
                                    <asp:ListItem Text="Pendiente" Value="Pendiente"></asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                        </div>					   
                        <div class="col-md-2"></div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Keywords" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
      <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/cuponin/keywords.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

