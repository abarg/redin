﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Socios-Unificar.aspx.cs" Inherits="common_Socios_Unificar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/SociosUnificar.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li class="last">Socios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Unificar socios repetidos por DNI</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
           <%-- <div class="alert alert-danger alert-dismissable" id="divErrorEmptySearch" style="display: none">El campo de Nro. de Documento no puede estar vacio</div>    --%>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
		    <form id="formSocio" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDFranquicia" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    <div class="col-sm-3">
						    <label><span class="f_req">* </span>Nro. de Documento</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
					    </div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>
                             <label  id="lblseleccion" style="display:none;color:green;"><br />Haga click en el ícono de selección para unificar a los demás socios con el seleccionado</label>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
        <div class="row">
        <div class="col-sm-12 col-md-12" id="tblLiquidacionDetalle">
            <table id="tablaSocios" class="table table-striped table-bordered mediaTable" style="display:none" >
				<thead>
					<tr>
                        <th class="essential" style="width:100px">Unificar</th>
                        <th class="essential" style="width:100px">ID</th>
                        <th class="essential">Nro. Documento</th>
                        <th class="optional">Tarjeta</th>
					</tr>
				</thead>
				<tbody id="bodySociosDetalle">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
    </div>
    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog  modal-lg">
			<div class="modal-content">
				<div class="modal-header">
                 
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
                    <input type="hidden" id="hdnIDSocio" value="0" />
				</div>         
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
</asp:Content>

