﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Terminalese.aspx.cs" Inherits="common_Terminalese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }
    </style>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/common/Terminales.aspx") %>">Terminales</a></li>
               <li>Edición</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Terminales</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>           
            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPos" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabArancelPuntos" data-toggle="tab">Arancel & Puntos</a></li>
                    <li><a href="#tabDomicilioC" data-toggle="tab">Dom. Comercial</a></li>
                    <li><a href="#tabGiftcard" data-toggle="tab">Giftcard</a></li>
                    <li><a href="#tabPuntosDeVenta" class="hide" data-toggle="tab">Puntos de venta</a></li>
                    <li><a href="#tabCuponIN" data-toggle="tab">CuponIN</a></li>
                    <li><a href="#tabPruebas" data-toggle="tab">Verificaciones POS</a></li>                    
                </ul>
                <form runat="server" id="formComercio" class="form-horizontal" role="form">
                    <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                    <div class="tab-content">

                        <div class="tab-pane" id="tabArancelPuntos">
                            <br />
                             <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Arancel %</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel" CssClass="form-control required"  MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel2" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel3" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel4" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel5" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel6" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtArancel7" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Puntos %</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos" CssClass="form-control required"  MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos2" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos3" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos4" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos5" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos6" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntos7" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Mult Puntos</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos1" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Descuento</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Modalidad</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkModalidadDebito" Text="Débito" />
                                    <asp:CheckBox runat="server" ID="chkModalidadCredito" Text="Credito" />
                                    <asp:CheckBox runat="server" ID="chkModalidadEfectivo" Text="Efectivo" />
                                    <%--<asp:TextBox runat="server" ID="txtModalidadDescuento" CssClass="form-control" MaxLength="100"></asp:TextBox>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Descripcion</label>
                                <div class="col-lg-6">
                                    <asp:TextBox runat="server" ID="txtDescuentoDescripcion" CssClass="form-control" MaxLength="500" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Desc. Vip</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip2" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip3" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip4" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip5" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip6" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip7" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Puntos vip%</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtPuntosVip1" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Lunes</span>                                          
                                </div>                                                                             
                                <div class="col-lg-1">                                                             
                                    <asp:TextBox runat="server" ID="txtPuntosVip2" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Martes</span>                                         
                                </div>                                                                             
                                <div class="col-lg-1">                                                             
                                    <asp:TextBox runat="server" ID="txtPuntosVip3" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>                                      
                                </div>                                                                             
                                <div class="col-lg-1">                                                             
                                    <asp:TextBox runat="server" ID="txtPuntosVip4" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Jueves</span>                                      
                                </div>                                                                          
                                <div class="col-lg-1">                                                          
                                    <asp:TextBox runat="server" ID="txtPuntosVip5" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Viernes</span>                                        
                                </div>                                                                             
                                <div class="col-lg-1">                                                             
                                    <asp:TextBox runat="server" ID="txtPuntosVip6" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Sábado</span>                                         
                                </div>                                                                             
                                <div class="col-lg-1">                                                             
                                    <asp:TextBox runat="server" ID="txtPuntosVip7" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Mult Puntos Vip</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip2" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip3" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip4" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip5" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip6" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip7" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="col-lg-2 control-label">Modalidad vieja</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtModalidadDescuentoVip" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Modalidad</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkModalidadDebitoVip" Text="Débito" />
                                    <asp:CheckBox runat="server" ID="chkModalidadCreditoVip" Text="Credito" />
                                    <asp:CheckBox runat="server" ID="chkModalidadEfectivoVip" Text="Efectivo" />
                                    <%--<asp:TextBox runat="server" ID="txtModalidadDescuento" CssClass="form-control" MaxLength="100"></asp:TextBox>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Descripción</label>
                                <div class="col-lg-6">
                                    <asp:TextBox runat="server" ID="txtDescuentoDescripcionVip" CssClass="form-control" MaxLength="500" Rows="5" TextMode="MultiLine" ></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Affinity</label>
                                <div class="col-sm-2 col-md-2">
                                    <asp:TextBox runat="server" ID="txtAffinity" CssClass="form-control required" MaxLength="4" MinLength="4" Text="0000"></asp:TextBox>
                                    <%--<asp:DropDownList runat="server" class="form-control" ID="ddlAffinity">
                                        <asp:ListItem Value="0000" Text="Común" />
                                        <asp:ListItem Value="1111" Text="Vip" />
                                        <asp:ListItem Value="9999" Text="Ambos" />
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cobrar uso de Red</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkCobrarUsoRed" Text="Si" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">POS Web</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkPosWeb" Text="Habilitado" />
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtCostoPosWeb" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Costo</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="tabDatosPos">
                            <br />
<%--                            <div class="form-group" id="divComercios" runat="server">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Comercios</label>
                                <div class="col-lg-9">
                                    <asp:DropDownList runat="server" ID="searchable" multiple="multiple" ClientIDMode="Static" DataTextField="Nombre" DataValueField="ID" />
                                    <br />
                                    <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                                </div>
                            </div>--%>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span>Comercio</label>
                            <div class="col-lg-6">
                                <asp:DropDownList runat="server" class="form-control chosen" onchange="cargarComercios();" ID="ddlComercios"  ClientIDMode="Static"  />
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Fecha de Carga</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFechaAlta" CssClass="form-control required" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Tipo</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" class="form-control required" ID="ddlPOSTipo" onchange="showFidely();">
                                        <asp:ListItem Value="Web" Text="Web" />
                                        <asp:ListItem Value="POS" Text="POS" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%-- <div class="form-group" id="ddlVisa1" style="">
                                <label class="col-lg-2 control-label">Estado VISA</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlEstadoVisa">
                                        <asp:ListItem Value="IN" Text="ALTA" />
                                        <asp:ListItem Value="UP" Text="MODIFICACION" />
                                        <asp:ListItem Value="DE" Text="BAJA" />
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                            <div class="form-group" runat="server" id="ddlFidely1">
                                <label class="col-lg-2 control-label">Fidely Usuario</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFidelyUsuario" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" runat="server" id="ddlFidely2">
                                <label class="col-lg-2 control-label">Fidely Pwd</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFidelyPwd" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" runat="server" id="ddlFidely3">
                                <label class="col-lg-2 control-label">Fidely Clave Licencia</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFidelyLicencia" CssClass="form-control" MinLength="12" MaxLength="12"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Tipo Terminal</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlPOSSistema">
                                        <asp:ListItem Value="AMEX" Text="AMEX" />
                                        <asp:ListItem Value="LAPOS" Text="LAPOS" />
                                        <asp:ListItem Value="POS" Text="POS" />
                                        <asp:ListItem Value="POSNET" Text="POSNET" />
                                        <asp:ListItem Value="SOFT" Text="SOFT" />
                                        <asp:ListItem Value="NEXGO" Text="NEXGO" />
                                        <asp:ListItem Value="MPOS" Text="M-POS" />

                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Terminal/Licencia</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSTerminal" CssClass="form-control required" MaxLength="12" MinLength="8"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Repetir Terminal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSTerminal_Rep" CssClass="form-control required" equalTo="#txtPOSTerminal" MaxLength="12" MinLength="8"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span>Establec.</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSEstablecimiento" CssClass="form-control required" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Marca</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSMarca" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Modelo</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtModeloPOS" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">SIMCARD</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtSimcardPOS" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular empresa</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="ddlCelularEMpresaPOS" CssClass="form-control">
                                        <asp:ListItem Text="" Value="" />
                                        <asp:ListItem Text="Claro" Value="Claro" />
                                        <asp:ListItem Text="Movistar" Value="Movistar" />
                                        <asp:ListItem Text="Nextel" Value="Nextel" />
                                        <asp:ListItem Text="Personal" Value="Personal" />
                                        <asp:ListItem Text="Otro" Value="Otro" />
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Activo</label>
                                <div class="col-lg-4">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox runat="server" ID="chkActivo" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de activación</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSFechaActivacion" Enabled="false" CssClass="form-control validDate" MaxLength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Reprogramado</label>
                                <div class="col-lg-4">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox runat="server" ID="chkPOSReprogramado" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de reprogramación</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPOSFechaReprogramacion" CssClass="form-control validDate" MaxLength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Terminal inválida</label>
                                <div class="col-lg-4">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox runat="server" ID="chkInvalido" Enabled="false" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-lg-2 control-label">Costo POS Propio</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCostoPOSPropio" CssClass="form-control" value="0"  MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtPOSObservaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabDomicilioC">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais2">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia2" onchange="LoadCiudades2(this.value,'ddlCiudad2');">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad2" onchange="LoadZonas(this.value,'cmbZona');"
                                        data-placeholder="Seleccione una ciudad">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio2" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto2" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal2" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom2" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fax</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFax2" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Geolocalizar</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLatitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Latitud</span>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLongitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Longitud</span>
                                </div>
                                <a href="javascript:showMap();" id="btnMap" class="btn">Ver mapa</a>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Zona</label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="cmbZona" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tabGiftcard">
                            <br />
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Arancel  %</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtGiftcardCarga" CssClass="form-control"  MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Carga</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtGiftcardDescarga" CssClass="form-control"  MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Descarga</span>
                                </div>
                            </div>
                            <div class="form-group hide">
                                <label class="col-lg-2 control-label">Genera puntos</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkGiftcardGeneraPuntos" Text="Si" Checked="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cobrar uso de Red</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkGiftcardCobrarUsored" Text="Si" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Gift Web</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkGiftcardPosWeb" Text="Habilitado" />
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtGiftcardCostoPosWeb" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Costo</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabPuntosDeVenta">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorPuntos" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtPuntoVenta" CssClass="form-control" MaxLength="100" ClientIDMode="Static"></asp:TextBox>
                                        <span class="help-block">Punto</span>
                                    </div>
                                    <div class="col-lg-1">
                                        <asp:CheckBox runat="server" ID="chkDefault" Checked="false" ClientIDMode="Static" />
                                        <span class="help-block">Es default</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarPuntoVenta" class="btn" type="button" onclick="agregarPuntoVenta();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="gridPuntos"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hfIDPuntoVenta" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabCuponIN">
                            <br />
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Arancel  %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCuponINArancel" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Carga</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cobrar uso de Red</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkCuponINCobrarUsoRed" Text="Si" />
                                </div>
                            </div>
                        </div>                        
                        <div class="tab-pane" id="tabPruebas">
                            <br />
                            <div class="row">
                                 <div class="col-lg-12">
                                     <asp:Literal runat="server" ClientIDMode="Static"  ID="litEstadoCompras" ></asp:Literal>&nbsp;&nbsp;
                                     <asp:Literal runat="server" ClientIDMode="Static"  ID="litEstadoGift" > </asp:Literal>&nbsp;&nbsp;
                                     <asp:Literal runat="server" ClientIDMode="Static"  ID="litEstadoCanjes" > </asp:Literal>
                                  </div>
                            </div>
                            <br/>
                            <div class="alert alert-danger alert-dismissable" id="divErrorPruebas" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFechaDesdePruebas" ClientIDMode="Static" CssClass="form-control validDate" MaxLength="10" />
                                        <span class="help-block">Fecha desde</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFechaHastaPruebas" ClientIDMode="Static" CssClass="form-control validDate" MaxLength="10" />
                                        <span class="help-block">Fecha hasta</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button id="Button1" class="btn" type="button" onclick="filterPruebas();">Buscar</button>
                                        <button id="btnVerTodos" class="btn" type="button" onclick="verTodosPruebas();">Ver todas</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="gridPruebas"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hdnPruebas" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="formButtons">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="Terminales.aspx" class="btn btn-link">Cancelar</a>

                                <asp:HiddenField runat="server" ID="hfIDComercio" Value="0" />
                                <asp:HiddenField runat="server" ID="hfIDTerminal" Value="0" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
   
         <div class="modal fade" id="modalPruebaPOS">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Verificaciones POS</h4>
                        <input type="hidden" id="hdnIDVerificacionPOS" value="0" />
                    </div>
                </div>
            </div>
        </div>

    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle">Mapa</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var drag = false;
        var infowindow;

        /*$('#btnMap').click(function () {
            showMap();
        });*/

        function computepos(point) {
            $("#txtLatitud").val(point.lat().toFixed(6));
            $("#txtLongitud").val(point.lng().toFixed(6));
        }

        function drawMap(latitude, longitude) {
            var point = new google.maps.LatLng(latitude, longitude);

            var mapOptions = {
                zoom: 14,
                center: point,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: point
            })
            marker.setMap(map);
            map.setCenter(point);

            google.maps.event.addListener(map, 'click', function (event) {
                if (drag) { return; }
                if (map.getZoom() < 10) { map.setZoom(10); }
                map.panTo(event.latLng);
                computepos(event.latLng);
            });

            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "</p></div>";

                infowindow = new google.maps.InfoWindow({ content: html });
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(marker, 'dragstart', function () { if (infowindow) { infowindow.close(); } });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                //if (map.getZoom() < 10) { map.setZoom(10); }
                map.setCenter(event.latLng);
                computepos(event.latLng);
                drag = true;
                setTimeout(function () { drag = false; }, 250);
            });

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(point);
            });

            $("#myMapModal").modal("show");
        }

        function showMap() {
            var embAddr = $("#txtDomicilio2").val() + ',' + $("#ddlCiudad2 option:selected").text() + ',' + $("#ddlProvincia2 option:selected").text();
            $("#modalTitle").html(embAddr);

            if ($("#txtLatitud").val() == "" && $("#txtLongitud").val() == "") {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': embAddr }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        drawMap(latitude, longitude);

                        $("#txtLatitud").val(latitude);
                        $("#txtLongitud").val(longitude);
                    }
                    else {
                        $("#map-canvas").html('Could not find this location from the address given.<p>' + embAddr + '</p>');
                    }
                });
            }
            else {
                var latitude = $("#txtLatitud").val();
                var longitude = $("#txtLongitud").val();

                drawMap(latitude, longitude);
            }
        };
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/terminalese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
</asp:Content>
