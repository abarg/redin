﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.IO;
using ACHE.Business;


public partial class common_dashboard_PlusIN : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
             
                hdnIDAdmin.Value = "0";
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                Franquicias franquicia = new Franquicias();
                cargarCombos(listFranquicias);

                if (!String.IsNullOrEmpty(Request.QueryString["IDFranquicia"]))
                {
                    franquicia = listFranquicias[Convert.ToInt32(Request.QueryString["IDFranquicia"]) - 1];
                    ddlFranquicias.SelectedValue = Request.QueryString["IDFranquicia"];
                    hdnIDFranquicias.Value = Request.QueryString["IDFranquicia"];
                }

            }
            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                if (usu.Tipo == "B")
                    Response.Redirect("home.aspx");

                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
            else
                Response.Redirect("/login.aspx");
        }

    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    private void cargarCombos(List<Franquicias> listFranquicias)
    {
        try
        {
            //bFranquicia bFranquicia = new bFranquicia();
            this.ddlFranquicias.DataSource = listFranquicias;// bFranquicia.getFranquicias();
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    [WebMethod(true)]
    public static string obtenerTotalSociosPlusIN()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_CantidadSociosPlusIN ", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("");
                else
                    html = "0";
            }
        }

        return html;
    }
    [WebMethod(true)]
    public static string obtenerTotalFacturacionPLUSIN()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalFacturadoPlusINMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }
        return html;
    }


    [WebMethod(true)]
    public static string obtenerTotalSociosPlusINFranq(int IDFranquicia)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_CantidadSociosPlusIN " + IDFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("");
                else
                    html = "0";
            }
        }

        return html;
    }


    [WebMethod(true)]
    public static string obtenerTotalFacturacionPLUSINFranq(int IDFranquicia)
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_TotalFacturadoPlusINMensual '" + fechaDesde + "','" + fechaHasta + "'," + IDFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }
        return html;
    }
}