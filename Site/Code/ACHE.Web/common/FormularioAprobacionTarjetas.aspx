﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FormularioAprobacionTarjetas.aspx.cs" Inherits="common_FormularioAprobacionTarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Formulario aprobacion tarjetas</a></li>
        </ul>
    </div>
      <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Formularios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <asp:HiddenField ID="hdnIDFranquicia" ClientIDMode="Static" runat="server" />
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2 franquicia">
                            <label> Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"> </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-sm-8 col-sm-md-8">
                                <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                                <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                                <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                                <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                                <a href="" id="lnkDownload" download="FormularioAprobacionTarjetas" style="display:none">Descargar</a>
                            </div>
                     </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/formularioAprobacionTarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

