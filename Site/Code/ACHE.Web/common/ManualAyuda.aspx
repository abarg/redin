﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManualAyuda.aspx.cs" Inherits="common_ManualAyuda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" media="all" href="//p6.zdassets.com/hc/assets/application-e89106f6336e1970d4e3.css" id="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//p6.zdassets.com/hc/themes/711620/205488657/style-966e89d4953d1fdfb4835b5e86ff4bc0.css?brand_id=203747&amp;locale=es" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel Visible="false" runat="server" ID="pnlTitulos">
        <section class="hero-unit search-box">
            <form role="search" class="search" data-search="" action="ManualAyuda.aspx?search=" accept-charset="UTF-8" method="get">
                <input type="search" name="search" placeholder="Buscar" autofocus="autofocus" />
            </form>
        </section>
        <section class="knowledge-base">
            <div class="category-tree">
                <section class="category">
                    <asp:Literal runat="server" ID="litHTMLTitulos"></asp:Literal>
                </section>
            </div>
        </section>

    </asp:Panel>

    <asp:Panel runat="server" ID="pnlSubTitulos" Visible="false">
        <nav class="sub-nav">
            <ol class="breadcrumbs">
                <li title="Centro de Ayuda">
                    <a href="ManualAyuda.aspx">Manual de ayuda</a>
                </li>
                <li>
                    <asp:Literal runat="server" ID="litTitulo"></asp:Literal>
                </li>
            </ol>
            <form role="search" class="search" data-search="" action="ManualAyuda.aspx?search=" accept-charset="UTF-8" method="get">
                <input type="search" name="search" placeholder="Buscar" autofocus="autofocus" />
            </form>
        </nav>
        <header class="page-header">
            <h1>
                <asp:Literal runat="server" ID="litTitulo2"></asp:Literal>
            </h1>
            <span></span>
        </header>
        <ul class="article-list">
            <asp:Literal runat="server" ID="litHTMLSubtitulos"></asp:Literal>

        </ul>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlArticulo" Visible="false">
        <nav class="sub-nav">
            <ol class="breadcrumbs">
                <asp:Literal runat="server" ID="litBreadCrumbsArticulo"></asp:Literal>
            </ol>
            <form role="search" class="search" data-search="" action="ManualAyuda.aspx?search=" accept-charset="UTF-8" method="get">
                <input type="search" name="search" placeholder="Buscar" autofocus="autofocus" />
            </form>
        </nav>

        <article class="main-column">
            <header class="article-header">
                <h1>
                    <asp:Literal runat="server" ID="litTituloArticulo"></asp:Literal>
                </h1>
                <div class="article-info">
                    <div class="article-meta">
                        <strong class="article-author">
                            <asp:Literal runat="server" ID="litUsuario"></asp:Literal>
                        </strong>
                        <div class="article-updated meta"><time datetime="2015-11-12T19:50:54Z" title="2015-11-12T19:50:54Z" data-datetime="calendar">
                            <asp:Literal runat="server" ID="litFecha"></asp:Literal></time></div>
                    </div>
                </div>
            </header>

            <div class="article-body markdown">
                <span runat="server" id="spnHTML"></span>
            </div>

            <div class="article-attachments">
                <ul class="attachments">
                </ul>
            </div>
        </article>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlBusqueda" Visible="false">
        <nav class="sub-nav">
            <ol class="breadcrumbs">
                <li title="Centro de Ayuda">
                    <a href="ManualAyuda.aspx">Manual de ayuda</a>
                </li>
                <li title="Resultados de búsqueda">
                    <asp:Literal runat="server" ID="litquery"></asp:Literal>
                </li>
            </ol>
            <form role="search" class="search" data-search="" action="ManualAyuda.aspx?search=" accept-charset="UTF-8" method="get">
                <input type="search" name="search" placeholder="Buscar" autofocus="autofocus" />
            </form>
        </nav>

        <div class="row search_page">
            <div class="col-sm-12 col-md-12">

                <div class="search_panel clearfix">
                    <span runat="server" id="spnHTMLArticulos"></span>

                </div>
            </div>
        </div>



    </asp:Panel>


</asp:Content>

