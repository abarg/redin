﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;

public partial class common_Terminalese : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
            Session["CurrentImagen"] = null;
            Session["CurrentLogo"] = null;
            Session["CurrentFicha"] = null;
            this.txtFechaAlta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            this.txtArancel.Text = this.txtArancel2.Text = this.txtArancel3.Text = this.txtArancel4.Text = this.txtArancel5.Text = this.txtArancel6.Text = this.txtArancel7.Text = "3";
            this.txtPuntos.Text = this.txtPuntos2.Text = this.txtPuntos3.Text = this.txtPuntos4.Text = this.txtPuntos5.Text = this.txtPuntos6.Text = this.txtPuntos7.Text = "1";

            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
                cargarCombos();

            if (!String.IsNullOrEmpty(Request.QueryString["IDTerminal"]))
            {
                this.hfIDTerminal.Value = Request.QueryString["IDTerminal"];
                if (!this.hfIDTerminal.Value.Equals("0"))
                {
                    this.cargarDatosTerminal(Convert.ToInt32(Request.QueryString["IDTerminal"]));
                    //ddlComercios.Enabled = false;
                }
            }
        }

    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";


    }

    private void cargarCombos()
    {
        try
        {
            int idfranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idfranquicia = usu.IDFranquicia;
            }
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            if (idfranquicia > 0)
                listMarcas = bMarca.getMarcasByFranquicia(idfranquicia);
            else
                listMarcas = bMarca.getMarcas();

            ddlProvincia2.DataSource = Common.LoadProvincias();
            ddlProvincia2.DataTextField = "Nombre";
            ddlProvincia2.DataValueField = "ID";
            ddlProvincia2.DataBind();

            var paises = Common.LoadPaises();
            if (paises != null)
            {

                ddlPais2.DataSource = paises;
                ddlPais2.DataTextField = "Nombre";
                ddlPais2.DataValueField = "ID";
                ddlPais2.DataBind();
            }

            using (var dbContext = new ACHEEntities())
            {
                var listComercios = dbContext.Comercios.Include("Domicilios").Select(x => new { IDComercio = x.IDComercio, Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio }).OrderBy(x => x.Nombre).ToList();
                this.ddlComercios.DataSource = listComercios;
                this.ddlComercios.DataValueField = "IDComercio";
                this.ddlComercios.DataTextField = "Nombre";
                this.ddlComercios.DataBind();
                this.ddlComercios.Items.Insert(0, new ListItem("", "0"));
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod]
    public static void DeleteImagen(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.ImagenesComercios.Where(x => x.IDImagenComercio == id).FirstOrDefault();
            if (entity != null)
            {
                dbContext.ImagenesComercios.Remove(entity);
                dbContext.SaveChanges();
            }
        }
    }

    private void cargarDatosTerminal(int IDTerminal)
    {
        try
        {

            using (var dbContext = new ACHEEntities())
            {
                Terminales terminal = new Terminales();
                terminal = dbContext.Terminales.Where(x => x.IDTerminal == IDTerminal).FirstOrDefault();

                if (terminal != null)
                {


                    //Datos POS

                    this.ddlComercios.SelectedValue = terminal.IDComercio.ToString();
                    //ddlComercios.Enabled = false;
                    this.ddlPOSTipo.SelectedValue = terminal.POSTipo;
                    this.txtCostoPOSPropio.Text = terminal.CostoPOSPropio.ToString();
                    //if (terminal.POSTipo != "FIDELY")
                    //{
                    //    this.ddlFidely1.Attributes.Add("style", "display:none");
                    //    this.ddlFidely2.Attributes.Add("style", "display:none");
                    //    this.ddlFidely3.Attributes.Add("style", "display:none");
                    //}
                    //else
                    //    ddlVisa1.Attributes.Add("style", "display:none");
                    //this.ddlEstadoVisa.SelectedValue = terminal.Estado;
                    this.ddlPOSSistema.SelectedValue = terminal.POSSistema;
                    this.txtPOSTerminal.Text = terminal.POSTerminal.Trim();
                    this.txtPOSTerminal_Rep.Text = terminal.POSTerminal.Trim();
                    this.txtPOSEstablecimiento.Text = terminal.POSEstablecimiento;
                    this.txtPOSMarca.Text = terminal.POSMarca;
                    this.txtModeloPOS.Text = terminal.ModeloPOS;
                    this.txtSimcardPOS.Text = terminal.SimcardPOS;
                    this.ddlCelularEMpresaPOS.SelectedValue = terminal.EmpresaCelularPOS;
                    this.txtPOSObservaciones.Text = terminal.POSObservaciones;
                    this.txtFidelyLicencia.Text = terminal.FidelyClaveLicencia;
                    this.txtFidelyPwd.Text = terminal.FidelyPwd;
                    this.txtFidelyUsuario.Text = terminal.FidelyUsuario;
                    if (terminal.POSFechaActivacion.HasValue)
                        this.txtPOSFechaActivacion.Text = terminal.POSFechaActivacion.Value.ToString("dd/MM/yyyy");
                    if (terminal.POSFechaReprogramacion.HasValue)
                        this.txtPOSFechaReprogramacion.Text = terminal.POSFechaReprogramacion.Value.ToString("dd/MM/yyyy");
                    this.chkActivo.Checked = terminal.Activo;
                    this.chkPOSReprogramado.Checked = terminal.POSReprogramado;
                    this.chkInvalido.Checked = terminal.POSInvalido;


                    //Arancel y puntos
                    this.txtArancel.Text = terminal.POSArancel.ToString();
                    this.txtArancel2.Text = terminal.Arancel2.ToString();
                    this.txtArancel3.Text = terminal.Arancel3.ToString();
                    this.txtArancel4.Text = terminal.Arancel4.ToString();
                    this.txtArancel5.Text = terminal.Arancel5.ToString();
                    this.txtArancel6.Text = terminal.Arancel6.ToString();
                    this.txtArancel7.Text = terminal.Arancel7.ToString();
                    this.txtPuntos.Text = terminal.POSPuntos.ToString();
                    this.txtPuntos2.Text = terminal.Puntos2.ToString();
                    this.txtPuntos3.Text = terminal.Puntos3.ToString();
                    this.txtPuntos4.Text = terminal.Puntos4.ToString();
                    this.txtPuntos5.Text = terminal.Puntos5.ToString();
                    this.txtPuntos6.Text = terminal.Puntos6.ToString();
                    this.txtPuntos7.Text = terminal.Puntos7.ToString();
                    this.txtDescuento.Text = terminal.Descuento.ToString();
                    this.txtDescuento2.Text = terminal.Descuento2.ToString();
                    this.txtDescuento3.Text = terminal.Descuento3.ToString();
                    this.txtDescuento4.Text = terminal.Descuento4.ToString();
                    this.txtDescuento5.Text = terminal.Descuento5.ToString();
                    this.txtDescuento6.Text = terminal.Descuento6.ToString();
                    this.txtDescuento7.Text = terminal.Descuento7.ToString();
                    this.txtDescuentoDescripcion.Text = terminal.DescuentoDescripcion;
                    //this.txtModalidadDescuento.Text = terminal.ModalidadDescuento;
                    this.chkModalidadCredito.Checked = terminal.ModalidadCredito;
                    this.chkModalidadDebito.Checked = terminal.ModalidadDebito;
                    this.chkModalidadEfectivo.Checked = terminal.ModalidadEfectivo;
                    this.txtDescuentoVip.Text = terminal.DescuentoVip.HasValue ? terminal.DescuentoVip.Value.ToString() : "";
                    this.txtDescuentoVip2.Text = terminal.DescuentoVip2.HasValue ? terminal.DescuentoVip2.Value.ToString() : "";
                    this.txtDescuentoVip3.Text = terminal.DescuentoVip3.HasValue ? terminal.DescuentoVip3.Value.ToString() : "";
                    this.txtDescuentoVip4.Text = terminal.DescuentoVip4.HasValue ? terminal.DescuentoVip4.Value.ToString() : "";
                    this.txtDescuentoVip5.Text = terminal.DescuentoVip5.HasValue ? terminal.DescuentoVip5.Value.ToString() : "";
                    this.txtDescuentoVip6.Text = terminal.DescuentoVip6.HasValue ? terminal.DescuentoVip6.Value.ToString() : "";
                    this.txtDescuentoVip7.Text = terminal.DescuentoVip7.HasValue ? terminal.DescuentoVip7.Value.ToString() : "";

                    this.txtPuntosVip1.Text = terminal.PuntosVip1.HasValue ? terminal.PuntosVip1.Value.ToString() : "";
                    this.txtPuntosVip2.Text = terminal.PuntosVip2.HasValue ? terminal.PuntosVip2.Value.ToString() : "";
                    this.txtPuntosVip3.Text = terminal.PuntosVip3.HasValue ? terminal.PuntosVip3.Value.ToString() : "";
                    this.txtPuntosVip4.Text = terminal.PuntosVip4.HasValue ? terminal.PuntosVip4.Value.ToString() : "";
                    this.txtPuntosVip5.Text = terminal.PuntosVip5.HasValue ? terminal.PuntosVip5.Value.ToString() : "";
                    this.txtPuntosVip6.Text = terminal.PuntosVip6.HasValue ? terminal.PuntosVip6.Value.ToString() : "";
                    this.txtPuntosVip7.Text = terminal.PuntosVip7.HasValue ? terminal.PuntosVip7.Value.ToString() : "";

                    this.txtMulPuntosVip.Text = terminal.MultiplicaPuntosVip1.HasValue ? terminal.MultiplicaPuntosVip1.Value.ToString() : "";
                    this.txtMulPuntosVip2.Text = terminal.MultiplicaPuntosVip2.HasValue ? terminal.MultiplicaPuntosVip2.Value.ToString() : "";
                    this.txtMulPuntosVip3.Text = terminal.MultiplicaPuntosVip3.HasValue ? terminal.MultiplicaPuntosVip3.Value.ToString() : "";
                    this.txtMulPuntosVip4.Text = terminal.MultiplicaPuntosVip4.HasValue ? terminal.MultiplicaPuntosVip4.Value.ToString() : "";
                    this.txtMulPuntosVip5.Text = terminal.MultiplicaPuntosVip5.HasValue ? terminal.MultiplicaPuntosVip5.Value.ToString() : "";
                    this.txtMulPuntosVip6.Text = terminal.MultiplicaPuntosVip6.HasValue ? terminal.MultiplicaPuntosVip6.Value.ToString() : "";
                    this.txtMulPuntosVip7.Text = terminal.MultiplicaPuntosVip7.HasValue ? terminal.MultiplicaPuntosVip7.Value.ToString() : "";


                    this.txtDescuentoDescripcionVip.Text = terminal.DescuentoDescripcionVip;
                    this.chkModalidadCreditoVip.Checked = terminal.ModalidadCreditoVip;
                    this.chkModalidadDebitoVip.Checked = terminal.ModalidadDebitoVip;
                    this.chkModalidadEfectivoVip.Checked = terminal.ModalidadEfectivoVip;
                    //this.txtModalidadDescuentoVip.Text = terminal.ModalidadDescuentoVip;
                    this.txtMulPuntos1.Text = terminal.MultiplicaPuntos1.ToString();
                    this.txtMulPuntos2.Text = terminal.MultiplicaPuntos2.ToString();
                    this.txtMulPuntos3.Text = terminal.MultiplicaPuntos3.ToString();
                    this.txtMulPuntos4.Text = terminal.MultiplicaPuntos4.ToString();
                    this.txtMulPuntos5.Text = terminal.MultiplicaPuntos5.ToString();
                    this.txtMulPuntos6.Text = terminal.MultiplicaPuntos6.ToString();
                    this.txtMulPuntos7.Text = terminal.MultiplicaPuntos7.ToString();
                    this.txtAffinity.Text = terminal.AffinityBenef;
                    this.chkCobrarUsoRed.Checked = terminal.CobrarUsoRed;
                    this.chkPosWeb.Checked = terminal.HabilitarPOSWeb;
                    this.txtCostoPosWeb.Text = terminal.CostoPOSWeb.ToString();


                    //Domicilio Comercial
                    if (terminal.IDDomicilio != 0)
                    {
                        Domicilios oDomicilioC = terminal.Domicilios;

                        var pais = dbContext.Paises.Where(x => x.Nombre == oDomicilioC.Pais).FirstOrDefault();
                        if (pais != null)
                        {
                            var idPais = pais.IDPais;

                            if (idPais > 0)
                            {
                                this.ddlPais2.SelectedValue = idPais.ToString();
                                List<ComboViewModel> listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
                                ddlProvincia2.DataSource = listProvincias;
                                ddlProvincia2.DataTextField = "Nombre";
                                ddlProvincia2.DataValueField = "ID";
                                ddlProvincia2.DataBind();
                                this.ddlProvincia2.SelectedValue = oDomicilioC.Provincia.ToString();
                            }
                        }


                        ddlCiudad2.DataSource = Common.LoadCiudades(oDomicilioC.Provincia);
                        ddlCiudad2.DataTextField = "Nombre";
                        ddlCiudad2.DataValueField = "ID";
                        ddlCiudad2.DataBind();
                        ddlCiudad2.Items.Insert(0, new ListItem("", ""));

                        if (oDomicilioC.Ciudad.HasValue)
                            ddlCiudad2.SelectedValue = oDomicilioC.Ciudad.Value.ToString();
                        this.txtDomicilio2.Text = oDomicilioC.Domicilio;
                        this.txtCodigoPostal2.Text = oDomicilioC.CodigoPostal;
                        this.txtTelefonoDom2.Text = oDomicilioC.Telefono;
                        this.txtFax2.Text = oDomicilioC.Fax;
                        this.txtPisoDepto2.Text = oDomicilioC.PisoDepto;
                        this.txtLatitud.Text = oDomicilioC.Latitud;
                        this.txtLongitud.Text = oDomicilioC.Longitud;


                        if (oDomicilioC.Ciudad.HasValue)
                        {
                            cmbZona.DataSource = Common.LoadZonas(oDomicilioC.Ciudad.Value);
                            cmbZona.DataTextField = "Nombre";
                            cmbZona.DataValueField = "ID";
                            cmbZona.DataBind();
                            cmbZona.Items.Insert(0, new ListItem("", ""));

                            if (terminal.IDZona.HasValue)
                                cmbZona.SelectedValue = terminal.IDZona.Value.ToString();
                        }

                    }

                    //Giftcard
                    this.txtGiftcardCarga.Text = terminal.GifcardArancelCarga.ToString();
                    this.txtGiftcardDescarga.Text = terminal.GifcardArancelDescarga.ToString();
                    this.chkGiftcardCobrarUsored.Checked = terminal.GiftcardCobrarUsoRed;
                    this.chkGiftcardPosWeb.Checked = terminal.HabilitarGifcard;
                    this.chkGiftcardGeneraPuntos.Checked = terminal.GiftcardGeneraPuntos;
                    this.txtGiftcardCostoPosWeb.Text = terminal.CostoGifcard.ToString();


                    //CuponIN
                    this.txtCuponINArancel.Text = terminal.CuponINArancel.ToString();
                    this.chkCuponINCobrarUsoRed.Checked = terminal.CuponINCobrarUsoRed;
                    this.litEstadoCanjes.Text = (terminal.EstadoCanjes != "" && terminal.EstadoCanjes != null) ? "Estado Canjes: " + terminal.EstadoCanjes : "Estado Canjes: -";
                    this.litEstadoCompras.Text = (terminal.EstadoCompras != "" && terminal.EstadoCompras != null) ? "Estado Compras: " + terminal.EstadoCompras : "Estado Compras: -";
                    this.litEstadoGift.Text = (terminal.EstadoGift != "" && terminal.EstadoGift != null) ? "Estado Gift: " + terminal.EstadoGift : "Estado Gift: -";

                }
                else
                    throw new Exception("La terminal no existe");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDComercio, int IDTerminal,
        /*ARANCEL & PUNTOS*/ string Descuento, string Descuento2, string Descuento3, string Descuento4, string Descuento5, string Descuento6, string Descuento7
        , bool ModalidadDebito, bool ModalidadCredito, bool ModalidadEfectivo, string DescripcionDescuento
        , string DescuentoVip, string DescuentoVip2, string DescuentoVip3, string DescuentoVip4, string DescuentoVip5, string DescuentoVip6, string DescuentoVip7
        , string PuntosVip, string PuntosVip2, string PuntosVip3, string PuntosVip4, string PuntosVip5, string PuntosVip6, string PuntosVip7
        , string MulPuntosVip, string MulPuntosVip2, string MulPuntosVip3, string MulPuntosVip4, string MulPuntosVip5, string MulPuntosVip6, string MulPuntosVip7
        , bool ModalidadDebitoVip, bool ModalidadCreditoVip, bool ModalidadEfectivoVip, string DescripcionDescuentoVip
        , string POSArancel, string Arancel2, string Arancel3, string Arancel4, string Arancel5, string Arancel6, string Arancel7,
        string POSPuntos, string Puntos2, string Puntos3, string Puntos4, string Puntos5, string Puntos6, string Puntos7
        , bool CobrarUsoRed
        , string MultPuntos1, string MultPuntos2, string MultPuntos3, string MultPuntos4, string MultPuntos5, string MultPuntos6, string MultPuntos7
        , string Affinity
        /*DOMICILIO COMERCIAL*/
        , string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax, string PisoDepto, string Lat, string Long, int IDZona,
        /*DATOS PRINCIPALES*/
        string POSTipo, string POSSistema, string POSTerminal, string POSEstablecimiento, string POSMarca, string ModeloPOS, string SimcardPOS,
        string EmpresaCelularPOS, string POSObservaciones, string POSFechaActivacion, string POSFechaReprogramacion, bool Activo, bool Reprogramado, bool Invalido
        , string FidelyUsuario, string FidelyPwd, string FidelyClaveLicencia, bool posWeb, string costoPosWeb
        /*GIFTCARD*/
        , string giftcardCarga, string giftcardDescarga, bool giftcardCobrarUsored, bool giftcardPosWeb, bool giftcardGeneraPuntos, string giftcardCostoPosWeb, string cuponInArancel,
        bool cuponInCobrarUsored, string CostoPOSPropio)
    {
        try
        {

            //Terminal
            Terminales entity;
            using (var dbContext = new ACHEEntities())
            {

                var cterminal = dbContext.Terminales.Where(x => x.POSTerminal == POSTerminal && x.IDComercio != IDComercio && x.IDTerminal != IDTerminal).Count();

                if (cterminal != 0)
                {
                    throw new Exception("Este terminal ya existe en otro comercio, por favor verifique sus datos");
                }

                if (IDTerminal > 0)
                    entity = dbContext.Terminales.FirstOrDefault(s => s.IDTerminal == IDTerminal);
                else
                {
                    entity = new Terminales();
                    entity.Estado = "IN";
                }

                //Datos POS
                if (IDComercio > 0)
                    entity.IDComercio = IDComercio;
                else
                    throw new Exception("Debe elegir un comercio");

                entity.POSTipo = POSTipo;
                //entity.Estado = EstadoVisa;
                entity.POSSistema = POSSistema;
                entity.POSTerminal = POSTerminal.Trim();
                entity.NumEst = POSEstablecimiento.Trim().PadLeft(15, '0');
                entity.POSEstablecimiento = POSEstablecimiento;
                entity.POSMarca = POSMarca;
                entity.ModeloPOS = ModeloPOS;
                entity.SimcardPOS = SimcardPOS;
                entity.EmpresaCelularPOS = EmpresaCelularPOS;
                entity.POSObservaciones = POSObservaciones;
                if (POSFechaActivacion != "")
                    entity.POSFechaActivacion = DateTime.Parse(POSFechaActivacion);
                else
                    entity.POSFechaActivacion = null;
                if (POSFechaReprogramacion != "")
                    entity.POSFechaReprogramacion = DateTime.Parse(POSFechaReprogramacion);
                else
                    entity.POSFechaReprogramacion = null;
                entity.Activo = Activo;
                //if (IDComercio > 0) {
                if (!Activo)
                    entity.Estado = "DE";
                else
                    entity.Estado = "UP";
                //}
                entity.POSReprogramado = Reprogramado;
                entity.POSInvalido = Invalido;
                entity.FidelyClaveLicencia = FidelyClaveLicencia;
                entity.FidelyPwd = FidelyPwd;
                entity.FidelyUsuario = FidelyUsuario;

                //Arancel y puntos
                entity.POSArancel = decimal.Parse(POSArancel);
                entity.Arancel2 = decimal.Parse(Arancel2);
                entity.Arancel3 = decimal.Parse(Arancel3);
                entity.Arancel4 = decimal.Parse(Arancel4);
                entity.Arancel5 = decimal.Parse(Arancel5);
                entity.Arancel6 = decimal.Parse(Arancel6);
                entity.Arancel7 = decimal.Parse(Arancel7);
                entity.POSPuntos = decimal.Parse(POSPuntos);
                entity.Puntos2 = decimal.Parse(Puntos2);
                entity.Puntos3 = decimal.Parse(Puntos3);
                entity.Puntos4 = decimal.Parse(Puntos4);
                entity.Puntos5 = decimal.Parse(Puntos5);
                entity.Puntos6 = decimal.Parse(Puntos6);
                entity.Puntos7 = decimal.Parse(Puntos7);
                entity.Descuento = int.Parse(Descuento);
                entity.Descuento2 = int.Parse(Descuento2);
                entity.Descuento3 = int.Parse(Descuento3);
                entity.Descuento4 = int.Parse(Descuento4);
                entity.Descuento5 = int.Parse(Descuento5);
                entity.Descuento6 = int.Parse(Descuento6);
                entity.Descuento7 = int.Parse(Descuento7);
                entity.ModalidadCredito = ModalidadCredito;
                entity.ModalidadDebito = ModalidadDebito;
                entity.ModalidadEfectivo = ModalidadEfectivo;
                entity.DescuentoDescripcion = DescripcionDescuento;
                if (DescuentoVip != string.Empty)
                    entity.DescuentoVip = int.Parse(DescuentoVip);
                else
                    entity.DescuentoVip = null;
                if (DescuentoVip2 != string.Empty)
                    entity.DescuentoVip2 = int.Parse(DescuentoVip2);
                else
                    entity.DescuentoVip2 = null;
                if (DescuentoVip3 != string.Empty)
                    entity.DescuentoVip3 = int.Parse(DescuentoVip3);
                else
                    entity.DescuentoVip3 = null;
                if (DescuentoVip4 != string.Empty)
                    entity.DescuentoVip4 = int.Parse(DescuentoVip4);
                else
                    entity.DescuentoVip4 = null;
                if (DescuentoVip5 != string.Empty)
                    entity.DescuentoVip5 = int.Parse(DescuentoVip5);
                else
                    entity.DescuentoVip5 = null;
                if (DescuentoVip6 != string.Empty)
                    entity.DescuentoVip6 = int.Parse(DescuentoVip6);
                else
                    entity.DescuentoVip6 = null;
                if (DescuentoVip7 != string.Empty)
                    entity.DescuentoVip7 = int.Parse(DescuentoVip7);
                else
                    entity.DescuentoVip7 = null;

                if (PuntosVip != string.Empty)
                    entity.PuntosVip1 = decimal.Parse(PuntosVip);
                else
                    entity.PuntosVip1 = null;
                if (PuntosVip2 != string.Empty)
                    entity.PuntosVip2 = decimal.Parse(PuntosVip2);
                else
                    entity.PuntosVip2 = null;
                if (PuntosVip3 != string.Empty)
                    entity.PuntosVip3 = decimal.Parse(PuntosVip3);
                else
                    entity.PuntosVip3 = null;
                if (PuntosVip4 != string.Empty)
                    entity.PuntosVip4 = decimal.Parse(PuntosVip4);
                else
                    entity.PuntosVip4 = null;
                if (PuntosVip5 != string.Empty)
                    entity.PuntosVip5 = decimal.Parse(PuntosVip5);
                else
                    entity.PuntosVip5 = null;
                if (PuntosVip6 != string.Empty)
                    entity.PuntosVip6 = decimal.Parse(PuntosVip6);
                else
                    entity.PuntosVip6 = null;
                if (PuntosVip7 != string.Empty)
                    entity.PuntosVip7 = decimal.Parse(PuntosVip7);
                else
                    entity.PuntosVip7 = null;

                if (MulPuntosVip != string.Empty)
                    entity.MultiplicaPuntosVip1 = int.Parse(MulPuntosVip);
                else
                    entity.MultiplicaPuntosVip1 = null;

                if (MulPuntosVip2 != string.Empty)
                    entity.MultiplicaPuntosVip2 = int.Parse(MulPuntosVip2);
                else
                    entity.MultiplicaPuntosVip2 = null;

                if (MulPuntosVip3 != string.Empty)
                    entity.MultiplicaPuntosVip3 = int.Parse(MulPuntosVip3);
                else
                    entity.MultiplicaPuntosVip3 = null;

                if (MulPuntosVip4 != string.Empty)
                    entity.MultiplicaPuntosVip4 = int.Parse(MulPuntosVip4);
                else
                    entity.MultiplicaPuntosVip4 = null;

                if (MulPuntosVip5 != string.Empty)
                    entity.MultiplicaPuntosVip5 = int.Parse(MulPuntosVip5);
                else
                    entity.MultiplicaPuntosVip5 = null;

                if (MulPuntosVip6 != string.Empty)
                    entity.MultiplicaPuntosVip6 = int.Parse(MulPuntosVip6);
                else
                    entity.MultiplicaPuntosVip6 = null;

                if (MulPuntosVip7 != string.Empty)
                    entity.MultiplicaPuntosVip7 = int.Parse(MulPuntosVip7);
                else
                    entity.MultiplicaPuntosVip7 = null;


                //entity.ModalidadDescuentoVip = ModalidadDescuentoVip;
                entity.ModalidadCreditoVip = ModalidadCreditoVip;
                entity.ModalidadDebitoVip = ModalidadDebitoVip;
                entity.ModalidadEfectivoVip = ModalidadEfectivoVip;
                entity.DescuentoDescripcionVip = DescripcionDescuentoVip;
                if (!string.IsNullOrEmpty(MultPuntos1))
                    entity.MultiplicaPuntos1 = int.Parse(MultPuntos1);
                if (!string.IsNullOrEmpty(MultPuntos2))
                    entity.MultiplicaPuntos2 = int.Parse(MultPuntos2);
                if (!string.IsNullOrEmpty(MultPuntos3))
                    entity.MultiplicaPuntos3 = int.Parse(MultPuntos3);
                if (!string.IsNullOrEmpty(MultPuntos4))
                    entity.MultiplicaPuntos4 = int.Parse(MultPuntos4);
                if (!string.IsNullOrEmpty(MultPuntos5))
                    entity.MultiplicaPuntos5 = int.Parse(MultPuntos5);
                if (!string.IsNullOrEmpty(MultPuntos6))
                    entity.MultiplicaPuntos6 = int.Parse(MultPuntos6);
                if (!string.IsNullOrEmpty(MultPuntos7))
                    entity.MultiplicaPuntos7 = int.Parse(MultPuntos7);
                entity.AffinityBenef = Affinity;
                entity.CobrarUsoRed = CobrarUsoRed;
                entity.HabilitarPOSWeb = posWeb;
                if (costoPosWeb != "")
                    entity.CostoPOSWeb = decimal.Parse(costoPosWeb);
                else
                    entity.CostoPOSWeb = 0;

                if (CostoPOSPropio != "")
                    entity.CostoPOSPropio = decimal.Parse(CostoPOSPropio);
                else
                    entity.CostoPOSPropio = 0;


                if (entity.IDDomicilio == 0)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    entity.Domicilios.Estado = "A";
                }
                entity.Domicilios.TipoDomicilio = "C";
                entity.Domicilios.Entidad = "C";
                entity.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";

                if (Provincia != "" && Provincia != null)
                    entity.Domicilios.Provincia = int.Parse(Provincia);

                if (Ciudad != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                entity.Domicilios.CodigoPostal = CodigoPostal;
                entity.Domicilios.Telefono = TelefonoDom;
                entity.Domicilios.Fax = Fax;
                entity.Domicilios.PisoDepto = PisoDepto;
                entity.Domicilios.Latitud = Lat;
                entity.Domicilios.Longitud = Long;

                if (IDZona > 0)
                    entity.IDZona = IDZona;
                else
                    entity.IDZona = null;

                //Giftcard
                if (giftcardCarga != string.Empty)
                    entity.GifcardArancelCarga = decimal.Parse(giftcardCarga);
                else
                    entity.GifcardArancelCarga = 0;
                if (giftcardDescarga != string.Empty)
                    entity.GifcardArancelDescarga = decimal.Parse(giftcardDescarga);
                else
                    entity.GifcardArancelDescarga = 0;
                entity.GiftcardCobrarUsoRed = giftcardCobrarUsored;
                entity.HabilitarGifcard = giftcardPosWeb;
                entity.GiftcardGeneraPuntos = true;// giftcardGeneraPuntos;
                if (giftcardCostoPosWeb != "")
                    entity.CostoGifcard = decimal.Parse(giftcardCostoPosWeb);
                else
                    entity.CostoGifcard = 0;

                //CupoinIN
                entity.CuponINCobrarUsoRed = cuponInCobrarUsored;
                if (cuponInArancel != string.Empty)
                    entity.CuponINArancel = decimal.Parse(cuponInArancel);
                else
                    entity.CuponINArancel = 0;

                //Reprogramaciones

                if (IDTerminal > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Terminales.Add(entity);
                    dbContext.SaveChanges();
                }

            }
            return entity.IDTerminal.ToString();
        }

        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    #region Pruebas POS
    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPruebas(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio, string desde, string hasta)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                DateTime fDesde, fHasta;
                var result = dbContext.VerificacionesPOS
                    .Where(x => x.IDComercio == idComercio)
                    .OrderByDescending(x => x.FechaPrueba)
                    .Select(x => new VerificacionPOSViewModel()
                    {
                        FechaPrueba = x.FechaPrueba,
                        IDVerificacionPOS = x.IDVerificacionPOS,
                        EstadoCanjes = x.EstadoCanjes,
                        EstadoCompras = x.EstadoCompras,
                        EstadoGift = x.EstadoGift,
                        Calco = x.Calco == true ? "Si" : "No",
                        CalcoPuerta = x.CalcoPuerta == true ? "Si" : "No",
                        Display = x.Display == true ? "Si" : "No",
                        Folletos = x.Folletos == true ? "Si" : "No",
                        ObservacionesGenerales = x.ObservacionesGenerales,
                        UsuarioPrueba = x.Usuarios.Usuario

                    }).AsQueryable();

                if (!string.IsNullOrEmpty(desde))
                {
                    fDesde = DateTime.Parse(desde).Date;
                    result = result.Where(x => x.FechaPrueba >= fDesde).AsQueryable();
                }

                if (!string.IsNullOrEmpty(hasta))
                {
                    fHasta = DateTime.Parse(hasta).Date.AddDays(1).AddTicks(-1);
                    result = result.Where(x => x.FechaPrueba <= fHasta).AsQueryable();
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePrueba(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.VerificacionesPOS.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void ProcesarPrueba(int idComercio, int IDVerificacionPOS, string estadoCanjes, string estadoGift, string estadoCompras, string observacionesCanjes, string observacionesGift, string observacionesCompras
        , string usuario, string obs, string puntosCanjes, string puntosGift, string puntosCompras, string fechaPrueba)
    {
        try
        {
            int idusu = 0;
            bool logueado = false;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                logueado = true;
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idusu = usu.IDUsuario;
            }
            else
            {
                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    logueado = true;
                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    idusu = usu.IDUsuario;
                }
            }
            if (logueado)
            {
                using (var dbContext = new ACHEEntities())
                {
                    VerificacionesPOS entity;
                    if (IDVerificacionPOS == 0)
                        entity = new VerificacionesPOS();
                    else
                        entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == IDVerificacionPOS).FirstOrDefault();


                    entity.IDUsuario = idusu;

                    entity.IDComercio = idComercio;
                    entity.EstadoCanjes = estadoCanjes;
                    entity.EstadoCompras = estadoCompras;
                    entity.EstadoGift = estadoGift;

                    entity.Puntos_POSCanjes = puntosCanjes;
                    entity.Puntos_POSCompras = puntosCompras;
                    entity.Puntos_POSGift = puntosGift;

                    entity.Observaciones_POSCanjes = observacionesCanjes;
                    entity.Observaciones_POSCompras = observacionesCompras;
                    entity.Observaciones_POSGift = observacionesGift;
                    entity.FechaPrueba = DateTime.Parse(fechaPrueba);

                    entity.UsuarioPrueba = usuario;
                    entity.ObservacionesGenerales = obs;

                    if (IDVerificacionPOS == 0)
                        dbContext.VerificacionesPOS.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Puntos de venta

    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPuntos(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.PuntosDeVenta
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Numero)
                    .Select(x => new PuntoDeVentaViewModel()
                    {
                        IDPuntoVenta = x.IDPuntoVenta,
                        IDComercio = x.IDComercio,
                        Punto = x.Numero,
                        FechaAlta = x.FechaAlta,
                        EsDefault = x.EsDefault ? "Si" : "No",
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePunto(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.PuntosDeVenta.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarPunto(int IDComercio, int IDPuntoVenta, int numero, DateTime fechaAlta, bool esDefault)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.PuntosDeVenta.Any(x => x.Numero == numero && x.IDComercio == IDComercio && x.IDPuntoVenta != IDPuntoVenta))
                        throw new Exception("Ya existe un punto de venta con el numero ingresado para este comercio");

                    PuntosDeVenta entity;
                    if (IDPuntoVenta == 0)
                        entity = new PuntosDeVenta();
                    else
                        entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == IDPuntoVenta).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Numero = numero;
                    entity.FechaAlta = DateTime.Now;

                    #region default
                    var algunoDefault = dbContext.PuntosDeVenta.Any(x => x.IDComercio == IDComercio && x.EsDefault);
                    if (algunoDefault && esDefault)
                        throw new Exception("Solo puede haber un punto de venta como default");
                    else
                        entity.EsDefault = esDefault;
                    #endregion

                    if (IDPuntoVenta == 0)
                        dbContext.PuntosDeVenta.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio)
    {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.UsuariosComercios
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.UsuariosComercios.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDComercio, int IDUsuario, string usuario, string email, string pwd, string tipo)
    {
        try
        {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null))
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.UsuariosComercios.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosComercios.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosComercios entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosComercios();
                    else
                        entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosComercios.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }

}
