﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using ClosedXML.Excel;
using ACHE.Business;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;

using System.Web.UI.HtmlControls;
using ClosedXML.Excel;


public partial class common_Sorteos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int idMarca = 0;
        int idFran = 0;
        int idAdmin = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
        }
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usuFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFran = usuFran.IDFranquicia;
        }
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usuAdmin = (Usuarios)HttpContext.Current.Session["CurrentUser"];
            idAdmin = usuAdmin.IDUsuario;
        }
        cargarMarcas(idFran, idMarca, idAdmin);
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarMarcas(int idFran, int idMarca, int idAdmin)
    {
        using (var dbContext = new ACHEEntities())
        {
            if (idMarca > 0)
            {
                //marcas = marcas.Where(x => x.IDMarca == idMarca).OrderBy(x => x.Nombre).ToList();
                this.divMarca.Visible = false;
            }
            else if (idFran > 0 || idAdmin > 0)
            {
                var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
                if (idFran > 0)
                {
                    marcas = marcas.Where(x => x.IDFranquicia == idFran).OrderBy(x => x.Nombre).ToList();
                }
                if (marcas != null)
                {
                    cmbMarcas.DataSource = marcas;
                    cmbMarcas.DataTextField = "Nombre";
                    cmbMarcas.DataValueField = "IDMarca";
                    cmbMarcas.DataBind();
                    cmbMarcas.Items.Insert(0, new ListItem("", ""));
                }
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        int idFranq = 0;
        int idMarca = 0;

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usuFranq = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranq = usuFranq.IDFranquicia;
        }

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
        }

        if (HttpContext.Current.Session["CurrentUser"] != null || idFranq > 0 || idMarca > 0)
        {

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Sorteos.Include("Marcas")
                         .OrderByDescending(x => x.IDSorteo)
                         .Select(x => new
                         {
                             IDSorteo = x.IDSorteo,
                             Marca = x.Marcas.Nombre,
                             IDFranquicia = x.Marcas.IDFranquicia,
                             IDMarca = x.IDMarca,
                             Titulo = x.Titulo,
                             FechaDesde = x.FechaDesde,
                             FechaHasta = x.FechaHasta,
                             Estado = x.Activo ? "Activo" : "Inactivo",
                             ImporteDesde = x.ImporteDesde,
                             ImporteHasta = x.ImporteHasta,
                             Pais = x.Domicilios.Pais,
                             Provincia = x.Domicilios.Provincias.Nombre,
                             Ciudad = x.Domicilios.Ciudades.IDCiudad != null ? x.Domicilios.Ciudades.Nombre : "",
                             Domicilio = x.Domicilios.Domicilio != null ? (x.Domicilios.PisoDepto != null ? x.Domicilios.Domicilio +" " + x.Domicilios.PisoDepto : x.Domicilios.Domicilio) : ""
                         });

                if (idFranq > 0)
                    result = result.Where(x => x.IDFranquicia == idFranq);
                else if (idMarca > 0)
                    result = result.Where(x => x.IDMarca == idMarca);

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaDesde >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaHasta <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Sorteos.Where(x => x.IDSorteo == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Sorteos.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}