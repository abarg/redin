﻿using ACHE.Business;
using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class common_FormularioAprobacionTarjetase : System.Web.UI.Page
{
    private static string logo;
    private static string imagenes;
    private static string anverso;
    private static string reverso;
    private static string aprobacionCliente;
    private static string comprobante;
    private static string formularioAprobacion;
    private static string facturaAnticipo;
    private static string baseTracks;
    private static string importeFactura;
    private static string adjuntarGuia;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            logo="";
            imagenes="";
            anverso="";
            reverso="";
            aprobacionCliente="";
            comprobante="";
            formularioAprobacion = "";
            facturaAnticipo="";
            baseTracks="";
            importeFactura="";
            adjuntarGuia="";

            CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["IDFormulario"]))
            {
                int id = int.Parse(Request.QueryString["IDFormulario"]);
                this.hdnID.Value = id.ToString();
                loadInfo(id);
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }
    
    [System.Web.Services.WebMethod(true)]
    public static string generarTablaCostos(int idFormulario) {
        string html = "";
        string[] titulos = new string[6] { "TARJETA", "PERSONALIZACION", "PREACTIVACION RED POS", "TERMINADO MATE", "FOTO", "OTROS" };
        using (var dbContext = new ACHEEntities()) {
            if (idFormulario > 0) {
                var costos = dbContext.Costos.Where(x => x.IDFormularioTarjetas == idFormulario).ToList();
                if (costos.Any()) {
                    int i = 0;
                    foreach (var costo in costos) {
                        html += "<tr class=\"selectCostos\"  id=\"%\">";
                        html += "<td><input type=\"hidden\"  id=\"hdnTitulo\" class=\"selectCostos\" value='" + titulos[i] + "' />" + titulos[i] + "</td>";
                        // item.ImporteTotal.ToString().Replace(",", ".")
                        html += "<td><input    type=\"number\" min=\"0.01\" step=\"0.01\"  name=\"txtUnitario_0\"   id=\"txtUnitario_" + i + "\"  class=\"selectCostos\" onkeypress='return ActualizarTotalKeyPress(event," + i + ");'  value='" + costo.PrecioUnitario.ToString().Replace(",", ".") + "' /></td>";
                        html += "<td><input   type=\"number\" min=\"0.01\" step=\"0.01\"   id=\"txtCantidad_" + i + "\"  class=\"selectCostos\" onkeypress='return ActualizarTotalKeyPress(event," + i + ");' value='" + costo.Cantidad.ToString() + "'  /></td>";
                        html += "<td><input   type=\"number\" min=\"0.01\" step=\"0.01\"  id=\"txtTotal_" + i + "\"  class=\"selectCostos totales\"  value='" + costo.Total.ToString().Replace(",", ".") + "' disabled /></td>";
                        html += "<td><input id=\"txtObs_" + i + "\"  class=\"selectCostos\"  value='" + (costo.Observaciones ?? "") + "'  /></td>";
                        //html += "<td> <button id='Button_'" + i + " class='btn' type='button' onkeypress='return ActualizarItemCarritoKeyPress(event,"+i+");'>Calcular Total</button> </td>";
                        html += "</tr>";
                        i++;
                    }//onkeypress="return ActualizarItemCarritoKeyPress(event, '6','XS','2','1');"
                }
            }
            else {
                for (int i = 0; i < 6; i++) {
                    html += "<tr class=\"selectCostos\"  id=\"%\">";
                    html += "<td><input type=\"hidden\"  id=\"hdnTitulo\" class=\"selectCostos\" value='" + titulos[i] + "' />" + titulos[i] + "</td>";
                    // item.ImporteTotal.ToString().Replace(",", ".")
                    html += "<td><input  type=\"number\" min=\"0.01\" step=\"0.01\" id=\"txtUnitario_" + i + "\" class=\"selectCostos\" onkeypress='return ActualizarTotalKeyPress(event," + i + ");'  /></td>";
                    html += "<td><input type=\"number\" min=\"0.01\" step=\"0.01\"  id=\"txtCantidad_" + i + "\"  class=\"selectCostos\" onkeypress='return ActualizarTotalKeyPress(event," + i + ");'  /></td>";
                    html += "<td><input type=\"number\" min=\"0.01\" step=\"0.01\"  id=\"txtTotal_" + i + "\" class=\"selectCostos totales\" disabled/></td>";
                    html += "<td><input  id=\"txtObs_" + i + "\"  class=\"selectCostos_\" /></td>";
                    html += "</tr>";
                }
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTablaEspecificaciones(int idFormulario)
    {
        string html = "";
        string[] titulos = new string[12] { "BANDA MAGNÉTICA", "PANEL DE FIRMA", "TERMINADO LAMINACIÓN", "PERSONALIZACIÓN", "BASE DE DATOS", "CANT. PERSONALIZADAS", "NOMBRE Y APELLIDO ", "D.N.I", "N° DE SOCIO", "FOTO", "PROFF" ,"OTRO" };
        using (var dbContext = new ACHEEntities())
        {
            if (idFormulario > 0)
            {
                var esps = dbContext.Especificaciones.Where(x => x.IDFormularioTarjetas == idFormulario).ToList();
                if (esps.Any())
                {
                    int i = 0;
                    foreach (var esp in esps) {                        
                        html += "<tr class=\"selectEspecificaciones\"  id=\"%\">";
                        html += "<td><input type=\"hidden\"  id=\"hdnTitulo\" class=\"selectEspecificaciones\" value='" + titulos[i] + "' />" + titulos[i] + "</td>";
                        switch (esp.Tiene)
                        {
                            case "SI":
                                html += "<td> <select id=\"cmbTiene\" class=\"form-control selectEspecificaciones\"><option value=\"\"></option>  <option selected value=\"SI\">SI</option><option value=\"NO\">NO</option></select>  </td>";
                                break;
                            case "NO":
                                html += "<td> <select id=\"cmbTiene\" class=\"form-control selectEspecificaciones\"><option value=\"\"></option>  <option value=\"SI\">SI</option><option selected value=\"NO\">NO</option></select>  </td>";
                                break;
                            default:
                                html += "<td> <select id=\"cmbTiene\" class=\"form-control selectEspecificaciones\"><option value=\"\"></option>  <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select>  </td>";
                                break;
                        }

                        html += "<td><input id=\"txtObs\" class=\"selectEspecificaciones\" value='" + ((esp.Observaciones != null) ? esp.Observaciones.ToString() : "") + "'/></td>";
                        html += "</tr>";
                        i++;
                    }
                }
            }
            else
            {
                for (int i = 0; i < 12; i++)
                {
                    html += "<tr class=\"selectEspecificaciones\"  id=\"%\">";
                    html += "<td><input type=\"hidden\"  id=\"hdnTitulo\" class=\"selectEspecificaciones\" value='" + titulos[i] + "' />" + titulos[i] + "</td>";
                    html += "<td> <select id=\"cmbTiene\" class=\"form-control selectEspecificaciones\" ><option value=\"\"></option>  <option value=\"SI\">SI</option><option value=\"NO\">NO</option></select>  </td>";

                    html += "<td><input id=\"txtObs\" class=\"selectEspecificaciones\"/></td>";
                    html += "</tr>";
                }
            }
        }


        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static void guardar(int idformulario, string fecha, int idFranquicia, string estado, string unidadNegocio, string costos, string cliente, string descripcion,
        string obsAnverso, string obsReverso, string especificaciones, bool imprimirSimulacion, bool corregirConSimulacion,
        bool corregirSinSimulacion, string obs, string razonSocial, string paisFact, string ciudadFact, string provinciaFact, string domFiscal, string cuit, string condIVA, string contacto, string telContacto,
        string emailContacto, string obsDatosFact, string formaPago, string fechaEnvioProv, string nombre, string dni, string domicilio, string localidad,
        string cp, string provincia, string fechaEnvio, string empresa, string nroGuia, string formaEnvio, string telefono, string paginaWeb, string email, string facebook, string basesycondiciones, bool logoDatosReverso)
    {
           if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    FormularioTarjetas entity;
                    if (idformulario > 0)
                        entity = dbContext.FormularioTarjetas.Where(x => x.IDFormularioTarjetas == idformulario).FirstOrDefault();
                    else
                    {
                        entity = new FormularioTarjetas();
                    }
                  
                    if (fecha != null && fecha != "")
                        entity.Fecha = DateTime.Parse(fecha);
                    else
                        entity.Fecha = null;
                    entity.IDFranquicia = idFranquicia;
                    entity.UnidadNegocio = unidadNegocio;

                    entity.Estado = estado;

                    if (entity.Estado != "ENTREGADO")
                    {
                        if (fecha != null && fecha != "")
                        {
                            entity.Estado = "PEDIDO";
                        }

                        if (formularioAprobacion != null && formularioAprobacion != "")
                        {
                            entity.Estado = "PRODUCCION";
                        }

                        if (nroGuia != null && nroGuia != "")
                        {
                            entity.Estado = "ENVIADO";
                        }

                    }
                    else 
                    {
                        entity.Estado = "ENTREGADO";
                    }
                   
                    //grabarCostos
                    if (idformulario > 0)
                    {
                        var costosViejos = entity.Costos.ToList();
                        dbContext.Costos.RemoveRange(costosViejos);
                    }
                    var filas = costos.Split('%').ToList();
                    List<Costos> listcostos = new List<Costos>();
                    for (int i = 1; i < filas.Count(); i++)
                    {
                        var fila = filas[i].Split('-').ToList();
                        Costos costo = new Costos();
                        for (int j = 0; j < fila.Count(); j++)
                        {
                            var valor = fila[j].Split('#').ToList();
                            //var valor = aux[1].Split('_').ToList();
                            var aux = valor[0].Split('_').ToList();
                            switch (aux[0]) {
                                case "txtCantidad":
                                    if (valor[1] != "")
                                        costo.Cantidad = int.Parse(valor[1]);
                                    else
                                        costo.Cantidad = null;
                                    break;
                                case "hdnTitulo":
                                    costo.Concepto = valor[1];
                                    break;
                                case "txtUnitario":
                                    if (valor[1] != "") {
                                        var precio = valor[1].Replace(".", ",");
                                        costo.PrecioUnitario = decimal.Parse(precio);
                                    }
                                    else
                                        costo.PrecioUnitario = null;
                                    break;
                                case "txtTotal":
                                    if (valor[1] != "") {
                                        var total = valor[1].Replace(".", ",");//aca multiplicar preciounitario por cantidad costo.Cantidad*costo.PrecioUnitario, validar que no esten vacios
                                        costo.Total = decimal.Parse(total);
                                    }
                                    else
                                        costo.Total = null;
                                    break;
                                case "txtObs":
                                    if (valor[1] != "") {
                                        var Observaciones = valor[1];
                                        costo.Observaciones = Observaciones;
                                    }
                                    else
                                        costo.Observaciones = null;
                                    break;
                    
                            }
                        }
                        listcostos.Add(costo);
                    }
                    
                    entity.Costos = listcostos;
                    entity.Cliente = cliente;
                    entity.Descripcion = descripcion;
                    entity.ObservacionAnverso = obsAnverso;
                    entity.ObservacionReverso = obsReverso;
                    //grabarEspecificaciones
                    if (idformulario > 0)
                    {
                        var espViejas = entity.Especificaciones.ToList();
                        dbContext.Especificaciones.RemoveRange(espViejas);
                    }
                    var filasEsp = especificaciones.Split('%').ToList();
                    List<Especificaciones> listEsp = new List<Especificaciones>();
                    for (int i = 1; i < filasEsp.Count(); i++)
                    {
                        var fila = filasEsp[i].Split('-').ToList();
                        Especificaciones esp = new Especificaciones();
                        for (int j = 0; j < fila.Count(); j++)
                        {
                            var valor = fila[j].Split('#').ToList();
                            switch (valor[0])
                            {
                                case "cmbTiene":
                                    esp.Tiene = valor[1];
                                    break;
                                case "hdnTitulo":
                                    esp.Concepto = valor[1];
                                    break;
                                case "txtObs":
                                    esp.Observaciones = valor[1];
                                    break;

                            }
                        }
                        listEsp.Add(esp);
                    }
                    entity.Especificaciones = listEsp;

                    entity.ImprimirSimulacion = imprimirSimulacion;
                    entity.CorregirConSimulacion = corregirConSimulacion;
                    entity.CorregirSinSimulacion = corregirSinSimulacion;
                    entity.Observaciones = obs;
                    entity.RazonSocial = razonSocial;
                    if (paisFact != "")
                        entity.IDPaisFact = int.Parse(paisFact);
                    else
                        entity.IDPaisFact = null;
                    if (provinciaFact != "")
                        entity.IDProvinciaFact = int.Parse(provinciaFact);
                    else
                        entity.IDProvinciaFact = null;
                    if (ciudadFact != "" && provinciaFact != "")
                        entity.IDCiudadFact = int.Parse(ciudadFact);
                    else
                        entity.IDCiudadFact = null;
                    entity.DomicilioFiscal = domFiscal;
                    entity.CUIT = cuit;
                    entity.CondicionIVA = condIVA;
                    entity.Contacto = contacto;
                    entity.TelContacto = telContacto;
                    entity.EmailContacto = emailContacto;
                    entity.ObservacionesDatosFact = obsDatosFact;
                    entity.FormaPago = formaPago;
                    if (fechaEnvioProv != null && fechaEnvioProv != "")
                    {
                        entity.FechaEnvioProveedor = DateTime.Parse(fechaEnvioProv);
                        TimeSpan span = DateTime.Now.Subtract(entity.FechaEnvioProveedor.Value);
                        var dias = Convert.ToInt32(span.TotalDays);
                        entity.Counter = dias;

                    }
                    else
                    {
                        entity.FechaEnvioProveedor = null;
                        entity.Counter = null;
                    }
                    entity.Nombre = nombre;
                    entity.DNI = dni;
                    entity.Domicilio = domicilio;
                    entity.Localidad = localidad;
                    entity.CP = cp;
                    if (provincia != "")
                        entity.IDProvincia = int.Parse(provincia);
                    else
                        entity.IDProvincia = null;
                    if (fechaEnvio != null && fechaEnvio != "")
                        entity.FechaEnvio = DateTime.Parse(fechaEnvio);
                    else
                        entity.FechaEnvio = null;

                    entity.Empresa = empresa;
                    entity.NroGuia = nroGuia;
                    entity.FormaEnvio = formaEnvio;
                    entity.logo = logo;
                    entity.imagenes = imagenes;
                    entity.anverso = anverso;
                    entity.reverso = reverso;
                    entity.aprobacionCliente = aprobacionCliente;
                    entity.comprobante = comprobante;
                    entity.formularioAprobacion = formularioAprobacion;
                    entity.FacturaAnticipo = facturaAnticipo;
                    entity.baseTracks = baseTracks;
                    entity.importeFactura = importeFactura;
                    entity.adjuntarGuia = adjuntarGuia;
                    entity.Telefono = telefono;
                    entity.PaginaWeb = paginaWeb;
                    entity.Email = email;
                    entity.Facebook = facebook;
                    entity.BasesYCondiciones = basesycondiciones;
                    entity.LogoDatReverso = logoDatosReverso;

                    if (idformulario > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.FormularioTarjetas.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

    }

    protected void uploadLogo(object sender, EventArgs e)
    {
        try
        {
            var button = (sender as Control).ClientID;
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "logo_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

                flpLogo.SaveAs(path);
                logo = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");

        }
    }
    
    protected void uploadImagenes(object sender, EventArgs e)
    {
        try
        {
            var extension = flpImagenes.FileName.Split('.')[1].ToLower();
            

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "imagenes_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

                flpImagenes.SaveAs(path);



                imagenes = uniqueName;
            
          
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadAnverso(object sender, EventArgs e)
    {
        try
        {
            var extension = flpAnverso.FileName.Split('.')[1].ToLower();
          

                string ext = System.IO.Path.GetExtension(flpAnverso.FileName);
                string uniqueName = "anverso_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

                flpAnverso.SaveAs(path);
                anverso = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadReverso(object sender, EventArgs e)
    {
        try
        {
            var extension = flpReverso.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpReverso.FileName);
            string uniqueName = "reverso_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);
            
            flpReverso.SaveAs(path);
            
            
            
            reverso = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadGuia(object sender, EventArgs e)
    {
        try
        {
            var extension = flpGuia.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpGuia.FileName);
            string uniqueName = "guia_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

            flpGuia.SaveAs(path);

            adjuntarGuia = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadImporte(object sender, EventArgs e)
    {
        try
        {
            var extension = flpImporte.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpImporte.FileName);
            string uniqueName = "imagenes_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);
            
            flpImporte.SaveAs(path);
            importeFactura = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadBaseTracks(object sender, EventArgs e)
    {
        try
        {
            var extension = flpBaseTracks.FileName.Split('.')[1].ToLower();

            string ext = System.IO.Path.GetExtension(flpBaseTracks.FileName);
            string uniqueName = "base_tracks_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

            flpBaseTracks.SaveAs(path);



            baseTracks = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadFormulario(object sender, EventArgs e)
    {
        try
        {
            var extension = flpFormulario.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpFormulario.FileName);
            string uniqueName = "formulario_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);
            
            flpFormulario.SaveAs(path);
            formularioAprobacion = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadFacturaAnticipo(object sender, EventArgs e)
    {
        try
        {
            var extension = flpFacturaAnticipo.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpFacturaAnticipo.FileName);
            string uniqueName = "formulario_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);

            flpFacturaAnticipo.SaveAs(path);
            facturaAnticipo = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadComprobante(object sender, EventArgs e)
    {
        try
        {
            var extension = flpComprobante.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpComprobante.FileName);
            string uniqueName = "comprobante_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);
            flpComprobante.SaveAs(path);
            comprobante = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }
    
    protected void uploadAprobacion(object sender, EventArgs e)
    {
        try
        {
            var extension = flpAprobacion.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpAprobacion.FileName);
            string uniqueName = "aprobacion_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/formulario/"), uniqueName);
            flpAprobacion.SaveAs(path);
            aprobacionCliente = uniqueName;
        }
        catch (Exception ex)
        {
            throw new Exception("No se pudo guardar la imagen");
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void eliminarImagen(int id, string archivo, string div)
    {
               if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var file = "//files//formulario//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.FormularioTarjetas.Where(x => x.IDFormularioTarjetas == id).FirstOrDefault();
                        if (entity != null)
                        {
                            switch (div)
                            {
                                case "divLogo":
                                    logo = "";
                                    entity.logo = "";
                                    break;
                                case "divImagenes":
                                    imagenes = "";
                                    entity.imagenes = "";
                                    break;
                                case "divAnverso":
                                    anverso = "";
                                    entity.anverso = "";
                                    break;
                                case "divReverso":
                                    reverso = "";
                                    entity.reverso = "";
                                    break;
                                case "divAprobacionCliente":
                                    aprobacionCliente = "";
                                    entity.aprobacionCliente = "";
                                    break;
                                case "divComprobante":
                                    comprobante = "";
                                    entity.comprobante = "";
                                    break;
                                case "divFormularioAprobacion":
                                    formularioAprobacion = "";
                                    entity.formularioAprobacion = "";
                                    break;
                                case "divFacturaAnticipo":
                                    facturaAnticipo = "";
                                    entity.FacturaAnticipo = "";
                                    break;
                                case "divBaseTracks":
                                    baseTracks = "";
                                    entity.baseTracks = "";
                                    break;
                                case "divImporteFactura":
                                    importeFactura = "";
                                    entity.importeFactura = "";
                                    break;
                                case "divGuia":
                                    adjuntarGuia = "";
                                    entity.adjuntarGuia = "";
                                    break;
                            }

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    private void CargarCombos()
    {
        ddlProvincia.DataSource = Common.LoadProvincias();
        ddlProvincia.DataTextField = "Nombre";
        ddlProvincia.DataValueField = "ID";
        ddlProvincia.DataBind();

        ddlPaisFact.DataSource = Common.LoadPaises();
        ddlPaisFact.DataTextField = "Nombre";
        ddlPaisFact.DataValueField = "ID";
        ddlPaisFact.DataBind();
        
        ddlProvinciaFact.DataSource = Common.LoadProvincias();
        ddlProvinciaFact.DataTextField = "Nombre";
        ddlProvinciaFact.DataValueField = "ID";
        ddlProvinciaFact.DataBind();
        
       
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            this.ddlFranquicias.Items.Add(new ListItem(usu.Franquicia, usu.IDFranquicia.ToString()));
            ddlFranquicias.Enabled = false;
        }
        else
        {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
        }
        //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));"
    }


    private void loadInfo(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.FormularioTarjetas.Where(x => x.IDFormularioTarjetas == id).FirstOrDefault();

                if (entity != null)
                {
                    txtFecha.Text = entity.Fecha.HasValue ? entity.Fecha.Value.ToString("dd/MM/yyyy") : "";
                    ddlEstado.SelectedValue = entity.Estado;
                    ddlUnidadNegocio.SelectedValue = entity.UnidadNegocio;
                    ddlFranquicias.SelectedValue = entity.IDFranquicia.ToString();
                    txtCliente.Text = entity.Cliente;
                    txtDescripcion.Text = entity.Descripcion;
                    txtObservacionesAnverso.Text = entity.ObservacionAnverso;
                    txtObservacionesReverso.Text = entity.ObservacionReverso;
                    chkImprimirSegunSimulacion.Checked = entity.ImprimirSimulacion ?? false;
                    chkCorregirConSimulacion.Checked = entity.CorregirConSimulacion ?? false;
                    chkCorregirSinSimulacion.Checked = entity.CorregirSinSimulacion ?? false;
                    txtObservaciones.Text = entity.Observaciones;
                    txtRazonSocial.Text = entity.RazonSocial;
                    ddlPaisFact.SelectedValue=entity.IDPaisFact.ToString();
                    // cargar combo provincia segun pais 
                    if (entity.IDPaisFact > 0 )
                    {
                        ddlProvinciaFact.DataSource = Common.LoadProvinciasByPais(entity.IDPaisFact??0);
                        ddlProvinciaFact.DataTextField = "Nombre";
                        ddlProvinciaFact.DataValueField = "ID";
                        ddlProvinciaFact.DataBind();
                        ddlProvinciaFact.Items.Insert(0, new ListItem("", ""));

                        ddlProvinciaFact.SelectedValue = entity.IDProvinciaFact.ToString();
                    }
                    else
                    {
                        ddlProvinciaFact.DataSource = Common.LoadProvinciasByPais(1);
                        ddlProvinciaFact.DataTextField = "Nombre";
                        ddlProvinciaFact.DataValueField = "ID";
                        ddlProvinciaFact.DataBind();
                        ddlProvinciaFact.Items.Insert(0, new ListItem("", ""));

                    }
                    
                    //cargar combo ciudades segun provincia
                    if (entity.IDProvinciaFact > 0)
                    {
                        ddlCiudadFact.DataSource = Common.LoadCiudades(entity.IDProvinciaFact ?? 0);
                        ddlCiudadFact.DataTextField = "Nombre";
                        ddlCiudadFact.DataValueField = "ID";
                        ddlCiudadFact.DataBind();
                        ddlCiudadFact.Items.Insert(0, new ListItem("", ""));

                        ddlCiudadFact.SelectedValue = entity.IDCiudadFact.ToString();
                    }
                    txtDomFiscal.Text = entity.DomicilioFiscal;
                    txtCuit.Text = entity.CUIT;
                    ddlIVA.SelectedValue = entity.CondicionIVA;
                    txtContacto.Text = entity.Contacto;
                    txtTelContacto.Text = entity.TelContacto;
                    txtEmailContacto.Text = entity.EmailContacto;
                    txtObservacionesDatosFacturacion.Text = entity.ObservacionesDatosFact;
                    ddlFormaDePago.SelectedValue = entity.FormaPago;
                    txtFechaEnvioProv.Text = entity.FechaEnvioProveedor.HasValue ? entity.FechaEnvioProveedor.Value.ToString("dd/MM/yyyy") : "";
                    txtNombre.Text = entity.Nombre;
                    txtDNI.Text = entity.DNI;
                    txtDomicilio.Text = entity.Domicilio;
                    txtLocalidad.Text = entity.Localidad;
                    txtCP.Text = entity.CP;
                    ddlProvincia.SelectedValue = entity.IDProvincia.HasValue ? entity.IDProvincia.ToString() : "";
                    txtFechaEnvio.Text = entity.FechaEnvio.HasValue ? entity.FechaEnvio.Value.ToString("dd/MM/yyyy") : "";
                    txtEmpresa.Text = entity.Empresa;
                    txtNroGuia.Text = entity.NroGuia;
                    ddlFormaEnvio.SelectedValue = entity.FormaEnvio;
                    txtTelefono.Text = entity.Telefono;
                    txtPaginaWeb.Text = entity.PaginaWeb;
                    txtEmail.Text = entity.Email;
                    txtFacebook.Text = entity.Facebook;
                    txtBasesYCondiciones.Text = entity.BasesYCondiciones;
                    chkLogo.Checked = entity.LogoDatReverso ?? false;
                    
                    if(entity.FechaEnvioProveedor!=null ){
                        
                          var dias = entity.Counter;
                          if (dias.HasValue)
                          {
                              if (dias <= 20)
                              {
                                  spnCounterDias.InnerHtml = "<span class='label label-verde'>" + dias.ToString() + "</span>";

                              }
                              else if (dias >= 21 && dias <= 30)
                              {

                                  spnCounterDias.InnerHtml = "<span class='label label-amarillo'>" + dias.ToString() + "</span>";
                              }
                              else if (dias > 30)
                              {
                                  spnCounterDias.InnerHtml = "<span class='label label-rojo'>" + dias.ToString() + "</span>";
                              }
                          }
                          
                        
                        }

                    

                    if (!string.IsNullOrEmpty(entity.logo))
                    {
                        lnkLogo.NavigateUrl = "/files/formulario/" + entity.logo;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.logo + "', 'divLogo')");
                        divLogo.Visible = true;
                        logo = entity.logo;
                    }
                    if (!string.IsNullOrEmpty(entity.imagenes))
                    {
                        lnkImagenes.NavigateUrl = "/files/formulario/" + entity.imagenes;
                        lnkImagenesDelete.NavigateUrl = "javascript: void(0)";
                        lnkImagenesDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.imagenes + "', 'divImagenes')");
                        divImagenes.Visible = true;
                        imagenes = entity.imagenes;
                    }
                    if (!string.IsNullOrEmpty(entity.anverso))
                    {
                        lnkAnverso.NavigateUrl = "/files/formulario/" + entity.anverso;
                        lnkAnversoDelete.NavigateUrl = "javascript: void(0)";
                        lnkAnversoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.anverso + "', 'divAnverso')");
                        divAnverso.Visible = true;
                        anverso = entity.anverso;
                    }
                    if (!string.IsNullOrEmpty(entity.reverso))
                    {
                        lnkReverso.NavigateUrl = "/files/formulario/" + entity.reverso;
                        lnkReversoDelete.NavigateUrl = "javascript: void(0)";
                        lnkReversoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.reverso + "', 'divReverso')");
                        divReverso.Visible = true;
                        reverso = entity.reverso;
                    }
                    if (!string.IsNullOrEmpty(entity.comprobante))
                    {
                        lnkComprobante.NavigateUrl = "/files/formulario/" + entity.comprobante;
                        lnkComprobanteDelete.NavigateUrl = "javascript: void(0)";
                        lnkComprobanteDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.comprobante + "', 'divComprobante')");
                        divComprobante.Visible = true;
                        comprobante = entity.comprobante;
                    }
                    if (!string.IsNullOrEmpty(entity.formularioAprobacion))
                    {
                        lnkFormularioAprobacion.NavigateUrl = "/files/formulario/" + entity.formularioAprobacion;
                        lnkFormularioAprobacionDelete.NavigateUrl = "javascript: void(0)";
                        lnkFormularioAprobacionDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.formularioAprobacion + "', 'divFormularioAprobacion')");
                        divFormularioAprobacion.Visible = true;
                        formularioAprobacion = entity.formularioAprobacion;
                    }
                    if (!string.IsNullOrEmpty(entity.FacturaAnticipo))
                    {
                        lnkFacturaAnticipo.NavigateUrl = "/files/formulario/" + entity.FacturaAnticipo;
                        lnkFacturaAnticipoDelete.NavigateUrl = "javascript: void(0)";
                        lnkFacturaAnticipoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.FacturaAnticipo + "', 'divFacturaAnticipo')");
                        divFacturaAnticipo.Visible = true;
                        facturaAnticipo = entity.FacturaAnticipo;
                    }
                    if (!string.IsNullOrEmpty(entity.baseTracks))
                    {
                        lnkBaseTracks.NavigateUrl = "/files/formulario/" + entity.baseTracks;
                        lnkBaseTracksDelete.NavigateUrl = "javascript: void(0)";
                        lnkBaseTracksDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.baseTracks + "', 'divBaseTracks')");
                        divBaseTracks.Visible = true;
                        baseTracks = entity.baseTracks;
                    }
                    if (!string.IsNullOrEmpty(entity.importeFactura))
                    {
                        lnkImporteFactura.NavigateUrl = "/files/formulario/" + entity.importeFactura;
                        lnkImporteFacturaDelete.NavigateUrl = "javascript: void(0)";
                        lnkImporteFacturaDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.importeFactura + "', 'divImporteFactura')");
                        divImporteFactura.Visible = true;
                        importeFactura = entity.importeFactura;
                    }
                    if (!string.IsNullOrEmpty(entity.NroGuia))
                    {
                        lnkGuia.NavigateUrl = "/files/formulario/" + entity.NroGuia;
                        lnkGuiaDelete.NavigateUrl = "javascript: void(0)";
                        lnkGuiaDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.NroGuia + "', 'divGuia')");
                        divGuia.Visible = true;
                        adjuntarGuia = entity.NroGuia;
                    }

                    
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }
}