﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="importarUsuarios.aspx.cs" Inherits="common_importarUsuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">


    <style>

        .form-group:hover{
            background: #cbe49b;
        }

    </style>


    <nav>   
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li>Importación de socios</li>
            </ul>
        </div>
    </nav>


    <div class="row">


        <div>


        </div>

        
    
        <form runat="server" id="formImportar" class="form-horizontal" role="form">

          

        <%--ACTIVIDADES--%>

            <h3 class="heading">Importación de Actividades y Beneficiarios</h3>
            
        
            
            <div class="alert alert-danger alert-dismissable" id="pnlError" runat="server" visible="false">
                <asp:Literal runat="server" ID="litError"></asp:Literal>
            </div>

            <div class="alert alert-success alert-dismissable" id="pnlOK" runat="server" visible="false">
                <asp:Literal runat="server" ID="litOk"></asp:Literal>
               
            </div>

  


      <div class="col-sm-12 col-md-6" style="border-right: 1px solid grey;">

            
          <h2>Importar Usuarios-Sedes</h2>

                <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />

      

                <br /> <br />

               <div class="form-group groupHover">
                    <label for="ddlFranquicias" class="col-lg-3 control-label"><span class="f_req">*</span> Archivo CSV Usuarios-Sedes</label>
                    <div class="col-lg-3">
                        <asp:FileUpload runat="server" ID="flpUsuarios" />
                    </div>

                     <div class="col-sm-8 col-sm-offset-2">
                        <asp:Button runat="server" ID="btnUploadUsuarios" CssClass="btn btn-success" Text="Importar Usuarios" OnClick="ImportarUsuarios" />
                    </div>

                </div>



                <br /> <br />


            </div>

    
    
      </form>

    </div>


    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle">Detalle de errores</h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>Affinity</th> 
                                <th>Nro Tarjeta</th> 
                                <th>Socio</th> 
                                <th>Error</th> 
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/importarBeneficiarios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>