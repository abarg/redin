﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PromocionesPuntualese.aspx.cs" Inherits="common_PromocionesPuntualese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>   
    

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>">Promociones</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

     <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Promociones</h3>
           <div class="alert alert-danger alert-dismissable" id="div2" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="div3" style="display: none">Los datos se han actualizado correctamente.</div>
                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <br />

                    <div class="form-group" id="divComercios" runat="server">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Comercios</label>
                        <div class="col-lg-9">
                            <asp:DropDownList runat="server" ID="searchable" multiple="multiple" ClientIDMode="Static" DataTextField="NombreFantasia" DataValueField="IDComercio" />
                            <br />
                            <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Fecha desde</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan required" MaxLength="10" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Fecha hasta</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan required" MaxLength="10" />
                        </div>
                    </div>
                    <div class="form-group admin">
                        <label for="txtArancel" class="col-lg-2 control-label"><span class="f_req">*</span> Arancel %</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtArancel" CssClass="form-control required small" MaxLength="100" />
                        </div>
                    </div>
                     <div class="form-group admin">
                        <label for="txtDescuentoVip" class="col-lg-2 control-label">Arancel Vip %</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtArancelVip" CssClass="form-control small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group admin">
                        <label for="txtPuntos" class="col-lg-2 control-label"><span class="f_req">*</span>  Puntos %</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtPuntos" CssClass="form-control required small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group admin">
                        <label for="txtMulPuntos" class="col-lg-2 control-label"><span class="f_req">*</span>  Mult. Puntos</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtMulPuntos" CssClass="form-control required small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtDescuento" class="col-lg-2 control-label"><span class="f_req">*</span>  Descuento</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtDescuento" CssClass="form-control required small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtDescuentoVip" class="col-lg-2 control-label">Descuento Vip</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtDescuentoVip" CssClass="form-control small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtPuntosVip" class="col-lg-2 control-label"> Puntos Vip %</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtPuntosVip" CssClass="form-control small" MaxLength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtMulPuntosVip" class="col-lg-2 control-label"> Mult. puntos Vip</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtMulPuntosVip" CssClass="form-control small" MaxLength="100" />
                        </div>
                    </div>
                    <asp:HiddenField runat="server" ID="hdnIDPromocionPuntual" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                            <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                            <a href="PromocionesPuntuales.aspx" class="btn btn-link">Cancelar</a>
                        </div>
                   </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/PromocionesPuntualese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
