﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="keywordse.aspx.cs" Inherits="common_keywordse" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Cupon IN</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/cuponin/keywords.aspx") %>">Keywords</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Keyword</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />

                <div class="form-group">
                    <label for="u_Precio" class="col-lg-3 control-label"><span class="f_req">*</span> Comercio</label>
                    <div class="col-lg-6">
                         <asp:DropDownList runat="server" ID="ddlComercio" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un comercio"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Código (ej: CINE)</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control required" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Vigencia desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Vigencia hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" runat="server" id="divTarjetasDesde">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Nro Tarjetas desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtTarjetasDesde" CssClass="form-control required" MaxLength="20" MinLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" runat="server" id="divTarjetasHasta">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Nro Tarjetas hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtTarjetasHasta" CssClass="form-control required" MaxLength="20" MinLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" runat="server" id="divStock">
                    <label class="col-lg-3 control-label">Stock</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtStock" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Respuesta</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtRespuesta" CssClass="form-control required alphanumeric rtaMaxTar" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        <small id="litXtarj" runat="server" >En el caso de usar rango de tarjetas, incluir la palabra "XTARJETAX"</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Respuesta Sin Stock</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtRespuestaSinStock" CssClass="form-control required alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Respuesta Vencido</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtRespuestaVencido" CssClass="form-control required alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" runat="server" id="divActivo">
                    <label class="col-lg-3 control-label">Activo</label>
                    <div class="checkbox-inline" style="margin-left: 15px">
                        <asp:CheckBox runat="server" ID="chkActivo" Text="Si" />
                    </div>
                </div>
                <div class="form-group" runat="server" id="divEstado">
                    <label for="u_Precio" class="col-lg-3 control-label"><span class="f_req">*</span>Estado</label>
                    <div class="col-lg-3">
                         <asp:DropDownList CssClass="form-control required"  runat="server" ID="ddlEstado">
                            <asp:ListItem Text="Confirmado" Value="Confirmado"></asp:ListItem>
                            <asp:ListItem Text="Pendiente" Value="Pendiente"></asp:ListItem>
                         </asp:DropDownList>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a href="keywords.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.jqEasyCharCounter.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/cuponin/keywordse.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
