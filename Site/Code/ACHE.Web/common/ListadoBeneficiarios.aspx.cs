﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ListadoBeneficiarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
            cargarSedes();

            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicia.Value = usu.IDFranquicia.ToString();
                btnMasAccciones.Visible = false;
            }
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                hdnIDMarca.Value = usu.IDMarca.ToString();
                btnMasAccciones.Visible = false;
            }
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                hdnIDMultimarca.Value = usu.IDMultimarca.ToString();
                btnMasAccciones.Visible = false;
            }
        }
    }

    private void cargarCombos()
    {
        try{

            using (var dbContext = new ACHEEntities())
            {

                var actividades = dbContext.Actividades.OrderBy(x => x.Actividad).Select(x => new { IDActividad = x.IDActividad, Actividad = x.Actividad.ToUpper() }).ToList();
                if (actividades != null)
                {
                    ddlActividades.DataSource = actividades;
                    ddlActividades.DataTextField = "Actividad";
                    ddlActividades.DataValueField = "Actividad";
                    ddlActividades.DataBind();
                    ddlActividades.Items.Insert(0, new ListItem("", ""));
                }


                string sqlLocalidades = @"select distinct C.Nombre from ActividadSocioNivel as ASN
											join Socios as S on S.IDSocio = ASN.IDSocio
                                            join Domicilios as D on D.IDDomicilio = S.IDDomicilio
                                            join Ciudades as C on C.IDCiudad = D.Ciudad";


                var localidadesData = dbContext.Database.SqlQuery<Combo>(sqlLocalidades).ToList();
                if(localidadesData != null)
                {
                    localidades.DataSource = localidadesData;
                    localidades.DataTextField = "Nombre";
                    localidades.DataValueField = "Nombre";
                    localidades.DataBind();
                    localidades.Items.Insert(0, new ListItem("", ""));
                }


                // CARGO HORARIOS


                string sqlHorarios = @"select distinct AH.Horario as Nombre from ActividadHorarios as AH
							                join Actividades as A on A.IDActividad = AH.IDActividad
                                            join ActividadComercio as AC on AC.IDActividad = A.IDActividad";


                var horariosData = dbContext.Database.SqlQuery<Combo>(sqlHorarios).ToList();
                if (horariosData != null)
                {
                    ddlHorarios.DataSource = horariosData;
                    ddlHorarios.DataTextField = "Nombre";
                    ddlHorarios.DataValueField = "Nombre";
                    ddlHorarios.DataBind();
                    ddlHorarios.Items.Insert(0, new ListItem("", ""));
                }

            }




        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public class Combo {
        public string Nombre { get; set; }
        public int ID { get; set; }
    }

    private void cargarSedes()
    {
        try
        {

            using (var dbContext = new ACHEEntities())
            {

                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio where C.NombreFantasia != '-'";


                var sedesData = dbContext.Database.SqlQuery<Combo>(sqlSedes).ToList();
                if (sedesData != null)
                {
                    ddlSedes.DataSource = sedesData;
                    ddlSedes.DataTextField = "Nombre";
                    ddlSedes.DataValueField = "Nombre";
                    ddlSedes.DataBind();
                    ddlSedes.Items.Insert(0, new ListItem("", ""));
                }

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null )
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {

        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }

        if (idFranquicia > 0)
        {

            using (var dbContext = new ACHEEntities())
            {

                var sql = @"select c.Nombre AS Ciudad, S.Nombre, S.Apellido, S.NroDocumento as NumDoc, S.FechaNacimiento, A.Actividad, AN.Nivel, ASUB.SubCategoria, ACAT.Categoria, ASEC.Nombre as Secretaria, S.FechaAlta, S.IDSocio, co.NombreFantasia as Sede, S.Email, S.Celular, S.Telefono, AH.Horario, ASN.FechaInscripcion
                                from socios as S
                                join ActividadSocioNivel as ASN on S.IDSocio = ASN.IDSocio
                                join Actividades as A on ASN.IDActividad = A.IDActividad
                                join ActividadNiveles as AN on AN.IDNivel = ASN.IDNivel
								join ActividadHorarios as AH on AH.IDHorario = ASN.IDHorario
                                join ActividadSubCategorias as ASUB on A.IDSubCategoria = ASUB.IDSubCategoria
                                join ActividadCategorias as ACAT on ASUB.IDCategoria = ACAT.IDCategoria
					            join domicilios as d on d.IDDomicilio = s.IDDomicilio
					            join ciudades as c on c.IDCiudad = d.Ciudad
								join comercios as co on co.IDComercio = asn.IDComercio
                                join ActividadSecretariaCategoria as ASEC on ASEC.IDSecretaria = ACAT.IDSecretaria where A.IDActividad > 1";


                var result = dbContext.Database.SqlQuery<BeneficiariosView>(sql).AsQueryable();

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaInscripcion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaInscripcion <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                bSocio bSocio = new bSocio();
                bSocio.deleteSocio(id);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string nombre,string apellido, string documento, string actividad, string ciudad, string sede, string fechaDesde, string fechaHasta, string horarios)
    {

        string fileName = "Beneficiarios";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var sql = @"select c.Nombre AS Ciudad, S.Nombre, S.Apellido, S.NroDocumento as NumDoc, S.FechaNacimiento, A.Actividad, AN.Nivel, ASUB.SubCategoria, ACAT.Categoria, ASEC.Nombre as Secretaria, S.FechaAlta, S.IDSocio, co.NombreFantasia as Sede, S.Email, S.Celular, S.Telefono, AH.Horario, ASN.FechaInscripcion
                                from socios as S
                                join ActividadSocioNivel as ASN on S.IDSocio = ASN.IDSocio
                                join Actividades as A on ASN.IDActividad = A.IDActividad
                                join ActividadNiveles as AN on AN.IDNivel = ASN.IDNivel
								join ActividadHorarios as AH on AH.IDHorario = ASN.IDHorario
                                join ActividadSubCategorias as ASUB on A.IDSubCategoria = ASUB.IDSubCategoria
                                join ActividadCategorias as ACAT on ASUB.IDCategoria = ACAT.IDCategoria
					            join domicilios as d on d.IDDomicilio = s.IDDomicilio
					            join ciudades as c on c.IDCiudad = d.Ciudad
								join comercios as co on co.IDComercio = asn.IDComercio
                                join ActividadSecretariaCategoria as ASEC on ASEC.IDSecretaria = ACAT.IDSecretaria where A.IDActividad > 1";

                    if (nombre != "")
                        sql += "and S.Nombre like '%" + nombre + "%'";
                    if (apellido != "")
                        sql += "and S.Apellido like '%" + apellido + "%'";
                    if (documento != "")
                        sql += "and S.NroDocumento like '%" + documento + "%'";
     
                    if (ciudad != "null")
                    {

                        var splitted = ciudad.Split(",");


                        sql += "and c.Nombre IN (";

                        for (int i = 0; i < splitted.Length; i++)
                        {
                            if (i != splitted.Length - 1)
                                sql += "'" + splitted[i] + "',";
                            else
                                sql += "'" + splitted[i] + "'";

                        }

                        sql += ")";
                    }


                    if (actividad != "null")
                    {

                        var splitted = actividad.Split(",");


                        sql += " and A.Actividad IN (";

                        for (int i = 0; i < splitted.Length; i++)
                        {
                            if (i != splitted.Length - 1)
                                sql += "'" + splitted[i] + "',";
                            else
                                sql += "'" + splitted[i] + "'";

                        }

                        sql += ")";
                    }


                    if (sede != "null")
                    {

                        var splitted = sede.Split(",");


                        sql += " and co.NombreFantasia IN (";

                        for (int i = 0; i < splitted.Length; i++)
                        {
                            if (i != splitted.Length - 1)
                                sql += "'" + splitted[i] + "',";
                            else
                                sql += "'" + splitted[i] + "'";

                        }

                        sql += ")";
                    }


                    if (horarios != "null")
                    {

                        var splitted = horarios.Split(",");


                        sql += " and AH.Horario IN (";

                        for (int i = 0; i < splitted.Length; i++)
                        {
                            if (i != splitted.Length - 1)
                                sql += "'" + splitted[i] + "',";
                            else
                                sql += "'" + splitted[i] + "'";

                        }

                        sql += ")";

                    }


                    sql += ";";

                    var info = dbContext.Database.SqlQuery<BeneficiariosView>(sql).AsQueryable();

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaInscripcion >= dtDesde);
                    }

                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.FechaInscripcion <= dtHasta);
                    }

                    dt = info.Select(x => new
                    {
 
                        Ciudad = x.Ciudad,
                        Nombre = x.Nombre,
                        Apellido = x.Apellido,
                        NroDocumento = x.NumDoc,
                        FechaNacimiento = x.FechaNacimiento,
                        Actividad = x.Actividad,
                        Nivel = x.Nivel,
                        SubCategoria = x.SubCategoria,
                        Categoria = x.Categoria,
                        Secretaria = x.Secretaria,
                        FechaAlta = x.FechaAlta,
                        FechaInscripcion = x.FechaInscripcion ?? DateTime.Now,
                        Sede = x.Sede,
                        Horario = x.Horario,
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        Email = x.Email,

                    }).ToList().ToDataTable();

                }

            

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            return GetTrBySocio(id, null);
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            return GetTrBySocioMarca(id, null);

        return null;

    }
    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesView.Where(x => x.IDSocio.HasValue && x.IDSocio == id)
                    .OrderBy(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel()
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        Marca = x.Marca,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,

                    });

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }
    private static List<TransaccionesViewModel> GetTrBySocioMarca(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

            int idMarca = usu.IDMarca;
            var marca = usu.Marca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesMarcasView.Where(x => x.IDSocio.HasValue && x.IDSocio == id
                     && x.IDMarca == idMarca && x.ImporteOriginal > 1)
                     .OrderBy(x => x.FechaTransaccion).Select(x => new
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Marca = marca,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0,
                         ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0
                     });


                if (max.HasValue)
                    result = result.OrderByDescending(x => x.FechaTransaccion).Take(max.Value);

                list = result.Select(x => new TransaccionesViewModel
                {
                    Fecha = x.Fecha,
                    FechaTransaccion = x.FechaTransaccion,
                    Hora = x.Hora,
                    Operacion = x.Operacion,
                    Origen = x.Origen,
                    SDS = x.SDS,
                    Comercio = x.Comercio,
                    NroEstablecimiento = x.NroEstablecimiento,
                    //POSTerminal = x.POSTerminal,
                    Marca = marca,
                    Tarjeta = x.Tarjeta,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal,
                    ImporteAhorro = x.ImporteAhorro
                }).ToList();
            }
        }
        return list;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> sedesPorLocalidades(string localidades) {

        List<ComboViewModel> listSedes = new List<ComboViewModel>();

        if(localidades != string.Empty) {


            var splitted = localidades.Split(",");


            using (var dbContext = new ACHEEntities()) {



                var sql = @"select NombreFantasia as Nombre from comercios AS C
                            join Domicilios AS D on C.IDDomicilio = D.IDDomicilio
                            join Ciudades AS CI ON CI.IDCiudad = D.Ciudad
                            where IDFranquicia = 43 and CI.Nombre IN ( ";

                for (int i = 0; i < splitted.Length; i++)
                {
                    if(i != splitted.Length - 1)
                     sql += "'" + splitted[i] + "',";
                    else
                     sql += "'" + splitted[i] + "'";

                }



                sql += ");";



                listSedes = dbContext.Database.SqlQuery<ComboViewModel>(sql).ToList();


            }

        }
        else
        {
            using (var dbContext = new ACHEEntities())
            {

                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio";




                listSedes = dbContext.Database.SqlQuery<ComboViewModel>(sqlSedes).ToList();


            }
        }

        return listSedes;       
    }


    [WebMethod(true)]
    public static List<ComboViewModel> actividadesPorSedes(string sedes)
    {

        List<ComboViewModel> listActividades = new List<ComboViewModel>();

        if (sedes != string.Empty)
        {


            var splitted = sedes.Split(",");


            using (var dbContext = new ACHEEntities())
            {



                var sql = @"select distinct A.Actividad as Nombre from ActividadComercio AS AC
                            join Comercios as C on C.IDComercio = AC.IDComercio
							join Actividades as A on A.IDActividad = AC.IDActividad
							where C.NombreFantasia IN ( ";

                for (int i = 0; i < splitted.Length; i++)
                {
                    if (i != splitted.Length - 1)
                        sql += "'" + splitted[i] + "',";
                    else
                        sql += "'" + splitted[i] + "'";

                }



                sql += ");";



                listActividades = dbContext.Database.SqlQuery<ComboViewModel>(sql).ToList();


            }

        }
        else
        {

            using(var dbContext = new ACHEEntities())
                 listActividades = dbContext.Actividades.OrderBy(x => x.Actividad).Select(x => new ComboViewModel{ ID = x.IDActividad.ToString(), Nombre = x.Actividad.ToUpper() }).ToList();

        }

        return listActividades;
    }


    [WebMethod(true)]
    public static List<ComboViewModel> horariosPorActividades(string actividades)
    {

        List<ComboViewModel> listHorarios = new List<ComboViewModel>();

        int idEmpresa = 0;
        int idFranquicia = 0;
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            WebEmpresasUser usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
            idEmpresa = usu.IDEmpresa;
        }

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
        }

        if (idEmpresa > 0 || idFranquicia > 0)
        {

            if (actividades != string.Empty)
            {


                var splitted = actividades.Split(",");


                using (var dbContext = new ACHEEntities())
                {

                    var empresasComercios = dbContext.EmpresasComercios.Where(x => x.IDEmpresa == idEmpresa).Select(x => x.IDComercio).ToArray();


                    var sql = @"select distinct AH.Horario as Nombre from ActividadHorarios as AH
							    join Actividades as A on A.IDActividad = AH.IDActividad
                                where A.Actividad IN ( ";

                    for (int i = 0; i < splitted.Length; i++)
                    {
                        if (i != splitted.Length - 1)
                            sql += "'" + splitted[i] + "',";
                        else
                            sql += "'" + splitted[i] + "'";

                    }

                    sql += ");";


                    listHorarios = dbContext.Database.SqlQuery<ComboViewModel>(sql).ToList();


                }

            }
            else
            {

                using (var dbContext = new ACHEEntities())
                {
                    var idsComerciosHabilitados = dbContext.EmpresasComercios.Where(x => x.IDEmpresa == idEmpresa).Select(x => x.IDComercio).ToArray();

                    var sqlActividades = @"select distinct AH.Horario as Nombre from ActividadHorarios as AH
							                join Actividades as A on A.IDActividad = AH.IDActividad
                                            join ActividadComercio as AC on AC.IDActividad = A.IDActividad;";

                    listHorarios = dbContext.Database.SqlQuery<ComboViewModel>(sqlActividades).ToList();

                }
            }
        }

        return listHorarios;
    }

}