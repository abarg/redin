﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;


public partial class common_alertas_seguimiento : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] == null) && (HttpContext.Current.Session["CurrentUser"] == null))
            Response.Redirect("~/home.aspx");
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> buscarTarjetas(string tarjeta) {
        try {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();

            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    list = dbContext.Tarjetas.Where(x => x.Numero.Contains(tarjeta)).OrderBy(x => x.Numero)
                     .Select(x => new Combo2ViewModel {
                         Nombre = x.Numero,
                         ID = x.IDTarjeta
                     }).Take(10).ToList();


                }
            }

            return list;
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void agregarTarjeta(int idTarjeta) {
        try {
            int IDFranquicia = 0;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                    var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    IDFranquicia = usu.IDFranquicia;
                }
                using (var dbContext = new ACHEEntities()) {
                    if (!dbContext.AlertasTarjetas.Any(x => (x.IDFranquicia == IDFranquicia || IDFranquicia == 0) && x.IDTarjeta == idTarjeta)) {
                        AlertasTarjetas alerta = new AlertasTarjetas();
                        alerta.IDFranquicia = IDFranquicia;
                        alerta.IDTarjeta = idTarjeta;

                        dbContext.AlertasTarjetas.Add(alerta);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void eliminarTarjeta(int idTarjeta) {
        try {
            int IDFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                IDFranquicia = usu.IDFranquicia;
            }

                using (var dbContext = new ACHEEntities()) {

                    AlertasTarjetas alerta = dbContext.AlertasTarjetas.FirstOrDefault(x => (x.IDFranquicia == IDFranquicia || IDFranquicia == 0) && x.IDTarjeta == idTarjeta);
                    if (alerta != null) {
                        dbContext.AlertasTarjetas.Remove(alerta);
                        dbContext.SaveChanges();
                    }


                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static List<SociosViewModel> getTarjetasEnSeguimiento() {
        List<SociosViewModel> list = new List<SociosViewModel>();
        int IDFranquicia = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            IDFranquicia = usu.IDFranquicia;
        }

            using (var dbContext = new ACHEEntities()) {
                list = dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => (x.IDFranquicia == IDFranquicia || IDFranquicia == 0))
                 .Select(x => new SociosViewModel {
                     Tarjeta = x.Tarjetas.Numero,
                     Nombre = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Nombre : "",
                     Apellido = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido : "",
                     NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                     IDSocio = x.IDTarjeta
                 }).OrderByDescending(x => x.Apellido).ToList();
            }
        }

        return list;
    }
}