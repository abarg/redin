﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class common_TarjetasAnulacion : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null){
                var marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                hdnIDMarca.Value = marca.IDMarca.ToString();
                }
            //else
            //    this.cargarMarcas();
            //this.getSocios();
            if (Request.QueryString["IDSocio"] != null) {
                //this.ddlSocio.SelectedValue = Request.QueryString["IDSocio"];
                //this.ddlSocio.Enabled = false;
                this.hfIDSocio.Value = Request.QueryString["IDSocio"];
            }
            if (Request.QueryString["IDTarjeta"] != null) {
                this.hfIDTarjeta.Value = Request.QueryString["IDTarjeta"];
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }


    [WebMethod(true)]
    //public static void grabar(string Tipo, string Numero, int Marca, string Motivo, string NumeroNuevo, int IDSocio)
    public static void grabar(string Tipo, string Numero,  string Motivo, string NumeroNuevo, int IDSocio)
    {
        try {
            //Tarjetas
            bool actualizarTr = false;
            var marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            //using (TransactionScope ts = new TransactionScope())
            //{
            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(Numero, false);
            Tarjetas oTarjetaNueva = null;
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null){
               // Marca=((WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"]).IDMarca; 
                if (oTarjeta.IDMarca != marca.IDMarca)
                    throw new Exception("El Número de Tarjeta no existe");
                //else
                   // Marca = marca.IDMarca;
            }

            if (oTarjeta == null) throw new Exception("El Número de Tarjeta no existe");

            if (Tipo.Equals("D")) //Desvinculación
            {
                if (!oTarjeta.IDSocio.HasValue)
                    throw new Exception("La Tarjeta no tiene un socio asignado");

                oTarjeta.IDSocio = null;
                bTarjeta.add(oTarjeta);
            }
            else if (Tipo.Equals("A")) //Anulación
            {
                oTarjeta.Estado = "B";
                oTarjeta.FechaBaja = DateTime.Now;
                oTarjeta.MotivoBaja = Motivo;
                if (HttpContext.Current.Session["CurrentUser"] != null) {
                    var Usuarios = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }
                else if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                    var Usuarios = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }

                bTarjeta.add(oTarjeta);
            }
            else if (Tipo.Equals("S")) //Sustitución
            {
                //if (oTarjeta.IDSocio != IDSocio)
                //    throw new Exception("La Tarjeta no corresponde al Socio ingresado");

                //int PuntosDisponibles = oTarjeta.PuntosDisponibles.HasValue ? oTarjeta.PuntosDisponibles.Value : 0;
                int PuntosTotales = oTarjeta.PuntosTotales;

                oTarjetaNueva = bTarjeta.getTarjetaPorNumero(NumeroNuevo, false);
                if (oTarjetaNueva == null)
                    throw new Exception("El Nuevo Número de Tarjeta no existe");
                if (oTarjetaNueva.IDSocio.HasValue && oTarjetaNueva.IDSocio != oTarjeta.IDSocio)
                    throw new Exception("La Nueva Tarjeta tiene un Socio vinculado");

                oTarjeta.Estado = "B";
                oTarjeta.FechaBaja = DateTime.Now;
                oTarjeta.MotivoBaja = Motivo;
                var Usuarios = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                oTarjeta.UsuarioBaja = Usuarios.Usuario;
                oTarjeta.PuntosTotales = 0;

                decimal credito = oTarjeta.Credito;
                decimal giftcard = oTarjeta.Giftcard;

                oTarjeta.Credito = 0;
                oTarjeta.Giftcard = 0;

                bTarjeta.add(oTarjeta);

                oTarjetaNueva.Estado = "A";
                oTarjetaNueva.FechaAsignacion = DateTime.Now;
                oTarjetaNueva.IDSocio = oTarjeta.IDSocio;
                oTarjetaNueva.IDMarca = marca.IDMarca;
                oTarjetaNueva.IDFranquicia = oTarjeta.IDFranquicia;
                //oTarjetaNueva.PuntosDisponibles = PuntosDisponibles;
                oTarjetaNueva.PuntosTotales += PuntosTotales;
                oTarjetaNueva.Credito += credito;
                oTarjetaNueva.Giftcard += giftcard;

                bTarjeta.add(oTarjetaNueva);

            }

            //ts.Complete();

            if (Tipo.Equals("S"))
                actualizarTr = true;
            //}

            if (actualizarTr) {
                using (var dbContext = new ACHEEntities()) {
                    dbContext.ActualizarTrDeTarjeta(Numero, NumeroNuevo);
                    dbContext.ActualizarPuntosPorTarjeta(NumeroNuevo);
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if ( HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"]; ;

                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.SociosView
                                .Where(x => (x.IDMarca == marca.IDMarca) && (x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower())))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();

                            break;
                        case "DNI":
                            list = dbContext.SociosView
                                .Where(x => (x.IDMarca == marca.IDMarca) && (x.IDMarca == marca.IDMarca) && x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas
                                .Where(x => x.Numero.Contains(valor))
                                .OrderBy(x => x.Numero)
                                .Select(x => new Combo3ViewModel()
                                {
                                    ID = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                                    Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                                    Importe = x.Credito.ToString()

                                }).Distinct().OrderBy(x => x.Nombre).Take(10).ToList();                            
                            break;
                    }
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio  && !x.FechaBaja.HasValue)
                     .OrderBy(x => x.Numero)
                     .Select(x => new Combo3ViewModel() { ID = x.IDMarca, Nombre = x.Numero, Importe = x.Credito.ToString() })
                     .Distinct().OrderBy(x => x.Nombre).AsEnumerable().ToList();

                    if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                        var marca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                        list = list.Where(x => x.ID == marca.IDMarca).ToList();
                    }
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}