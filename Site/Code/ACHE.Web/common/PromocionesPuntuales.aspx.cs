﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Model;


public partial class common_PromocionesPuntuales : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            hdnIDMarca.Value =  usuMarca.IDMarca.ToString();
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            MasterPageFile = "~/MasterPageComercios.master";
    }



    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
        int idMarca = 0;
        int idComer = 0;

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usuMarca.IDMarca;
        }

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null) {
            var usuComer = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            idComer = usuComer.IDComercio;
        }

        if (HttpContext.Current.Session["CurrentUser"] != null || idMarca > 0 ||idComer >0) {
            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.PromocionesPuntuales.Include("PromocionesPorComercio").OrderBy(x => x.FechaDesde).ToList();


                if (fechaDesde != string.Empty) {
                    var dtDesde = DateTime.Parse(fechaDesde);
                    list = list.Where(x => x.FechaDesde >= dtDesde).ToList();
                }

                if (fechaHasta != string.Empty) {
                    var dtHasta = DateTime.Parse(fechaHasta);
                    list = list.Where(x => x.FechaHasta <= dtHasta).ToList();
                }

                if (idMarca > 0) {
                    var ds = dbContext.PromocionesPorComercio.Include("Comercios").Where(y => y.Comercios.IDMarca == idMarca).Select(x => x.IDPromocionesPuntuales).ToList();
                    var promos = list.Where(x => ds.Contains(x.IDPromocionesPuntuales)).ToList();
                    list = promos;
                }

                if (idComer > 0) {
                    var ds = dbContext.PromocionesPorComercio.Where(y => y.IDComercio == idComer).Select(x => x.IDPromocionesPuntuales).ToList();
                    var promos = list.Where(x => ds.Contains(x.IDPromocionesPuntuales)).ToList();
                    list = promos;
                }
               
                return list.Select(x => new PromocionesViewModel() {
                    IDPromocion = x.IDPromocionesPuntuales,
                    fechaDesde = x.FechaDesde,
                    fechaHasta = x.FechaHasta,
                    arancel = x.Arancel ?? 0,
                    //arancelVip = (x.ArancelVip != null) ? x.ArancelVip.ToString() : "",
                    puntos = x.Puntos ?? 0,
                    puntosVip = (x.PuntosVip != null) ? x.PuntosVip.ToString() : "",
                    descuento = x.Descuento,
                    descuentoVip = (x.DescuentoVip != null) ? x.DescuentoVip.ToString() : "",
                    mulPuntos = x.MultiplicaPuntos,
                    mulPuntosVip = (x.MultiplicaPuntosVip != null) ? x.MultiplicaPuntosVip.ToString() : ""
                }).AsQueryable().ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {

        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null || HttpContext.Current.Session["CurrentComerciosUser"] != null) {            
            using (var dbContext = new ACHEEntities()) {
                try {
                    var prom = dbContext.PromocionesPuntuales.Where(x => x.IDPromocionesPuntuales == id).FirstOrDefault();
                    if (prom != null) {
                        dbContext.PromocionesPuntuales.Remove(prom);
                        var promPorComercios = dbContext.PromocionesPorComercio.Where(x => x.IDPromocionesPuntuales == id);
                        dbContext.PromocionesPorComercio.RemoveRange(promPorComercios);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e) {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }
}