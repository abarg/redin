﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_ActividadesSubCategoriase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.cargarCategorias();



        if (!String.IsNullOrEmpty(Request.QueryString["IDSubCategoria"]))
        {
            try
            {
                int IDSubCategoria = int.Parse(Request.QueryString["IDSubCategoria"]);
                if (IDSubCategoria > 0)
                {
                    this.hdnIDSubCategoria.Value = IDSubCategoria.ToString();
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDSubCategoria"]));
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ActividadesSubCategorias.aspx");
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMultimarcas.master";

    }


    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.ActividadSubCategorias.Where(x => x.IDSubCategoria == id).FirstOrDefault();
                if (entity != null)
                {

                    this.txtSubCategoria.Text = entity.SubCategoria;
                    this.cmbCategorias.SelectedValue = entity.IDCategoria.ToString();

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.ActividadCategorias.OrderBy(x => x.Categoria).ToList();
            if (categorias != null)
            {
                cmbCategorias.DataSource = categorias;
                cmbCategorias.DataTextField = "Categoria";
                cmbCategorias.DataValueField = "IDCategoria";
                cmbCategorias.DataBind();
                cmbCategorias.Items.Insert(0, new ListItem("", ""));
            }
        }
    }


    [WebMethod(true)]
    public static string grabar(int idSubCategoria, string SubCategoria, int idCategoria)
    {
        try
        {
            //Ciudad
            ActividadSubCategorias entity;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.ActividadSubCategorias.Where(x => x.IDCategoria == idCategoria && x.SubCategoria == SubCategoria && x.IDSubCategoria != idSubCategoria).FirstOrDefault();
                if (aux != null)
                    throw new Exception("Ya existe una subcategoria perteneciente a la categoria con el mismo nombre");

                if (idSubCategoria > 0)
                    entity = dbContext.ActividadSubCategorias.FirstOrDefault(s => s.IDSubCategoria == idSubCategoria);
                else
                    entity = new ActividadSubCategorias();

                entity.SubCategoria = SubCategoria != null && SubCategoria != "" ? SubCategoria.Trim().ToUpper() : "";
                entity.IDCategoria= idCategoria;

                if (idSubCategoria > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ActividadSubCategorias.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDSubCategoria.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


}