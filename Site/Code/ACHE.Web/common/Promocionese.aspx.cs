﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class common_Promocionese : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        
        if (!IsPostBack) {
            try {
                cargarPaises();
                int idMarca = 0;
                int idFran = 0;
                int idAdmin = 0;
                int idPromo = 0;

                if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                    var usuMarca = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    idMarca = usuMarca.IDMarca;
                    this.hdnIDMarca.Value = idMarca.ToString();
                }
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                    var usuFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    idFran = usuFran.IDFranquicia;
                }
                if (HttpContext.Current.Session["CurrentUser"] != null) {
                    var usuAdmin = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    idAdmin = usuAdmin.IDUsuario;
                }

                if (idAdmin > 0 || idFran > 0 || idMarca > 0) {
                    if (!String.IsNullOrEmpty(Request.QueryString["IDPromocion"])) {
                        idPromo = int.Parse(Request.QueryString["IDPromocion"]);
                        if (idPromo > 0) {
                            this.hdnIDPromocion.Value = idPromo.ToString();
                            cargarDatosPromociones(idPromo,idAdmin,idFran,idMarca);
                        }
                    }
                    cargarMarcas(idPromo,idAdmin,idFran,idMarca);
                }
            }

            catch (Exception ex) {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "el error en promocionesE es: ", ex.ToString());
                Response.Redirect("Promociones.aspx");
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarPaises() {
        ddlPais.DataSource = Common.LoadPaises();
        ddlPais.DataTextField = "Nombre";
        ddlPais.DataValueField = "ID";
        ddlPais.DataBind();
        this.ddlPais.Items.Insert(0, new ListItem("", ""));

    }

    private void cargarDatosPromociones(int idPromo,int idFran, int idMarca, int idAdmin) {
        if (idAdmin > 0 || idFran > 0 || idMarca > 0) {
            using (var dbContext = new ACHEEntities()) {
                 var promocion = dbContext.Promociones.Where(x => x.IDPromociones == idPromo).FirstOrDefault();
                if (promocion != null) {
                    this.txtTitulo.Text = promocion.Titulo;
                    this.txtMsj1.Text = promocion.Mensaje1;
                    this.txtMsj2.Text = promocion.Mensaje2;
                    this.txtMsj3.Text = promocion.Mensaje3;
                    this.txtMsj4.Text = promocion.Mensaje4;
                    this.txtTipoCodigo.Text = promocion.TipoCodigo.ToString();
                    this.txtInfoCodificar.Text = promocion.InformacionACodificar;
                    DateTime dtDsd = Convert.ToDateTime(promocion.FechaDesde.ToString());
                    DateTime dtHst = Convert.ToDateTime(promocion.FechaHasta.ToString());
                    this.txtFechaDesde.Text = dtDsd.ToString("dd/M/yyyy");
                    this.txtFechaHasta.Text = dtHst.ToString("dd/M/yyyy");
                    this.chkActivo.Checked = promocion.Activo;
                    this.ddlSexo.SelectedValue = promocion.Sexo;
                    this.txtImporteDesde.Text = promocion.ImporteDesde.HasValue ? promocion.ImporteDesde.ToString() : "0";
                    this.txtImporteHasta.Text = promocion.ImporteHasta.HasValue ? promocion.ImporteHasta.ToString() : "0";

                    if (promocion.Domicilios != null && promocion.Domicilios.IDDomicilio != 0)
                    {
                        var idPais = dbContext.Paises.Where(x => x.Nombre.ToLower() == promocion.Domicilios.Pais.ToString().ToLower()).FirstOrDefault().IDPais;
                        this.ddlPais.SelectedValue = idPais.ToString();

                        ddlProvincia.DataSource = Common.LoadProvinciasByPais(idPais);
                        ddlProvincia.DataTextField = "Nombre";
                        ddlProvincia.DataValueField = "ID";
                        ddlProvincia.DataBind();
                        if (promocion.Domicilios.Provincia != null)
                        {
                            this.ddlProvincia.SelectedValue = promocion.Domicilios.Provincia.ToString();

                            ddlCiudad.DataSource = Common.LoadCiudades(promocion.Domicilios.Provincia);
                            ddlCiudad.DataTextField = "Nombre";
                            ddlCiudad.DataValueField = "ID";
                            ddlCiudad.DataBind();
                            ddlCiudad.Items.Insert(0, new ListItem("", ""));

                            if (promocion.Domicilios.Ciudad != null)
                                this.ddlCiudad.SelectedValue = promocion.Domicilios.Ciudad.ToString();
                            else
                                this.ddlCiudad.SelectedValue = "";
                        }


                        this.txtDomicilio.Text = promocion.Domicilios.Domicilio;
                        this.txtPisoDepto.Text = promocion.Domicilios.PisoDepto;
                    }
                }
            }
        }
    }

    private void cargarMarcas(int idPromo,int idAdmin,int idFran, int idMarca) {


        using (var dbContext = new ACHEEntities()) {
            if (idMarca > 0) {
                //marcas = marcas.Where(x => x.IDMarca == idMarca).OrderBy(x => x.Nombre).ToList();
                this.divMarcas.Visible = false;
            }
            else if (idFran > 0 || idAdmin > 0) {
                var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
                if (idFran > 0) {
                    marcas = marcas.Where(x => x.IDFranquicia == idFran).OrderBy(x => x.Nombre).ToList();
                }
                if (marcas != null) {
                    cmbMarcas.DataSource = marcas;
                    cmbMarcas.DataTextField = "Nombre";
                    cmbMarcas.DataValueField = "IDMarca";
                    cmbMarcas.DataBind();
                    cmbMarcas.Items.Insert(0, new ListItem("", ""));
                }
            }
            if (idPromo > 0) {
                int idMarcaPromocion = dbContext.Promociones.Where(x => x.IDPromociones == idPromo).FirstOrDefault().IDMarca;
                cmbMarcas.SelectedValue = idMarcaPromocion.ToString();
            }
        }
    }

    [WebMethod(true)]
    public static void grabar(int idPromocion, int idMarca, string Titulo, string Msj1, string Msj2, string Msj3, string Msj4, int tipoCodigo, string infoCodificar, string fechaDesde, string fechaHasta, bool activo, string Sexo,string ImporteDesde,string ImporteHasta,string Pais, string Provincia, string Ciudad, string Domicilio, string PisoDepto) {
        try {
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                if (idMarca > 0) {
                    Promociones promocion = new Promociones();
                    using (var dbContext = new ACHEEntities()) {

                        if (idPromocion > 0) {
                            promocion = dbContext.Promociones.Include("Domicilios").Where(x => x.IDPromociones == idPromocion).FirstOrDefault();
                            if (promocion != null) {
                                promocion.IDMarca = idMarca;
                                promocion.Titulo = Titulo;
                                promocion.Mensaje1 = Msj1;
                                promocion.Mensaje2 = Msj2;
                                promocion.Mensaje3 = Msj3;
                                promocion.Mensaje4 = Msj4;
                                promocion.TipoCodigo = tipoCodigo;
                                promocion.InformacionACodificar = infoCodificar;
                                promocion.FechaDesde = DateTime.Parse(fechaDesde);
                                promocion.FechaHasta = DateTime.Parse(fechaHasta);
                                promocion.Sexo = Sexo;
                                promocion.ImporteDesde = int.Parse(ImporteDesde);
                                promocion.ImporteHasta = int.Parse(ImporteHasta);                                    
                                promocion.Activo = activo;

                                //Domicilio
                                if (promocion.IDDomicilio == null || promocion.IDDomicilio == 0) {
                                    promocion.Domicilios = new Domicilios();
                                    promocion.Domicilios.FechaAlta = DateTime.Now;
                                    promocion.Domicilios.Estado = "A";
                                    promocion.Domicilios.TipoDomicilio = "P"; //Promocion
                                    
                                }
                                promocion.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                                promocion.Domicilios.Provincia = int.Parse(Provincia);
                                if (!string.IsNullOrEmpty(Ciudad))
                                    promocion.Domicilios.Ciudad = int.Parse(Ciudad);
                                else
                                    promocion.Domicilios.Ciudad = null;
                                promocion.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                                promocion.Domicilios.PisoDepto = PisoDepto;
                            }
                        }
                        else {
                            promocion.IDMarca = idMarca;
                            promocion.Titulo = Titulo;
                            promocion.Mensaje1 = Msj1;
                            promocion.Mensaje2 = Msj2;
                            promocion.Mensaje3 = Msj3;
                            promocion.Mensaje4 = Msj4;
                            promocion.TipoCodigo = tipoCodigo;
                            promocion.InformacionACodificar = infoCodificar;
                            promocion.FechaDesde = DateTime.Parse(fechaDesde);
                            promocion.FechaHasta = DateTime.Parse(fechaHasta);
                            promocion.Sexo = Sexo;
                            promocion.ImporteDesde = int.Parse(ImporteDesde);
                            promocion.ImporteHasta = int.Parse(ImporteHasta);   
                            promocion.Activo = activo;

                            if (promocion.IDDomicilio == null || promocion.IDDomicilio == 0) {
                                promocion.Domicilios = new Domicilios();
                                promocion.Domicilios.FechaAlta = DateTime.Now;
                                promocion.Domicilios.Estado = "A";
                                promocion.Domicilios.TipoDomicilio = "P"; //Promocion

                            }

                            promocion.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                            promocion.Domicilios.Provincia = int.Parse(Provincia);
                            if (Ciudad != string.Empty)
                                promocion.Domicilios.Ciudad = int.Parse(Ciudad);
                            else
                                promocion.Domicilios.Ciudad = null;
                            promocion.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                            promocion.Domicilios.PisoDepto = PisoDepto;


                            dbContext.Promociones.Add(promocion);
                        }
                        dbContext.SaveChanges();
                    }
                }
                else {
                    throw new Exception("Debe seleccionar una marca");
                }
            }
        }


            catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }

}