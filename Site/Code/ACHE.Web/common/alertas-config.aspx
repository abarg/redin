﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alertas-config.aspx.cs" Inherits="common_alertas_config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Alertas</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Configuración de Alertas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-sm-3 admin" style="display: none">
                            <label>Franquicias</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"/>
                        </div>
                        <div class="col-sm-3 admin" style="display: none">
                            <label>Marcas</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 admin" style="display: none">
                            <label>Empresas</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlEmpresas" />
                        </div>
                        <div class="col-sm-4 admin" style="display: none">
                            <label>Comercios</label>
                            <asp:DropDownList runat="server" class="form-control chosen" ID="ddlComercios" data-placeholder="Seleccione un comercio" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/alertas-config.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>