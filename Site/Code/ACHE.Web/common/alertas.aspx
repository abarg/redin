﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alertas.aspx.cs" Inherits="common_alertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <form runat="server" id="form" class="form-horizontal" role="form">
    <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="0" />
    </form>
    <div class="row admin">
        <div class="col-sm-6 col-md-6 dd_column">
        <div class="w-box">
            <div class="w-box-header">
                Alertas con prioridad
                <div class="pull-right">
                    <span class="label label-rojo">Alta</span>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Franquicia</th>
                            <th>Cantidad</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal runat="server" ID="litAlertasAlta"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">
                    Alertas con prioridad
                    <div class="pull-right">
                        <span class="label label-amarillo">Media</span>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                <th>Franquicia</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal runat="server" ID="litAlertasMedia"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row admin">
        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">
                    Alertas con prioridad
                    <div class="pull-right">
                        <span class="label label-naranja">Baja</span>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                <th>Franquicia</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal runat="server" ID="litAlertasBaja"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row franquicia" style="display:none">
        <div class="col-sm-6 col-md-6 dd_column">
        <div class="w-box">
            <div class="w-box-header">
                Alertas con prioridad
                <div class="pull-right">
                    <span class="label label-rojo">Alta</span>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Entidad</th>
                            <th>Cantidad</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal runat="server" ID="litAlertasAlta2"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">
                    Alertas con prioridad
                    <div class="pull-right">
                        <span class="label label-amarillo">Media</span>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                <th>Entidad</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal runat="server" ID="litAlertasMedia2"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row franquicia" style="display:none">
        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">
                    Alertas con prioridad
                    <div class="pull-right">
                        <span class="label label-naranja">Baja</span>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                <th>Entidad</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal runat="server" ID="litAlertasBaja2"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content" style="width:700px !important">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="modalDetalle2">
		<div class="modal-dialog">
			<div class="modal-content" style="width:700px !important">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle2"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle2">
							
						</thead>
						<tbody id="bodyDetalle2">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle2').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
    <div class="modal fade" id="modalDetalle3">
		<div class="modal-dialog">
			<div class="modal-content" style="width:700px !important">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle3"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle3">
							
						</thead>
						<tbody id="bodyDetalle3">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle3').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
       <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/alertas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>