﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class common_UsuariosDormidos : System.Web.UI.Page {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                hdnIDFranquicias.Value = usu.IDFranquicia.ToString();
            }
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                hdnIDMarcas.Value = usu.IDMarca.ToString();
            }
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null))
                cargarCombos();
        }
    }



    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;

            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                //var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
               // this.ddlMarcas.SelectedValue = usu.IDMarca.ToString();
                divMarcas.Visible = false;
                divFranquicias.Visible = false;
            }            

            else if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                listMarcas = bMarca.getMarcasByFranquicia(usu.IDFranquicia);
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();

                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

                divFranquicias.Visible = false;
            }
            else {
                bFranquicia bFranquicia = new bFranquicia();
                List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
                this.ddlFranquicias.DataSource = listFranquicias;
                this.ddlFranquicias.DataValueField = "IDFranquicia";
                this.ddlFranquicias.DataTextField = "NombreFantasia";
                this.ddlFranquicias.DataBind();

                this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> marcasByFranquicias(int idFranquicia) {
        List<ComboViewModel> listMarcas = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {
            listMarcas = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia).Select(x => new ComboViewModel { ID = x.IDMarca.ToString(), Nombre = x.Nombre }).OrderBy(x=>x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listMarcas.Insert(0, todas);
        }
        return listMarcas;
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter,  int IDFranquicia, int IDMarca,string FechaDesde, string Sexo, int EdadDesde,int EdadHasta, int CantTrans) {

        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                string sql = "";
                string fechaDsd = "";
                if (FechaDesde != "") {
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    fechaDsd = (DateTime.Parse(FechaDesde)).ToString(formato);
                }

                if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                    var usuMarcas = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                        IDMarca = usuMarcas.IDMarca;
                }
                if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                    var usuFranquicias = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                    IDFranquicia = usuFranquicias.IDFranquicia;
                }

                sql = "Exec Rpt_UsuariosDormidos " + IDFranquicia.ToString() + "," + IDMarca.ToString() + ",'" + fechaDsd + "','" + Sexo + "'," + EdadDesde.ToString() + "," + EdadHasta.ToString() + "," + CantTrans.ToString();
                var list = dbContext.Database.SqlQuery<UsuariosDormidosViewModel>(sql, new object[] { })
                    .Select(x => new UsuariosDormidosViewModel() {
                              Nombre = x.Nombre,
                              Apellido = x.Apellido,
                              CantTrans = x.CantTrans,
                              IDFranquicia = x.IDFranquicia,
                              Franquicia = x.Franquicia,
                              IDMarca = x.IDMarca,
                              Marca = x.Marca,
                              Sexo = x.Sexo,
                              Edad = x.Edad
                            
                          }).OrderByDescending(x=>x.CantTrans).ToList();

                return list.AsQueryable().ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }


    [System.Web.Services.WebMethod]
    public static string Exportar(int IDFranquicia, int IDMarca, string FechaDesde, string Sexo, int EdadDesde, int EdadHasta, int CantTrans) {

        string fileName = "UsuariosDormidos";
        string path = "/tmp/";
        DateTime dtDsd = Convert.ToDateTime(FechaDesde);

        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentFranquiciasUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usuarioFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    string sql = "";
                    string fechaDsd="";
                    if(FechaDesde != ""){
                        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                        fechaDsd = (DateTime.Parse(FechaDesde)).ToString(formato);
                    }


                    sql = "Exec Rpt_UsuariosDormidos " + IDFranquicia.ToString() + "," + IDMarca + ",'" + fechaDsd + "','" + Sexo + "'," + EdadDesde.ToString() + "," + EdadHasta.ToString() + "," + CantTrans.ToString();
                    var info = dbContext.Database.SqlQuery<UsuariosDormidosViewModel>(sql, new object[] { })
                        .Select(x => new  {
                            Nombre = x.Nombre ?? "",
                            Apellido = x.Apellido ?? "",
                            Sexo = x.Sexo ?? "",
                            Edad = x.Edad ?? 0,
                            Franquicia = x.Franquicia ?? "",
                            Marca = x.Marca ?? "",
                            CantTrans = x.CantTrans,
                            Email=x.Email,
                            Celular=x.Celular,
                            Empresa=x.EmpresaCelular
                        }).OrderByDescending(x => x.CantTrans).ToList();

                    dt = info.ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}