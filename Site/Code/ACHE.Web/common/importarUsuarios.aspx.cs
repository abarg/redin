﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model.EntityData;
using System.Collections.Specialized;




public partial class common_importarUsuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }

    private void cargarCombos()
    {

    }


    public void ImportarUsuarios(object sender, EventArgs e)
    {
        int error = 0;

        List<string> errores = new List<string>();

        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpUsuarios.HasFile)
            {
                if (flpUsuarios.FileName.Split('.')[flpUsuarios.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpUsuarios.FileName;
                    flpUsuarios.SaveAs(path);

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);


                    using (var dbContext = new ACHEEntities())
                    {


                        for (int i = 0; i < rows.Length; i++)
                        {


                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });


                                if (dr.Length == 3)
                                {

                                    // columna "3" = Sede
                                    if (dr[2].Trim() != "")
                                    {

                                        string tmpUser = dr[0].Trim(); // User
                                        string tmpPW = dr[1].Trim(); // PW
                                        string tmpSede = dr[2].Trim(); // Sede

                                        // BUSCO SEDE
                                        var auxSede = dbContext.Comercios.Where(x => x.NombreFantasia == tmpSede).FirstOrDefault();                       

                                        if (auxSede != null)
                                        {

                                            // BUSCO USUARIO EMPRESA
                                            var auxUser = dbContext.UsuariosEmpresas.Where(x => x.Usuario.ToLower() == tmpUser.ToLower()).FirstOrDefault();
 

                                            if (auxUser != null)
                                            {

                                                var auxECO = dbContext.EmpresasComercios.Where(x => x.IDComercio == auxSede.IDComercio && auxUser.IDEmpresa == x.IDEmpresa).FirstOrDefault();

                                                if (auxECO != null)
                                                {

                                                    EmpresasComercios ECOEntity;

                                                    ECOEntity = new EmpresasComercios();
                                                    ECOEntity.IDEmpresa = auxUser.IDEmpresa;
                                                    ECOEntity.IDComercio = auxSede.IDComercio;

                                                    dbContext.EmpresasComercios.Add(ECOEntity);

                                                    dbContext.SaveChanges();


                                                }
                                                else
                                                {
                                                    error++;

                                                    errores.Add("El usuario/empresa " + tmpUser + "ya tiene la sede " + tmpSede + " asignada.");

                                                }
                                            }
                                            else // Creamos una empresa y usuario nuevo
                                            {

                                                Empresas EMEntity;
                                                UsuariosEmpresas UMEntity;

                                                EMEntity = new Empresas();
                                                EMEntity.Nombre = tmpUser;
                                                EMEntity.Color = "blue";
                                                EMEntity.Logo = "marca_21122017095959.jpg";
                                                EMEntity.FechaAlta = DateTime.Now;

                                                dbContext.Empresas.Add(EMEntity);

                                                dbContext.SaveChanges();


                                                UMEntity = new UsuariosEmpresas();
                                                UMEntity.IDEmpresa = EMEntity.IDEmpresa;
                                                UMEntity.Usuario = tmpUser;
                                                UMEntity.Email = tmpUser + "@sanmartin.com";
                                                UMEntity.Pwd = tmpPW;
                                                UMEntity.Tipo = "A";
                                                UMEntity.Activo = true;

                                                dbContext.UsuariosEmpresas.Add(UMEntity);

                                                dbContext.SaveChanges();

                                            }

                                        }


                                        else
                                        {

                                            error++;

                                            errores.Add("No se encontro la sede " + tmpSede);


                                        }


                                    }



                                }
                                else
                                {

                                    error++;

                                    errores.Add("La estructura del archivo es incorrecta, debe tener 4 columnas");

                                    ShowError("La estructura del archivo es incorrecta, debe tener 4 columnas");
                                }


                            }
                        }


                    }


                    int totalImportados = rows.Length - error;

                    if (error < 1)
                        ShowOk("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                    else
                    {
                        ShowError("Se importaron " + totalImportados + " de " + rows.Length + " registros");
                        devolverErroresAsignacion(errores);
                    }
                    File.Delete(path);

                }

                File.Delete(path);


            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }


        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());


    }


    //protected void ImportarUsuarios(object sender, EventArgs e)
    //{

    //    using (TransactionScope dbContext = new ACHEEntities())
    //    {
    //        ACHEEntities dbContext = null;
    //        try
    //        {
    //            dbContext = new ACHEEntities();
    //            dbContext.Configuration.AutoDetectChangesEnabled = false;

    //            int count = 0;
    //            foreach (var entityToInsert in someCollectionOfEntitiesToInsert)
    //            {
    //                ++count;
    //                dbContext = AddToContext(dbContext, entityToInsert, count, 100, true);
    //            }

    //            dbContext.SaveChanges();
    //        }
    //        finally
    //        {
    //            if (dbContext != null)
    //                dbContext.Dispose();
    //        }

    //        scope.Complete();
    //    }

    //}



    //private ACHEEntities AddToContext(ACHEEntities dbContext, Entity entity, int count, int commitCount, bool recreateContext)
    //{
    //    dbContext.Set<Entity>().Add(entity);

    //    if (count % commitCount == 0)
    //    {
    //        dbContext.SaveChanges();
    //        if (recreateContext)
    //        {
    //            dbContext.Dispose();
    //            dbContext = new MyDbContext();
    //            dbContext.Configuration.AutoDetectChangesEnabled = false;
    //        }
    //    }

    //    return dbContext;
    //}


[WebMethod(true)]
    public static string devolverErroresAsignacion(List<string> errores)
    {

        var html = string.Empty;

        if (errores.Any())
        {
            foreach (var detalle in errores)
            {
                html += "<tr>";
                html += "<td>" + detalle + "</td>";
                html += "</tr>";
            }
        }
        else
            html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";


        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", html.ToString());

        return html;
    }

  
    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}