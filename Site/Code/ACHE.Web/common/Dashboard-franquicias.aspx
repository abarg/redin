﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard-franquicias.aspx.cs" Inherits="common_Dashboard_franquicias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }

        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }

        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }

        .ov_boxes .ov_text {
            width: 125px !important;
        }

        .modal.modal-wide .modal-dialog {
            width: 90%;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="row admin" style="display:none"> 
          <form runat="server">
            <div class="col-sm-3" >
                <label>Franquicia</label>
                <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"  onchange="mostrarReporte(this.value);" />
            </div>
            <asp:HiddenField runat="server" ID="hdnIDFranquicias" Value="-1" />
            <asp:HiddenField runat="server" ID="hdnIDFranquiciasAdmin" Value="-1" />
          </form>
      </div>
    <div class="dashboard" style="display:none">
        <div class="row">
            <div class="col-sm-12 tac">
                <h3 class="heading">Comisiones
                    <asp:Literal runat="server" ID="litFecha2"></asp:Literal></h3>
                <ul class="ov_boxes">
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/coins.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_comision1">Calculando...</strong>
                            <asp:Literal runat="server" ID="litLeyenda1"></asp:Literal>%
                            <small>Tarj propias en comercios propios </small>
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/coins.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_comision2">Calculando...</strong>
                            <asp:Literal runat="server" ID="litLeyenda2"></asp:Literal>%
                            <small>Tarj propias en otros comercios </small>
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/coins.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_comision3">Calculando...</strong>
                            <asp:Literal runat="server" ID="litLeyenda3"></asp:Literal>%
                            <small>Otras tarj en comercios propios </small>
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/coins.png" style="width: 48px" />
                        </div>
                        <div class="ov_text" style="height: 70px;">
                            <strong id="resultado_comisionTotal">Calculando...</strong>
                            Total sin IVA
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/invoice.png" style="width: 48px" />
                        </div>
                        <div class="ov_text" style="height: 70px;">
                            <strong id="resultado_comisionTotalIVA">Calculando...</strong>
                            Total + IVA
                        </div>
                    </li>
                     


                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 tac">
                <h3 class="heading">Estadísticas generales</h3>
                <ul class="ov_boxes">
                    <li>
                        <div class="p_bar_down p_canvas">
                            <img src="../../img/dashboard/sex.png" style="width: 48px" />
                        </div>
                        <div class="ov_text" style="padding: 3px">
                            <strong id="resultado_sexo" style="font-size: 13px; padding-top: 9px;">Calculando...</strong><%--F: 100 - M: 300--%>
						    Total Socios: <span id="totalSexo"></span>
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/email.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_mails">Calculando...</strong>
                            Cantidad de
                            <br />
                            mails
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/MetroUI_Phone.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_cel">Calculando...</strong>
                            Cantidad de
                            <br />
                            celulares
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/credit_card_yellow.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_total_tarjetas_activas">Calculando...</strong>
                            Cantidad de
                            <br />
                            Tarjetas activas
                        </div>
                    </li>
                    <li>
					    <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/money2.png" style="width:48px" />
					    </div>
					    <div class="ov_text">
						    <strong id="resultado_total_dinero_credito" style="font-size: 13px">Calculando...</strong>
						    Dinero disponible en tarjetas
					    </div>
				    </li>                   
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 tac">
                <h3 class="heading">
                    <asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
                <ul class="ov_boxes">
                    <li>
                        <div class="p_bar_up p_canvas">
                            <img src="../../img/dashboard/money-graph.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_ticketpromedio_mensual">Calculando...</strong>
                            Promedio ticket
                            <br />
                            mensual
                        </div>
                    </li>
                    <li>
                        <div class="p_line_up p_canvas">
                            <img src="../../img/dashboard/Tracking_3D.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_tasauso_mensual">Calculando...</strong>
                            Tasa de
                            <br />
                            uso
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/transaction.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_tr_mensual">Calculando...</strong>
                            Cantidad de
                            <br />
                            trans. mensuales
                        </div>
                    </li>
                    <li style="display: none">
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/billete.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_comision_mensual">Calculando...</strong>
                            Total comision
                            <br />
                            mensual
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/graph.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_promedio_arancel">Calculando...</strong>
                            Promedio<br />
                            arancel 
                        </div>
                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/graph.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_promedio_descuentos">Calculando...</strong>
                            Promedio
                            <br />
                            descuentos
                        </div>
                    </li>

                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/graph.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_promedio_puntos">Calculando...</strong>
                            Promedio<br />
                            puntos
                        </div>

                    </li>
                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/credit_card.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_cantidad_tarjetas">Calculando...</strong>
                            Cantidad
                            <br />
                            tarjetas
                        </div>
                    </li>

                    <li>
                        <div class="p_line_down p_canvas">
                            <img src="../../img/dashboard/1396123817_banker.png" style="width: 48px" />
                        </div>
                        <div class="ov_text">
                            <strong id="resultado_facturacion_comercios">Calculando...</strong>
                            Facturacion<br />
                            comercios 
                        </div>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-6"  >
                <div class="heading clearfix">
                    <h3 class="pull-left">TOP 10 Mejores socios</h3>
                </div>
                <a href="javascript:mostrarSociosMensual();">Vista mensual&nbsp;|</a> 
                <a href="javascript:mostrarSociosGeneral();">Histórico</a>
                <table id="tblSocios"class="table table-striped table-bordered mediaTable" style="display: none" >
				    <thead>
					    <tr>
						    <th class="essential persist">Nombre y Apellido</th>
						    <th class="optional">Tarjeta</th>
                             <th class="optional">Marca</th>
						    <th class="essential">Importe</th>
					    </tr>
				    </thead>
				    <tbody id="bodySocios">
                        <tr><td colspan="4">Calculando...</td></tr>
				    </tbody>
			    </table>

                <table id="tblSociosMensual"class="table table-striped table-bordered mediaTable">
                    <thead>
                        <tr>
                            <th class="essential persist">Nombre y Apellido</th>
                            <th class="optional">Tarjeta</th>
                            <th class="optional">Marca</th>
                            <th class="essential">Importe</th>
                        </tr>
                    </thead>
                    <tbody id="bodySociosMes">
                        <tr>
                            <td colspan="4">Calculando...</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-sm-6 col-lg-6">
                <div class="heading clearfix">
                    <h3 class="pull-left">TOP 10 Mejores comercios</h3>
                </div>
                <a href="javascript:mostrarComerciosMensual();">Vista mensual&nbsp;|</a>
                <a href="javascript:mostrarComerciosGeneral();">Histórico</a>
                <table id="tblComerciosMensual" class="table table-striped table-bordered mediaTable">
                    <thead>
                        <tr>

                            <th class="essential persist">Nombre y Apellido</th>
                            <th class="optional">Tarjeta</th>
                            <th class="essential">Importe</th>
                        </tr>
                    </thead>
                    <tbody id="bodyComerciosMes">
                        <tr>
                            <td colspan="3">Calculando...</td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblComercios" class="table table-striped table-bordered mediaTable" style="display: none" >
				    <thead>
					    <tr>
						
						    <th class="essential persist">Comercio</th>
						    <th class="optional">Domicilio</th>
						    <th class="essential">Importe</th>
					    </tr>
				    </thead>
				    <tbody id="bodyComercios">
                        <tr><td colspan="3">Calculando...</td></tr>
				    </tbody>
			    </table>
            </div>
	    </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="heading">Comisiones <small>últimos 30 días</small></h3>
                <div id="fl_comisiones_detalle" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h3 class="heading">Comisiones <small>últimos 4 meses</small></h3>
                <div id="fl_comisiones_4meses" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading">Tarjetas asignadas <small>últimos 4 meses</small></h3>
                <div id="fl_tarjetas_asignadas" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading">Tarjetas activas & inactivas <small>últimos 4 meses</small></h3>
                <div id="fl_tarjetas_activas" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading">Cantidad de transacciones <small>últimos 4 meses</small></h3>
                <div id="fl_cant_transacciones" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading">Tarjetas impresas por marca
                    &nbsp;
                    <small>
                        <a href="javascript: ExportarTarjetasImpresas();">Exportar a Excel</a>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingEmitidas" style="display: none" />
                        <a href="" id="lnkDownloadEmitidas" download="Emitidas" style="display: none">Descargar</a>
                    </small>
                </h3>
                <table class="table table-condensed table-striped" data-provides="rowlink" id="tablaImpresas">
                    <thead id="headImpresas">
                        <tr>
                            <th style="min-width:200px">Marca</th>
                            <th>Tarjetas Activas</th>
                            <th>Tarjetas Inactivas</th>
                            <th>Tarjetas Totales</th>
                        </tr>
                    </thead>
                    <tbody id="bodyImpresas">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading" id="lblTerminalesPorPOS">Terminales por pos </h3>
                <div id="fl_terminales_activas" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
            <div class="col-sm-6 col-lg-6">
                <h3 class="heading" id="lblEstadoTerminales">Estado de las terminales</h3>
                <div id="fl_estado_terminales" style="height: 270px; width: 90%; margin: 15px auto 0">
                    <img src="/img/dashboard/gif-load.gif" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-wide fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>SDS</th>
                                <th>Nombre</th>
                                <th>Marca</th>
                                <th>Domicilio</th>
                                <th>Fecha Carga</th>
                                <th>Fecha Alta</th>
                                <th>Terminal</th>
                                <th>Establecimiento</th>
                                <th>Fecha Activ</th>
                                <th>Fecha Reprog</th>
                                <th>Reprogramado</th>
                                <th>Invalido</th>
                                <th>Activo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/dashboard-franquicias.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <!-- charts functions -->

    <!-- tablas -->
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

