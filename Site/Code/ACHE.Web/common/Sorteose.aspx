﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sorteose.aspx.cs" Inherits="common_Sorteose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Sorteos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formSorteo" class="form-horizontal" role="form">
                <br />         
                <div class="form-group">
                    <label for="cmbMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Marca</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbMarcas" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtTitulo" class="col-lg-2 control-label"><span class="f_req">*</span> Titulo</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtTitulo" CssClass="form-control required" MaxLength="40"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtMsj1" class="col-lg-2 control-label"> Mensaje 1</label>
                    <div class="col-lg-4">                      
                        <asp:TextBox runat="server" ID="txtMsj1" Rows="2" CssClass="form-control"  TextMode="MultiLine" MaxLength="40"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtMsj2" class="col-lg-2 control-label"> Mensaje 2</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtMsj2" Rows="2" CssClass="form-control" TextMode="MultiLine" MaxLength="40"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtMsj3" class="col-lg-2 control-label"> Mensaje 3</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtMsj3" Rows="2" CssClass="form-control" TextMode="MultiLine" MaxLength="40"></asp:TextBox>

                    </div>
                </div>
                <div class="form-group">
                    <label for="txtMsj4" class="col-lg-2 control-label">  Mensaje 4</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtMsj4" Rows="2" CssClass="form-control" TextMode="MultiLine" MaxLength="40"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
					<label class="col-lg-2 control-label"><span class="f_req">*</span>Fecha desde</label>
                    <div class="col-lg-2">
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
					    <span class="help-block"></span>
                    </div>
				</div>    
                <div class="form-group">
					<label class="col-lg-2 control-label"><span class="f_req">*</span>Fecha hasta</label>
                    <div class="col-lg-2">
                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
					    <span class="help-block"></span>
                    </div>
				</div> 
                <div class="form-group">
                    <label for="txtMsj4" class="col-lg-2 control-label">Importe Desde</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtImporteDesde" CssClass="form-control numeric required" MaxLength="10"></asp:TextBox>
                        <br />
                    </div>
                </div>    
                <div class="form-group">
                    <label for="txtMsj4" class="col-lg-2 control-label">Importe Hasta</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtImporteHasta" CssClass="form-control numeric required" MaxLength="10"></asp:TextBox>
                        <br />
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-lg-2 control-label"> Sexo</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlSexo">
                            <asp:ListItem Value="H" Text="Hombre"></asp:ListItem>
                            <asp:ListItem Value="M" Text="Mujer"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>                                               
                <div class="form-group">
                    <label for="txtPais" class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group provincia">
                    <label for="txtProvincia" class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia" onchange="LoadCiudades2(this.value,'ddlCiudad');return false;">
                                        
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group ciudad">
                    <label for="txtCiudad" class="col-lg-2 control-label">Ciudad</label>
                    <div class="col-lg-4">
                            <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                data-placeholder="Seleccione una ciudad">
                                        
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtDomicilio" class="col-lg-2 control-label">Domicilio</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtPisoDepto" class="col-lg-2 control-label">Piso/Depto</label>
                    <div class="col-lg-2">
                        <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    </div>
                </div>                   
                <div class="form-group">
                    <label for="lblActivo" class="col-lg-2 control-label "> Activo</label>
                    <div class="col-lg-4">
                        <asp:CheckBox ID="chkActivo" runat="server" />
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIDSorteo" Value="0" />
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a href="Sorteos.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/sorteose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
   <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
