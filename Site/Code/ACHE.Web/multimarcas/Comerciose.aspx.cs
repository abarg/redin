﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;

public partial class multimarcas_Comerciose : PaginaMultimarcasBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                hdnIDMultimarca.Value = usu.IDMultimarca.ToString();
            }
            Session["CurrentImagen"] = null;
            Session["CurrentLogo"] = null;
            Session["CurrentFicha"] = null;

            if (!String.IsNullOrEmpty(Request.QueryString["IDTerminal"])) {
                this.hfIDTerminal.Value = Request.QueryString["IDTerminal"];
                if (!this.hfIDTerminal.Value.Equals("0")) {
                    this.cargarDatosComercio(Convert.ToInt32(Request.QueryString["IDTerminal"]));
                }
            }
        }
    }


    private void cargarDatosComercio(int IDTerminal) {
        try {
            using (var dbContext = new ACHEEntities()) {
                //Comercio
                Terminales terminal = dbContext.Terminales.Where(x => x.IDTerminal == IDTerminal).FirstOrDefault();
                if (terminal != null) {
                    txtNombreFantasia.Value = terminal.Comercios.NombreFantasia;
                    //Arancel y puntos

                    this.txtDescuento.Text = terminal.Descuento.ToString();
                    this.txtDescuento2.Text = terminal.Descuento2.ToString();
                    this.txtDescuento3.Text = terminal.Descuento3.ToString();
                    this.txtDescuento4.Text = terminal.Descuento4.ToString();
                    this.txtDescuento5.Text = terminal.Descuento5.ToString();
                    this.txtDescuento6.Text = terminal.Descuento6.ToString();
                    this.txtDescuento7.Text = terminal.Descuento7.ToString();

                    this.txtDescuentoVip.Text = terminal.DescuentoVip.HasValue ? terminal.DescuentoVip.Value.ToString() : "";
                    this.txtDescuentoVip2.Text = terminal.DescuentoVip2.HasValue ? terminal.DescuentoVip2.Value.ToString() : "";
                    this.txtDescuentoVip3.Text = terminal.DescuentoVip3.HasValue ? terminal.DescuentoVip3.Value.ToString() : "";
                    this.txtDescuentoVip4.Text = terminal.DescuentoVip4.HasValue ? terminal.DescuentoVip4.Value.ToString() : "";
                    this.txtDescuentoVip5.Text = terminal.DescuentoVip5.HasValue ? terminal.DescuentoVip5.Value.ToString() : "";
                    this.txtDescuentoVip6.Text = terminal.DescuentoVip6.HasValue ? terminal.DescuentoVip6.Value.ToString() : "";
                    this.txtDescuentoVip7.Text = terminal.DescuentoVip7.HasValue ? terminal.DescuentoVip7.Value.ToString() : "";


                    this.txtMulPuntosVip.Text = terminal.MultiplicaPuntosVip1.HasValue ? terminal.MultiplicaPuntosVip1.Value.ToString() : "";
                    this.txtMulPuntosVip2.Text = terminal.MultiplicaPuntosVip2.HasValue ? terminal.MultiplicaPuntosVip2.Value.ToString() : "";
                    this.txtMulPuntosVip3.Text = terminal.MultiplicaPuntosVip3.HasValue ? terminal.MultiplicaPuntosVip3.Value.ToString() : "";
                    this.txtMulPuntosVip4.Text = terminal.MultiplicaPuntosVip4.HasValue ? terminal.MultiplicaPuntosVip4.Value.ToString() : "";
                    this.txtMulPuntosVip5.Text = terminal.MultiplicaPuntosVip5.HasValue ? terminal.MultiplicaPuntosVip5.Value.ToString() : "";
                    this.txtMulPuntosVip6.Text = terminal.MultiplicaPuntosVip6.HasValue ? terminal.MultiplicaPuntosVip6.Value.ToString() : "";
                    this.txtMulPuntosVip7.Text = terminal.MultiplicaPuntosVip7.HasValue ? terminal.MultiplicaPuntosVip7.Value.ToString() : "";


                    this.txtMulPuntos1.Text = terminal.MultiplicaPuntos1.ToString();
                    this.txtMulPuntos2.Text = terminal.MultiplicaPuntos2.ToString();
                    this.txtMulPuntos3.Text = terminal.MultiplicaPuntos3.ToString();
                    this.txtMulPuntos4.Text = terminal.MultiplicaPuntos4.ToString();
                    this.txtMulPuntos5.Text = terminal.MultiplicaPuntos5.ToString();
                    this.txtMulPuntos6.Text = terminal.MultiplicaPuntos6.ToString();
                    this.txtMulPuntos7.Text = terminal.MultiplicaPuntos7.ToString();

                }
                else
                    throw new Exception("No existe la terminal");
            }

        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDTerminal
        , string Descuento, string Descuento2, string Descuento3, string Descuento4, string Descuento5, string Descuento6, string Descuento7
        , string DescuentoVip, string DescuentoVip2, string DescuentoVip3, string DescuentoVip4, string DescuentoVip5, string DescuentoVip6, string DescuentoVip7
        , string MulPuntosVip, string MulPuntosVip2, string MulPuntosVip3, string MulPuntosVip4, string MulPuntosVip5, string MulPuntosVip6, string MulPuntosVip7
        , string MultPuntos1, string MultPuntos2, string MultPuntos3, string MultPuntos4, string MultPuntos5, string MultPuntos6, string MultPuntos7) {
        try {

            //Comercio
            Terminales entity;
            using (var dbContext = new ACHEEntities()) {

                if (IDTerminal > 0)
                    entity = dbContext.Terminales.FirstOrDefault(s => s.IDTerminal == IDTerminal);
                else {
                    entity = new Terminales();
                    entity.Comercios.FechaAlta = DateTime.Now;
                    // entity.Estado = "IN";
                }

                //Arancel y puntos
                entity.Descuento = int.Parse(Descuento);
                entity.Descuento2 = int.Parse(Descuento2);
                entity.Descuento3 = int.Parse(Descuento3);
                entity.Descuento4 = int.Parse(Descuento4);
                entity.Descuento5 = int.Parse(Descuento5);
                entity.Descuento6 = int.Parse(Descuento6);
                entity.Descuento7 = int.Parse(Descuento7);
                if (DescuentoVip != string.Empty)
                    entity.DescuentoVip = int.Parse(DescuentoVip);
                else
                    entity.DescuentoVip = null;
                if (DescuentoVip2 != string.Empty)
                    entity.DescuentoVip2 = int.Parse(DescuentoVip2);
                else
                    entity.DescuentoVip2 = null;
                if (DescuentoVip3 != string.Empty)
                    entity.DescuentoVip3 = int.Parse(DescuentoVip3);
                else
                    entity.DescuentoVip3 = null;
                if (DescuentoVip4 != string.Empty)
                    entity.DescuentoVip4 = int.Parse(DescuentoVip4);
                else
                    entity.DescuentoVip4 = null;
                if (DescuentoVip5 != string.Empty)
                    entity.DescuentoVip5 = int.Parse(DescuentoVip5);
                else
                    entity.DescuentoVip5 = null;
                if (DescuentoVip6 != string.Empty)
                    entity.DescuentoVip6 = int.Parse(DescuentoVip6);
                else
                    entity.DescuentoVip6 = null;
                if (DescuentoVip7 != string.Empty)
                    entity.DescuentoVip7 = int.Parse(DescuentoVip7);
                else
                    entity.DescuentoVip7 = null;

                if (MulPuntosVip != string.Empty)
                    entity.MultiplicaPuntosVip1 = int.Parse(MulPuntosVip);
                else
                    entity.MultiplicaPuntosVip1 = null;

                if (MulPuntosVip2 != string.Empty)
                    entity.MultiplicaPuntosVip2 = int.Parse(MulPuntosVip2);
                else
                    entity.MultiplicaPuntosVip2 = null;

                if (MulPuntosVip3 != string.Empty)
                    entity.MultiplicaPuntosVip3 = int.Parse(MulPuntosVip3);
                else
                    entity.MultiplicaPuntosVip3 = null;

                if (MulPuntosVip4 != string.Empty)
                    entity.MultiplicaPuntosVip4 = int.Parse(MulPuntosVip4);
                else
                    entity.MultiplicaPuntosVip4 = null;

                if (MulPuntosVip5 != string.Empty)
                    entity.MultiplicaPuntosVip5 = int.Parse(MulPuntosVip5);
                else
                    entity.MultiplicaPuntosVip5 = null;

                if (MulPuntosVip6 != string.Empty)
                    entity.MultiplicaPuntosVip6 = int.Parse(MulPuntosVip6);
                else
                    entity.MultiplicaPuntosVip6 = null;

                if (MulPuntosVip7 != string.Empty)
                    entity.MultiplicaPuntosVip7 = int.Parse(MulPuntosVip7);
                else
                    entity.MultiplicaPuntosVip7 = null;

                if (!string.IsNullOrEmpty(MultPuntos1))
                    entity.MultiplicaPuntos1 = int.Parse(MultPuntos1);
                if (!string.IsNullOrEmpty(MultPuntos2))
                    entity.MultiplicaPuntos2 = int.Parse(MultPuntos2);
                if (!string.IsNullOrEmpty(MultPuntos3))
                    entity.MultiplicaPuntos3 = int.Parse(MultPuntos3);
                if (!string.IsNullOrEmpty(MultPuntos4))
                    entity.MultiplicaPuntos4 = int.Parse(MultPuntos4);
                if (!string.IsNullOrEmpty(MultPuntos5))
                    entity.MultiplicaPuntos5 = int.Parse(MultPuntos5);
                if (!string.IsNullOrEmpty(MultPuntos6))
                    entity.MultiplicaPuntos6 = int.Parse(MultPuntos6);
                if (!string.IsNullOrEmpty(MultPuntos7))
                    entity.MultiplicaPuntos7 = int.Parse(MultPuntos7);


                if (IDTerminal > 0)
                    dbContext.SaveChanges();
            }
            return entity.IDTerminal.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void eliminarLogo(int id, string archivo) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            var file = "//files//logos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                        if (entity != null) {
                            entity.Logo = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    [WebMethod(true)]
    public static void eliminarFicha(int id, string archivo) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            var file = "//files//fichas//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                        if (entity != null) {
                            entity.FichaAlta = null;
                            HttpContext.Current.Session["CurrentFicha"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }


    #region Pruebas POS
    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPruebas(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio, string desde, string hasta) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            using (var dbContext = new ACHEEntities()) {
                DateTime fDesde, fHasta;
                var result = dbContext.VerificacionesPOS
                    .Where(x => x.IDComercio == idComercio)
                    .OrderByDescending(x => x.FechaPrueba)
                    .Select(x => new VerificacionPOSViewModel() {
                        FechaPrueba = x.FechaPrueba,
                        IDVerificacionPOS = x.IDVerificacionPOS,
                        EstadoCanjes = x.EstadoCanjes,
                        EstadoCompras = x.EstadoCompras,
                        EstadoGift = x.EstadoGift,
                        Calco = x.Calco == true ? "Si" : "No",
                        CalcoPuerta = x.CalcoPuerta == true ? "Si" : "No",
                        Display = x.Display == true ? "Si" : "No",
                        Folletos = x.Folletos == true ? "Si" : "No",
                        ObservacionesGenerales = x.ObservacionesGenerales,
                        UsuarioPrueba = x.Usuarios.Usuario

                    }).AsQueryable();

                if (!string.IsNullOrEmpty(desde)) {
                    fDesde = DateTime.Parse(desde).Date;
                    result = result.Where(x => x.FechaPrueba >= fDesde).AsQueryable();
                }

                if (!string.IsNullOrEmpty(hasta)) {
                    fHasta = DateTime.Parse(hasta).Date.AddDays(1).AddTicks(-1);
                    result = result.Where(x => x.FechaPrueba <= fHasta).AsQueryable();
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePrueba(int id) {
        try {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.VerificacionesPOS.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void ProcesarPrueba(int idComercio, int IDVerificacionPOS, string estadoCanjes, string estadoGift, string estadoCompras, string observacionesCanjes, string observacionesGift, string observacionesCompras
        , string usuario, string obs, string puntosCanjes, string puntosGift, string puntosCompras, string fechaPrueba) {
        try {
            int idusu = 0;
            bool logueado = false;
            if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
                logueado = true;
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                idusu = usu.IDUsuario;
            }
            else {
                if (HttpContext.Current.Session["CurrentUser"] != null) {
                    logueado = true;
                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    idusu = usu.IDUsuario;
                }
            }
            if (logueado) {
                using (var dbContext = new ACHEEntities()) {
                    VerificacionesPOS entity;
                    if (IDVerificacionPOS == 0)
                        entity = new VerificacionesPOS();
                    else
                        entity = dbContext.VerificacionesPOS.Where(x => x.IDVerificacionPOS == IDVerificacionPOS).FirstOrDefault();


                    entity.IDUsuario = idusu;

                    entity.IDComercio = idComercio;
                    entity.EstadoCanjes = estadoCanjes;
                    entity.EstadoCompras = estadoCompras;
                    entity.EstadoGift = estadoGift;

                    entity.Puntos_POSCanjes = puntosCanjes;
                    entity.Puntos_POSCompras = puntosCompras;
                    entity.Puntos_POSGift = puntosGift;

                    entity.Observaciones_POSCanjes = observacionesCanjes;
                    entity.Observaciones_POSCompras = observacionesCompras;
                    entity.Observaciones_POSGift = observacionesGift;
                    entity.FechaPrueba = DateTime.Parse(fechaPrueba);

                    entity.UsuarioPrueba = usuario;
                    entity.ObservacionesGenerales = obs;

                    if (IDVerificacionPOS == 0)
                        dbContext.VerificacionesPOS.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Puntos de venta

    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaPuntos(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.PuntosDeVenta
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Numero)
                    .Select(x => new PuntoDeVentaViewModel() {
                        IDPuntoVenta = x.IDPuntoVenta,
                        IDComercio = x.IDComercio,
                        Punto = x.Numero,
                        FechaAlta = x.FechaAlta,
                        EsDefault = x.EsDefault ? "Si" : "No",
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeletePunto(int id) {
        try {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.PuntosDeVenta.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarPunto(int IDComercio, int IDPuntoVenta, int numero, DateTime fechaAlta, bool esDefault) {
        try {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.PuntosDeVenta.Any(x => x.Numero == numero && x.IDComercio == IDComercio && x.IDPuntoVenta != IDPuntoVenta))
                        throw new Exception("Ya existe un punto de venta con el numero ingresado para este comercio");

                    PuntosDeVenta entity;
                    if (IDPuntoVenta == 0)
                        entity = new PuntosDeVenta();
                    else
                        entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == IDPuntoVenta).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Numero = numero;
                    entity.FechaAlta = DateTime.Now;

                    #region default
                    var algunoDefault = dbContext.PuntosDeVenta.Any(x => x.IDComercio == IDComercio && x.EsDefault);
                    if (algunoDefault && esDefault)
                        throw new Exception("Solo puede haber un punto de venta como default");
                    else
                        entity.EsDefault = esDefault;
                    #endregion

                    if (IDPuntoVenta == 0)
                        dbContext.PuntosDeVenta.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idComercio) {
        if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.UsuariosComercios
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel() {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id) {
        try {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.UsuariosComercios.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDComercio, int IDUsuario, string usuario, string email, string pwd, string tipo) {
        try {
            if ((HttpContext.Current.Session["CurrentFranquiciasUser"] != null) || (HttpContext.Current.Session["CurrentUser"] != null)) {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.UsuariosComercios.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosComercios.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosComercios entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosComercios();
                    else
                        entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDComercio = IDComercio;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosComercios.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion


    public static string corregirCelular(string Celular) {
        if (Celular == null) Celular = "";
        if (Celular.Length > 6) {
            Celular = Celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            Celular = Celular.Trim();
            if (Celular[0] == '0') Celular = Celular.Remove(0, 1);
            if (Celular[1] == '0') Celular = Celular.Remove(1, 1);
            if (Celular[2] == '0') Celular = Celular.Remove(2, 1);
            if (Celular[0] != '5') Celular = Celular.Insert(0, "5");
            if (Celular[1] != '4') Celular = Celular.Insert(1, "4");
        }
        return Celular;
    }

    public static string corregirEmail(string Email) {
        if (Email == null) Email = "";
        if (Email != "") {
            Email = Email.Replace("(", "").Replace(")", "").Replace("emal", "email").Replace("email.com.ar", "email.com.ar").Replace("hotmai.com", "hotmail.com").Replace("hotmal", "hotmail").Replace("yaho.com", "yahoo.com").Replace(" ", "").Replace("?", "").Replace("¿", "").Replace("¡", "").Replace("!", "").Replace("&", "").Replace("}", "").Replace("{", "").Replace(",", "").Replace("#", "");
            Email = Email.Trim();
        }
        return Email;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubRubros(int IDRubroPadre) {

        using (var dbContext = new ACHEEntities()) {
            List<ComboViewModel> result = new List<ComboViewModel>();

            var subRubros = dbContext.Rubros.Where(x => x.IDRubroPadre == IDRubroPadre).OrderBy(x => x.IDRubro).Select(x => new ComboViewModel() {
                ID = x.IDRubro.ToString(),
                Nombre = x.Nombre
            }).ToList();

            if (subRubros != null && subRubros.Count() > 0)
                result = subRubros;

            result.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });

            return result;
        }
    }

    private static void asociarComercioAEmpresa(int idComercio, int idEmpresa) {
        using (var dbContext = new ACHEEntities()) {
            EmpresasComercios empresaComercioNueva = new EmpresasComercios();
            empresaComercioNueva.IDComercio = idComercio;
            empresaComercioNueva.IDEmpresa = idEmpresa;
            dbContext.EmpresasComercios.Add(empresaComercioNueva);
            dbContext.SaveChanges();
        }

    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais) {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }

}