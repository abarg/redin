﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;



public partial class Multimarcas_home : PaginaMultimarcasBase
{
    protected void Page_Load(object sender, EventArgs e) {
        litNombre.Text = CurrentMultimarcasUser.Marca;
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Multimarcas.Where(x => x.IDMultimarca == CurrentMultimarcasUser.IDMultimarca).FirstOrDefault();
            if (entity != null) {
                if (!string.IsNullOrEmpty(entity.Logo))
                    imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                else
                    imgLogo.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";

            }           
        }
    }
}