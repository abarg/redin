﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMultimarcas.master" AutoEventWireup="true" CodeFile="Comerciose.aspx.cs" Inherits="multimarcas_Comerciose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/marcas/Comercios.aspx") %>">Comercios</a></li>
               <li>Edición</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Comercios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>           
            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabArancelPuntos" data-toggle="tab">Arancel & Puntos</a></li>
                </ul>
                <form runat="server" id="formComercio" class="form-horizontal" role="form">
                    <asp:HiddenField runat="server" ID="txtNombreFantasia" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDMultimarca" Value="0" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                    <div class="tab-content">
                        <div class="tab-pane" id="tabArancelPuntos">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Mult Puntos</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos1" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntos7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Descuento</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuento7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Desc. Vip</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip2" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip3" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip4" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip5" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip6" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtDescuentoVip7" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Mult Puntos Vip</label>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Lunes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip2" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Martes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip3" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Miércoles</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip4" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Jueves</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip5" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Viernes</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip6" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Sábado</span>
                                </div>
                                <div class="col-lg-1">
                                    <asp:TextBox runat="server" ID="txtMulPuntosVip7" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                    <span class="help-block">Domingo</span>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group" id="formButtons">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="Comercios.aspx" class="btn btn-link">Cancelar</a>

                                <asp:HiddenField runat="server" ID="hfIDTerminal" Value="0" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
         <div class="modal fade" id="modalPruebaPOS">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Verificaciones POS</h4>
                        <input type="hidden" id="hdnIDVerificacionPOS" value="0" />
                    </div>             
                </div>
            </div>
        </div>

    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle">Mapa</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var drag = false;
        var infowindow;
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/comerciose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <style type="text/css">
        #map-canvas {
            height: 280px;
            background-color: transparent;
        }
    </style>
</asp:Content>
