﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMultimarcas.master" AutoEventWireup="true" CodeFile="transacciones.aspx.cs" Inherits="Multimarcas_transacciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/multimarcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Transacciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Listado de Transacciones</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. Tarjeta</label>
                            <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2">
                            <label>Nro. Doc. Socio</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">
                            <label>Operación</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlOperacion">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Venta" Value="Venta"></asp:ListItem>
                                <asp:ListItem Text="Anulacion" Value="Anulacion"></asp:ListItem>
                                <asp:ListItem Text="Canje" Value="Canje"></asp:ListItem>
                                <asp:ListItem Text="Carga" Value="Carga"></asp:ListItem>
                                <asp:ListItem Text="Descarga" Value="Descarga"></asp:ListItem>
                                <asp:ListItem Text="CuponIn" Value="CuponIn"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                       <div class="col-sm-3 marca">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" value="" class="form-control" ID="ddlMarcas" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Transacciones" style="display:none">Descargar</a>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="NuevaTr();">Nuevo</button>--%>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
<%--        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/transacciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script> --%>   
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/multimarcas/transacciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>    
</asp:Content>