﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMultimarcas.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="Multimarcas_home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .main_content li {
            line-height: 32px;
        }
        .vcard-item, .item-key {
            font-size:17px;
        }
        .item-key {
            width:150px;
        }

        .logo_home {
            margin-left: 350px !important;
            /* vertical-align: top; */
            /* clear: both; */
            top: -210px;
            position: relative;
            max-width: 500px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <%--<h1 class="heading">Bienvenido</h1>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>--%>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div align="center">
                    <br /><br />
                    <h1 class="heading" style="border-bottom: 0px">Bienvenido <asp:Literal runat="server" ID="litNombre"></asp:Literal></h1>
                    <br />
                    <asp:Image runat="server" ID="imgLogo" style="max-width: 400px" />
                    <br />
                </div>
            </form>
        </div>
    </div>
</asp:Content>


