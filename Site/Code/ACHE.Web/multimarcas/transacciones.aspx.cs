﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class Multimarcas_transacciones : PaginaMultimarcasBase{
    protected void Page_Load(object sender, EventArgs e) {
        cargarCombos();
        if (!IsPostBack) {

            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            if (CurrentMultimarcasUser == null)
                Response.Redirect("home.aspx");
        }
    }

    private void cargarCombos() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                bMultimarca bMultimarca = new bMultimarca();
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                listMarcas = bMultimarca.getMarcas(usu.IDMultimarca);
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();
                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            int idMultimarca = usu.IDMultimarca;

            using (var dbContext = new ACHEEntities()) {

                var result = dbContext.TransaccionesMarcasView
                    .Where(x => x.ImporteOriginal > 1)// && x.FechaTransaccion >= fechaDesdeBase)
                    .OrderByDescending(x => x.FechaTransaccion)
                    .Select(x => new {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0,
                        IDMarca = x.IDMarca,
                        Marca = x.Marca,
                        Domicilio = x.DomicilioComercio,
                        Credito = (x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.PuntosAContabilizar ?? 0) / 100) : (((x.PuntosAContabilizar ?? 0) / 100) * -1)
                    }).AsQueryable();



                //if (usu.SoloPOSPropios)
                //    result = result.Where(x => x.ComercioMarca == idMarca);

                //if (usu.SoloTarjetasPropias)
                //    result = result.Where(x => x.IDMarca == idMarca);


                if (fechaDesde != string.Empty) {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty) {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }

                #region Multimarca
                var multimarcas = dbContext.MarcasAsociadas.Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                if (multimarcas.Count > 0)
                    result = result.Where(x => multimarcas.Contains(x.IDMarca));
                else
                    return null;
                #endregion


                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string documento, string comercio,string operacion,int idMarca) {
        string fileName = "Transacciones";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            int idMultimarca = usu.IDMultimarca;

            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.TransaccionesMarcasView
                        .Where(x => x.ImporteOriginal > 1)
                        .OrderByDescending(x => x.FechaTransaccion).AsQueryable();

                    if (idMarca > 0)
                        info = info.Where(x => x.IDMarca == idMarca);
                    if (tarjeta != "")
                        info = info.Where(x => x.Numero.ToLower().Contains(tarjeta.ToLower()));
                    if (operacion != "")
                        info = info.Where(x => x.Operacion.ToLower().Contains(operacion.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumentoSocio.ToLower().Contains(documento.ToLower()));
                    if (comercio != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(comercio.ToLower()));
                    if (fechaDesde != string.Empty) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }

                    //if (usu.SoloPOSPropios)
                    //    info = info.Where(x => x.IDMarcaComercio.HasValue && x.IDMarcaComercio.Value == idMarca);
                    //if (usu.SoloTarjetasPropias)
                    //    info = info.Where(x => x.IDMarca == idMarca);

                    var multimarcas = dbContext.MarcasAsociadas.Include("Marcas").Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                    if (multimarcas.Count > 0)
                        info = info.Where(x => multimarcas.Contains(x.IDMarca));
                    else
                        info = null;

                    dt = info.ToList().Select(x => new {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Marca = x.Marca,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Domicilio = x.DomicilioComercio,
                        Credito = (x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.PuntosAContabilizar ?? 0) / 100) : (((x.PuntosAContabilizar ?? 0) / 100) * -1),
                        PuntoDeVenta = x.PuntoDeVenta,
                        TipoComprobante = x.TipoComprobante,
                        NroComprobante = x.NroComprobante,
                        Observacion = x.Descripcion
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}