﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class multimarcas_Comercios : PaginaMultimarcasBase {

    protected void Page_Load(object sender, EventArgs e) {
        cargarCombos();
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                hdnIDMultimarca.Value = usu.IDMultimarca.ToString();
            }
        }
    }



    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        int hdnIDMultimarca = 0;
        if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
            var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
            hdnIDMultimarca = usu.IDMultimarca;

            using (var dbContext = new ACHEEntities()) {
                var multimarcas = dbContext.MarcasAsociadas.Where(x => x.IDMultimarca == hdnIDMultimarca).Select(x => x.IDMarca).ToList();
                
                var result = dbContext.Terminales.Include("Comercios")
                    //.Where(x => idMarca == 0 || x.Comercios.IDMarca == idMarca)                     
                    .OrderBy(x => x.Comercios.NombreFantasia)
                    .Select(x => new {
                        IDComercio = x.IDComercio,
                        IDTerminal = x.IDTerminal,
                        IDMarca = x.Comercios.IDMarca ?? 0,
                        Marca = x.Comercios.IDMarca.HasValue ? x.Comercios.Marcas.Nombre : "",
                        SDS = x.Comercios.SDS,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        RazonSocial = x.Comercios.RazonSocial,
                        TipoDocumento = x.Comercios.TipoDocumento,
                        NroDocumento = x.Comercios.NroDocumento,
                        Telefono = x.Comercios.Telefono != null ? x.Comercios.Telefono : "",
                        Celular = x.Comercios.Celular != null ? x.Comercios.Celular : "",
                        Responsable = x.Comercios.Responsable != null ? x.Comercios.Responsable : "",
                        Email = x.Comercios.Email != null ? x.Comercios.Email : "",                        
                        POSTerminal = x.POSTerminal,
                        POSSistema = (x.POSSistema != null) ? x.POSSistema : "",
                        NumEst = x.NumEst,
                        CobrarUsoRed = x.CobrarUsoRed,
                        IDFranquicia = x.Comercios.IDFranquicia
                    }).AsQueryable();

                if (multimarcas.Count() > 0)
                    result = result.Where(x => multimarcas.Contains(x.IDMarca));
                else
                    return null;

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    private void cargarCombos() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas;
            if (HttpContext.Current.Session["CurrentMultimarcasUser"] != null) {
                bMultimarca bMultimarca = new bMultimarca();
                var usu = (WebMultimarcasUser)HttpContext.Current.Session["CurrentMultimarcasUser"];
                listMarcas = bMultimarca.getMarcas(usu.IDMultimarca);
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();
                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }
}

