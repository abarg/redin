﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageComercios.master" AutoEventWireup="true" CodeFile="alertas.aspx.cs" Inherits="comercios_alertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/comercios/alertas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <%--<div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Alertas</li>
        </ul>
    </div>--%>
    

    <div class="row">
        <div class="col-sm-6 col-md-6 dd_column">
        <div class="w-box">
            <div class="w-box-header">
                Alertas con prioridad
                <div class="pull-right">
                    <span class="label label-rojo">Alta</span>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal runat="server" ID="litAlertasAlta"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <div class="col-sm-6 col-md-6 dd_column">
        <div class="w-box">
            <div class="w-box-header">
                Alertas con prioridad
                <div class="pull-right">
                    <span class="label label-amarillo">Media</span>
                </div>
            </div>
            <div class="w-box-content">
                <table class="table table-striped">
                    <thead>
                         <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal runat="server" ID="litAlertasMedia"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">
                    Alertas con prioridad
                    <div class="pull-right">
                        <span class="label label-naranja">Baja</span>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-striped">
                        <thead>
                             <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal runat="server" ID="litAlertasBaja"></asp:Literal>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content" style="width:700px !important">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

