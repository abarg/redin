﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;

public partial class comercios_home : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litNombre.Text = CurrentComerciosUser.Comercio;
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Comercios.Where(x => x.IDComercio == CurrentComerciosUser.IDComercio).FirstOrDefault();
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Logo))
                    imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                else
                    imgLogo.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";

                litRazonSocial.Text = entity.RazonSocial;
                litCuit.Text = entity.NroDocumento;
                litTel.Text = entity.Telefono;
                litCel.Text = entity.Celular;
                litEmail.Text = entity.Email;
                if (entity.IDDomicilio != null)
                    litDom.Text = entity.Domicilios.Domicilio + ", " + entity.Domicilios.Provincias.Nombre;
            }
        }
    }
}