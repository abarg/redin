﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class comercios_cambiar_pwd : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            litUsuario.Text = CurrentComerciosUser.Usuario;
    }

    [WebMethod(true)]
    public static void grabar(string pwdActual, string pwd)
    {
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var user = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];

            using (var dbContext = new ACHEEntities())
            {
                UsuariosComercios entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                if (entity.Pwd != pwdActual)
                    throw new Exception("La contraseña actual ingresada es inválida");

                entity.Pwd = pwd;
                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentComerciosUser"] = user;
        }
    }
}