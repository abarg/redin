﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

using System.Collections.Specialized;
using System.Collections;
using System.Net.Mail;
using System.IO;
using System.IO.IsolatedStorage;
using System.Text;

public partial class comercios_encuestase : PaginaComerciosBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }

    [WebMethod(true)]
    public static string grabar(string detalle, string rangoMin, string rangoMax)
    {

        var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];

        if (usu != null) {

            using (var dbContext = new ACHEEntities())
            {
                ComerciosEncuestas entity;

                int min = int.Parse(rangoMin);
                int max = int.Parse(rangoMax);

                entity = new ComerciosEncuestas();
                entity.Pregunta = detalle;
                entity.RangoMin = (byte)int.Parse(rangoMin);
                entity.RangoMax = (byte)int.Parse(rangoMax);

                entity.IDComercio = usu.IDComercio;

                dbContext.ComerciosEncuestas.Add(entity);

                dbContext.SaveChanges();
            }

            return "ok";
        }
        else
            return "error";
    }

   
}