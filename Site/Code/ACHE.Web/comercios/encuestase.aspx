﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageComercios.master" AutoEventWireup="true" CodeFile="encuestase.aspx.cs" Inherits="comercios_encuestase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/comercios/encuestas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/comercios/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/comercios/encuestas.aspx") %>">Encuestas</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <div id="print">
                <h3 class="heading" id="litTitulo">Alta de Encuesta</h3>
                <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		        <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    

                    <div class="form-group">
                        <label class="col-lg-2 control-label" style="vertical-align:top">Pregunta / Detalle</label>
                        <div class="col-lg-8" style="padding-top:5px;">
                            <asp:TextBox ID="txtPregunta" CssClass="form-control required" MaxLength="30" runat="server" required="true"></asp:TextBox> 
                        
                        </div>
                    </div>

                   <div class="form-group">
                        <label class="col-lg-2 control-label" style="vertical-align:top">Puntaje Minimo</label>
                        <div class="col-lg-8" style="padding-top:5px;">
                            <asp:TextBox ID="txtRangoMin" min="0" max="255" type="number" CssClass="form-control required" MaxLength="30" runat="server" required="true"></asp:TextBox>                         
                        </div>
                    </div>


                   <div class="form-group">
                        <label class="col-lg-2 control-label" style="vertical-align:top">Puntaje Maximo</label>
                        <div class="col-lg-8" style="padding-top:5px;">
                            <asp:TextBox ID="txtRangoMax" min="0" max="255" type="number" CssClass="form-control required" MaxLength="30" runat="server" required="true"></asp:TextBox>                         
                        </div>
                    </div>


                    <div class="row"> 
                        <div class="col-sm-10 col-sm-offset-2">
                           <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="encuestasEdicion.Guardar();" >Guardar</button>
                           <a href="canjes.aspx" class="btn btn-link">Cancelar</a>
                        </div>
                    </div>

                </form>
            </div>
            </div>
    </div>
</asp:Content>

