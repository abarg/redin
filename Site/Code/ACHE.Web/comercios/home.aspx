﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageComercios.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="comercios_home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .main_content li {
            line-height: 32px;
        }
        .vcard-item, .item-key {
            font-size:14px;
        }
        .item-key {
            width:150px;
        }

        .logo_home {
            margin-left: 350px !important;
            /* vertical-align: top; */
            /* clear: both; */
            top: -170px;
            position: relative;
            max-width: 700px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <%--<h1 class="heading">Bienvenido</h1>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>--%>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div>
                    <br /><br />
                    
                    <div class="vcard">
                        <asp:Image runat="server" ID="imgLogo" style="max-width: 400px" />
					    
                        <ul class="logo_home">
						    <li class="v-heading" id="liSocia" style="font-size: 20px;">Bienvenido <asp:Literal runat="server" ID="litNombre"></asp:Literal></li>
						    <%--<li>
							    <span class="item-key">Nombre</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litNombre2"></asp:Literal></div>
						    </li>--%>
						    <li>
							    <span class="item-key">Razón social</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litRazonSocial"></asp:Literal></div>
						    </li>
						    <li>
							    <span class="item-key">CUIT</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litCuit"></asp:Literal></div>
						    </li>
                            <li>
							    <span class="item-key">Domicilio</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litDom"></asp:Literal></div>
						    </li>
                            <li>
							    <span class="item-key">Teléfono</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litTel"></asp:Literal></div>
						    </li>
                            <li>
							    <span class="item-key">Celular</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litCel"></asp:Literal></div>
						    </li>
                            <li>
							    <span class="item-key">Email</span>
							    <div class="vcard-item"><asp:Literal runat="server" ID="litEmail"></asp:Literal></div>
						    </li>
                            <li style="margin-top:20px; border:0">
                                
                                Para modificar sus datos, por favor comuníquese con atención al cliente: 0800-999-4646 o envíe un mail a <a href="mailto:empresas@redin.com.ar">empresas@redin.com.ar</a>

                            </li>
                        </ul>
                    </div>
                    <br />
                </div>
            </form>
        </div>
    </div>
</asp:Content>

