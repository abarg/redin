﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.IO;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using System.Data;

public partial class comercios_facturas_detalle : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentComerciosUser.Tipo == "B")
                Response.Redirect("home.aspx");

            Session["Detalle_Fc"] = null;

            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
                CargarDetalle(int.Parse(Request.QueryString["Id"]));
            else
                Response.Redirect("facturas.aspx");
        }
    }

    private void CargarDetalle(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var factura = dbContext.Facturas.Include("Comercios")
                .Where(x => x.FechaCAE.HasValue && x.Visible && x.Comercios.NroDocumento == CurrentComerciosUser.NroDocumento && x.IDFactura == id).FirstOrDefault();

            if (factura != null)
            {

                if (!factura.FechaRecibida.HasValue)
                {
                    factura.FechaRecibida = DateTime.Now;
                    dbContext.SaveChanges();
                }

                litNroFc.Text = factura.Numero;
                litPeriodoDesde.Text = factura.PeriodoDesde.ToString("dd/MM/yyyy");
                litPeriodoHasta.Text = factura.PeriodoHasta.ToString("dd/MM/yyyy");
                if (factura.Tipo != "RI")
                {
                    pnlFcB.Visible = true;
                    pnlFcA.Visible = false;
                }
                else
                {
                    pnlFcB.Visible = false;
                    pnlFcA.Visible = true;
                }

                var dtHasta = factura.PeriodoHasta.AddDays(1);

                //litSubtotal.Text = factura.ImporteTotal.ToString("N2");
                //litIva.Text = (factura.ImporteTotal * 0.21M).ToString("N2");
                //litImporte.Text = litImporte2.Text = (factura.ImporteTotal + (factura.ImporteTotal * 0.21M)).ToString("N2");

                var detalle = dbContext.TransaccionesView
                        .Where(x => x.NroDocumento == CurrentComerciosUser.NroDocumento
                        && x.FechaTransaccion >= factura.PeriodoDesde && x.FechaTransaccion <= dtHasta
                        && x.ImporteOriginal > 1)
                        .OrderBy(x => x.FechaTransaccion)
                        .Select(x => new FacturasDetViewModel
                        {
                            Fecha = x.Fecha,
                            //FechaTransaccion = x.FechaTransaccion,
                            Hora = x.Hora,
                            Operacion = x.Operacion,
                            //Comercio = x.NombreFantasia,
                            //NroEstablecimiento = x.NroEstablecimiento,
                            //POSTerminal = x.POSTerminal,
                            Tarjeta = x.Numero,
                            //Socio = x.Apellido + ", " + x.Nombre,
                            //ImporteOriginal = x.ImporteOriginal,
                            //ImporteAhorro = x.ImporteAhorro,
                            Ticket = x.Ticket.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? (x.Ticket.Value - x.CostoRedIn) : ((x.Ticket.Value - x.CostoRedIn) * -1) : 0,
                            Arancel = x.Arancel.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.Arancel.Value : (x.Arancel.Value * -1) : 0,
                            Puntos = x.Puntos.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.Puntos.Value : (x.Operacion == "Canje" ? 0 : (x.Puntos.Value * -1)) : 0,
                            CostoRed = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.CostoRedIn : (x.CostoRedIn * -1)
                        }).ToList();

                foreach (var det in detalle)
                {
                    det.Ticket = MathExt.Round(det.Ticket, 2);
                    det.Arancel = MathExt.Round(det.Arancel, 2);
                    det.Puntos = MathExt.Round(det.Puntos, 2);
                    det.CostoRed = MathExt.Round(det.CostoRed, 2);

                    //if (factura.PeriodoDesde < DateTime.Parse("2016-04-01"))//Por cambio en los gravamenes
                    //{
                        if (det.Operacion == "Canje")
                        {
                            det.NetoGrabado = MathExt.Round(det.Ticket, 2);// -((det.Ticket * 21) / 100);
                            det.Iva = MathExt.Round(det.NetoGrabado * 0.21M, 4); // Math.Round(((det.Ticket * 21) / 100), 2);//det.Iva;
                            det.Total = MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 4);

                            if (factura.PeriodoDesde > DateTime.Parse("2016-05-01"))//Por cambio en los gravamenes
                            {
                                det.NetoGrabado = Math.Abs(det.NetoGrabado);
                                det.Iva = Math.Abs(det.Iva); //esto tiene que ser positivo? sino no tiene sentido guardarlo
                                det.Total = Math.Abs(det.Total); //esto tiene que ser positivo? sino no tiene sentido guardarlo
                                det.Ticket = Math.Abs(det.Ticket);
                                det.Arancel = Math.Abs(det.Arancel); 
                                det.Puntos = Math.Abs(det.Puntos); 
                                det.CostoRed = Math.Abs(det.CostoRed); // esto en canje es siempre 0
                            }
                        }
                        else
                        {
                            det.NetoGrabado = MathExt.Round(det.Puntos + det.Arancel + det.CostoRed, 2);// -((det.Ticket * 21) / 100);
                            det.Iva = MathExt.Round(det.NetoGrabado * 0.21M, 4); //Math.Round(((det.NetoGrabado * 21) / 100), 2);//det.Iva;
                            det.Total = MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 4);
                        }
                    //}
                    //else
                    //{
                    //    if (det.Operacion == "Canje")
                    //    {
                    //        det.NetoGrabado = MathExt.Round(det.Ticket, 2);// -((det.Ticket * 21) / 100);
                    //        det.Iva = 0;// MathExt.Round(det.NetoGrabado * 0.21M, 2); // Math.Round(((det.Ticket * 21) / 100), 2);//det.Iva;
                    //        det.Total = MathExt.Round(det.Ticket, 2);//MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 2);
                    //    }
                    //    else
                    //    {
                    //        var netoIva = MathExt.Round(det.Arancel + det.CostoRed, 2);
                    //        det.NetoGrabado = MathExt.Round(det.Puntos + det.Arancel + det.CostoRed, 2);// -((det.Ticket * 21) / 100);
                    //        det.Iva = MathExt.Round(netoIva * 0.21M, 4); //Math.Round(((det.NetoGrabado * 21) / 100), 2);//det.Iva;
                    //        det.Total = MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 4);
                    //    }
                    //}
                }

                //litIva.Text = Math.Round(detalle.Sum(x => x.Iva), 2).ToString("N2");
                //litImporte.Text = litImporte2.Text = Math.Round(detalle.Sum(x => x.Total), 2).ToString("N2");

                rptDetalle.DataSource = detalle;
                rptDetalle.DataBind();

                var auxPrint = detalle.Select(x => new
                {
                    Fecha = x.Fecha,
                    Hora = x.Hora,
                    Operacion = x.Operacion,
                    Tarjeta = x.Tarjeta,
                    Ticket = x.Ticket,
                    Arancel = x.Arancel,
                    Puntos = x.Puntos,
                    CostoRed = x.CostoRed,
                    NetoGrabado = x.NetoGrabado,
                    Iva = x.Iva,
                    Total = x.Total
                }).ToList();

                var auxTable = auxPrint.ToDataTable();

                decimal totalNeto = MathExt.Round(detalle.Sum(x => x.NetoGrabado), 2);
                decimal totalIva = 0;
                totalIva = Math.Round(detalle.Sum(x => x.Iva), 2); //totalNeto * 0.21M; //
                decimal total = totalNeto + totalIva;// Math.Round(detalle.Sum(x => x.Total), 2);

                //var subTotal = auxPrint.Sum(x => x.NetoGrabado);



                //litSubtotal.Text = totalNeto.ToString("N2");
                //litIva.Text = totalIva.ToString("N2");
                //litImporte.Text = litImporte2.Text = total.ToString("N2");
                //litImporte.Text = litImporte2.Text = auxPrint.Sum(x => x.Total).ToString();

            /***CAMBIO PROVISORIO***/ //logica anterior implementada arria de este codigo
                litImporte2.Text = factura.ImporteTotal.ToString("N2");
                 litImporte.Text =factura.ImporteTotal.ToString("N2");
                 litIva.Text = factura.TotalIva.ToString("N2");
                 litSubtotal.Text = (factura.ImporteTotal - factura.TotalIva).ToString("N2");          

                Session["Detalle_Fc"] = auxTable;
            }

            /*.Select(x => new FacturasFrontViewModel()
                {
                    ID = x.IDFactura,
                    Comercio = x.Comercios.RazonSocial,
                    NroDocumento = x.Comercios.NroDocumento,
                    Numero = x.Numero,
                    Importe = x.ImporteTotal,
                    FechaEmision = x.PeriodoHasta,
                    FechaVto = x.PeriodoHasta
                }).OrderByDescending(x => x.FechaEmision).ToList();

            rptFacturas.DataSource = result;
            rptFacturas.DataBind();

            if (!result.Any())
                trSinFacturas.Visible = true;*/
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar()
    {
        string fileName = "Detalle_Fc";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null && HttpContext.Current.Session["Detalle_Fc"] != null)
        {
            try
            {
                DataTable dt = new DataTable();

                dt = (DataTable)HttpContext.Current.Session["Detalle_Fc"];

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);

        wb.Worksheets.First().Column(9).Style.NumberFormat.Format = "#,##0.00";//Iva
        wb.Worksheets.First().Column(10).Style.NumberFormat.Format = "#,##0.00";//Total

        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}