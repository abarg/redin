﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageComercios.master" AutoEventWireup="true" CodeFile="encuestas.aspx.cs" Inherits="comercios_encuestas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/comercios/encuestas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/comercios/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Encuestas</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Encuestas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form runat="server">
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
                        <div class="col-md-2">
                            <label>Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
					    <div class="col-md-2">
                            <label>Estado</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                            </asp:DropDownList>
                        </div>
				    </div>

                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <a class="btn" type="button" id="btnNuevo" href="/comercios/encuestase.aspx">Nuevo</a>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportarCanjes();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="SeguimientoCanjes" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>

