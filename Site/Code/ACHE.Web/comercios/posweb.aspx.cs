﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class comercios_posweb : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarCombos();
            hdnTieneCatalogo.Value = "1";
            if (!CurrentComerciosUser.POSWeb)
                Response.Redirect("home.aspx");
        }
    }

    private void CargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var result = dbContext.Premios
                .Where(x => x.TipoMov != "B" && !x.IDMarca.HasValue)
                .OrderBy(x => x.Descripcion).ToList()
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDPremio.ToString() + "_" + x.ValorPuntos.ToString(),
                    Nombre = x.Descripcion
                }).ToList();

            ddlPremios.DataSource = result;
            ddlPremios.DataTextField = "Nombre";
            ddlPremios.DataValueField = "ID";
            ddlPremios.DataBind();

            ddlPremios.Items.Insert(0, new ListItem("", ""));
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];

            var comercio = dbContext.Comercios.Where(x => x.IDComercio == usu.IDComercio).FirstOrDefault();
            if (comercio != null && comercio.IDMarca != null)
            {
                var aux = dbContext.Marcas.Where(x => x.IDMarca == comercio.IDMarca).FirstOrDefault();
                if (aux != null)
                {
                    txtPuntoVenta.Text = aux.PuntoDeVenta;

                    txtNroComprobante.Text = aux.NroComprobante;

                    if (!string.IsNullOrEmpty(aux.TipoComprobante))
                        ddlTipoComprobante.SelectedValue = aux.TipoComprobante;
                }
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.SociosView
                                .Where(x => x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                        case "DNI":
                            list = dbContext.SociosView
                                .Where(x => x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas
                                .Where(x => x.Numero.Contains(valor) && x.TipoTarjeta != "G")
                                .OrderBy(x => x.Numero)
                                .Select(x => new Combo3ViewModel()
                                {
                                    ID = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                                    Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                                    Importe = x.Credito.ToString()

                                    //  ID = x.PuntosTotales,
                                    // Nombre = x.Numero
                                }).Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                    }
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {

                var dtHoy = DateTime.Now.Date;
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio && x.TipoTarjeta != "G"
                        && (x.FechaBaja == null || (x.FechaBaja.HasValue && x.FechaBaja.Value > dtHoy)))
                     .OrderBy(x => x.Numero)
                     .Select(x => new Combo3ViewModel() { ID = x.PuntosTotales, Nombre = x.Numero, Importe = x.Credito.ToString() })
                     .Distinct().OrderBy(x => x.Nombre).ToList();
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string grabar(string tipo, string tarjeta, string importe, string descripcion,
        string idPremio, string puntoVenta, string tipoComprobante, string nroComprobante)
    {

        string idTransaccion = "";

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];


                    var terminal = dbContext.Terminales.Where(x => x.IDComercio == usu.IDComercio).FirstOrDefault();
                    if (terminal != null)
                    {

                        decimal currentImporte = decimal.Parse(importe);
                        idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "Web", "", descripcion, currentImporte, "", "", "", "", tarjeta, "", tipo, "000000000000", "1100", puntoVenta, nroComprobante, tipoComprobante, "C-" + usu.Usuario);
                    }
                    else
                        throw new Exception("No tiene terminales cargadas en el sistema.");
                }

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }


        return idTransaccion;
    }

    [System.Web.Services.WebMethod(true)]
    public static ComboViewModel obtenerInfoSocio(int id)
    {
        try
        {
            ComboViewModel info = new ComboViewModel();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                    if (aux != null)
                    {
                        info.ID = aux.NroDocumento == "00" ? "No disponible" : aux.NroDocumento;
                        info.Nombre = aux.Foto ?? "";
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string anularOperacion(int id)
    {
        string idTransaccion = "";

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            try
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int idComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Transacciones.Where(x => x.IDTransaccion == id && (x.Operacion == "Venta" || x.Operacion == "Canje")).FirstOrDefault();
                    if (aux == null)
                        throw new Exception("El ID ingresado es inexistente o no corresponde a una venta/canje.");

                    int IDTerminal = dbContext.Terminales.Where(x => x.POSTerminal == aux.Terminales.POSTerminal).FirstOrDefault().IDTerminal;

                    idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, IDTerminal, "Web", "", "Anulacion via web de ID " + id, aux.Importe,
                        "", "", "", aux.IDTransaccion.ToString(), aux.NumTarjetaCliente, "", "Anulacion", aux.PuntosDisponibles, "1100", aux.PuntoDeVenta, "", "", "C-" + usu.Usuario);

                }

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        return idTransaccion;
    }

    [System.Web.Services.WebMethod(true)]
    public static TransaccionesViewModel buscarTrParaAnular(int id)
    {
        try
        {
            TransaccionesViewModel info = new TransaccionesViewModel();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int idComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.TransaccionesView.Where(x => x.IDComercio == idComercio && x.IDTransaccion == id && (x.Operacion == "Venta" || x.Operacion == "Canje")).FirstOrDefault();
                    if (aux == null)
                        throw new Exception("El ID ingresado es inexistente o no corresponde a una venta/canje.");
                    else
                    {
                        info.Puntos = aux.PuntosAContabilizar ?? 0;
                        info.Tarjeta = aux.Numero;
                        info.ImporteOriginal = aux.Ticket ?? 0;
                        info.Fecha = aux.FechaTransaccion.ToString("dd/MM/yyyy");
                        info.Hora = aux.FechaTransaccion.ToString("HH:mm:ss");
                        info.Socio = aux.Nombre + ", " + aux.Apellido;
                        info.Comercio = aux.NombreFantasia;
                        info.Origen = aux.Origen;
                        info.Operacion = aux.Operacion;
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            //var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}