﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class comercios_giftweb : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentComerciosUser.GiftWeb)
            Response.Redirect("home.aspx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.SociosView
                                .Where(x => x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().Take(10).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "DNI":
                            list = dbContext.SociosView
                                .Where(x => x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().Take(10).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas
                                .Where(x => x.Numero.Contains(valor) && x.TipoTarjeta == "G")
                                .OrderBy(x => x.Numero)
                                .Select(x => new Combo3ViewModel()
                                {
                                    ID = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                                    Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                                    Importe = x.Giftcard.ToString()

                                }).Distinct().Take(10).OrderBy(x => x.Nombre).ToList();
                            break;
                    }

                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo2ViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var dtHoy = DateTime.Now.Date;

                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio && x.TipoTarjeta == "G"
                        && (x.FechaBaja == null || (x.FechaBaja.HasValue && x.FechaBaja.Value > dtHoy)))
                     .OrderBy(x => x.Numero)
                     .Select(x => new Combo2ViewModel() { ID = (int)x.Giftcard, Nombre = x.Numero })
                     .Distinct().OrderBy(x => x.Nombre).ToList();
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string grabar(string tipo, string tarjeta, string importe, string descripcion,
        string puntoVenta, string tipoComprobante, string nroComprobante)
    {

        string idTransaccion = "";

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            try
            {
                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(tarjeta, false);
                if (oTarjeta == null)
                    throw new Exception("El Número de Tarjeta no existe");
                else
                {
                    decimal currentImporte = decimal.Parse(importe);

                    //string codigo = "";
                    if (tipo == "Descarga")
                    {
                        //    bPremio bPremio = new bPremio();
                        //    Premios premio = bPremio.getPremio(int.Parse(idPremio));

                        //    codigo = premio.Codigo;

                        int saldoTarjeta = (int)oTarjeta.Giftcard;
                        if (currentImporte > saldoTarjeta)
                            throw new Exception("La tarjeta no tiene saldo suficiente. Saldo actual: " + oTarjeta.Giftcard.ToString("N2"));
                    }

                    var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                    int idComercio = usu.IDComercio;
                    using (var dbContext = new ACHEEntities())
                    {

                        var terminal = dbContext.Terminales.Where(x => x.IDComercio == usu.IDComercio).FirstOrDefault();
                        if (terminal != null)
                        {
                            idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "Web", "", descripcion, currentImporte,
                           "", "", "", "", tarjeta, "", tipo, "000000000000", "2200", puntoVenta, "", "", "A-" + usu.Usuario);
                        }
                        else
                            throw new Exception("El comercio seleccionado no tiene terminales cargadas en el sistema.");
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        return idTransaccion;
    }

    [System.Web.Services.WebMethod(true)]
    public static ComboViewModel obtenerInfoSocio(int id)
    {
        try
        {
            ComboViewModel info = new ComboViewModel();
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                    if (aux != null)
                    {
                        info.ID = aux.NroDocumento == "00" ? "No disponible" : aux.NroDocumento;
                        info.Nombre = aux.Foto ?? "";
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string getSaldoPorNumero(string tarjeta)
    {
        string saldoGC;
        using (var dbContext = new ACHEEntities())
        {
            saldoGC = dbContext.Tarjetas.Where(x => x.Numero == tarjeta && x.TipoTarjeta == "G").FirstOrDefault().Giftcard.ToString();
            if (saldoGC != null)
                return saldoGC;
        }
        return "";
    }

}