﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

public partial class comercios_alertas_seguimiento : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentComerciosUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> buscarTarjetas(string tarjeta)
    {
        try
        {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();

            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int IDComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.Numero.Contains(tarjeta)).OrderBy(x => x.Numero)
                     .Select(x => new Combo2ViewModel
                     {
                         Nombre = x.Numero,
                         ID = x.IDTarjeta
                     }).Take(10).ToList();


                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void agregarTarjeta(int idTarjeta)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int IDComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    if (!dbContext.AlertasTarjetas.Any(x => x.IDComercio == IDComercio && x.IDTarjeta == idTarjeta))
                    {
                        AlertasTarjetas alerta = new AlertasTarjetas();
                        alerta.IDComercio = IDComercio;
                        alerta.IDTarjeta = idTarjeta;

                        dbContext.AlertasTarjetas.Add(alerta);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void eliminarTarjeta(int idTarjeta)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int IDComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    AlertasTarjetas alerta = dbContext.AlertasTarjetas.FirstOrDefault(x => x.IDComercio == IDComercio && x.IDTarjeta == idTarjeta);
                    if (alerta != null)
                    {
                        dbContext.AlertasTarjetas.Remove(alerta);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static List<SociosViewModel> getTarjetasEnSeguimiento()
    {
        List<SociosViewModel> list = new List<SociosViewModel>();

        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int IDComercio = usu.IDComercio;


            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDComercio == IDComercio)
                 .Select(x => new SociosViewModel
                 {
                     Tarjeta = x.Tarjetas.Numero,
                     Nombre = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Nombre : "",
                     Apellido = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido : "",
                     NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                     IDSocio = x.IDTarjeta
                 }).OrderByDescending(x => x.Apellido).ToList();
            }
        }

        return list;
    }
}