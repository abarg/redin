﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;

public partial class comercios_dashboard : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentComerciosUser.Tipo == "B")
                Response.Redirect("home.aspx");
        }

       litFechaTit.Text = "Desde " + DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy") + " hasta " + DateTime.Now.ToString("dd/MM/yyyy");
    }

    #region Graficos

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosOtorgados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PuntosOtorgados " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PuntosOtorgados " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PuntosOtorgados " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PuntosOtorgados " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorro()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteAhorro " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteAhorro " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteAhorro " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteAhorro " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagado()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImportePagado " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImportePagado " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImportePagado " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImportePagado " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginal()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteOriginal " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteOriginal " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteOriginal " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_ImporteOriginal " + idComercio + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Socios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Comercios_TopSocios " + idComercio, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10SociosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Comercios_TopSociosMensual '" + fechaDesde + "','" + fechaHasta + "', " + idComercio, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorroDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Comercios_ImporteAhorroPorDias " + idComercio + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginalDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);//"01-20-2014";// 
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Comercios_ImporteOriginalPorDias " + idComercio + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagadoDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Comercios_ImportePagadoPorDias " + idComercio + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    #endregion

    #region Estadisticas Superiores

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalSociosPorSexo()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalSociosPorSexo " + idComercio, new object[] { }).ToList();
                if (list.Any())
                {

                    html = "F: " + list[0].data + " - M: " + list[1].data + " <br/> I: " + (list[2].data + list[3].data);
                    decimal total = list[0].data + list[1].data + list[2].data + list[3].data;
                    html += "," + total;
                }
                else
                    html = "Error";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTR()
    {
        var mensual = string.Empty;
        var anual = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var totalMensual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + idComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalMensual.Any())
                    mensual = totalMensual[0].data.ToString();
                else
                    mensual = "0";

                var totalAnual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + idComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalAnual.Any())
                    anual = totalAnual[0].data.ToString();
                else
                    anual = "0";
            }
        }

        return "Mensuales: " + mensual + " <br/> Anuales: " + anual;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicket()
    {
        var mensual = string.Empty;
        var anual = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var totalMensual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + idComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalMensual.Any())
                    mensual = Math.Abs(totalMensual[0].data).ToString("N2");
                else
                    mensual = "0";

                var totalAnual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + idComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalAnual.Any())
                    anual = Math.Abs(totalAnual[0].data).ToString("N2");
                else
                    anual = "0";
            }
        }

        return "Mensual: $" + mensual + " <br/> Anual: $" + anual;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerFacturacion()
    {
        var mensual = string.Empty;
        var anual = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int idComercio = usu.IDComercio;

            string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var totalMensual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_Facturacion " + idComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalMensual.Any())
                    mensual = Math.Abs(totalMensual[0].data).ToString("N2");
                else
                    mensual = "0";

                var totalAnual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_Facturacion " + idComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (totalAnual.Any())
                    anual = Math.Abs(totalAnual[0].data).ToString("N2");
                else
                    anual = "0";
            }
        }

        return "Mensual: $" + mensual + " <br/> Anual: $" + anual;
    }

    #endregion
}