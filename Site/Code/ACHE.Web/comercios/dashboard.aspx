﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageComercios.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="comercios_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!--[if lt IE 9]>
		<script src="/lib/flot/excanvas.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
		<div class="col-sm-12 tac">
			<h3 class="heading"><asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
            <ul class="ov_boxes">
				<li>
					<div class="p_bar_up p_canvas">
                        <img src="../../img/dashboard/money-graph.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0">
						<strong id="resultado_ticketpromedio_mensual" style="font-size: 13px;padding-top: 9px;">Calculando...</strong>
						Promedio ticket
					</div>
				</li>
				<li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/transaction.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0">
						<strong id="resultado_tr_mensual" style="font-size: 13px;padding-top: 9px;">Calculando...</strong>
						Cant. transacciones
					</div>
				</li>
                <li>
					<div class="p_bar_down p_canvas">
                        <img src="../../img/dashboard/sex.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0">
						<strong id="resultado_sexo" style="font-size: 13px;padding-top: 9px;">Calculando...</strong><%--F: 100 - M: 300--%>
						Total Socios: <span id="totalSexo"></span>
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/billete.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0">
						<strong id="resultado_facturacion" style="font-size: 13px;padding-top: 9px;">Calculando...</strong>
						Total facturado
					</div>
				</li>
			</ul>
		</div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Puntos otorgados <small>últimos 4 meses</small></h3>
            <div id="fl_puntos_otorgados" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Facturación & Ahorro <small>últimos 4 meses</small></h3>
            <div id="fl_facturacion_ahorro" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
			<h3 class="heading">Facturación & Ahorro <small>últimos 30 días</small></h3>
            <div id="fl_facturacion_ahorro_detalle" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-sm-6 col-lg-6"  >
            <div class="heading clearfix">
                <h3 class="pull-left">TOP 10 Mejores socios</h3>
            </div>
            <a onclick="mostrarSociosMensual();" href="#">Vista mensual&nbsp;|</a> 
            <a onclick="mostrarSociosGeneral();" href="#" >Histórico</a>
            <table id="tblSocios"class="table table-striped table-bordered mediaTable" style="display: none" >
				<thead>
					<tr>
						<th class="essential persist">Nombre y Apellido</th>
						<th class="optional">Tarjeta</th>
						<th class="essential">Importe</th>
					</tr>
				</thead>
				<tbody id="bodySocios">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>

            <table id="tblSociosMensual"class="table table-striped table-bordered mediaTable">
                <thead>
                    <tr>
                        <th class="essential persist">Nombre y Apellido</th>
                        <th class="optional">Tarjeta</th>
                        <th class="essential">Importe</th>
                    </tr>
                </thead>
                <tbody id="bodySociosMes">
                    <tr>
                        <td colspan="3">Calculando...</td>
                    </tr>
                </tbody>
            </table>
         </div>
        </div>
        <div class="col-sm-6 col-lg-6">
        </div>
	</div>
    
    
	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/comercios/dashboard.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

