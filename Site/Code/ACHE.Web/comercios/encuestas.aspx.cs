﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Linq.Expressions; 

public partial class comercios_encuestas : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        
        }
    }

    private void CargarCombos()
    {

    }


    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {

        int idComercio = 0;
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            idComercio = usu.IDComercio;
        }
        if (idComercio > 0)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.ComerciosEncuestas
                    .Where(x => x.IDComercio == idComercio)
                    .OrderBy(x => x.IDEncuesta)
                    .Select(x => new ComerciosReviewsViewModel()
                    {
                        IDEncuesta = x.IDEncuesta,
                        Detalle = x.Pregunta,
                        Rango = x.RangoMin + " - " + x.RangoMax,
                        avgRating = x.ComerciosReviews.Average(n => n.Rating),
                        totalAnswers = x.ComerciosReviews.Count,
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;

    }

   

}