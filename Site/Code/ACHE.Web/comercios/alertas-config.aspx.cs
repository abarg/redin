﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class comercios_alertas_config : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentComerciosUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
            int IDComercio = usu.IDComercio;
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Alertas
              .Where(x => x.IDComercio.HasValue && x.IDComercio == IDComercio)
              .OrderBy(x => x.Nombre)
              .Select(x => new AlertasViewModel()
              {
                  ID = x.IDAlerta,
                  Nombre = x.Nombre,
                  Tipo = x.Tipo,
                  Prioridad = x.Prioridad == "A" ? "Alta" : (x.Prioridad == "M" ? "Media" : "Baja"),
                  Activa = x.Activa ? "Si" : "No",
                  Restringido = x.Restringido ? "Tarjetas en seguimiento" : "Todas las tarjetas"
              }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        
        }
        else
            return null;
       
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
            {
                var usu = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];
                int IDComercio = usu.IDComercio;

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Alertas.Where(x => x.IDAlerta == id
                        && x.IDComercio.HasValue && x.IDComercio.Value == IDComercio).FirstOrDefault();
                    if (entity != null)
                    {

                        dbContext.Alertas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}