﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class comercios_mis_datos : PaginaComerciosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            this.cargarDatosUsuario();
    }

    [WebMethod(true)]
    public static void grabar(string email)
    {
        if (HttpContext.Current.Session["CurrentComerciosUser"] != null)
        {
            var user = (WebComerciosUser)HttpContext.Current.Session["CurrentComerciosUser"];

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.UsuariosMarcas.Where(x => x.Email == email.Trim()).FirstOrDefault();
                if (aux != null && aux.IDUsuario != user.IDUsuario)
                    throw new Exception("El email ya se encuentra registrado.");

                UsuariosMarcas entity = dbContext.UsuariosMarcas.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                //entity.Name = name;
                //entity.LastName = lastName;
                entity.Email = email;
                //if (pwd != string.Empty)
                //    entity.Pwd = pwd;

                user.Email = email;

                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentComerciosUser"] = user;
        }
    }

    private void cargarDatosUsuario()
    {
        WebComerciosUser user = CurrentComerciosUser;
        //this.txtName.Text = user.Nombre;
        //this.txtLastname.Text = user.Apellido;
        this.litUsuario.Text = user.Usuario;
        this.txtEmail.Text = user.Email;
    }
}