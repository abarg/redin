﻿<%@ WebHandler Language="C#" Class="fileHandler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using ACHE.Model;

public class fileHandler : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        string file = context.Request.QueryString["file"];
        string type = context.Request.QueryString["type"];
        string module = context.Request.QueryString["module"];

        //System.IO.File.Exists(context.Server.MapPath(filePath))
        if (!string.IsNullOrEmpty(file) && !string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(module))
        {
            string fileName = string.Empty;
            if (HasAccess(module, file, type, context, out fileName))
            {
                if (fileName != string.Empty)
                {
                    //string extension = fileName.Split('.')[1].ToLower();
                    string filePath = "~/files/" + type + "/" + fileName;

                    file = fileName;

                    // Process the file in 4K blocks
                    byte[] dataBlock = new byte[0x1000];
                    long fileSize;
                    int bytesRead;
                    long totalBytesRead = 0;

                    /*using (var fs = new System.IO.FileStream(context.Server.MapPath(filePath),
                        System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                    {
                        fileSize = fs.Length;

                        context.Response.Clear();
                        context.Response.ContentType = "application/octet-stream";
                        context.Response.AddHeader("Content-Length", fs.Length.ToString());
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file);

                        while (totalBytesRead < fileSize)
                        {
                            if (!context.Response.IsClientConnected)
                                break;

                            bytesRead = fs.Read(dataBlock, 0, dataBlock.Length);
                            context.Response.OutputStream.Write(dataBlock, 0, bytesRead);
                            context.Response.Flush();
                            totalBytesRead += bytesRead;
                        }

                        context.Response.End();
                    }*/

                    context.Response.Clear();
                    //context.Response.BufferOutput = false;  // for large files
                    context.Response.ContentType = "application/octet-stream";
                    //I have set the ContentType to "application/octet-stream" which cover any type of file
                    context.Response.AddHeader("content-disposition", "attachment;filename=" + System.IO.Path.GetFileName(filePath.Replace(",", "")));
                    context.Response.WriteFile(filePath);
                    context.Response.End();
                    
                    /*context.Response.Clear();
                    context.Response.ClearHeaders();
                    context.Response.ClearContent();
                    context.Response.Buffer = false;  // for large files
                    context.Response.ContentType = "application/octet-stream";
                    //I have set the ContentType to "application/octet-stream" which cover any type of file
                    context.Response.AddHeader("content-disposition", "attachment;filename=" + System.IO.Path.GetFileName(filePath));
                    context.Response.TransmitFile(context.Server.MapPath(filePath));
                    context.Response.End();*/
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("ERROR: Archivo desconocido!");
                }
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("ERROR: Permiso inválido!");
            }
        }
        else
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("ERROR: Parametros incorrectos!");
        }
    }

    private bool HasAccess(string module, string file, string type, HttpContext context, out string fileName)
    {
        bool hasAccess = false;
        fileName = string.Empty;

        if (module.ToLower() == "marcas")
        {
            if (context.Session["CurrentMarcasUser"] != null )
            {
                WebMarcasUser user = (WebMarcasUser)context.Session["CurrentMarcasUser"];
                int idMarca = user.IDMarca;
                if (type == "facturas")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        int idFc = int.Parse(file);
                        var factura = dbContext.Facturas.Include("Comercios").Where(x => x.IDFactura == idFc).FirstOrDefault();
                        if (factura != null)
                        {
                            if (factura.Comercios.IDMarca == user.IDMarca && factura.FechaCAE.HasValue)
                            {
                                hasAccess = true;
                                fileName = factura.Numero + ".pdf";
                                if (!factura.FechaRecibida.HasValue)
                                    factura.FechaRecibida = DateTime.Now;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
                else if (type == "socios")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        var socios = dbContext.SociosView.Where(x => x.IDMarca == idMarca && x.Foto == file).FirstOrDefault();
                        if (socios != null)
                        {
                            hasAccess = true;
                            fileName = socios.Foto;
                        }
                    }
                }
                else if (type == "premios")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        var premio = dbContext.Premios.Where(x => x.IDMarca == idMarca && x.Foto == file).FirstOrDefault();
                        if (premio != null)
                        {
                            hasAccess = true;
                            fileName = premio.Foto;
                        }
                    }
                }
            }
        }
        else if (module.ToLower() == "franquicias")
        {
            if (context.Session["CurrentFranquiciasUser"] != null)
            {
                WebFranquiciasUser user = (WebFranquiciasUser)context.Session["CurrentFranquiciasUser"];
                int idFranquicia = user.IDFranquicia;
                if (type == "socios")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        var socios = dbContext.SociosView.Where(x => x.IDFranquicia == idFranquicia && x.Foto == file).FirstOrDefault();
                        if (socios != null)
                        {
                            hasAccess = true;
                            fileName = socios.Foto;
                        }
                    }
                }
                if (type == "logos") {
                    using (var dbContext = new ACHE.Model.ACHEEntities()) {
                        var socios = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia && x.Logo == file).FirstOrDefault();
                        if (socios != null) {
                            hasAccess = true;
                            fileName = socios.Logo;
                        }
                    }
                }
                //else if (type == "premios")
                //{
                //    using (var dbContext = new ACHE.Model.ACHEEntities())
                //    {
                //        var premio = dbContext.Premios.Where(x => x.IDMarca == idMarca && x.Foto == file).FirstOrDefault();
                //        if (premio != null)
                //        {
                //            hasAccess = true;
                //            fileName = premio.Foto;
                //        }
                //    }
                //}
            }
        }
        else if (module.ToLower() == "comercios")
        {
            if (context.Session["CurrentComerciosUser"] != null)
            {
                WebComerciosUser user = (WebComerciosUser)context.Session["CurrentComerciosUser"];
                if (type == "facturas")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        int idFc = int.Parse(file);
                        var factura = dbContext.Facturas.Include("Comercios").Where(x => x.IDFactura == idFc).FirstOrDefault();
                        if (factura != null)
                        {
                            if (factura.Comercios.NroDocumento == user.NroDocumento && factura.FechaCAE.HasValue)
                            {
                                hasAccess = true;
                                fileName = factura.Numero + ".pdf";
                                if (!factura.FechaRecibida.HasValue)
                                    factura.FechaRecibida = DateTime.Now;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
                /*else if (type == "comercios")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        int idFc = int.Parse(file);
                        var factura = dbContext.Facturas.Include("Comercios").Where(x => x.IDFactura == idFc).FirstOrDefault();
                        if (factura != null)
                        {
                            if (factura.Comercios.NroDocumento == user.NroDocumento && factura.FechaCAE.HasValue)
                            {
                                hasAccess = true;
                                fileName = factura.Numero + ".pdf";
                                if (!factura.FechaRecibida.HasValue)
                                    factura.FechaRecibida = DateTime.Now;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }*/
            }
        }
        else if (module.ToLower() == "empresas")
        {
            if (context.Session["CurrentEmpresasUser"] != null)
            {
                WebEmpresasUser user = (WebEmpresasUser)context.Session["CurrentEmpresasUser"];
                if (type == "facturas")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        int idFc = int.Parse(file);
                        var factura = dbContext.Facturas.Include("Comercios").Where(x => x.IDFactura == idFc).FirstOrDefault();
                        if (factura != null)
                        {
                            if (dbContext.EmpresasComercios.Any(x => x.IDEmpresa == user.IDEmpresa && x.Comercios.NroDocumento == factura.Comercios.NroDocumento))
                            {
                                if (factura.FechaCAE.HasValue)
                                {
                                    hasAccess = true;
                                    fileName = factura.Numero + ".pdf";
                                    if (!factura.FechaRecibida.HasValue)
                                        factura.FechaRecibida = DateTime.Now;
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                /*else if (type == "comercios")
                {
                    using (var dbContext = new ACHE.Model.ACHEEntities())
                    {
                        int idFc = int.Parse(file);
                        var factura = dbContext.Facturas.Include("Comercios").Where(x => x.IDFactura == idFc).FirstOrDefault();
                        if (factura != null)
                        {
                            if (factura.Comercios.NroDocumento == user.NroDocumento && factura.FechaCAE.HasValue)
                            {
                                hasAccess = true;
                                fileName = factura.Numero + ".pdf";
                                if (!factura.FechaRecibida.HasValue)
                                    factura.FechaRecibida = DateTime.Now;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }*/
            }
        }
        else if (module.ToLower() == "admin")
        {
            if (context.Session["CurrentUser"] != null)
            {
                hasAccess = true;
                fileName = file;
            }
        }

        return hasAccess;
    }

    public bool IsReusable
    {
        get
        {
            return true;
        }
    }

}