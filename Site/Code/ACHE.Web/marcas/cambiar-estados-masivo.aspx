﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="cambiar-estados-masivo.aspx.cs" Inherits="marcas_cambiar_estados_masivo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h3 class="heading">Editar Estados Masivo</h3>
    <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
    <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente</div>
    <form id="form" runat="server">
        <div class="formSep col-sm-12 col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <label>Fecha desde</label>
                    <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                </div>
                <div class="col-md-2">
                    <label>Fecha hasta</label>
                    <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                </div>
                <div class="col-md-2">
                    <label>Estado</label>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label>Familia</label>
                    <asp:DropDownList runat="server" ID="cmbFamilia" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label>Socio</label>
                    <input type="text" id="txtSocio" value="" maxlength="100" class="form-control" />
                </div>
                <div class="col-sm-2">
                    <label>Producto</label>
                    <input type="text" id="txtProducto" value="" maxlength="100" class="form-control" />
                </div>
            </div>
            <div class="row" style="margin-left: 0px">
                <button class="btn" type="button" id="btnBuscar" onclick="filtrarEstadosMasivo();">Buscar</button>
                <button class="btn btn-success" type="button" id="btnExportar" onclick="$('#modalDetalle').modal('show');">Actualizar Estados Masivamente</button>
            </div>
        </div>
        <div class="formSep col-sm-12 col-md-12">
            <div class="row">
                <label class="col-lg-12 control-label"></label>
                <div class="col-lg-12" id="tblCostos">
                    <table class="table table-striped table-bordered mediaTable">
                        <thead>
                            <tr>
                                <th style="width: 100px">Fecha</th>
                                <th style="width: 100px">Producto</th>
                                <th style="width: 100px">Cantidad</th>
                                <th style="width: 100px">Familia</th>
                                <th style="width: 100px">Socio</th>
                                <th style="width: 100px">Estado</th>
                            </tr>
                        </thead>
                        <tbody id="bodyCanjes">
                            <tr>
                                <td colspan="6">Calculando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row stock">
            <div class="col-sm-3">
                <br />
                <button class="btn btn-success" type="button" id="btnActualizarEstados" onclick="ActualizarEstados();">Actualizar estados</button>
            </div>
        </div>

        <div class="modal fade" id="modalDetalle">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="titRetenciones">Actualización masiva de estados</h3>
                    </div>
                    <div class="modal-body" style="height: 100px;">

                        <div class="col-sm-6">
                            Seleccione el nuevo estado:
                            <asp:DropDownList runat="server" class="form-control" ID="cmbEstadoMasivo"></asp:DropDownList>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="button" onclick="actualizarMasivamete();filtrar();">Aceptar</button>
                        <button class="btn btn-default" type="button" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/cambiar-estados-masivo.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

