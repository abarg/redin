﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="catalogo.aspx.cs" Inherits="marcas_catalogo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .search_page .box_view .search_item {
            height: auto;
        }
    </style>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Catálogo</li>
        </ul>
    </div>
    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
        <div class="row search_page">
            <div class="col-sm-12 col-md-12">
                <h3 class="heading">Catálogo de productos</h3>
                <div class="well clearfix">
                    <%--<div class="pull-left">Mostrando 1 - 20 de 204 Resultados</div>--%>
                    <div class="pull-left">
                        <span class="sepV_c">Socio:
                            <asp:DropDownList runat="server" class="form-control chzn_b" ID="ddlSocios" data-placeholder="Seleccione un socio" Style="top: 7px;" />
                        </span>
                        <br />
                        <div style="margin-top:10px">
                            <span class="sepV_c">Producto:
					            <input class="form-control" type="text" id="txtNombre" style="width: 100px; display: inline;" />
                            </span>
                            <span class="sepV_c">Familia:
					            <asp:DropDownList runat="server" class="form-control input-sm" ID="cmbFamilia">
                                </asp:DropDownList>

                            </span>
                            <span class="sepV_c">Sub familia:
					            <asp:DropDownList runat="server" class="form-control input-sm" ID="cmbSubFamilia">
                                </asp:DropDownList>
                            </span>
                        </div>
                       
                        <div style="margin-top:10px">
                            <span class="sepV_c">Puntos:
					            <input placeholder="desde" class="form-control" type="text" id="txtPuntosDesde" style="width: 100px; display: inline;" />
                                <input placeholder="hasta" class="form-control" type="text" id="txtPuntosHasta" style="width: 100px; display: inline;" />
                            </span>
                            <span class="sepV_c">Mostrar:
					            <select class="form-control input-sm" id="ddlPageSize">
                                    <option value="12">12</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select>
                            </span>
                            <span class="sepV_c">&nbsp;</span>
                            <button id="btnBuscar" class="btn btn-primary btn-sm" type="button" onclick="reiniciar();">Buscar</button>
                        </div>
                    </div>
                </div>
                <div class="search_panel clearfix box_view" id="results">
                </div>
                <div class="col-sm-12 col-md-12" id="divSinResultados" style="display: none">
                    <div class="alert alert-info alert-dismissable">
                        <strong>Lo siento!</strong> No se han encontrado premios disponibles.
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script id="resultTmpl" type="text/x-jquery-tmpl">
        <!--Item-->
        <div class="search_item clearfix" style="max-height: 112px;">
            <%--<span class="searchNb">${IDPremio}</span>--%>
           {{if StockActual != "0"}}
                    <a onclick="canjearProducto(${IDPremio})" style="float: right;">
                        <img alt="canjear" title="canjear" src="/files/premios/redeem.png" /></a>
            <%--<a href="canjese.aspx?IdProducto=${IDPremio}_${Imagen}_${Precio}_${ValorPuntos}" style="float: right;"><img alt="canjear" title="canjear" src="/files/premios/redeem.png" /></a>--%>
                    {{/if}}
             <div class="thumbnail pull-left">
                 <img alt="${Descripcion}" title="${Descripcion}" src="/files/marcas/fotos/${Imagen}" style="width: 80px; height: 80px" />
             </div>
            <div class="search_content" style="max-height: 95px">
                <h4>${Nombre}
                    <span class="label label-info">${Rubro}</span>
                </h4>
                <h4>
                    <a href="javascript:void(0)" class="sepV_a">${Codigo}</a>
                </h4>
                <p class="sepH_b item_description" style="font-size: 11px;">${Descripcion}</p>
                {{if StockActual == "0"}}
                    <p class="sepH_a"><strong style="color: red">NO Disponible</strong></p>
                {{else}}
                    <p class="sepH_a"><strong>Stock:</strong> ${StockActual}. <small>Valor puntos: ${ValorPuntos} - Valor pesos: ${Precio}</small></p>
                {{/if}}
                
                <%--<p class="sepH_a"><strong>canjear</strong></p>--%>
            </div>
        </div>
        <!--Fin Item-->
    </script>

    <input type="hidden" id="hdnPage" value="1" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/catalogo.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/tmpl/jquery.tmpl.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
