﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="email-cumpleanios.aspx.cs" Inherits="marcas_email_cumpleanios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/email-cumpleanios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/redactor.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/redactor.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Email</a></li>
            <li>Edición de Email de cumpleanios</li>
        </ul>
    </div>
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Email de Bienvenida</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" role="form">
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtEmail" ClientIDMode="Static" CssClass="form-control htmlEditor" TextMode="MultiLine" Rows="4"></asp:TextBox>
                </div>
               
                <div class="form-group">

                    <div>
                       <br/>
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a href="home.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

