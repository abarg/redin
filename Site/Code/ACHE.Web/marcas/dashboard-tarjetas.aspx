﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="dashboard-tarjetas.aspx.cs" Inherits="marcas_dashboard_tarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
                height: 56px !important;
                opacity: 1 !important;
                top: 8px;
                right: 10px;
                margin-right: -5px;
                width: 0px !important;
                background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }
        
        .modal.modal-wide .modal-dialog {
          width: 90%;
        }
        .modal-wide .modal-body {
          overflow-y: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Tarjetas y socios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn btn-primary" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Dashboard-tarjetas" style="display:none">Descargar</a>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="NuevaTr();">Nuevo</button>--%>
                        </div>
                    </div>            
                </div>
            </form>
        </div>
    </div>

      <div class="row">
        <div class="col-sm-12 col-lg-6">
			<h3 class="heading">Tarjetas asignadas <small>últimos 6 meses</small></h3>
            <div id="fl_tarjetas_asignadas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-12 col-lg-6">
			<h3 class="heading" id="lblActivas">Tarjetas activas e inactivas</h3>
            <div id="fl_tarjetas_activas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
     <div class="row">        
        <div class="col-sm-12 col-lg-8">
			<h3 class="heading" id="lblEstadoTerminales">Estado de las terminales</h3>
            <div id="fl_estado_terminales" style="height:270px;width:100%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
         <div class="modal modal-wide fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>SDS</th> 
                                <th>Nombre</th> 
                                <%--<th>Franquicia</th> 
                                <th>Marca</th> --%>
                                <th>Domicilio</th> 
                                <th>Fecha Carga</th> 
                                <th>Fecha Alta</th> 
                                <th>Terminal</th>
                                <th>Establecimiento</th>
                                <th>Fecha Activ</th>
                                <th>Fecha Reprog</th>
                                <th>Reprogramado</th>
                                <th>Invalido</th>
                                <th>Activo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <%--<button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>--%>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
   <div class="row mt3">
        <div class="col-sm-12 col-lg-6">
			<h3 class="heading" id="lblSexo">Total usuarios por sexo</h3>
            <div id="fl_graphSexo" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
                
            </div>
        </div>
    </div>
    	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/dashboard-tarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

