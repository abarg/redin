﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Linq.Expressions; 

public partial class marcas_imprimir_etiquetas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string estado = Request.QueryString["estado"];
        string tarjeta = Request.QueryString["tarjeta"];
        string documento = Request.QueryString["documento"];
        string fchdsd = Request.QueryString["fchdsd"];
        string fchhst = Request.QueryString["fchhst"];
        string familia = Request.QueryString["familia"];
        string producto = Request.QueryString["producto"];
        string socio = Request.QueryString["socio"];

       bodyetiquetas.InnerHtml = ImprimirEtiquetas(estado, tarjeta, documento, familia, fchdsd, fchhst, producto, socio);
       }
    
    
    /*IMPRIMIR ETIQUETAS*/
    [System.Web.Services.WebMethod(true)]
    public static string ImprimirEtiquetas(string estado, string tarjeta, string nroDoc, string idFamilia, string fechaDesde, string fechaHasta, string producto, string socio)
        {
        DataTable dt = new DataTable();


        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var result = dbContext.Canjes.Include("Productos").Include("Tarjetas").Include("Premios").Include("EstadosCanjes")
                        .Where(x => x.Tarjetas.IDMarca == idMarca)
                        .OrderBy(x => x.FechaAlta).Select(x => new SeguimientoCanjesViewModel()
                        {
                            IDCanje = x.IDCanje,
                            Estado = x.EstadosCanjes.Nombre,
                            Iestado = x.IDEstado.ToString(),
                            Fecha = x.FechaAlta,
                            Producto = x.Productos.Nombre,
                            Cantidad = x.Cantidad,
                            Socio = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + ", " + x.Tarjetas.Socios.Nombre : "",
                            Tarjeta = x.Tarjetas.Numero,
                            NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                            Familia = x.Productos.IDFamilia.ToString(),
                        });

                    if (fechaDesde != "")
                    {
                        DateTime dtDsd = DateTime.Parse(fechaDesde);
                        result = result.Where(x => x.Fecha >= dtDsd).OrderBy(x => x.Fecha);
                    }

                    if (fechaHasta != "")
                    {
                        DateTime dtHst = DateTime.Parse(fechaHasta);
                        result = result.Where(x => x.Fecha <= dtHst).OrderBy(x => x.Fecha);
                    }

                    if (idFamilia != "")
                    {
                        result = result.Where(x => x.Familia == idFamilia).OrderBy(x => x.Fecha);
                    }

                    if (estado != "")
                    {
                        result = result.Where(x => x.Iestado == estado).OrderBy(x => x.Fecha);
                    }

                    if (tarjeta != "")
                    {
                        result = result.Where(x => x.Tarjeta == tarjeta).OrderBy(x => x.Fecha);
                    }

                    if (nroDoc != "")
                    {
                        result = result.Where(x => x.NroDocumento == nroDoc).OrderBy(x => x.Fecha);
                    }

                    if (socio != "")
                    {
                        result = result.Where(x => x.Socio.Contains(socio));
                    }

                    if (producto != "")
                    {
                        result = result.Where(x => x.Producto.Contains(producto));
                    }

                    dt = result.ToList().ToDataTable();
  
                    if (dt.Rows.Count > 0)
                    {
                        return generarEtiquetas(result);
                    }
                    else
                    {
                        throw new Exception("No se encuentran datos para Imprimir");
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarEtiquetas(IQueryable<SeguimientoCanjesViewModel> result)
    {
        int n = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string html = "";
            using (var dbContext = new ACHEEntities())
            {
                var list = result;
                if (list.Count() > 0)
                {
                    foreach (var item in list)
                    {
                        n++;
                        html += "<div id=etiqueta"+n + " class=seccion>";
                        html += "<table id=tablaEtiquetas>";
                        html += "<tr>";
                        html += "<td class=etiquetas><img src=../../img/grid/gridDelete.gif style=cursor:pointer id=no-print title=Eliminar class=deleteColumn onclick=ocultarEtiqueta("+n+");></td>";
                        html += "<td class=etiquetas>";
                        html += item.Socio + "<br>";
                        html += item.Producto + "<br>";
                        html += item.Tarjeta + "<br>";
                        html += "</td>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</div>";
                    }
                }
            }
            return html;
        }
        return "";
    }





}