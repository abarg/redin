﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="canjese.aspx.cs" Inherits="marcas_canjese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/canjese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/marcas/canjes.aspx") %>">Canjes</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
                                    <button id="btnImprimir" class="btn btn-default btn-sm" type="button" style="float:right" onclick="imprimir();">Imprimir</button>
            <div id="print">
                <h3 class="heading" id="litTitulo">Edición de Canje</h3>
                <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		        <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDTarjeta" Value="0" />

                    <div class="form-group">
                        <label class="col-lg-2 control-label" style="vertical-align:top">Productos</label>
                        <div class="col-lg-8" style="padding-top:5px;">
                            <asp:Literal runat="server" ID="litPremioDesc"></asp:Literal>
                            <asp:DropDownList runat="server" class="chzn_b form-control" ID="ddlProductos" 
                                 data-placeholder="Seleccione un premio"
                                style="margin-bottom:20px" onchange="getInfoPremio();">                            
                            </asp:DropDownList>  
                                              
                            <asp:Image runat="server" ID="imgFoto" ImageUrl="~/files/premios/nogift.png" style="width: 180px; height: 120px;"></asp:Image>
                            <br /><br />
                            <strong id="spnValorPremio"><asp:Literal runat="server" id="litValorPremio"></asp:Literal></strong>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Fecha de Alta</label>
                        <div class="col-lg-2">
                            <asp:TextBox runat="server" ID="txtFechaAlta" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Estado</label>
                        <div class="col-sm-4 col-md-4">
                            <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group ddlMotivos" style="display: none" >
                        <label class="col-lg-2 control-label">Motivo</label>
                        <div class="col-sm-4 col-md-4">
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMotivos">
                                <asp:ListItem Text="Carga Fraudulenta" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Faltante de stock" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Error de solicitud" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Cancela el cliente" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Otros" Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-4 col-md-4" id="txtOtros" style="display: none">
                            <asp:TextBox runat="server" ID="txtObsOtros" CssClass="form-control required"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Cantidad</label>
                        <div class="col-lg-3">
                            <asp:TextBox runat="server" ID="txtCantidad" CssClass="form-control required" Text="1"></asp:TextBox>
                        </div>
                    </div>
                    <h3 class="heading" style="margin-top: 20px;color:#428bca">Socio que realiza el canje</h3>
                    <div class="alert alert-danger alert-dismissable" id="divErrorTrSocio" style="display: none"></div>

                    <div class="row" id="divBuscar1" runat="server"> 
                        <div class="col-lg-3">
                            <label><span class="f_req">*</span> Buscar por</label>
                            <select id="ddlTrBuscarSocio" class="form-control">
                                <option value="Tarjeta">Nro Tarjeta</option>
                                <option value="Nombre">Nombre y apellido</option>
                                <option value="DNI">DNI</option>
                            </select>                                         
                        </div>
                        <div class="col-lg-4">
                            <label><span class="f_req">*</span> Valor a buscar</label>
                            <input type="text" id="txtValorSocio" class="form-control" MaxLength="20" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <label>&nbsp;</label>
                            <button id="btnBuscarSocio" class="btn" type="button" onclick="buscarSocios();">Buscar</button>
                        </div>
                    </div>
                    <div class="row" id="divBuscar2" runat="server"> 
                        <div class="col-lg-4">
                            <label><span class="f_req">*</span> Socio</label>
                            <select id="ddlTrSocio" class="form-control required" onchange="buscarTarjetas();">
                                            
                            </select>                                         
                        </div>
                        <div class="col-lg-4" id="divResultado1">
                            <label><span class="f_req">*</span> Tarjeta</label>
                            <select id="ddlTrTarjeta" class="form-control required" onchange="getPuntos(this.value);">
                                            
                            </select>                                         
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-lg-4">
                            <img src="http://www.placehold.it/180x120/EFEFEF/AAAAAA" style="max-width:180px; max-height:120px" id="imgSocio" runat="server" />
                        </div>
                        <div class="col-lg-4">
                            <strong><asp:Label runat="server" ID="litSocio" Text="" Font-Size="Medium"></asp:Label></strong>
                            <label id="lblPuntos" style="font-weight:bold;"></label>
                        </div>
                    </div>

                    <div id="divHistorial" runat="server" style="display:none">
                        <h3 class="heading" style="margin-top: 20px;color:#428bca">Historial del canje</h3>
                        <div class="row">
                            <table class="table" id="tbDetalle" style="margin-left:15px">
			                    <thead>
				                    <tr>
					                    <th>Fecha</th> 
                                        <th>Estado</th> 
                                        <th>Usuario</th>
				                    </tr>
			                    </thead>
			                    <tbody id="tBody">
                                        <asp:Repeater runat="server" ID="rptHistorial">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Fecha").ToString() %></td>
                                                    <td><%# Eval("Estado").ToString() %></td>
                                                    <td><%# Eval("Usuario").ToString() %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>   
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row" style="margin-top:15px;"> 
                        <label for="txtObservaciones" class="col-lg-2 control-label">Observaciones</label>
                        <div class="col-lg-6">
                            <asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" Height="100px" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="row"> 
                        <div class="col-sm-10 col-sm-offset-2">
                           <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                           <a href="canjes.aspx" class="btn btn-link">Cancelar</a>
                        </div>
                    </div>

                </form>
            </div>
            </div>
    </div>
</asp:Content>

