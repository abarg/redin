﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
public partial class marcas_dashboard_marketing : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalEmails()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalEmails " + idMarca, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalSMSEnviados()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.AddYears(-9).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<DatosSMSEnviados>("exec CantidadSMSEnviados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                //if (total.Any())
                //    html = Math.Abs(total[0].data).ToString("N2");
                //else
                //    html = "0";
            }
        }

        return html;
    }

   
}