﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="socios.aspx.cs" Inherits="marcas_socios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/socios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Socios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de Socios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form id="formSocio">
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    <div class="col-sm-2">
						    <label>Socio</label>
                            <input type="text" id="txtApellido" value="" maxlength="100" class="form-control"/>
					    </div>
					    <div class="col-sm-2">
						    <label>Nro. de Documento</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
					    </div>
                         <div class="col-sm-3">
						    <label>Email</label>
                            <input type="text" id="txtEmail" value="" maxlength="100" class="form-control"/>
					    </div>
                        <div class="col-sm-3">
						    <label>Nro. de Tarjeta</label>
						    <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">&nbsp:</div>
                        <%--<div class="col-sm-2">
                            <label>Nro. de Cuenta</label>
						    <input type="text" id="txtCuenta" value="" maxlength="20" class="form-control number" />
					    </div>--%>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>--%>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Socios" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>

    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha</th> 
                                <th>Hora</th> 
                                <th>Operacion</th> 
                                <!--th>SDS</!--th--> 
                                <th>Comercio</th> 
                                <th>Establecimiento</th> 
                                <th>$ Original</th> 
                                <th>$ Ahorro</th> 
                                <th>POS</th> 
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>


