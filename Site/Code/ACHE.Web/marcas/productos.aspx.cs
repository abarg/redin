﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using ClosedXML.Excel;
using ACHE.Business;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Data;

public partial class marcas_productos : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var marca = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault();
                    if (CurrentMarcasUser.Tipo != "A" || !marca.MostrarProductos)
                    {
                        Response.Redirect("home.aspx");
                    }
                    cargarCombos();
                }
            }
        }
    }
    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Productos
                    .Where(x => x.IDMarca == idMarca)
                    .OrderBy(x => x.Nombre)
                    .Select(x => new ProductosViewModel()
                    {
                        ID = x.IDProducto,
                        Nombre = x.Nombre,
                        Codigo = x.Codigo,
                        Descripcion = x.Descripcion,
                        Precio = x.Precio,
                        Familia = x.FamiliaProductos1.Nombre,
                        IDFamilia = x.IDFamilia,
                        SubFamilia = x.IDSubFamilia.HasValue ? x.FamiliaProductos.Nombre : "",
                        IDSubFamilia = x.IDSubFamilia.HasValue ? x.IDSubFamilia.Value : 0,
                        Puntos = x.Puntos,
                        StockActual = x.StockActual,
                        StockMinimo = x.StockMinimo,
                        Activo = x.Activo ? "Si" : "No",
                        PrecioXCantidad = x.Precio * x.StockActual

                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Productos.Where(x => x.IDProducto == id && x.IDMarca == idMarca).FirstOrDefault();
                    if (entity != null)
                    {
                        if (dbContext.Canjes.Any(x => x.IDProducto == entity.IDProducto))
                            throw new Exception("No se puede eliminar  ya que hay un canje involucrado con este producto");
                        else
                        {
                            dbContext.Productos.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null)
            {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }


    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubFamilias(string idFamiliaPadre)
    {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                int id = int.Parse(idFamiliaPadre);
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == id).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();
                ComboViewModel vacio = new ComboViewModel();
                vacio.ID = "";
                vacio.Nombre = "";
                listFamilias.Insert(0, vacio);
            }
        }
        return listFamilias;
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string nombre, int idFamilia, int idSubfamilia) {

        string fileName = "Productos";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            try {

                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Productos.Where(x=> x.IDMarca == idMarca)
                        .OrderByDescending(x => x.Nombre)
                        .Select(x => new ProductosViewModel() {
                            Nombre = x.Nombre,
                            Codigo = x.Codigo,
                            Precio = x.Precio,
                            Puntos = x.Puntos,
                            Descripcion = x.Descripcion,
                            NombrePadre = x.FamiliaProductos.Nombre,
                            SubFamilia = x.FamiliaProductos1.Nombre,  
                            StockActual = x.StockActual,
                            StockMinimo  = x.StockMinimo,
                            Activo  = x.Activo ? "SI" : "NO",
                            PrecioXCantidad = x.Precio * x.StockActual

                        }).AsEnumerable();

                    if (idFamilia >0)
                        info = info.Where(x => x.IDFamilia == idFamilia);

                    if (idSubfamilia >0)
                        info = info.Where(x => x.IDSubFamilia == idSubfamilia);

                    if (nombre != "")
                        info = info.Where(x => x.Nombre != null && x.Nombre.ToLower().Equals(nombre.ToLower()));


                    dt = info.ToList().Select(x => new {
                        Nombre = x.Nombre,
                        Codigo = x.Codigo,
                        Familia = x.NombrePadre,
                        SubFamilia = x.SubFamilia,
                        Descripcion = x.Descripcion,
                        Precio = x.Precio,
                        Puntos = x.Puntos,
                        StockActual = x.StockActual,
                        StockMinimo = x.StockMinimo,
                        Activo = x.Activo,
                        PrecioXCantidad = x.Precio * x.StockActual
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}