﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="mapa.aspx.cs" Inherits="marcas_mapa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCvucvVugcM6B5-rcps4y3QAAQQOAwPivI&sensor=false"></script>
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/js/markerClusterer.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/mapa.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <style type="text/css">
        html, body, #maincontainer, #contentwrapper, .main_content {
            height: 100%;
        }

        #contentwrapper, .main_content {
            min-height: 100%;
        }

        #map-canvas{
            position:absolute;

        }

        .main_content {
            padding-left: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
        }

        #map-optionbar-r {
            position: absolute;
            top: 125px;
            right: 35px;
            width: 230px;
            background-color: #fff;
            padding: 5px;
            font-size: 14px;
            line-height: 16px;
            border: 1px solid #232020;
            line-height: 16px;
        }

        .chk {
            vertical-align: top;
        }

        #mapSearch {
            right: 50px;
            height: 0px;
            position: relative;
            /* right: 0; */
            text-align: center;
            top: 20px;
            -webkit-transition: left 250ms cubic-bezier(0,0,0.2,1);
            transition: left 250ms cubic-bezier(0,0,0.2,1);
            z-index: 501;
            float: right;
        }

        .map-search-view {
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background-color: rgba(255,255,255,0.9);
            border-radius: 3px;
            box-shadow: 0 2px 5px 1px rgba(25,24,28,0.3);
            margin: 0 auto;
            padding: 0;
            -webkit-transition: width 250ms cubic-bezier(0,0,0.2,1);
            transition: width 250ms cubic-bezier(0,0,0.2,1);
            width: 300px;
        }

            .map-search-view .search-field {
                border: 1px solid transparent;
                padding: 3px;
                text-align: left;
            }
            /*.map-search-view .search-text {
                -webkit-transition: width 500ms;
                transition: width 500ms;
                background-color: transparent;
                border: 0;
                box-shadow: none;
                color: #333;
                font-size: 14px;
                margin: 0 0 0 32px;
                width: 330px;
            }*/
        .contentImg{
            float: left;
            width: 150px;
            margin-right: 10px;
            text-align: center;
        }
        .contentImg a{
            color: #369;
            font-size: 12px;
        }
        .contentTxt{
            float: left;
            width: 300px;
        }
        .clear{ clear: both; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="mapSearch">
        <div id="search" class="map-search-view">
            <div class="search-field">
                <form runat="server" id="frmBuscar">
                    Tipo de búsqueda: <input type="radio" id="rdbSocios" name="Tipo" checked="checked" onclick="toggleTipo();" />Socios &nbsp;<input type="radio" id="rdbComercios" name="Tipo" onclick="toggleTipo();" />Mis Comercios
                    <br /><br />
                    <%--Marca<asp:DropDownList runat="server" ID="ddlMarcas" class="form-control"></asp:DropDownList>
                    <br /><br />--%>
                    <div id="divComercios" style="display:none">
                          Rubro<asp:DropDownList runat="server" ID="ddlRubro"  onchange="cargarSubRubros();" ClientIDMode="Static" class="form-control"></asp:DropDownList>&nbsp;
                          Sub Rubro<asp:DropDownList runat="server" ID="ddlSubRubro"  class="form-control"></asp:DropDownList>&nbsp;
                        <%--Rubros: <br />
                        <asp:Repeater runat="server" ID="rptRubros">
                            <ItemTemplate>
                                <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox<%# (Container.ItemIndex + 1) %>" value="<%# Eval("IDRubro") %>" onclick="<%# Eval("Funcion") %>">
                                <img src="<%# Eval("Icono") %>" style="width: 24px" />
                                Rubro <%# Eval("Nombre") %>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>--%>
                    </div>
                    <div id="divSocios">
                        Sexo: <br />
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkMasculino" onclick="filtroClick(this, 'masculino');" value="masculino" />
                        <img src="/img/markers/masculino.png" style="width: 24px" />
                        Socios masculinos
                        <br>
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkFemenino" onclick="filtroClick(this, 'femenino');" value="femenino" />
                        <img src="/img/markers/femenino.png" style="width: 24px" />
                        Socios femeninos
        
                        <br />
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkIndefinido" onclick="filtroClick(this, 'indefinido');" value="indefinido" />
                        <img src="/img/markers/indefinido.png" style="width: 24px" />
                        Socios indefinidos

                        <br /><br />
                        Edad: 
                        <asp:DropDownList runat="server" ID="ddlEdad" class="form-control" Width="100" style="display:inline">
                            <asp:ListItem Text="0-18" Value="0-18"></asp:ListItem>
                            <asp:ListItem Text="19-25" Value="19-25"></asp:ListItem>
                            <asp:ListItem Text="26-30" Value="26-30"></asp:ListItem>
                            <asp:ListItem Text="31-40" Value="31-40"></asp:ListItem>
                            <asp:ListItem Text="41-50" Value="41-50"></asp:ListItem>
                            <asp:ListItem Text="51-60" Value="51-60"></asp:ListItem>
                            <asp:ListItem Text="61+" Value="61-200"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <br />
                    <span id="litResultados"></span>
                    <br /><br />
                    <a href="javascript:buscar();" class="btn btn-primary" id="btnBuscar">Buscar</a>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                </form>
            </div>
            <div class="autocomplete dropdown-menu" style="display: none;"></div>
        </div>
    </div>


    <div id="map-canvas" style="height: 100%; min-height: 100%; width: 100%; margin-top: -20px"></div>

    <div id="map-optionbar-r" style="display:none">
        Mostrar/Ocultar<br>
        <br>

        
        
        <br />
        <br />
        <b>Comercios</b>
        <br />

    </div>


    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</asp:Content>



