﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="productose.aspx.cs" Inherits="marcas_productose" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/marcas/productos.aspx") %>">Productos</a></li>
            <li>Edición</li>
        </ul>
    </div>
      <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Productos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <br />
                 <div class="form-group">
                    <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                    <label for="txtCodigo" class="col-lg-2 control-label"><span class="f_req">*</span> Código</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtCodigo" ClientIDMode="Static" CssClass="form-control required"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtNombre" ClientIDMode="Static" CssClass="form-control required"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                      <label for="cmbFamilia" class="col-lg-2 control-label"><span class="f_req">*</span>Familia</label>
                      <div  class="col-sm-4 col-md-4 col-lg-3">
                            <asp:DropDownList runat="server" ID="cmbFamilia" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                      </div>
                </div>
                <div class="form-group subFamilia">
                      <label class="col-lg-2 control-label">Sub Familia</label>
                      <div  class="col-sm-4 col-md-4 col-lg-3">
                            <asp:DropDownList runat="server" ID="cmbSubFamilia" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                      </div>
                </div>
                 <div class="form-group">
                    <label for="txtPrecio" class="col-lg-2 control-label"><span class="f_req">*</span> Precio</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtPrecio" CssClass="form-control required small" MaxLength="10000000"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtDescripcion" class="col-lg-2 control-label">Descripción</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control long" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtPrecio" class="col-lg-2 control-label"><span class="f_req">*</span> Stock Actual</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtStockActual" CssClass="form-control required number" MaxLength="10000"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtPrecio" class="col-lg-2 control-label"><span class="f_req">*</span> Stock Minimo</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtStockMinimo" CssClass="form-control required number" MaxLength="10000"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtPuntos" class="col-lg-2 control-label"><span class="f_req">*</span>Puntos</label>
                    <div  class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtPuntos" CssClass="form-control required number" MaxLength="10000000"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Imagen</label>
                    <div class="col-lg-6">
                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                            ThrobberID="throbber"
                            OnUploadedComplete="uploadLogo" />

                        <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                            <img alt="" src="../../img/ajax_loader.gif" />
                        </asp:Label> 
                    </div>
                    <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                        Actual:
                        <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                    </div>
                </div>                                              
                  <div class="form-group">
                    <label for="chkActivo" class="col-lg-2 control-label"> Activo</label>
                    <div class="col-sm-2 col-md-2">
                        <asp:CheckBox runat="server" ID="chkActivo"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();">Grabar</button>
                        <a href="Productos.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/productose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>