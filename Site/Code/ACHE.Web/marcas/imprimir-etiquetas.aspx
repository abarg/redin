﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageEtiquetas.master" AutoEventWireup="true" CodeFile="imprimir-etiquetas.aspx.cs" Inherits="marcas_imprimir_etiquetas" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/imprimir-etiquetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<form id="Form1" runat="server">   
    <div class="row" id="no-print">
        <div class="col-sm-12 col-md-12">
           <div></div> 
			<h3 class="heading">Seleccione Elementos a Imprimir </h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
			    <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="mostrarTodo();">Deshacer Cambios</button>
                            <button class="btn btn-success" type="button" id="Button1" onclick="javascript:window.print()">Imprimir</button>
                        </div>
                    </div>
                </div>
		</div>
    </div>
    <div id="bodyetiquetas" runat="server">
    </div>
 </form>
</asp:Content>


