﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="Comercios.aspx.cs" Inherits="marcas_Comercios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Terminales</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formComercio" runat="server">
            <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
                <div class="formSep col-sm-12 col-md-12">
                   <div class="row">
                        <div class="col-sm-3">
                            <label>Nombre Fantasía</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-sm-3">
                            <label>Razón social</label>
                            <input type="text" id="txtRazonSocial" value="" maxlength="128" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. de Documento</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">
                            <label>SDS</label>
                            <input type="text" id="txtSDS" value="" maxlength="50" class="form-control number" />
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="row"> 
                        <div class="col-sm-2">
                            <label>Nro. de establecimiento</label>
                            <input type="text" id="txtEstablecimiento" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label>POS Terminal</label>
                            <input type="text" id="txtPosTerminal" value="" maxlength="50" class="form-control" />
                        </div>
                        
                        <div class="col-sm-2" style="display:none">
                            <label>Descuento</label>
                            <input type="text" id="txtDescuento" value="" maxlength="3" class="form-control number" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
<%--                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>--%>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/comercios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>