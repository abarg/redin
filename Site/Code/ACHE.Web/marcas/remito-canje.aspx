﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="remito-canje.aspx.cs" Inherits="marcas_remito_canje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="canjes.aspx">Canjes</a></li>
            <li>Remito</li>
        </ul>
    </div>
     <div class="row">
        <div class="col-sm-8 col-md-8">
        <button id="btnImprimir" class="btn btn-default btn-sm" type="button" style="float:right" onclick="imprimir();">Imprimir</button>
              <div id="divImpresion">
            <h3 class="heading" id="litTitulo">Remito  <asp:Literal id="litNroCanje" runat="server"></asp:Literal> </h3>
               
		    <form runat="server" id="formEdicion" role="form" style="margin-left:30px">
                <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Fecha:</b></label>
                  
                        <asp:Literal ID="litFecha" runat="server" ></asp:Literal>
                   
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>N° de socio:</b></label>
                  
                        <asp:Literal ID="litNroSocio" runat="server" ></asp:Literal>
                   
                </div>
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>DNI:</b></label>
                  
                        <asp:Literal ID="litDNI" runat="server" ></asp:Literal>
                    
                </div>
               
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Socio:</b></label>
                  
                        <asp:Literal ID="litNombreSocio" runat="server" ></asp:Literal>
                    
                </div>
               
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Premio entregado:</b></label>
                  
                        <asp:Literal ID="litPremio" runat="server" ></asp:Literal>
                    
                </div><br/> <br/>
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Entregado por: </b></label>
                    <br/><p>_____________________________</p>
                </div><br/><br/>
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Firma: </b></label>
                     <br/><p>_____________________________</p>
                </div><br/><br/>
                 <div class="form-group">
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label"><b>Aclaracion:</b></label>
                  <br/><p>_____________________________</p>
                </div><br/><br/>
               
            </form>
                   
                  </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
       <script type="text/javascript" >

        
           jQuery(document).ready(function () {
               var doc = new jsPDF('p', 'pt', 'a4', true);
               //  var pdf = new jsPDF('p', 'pt', 'a4');

               var specialElementHandlers = {
                   '#editor': function (element, renderer) {
                       return true;
                   }
               };

               $('#cmd').click(function () {
                   doc.addHTML($('.content')[0], 10, 10, {
                       'width': 1000,
                       //   'margin': 1,
                       //'pagesplit': true,
                       //  'width': 100,
                       //'quality': 0.9, // compression works now!
                       'elementHandlers': specialElementHandlers,
                       'background': '#fff',
                   }, function () {
                       doc.save('liquidacion.pdf');
                   });
                   // doc.save('sample-file.pdf');
               });
           });
           function imprimir() {
               //window.print();
               //  $("#divImpresion").printElement();
               w = window.open();
               w.document.write($('#divImpresion').html());
               w.print();
               w.close();
           }
    </script>
</asp:Content>

