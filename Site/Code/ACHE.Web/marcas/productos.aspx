﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="productos.aspx.cs" Inherits="marcas_productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/productos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Productos</li>
        </ul>
     </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Configuración de Productos</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                              <label >Familia</label>
                               <asp:DropDownList runat="server" ID="cmbFamilia" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                              <label >Sub Familia</label>
                               <asp:DropDownList runat="server" ID="cmbSubFamilia" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-md-6">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Tickets" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

