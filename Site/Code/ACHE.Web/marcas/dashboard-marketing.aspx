﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="dashboard-marketing.aspx.cs" Inherits="marcas_dashboard_marketing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
                height: 56px !important;
                opacity: 1 !important;
                top: 8px;
                right: 10px;
                margin-right: -5px;
                width: 0px !important;
                background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }
        
        .modal.modal-wide .modal-dialog {
          width: 90%;
        }
        .modal-wide .modal-body {
          overflow-y: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Marketing</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
       
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6">
            <h3 class="heading">Campaña SMS</h3>
             <label>
                    <span class="info-box-text">Mensaje bienvenida</span>
             </label>
             <label class="switch">
                  <input type="checkbox" value="1" id="checkboxSMSBienvenida">
                  <span class="slider round"></span>
             </label>
        
               

            <label>
                <span class="info-box-text">Mensaje cumpleaños</span>
            </label>
            <label class="switch">
                  <input type="checkbox" value="1" id="checkboxSMSCumpleaños">
                  <span class="slider round"></span>
             </label>
                
            <div>
                
						Total Mensajes SMS enviados: <span id="totalSMSEnviado"></span>
            </div>
            <div>
                 
						Total Mensajes SMS no enviados: <span id="totalSMSNoEnviado"></span>
            </div>
        </div>
         <div class="col-sm-6 col-md-6">
            <h3 class="heading">Campaña Email</h3>
             <label>
                <span class="info-box-text">Email bienvenida</span>
            </label>
             <label class="switch">
                  <input type="checkbox" value="1" id="checkboxEmailBienvenida">
                  <span class="slider round"></span>
             </label>


            <label>
                 <span class="info-box-text">Email cumpleaños</span>
            </label>
             <label class="switch">
                  <input type="checkbox" value="1" id="checkboxEmailCumpleaños" data-toggle="toggle">
                  <span class="slider round"></span>
             </label>
             
             <div>
                
						Total Emails enviados: <strong><span id="resultado_total_mails"></span></strong>
            </div>
            <div>
                 <strong id="resultado_email_abierto"></strong>
						Total Emails abiertos: <span id="totalEmailAbierto"></span>
            </div>
             <div>
                 <strong id="resultado_email_rechazado"></strong>
						Total Emails rechazados: <span id="totalEmailRechazado"></span>
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <h3 class="heading mt3">Sorteos</h3>
               <label class="switch">
                  <input type="checkbox" value="1" id="checkboxSorteo">
                  <span class="slider round"></span>
             </label>
            <div>
                <strong id="resultado_participante"></strong>
						Total de participantes: <span id="totalParticipantes"></span>
            </div>
            <div>
                 <strong id="resultado_genero_sorteo"></strong>
						Total de participantes por género: <span id="totalGeneroParticipantes"></span>
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <h3 class="heading mt3">Promociones</h3>
              <label class="switch">
                  <input type="checkbox" value="1" id="checkboxPromociones">
                  <span class="slider round"></span>
             </label>              
        </div>
  </div>
        <div class="row">
         <div class="col-sm-6 col-md-6">
            <h3 class="heading mt3">Cupón check</h3>
              <label class="switch">
                  <input type="checkbox" value="1" id="checkboxCupon">
                  <span class="slider round"></span>
             </label>
            <div>
                <strong id="resultado_cupon_emitido"></strong>
						Total de cupones emitidos: <span id="totalCuponEmitido"></span>
            </div>
              <div>
                <strong id="resultado_cupon_redimido"></strong>
						Total de cupones redimido: <span id="totalCuponRedimido"></span>
            </div>
              <div>
                <strong id="resultado_cupon_pendiente"></strong>
						Total de cupones pendientes de redención: <span id="totalCuponPendiente"></span>
            </div>
           
        </div>
        <div class="col-sm-6 col-md-6">
            <h3 class="heading mt3">Encuestas</h3>
             
            <div>
                <strong id="resultado_encuesta_participantes"></strong>
						Total de participantes: <span id="totalEncuestaParticipantes"></span>
            </div>
              <div>
                <strong id="resultado_encuesta_positivo"></strong>
						Total de votos positivos: <span id="totalEncuestaPositivo"></span>
            </div>
              <div>
                <strong id="resultado_encuesta_negativo"></strong>
						Total de votos negativos: <span id="totalEncuestaNegativo"></span>
            </div>
           
        </div>
            </div>
  

      
    	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/dashboard-marketing.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

