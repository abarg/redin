﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;
using System.Collections.Specialized;

public partial class marcas_smse : PaginaMarcasBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");

            if (!string.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }

            if (!string.IsNullOrEmpty(Request.QueryString["Tipo"])) {
                hdnTipo.Value = Request.QueryString["Tipo"];
                if (hdnTipo.Value == "I") {
                    CargarProvincias();
                    CargarProfesiones();
                    CargarCombos();
                }
                else {
                    divCiudades.Visible = false;
                    divProfesiones.Visible = false;
                    divProvincias.Visible = false;
                    divTransacciones.Visible = false;
                }
            }
            else {
                if (string.IsNullOrEmpty(hdnID.Value) || hdnID.Value == "0")
                    Response.Redirect("sms.aspx");
            }

            using (var dbContext = new ACHEEntities()) {
                hdnPrecio.Value = litCostoFijo.Text = dbContext.Marcas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault().CostoSMS.ToString();
            }

            rdbTodos.InputAttributes["class"] = "radio todos";
            rdbFemenino.InputAttributes["class"] = "radio femenino";
            rdbMasculino.InputAttributes["class"] = "radio masculino";

            chk0.InputAttributes.Add("value", "0");
            chk1.InputAttributes.Add("value", "1");
            chk2.InputAttributes.Add("value", "2");
            chk3.InputAttributes.Add("value", "3");
            chk4.InputAttributes.Add("value", "4");
            chk5.InputAttributes.Add("value", "5");
            chk6.InputAttributes.Add("value", "6");
            chk7.InputAttributes.Add("value", "7");
        }
    }

    [WebMethod]
    public static List<string> Calcular(string edad, string sexo, string precioSMS, string idProvincia, string idCiudad, string tipoTrans, string fechaDesdeTrans, string idProfesion, string localidad, string sede, string actividad) {
        try {
            List<string> result = new List<string>();
            ArrayList sociosList = new ArrayList();
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;
                using (var dbContext = new ACHEEntities()) {
                    #region calculo de estimados
                    string sql = @"select SociosView.Nombre, SociosView.Apellido, SociosView.Celular, SociosView.EmpresaCelular, d.Provincia 
                                    from SociosView 
                                    join Domicilios d on d.IDDomicilio = SociosView.IDDomicilio
                                    join Ciudades c on d.Ciudad = c.IDCiudad 
                                    join Provincias p on d.Provincia = p.IDProvincia
                                    join ActividadSocioNivel as ASN on ASN.IDSocio = SociosView.IDSocio
								    join ActividadComercio as AC on ASN.IDActividad = AC.IDActividad
								    join comercios as CO on AC.IDComercio = CO.idcomercio
                                    where SociosView.Celular is not null 
                                    and SociosView.IDMarca = " + idMarca + " ";

                    #region Transacciones
                    if (tipoTrans != "") {
                        if (fechaDesdeTrans != "") {
                            string estadoTrans = "";

                            if (tipoTrans == "sin")
                                estadoTrans = " not in ";
                            else
                                estadoTrans = " in ";
                            sql += " and SociosView.idsocio " + estadoTrans + " (select distinct(tar.IDSocio) from Transacciones tr,Tarjetas tar where tr.NumTarjetaCliente = tar.Numero and  tr.FechaTransaccion >= '" + fechaDesdeTrans + "' )";
                        }
                        else
                            throw new Exception("Debe ingresar una fecha");

                    }

                    #endregion

                    #region Ubicacion
                    if (idProvincia != "") {
                        sql += " and ";
                        sql += idProvincia + " = p.IDProvincia ";

                        if (idCiudad != "") {
                            sql += " and ";
                            sql += idCiudad + " = c.IDCiudad ";
                        }
                    }
                    #endregion

                    #region edad
                    if (edad != "Todas") {
                        sql += " and (";

                        string[] edades = edad.Split(";");
                        string[] min = new string[6];
                        string[] max = new string[6];
                        for (int i = 0; i < edades.Count(); i++) {
                            if (!string.IsNullOrEmpty((edades[i]))) {
                                min[i] = edades[i].Split("-")[0];
                                max[i] = edades[i].Split("-")[1];
                            }
                        }

                        int hasta = min.Count();
                        int hasta2 = hasta - 1;
                        for (int j = 0; j < hasta; j++) {
                            if (!string.IsNullOrEmpty(min[j]) && !string.IsNullOrEmpty(max[j])) {
                                if (j == 0)
                                    sql += "(Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                                else
                                    sql += " or (Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                            }
                        }

                        sql += " )";
                    }
                    //else
                    //{
                    //    sql += "(Edad >= 0 and Edad <= 200)";
                    //}
                    #endregion

                    #region sexo
                    if (sexo != "T") {
                        sql += " and Sexo = ";
                        if (sexo == "F")
                            sql += "'F'";
                        else
                            sql += "'M'";
                    }
                    #endregion

                    #region profesion
                    if (idProfesion != "") {
                        sql += " and IDSocio in (select s.IDSocio from Socios s left join Profesiones prof on s.IDProfesion = prof.IDProfesion where prof.IDProfesion = " + idProfesion + " )";
                    }
                    #endregion

                    if (localidad != "")
                        sql += "and c.IDciudad = " + localidad + " ";
                    if (sede != "")
                        sql += "and CO.IDComercio =" + sede + " ";
                    if (actividad != "")
                        sql += "and ASN.IDActividad = " + actividad;

                    var socios = dbContext.Database.SqlQuery<SociosViewModel>(sql, new object[] { }).ToList();

                    var costoTotal = (socios.Count * decimal.Parse(precioSMS)).ToString("N2");
                    result.Add(costoTotal);
                    result.Add(socios.Count.ToString());
                    sociosList.Add(socios);
                    #endregion
                }
            }
            return result;
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(true)]
    public static List<DetalleSocios> obtenerDetalle(string tipo, string localidad, string sede, string actividad)
    {
        var html = string.Empty;
        var camposPendientes = string.Empty;
        List<DetalleSocios> list = new List<DetalleSocios>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                string sql = @"select top 4000 SV.Nombre, SV.Apellido
                                from Socios as SV
                                join ActividadSocioNivel as ASN on ASN.IDSocio = SV.IDSocio
								join ActividadComercio as AC on ASN.IDActividad = AC.IDActividad
					            join domicilios as d on d.IDDomicilio = SV.IDDomicilio
					            join ciudades as c on c.IDCiudad = d.Ciudad
								join comercios as CO on AC.IDComercio = CO.idcomercio
                                where SV.Celular is not null ";

                if (localidad != "")
                    sql += "and c.IDciudad = " + localidad + " ";
                if (sede != "")
                    sql += "and CO.IDComercio =" + sede + " ";
                if (actividad != "")
                    sql += "and ASN.IDActividad = " + actividad;


                try
                {
                    list = dbContext.Database.SqlQuery<DetalleSocios>(sql, new object[] { }).ToList();
                }

                catch (Exception ex)
                {
                    throw ex;
                }


            }

        }


        return list;
    }

    [WebMethod(true)]
    public static int guardar(int id, string tipo, string nombre, string sexo, string edad, string mensaje, string fechaEnvio, string telefonos, int idProvincia, int idCiudad, string tipoTrans, string fechaDesdeTrans, int idProfesion) {
        int idGenerado = 0;
        try {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;
                using (var dbContext = new ACHEEntities()) {
                    SMS entity;
                    if (id > 0)
                        entity = dbContext.SMS.Where(x => x.IDSMS == id && x.IDMarca == idMarca).FirstOrDefault();
                    else {
                        entity = new SMS();
                        entity.IDMarca = idMarca;
                        entity.UsuarioAlta = usu.Usuario;
                        entity.FechaAlta = DateTime.Now;
                        entity.Estado = "B";
                        entity.Tipo = tipo;
                    }

                    entity.Nombre = nombre;
                    entity.Sexo = sexo;
                    entity.Edad = edad;
                    entity.Mensaje = mensaje;
                    entity.FechaEnvioSolicitada = DateTime.Parse(fechaEnvio);
                    entity.Telefonos = telefonos;

                    if (idProvincia == 0)
                        entity.IDProvincia = null;
                    else
                        entity.IDProvincia = idProvincia;

                    if (idCiudad == 0)
                        entity.IDCiudad = null;
                    else
                        entity.IDCiudad = idCiudad;

                    if (idProfesion == 0)
                        entity.IDProfesion = null;
                    else
                        entity.IDProfesion = idProfesion;

                    if (tipoTrans != "") {
                        entity.ConTransacciones = tipoTrans == "con" ? true : false;
                        if (fechaDesdeTrans != "")
                            entity.FechaDesdeTransacciones = Convert.ToDateTime(fechaDesdeTrans);
                    }
                    else {
                        entity.FechaDesdeTransacciones = null;
                        entity.ConTransacciones = null;
                    }

                    if (entity.FechaEnvioSolicitada.Year <= DateTime.Now.Year
                        && entity.FechaEnvioSolicitada.Month <= DateTime.Now.Month
                        && entity.FechaEnvioSolicitada.Day <= DateTime.Now.Day)
                    {
                        throw new Exception("La fecha de envio de la campaña no puede ser hoy");
                    }

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.SMS.Add(entity);
                        dbContext.SaveChanges();
                    }

                    idGenerado = entity.IDSMS;
                }
            }

            return idGenerado;
        }
        catch (Exception ex) {
            var msg = ex.Message;
            throw ex;
        }
    }

    [WebMethod(true)]
    public static void activar(int id) {
        try {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                    var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities()) {
                    SMS entity = dbContext.SMS.Where(x => x.IDSMS == id && x.IDMarca == idMarca).FirstOrDefault();

                    entity.Estado = "P";
                    entity.FechaUltEstado = DateTime.Now;

                    dbContext.SaveChanges();

                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<NOMBRE>", entity.Nombre);
                    replacements.Add("<MARCA>", usu.Marca);
                    replacements.Add("<MENSAJE>", entity.Mensaje);

                    bool send = EmailHelper.SendMessage(EmailTemplate.NuevaCampaniaSMS, replacements, ConfigurationManager.AppSettings["Email.SMSActivacion"], "RedIN: Solicitud de campaña SMS");
                }
            }
        }
        catch (Exception ex) {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.SMS.Where(x => x.IDSMS == id && x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault();

            CargarProfesiones();
            CargarProvincias();

            CargarCombos();

            if (entity != null) {
                hdnTipo.Value = entity.Tipo;
                txtNombre.Text = entity.Nombre;
                #region sexo
                rdbTodos.Checked = false;
                rdbFemenino.Checked = false;
                rdbMasculino.Checked = false;

                switch (entity.Sexo) {
                    case "T":
                        rdbTodos.Checked = true;
                        break;
                    case "F":
                        rdbFemenino.Checked = true;
                        break;
                    case "M":
                        rdbMasculino.Checked = true;
                        break;
                }
                #endregion

                #region edad

                if (entity.Edad == "Todas") {
                    chk0.Checked = true;
                    chkSegmentado.Checked = false;
                }
                else {
                    chk0.Checked = false;
                    chkSegmentado.Checked = true;

                    if (entity.Edad.Contains("0-18"))
                        chk1.Checked = true;
                    if (entity.Edad.Contains("19-25"))
                        chk2.Checked = true;
                    if (entity.Edad.Contains("26-30"))
                        chk3.Checked = true;
                    if (entity.Edad.Contains("31-40"))
                        chk4.Checked = true;
                    if (entity.Edad.Contains("41-50"))
                        chk5.Checked = true;
                    if (entity.Edad.Contains("51-60"))
                        chk6.Checked = true;
                    if (entity.Edad.Contains("61+"))
                        chk7.Checked = true;
                }

                #endregion

                txtMensaje.Text = entity.Mensaje;
                txtNombre.Text = entity.Nombre;
                txtFechaEnvio.Text = entity.FechaEnvioSolicitada.ToString("dd/MM/yyyy");
                txtTelefonos.Text = entity.Telefonos;

                if (entity.ConTransacciones != null)
                    ddlTransacciones.SelectedValue = entity.ConTransacciones.Value ? "con" : "sin";

                if (entity.FechaDesdeTransacciones != null) {
                    txtFechaDesde.Text = entity.FechaDesdeTransacciones.Value.ToShortDateString();
                    divFechaTrans.Visible = true;
                }
                else {
                    entity.FechaDesdeTransacciones = null;
                    entity.ConTransacciones = null;
                }

                if (entity.IDProvincia != null) {
                    ddlProvincias.SelectedValue = entity.IDProvincia.ToString();
                    List<ComboViewModel> listaCiudades = new List<ComboViewModel>();
                    CargarCiudades(entity.IDProvincia.Value);
                }

                if (entity.IDCiudad != null)
                    ddlCiudades.SelectedValue = entity.IDCiudad.ToString();

                if (entity.IDProfesion != null)
                    ddlProfesiones.SelectedValue = entity.IDProfesion.ToString();
            }
        }
    }


    private void CargarCombos()
    {
        try
        {

            using (var dbContext = new ACHEEntities())
            {

                var actividades = dbContext.Actividades.OrderBy(x => x.Actividad).Select(x => new { IDActividad = x.IDActividad, Actividad = x.Actividad.ToUpper() }).ToList();
                if (actividades != null)
                {
                    this.ddlActividades.DataSource = actividades;
                    this.ddlActividades.DataTextField = "Actividad";
                    this.ddlActividades.DataValueField = "IDActividad";
                    this.ddlActividades.DataBind();
                    this.ddlActividades.Items.Insert(0, new ListItem("", ""));
                }


                string sqlLocalidades = @"select distinct C.Nombre, C.IDCiudad as ID from ActividadComercio as AC
                                            join Comercios as CO on AC.IDComercio = CO.IDComercio
                                            join Domicilios as D on D.IDDomicilio = CO.IDDomicilio
                                            join Ciudades as C on C.IDCiudad = D.Ciudad";


                var localidadesData = dbContext.Database.SqlQuery<Combo>(sqlLocalidades).ToList();
                if (localidadesData != null)
                {
                    this.localidades.DataSource = localidadesData;
                    this.localidades.DataTextField = "Nombre";
                    this.localidades.DataValueField = "ID";
                    this.localidades.DataBind();
                    this.localidades.Items.Insert(0, new ListItem("", ""));
                }



                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio as ID from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio";



                var sedesData = dbContext.Database.SqlQuery<Combo>(sqlSedes).ToList();
                if (sedesData != null)
                {
                    this.ddlSedes.DataSource = sedesData;
                    this.ddlSedes.DataTextField = "Nombre";
                    this.ddlSedes.DataValueField = "ID";
                    this.ddlSedes.DataBind();
                    this.ddlSedes.Items.Insert(0, new ListItem("", ""));
                }

            }




        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public class Combo
    {
        public string Nombre { get; set; }
        public int ID { get; set; }
    }

    private void CargarProvincias() {
        using (var dbContext = new ACHEEntities()) {
            //Provincias
            var prov = dbContext.Provincias.ToList();
            this.ddlProvincias.DataSource = prov;
            this.ddlProvincias.DataValueField = "IDProvincia";
            this.ddlProvincias.DataTextField = "Nombre";
            this.ddlProvincias.DataBind();
            this.ddlProvincias.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void CargarProfesiones() {
        using (var dbContext = new ACHEEntities()) {
            //Profesiones
            var profesion = dbContext.Profesiones.ToList();
            this.ddlProfesiones.DataSource = profesion;
            this.ddlProfesiones.DataValueField = "IDProfesion";
            this.ddlProfesiones.DataTextField = "Nombre";
            this.ddlProfesiones.DataBind();
            this.ddlProfesiones.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void CargarCiudades(int idProvincia) {
        using (var dbContext = new ACHEEntities()) {
            //Ciudades
            var ciudad = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).ToList();
            this.ddlCiudades.DataSource = ciudad;
            this.ddlCiudades.DataValueField = "IDCiudad";
            this.ddlCiudades.DataTextField = "Nombre";
            this.ddlCiudades.DataBind();
            this.ddlCiudades.Items.Insert(0, new ListItem("", ""));
        }
    }


    [WebMethod(true)]
    public static List<ComboViewModel> ciudadesByProvincias(int idProvincia) {
        List<ComboViewModel> listaCiudades = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {
            listaCiudades = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).Select(x => new ComboViewModel { ID = x.IDCiudad.ToString(), Nombre = x.Nombre }).OrderBy(x=> x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listaCiudades.Insert(0, todas);
        }
        return listaCiudades;
    }


    [WebMethod(true)]
    public static List<Combo> sedesPorLocalidades(string localidad)
    {

        List<Combo> listSedes = new List<Combo>();

        if (localidad != "")
        {

            using (var dbContext = new ACHEEntities())
            {

                var sql = @"select NombreFantasia as Nombre, C.IDComercio as ID from comercios AS C
                            join Domicilios AS D on C.IDDomicilio = D.IDDomicilio
                            join Ciudades AS CI ON CI.IDCiudad = D.Ciudad
                            where IDFranquicia = 43 and CI.IDCiudad = " + localidad  + ";";




                listSedes = dbContext.Database.SqlQuery<Combo>(sql).ToList();


            }

        }
        else
        {
            using(var dbContext = new ACHEEntities())
            {

                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio as ID from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio";




                listSedes = dbContext.Database.SqlQuery<Combo>(sqlSedes).ToList();
            }
   
        }

        return listSedes;
    }


    [WebMethod(true)]
    public static List<Combo> actividadesPorSedes(string sede)
    {

        List<Combo> listActividades = new List<Combo>();

        if (sede != string.Empty)
        {

            using (var dbContext = new ACHEEntities())
            {



                var sql = @"select distinct A.Actividad as Nombre, A.IDActividad as ID from ActividadComercio AS AC
                            join Comercios as C on C.IDComercio = AC.IDComercio
							join Actividades as A on A.IDActividad = AC.IDActividad
							where C.IDComercio = " + sede + ";";

               



                listActividades = dbContext.Database.SqlQuery<Combo>(sql).ToList();


            }

        }
        else
        {

            using (var dbContext = new ACHEEntities())
                listActividades = dbContext.Actividades.OrderBy(x => x.Actividad).Select(x => new Combo { ID = x.IDActividad, Nombre = x.Actividad.ToUpper() }).ToList();

        }

        return listActividades;
    }


    [WebMethod(true)]
    public static List<Combo> horariosPorActividades(string actividad)
    {

        List<Combo> listHorarios = new List<Combo>();

        if (actividad != string.Empty)
        {

            using (var dbContext = new ACHEEntities())
            {



                var sql = @"SELECT CONCAT(AH.Dia, ' - ',  AH.Horario ) as Nombre, AH.IDHorario as ID from ActividadHorarios as AH
                            where AH.IDActividad = " + actividad +  ";";


                listHorarios = dbContext.Database.SqlQuery<Combo>(sql).ToList();


            }

        }
        else
        {


        }

        return listHorarios;
    }


    public class DetalleSocios
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Celular { get; set; }
        public string EmpresaCelular { get; set; }
        public string Pendientes { get; set; }
    }
}