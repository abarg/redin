﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Text;
using FileHelpers;

public partial class marcas_tarjetas : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {

        WebMarcasUser userMarca = new PaginaMarcasBase().CurrentMarcasUser;
        if (userMarca != null){
            int idMarca = userMarca.IDMarca;
            using (var dbContext = new ACHEEntities()) {
                var result = dbContext.Tarjetas.Include("Franquicias").Include("Marcas")
                    .Where(x => x.IDMarca == idMarca)
                     .OrderBy(x => x.IDSocio)
                     .Select(x => new TarjetasViewModel() {
                         IDTarjeta = x.IDTarjeta,
                         Socio = x.Socios.Apellido + ", " + x.Socios.Nombre,
                         Marca = x.Marcas.Nombre,
                         IDSocio = x.IDSocio.HasValue ? (int)x.IDSocio : 0,
                         Numero = x.Numero,
                         FechaAsignacion = x.FechaAsignacion,
                         FechaVencimiento = x.FechaVencimiento,
                         Estado = x.Estado == "A" ? "Activa" : "Baja",
                         MotivoBaja = x.MotivoBaja,
                         FechaBaja = x.FechaBaja,
                         Puntos = x.PuntosTotales,
                         Credito = x.Credito,
                         Giftcard = x.Giftcard,
                         POS = Math.Round(x.Credito + x.Giftcard),
                         Total = x.Credito + x.Giftcard
                     }).ToDataSourceResult(take, skip, sort, filter);
                return result;
            }
        }
        else
            return null;
    }


     [System.Web.Services.WebMethod]
    public static void guardarFechaVenc(int id, string fechaVencimiento, string numero) {
        using (var dbContext = new ACHEEntities()) {
            Tarjetas tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == id).FirstOrDefault();
            //tarj.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            tarj.FechaVencimiento = DateTime.Parse(fechaVencimiento);
            dbContext.SaveChanges();
        }
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string numero)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                int idMarca= new PaginaMarcasBase().CurrentMarcasUser.IDMarca;
                var tarjetas = dbContext.Tarjetas.Include("Marcas").Include("Franquicias")
                    .OrderBy(x => x.IDTarjeta)
                    .Select(x => new
                    {
                        IDTarjeta = x.IDTarjeta,
                        Socio = x.Socios.Apellido + ", " + x.Socios.Nombre,
                        IDMarca = x.IDMarca,
                        IDFranquicia = x.IDFranquicia ?? 0,
                        Marca = x.Marcas.Nombre,
                        Franquicia = x.IDFranquicia.HasValue ? x.Franquicias.NombreFantasia : "",
                        Numero = x.Numero,
                        Estado = x.Estado == "A" ? "Activa" : "Baja",
                        FechaAlta = x.FechaAlta,
                        FechaEmision = x.FechaEmision,
                        FechaVencimiento = x.FechaVencimiento,
                        FechaBaja = x.FechaBaja,
                        Puntos = x.PuntosTotales,
                        Credito = x.Credito,
                        Giftcard = x.Giftcard,
                        POS = x.Credito + x.Giftcard,
                        Total = x.Credito + x.Giftcard
                    }).AsEnumerable();

                if (!string.IsNullOrEmpty(numero))
                    tarjetas = tarjetas.Where(x => x.Numero.Contains(numero));
                if (idMarca > 0)
                    tarjetas = tarjetas.Where(x => x.IDMarca == idMarca);
                //if (idFranquicia > 0)
                //    tarjetas = tarjetas.Where(x => x.IDFranquicia == idFranquicia);

                #region header y footer
                string header = "IDTarjeta;Socio;Franquicia;Marca;Numero;Estado;FechaAlta;FechaEmision;FechaVencimiento;FechaBaja;Puntos;Credito;Giftcard;POS;Total;";
                //string fechaHoy = DateTime.Now.ToString("yyyyMMdd");
                #endregion


                #region detalle
                List<TarjetaFH> listaTarjetas = new List<TarjetaFH>();

                foreach (var tarj in tarjetas)
                {
                    TarjetaFH tarjeta = new TarjetaFH();
                    tarjeta.IDTarjeta = tarj.IDTarjeta;
                    tarjeta.Socio = tarj.Socio;
                    tarjeta.Estado = tarj.Estado;
                    tarjeta.Franquicia = tarj.Franquicia;
                    tarjeta.Marca = tarj.Marca;
                    tarjeta.Numero = tarj.Numero;
                    tarjeta.FechaAlta = tarj.FechaAlta.ToString("dd/MM/yyyy");
                    tarjeta.FechaEmision = tarj.FechaEmision.HasValue ? tarj.FechaEmision.Value.ToString("dd/MM/yyyy") : "";
                    tarjeta.FechaVencimiento = tarj.FechaVencimiento.ToString("dd/MM/yyyy") ;
                    tarjeta.FechaBaja = tarj.FechaBaja.HasValue ? tarj.FechaBaja.Value.ToString("dd/MM/yyyy") : "";
                    tarjeta.Puntos = tarj.Puntos;
                    tarjeta.Credito = tarj.Credito;
                    tarjeta.Giftcard = tarj.Giftcard;
                    tarjeta.POS = tarj.POS;
                    tarjeta.Total = tarj.Total;
                    listaTarjetas.Add(tarjeta);
                }
                #endregion

                string path = HttpContext.Current.Server.MapPath("/files/tarjetas/tarjetas.csv");
                var engine = new DelimitedFileEngine(typeof(TarjetaFH))
                {
                    HeaderText = header
                };
                engine.Options.Delimiter = ";";
                engine.WriteFile(path, listaTarjetas);
                return path;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}