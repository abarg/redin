﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

public partial class marcas_premiose : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");

            Session["CurrentFoto"] = null;
            this.txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");

            if (CurrentMarcasUser.Catalogo)
                txtCodigo.MaxLength = 7;
            else
                txtCodigo.MaxLength = 3;

            CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }
        }
    }

    private void CargarCombos()
    {
        bRubros bRubros = new bRubros();
        this.ddlRubro.DataSource = bRubros.getRubros();
        this.ddlRubro.DataValueField = "IDRubro";
        this.ddlRubro.DataTextField = "Nombre";
        this.ddlRubro.DataBind();
        this.ddlRubro.Items.Insert(0, new ListItem("", "0"));
    }

    [WebMethod(true)]
    public static void guardar(int id, string codigo, string descripcion, string fechaDesde, string fechaHasta,
        int valorPuntos, int valorPesos, int stock, int stockMinimo, int idRubro)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Premios.FirstOrDefault(s => s.Codigo == codigo && s.IDMarca == idMarca);
                    if (aux != null && aux.IDPremio != id)
                        throw new Exception("Ya existe un premio con el código ingresado");

                    Premios entity;
                    if (id > 0)
                        entity = dbContext.Premios.Where(x => x.IDPremio == id && x.IDMarca == idMarca).FirstOrDefault();
                    else
                    {
                        entity = new Premios();
                        entity.TipoMov = "A";
                        entity.IDMarca = idMarca;
                    }

                    entity.FechaVigenciaDesde = DateTime.Parse(fechaDesde);
                    entity.FechaVigenciaHasta = DateTime.Parse(fechaHasta);
                    entity.Codigo = codigo;
                    entity.Descripcion = descripcion;
                    if (idRubro > 0)
                        entity.IDRubro = idRubro;
                    else
                        entity.IDRubro = null;
                    entity.ValorPesos = valorPesos;
                    entity.ValorPuntos = valorPuntos;
                    entity.StockActual = stock;
                    entity.StockMinimo = stockMinimo;
                    if (HttpContext.Current.Session["CurrentFoto"] != null && HttpContext.Current.Session["CurrentFoto"] != "")
                        entity.Foto = HttpContext.Current.Session["CurrentFoto"].ToString();

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Premios.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Premios.Where(x => x.IDPremio == id && x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault();
            if (entity != null)
            {
                if (entity.FechaVigenciaDesde.HasValue)
                    txtFechaDesde.Text = entity.FechaVigenciaDesde.Value.ToShortDateString();
                if (entity.FechaVigenciaHasta.HasValue)
                    txtFechaHasta.Text = entity.FechaVigenciaHasta.Value.ToShortDateString();
                txtCodigo.Text = entity.Codigo;
                txtDescripcion.Text = entity.Descripcion;
                txtValorPuntos.Text = entity.ValorPuntos.ToString();
                txtValorPesos.Text = entity.ValorPesos.ToString();
                txtStock.Text = entity.StockActual.ToString();
                txtStockMinimo.Text = entity.StockMinimo.ToString();
                if (entity.IDRubro.HasValue)
                    ddlRubro.SelectedValue = entity.IDRubro.Value.ToString();

                if (!string.IsNullOrEmpty(entity.Foto))
                {
                    lnkFoto.NavigateUrl = "/fileHandler.ashx?type=premios&module=marcas&file=" + entity.Foto;
                    lnkFotoDelete.NavigateUrl = "javascript: void(0)";
                    lnkFotoDelete.Attributes.Add("onclick", "return deleteUpload('" + entity.Foto + "', 'Foto')");
                    divFoto.Visible = true;

                    imgFoto.ImageUrl = "/files/premios/" + entity.Foto;
                }
                else
                    imgFoto.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
            }
        }
    }

    [WebMethod(true)]
    public static void eliminarFoto(int id, string archivo)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var file = "//files//premios//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Premios.Where(x => x.IDPremio == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Foto = null;
                            HttpContext.Current.Session["CurrentFoto"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadFoto(object sender, EventArgs e)
    {
        try
        {
            var extension = flpFoto.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpFoto.FileName);
                string uniqueName = "premio_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/premios/"), uniqueName);

                flpFoto.SaveAs(path);

                Session["CurrentFoto"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentFoto"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }
}