﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;


public partial class marcas_Comercios : PaginaMarcasBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                hdnIDMarca.Value = usu.IDMarca.ToString();
            }
        }
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        int idMarca = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            idMarca = usu.IDMarca;
        
            using (var dbContext = new ACHEEntities()) {
                return dbContext.Terminales.Include("Comercios")
                    .Where(x => (idMarca == 0 || x.Comercios.IDMarca == idMarca) && x.Comercios.Activo && x.Estado != "DE")
                    .OrderBy(x => x.Comercios.NombreFantasia)
                    .Select(x => new  {
                        IDComercio = x.IDComercio,
                        IDTerminal = x.IDTerminal,
                        SDS = x.Comercios.SDS,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        RazonSocial = x.Comercios.RazonSocial,
                        TipoDocumento = x.Comercios.TipoDocumento,
                        NroDocumento = x.Comercios.NroDocumento,
                        Telefono = x.Comercios.Telefono != null ? x.Comercios.Telefono :"",
                        Celular = x.Comercios.Celular != null ? x.Comercios.Celular : "",
                        Responsable = x.Comercios.Responsable != null ? x.Comercios.Responsable : "",
                        Email = x.Comercios.Email != null ? x.Comercios.Email :"",
                        IDMarca = x.Comercios.IDMarca ?? 0,
                        POSTerminal = x.POSTerminal,
                        POSSistema = (x.POSSistema != null) ? x.POSSistema : "",
                        NumEst = x.NumEst,
                        CobrarUsoRed = x.CobrarUsoRed,
                        IDFranquicia = x.Comercios.IDFranquicia

                    }).ToDataSourceResult(take, skip, sort, filter);
            }                 
        }
        else
            return null;
    }
}