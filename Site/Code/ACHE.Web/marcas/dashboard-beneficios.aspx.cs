﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
public partial class marcas_dashboard_beneficios : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosOtorgados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes6 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "1", data = (mes6.Any() ? mes6[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes5 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "2", data = (mes5.Any() ? mes5[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "3", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "4", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "5", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "6", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosCanjeados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes6 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "1", data = (mes6.Any() ? Math.Abs(mes6[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes5 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "2", data = (mes5.Any() ? Math.Abs(mes5[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "3", data = (mes4.Any() ? Math.Abs(mes4[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "4", data = (mes3.Any() ? Math.Abs(mes3[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "5", data = (mes2.Any() ? Math.Abs(mes2[0].data) : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "6", data = (mes1.Any() ? Math.Abs(mes1[0].data) : 0) });
            }
        }

        return list;
    }
}