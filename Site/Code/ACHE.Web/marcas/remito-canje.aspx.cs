﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class marcas_remito_canje : PaginaMarcasBase{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["IdCanje"]))
            {
                int idcanje = int.Parse(Request.QueryString["IdCanje"]);
                CargarDatos(idcanje);
            }
        }
    }
    private void CargarDatos(int idCanje)
    {
        using (var dbContext = new ACHEEntities())
        {
            var canje = dbContext.Canjes.Where(x => x.IDCanje == idCanje).FirstOrDefault();
            if (canje != null)
            {
                litFecha.Text = canje.FechaAlta.ToString("dd/MM/yyyy");
                litNroSocio.Text = canje.Tarjetas.Socios.IDSocio.ToString();
                litPremio.Text = canje.Productos.Nombre;
                litDNI.Text = canje.Tarjetas.Socios.NroDocumento;
                litNroCanje.Text ="#"+ idCanje.ToString();
                litNombreSocio.Text = canje.Tarjetas.Socios.Nombre + ", " + canje.Tarjetas.Socios.Apellido;

            }
        }
    }
}