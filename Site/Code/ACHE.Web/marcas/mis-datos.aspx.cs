﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class marcas_mis_datos : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            this.cargarDatosUsuario();
    }

    [WebMethod(true)]
    public static void grabar(string email)
    {
        var user = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

        using (var dbContext = new ACHEEntities())
        {
            var aux = dbContext.UsuariosComercios.Where(x => x.Email == email.Trim()).FirstOrDefault();
            if (aux != null && aux.IDUsuario != user.IDUsuario)
                throw new Exception("El email ya se encuentra registrado.");

            UsuariosComercios entity = dbContext.UsuariosComercios.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
            //entity.Name = name;
            //entity.LastName = lastName;
            entity.Email = email;
            user.Email = email;
            //if (pwd != string.Empty)
            //    entity.Pwd = pwd;

            dbContext.SaveChanges();
        }

        HttpContext.Current.Session["CurrentMarcasUser"] = user;
    }

    private void cargarDatosUsuario()
    {
        WebMarcasUser user = CurrentMarcasUser;
        //this.txtName.Text = user.Nombre;
        //this.txtLastname.Text = user.Apellido;
        this.litUsuario.Text = user.Usuario;
        this.txtEmail.Text = user.Email;
    }
}