﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Collections.Specialized;

public partial class marcas_campaniasEmail : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.SMS
                    .Where(x => x.IDMarca == idMarca)
                    .OrderBy(x => x.Nombre)
                    .Select(x => new SMSViewModel()
                    {
                        ID = x.IDSMS,
                        Nombre = x.Nombre,
                        Mensaje = x.Mensaje,
                        Estado = x.Estado == "B" ? "Borrador" : (x.Estado == "P" ? "Pendiente" : (x.Estado == "R" ? "Rechazada" : "Aprobada")),
                        FechaAlta = x.FechaAlta,
                        FechaEnvio = x.FechaEnvioSolicitada,
                        Enviados = x.SMSEnvios.Count(),
                        Tipo = x.Tipo == "I" ? "Interna" : "Externa",
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.SMS.Where(x => x.IDSMS == id && x.IDMarca == idMarca).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.Estado == "P")
                        {
                            ListDictionary replacements = new ListDictionary();
                            replacements.Add("<USUARIO>", "administrador");
                            replacements.Add("<MENSAJE>", "La campaña " + entity.Nombre + " de la marca " + usu.Marca + " ha sido cancelada por el usuario " + usu.Usuario);

                            bool send = EmailHelper.SendMessage(EmailTemplate.Notificacion, replacements, ConfigurationManager.AppSettings["Email.SMSActivacion"], "RedIN: Cancelacion de campaña SMS");
                        }

                        dbContext.SMS.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}