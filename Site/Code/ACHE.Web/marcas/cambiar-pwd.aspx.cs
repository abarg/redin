﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class marcas_cambiar_pwd : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            litUsuario.Text = CurrentMarcasUser.Usuario;
    }

    [WebMethod(true)]
    public static void grabar(string pwdActual, string pwd)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var user = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];

            using (var dbContext = new ACHEEntities())
            {
                UsuariosMarcas entity = dbContext.UsuariosMarcas.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                if (entity.Pwd != pwdActual)
                    throw new Exception("La contraseña actual ingresada es inválida");

                entity.Pwd = pwd;
                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentMarcasUser"] = user;
        }
    }
}