﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Linq.Expressions; 

public partial class marcas_canjes : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
            CargarCombos();
        }
    }

    private void CargarCombos() {
        CargarEstados();
        CargarFamilias();
    }

    private void CargarEstados() {
        using (var dbContext = new ACHEEntities()) {
            var result = dbContext.EstadosCanjes.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca)
                .OrderBy(x => x.IDEstado)
                .Select(x => new {
                    ID = x.IDEstado,
                    Nombre = x.Nombre
                }).ToList();

            if (result != null) {
                ddlEstado.DataSource = result;
                ddlEstado.DataTextField = "Nombre";
                ddlEstado.DataValueField = "ID";
                ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void CargarFamilias() {
        using (var dbContext = new ACHEEntities()) {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null) {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Canjes.Include("Productos").Include("Tarjetas").Include("Premios").Include("EstadosCanjes")
                    .Where(x => x.Tarjetas.IDMarca == idMarca)
                    .OrderBy(x => x.FechaAlta)
                    .Select(x => new SeguimientoCanjesViewModel()
                    {
                        IDCanje = x.IDCanje,
                        Cantidad = x.Cantidad,
                        Estado = x.EstadosCanjes.Nombre,
                        IDEstado = x.IDEstado,
                        Producto = x.Productos.Nombre,
                        NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                        Socio = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + ", " + x.Tarjetas.Socios.Nombre : "",
                        Tarjeta = x.Tarjetas.Numero,
                        Fecha = x.FechaAlta,
                        Familia = x.Productos.FamiliaProductos.Nombre,
                        IDFamilia = x.Productos.IDFamilia
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.Fecha <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            
            }
        }
        else
            return null;
    }


    [WebMethod(true)]
    public static string filter(string estado, string tarjeta, string nroDoc, string fechaDesde, string fechaHasta,string producto,string socio)
    {
        string fileName = "Productos";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var result = dbContext.Canjes.Include("Tarjetas").Include("Productos")
                    .Where(x => x.Tarjetas.IDMarca == idMarca).AsEnumerable();

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        result = result.Where(x => x.FechaAlta >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        result = result.Where(x => x.FechaAlta <= dtHasta);
                    }
                    //if (estado != "")
                    //    result = result.Where(x => x.Estado == estado);
                    if (tarjeta != "")
                        result = result.Where(x => x.Tarjetas.Numero.Contains(tarjeta.ToLower()));
                    if (nroDoc != "")
                        result = result.Where(x => x.Tarjetas.IDSocio.HasValue && x.Tarjetas.Socios.NroDocumento.Contains(nroDoc.ToLower()));
                    if (producto != "")
                        result = result.Where(x => x.Productos.Nombre.ToUpper().Contains(producto.ToUpper()));

                    ////var aux = dbContext.Canjes.Include("Tarjetas").Include("Productos")
                    ////    .Where(x => x.Tarjetas.IDMarca == idMarca).OrderBy(x => x.FechaAlta).ToList()
                    ////.Select(x => new {
                    ////    IDCanje = x.IDCanje,
                    ////    Estado = x.EstadosCanjes.Nombre,
                    ////    Fecha = x.FechaAlta,
                    ////    Producto = x.Productos.Nombre,
                    ////    Cantidad = x.Cantidad,
                    ////    Socio = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + ", " + x.Tarjetas.Socios.Nombre : "",
                    ////    Tarjeta = x.Tarjetas.Numero,
                    ////    NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : ""

                    ////});

                    ////if (socio != "")
                    ////    aux = aux.Where(x => x.Socio.Contains(socio.ToLower()));

                    ////if (producto != "")
                    ////    aux = aux.Where(x => x.Producto.Contains(producto.ToLower()));

                    var aux = result.OrderBy(x => x.FechaAlta).ToList()
                    .Select(x => new SeguimientoCanjesViewModel
                    {
                        IDCanje = x.IDCanje,
                        Estado = x.EstadosCanjes.Nombre,
                        Fecha = x.FechaAlta,
                        Producto = x.Productos.Nombre,
                        Cantidad = x.Cantidad,
                        Socio = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + ", " + x.Tarjetas.Socios.Nombre : "",
                        Tarjeta = x.Tarjetas.Numero,
                        NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : ""

                    });

                    if (socio != "")
                        aux = aux.Where(x => x.Socio.ToLower().Contains(socio.ToLower()));

                    //if (producto != "")
                    //    aux = aux.Where(x => x.Producto.Contains(producto.ToLower()));

                    dt = aux.ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }


    /*NUEVO MODULO DE EXPORTACION DE CANJES*/

    [System.Web.Services.WebMethod(true)]
    public static string exportarCanjes(string estado, string tarjeta, string nroDoc, string idFamilia, string fechaDesde, string fechaHasta, string producto, string socio)
    {
        string fileName = "Productos";
        string path = "/tmp/";
        DataTable dt = new DataTable();


        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var result = dbContext.Canjes.Include("Productos").Include("Tarjetas").Include("Premios").Include("EstadosCanjes")
                        .Where(x => x.Tarjetas.IDMarca == idMarca)
                        .OrderBy(x => x.FechaAlta).Select(x => new SeguimientoCanjesViewModel()
                        {
                            IDCanje = x.IDCanje,
                            Estado = x.EstadosCanjes.Nombre,
                            Iestado = x.IDEstado.ToString(),
                            Fecha = x.FechaAlta,
                            Producto = x.Productos.Nombre,
                            Cantidad = x.Cantidad,
                            Socio = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + ", " + x.Tarjetas.Socios.Nombre : "",
                            Tarjeta = x.Tarjetas.Numero,
                            NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                            Familia = x.Productos.IDFamilia.ToString(),
                        });

                    if (fechaDesde != "")
                    {
                        DateTime dtDsd = DateTime.Parse(fechaDesde);
                        result = result.Where(x => x.Fecha >= dtDsd).OrderBy(x => x.Fecha);
                    }

                    if (fechaHasta != "")
                    {
                        DateTime dtHst = DateTime.Parse(fechaHasta);
                        result = result.Where(x => x.Fecha >= dtHst).OrderBy(x => x.Fecha);
                    }

                    if (idFamilia != "")
                    {
                        result = result.Where(x => x.Familia == idFamilia).OrderBy(x => x.Fecha);
                    }

                    if (estado != "")
                    {
                        result = result.Where(x => x.Iestado == estado).OrderBy(x => x.Fecha);
                    }

                    if (tarjeta != "")
                    {
                        result = result.Where(x => x.Tarjeta == tarjeta).OrderBy(x => x.Fecha);
                    }

                    if (nroDoc != "")
                    {
                        result = result.Where(x => x.NroDocumento == nroDoc).OrderBy(x => x.Fecha);
                    }

                    if (socio != "")
                    {
                        result = result.Where(x => x.Socio.Contains(socio));
                    }

                    if (producto != "")
                    {
                        result = result.Where(x => x.Producto.Contains(producto));
                    }

                    dt = result.ToList().ToDataTable();

                    if (dt.Rows.Count > 0)
                    {
                        generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    }
                    else
                    {
                        throw new Exception("No se encuentran datos para los filtros seleccionados");
                    }

                    return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

}