﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

public partial class marcas_email_comercio : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");

            cargarMensaje();
        }
    }
    private void cargarMensaje()
    {
        using (var dbContext = new ACHEEntities())
        {
            var msg = dbContext.Marcas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault().EmailRegistroComercio;
            if (!string.IsNullOrEmpty(msg))
                txtEmail.Text = msg;
        }
    }

    [WebMethod(true)]
    public static void GuardarMensaje(string mensaje)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                using (var dbContext = new ACHEEntities())
                {
                    Marcas entity;
                    entity = dbContext.Marcas.Where(x => x.IDMarca == usu.IDMarca).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.EmailRegistroComercio = mensaje;
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }
}