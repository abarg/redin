﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
public partial class marcas_dashboard_tarjetas : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorro()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteAhorro " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteAhorro " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteAhorro " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteAhorro " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagado()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImportePagado " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImportePagado " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImportePagado " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImportePagado " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginal()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteOriginal " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteOriginal " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteOriginal " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_ImporteOriginal " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }
    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorroDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Marcas_ImporteAhorroPorDias " + idMarca + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginalDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);//"01-20-2014";// 
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Marcas_ImporteOriginalPorDias " + idMarca + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagadoDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Marcas_ImportePagadoPorDias " + idMarca + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }
    [WebMethod(true)]
    public static string obtenerTop10Socios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Marcas_TopSocios " + idMarca, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }
    [WebMethod(true)]
    public static string obtenerTop10SociosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Marcas_TopSociosMensual '" + fechaDesde + "','" + fechaHasta + "', " + idMarca, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTop10ComerciosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Marcas_TopComerciosMensual '" + fechaDesde + "','" + fechaHasta + "', " + idMarca, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTop10Comercios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Marcas_TopComercios " + idMarca, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCantTransacciones()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }





}



