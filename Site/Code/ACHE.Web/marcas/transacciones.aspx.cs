﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class marcas_transacciones : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }
    //irina nabel 
    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            int idComercioAjustesPuntos = 0;
            //string nombreComercioAjustesPuntos = ConfigurationManager.AppSettings["ComercioAjustesPuntos.Nombre"];
            int idTerminalAjustesPuntos = int.Parse(ConfigurationManager.AppSettings["TransaccionPuntos.IDTerminal"]);
            int idcanjes = int.Parse(ConfigurationManager.AppSettings["IDComercioCanjes"]);

            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesMarcasView
                    .Where(x => x.ImporteOriginal > 0.9m)// && x.FechaTransaccion >= fechaDesdeBase)
                    .OrderByDescending(x => x.FechaTransaccion)
                    .Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0,
                        IDMarca = x.IDMarca,
                        Marca = x.Marca,
                        Domicilio = x.DomicilioComercio,
                        IDComercio = x.IDComercio,
                        Credito = (x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.PuntosAContabilizar ?? 0) / 100) : (((x.PuntosAContabilizar ?? 0) / 100) * -1)
                    });

                if(idTerminalAjustesPuntos>0)
                 idComercioAjustesPuntos = dbContext.Terminales.Where(x => x.IDTerminal == idTerminalAjustesPuntos).FirstOrDefault().IDComercio;
               
                if (usu.SoloPOSPropios )
                    result = result.Where(x => x.ComercioMarca == idMarca || x.IDComercio == idComercioAjustesPuntos||x.IDComercio==idcanjes);
                //if (usu.SoloPOSPropios)
                //    result = result.Where(x => x.ComercioMarca == idMarca || x.Comercio.ToUpper() == nombreComercioAjustesPuntos);


                if (usu.SoloTarjetasPropias)
                    result = result.Where(x => x.IDMarca == idMarca);

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }








                #region Multimarca
                //idMultimarca = 0;
                //if (idMultimarca > 0) {
                //    var multimarcas = dbContext.MarcasAsociadas.Include("Marcas").Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                //    if (multimarcas.Count > 0)
                //        result = result.Where(x => multimarcas.Contains(x.IDMarca));
                //}
                #endregion


                return result.AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string documento, string comercio)
    {
        string fileName = "Transacciones";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.TransaccionesMarcasView
                        .Where(x => x.ImporteOriginal > 0.9m)
                        .OrderByDescending(x => x.FechaTransaccion).AsQueryable();

                    if (tarjeta != "")
                        info = info.Where(x => x.Numero.ToLower().Contains(tarjeta.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumentoSocio.ToLower().Contains(documento.ToLower()));
                    if (comercio != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(comercio.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }

                    if (usu.SoloPOSPropios)
                        info = info.Where(x => x.IDMarcaComercio.HasValue && x.IDMarcaComercio.Value == idMarca);
                    if (usu.SoloTarjetasPropias)
                        info = info.Where(x => x.IDMarca == idMarca);

                    dt = info.ToList().Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Domicilio = x.DomicilioComercio,
                        Credito = (x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.PuntosAContabilizar ?? 0) / 100) : (((x.PuntosAContabilizar ?? 0) / 100) * -1),
                        PuntoDeVenta = x.PuntoDeVenta,
                        TipoComprobante = x.TipoComprobante,
                        NroComprobante = x.NroComprobante,
                        Observacion = x.Descripcion
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjeta(string tarjeta)
    {
        return GetTrByTarjeta(tarjeta, null);
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjetaMax(string tarjeta, int max)
    {
        return GetTrByTarjeta(tarjeta, max);
    }

    private static List<TransaccionesViewModel> GetTrByTarjeta(string tarjeta, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesMarcasView.Where(x => x.Numero == tarjeta
                     && x.IDMarca == idMarca && x.ImporteOriginal > 1)
                     .OrderByDescending(x => x.FechaTransaccion).Select(x => new
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0,
                         ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0
                     });

                if (usu.SoloPOSPropios)
                    result = result.Where(x => x.ComercioMarca == idMarca);

                if (max.HasValue)
                    result = result.OrderByDescending(x => x.FechaTransaccion).Take(max.Value);

                list = result.Select(x => new TransaccionesViewModel
                {
                    Fecha = x.Fecha,
                    FechaTransaccion = x.FechaTransaccion,
                    Hora = x.Hora,
                    Operacion = x.Operacion,
                    Origen = x.Origen,
                    SDS = x.SDS,
                    Comercio = x.Comercio,
                    NroEstablecimiento = x.NroEstablecimiento,
                    //POSTerminal = x.POSTerminal,
                    Tarjeta = x.Tarjeta,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal,
                    ImporteAhorro = x.ImporteAhorro
                }).ToList();

            }
        }
        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        return GetTrBySocio(id, null);

    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocioMax(int id, int max)
    {
        return GetTrBySocio(id, max);
    }

    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesMarcasView.Where(x => x.IDSocio.HasValue && x.IDSocio == id
                     && x.IDMarca == idMarca && x.ImporteOriginal > 1)
                     .OrderByDescending(x => x.FechaTransaccion).Select(x => new
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0,
                         ComercioMarca = x.IDMarcaComercio.HasValue ? x.IDMarcaComercio.Value : 0
                     });

                if (usu.SoloPOSPropios)
                    result = result.Where(x => x.ComercioMarca == idMarca);

                if (max.HasValue)
                    result = result.OrderByDescending(x => x.FechaTransaccion).Take(max.Value);

                list = result.Select(x => new TransaccionesViewModel
                {
                    Fecha = x.Fecha,
                    FechaTransaccion = x.FechaTransaccion,
                    Hora = x.Hora,
                    Operacion = x.Operacion,
                    Origen = x.Origen,
                    SDS = x.SDS,
                    Comercio = x.Comercio,
                    NroEstablecimiento = x.NroEstablecimiento,
                    //POSTerminal = x.POSTerminal,
                    Tarjeta = x.Tarjeta,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal,
                    ImporteAhorro = x.ImporteAhorro
                }).ToList();
            }
        }
        return list;
    }

    //private static bool esMultimarca(ACHEEntities dbContext,int idMarca){
    //var marcasAsociadas=dbContext.Multimarcas.Where(x=> x.IDMarca1 == idMarca).ToList();
    //if (marcasAsociadas.Count() > 0)
    //    return true;
    //else
    //    return false;
    //}
}