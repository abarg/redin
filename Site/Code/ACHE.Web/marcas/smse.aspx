﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="smse.aspx.cs" Inherits="marcas_smse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.jqEasyCharCounter.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/smse.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
      <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

      <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
      <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
      <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
       <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>


    <style type="text/css">
        .thumb {
            width: inherit;
            margin: 5px;
            border: solid 1px #ccc;
        }

        .sexo {
            height: 100px;
        }

        input.radio.todos {
            margin-left: 46px;
        }

        input.radio.femenino {
            margin-left: 20px;
        }

        input.radio.masculino {
            margin-left: 16px;
        }

        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            position: absolute;
            top: 50%;
            left: 50%;
            display: none;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">


    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/marcas/sms.aspx") %>">Campañas SMS</a></li>
            <li>Edición</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Campañas SMS Internas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            <label id="lblError" style="color: red; display: none;"></label>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" ClientIDMode="Static" Value="I" />
                <asp:HiddenField runat="server" ID="hdnPrecio" Value="" />
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MinLength="3" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Fecha de envío</label>
           
                        Enviar Ahora?   
                        <asp:CheckBox ID="sendNowChk" runat="server" ></asp:CheckBox> 
                
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaEnvio" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div runat="server" id="divTransacciones" class="form-group">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label"><br />c/s Transacciones</label>
                <div>                     
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <br /><asp:DropDownList runat="server" class="form-control" ID="ddlTransacciones">
                            <asp:ListItem Text="TODAS" Value=""></asp:ListItem>
                            <asp:ListItem Text="SIN TRANSACCIONES" Value="sin"></asp:ListItem>
                            <asp:ListItem Text="CON TRANSACCIONES" Value="con"></asp:ListItem>
                        </asp:DropDownList>
                    </div>  
                    <div runat="server" id="divFechaTrans" class="col-md-2" style="bottom:5px;display: none">
                        <label> <span class="f_req">* </span>Fecha desde</label>
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10"/>
                    </div>                   
                </div>
                </div>
                <div runat="server" id="divProvincias"  class="form-group">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Provincias</label>
                    <div>                     
                        <div class="col-sm-4 col-md-4 col-lg-4">                            
                            <asp:DropDownList runat="server" class="form-control" ID="ddlProvincias" />
                        </div>                             
                    </div>
                </div>
                <div runat="server" id="divCiudades"  class="form-group">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Ciudades</label>
                    <div>                             
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <asp:DropDownList runat="server" value="" class="form-control" ID="ddlCiudades" />
                        </div>                      
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Sexo</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="imgs">
                            <table>
                                <tr>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/todos.png" id="img4" />
                                            <asp:RadioButton runat="server" Checked="true" ID="rdbTodos" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/femenino.png" id="img5" />
                                            <asp:RadioButton runat="server" ID="rdbFemenino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/masculino.png" id="img6" />
                                            <asp:RadioButton runat="server" ID="rdbMasculino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Edad</label>
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <table>
                            <tr>
                                <td>
                                    <%--<input type="checkbox" value="0" id="chk0" onclick="changeCheck('T');" />&nbsp;Todas--%>
                                    <asp:RadioButton runat="server" ID="chk0" ClientIDMode="Static" onclick="changeCheck();" Checked="true" GroupName="Edad" />&nbsp;Todas
                                    <asp:RadioButton runat="server" ID="chkSegmentado" ClientIDMode="Static" onclick="changeCheck();" GroupName="Edad" />&nbsp;Segmentado por rango
                                    <br />
                                    <div id="divTodos" style="display: none;margin-top: 20px;">
                                        <%--<input type="checkbox" value="1" id="chk1"  />&nbsp;0-18--%>
                                        <asp:CheckBox runat="server" ID="chk1" ClientIDMode="Static" />&nbsp;0-18
                                        <br />
                                        <%--<input type="checkbox" value="2" id="chk2"  />&nbsp;19-25--%>
                                        <asp:CheckBox runat="server" ID="chk2" ClientIDMode="Static" />&nbsp;19-25
                                        <br />
                                        <%--<input type="checkbox" value="3" id="chk3"  />&nbsp;26-30--%>
                                        <asp:CheckBox runat="server" ID="chk3" ClientIDMode="Static" />&nbsp;26-30
                                        <br />
                                        <%--<input type="checkbox" value="4" id="chk4"  />&nbsp;31-40--%>
                                        <asp:CheckBox runat="server" ID="chk4" ClientIDMode="Static" />&nbsp;31-40
                                        <br />
                                        <%--<input type="checkbox" value="5" id="chk5"  />&nbsp;40-50--%>
                                        <asp:CheckBox runat="server" ID="chk5" ClientIDMode="Static" />&nbsp;41-50
                                        <br />
                                        <%--<input type="checkbox" value="6" id="chk6"  />&nbsp;51-60--%>
                                        <asp:CheckBox runat="server" ID="chk6" ClientIDMode="Static" />&nbsp;51-60
                                        <br />
                                        <%--<input type="checkbox" value="7" id="chk7"  />&nbsp;61+--%>
                                        <asp:CheckBox runat="server" ID="chk7" ClientIDMode="Static" />&nbsp;61+
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div runat="server" id="divProfesiones"  class="form-group">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Profesión</label>
                    <div>                     
                        <div class="col-sm-4 col-md-4 col-lg-4">                            
                            <asp:DropDownList runat="server" class="form-control" ID="ddlProfesiones" />
                        </div>                             
                    </div>
                </div>
              
                
                <div runat="server" id="div1"  class="form-group">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Hobbies</label>
                    <div>                     
                        <div class="col-sm-4 col-md-4 col-lg-4">                            
                            <asp:DropDownList runat="server" class="form-control" ID="ddlHobbies" />
                        </div>                             
                    </div>
                </div>

       
                    <div id="div4" runat="server" class="form-group">
                        <label class="col-sm-4 col-md-4 col-lg-3 control-label">Localidad</label>
                        <div class="col-sm-4 col-md-4 col-lg-4">   
                            <asp:DropDownList  runat="server" value="" class="form-control chosen" ID="localidades" />
                        </div>
                    </div> 
   
                   
                    <div class="form-group">
                        <label class="col-sm-4 col-md-4 col-lg-3 control-label">Sede</label>
                        <div class="col-sm-4 col-md-4 col-lg-4">   
                          <asp:DropDownList  runat="server" class="form-control chosen" ID="ddlSedes" />
                        </div>
                    </div>

               
                    <div class="form-group">
                        <label class="col-sm-4 col-md-4 col-lg-3 control-label">Actividad</label>
                        <div class="col-sm-4 col-md-4 col-lg-4">   
                          <asp:DropDownList runat="server" value="" class="form-control chosen-select" ID="ddlActividades" />
                        </div>
                    </div>

                
                    <div class="form-group">
                        <label class="col-sm-4 col-md-4 col-lg-3 control-label">Horarios</label>
                        <div class="col-sm-4 col-md-4 col-lg-4">   
                          <asp:DropDownList runat="server" value="" class="form-control chosen-select" ID="ddlHorarios" />
                        </div>
                    </div>


                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mensaje</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" CssClass="form-control required alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group externa">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Teléfonos</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtTelefonos" CssClass="form-control required telefonos" ClientIDMode="Static" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        <br />
                        <p>Formato: <abbr style="color: #1975FF;">542236874456-movistar;542236859456-personal;542236874445-claro</abbr></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">
                        Costo x msj: $
                        <asp:Literal runat="server" ID="litCostoFijo" />
                    </label>
                    <div class="col-lg-9">


                        <div class="col-sm-2 col-lg-2 interna">
                             <a onclick="calcular()">Calcular Precio Final</a>       
                        </div>

                       <div class="col-sm-2 col-lg-2 interna">
                             <a onclick="verDetalle('sociosSMS')">Ver Listado</a>       
                        </div>


             




                        <div class="col-sm-3 col-lg-3 calculo interna">
                            Cant. a enviar: <span id="spnCantidad" />
                        </div>
                        <div class="col-lg-2 calculo">
                            <span class="text-success" style="font-weight: bold; font-size: 18px;" id="Span1">Total: </span>
                            <span class="text-success" style="font-weight: bold; font-size: 18px;" id="spnCosto"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();">Guardar</button>
                        <a href="sms.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
              
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                    <%--<a href="/modulos/reportes/facturacion.aspx">Facturación</a>--%>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


        <div class="loader"></div>


</asp:Content>

