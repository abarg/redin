﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;

public partial class marcas_SociosEdicion : PaginaMarcasBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            Session["CurrentFoto"] = null;
            cargarProvincias();

            if (!String.IsNullOrEmpty(Request.QueryString["IDSocio"])) {
                this.limpiarControles();
                this.hfIDSocio.Value = Request.QueryString["IDSocio"];
                Session["IDSocio"] = Convert.ToInt32(Request.QueryString["IDSocio"]);

                if (!this.hfIDSocio.Value.Equals("0")) {
                    this.hfGrabarDomicilio.Value = "1";
                    this.hfVerTarjetas.Value = "1";
                    this.cargarDatosSocio(Convert.ToInt32(Request.QueryString["IDSocio"]));
                }
            }
        }
    }

    private void cargarProvincias() {
        ddlProvincia.DataSource = Common.LoadProvincias();
        ddlProvincia.DataTextField = "Nombre";
        ddlProvincia.DataValueField = "ID";
        ddlProvincia.DataBind();
    }

    private void limpiarControles() {
        //Socio
        //this.txtTarjeta.Text = "";
        this.txtNroCuenta.Text = "";
        this.txtNombre.Text = "";
        this.txtApellido.Text = "";
        this.txtEmail.Text = "";
        this.txtNroDoc.Text = "";
        this.txtTelefono.Text = "";
        this.txtCelular.Text = "";
        this.ddlEmpresaCelular.SelectedValue = "";
        //this.txtPuntosDisponibles.Text = "";
        //this.txtPuntosTotales.Text = "";
        this.txtObservaciones.Text = "";
        //Domicilio
        //this.txtCiudad.Text = "";
        this.txtDomicilio.Text = "";
        this.txtCodigoPostal.Text = "";
        this.txtTelefonoDom.Text = "";
        this.txtFax.Text = "";
        this.txtPisoDepto.Text = "";
    }
    [WebMethod(true)]
    public static void guardarFechaVenc(int id, string fechaVencimiento,string numero)
    {
        using (var dbContext = new ACHEEntities())
        {
            Tarjetas tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == id&&x.Numero==numero).FirstOrDefault();
            tarj.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            dbContext.SaveChanges();
        }
    }
    private void cargarDatosSocio(int IDSocio) {
        try {
            //Socio
            bSocio bSocio = new bSocio();
            Socios oSocio = bSocio.getSocio(IDSocio);

            this.txtNroCuenta.Text = oSocio.NroCuenta;
            this.txtNombre.Text = oSocio.Nombre;
            this.txtApellido.Text = oSocio.Apellido;
            this.txtEmail.Text = oSocio.Email;
            if (oSocio.FechaNacimiento.HasValue) {
                this.ddlDia.SelectedValue = oSocio.FechaNacimiento.Value.Day.ToString();
                this.ddlMes.SelectedValue = oSocio.FechaNacimiento.Value.Month.ToString();
                this.ddlAnio.SelectedItem.Text = oSocio.FechaNacimiento.Value.Year.ToString();
            }
            if (oSocio.Sexo != null && oSocio.Sexo.Equals("F"))
                this.rdbFem.Checked = true;
            else if (oSocio.Sexo != null && oSocio.Sexo.Equals("M"))
                this.rdbMas.Checked = true;
            else
                this.rdbInd.Checked = true;
            this.txtNroDoc.Text = oSocio.NroDocumento;
            this.txtTelefono.Text = oSocio.Telefono;
            this.txtCelular.Text = oSocio.Celular;
            this.ddlEmpresaCelular.SelectedValue = oSocio.EmpresaCelular;
            this.txttwitter.Text = oSocio.Twitter;
            this.txtFacebook.Text = oSocio.Facebook;
            this.txtObservaciones.Text = oSocio.Observaciones;

            if (oSocio.AuthTipo == "FB") {
                imgFoto.ImageUrl = string.Format("https://graph.facebook.com/{0}/picture?type=normal", oSocio.AuthID);
                divFoto.Visible = true;
                lnkFotoDelete.Visible = false;
                lnkFoto.Visible = false;
            }
            else {
                if (!string.IsNullOrEmpty(oSocio.Foto)) {
                    lnkFoto.NavigateUrl = "/fileHandler.ashx?type=socios&module=marcas&file=" + oSocio.Foto;
                    lnkFotoDelete.NavigateUrl = "javascript: void(0)";
                    lnkFotoDelete.Attributes.Add("onclick", "return deleteUpload('" + oSocio.Foto + "', 'Foto')");
                    divFoto.Visible = true;

                    imgFoto.ImageUrl = "/files/socios/" + oSocio.Foto;
                }
                else
                    imgFoto.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";
            }

            //Domicilio
            if (oSocio.IDDomicilio.HasValue) {
                this.ddlPais.SelectedValue = "1";
                var oDomicilio = oSocio.Domicilios;
                if (oDomicilio != null) {
                    this.ddlProvincia.SelectedValue = oDomicilio.Provincia.ToString();
                    ddlCiudad.DataSource = Common.LoadCiudades(oDomicilio.Provincia);
                    ddlCiudad.DataTextField = "Nombre";
                    ddlCiudad.DataValueField = "ID";
                    ddlCiudad.DataBind();
                    ddlCiudad.Items.Insert(0, new ListItem("", ""));

                    if (oDomicilio.Ciudad.HasValue)
                        ddlCiudad.SelectedValue = oDomicilio.Ciudad.Value.ToString();
                    this.txtDomicilio.Text = oDomicilio.Domicilio;
                    this.txtCodigoPostal.Text = oDomicilio.CodigoPostal;
                    this.txtTelefonoDom.Text = oDomicilio.Telefono;
                    this.txtFax.Text = oDomicilio.Fax;
                    this.txtPisoDepto.Text = oDomicilio.PisoDepto;
                    this.txtLatitud.Text = oDomicilio.Latitud;
                    this.txtLongitud.Text = oDomicilio.Longitud;
                }
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDSocio, string NroCuenta, string Nombre, string Apellido, string Email, string Dia, string Mes, string Anio, string Sexo, string TipoDoc
        , string NroDoc, string Telefono, string Celular, string EmpresaCelular,string Twitter, string Facebook, string Observaciones
        , string GrabarDomicilio, string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax, string PisoDepto, string Lat, string Long) {

        try {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var currentUser = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                Socios entity;
                using (var dbContext = new ACHEEntities()) {
                    if (NroDoc != string.Empty && NroDoc != "00") {
                        var aux = dbContext.Socios.FirstOrDefault(s => s.NroDocumento == NroDoc && s.TipoDocumento == TipoDoc);
                        if (aux != null && aux.IDSocio != IDSocio)
                            throw new Exception("Ya existe un socio llamado " + aux.Apellido + ", " + aux.Nombre + " con ese Número de Documento en RedIN");
                    }

                    if (IDSocio > 0)
                        entity = dbContext.Socios.FirstOrDefault(s => s.IDSocio == IDSocio);
                    else {
                        entity = new Socios();
                        entity.FechaAlta = DateTime.Now;
                        entity.Pwd = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia)).ToString("ddMMyyyy");

                        var idFranquicia = dbContext.Marcas.Where(x => x.IDMarca == currentUser.IDMarca).First().IDFranquicia.Value;
                        entity.IDFranquiciaAlta = idFranquicia;
                    }
                    
                    entity.TipoMov = "A";
                    entity.TipoReg = "D";
                    entity.Fecha = DateTime.Now;
                    entity.NroCuenta = NroCuenta.ToString();
                    entity.Nombre = Nombre;
                    entity.Apellido = Apellido;
                    entity.Email = Email;
                    entity.FechaNacimiento = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), Convert.ToInt32(Dia));
                    entity.Sexo = Sexo;
                    entity.TipoDocumento = TipoDoc;
                    entity.NroDocumento = NroDoc.ToString();
                    entity.Telefono = Telefono;
                    entity.Celular = Celular;
                    entity.EmpresaCelular = EmpresaCelular;
                    entity.Twitter = Twitter;
                    entity.Facebook = Facebook;
                    entity.Observaciones = Observaciones;
                    entity.Activo = true;
                    if (HttpContext.Current.Session["CurrentFoto"] != null && HttpContext.Current.Session["CurrentFoto"] != "")
                        entity.Foto = HttpContext.Current.Session["CurrentFoto"].ToString();


                    if (!entity.IDDomicilio.HasValue) {
                        entity.Domicilios = new Domicilios();
                        entity.Domicilios.FechaAlta = DateTime.Now;
                    }

                    entity.Domicilios.TipoDomicilio = "S";
                    entity.Domicilios.Entidad = "S";
                    entity.Domicilios.Estado = "A";
                    entity.Domicilios.Pais = Pais;
                    entity.Domicilios.Provincia = int.Parse(Provincia);
                    if (Ciudad != string.Empty)
                        entity.Domicilios.Ciudad = int.Parse(Ciudad);
                    else
                        entity.Domicilios.Ciudad = null;
                    entity.Domicilios.Domicilio = Domicilio;
                    entity.Domicilios.CodigoPostal = CodigoPostal;
                    entity.Domicilios.Telefono = TelefonoDom;
                    entity.Domicilios.Fax = Fax;
                    entity.Domicilios.PisoDepto = PisoDepto;
                    entity.Domicilios.Latitud = Lat;
                    entity.Domicilios.Longitud = Long;
                //    entity.CostoTransaccional = 0;
                    entity.CostoSMS = 0;
                    entity.CostoSeguro = 0;
                    entity.CostoPlusin = 0;

                    if (IDSocio > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.Socios.Add(entity);
                        dbContext.SaveChanges();
                    }
                }

                return entity.IDSocio.ToString();
            }
            return "";
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void eliminarFoto(int id, string archivo) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var file = "//files//socios//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                        if (entity != null) {
                            entity.Foto = null;
                            HttpContext.Current.Session["CurrentFoto"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadFoto(object sender, EventArgs e) {
        try {
            var extension = flpFoto.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif") {

                string ext = System.IO.Path.GetExtension(flpFoto.FileName);
                string uniqueName = "foto_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/socios/"), uniqueName);

                flpFoto.SaveAs(path);

                Session["CurrentFoto"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex) {
            Session["CurrentFoto"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    [WebMethod(true)]
    public static void asociarTarjeta(int IDSocio, string Tarjeta) {
        try {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = null;

                oTarjeta = bTarjeta.getTarjetaPorNumero(Tarjeta, false);
                if (oTarjeta == null)
                    throw new Exception("El Número de Tarjeta no existe");
                else if (oTarjeta.IDMarca != idMarca)
                    throw new Exception("El Número de Tarjeta no existe");
                else if (oTarjeta.IDSocio.HasValue)
                    throw new Exception("La Tarjeta ya se encuentra asignada a un socio");
                else {
                    oTarjeta.IDSocio = IDSocio;
                    oTarjeta.FechaAsignacion = DateTime.Now;
                    bTarjeta.add(oTarjeta);

                    bSocio bSocio = new bSocio();
                    Socios oSocio = bSocio.getSocio(IDSocio);
                    var marca = oTarjeta.Marcas;

                    if (marca != null)
                    {

                        if (marca.EnvioMsjBienvenida && oSocio.Celular != string.Empty)
                            PlusMobile.SendSMSBienvenida(oTarjeta.IDMarca, marca.CostoSMS, marca.MsjBienvenida, oSocio.Celular, oSocio.EmpresaCelular);

                        if (marca.EnvioEmailRegistroSocio && oSocio.Email != string.Empty)
                        {
                            ListDictionary mensaje = new ListDictionary();
                            string mailHtml = marca.EmailRegistroSocio.Replace("XNOMBREX", oSocio.Nombre).Replace("XAPELLIDOX", oSocio.Apellido);
                            mensaje.Add("<MENSAJE>", mailHtml);
                            bool send = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, mensaje, oSocio.Email, "Bienvenido a " + marca.Nombre);
                        }
                    }
                }
            }
            else
                throw new Exception("Debe iniciar sesion nuevamente");
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idSocio) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities()) {
                return dbContext.Tarjetas.Where(x => x.IDSocio.HasValue && x.IDSocio.Value == idSocio && x.IDMarca == idMarca)
                     .OrderBy(x => x.IDSocio)
                     .Select(x => new TarjetasViewModel() {
                         IDTarjeta = x.IDTarjeta,
                         Marca = x.Marcas.Nombre,
                         IDSocio = x.IDSocio.Value,
                         Numero = x.Numero,
                         FechaAsignacion = x.FechaAsignacion,
                         FechaVencimiento = x.FechaVencimiento,
                         Estado = x.Estado == "A" ? "Activa" : "Baja",
                         MotivoBaja = x.MotivoBaja,
                         FechaBaja = x.FechaBaja,
                         Puntos = x.PuntosTotales,
                         Credito = x.Credito,
                         Giftcard = x.Giftcard,
                         POS = Math.Round(x.Credito + x.Giftcard),
                         Total = x.Credito + x.Giftcard
                     }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> buscarTarjetas(string tarjeta) {
        try {
            List<ComboViewModel> list = new List<ComboViewModel>();

            if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities()) {
                    var aux = dbContext.Tarjetas.Where(x => !x.IDSocio.HasValue && x.IDMarca == idMarca && x.Numero.Contains(tarjeta))
                     .OrderBy(x => x.Numero)
                     .Select(x => new {
                         Numero = x.Numero
                     }).Take(10).ToList();

                    foreach (var tar in aux)
                        list.Add(new ComboViewModel() { ID = tar.Numero, Nombre = tar.Numero });
                }
            }

            return list;
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}

