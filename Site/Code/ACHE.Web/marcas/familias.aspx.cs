﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class marcas_familias : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");

        }
    }
    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.FamiliaProductos
                    .Where(x => x.IDMarca == idMarca)
                    .OrderBy(x => x.Nombre)
                    .Select(x => new ProductosViewModel()
                    {
                        ID = x.IDFamiliaProducto,
                        Nombre = x.Nombre,
                        NombrePadre = x.IDPadre.HasValue ? x.FamiliaProductos2.Nombre : ""
                        //NombrePadre = x.IDPadre!= null? x.FamiliaProductos1.Where(y=> y.IDFamiliaProducto == x.IDPadre).FirstOrDefault().Nombre :"" 

                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.FamiliaProductos.Where(x => x.IDFamiliaProducto == id
                        && x.IDMarca == idMarca).FirstOrDefault();

                    if (entity != null)
                    {
                        if (entity.Productos.Any())
                            throw new Exception("Esta familia no puede ser eliminada ya que esta siendo utilizada");
                        else
                        {
                            dbContext.FamiliaProductos.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
            //var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            //throw e;
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string nombre, int idPadre)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    FamiliaProductos entity;
                    if (id > 0)
                    {
                        entity = dbContext.FamiliaProductos.Where(x => x.IDFamiliaProducto == id && x.IDMarca == idMarca).FirstOrDefault();
                    }
                    else
                    {
                        entity = new FamiliaProductos();
                        entity.IDMarca = idMarca;
                    }

                    entity.Nombre = nombre;
                    if (idPadre > 0)
                    {
                        if (id == idPadre)
                            throw new Exception("La familia padre no puede ser la misma que la hija");
                        else
                            entity.IDPadre = idPadre;
                    }

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.FamiliaProductos.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    [WebMethod(true)]
    public static int obtenerPadre(int id)
    {
        int idPadre = 0;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                idPadre = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDFamiliaProducto == id).FirstOrDefault().IDPadre ?? 0;
            }
        }
        return idPadre;
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarFamiliasPadres()
    {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == null).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();
                ComboViewModel vacio = new ComboViewModel();
                vacio.ID = "0";
                vacio.Nombre = "";
                listFamilias.Insert(0, vacio);
            }
        }
        return listFamilias;
    }
}