﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="stocke.aspx.cs" Inherits="marcas_stocke" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <%--    <div class="col-sm-12 col-md-12">--%>
    <h3 class="heading">Editar Stock Masivo</h3>
    <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
    <div class="alert alert-danger alert-dismissable" id="divErrorEmptySearch" style="display: none">El campo del Nombre de comercio no puede estar vacio</div>
    <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente</div>
    <form id="form" runat="server">
        <asp:HiddenField runat="server" ID="hdnIDProducto" Value="0" />
        <asp:HiddenField runat="server" ID="hdnIDMarca" Value="0" />
        <div class="formSep col-sm-12 col-md-12">
            <div class="row">
                <div class="col-sm-3">
                    <label>Familia</label>
                    <asp:DropDownList runat="server" ID="cmbFamilia" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                </div>
                <div class="col-sm-3">
                    <label>Sub Familia</label>
                    <asp:DropDownList runat="server" ID="cmbSubFamilia" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="col-sm-3">
                    <br />
                    <button class="btn btn" type="button" id="Button1" onclick="GenerarTablaProductos();">Buscar</button>
                </div>
                <div style="clear: both"></div>

            </div>
        </div>
       
        <div class="formSep col-sm-12 col-md-12">
            <div class="row">
                <label class="col-lg-12 control-label"></label>
                <div class="col-lg-12" id="tblCostos">
                    <table class="table table-striped table-bordered mediaTable">
                        <thead>
                            <tr>
                                <th style="width: 100px">Producto</th>
                                <th style="width: 100px">Codigo</th>
                                <th style="width: 100px">Stock Actual</th>
                                <th style="width: 100px">Stock a Agregar</th>
                                <th style="width: 100px">Stock Minimo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyCostos">
                            <tr>
                                <td colspan="5">Calculando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row stock">
            <div class="col-sm-3">
                <br />
                <button class="btn btn-success" type="button" id="btnActualizarStock" onclick="ActualizarStock();">Actualizar Stock</button>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/stocke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
</asp:Content>
