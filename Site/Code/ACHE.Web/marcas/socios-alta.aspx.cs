﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class marcas_socios_alta : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static SociosViewModel buscar(string documento)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Socios.Where(x => x.NroDocumento == documento).FirstOrDefault();
                    if (aux != null)
                    {
                        return new SociosViewModel()
                        {
                            Nombre = aux.Nombre,
                            Apellido = aux.Apellido,
                            NroCuenta = aux.FechaNacimiento.HasValue? aux.FechaNacimiento.Value.ToString("dd/MM/yyyy"):"",
                            Sexo = aux.Sexo,
                            Email = aux.Email,
                            IDSocio = aux.IDSocio
                        };
                    }
                    else
                        return null;
                }
            }
            else
                return null;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}