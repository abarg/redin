﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="usuarios.aspx.cs" Inherits="marcas_usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/usuarios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Usuarios</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de usuarios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formEdicion" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control" MaxLength="100"></asp:TextBox>
                            <span class="help-block">Usuario</span>
                        </div>
                        <div class="col-lg-3">
                            <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="100"></asp:TextBox>
                            <span class="help-block">Email</span>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MinLength="3" MaxLength="10"></asp:TextBox>
                            <span class="help-block">Contraseña</span>
                        </div>
                        <div class="col-lg-2">
                            <asp:DropDownList runat="server" ID="ddlTipoUsuario" CssClass="form-control">
                                <asp:ListItem Text="Admin" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Backoffice" Value="B"></asp:ListItem>
                                <asp:ListItem Text="Dataentry" Value="D"></asp:ListItem>
                            </asp:DropDownList>
                            <span class="help-block">Tipo</span>
                        </div>
                        <div class="col-lg-3">
                            <button runat="server" id="btnAgregarUsuario" class="btn" type="button" onclick="agregarUsuario();">Agregar</button>
                        </div>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIDUsuario" Value="0" />
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

