﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;

public partial class marcas_home : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litNombre.Text = CurrentMarcasUser.Marca;
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Marcas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault();
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Logo))
                    imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                else
                    imgLogo.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";
            }

            if (CurrentMarcasUser.Tipo == "A" && CurrentMarcasUser.Catalogo)
            {
                var count = dbContext.Premios.Where(x => x.StockActual <= x.StockMinimo).Count();
                if (count > 0)
                {
                    litComercio.Text = count.ToString();
                    pnlAlertasStock.Visible = true;
                }
                else
                    pnlAlertasStock.Visible = false;
            }
            else
                pnlAlertasStock.Visible = false;

            if (entity != null && entity.TipoCatalogo == "C") {
                pnlAlertasStock.Visible = false;

                var count = dbContext.Productos.Where(x => x.StockActual <= x.StockMinimo && x.IDMarca == CurrentMarcasUser.IDMarca).Count();
                if (count > 0) {
                    litCantAlertasStockProductos.Text = count.ToString();
                    pnlAlertasStockProductos.Visible = true;
                }
                else
                    pnlAlertasStockProductos.Visible = false;           
            
            }
        }
    }
}