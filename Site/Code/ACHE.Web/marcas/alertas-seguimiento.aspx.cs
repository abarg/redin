﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;
public partial class marcas_alertas_seguimiento : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> buscarTarjetas(string tarjeta,string nombre,string dni)
    {
        try
        {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();

            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {

                   var aux = dbContext.Tarjetas.Where(x => x.IDMarca == idMarca)
                         .Select(x => new {
                             Tarjeta = x.Numero,
                             Nombre = x.IDSocio.HasValue ? x.Socios.Nombre : "",
                             Apellido = x.IDSocio.HasValue ? x.Socios.Apellido : "",
                             NroDocumento = x.IDSocio.HasValue ? x.Socios.NroDocumento : "",
                             IDTarjeta = x.IDTarjeta
                         }).OrderByDescending(x => x.Apellido).ToList();

                if(tarjeta != "")
                    aux = aux.Where(x => x.Tarjeta.Contains(tarjeta)).ToList();

                if (nombre != "")
                    aux = aux.Where(x => x.Nombre.ToLower().Contains(nombre) || x.Apellido.ToLower().Contains(nombre)).ToList();

                if (dni != "")
                    aux=aux.Where(x => x.NroDocumento.Contains(dni)).ToList();


                   list = aux.OrderBy(x => x.Tarjeta)
                     .Select(x => new Combo2ViewModel
                     {
                         Nombre = x.Tarjeta,
                         ID = x.IDTarjeta
                     }).Take(10).ToList();                                         
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void agregarTarjeta(int idTarjeta)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    if (!dbContext.AlertasTarjetas.Any(x => x.IDMarca == idMarca && x.IDTarjeta == idTarjeta))
                    {
                        AlertasTarjetas alerta = new AlertasTarjetas();
                        alerta.IDMarca = idMarca;
                        alerta.IDTarjeta = idTarjeta;

                        dbContext.AlertasTarjetas.Add(alerta);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void eliminarTarjeta(int idTarjeta)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    AlertasTarjetas alerta = dbContext.AlertasTarjetas.FirstOrDefault(x => x.IDMarca == idMarca && x.IDTarjeta == idTarjeta);
                    if (alerta != null)
                    {
                        dbContext.AlertasTarjetas.Remove(alerta);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static List<SociosViewModel> getTarjetasEnSeguimiento()
    {
        List<SociosViewModel> list = new List<SociosViewModel>();

        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;


            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == idMarca)
                 .Select(x => new SociosViewModel
                 {
                     Tarjeta = x.Tarjetas.Numero,
                     Nombre = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Nombre : "",
                     Apellido = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido : "",
                     NroDocumento = x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                     EstadoTarjeta = x.Tarjetas.Estado.ToUpper() == "B" || x.Tarjetas.FechaBaja.HasValue ? "Baja" : "Activa" ,
                     IDSocio = x.IDTarjeta
                 }).OrderByDescending(x => x.Apellido).ToList();
            }
        }

        return list;
    }
}