﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class marcas_socios : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.SociosView
                        .Where(x => x.IDMarca == idMarca)
                        .OrderBy(x => x.Apellido)
                        .Select(x => new SociosViewModel()
                        {
                            IDSocio = x.IDSocio,
                            //Tarjeta = x.NumeroTarjeta,
                            Tarjeta = x.NumeroTarjeta,
                            Marca = x.Marca,
                            //Nombre = x.Nombre,
                            Apellido = x.Apellido + ", " + x.Nombre,
                            Email = x.Email,
                            FechaNacimiento = x.FechaNacimiento,
                            Sexo = x.Sexo == "I" ? "" : x.Sexo,
                            NroDocumento = x.NroDocumento,
                            Telefono = x.Telefono,
                            Celular = x.Celular,
                            EmpresaCelular = x.EmpresaCelular,
                            Observaciones = x.Observaciones,
                            Nombre = x.NumeroTarjeta,//se usa para la busqueda de tarjetas
                            Puntos = x.PuntosTotales,
                            Credito = x.Credito,
                            Giftcard = x.Giftcard,
                            POS = x.Credito + x.Giftcard,
                            Total = x.Credito + x.Giftcard
                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    //[System.Web.Services.WebMethod]
    //public static void Delete(int id)
    //{
    //    if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //    {
    //        try
    //        {
    //            bSocio bSocio = new bSocio();
    //            bSocio.deleteSocio(id);
    //        }
    //        catch (Exception e)
    //        {
    //            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
    //            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
    //            throw e;
    //        }
    //    }
    //}

    [WebMethod(true)]
    public static string Exportar(string apellido, string documento, string email, string tarjeta)
    {
        string fileName = "Socios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            try
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.SociosFullView.Where(x => x.IDMarca == idMarca).OrderBy(x => x.Apellido).AsEnumerable();
                    if (apellido != "")
                        info = info.Where(x => x.Apellido.ToLower().Contains(apellido.ToLower()) || x.Apellido.ToLower().Contains(apellido.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumento != null && x.NroDocumento.ToLower().Contains(documento.ToLower()));
                    if (email != "")
                        info = info.Where(x => x.Email != null && x.Email.ToLower().Contains(email.ToLower()));
                    if (tarjeta != "")
                        info = info.Where(x => x.NumeroTarjeta != null && x.NumeroTarjeta.ToLower().Contains(tarjeta.ToLower()));

                    dt = info.Select(x => new
                    {
                        Tarjeta = x.NumeroTarjeta,
                        Apellido = x.Apellido,
                        Nombre = x.Nombre,
                        Email = x.Email,
                        FechaNacimiento = x.FechaNacimiento.HasValue ? x.FechaNacimiento.Value.ToShortDateString() : "",
                        Sexo = x.Sexo,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        EmpresaCelular = x.EmpresaCelular,
                        FechaAlta = x.FechaAlta.HasValue ? x.FechaAlta.Value.ToShortDateString() : "",
                        Observaciones = x.Observaciones,
                        PuntosTotales = x.PuntosTotales,
                        Pais = x.Pais,
                        Provincia = x.Provincia,
                        Ciudad = x.Ciudad,
                        Domicilio = x.Domicilio,
                        PisoDepto = x.PisoDepto,
                        CodigoPostal = x.CodigoPostal,
                        TelefonoDom = x.TelefonoDom,
                        Fax = x.Fax
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}