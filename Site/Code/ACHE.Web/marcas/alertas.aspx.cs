﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model;

public partial class marcas_alertas : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
            else
                LoadAlertas();
        }
    }

    private void LoadAlertas()
    {
        int cant = 0;
        var html = string.Empty;
        using (var dbContext = new ACHEEntities())
        {

            var altas = dbContext.Alertas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.Activa && x.Prioridad == "A").ToList();
            if (altas.Any())
            {
                html = string.Empty;

                foreach (var alerta in altas)
                {
                    cant = obtenerCantidad(alerta, dbContext);

                    html += "<tr>";
                    html += "<td>" + alerta.Nombre + "</td>";
                    html += "<td>" + obtenerDetalle(alerta) + "</td>";
                    html += "<td>" + cant + "</td>";
                    if (cant > 0)
                        html += "<td><a style='cursor:pointer;' onclick='detalleAlerta(" + alerta.IDAlerta + ")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                }

                litAlertasAlta.Text = html;
            }
            else
                litAlertasAlta.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

            var medias = dbContext.Alertas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.Activa && x.Prioridad == "M").ToList();
            if (medias.Any())
            {
                html = string.Empty;
                foreach (var alerta in medias)
                {
                    cant = obtenerCantidad(alerta, dbContext);

                    html += "<tr>";
                    html += "<td>" + alerta.Nombre + "</td>";
                    html += "<td>" + obtenerDetalle(alerta) + "</td>";
                    html += "<td>" + cant + "</td>";
                    if (cant > 0)
                        html += "<td><a style='cursor:pointer;' onclick='detalleAlerta(" + alerta.IDAlerta + ")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                }

                litAlertasMedia.Text = html;
            }
            else
                litAlertasMedia.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";

            var bajas = dbContext.Alertas.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.Activa && x.Prioridad == "B").ToList();
            if (bajas.Any())
            {
                html = string.Empty;
                foreach (var alerta in bajas)
                {
                    cant = obtenerCantidad(alerta, dbContext);

                    html += "<tr>";
                    html += "<td>" + alerta.Nombre + "</td>";
                    html += "<td>" + obtenerDetalle(alerta) + "</td>";
                    html += "<td>" + cant + "</td>";
                    if (cant > 0)
                        html += "<td><a style='cursor:pointer;' onclick='detalleAlerta(" + alerta.IDAlerta + ")'>detalle</td>";
                    else
                        html += "<td>&nbsp;</td>";
                    html += "</tr>";
                }

                litAlertasBaja.Text = html;
            }
            else
                litAlertasBaja.Text = "<tr><td colspan='4'>No hay alertas predefinidas</td></tr>";
        }
    }

    private int obtenerCantidad(Alertas alerta, ACHEEntities dbContext)
    {
        int cant = 0;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        string sql = "";
        string tarjetas = "";

        if (alerta.Restringido)
        {
            foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == CurrentMarcasUser.IDMarca))
                tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";

            if (tarjetas.Length > 0)
                tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
        }

        DateTime fechaDesde = DateTime.Now;
        if (alerta.FechaDesde.HasValue)
            fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);

        if (alerta.Tipo == "Por monto")
        {
            sql = "select count(Numero) from TransaccionesMarcasView where IDMarca=" + CurrentMarcasUser.IDMarca;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
        }
        else if (alerta.Tipo == "Por cantidad")
        {
            sql = "select count(cant) from (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + CurrentMarcasUser.IDMarca;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";

            //cant = dbContext.Database.SqlQuery<int>(sql, null).First();
        }
        else if (alerta.Tipo == "Por monto y cantidad")
        {
            sql = "select count(cant) from (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + CurrentMarcasUser.IDMarca;
            if (alerta.Restringido && tarjetas.Length > 0)
                sql += " and Numero in (" + tarjetas + ")";
            sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
            if (alerta.FechaDesde.HasValue)
                sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
            sql += " group by Numero";
            sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";
        }

        cant = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();

        return cant;
    }

    private string obtenerDetalle(Alertas alerta)
    {
        string detalle = string.Empty;

        if (alerta.Tipo == "Por monto")
        {
            detalle = "Entre $" + alerta.MontoDesde.Value + " y $" + alerta.MontoHasta.Value;
        }
        else if (alerta.Tipo == "Por cantidad")
        {
            detalle = "Con trans >= " + alerta.CantTR.Value;
        }
        else if (alerta.Tipo == "Por monto y cantidad")
        {
            detalle = "Entre $" + alerta.MontoDesde.Value + " y $" + alerta.MontoHasta.Value;
            detalle += ". Con trans >= " + alerta.CantTR.Value;
        }

        if (alerta.FechaDesde.HasValue)
            detalle += ". En los últimos " + alerta.FechaDesde.Value + " días";
        if (alerta.Restringido)
            detalle += ". Sólo para tarjetas en seguimiento";

        return detalle;
    }

    [WebMethod(true)]
    public static string obtenerDetalleAlerta(int id)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            DateTime fechaDesde = DateTime.Now;

            using (var dbContext = new ACHEEntities())
            {

                var alerta = dbContext.Alertas.Where(x => x.IDMarca == idMarca && x.Activa && x.IDAlerta == id).FirstOrDefault();
                if (alerta != null)
                {
                    var sql = string.Empty;
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    string tarjetas = "";

                    if (alerta.FechaDesde.HasValue)
                        fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);

                    if (alerta.Restringido)
                    {
                        foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == idMarca))
                            tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";
                        
                        if (tarjetas.Length > 0)
                            tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
                    }

                    if (alerta.Tipo == "Por monto")
                    {
                        sql = "select FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesMarcasView where IDMarca=" + idMarca;
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        sql += " order by ImporteOriginal desc, FechaTransaccion";
                    }
                    else if (alerta.Tipo == "Por cantidad")
                    {
                        sql = "select FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesMarcasView where IDMarca=" + idMarca;
                        //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        sql += " and Numero in (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + idMarca;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
                        sql += " order by Numero, FechaTransaccion";
                    }
                    else if (alerta.Tipo == "Por monto y cantidad")
                    {
                        sql = "select FechaTransaccion,Origen,Operacion,ImporteOriginal, Numero, ISNULL(Apellido + ', ' + Nombre, '') as Socio from TransaccionesMarcasView where IDMarca=" + idMarca;
                        sql += " and Numero in (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + idMarca;
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        if (alerta.Restringido && tarjetas.Length > 0)
                            sql += " and Numero in (" + tarjetas + ")";
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion > '" + fechaDesde.ToString(formato) + "'";
                        sql += " group by Numero";
                        sql += " having (count(fecha) >= " + alerta.CantTR.Value + "))";
                        if (alerta.FechaDesde.HasValue)
                            sql += " and FechaTransaccion >= '" + fechaDesde.ToString(formato) + "'";
                        sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                        sql += " order by Numero, FechaTransaccion";
                    }

                    var list = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                    if (list.Any())
                    {
                        foreach (var detalle in list)
                        {
                            html += "<tr>";
                            html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                            html += "<td>" + detalle.Origen + "</td>";
                            html += "<td>" + detalle.Operacion + "</td>";
                            html += "<td style='text-align: right;'>" + detalle.ImporteOriginal.ToString("N2") + "</td>";
                            html += "<td>" + detalle.Numero + "</td>";
                            html += "<td>" + detalle.Socio + "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    public class DetalleAlerta
    {
        public DateTime FechaTransaccion { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
        public decimal ImporteOriginal { get; set; }
        public string Socio { get; set; }
        public string Numero { get; set; }
    }
}