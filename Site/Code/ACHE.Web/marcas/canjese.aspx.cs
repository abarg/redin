﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections;
using System.Collections.Generic;

public partial class marcas_canjese : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentMarcasUser.Tipo != "A")
            //    Response.Redirect("home.aspx");

            this.txtFechaAlta.Text = DateTime.Now.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0") {
                    cargarCombos();
                    CargarInfo(int.Parse(hdnID.Value));
                    txtCantidad.Enabled = false;
                }
                else {
                    cargarCombos();
                    ddlEstado.SelectedValue = "Solicitado";
                    ddlEstado.Enabled = false;

                    if (!string.IsNullOrEmpty(Request.QueryString["IdProducto"]))
                        ddlProductos.SelectedValue = Request.QueryString["IdProducto"];
                    if (!string.IsNullOrEmpty(Request.QueryString["idTarjeta"]))
                        txtValorSocio.Value = Request.QueryString["idTarjeta"];
                }
            }
            else
            {
                cargarCombos();
                ddlEstado.SelectedValue = "Solicitado";
                ddlEstado.Enabled = false;
                if (!string.IsNullOrEmpty(Request.QueryString["IdProducto"]))
                    ddlProductos.SelectedValue = Request.QueryString["IdProducto"];
                if (!string.IsNullOrEmpty(Request.QueryString["idTarjeta"]))
                {
                    var idtarjeta = int.Parse(Request.QueryString["idTarjeta"]);
                    txtValorSocio.Value = getNumeroTarjeta(idtarjeta);
                }
            }
        }
    }
    
    private string getNumeroTarjeta(int id)
    {
        var numero = "";

        bTarjeta btarjeta = new bTarjeta();
        numero = btarjeta.getTarjeta(id).Numero;
        
        return numero;
    }

    private void  cargarCombos() {
        using (var dbContext = new ACHEEntities()) {
            var result = dbContext.Productos
                .Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.Activo && x.StockActual >0)
                .OrderBy(x => x.Nombre)
                .Select(x => new {
                    ID = x.IDProducto,
                    Nombre = x.Nombre
                }).ToList();

            if (result != null) {
                ddlProductos.DataSource = result;
                ddlProductos.DataTextField = "Nombre";
                ddlProductos.DataValueField = "ID";
                ddlProductos.DataBind();
                ddlProductos.Items.Insert(0, new ListItem("", ""));
            }

            var estados = dbContext.EstadosCanjes.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca)
                .OrderBy(x => x.IDEstado)
                .Select(x => new
                {
                    ID = x.IDEstado,
                    Nombre = x.Nombre
                }).ToList();

            if (estados != null)
            {
                ddlEstado.DataSource = estados;
                ddlEstado.DataTextField = "Nombre";
                ddlEstado.DataValueField = "ID";
                ddlEstado.DataBind();
            }
        }
    }

    [WebMethod(true)]   
    public static void guardar(int id, int idProducto, string estado, int idEstado,int cantidad, string tarjeta, string obs,string motivoRechazo,string obsOtros)
    {
        bool msjEnviado = true;
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;
                int idComercio = int.Parse(ConfigurationManager.AppSettings["IDComercioCanjes"]);
                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(tarjeta, false);
                if (id == 0 && oTarjeta == null)
                    throw new Exception("El Número de Tarjeta no existe");
                else
                {

                    using (var dbContext = new ACHEEntities()) {
                        var comercio = dbContext.Comercios.Where(x => x.IDComercio == idComercio).FirstOrDefault();
                        var producto = dbContext.Productos.Where(x => (x.IDProducto == idProducto || idProducto == 0)).FirstOrDefault();
                        var marca = dbContext.Marcas.Where(x => x.IDMarca == idMarca).FirstOrDefault();
                        //var socio = dbContext.Socios.Where(x => oTarjeta.IDSocio == x.IDSocio).FirstOrDefault();
                        if (comercio != null) {
                            if (producto != null) {

                                if (id == 0) {
                                    int puntosTarjeta = oTarjeta.PuntosTotales;
                                    if ((producto.Puntos * cantidad) > puntosTarjeta)
                                        throw new Exception("La tarjeta no tiene puntos suficientes. Puntos actuales: " + puntosTarjeta);
                                }

                                Canjes entity;
                                if (id > 0)
                                    entity = dbContext.Canjes.Include("Tarjetas").Where(x => x.IDCanje == id && x.Tarjetas.IDMarca == idMarca).FirstOrDefault();
                                else {
                                    entity = new Canjes();
                                    entity.FechaAlta = DateTime.Now;
                                    entity.FechaUltEstado = DateTime.Now;
                                }

                                if (id == 0) {
                                    entity.IDMarca = idMarca;
                                    entity.IDTarjeta = oTarjeta.IDTarjeta;
                                    entity.IDProducto = idProducto;


                                    //Solicitado
                                    if (estado.ToLower() == "solicitado") {
                                        if (cantidad > 0) {
                                            if (cantidad <= producto.StockActual) {
                                                entity.Cantidad = cantidad;
                                                producto.StockActual = (producto.StockActual - cantidad);
                                                //oTarjeta.PuntosTotales = oTarjeta.PuntosTotales - producto.Puntos;

                                                bool send = false;
                                                string mensaje = "Se solicito un canje del producto " + producto.Nombre + " realizado el " + entity.FechaAlta;
                                                ListDictionary replacements = new ListDictionary();
                                                replacements.Add("<MENSAJE>", mensaje);
                                                string mailTo = dbContext.UsuariosMarcas.Where(x => x.IDMarca == idMarca).FirstOrDefault().Email;
                                                string asunto = "Canje Solicitado";
                                               // send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, mailTo, "RedIN :: " + asunto);

                                                //SMS
                                                if (marca.CelularAlertas != null && marca.EmpresaCelularAlertas != null) {
                                                    TelefonoSMS tel = new TelefonoSMS();
                                                    tel.Celular = marca.CelularAlertas;
                                                    tel.EmpresaCelular = marca.EmpresaCelularAlertas;
                                                //    PlusMobile.SendSMS(mensaje, tel);
                                                }
                                                if (dbContext.Canjes.All(x => x.IDCanje != entity.IDCanje)) {

                                                    entity.IDTransaccion = int.Parse(ACHE.Business.Common.CrearTransaccionCanje(dbContext, DateTime.Now, comercio.Terminales.FirstOrDefault().IDTerminal, "POS", "", entity.Observaciones, producto.Precio,
                                                     comercio.Terminales.FirstOrDefault().NumEst, oTarjeta.Numero, comercio.Terminales.FirstOrDefault().POSTerminal, "Canje", "000000000000", "1100", usu.Usuario, cantidad, producto.IDProducto,""));

                                                }

                                                if (producto.StockActual <= producto.StockMinimo && (producto.StockActual + cantidad) >= producto.StockMinimo) {

                                                    send = false;
                                                    string mensajeSinStock = "El producto " + producto.Nombre + " alcanzo el minimo de stock ( Stock actual : " + producto.StockActual + " )";
                                                    ListDictionary replacements2 = new ListDictionary();
                                                    replacements2.Add("<MENSAJE>", mensajeSinStock);
                                                    string asunto2 = "Stock Minimo del producto "+ producto.Nombre +" codigo : "+ producto.Codigo ;
                                                    send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements2, mailTo, "RedIN :: " + asunto2);
                                                    //SMS
                                                    if (marca.CelularAlertas != null && marca.EmpresaCelularAlertas != null) {
                                                        TelefonoSMS tel = new TelefonoSMS();
                                                        tel.Celular = marca.CelularAlertas;
                                                        tel.EmpresaCelular = marca.EmpresaCelularAlertas;
                                                        PlusMobile.SendSMS(mensajeSinStock, tel);
                                                    }
                                                }

                                            }
                                            else
                                                throw new Exception("La cantidad ingresada supera el stock actual del producto seleccionado,ingrese otra cantidad.");
                                        }
                                        else
                                            throw new Exception("La cantidad ingresada debe ser mayor a 0");
                                    }
                                    entity.Observaciones = obs;
                                }

                                bool hayCambioEstado = false;
                                if (id > 0 && idEstado != entity.IDEstado) {
                                    hayCambioEstado = true;
                                    entity.FechaUltEstado = DateTime.Now;
                                }
                                else if (estado.ToLower() == "solicitado")
                                    entity.Cantidad = cantidad;

                                if (id > 0) {
                                    if (hayCambioEstado) {
                                        var hist = new CanjesHistorial();
                                        hist.FechaAlta = DateTime.Now;
                                        hist.IDEstado = idEstado;
                                        hist.Usuario = usu.Usuario;
                                        entity.CanjesHistorial.Add(hist);

                                        //Solicitado
                                        if (estado.ToLower() == "solicitado") {
                                            if (cantidad <= producto.StockActual) {
                                                producto.StockActual = (producto.StockActual - cantidad);
                                                //oTarjeta.PuntosTotales = oTarjeta.PuntosTotales - producto.Puntos;

                                                //MAIL
                                                bool send = false;
                                                string mensaje = "Se solicito un canje del producto " + producto.Nombre + " realizado el " + entity.FechaAlta;
                                                ListDictionary replacements = new ListDictionary();
                                                replacements.Add("<MENSAJE>", mensaje);
                                                string mailTo = dbContext.UsuariosMarcas.Where(x => x.IDMarca == idMarca).FirstOrDefault().Email;
                                                string asunto = "Canje Solicitado";
                                             //   send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, mailTo, "RedIN :: " + asunto);

                                                //SMS
                                                if (marca.CelularAlertas != null && marca.EmpresaCelularAlertas != null) {
                                                    TelefonoSMS tel = new TelefonoSMS();
                                                    tel.Celular = marca.CelularAlertas;
                                                    tel.EmpresaCelular = marca.EmpresaCelularAlertas;
                                                  //  msjEnviado=PlusMobile.SendSMS(mensaje, tel);
                                                }

                                            }
                                            else
                                                throw new Exception("La cantidad ingresada no puede superar el stock actual");
                                        }
                                        //Preparado
                                        else if (estado.ToLower() == "preparado") {
                                            var socio = dbContext.Tarjetas.Include("Socios").Where(x => x.IDTarjeta == entity.IDTarjeta).FirstOrDefault().Socios;
                                            if (socio != null) {
                                                //MAIL
                                                bool send = false;
                                                string mensaje = "El Canje del producto " + producto.Nombre + " realizado el " + entity.FechaAlta + "está listo para ser retirado";
                                                ListDictionary replacements = new ListDictionary();
                                                replacements.Add("<MENSAJE>", mensaje);
                                                string mailTo = socio.Email;
                                                string asunto = "Canje Preparado";
                                                if (mailTo != "" && mailTo != null)
                                                    send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, mailTo, "RedIN :: " + asunto);

                                                //SMS
                                                    if (socio.Celular != null && socio.EmpresaCelular != null) {
                                                        TelefonoSMS tel = new TelefonoSMS();
                                                        tel.Celular = socio.Celular;
                                                        tel.EmpresaCelular = socio.EmpresaCelular;
                                                        msjEnviado = PlusMobile.SendSMS(mensaje, tel);                                                                                             
                                                }
                                            }
                                        }
                                        //Rechazado
                                        else if (estado.ToLower() == "rechazado") {
                                            producto.StockActual = (producto.StockActual + cantidad);

                                            var tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == entity.IDTarjeta).FirstOrDefault();
                                            if (tarj != null)
                                                ACHE.Business.Common.CrearTransaccionCanje(dbContext, DateTime.Now, comercio.Terminales.FirstOrDefault().IDTerminal, "POS", "", entity.Observaciones, producto.Precio,
                                                    comercio.Terminales.FirstOrDefault().NumEst, tarj.Numero, comercio.Terminales.FirstOrDefault().POSTerminal, "Carga", "000000000000", "2200", usu.Usuario, cantidad, producto.IDProducto,(entity.IDTransaccion.HasValue)?entity.IDTransaccion.ToString():"");


                                            //oTarjeta.PuntosTotales = oTarjeta.PuntosTotales + producto.Puntos;
                                            if (motivoRechazo != "")
                                                entity.MotivoRechazo = motivoRechazo;
                                            if (obsOtros != "")
                                                entity.ObservacionesRechazo = obsOtros;
                                        }
                                        entity.Cantidad = cantidad;
                                        entity.IDEstado = idEstado;
                                        dbContext.SaveChanges();
                                    }
                                }
                                else {
                                    var hist = new CanjesHistorial();
                                    hist.FechaAlta = DateTime.Now;
                                    hist.IDEstado = idEstado;
                                    hist.Usuario = usu.Usuario;
                                    entity.CanjesHistorial.Add(hist);

                                    entity.IDEstado = idEstado;
                                    dbContext.Canjes.Add(entity);
                                    dbContext.SaveChanges();
                                }
                            }
                            else
                                throw new Exception("El producto asociado no existe");
                        }
                        else
                            throw new Exception("El comercio asociado no existe");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
            
        }
    }
        
    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Canjes.Include("CanjesHistorial").Include("Productos").Include("Tarjetas").Where(x => x.IDCanje == id && x.Tarjetas.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault();
            if (entity != null)
            {
                txtFechaAlta.Text = entity.FechaAlta.ToShortDateString();
                txtCantidad.Text = entity.Cantidad.ToString();
                txtObservaciones.Text = entity.Observaciones;
                ddlProductos.SelectedValue = entity.IDProducto.ToString();
                ddlEstado.SelectedValue = entity.IDEstado.ToString();
                txtObservaciones.Enabled = false;

                if (entity.EstadosCanjes.Nombre.ToLower() == "rechazado" || entity.EstadosCanjes.Nombre.ToLower() == "entregado") {
                    ddlEstado.Enabled = false;
                    ddlMotivos.Enabled = false;
                    txtObsOtros.Enabled = false;
                    btnGrabar.Visible = false;
                }

                if (entity.MotivoRechazo != null) {
                    ddlMotivos.SelectedValue = entity.MotivoRechazo;
                    ddlMotivos.Visible = true;
                }
                if (entity.ObservacionesRechazo != null && entity.MotivoRechazo == "5") {
                    txtObsOtros.Text = entity.ObservacionesRechazo.ToString();
                }

                ddlProductos.Attributes.Add("style", "display:none");
                divBuscar1.Attributes.Add("style", "display:none");
                divBuscar2.Attributes.Add("style", "display:none");

                litPremioDesc.Text = "<span><strong>" + entity.Productos.Descripcion + "<br><br><strong></span>";
                if (entity.Tarjetas.IDSocio.HasValue)
                {
                    var socio = entity.Tarjetas.Socios;
                    litSocio.Text = socio.Apellido + ", " + socio.Nombre + "<br>DNI: " + socio.NroDocumento;
                    if (!string.IsNullOrEmpty(socio.Foto))
                        imgSocio.Src = "/files/socios/" + socio.Foto;
                    else
                        imgSocio.Src = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
                }
                else
                {
                    litSocio.Text = entity.Tarjetas.Numero;
                    imgSocio.Src = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
                }

                divHistorial.Attributes.Add("style", "display:block");
                int idCanje= int.Parse(hdnID.Value);
                var historial = dbContext.CanjesHistorial.Include("EstadosCanjes").Where(x => x.IDCanje == idCanje).ToList().Select(y => new
                {
                    Estado = y.EstadosCanjes.Nombre,
                    Usuario = y.Usuario,
                    Fecha = y.FechaAlta.ToShortDateString()
                });
                rptHistorial.DataSource = historial;
                rptHistorial.DataBind();
            }
        }
    }
}