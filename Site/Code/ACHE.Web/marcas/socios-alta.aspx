﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="socios-alta.aspx.cs" Inherits="marcas_socios_alta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/sociosAlta.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/marcas/socios.aspx") %>">Socios</a></li>
                <li>Alta de Socio</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Alta de Socio</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formAlta" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />

                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Nro Documento</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtNroDoc" CssClass="form-control required number" MaxLength="20" MinLength="6"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnBuscar" class="btn btn-success" type="button" onclick="buscar();" >Buscar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    </div>
                </div>
                <div id="divSocioEncontrado" style="display:none">
                    <div class="vcard">
					    <ul style="margin: 10px 0 0 180px">
						    <li class="v-heading">
							    Socio encontrado! ¿No es el socio que buscabas? Haz click <a href="sociosedicion.aspx">aquí</a> para darlo de alta
						    </li>
						    <li>
							    <span class="item-key">Nombre</span>
							    <div class="vcard-item" id="lblNombre"></div>
						    </li>
						    <li>
							    <span class="item-key">Apellido</span>
							    <div class="vcard-item" id="lblApellido"></div>
						    </li>
						    <li>
							    <span class="item-key">Email</span>
							    <div class="vcard-item" id="lblEmail"></div>
						    </li>
						    <li>
							    <span class="item-key">Sexo</span>
							    <div class="vcard-item" id="lblSexo"></div>
						    </li>
                            <li>
							    <span class="item-key">Fecha Nac.</span>
							    <div class="vcard-item" id="lblFechaNac"></div>
						    </li>
                            <li class="v-heading">
							    Ingrese los últimos 5 dígitos de la tarjeta para asignarle...
						    </li>
                        </ul>
                        <div class="alert alert-danger alert-dismissable" id="divErrorTarjetas" style="display: none;margin: 20px 0 0 220px"></div>
                        <div class="row" style="margin: 20px 0 0 190px">
                            <div class="col-sm-12 col-md-12">
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNuevaTarjeta" CssClass="form-control number" MinLength="5" MaxLength="5"></asp:TextBox>
                                </div>
                                <div class="col-lg-2">
                                    <button id="btnBuscarTarjetas" class="btn" type="button" onclick="buscarTarjetas();">Buscar</button>
                                </div>
                                <div class="col-lg-6" id="divResultado1" style="display:none">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTarjetas">
                                        <asp:ListItem Text="Seleccione una tarjeta" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-2" id="divResultado2" style="display:none">
                                    <button runat="server" id="btnAsociarTarjeta" class="btn btn-success" type="button" onclick="asociarTarjeta();">Asociar tarjeta</button>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdnIDSocio" Value="0" />
                    </div>
                </div>
                <div id="divSocioNoEncontrado" style="display:none">
                    <div class="vcard">
					    <ul style="margin: 10px 0 0 180px">
						    <li class="v-heading">
							    Socio NO encontrado! Haz click <a href="sociosedicion.aspx">aquí</a> para darlo de alta
						    </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

