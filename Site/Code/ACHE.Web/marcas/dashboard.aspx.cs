﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;


public partial class marcas_dashboard : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }



    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosOtorgados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes12 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "1", data = (mes12.Any() ? mes12[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "2", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "3", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "4", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosOtorgados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "5", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosCanjeados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes12 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "1", data = (mes12.Any() ? Math.Abs(mes12[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "2", data = (mes4.Any() ? Math.Abs(mes4[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "3", data = (mes3.Any() ? Math.Abs(mes3[0].data) : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "4", data = (mes2.Any() ? Math.Abs(mes2[0].data) : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PuntosCanjeados " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = "5", data = (mes1.Any() ? Math.Abs(mes1[0].data) : 0) });
            }
        }

        return list;
    }

    #region Estadisticas Superiores

    // PARA HABILITAR INFO SOCIOS SEXO

    //[System.Web.Services.WebMethod(true)]
    //public static string obtenerTotalSociosPorSexo()
    //{
    //    //List<Chart> html = new List<Chart>();
    //    var html = string.Empty;

    //    if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
    //    {
    //        var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
    //        int idMarca = usu.IDMarca;

    //        using (var dbContext = new ACHEEntities())
    //        {
    //            dbContext.Database.CommandTimeout = 180;
    //            var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalSociosPorSexo " + idMarca, new object[] { }).ToList();
    //            if (list.Any())
    //            {                 
    //                html = "M: " + list[0].data + " - F: " + list[1].data + " <br/> I: " + list[2].data;// + list[3].data);
    //                decimal total = list[0].data + list[1].data + list[2].data;// +list[3].data;
    //                html += "," + total;

    //            }
    //            else
    //                html = "Error";
    //        }
    //    }

    //    return html;
    //}

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> ObtenerTotalSociosSexo()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalSociosPorSexo " + idMarca, new object[] { }).ToList();
               
                list.Add(new Chart() { data = aux[0].data , label = "Masculino (" +aux[0].data+")"});
                list.Add(new Chart() { data = aux[1].data , label = "Femenino ("+aux[1].data+ ")" });
                list.Add(new Chart() { data = aux[2].data , label = "Indefinido ("+aux[2].data+")"});
                
            }

        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosAltaPorFechaMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_SociosAltaPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosAltaPorFechaMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_SociosAltaPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }

        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerSociosAltaPorFechaAno()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_SociosAltaPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardEmitidasMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_GiftcardsEmitidasPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardEmitidasMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_GiftcardsEmitidasPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }

        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardEmitidasAno()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_GiftcardsEmitidasPorFecha " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardSaldoCargado()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_GiftSaldoCargado " + idMarca, new object[] { }).ToList();

                if (total.Any())
                    html = Math.Abs(total[0].data * 100).ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardSaldoCanjeado()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_GiftSaldoCanjeado " + idMarca, new object[] { }).ToList();

                if (total.Any())
                    html = Math.Abs(total[0].data * 100).ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    /*[System.Web.Services.WebMethod(true)]
    public static string obtenerGiftcardEmitidas()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = "";
            string fechaHasta = "";

            
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_NOMBRESTORE " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (mes3.Any())
                {
                    html = int.Parse(mes3[0].data.ToString()).ToString("N").Replace(",00", "");
                }
                else
                {
                    html = "0";
                }

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_NOMBRESTORE " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (mes2.Any())
                {
                    html = int.Parse(mes2[0].data.ToString()).ToString("N").Replace(",00", "");
                }
                else
                {
                    html = "0";
                }

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_NOMBRESTORE " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (mes1.Any()) { 
                    html = int.Parse(mes1[0].data.ToString()).ToString("N").Replace(",00", "");
                }
                else { 
                    html = "0";
                }
            }
        }

        return html;
    }*/

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalPuntosMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_TotalPuntosMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data*100).ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalPuntosMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_TotalPuntosMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data * 100).ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalPuntosAnoAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_TotalPuntosMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalArancelMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_TotalArancelMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTRMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTRMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
           
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTRAnoAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

           
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTRMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }
    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTasaUsoMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_TotalTasaUsoMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("#0.00");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicketMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PromedioTicketMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicketMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PromedioTicketMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }
    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicketAnoAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_PromedioTicketMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerFacturacionMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_FacturacionMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }
    [System.Web.Services.WebMethod(true)]
    public static string obtenerFacturacionMesAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = "";
            string fechaHasta = "";
         

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_FacturacionMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerFacturacionAnoAnterior()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string fechaDesde = "";
            string fechaHasta = "";


            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                fechaDesde = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
               // fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Marcas_FacturacionMensual " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalEmails()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalEmails " + idMarca, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalCelulares()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalCelulares " + idMarca, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTarjetasActivas()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalTarjetasActivas " + idMarca, new object[] { }).ToList();
                if (list.Any())
                    html = int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "");// + " / " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTarjetasInactivas()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasInactivas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (list.Any())
                    html = int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "");// + " / " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }


    #endregion
}