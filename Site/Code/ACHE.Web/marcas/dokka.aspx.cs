﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ACHE.Business;
using System.IO;
using System.Globalization;

public partial class marcas_dokka : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";
    }


    protected void Importar(object sender, EventArgs e)
    {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        int errores = 0;

        if (flpArchivo.HasFile)
        {
            if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                ShowError("Formato incorrecto. El archivo debe ser CSV.");
            else
            {

                try
                {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);
                    
                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    using (var dbContext = new ACHEEntities())
                    {

                        for (int i = 0; i < rows.Length; i++)
                        {
                            
                            //Split the tab separated fields (comma separated split commented)
                            if (i > 0)
                            {
                                string[] dr = rows[i].Split(new char[] { ';' });

                                // Farm. | Farmacia | Tarjeta | Nro. Tarjeta | Movimiento | Socio | Comp. | Número | Fecha | Puntos | Unidades | Imp. Afiliado | Imp. O.Soc. | Imp. Neto

                                if (dr.Length == 14)
                                {

                                    int idDokka = int.Parse(dr[0].Trim());
                                    string nroTarjeta = dr[3].Trim();
                                    string fecha = dr[8].Trim();
                                    decimal importe = decimal.Parse(dr[11].Trim(), CultureInfo.InvariantCulture);

                                    // buscamos la farmacia
                                    var pivot = dbContext.DokkaPivot.Where(x => x.IDDokka == idDokka).FirstOrDefault();
                                    
                                    if(pivot != null)
                                    {
                                        var comercio = pivot.Comercios;

                                        var terminal = pivot.Comercios.Terminales.Where(x => x.POSTerminal.StartsWith("d")).FirstOrDefault();

                                        if(terminal != null)
                                        {

                                            var tarjeta = dbContext.Tarjetas.Include("Marcas").FirstOrDefault(t => t.Numero == nroTarjeta);

                                            if(tarjeta != null && tarjeta.TipoTarjeta == "B" && (tarjeta.FechaBaja == null || (tarjeta.FechaBaja.HasValue && tarjeta.FechaBaja.Value > DateTime.Now)))
                                            {

                                                System.Diagnostics.Debug.WriteLine(idDokka + " " + fecha + " " + nroTarjeta + " " + importe);

                                                bComercio bComercio = new bComercio();
                                                Transacciones tr = new Transacciones();
                                                tr.FechaTransaccion = DateTime.ParseExact(fecha, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                                                tr.Origen = "Doka";
                                                tr.CodigoPremio = "";
                                                tr.Descripcion = "doka";
                                                int descuento = bComercio.obtenerDescuento(terminal, dbContext, tarjeta);
                                                tr.Descuento = descuento;
                                                tr.Importe = importe;
                                                if (descuento > 0)
                                                    tr.ImporteAhorro = (importe * descuento) / 100;
                                                else
                                                    tr.ImporteAhorro = 0;
                                                tr.NumCupon = "";
                                                tr.NumEst = terminal.NumEst.PadLeft(15, '0');
                                                tr.NumReferencia = "";
                                                tr.NumRefOriginal = "";
                                                tr.NumTarjetaCliente = tarjeta.Numero;
                                                tr.Operacion = "Venta";
                                                //tr.PuntosAContabilizar = (int)(datos.totalPrice - tr.ImporteAhorro);

                                                int puntosAContabilizar = (int)(tr.Importe - tr.ImporteAhorro);
                                                decimal POSpuntos = bComercio.obtenerPuntos(terminal, tarjeta, dbContext);
                                                tr.Puntos = POSpuntos;
                                                tr.PuntosAContabilizar = puntosAContabilizar * bComercio.obtenerMulPuntos(terminal, dbContext, tr.NumTarjetaCliente);

                                                tr.PuntosDisponibles = "000000000000";
                                                tr.PuntosIngresados = (importe.ToString().Replace(",", "")).PadLeft(12, '0');
                                                tr.TipoMensaje = "1100";
                                                tr.PuntoDeVenta = "";
                                                tr.NroComprobante = "";
                                                tr.TipoComprobante = "Ticket";
                                                tr.TipoTransaccion = "000000";
                                                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                                                tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, tarjeta);
                                                tr.IDMarca = terminal.Comercios.IDMarca;
                                                tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                                                tr.IDComercio = terminal.IDComercio;
                                                tr.IDTerminal = terminal.IDTerminal;
                                                //dbContext.Transacciones.Add(tr);

                                                tr.TransaccionesDetalle = new List<TransaccionesDetalle>();

                                                bool isValid = true;

                                                if (isValid)
                                                {

                                                    tr.Usuario = "Doka";
                                                    tr.NumTerminal = terminal.POSTerminal;
                                                    tr.IDTerminal = terminal.IDTerminal;

                                                    dbContext.Transacciones.Add(tr);
                                                    dbContext.SaveChanges();
                                                    dbContext.ActualizarPuntosPorTarjeta(tarjeta.Numero);


                                                }


                                            }                         
                                            else
                                            {
                                                ShowError("Tarjeta invalida "); // tarjeta invalida
                                            }

                                        }
                                        else
                                        {
                                            ShowError("No hay ninguna terminal configurada para dokka"); // no existe la terminal
                                        }

                                    }
                                    else
                                    {
                                        ShowError("No se encontro el comercio codigo "+ idDokka); // no existe el comercio
                                    }

                                }
                                else
                                {


                                    ShowError("Formato de archivo incorrecto, debe contener 14 columnas");// formato de archivo incorrecto


                                }

                            }
                            else
                            {
                                errores++;

                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Estructura del archivo dokka:", "fail");

                            }

                        }
                    }

                    bool ok = true;

                    File.Delete(path);

                }
                catch (Exception ex)
                {
                    ShowError(ex.Message.ToString());
                }
            }
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }

    [WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.SociosTmp.Where(x => x.Error.Length > 0).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Affinity + "</td>";
                        html += "<td>" + detalle.NroTarjeta + "</td>";
                        html += "<td>" + detalle.Apellido + " " + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}