﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;
using ACHE.Model.ViewModels;

public partial class marcas_stocke : PaginaMarcasBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            cargarCombos();
            hdnIDMarca.Value = CurrentMarcasUser.IDMarca.ToString();
        }
    }

    private void cargarCombos() {
        using (var dbContext = new ACHEEntities()) {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null) {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void actualizarStock(int idFamilia, int idSubFamilia, string agregarStock, string stockMinimo,int idProducto) {
        try {
            using (var dbContext = new ACHEEntities()) {

                //if (agregarStock == null || agregarStock == "")
                //    throw new Exception("El campo Stock Actual no puede estar vacio");
                //else if (stockMinimo == null || stockMinimo == "")
                //    throw new Exception("El campo Stock Minimo no puede etar vacio");

                var producto = dbContext.Productos.Where(x => x.IDProducto == idProducto && (x.IDFamilia == idFamilia || idFamilia == 0) && (x.IDSubFamilia == x.IDSubFamilia || idSubFamilia == 0)).FirstOrDefault();

                if (producto != null) {
                    producto.StockMinimo = stockMinimo == null || stockMinimo == "" ? 0 : int.Parse(stockMinimo);
                    producto.StockActual = agregarStock == null || agregarStock == "" ? 0 : producto.StockActual + int.Parse(agregarStock);                        
                }
                else{
                        throw new Exception ("El producto con ID:" + idProducto + " no existe");
                }
                dbContext.SaveChanges();
                
            }
        }
        catch {
            throw new Exception("No se pudo actualizar el producto con ID: " + idProducto);
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubFamilias(string idFamiliaPadre) {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities()) {
                int id = 0;
                if(idFamiliaPadre != "")
                    id = int.Parse(idFamiliaPadre);
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == id).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();
                ComboViewModel vacio = new ComboViewModel();
                vacio.ID = "";
                vacio.Nombre = "";
                listFamilias.Insert(0, vacio);
            }
        }
        return listFamilias;
    }

    void Page_PreInit(object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
            MasterPageFile = "~/MasterPageFranquicias.master";
    }

    [System.Web.Services.WebMethod(true)]
    public static string generarTablaProductos(int idFamilia, int idSubFamilia , int idMarca) {
        string html = "";
        using (var dbContext = new ACHEEntities()) {
            int n = 0;
            var list = dbContext.Productos.Where(x => x.IDMarca == idMarca).ToList();
            if (idFamilia != 0)
                list = list.Where(x => x.IDFamilia == idFamilia).ToList();
            if (idSubFamilia != 0)
                list =  list.Where(x => x.IDSubFamilia == idSubFamilia).ToList();
            if (list.Count() > 0) {
                foreach (var item in list) {
                    n++;
                    html += "<tr class=\"selectCostos\"  id=\"%\">";
                    html += "<td style=\"display: none\"> <input id=\"id" + n + "\" class='form-control selectTarjetas' value='" + item.IDProducto + "' readonly /></td> ";
                    html += "<td> <input id=\"nom" + n + "\" class='form-control selectTarjetas' value='" + item.Nombre + "' readonly/></td> ";
                    html += "<td> <input id=\"cod" + n + "\" class='form-control selectTarjetas' value='" + item.Codigo + "' readonly/></td> ";
                    html += "<td> <input  type=\"number\" min=\"1\" step=\"1\"  id=\"txtStockActual" + n + "\" disabled class='form-control selectTarjetas' value='" + item.StockActual + "'/></td> ";
                    html += "<td> <input  type=\"number\" min=\"0\" step=\"1\"  id=\"txtAgregarStock" + n + "\" class='form-control selectTarjetas' /></td> ";
                    html += "<td> <input  type=\"number\" min=\"1\" step=\"1\"  id=\"txtStockMinimo" + n + "\" class='form-control selectTarjetas' value='" + item.StockMinimo + "'/></td> ";
                    html += "</tr>";
                }
            }
        }
        return html;
    }
}