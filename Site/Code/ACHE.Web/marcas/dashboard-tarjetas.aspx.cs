﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
public partial class marcas_dashboard_tarjetas : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
        }
    }



    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasActivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
              

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasActivas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { data = mes1[0].data, label = "Activas"});

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasInactivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;
                
                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasInactivas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { data = mes1[0].data, label = "Inactivas" });
            }
        }

        return list;
    }
   
    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasAsignadas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 180;

                fechaDesde = DateTime.Now.AddMonths(-6).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-6).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes7 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-6).ToString("MMM"), data = (mes7.Any() ? mes7[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes6 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-5).ToString("MMM"), data = (mes6.Any() ? mes6[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes5 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-4).ToString("MMM"), data = (mes5.Any() ? mes5[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TarjetasAsignadas " + idMarca + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerEstadoTerminales()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + "" + "','" + "" + "','" + "" + "'," + idMarca + "," + 0, new object[] { }).ToList();
                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }

                //var prevList = aux.ToList();
                int negro = aux.Count(x => x.Estado == "Negro");
                int naranja = aux.Count(x => x.Estado == "Naranja");
                int verde = aux.Count(x => x.Estado == "Verde");
                int rojo = aux.Count(x => x.Estado == "Rojo");
                int amarillo = aux.Count(x => x.Estado == "Amarillo");
                int azul = aux.Count(x => x.Estado == "Azul");
                int desconocido = aux.Count(x => x.Estado == "Desconocido");
                int noprobado = aux.Count(x => x.Estado == "Marron");

                list.Add(new Chart() { data = negro, label = "<a href=\"javascript:verDetalleTerminales('Negro');\">Baja (" + negro + ")</a>" });
                list.Add(new Chart() { data = naranja, label = "<a href=\"javascript:verDetalleTerminales('Naranja');\">Inválido (" + naranja + ")</a>" });
                list.Add(new Chart() { data = verde, label = "<a href=\"javascript:verDetalleTerminales('Verde');\">Activo Mayor a $1 (" + verde + ")</a>" });
                list.Add(new Chart() { data = rojo, label = "<a href=\"javascript:verDetalleTerminales('Rojo');\">No Reprogramado (" + rojo + ")</a>" });
                list.Add(new Chart() { data = amarillo, label = "<a href=\"javascript:verDetalleTerminales('Amarillo');\">Activo Menor a $1 (" + amarillo + ")</a>" });
                list.Add(new Chart() { data = azul, label = "<a href=\"javascript:verDetalleTerminales('Azul');\">Web (" + azul + ")</a>" });
                list.Add(new Chart() { data = desconocido, label = "<a href=\"javascript:verDetalleTerminales('Desconocido');\">Desconocido (" + desconocido + ")</a>" });
                list.Add(new Chart() { data = noprobado, label = "<a href=\"javascript:verDetalleTerminales('Marron');\">No Probado (" + noprobado + ")</a>" });
            }
        }

        return list;
    }


    [WebMethod(true)]
    public static string obtenerDetalleTerminales(string estado)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + estado + "','" + "" + "','" + "" + "'," + idMarca + "," + 0, new object[] { }).ToList();
                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo  && row.Reprogramado  && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1)
                        row.Estado = "Amarillo";
                    else if (row.Activo  && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }

                aux = aux.Where(x => x.Estado.ToLower() == estado.ToLower()).ToList();

                if (aux.Any())
                {
                    foreach (var detalle in aux)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.SDS + "</td>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Domicilio + "</td>";
                        html += "<td>" + detalle.FechaCarga + "</td>";
                        html += "<td>" + detalle.FechaAlta + "</td>";
                        html += "<td>" + detalle.Terminal + "</td>";
                        html += "<td>" + detalle.Establecimiento + "</td>";
                        html += "<td>" + detalle.FechaActivacion + "</td>";
                        html += "<td>" + detalle.FechaReprogramacion.ToString("dd/MM/yyyy") + "</td>";
                        html += "<td>" + ((detalle.Reprogramado )?"Si":"No")+ "</td>";
                        html += "<td>" + ((detalle.ComercioInvalido)?"Si":"No") + "</td>";
                        html += "<td>" + ((detalle.Activo)?"Si":"No") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='12'>No hay un detalle disponible</td></tr>";

            }
        }

        return html;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> ObtenerTotalSociosSexo()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Marcas_TotalSociosPorSexo " + idMarca, new object[] { }).ToList();

                list.Add(new Chart() { data = aux[0].data, label = "Masculino (" + aux[0].data + ")" });
                list.Add(new Chart() { data = aux[1].data, label = "Femenino (" + aux[1].data + ")" });
                list.Add(new Chart() { data = aux[2].data, label = "Indefinido (" + aux[2].data + ")" });

            }

        }

        return list;
    }

}



