﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class marcas_cambiar_estados_masivo : PaginaMarcasBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
            CargarCombos();
        }
    }

    private void CargarCombos()
    {
        CargarEstados();
        CargarFamilias();
    }

    private void CargarEstados()
    {
        using (var dbContext = new ACHEEntities())
        {
            var result = dbContext.EstadosCanjes.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca)
                .OrderBy(x => x.IDEstado)
                .Select(x => new
                {
                    ID = x.IDEstado,
                    Nombre = x.Nombre
                }).ToList();

            if (result != null)
            {
                ddlEstado.DataSource = result;
                ddlEstado.DataTextField = "Nombre";
                ddlEstado.DataValueField = "ID";
                ddlEstado.DataBind();
                this.ddlEstado.Items.Insert(0, new ListItem("", "0"));


                cmbEstadoMasivo.DataSource = result;
                cmbEstadoMasivo.DataTextField = "Nombre";
                cmbEstadoMasivo.DataValueField = "Nombre";
                cmbEstadoMasivo.DataBind();
            }
        }
    }

    private void CargarFamilias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null)
            {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", "0"));

            }
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static string filtrarEstadosMasivos(string fechaDesde, string fechaHasta, int estado, int idFamilia, string socio, string producto)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {


            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string html = "";
            using (var dbContext = new ACHEEntities())
            {
                int n = 0;
                var list = dbContext.Canjes.Where(x => x.Tarjetas.IDMarca == idMarca).OrderBy(x => x.FechaAlta).ToList();


                if (fechaDesde != string.Empty)
                {
                    DateTime dtDsd = DateTime.Parse(fechaDesde);
                    list = list.Where(x => x.FechaAlta >= dtDsd).OrderBy(x => x.FechaAlta).ToList();
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHst = DateTime.Parse(fechaHasta);
                    list = list.Where(x => x.FechaAlta <= dtHst).OrderBy(x => x.FechaAlta).ToList();
                }
                if (estado != 0)
                {
                    list = list.Where(x => x.IDEstado == estado).OrderBy(x => x.FechaAlta).ToList();
                }
                if (idFamilia != 0)
                {
                    list = list.Where(x => x.Productos.IDFamilia == idFamilia).OrderBy(x => x.FechaAlta).ToList();
                }
                if (socio != string.Empty)
                {
                    list = list.Where(x => x.Tarjetas.IDSocio.HasValue && x.Tarjetas.Socios.Nombre.ToLower().Contains(socio.ToLower())).ToList();
                }
                if (producto != string.Empty)
                {
                    list = list.Where(x => x.Productos.Nombre.ToLower().Contains(producto.ToLower())).OrderBy(x => x.FechaAlta).ToList();
                }


                if (list.Count() > 0)
                {
                    foreach (var item in list)
                    {
                        n++;
                        html += "<tr>";
                        html += "<td>" + item.FechaAlta + "</td> ";

                        if (item.Productos.IDProducto != null)
                            html += "<td>" + item.Productos.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";

                        html += "<td>" + item.Cantidad + "</td> ";

                        if (item.Productos.FamiliaProductos.IDFamiliaProducto != null)
                            html += "<td>" + item.Productos.FamiliaProductos.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";


                        if (item.Tarjetas.IDSocio != null)
                            html += "<td>" + item.Tarjetas.Socios.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";

                        switch (item.EstadosCanjes.Nombre.ToLower())
                        {
                            case "solicitado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "verificado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option selected value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "rechazado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option selected value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "preparado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option selected value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "entregado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option selected value='Entregado'>Entregado</option></select></td>";
                                break;
                            default:
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                        }

                        html += "</tr>";
                    }
                }
            }
            return html;
        }
        return "";
    }



    [System.Web.Services.WebMethod(true)]
    public static string generarTablaCanjes()
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            string html = "";
            using (var dbContext = new ACHEEntities())
            {
                int n = 0;
                var list = dbContext.Canjes.Where(x => x.Tarjetas.IDMarca == idMarca).OrderBy(x => x.FechaAlta).ToList();

                if (list.Count() > 0)
                {
                    foreach (var item in list)
                    {
                        n++;
                        html += "<tr>";
                        html += "<td>" + item.FechaAlta + "</td> ";

                        if (item.Productos.IDProducto != null)
                            html += "<td>" + item.Productos.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";

                        html += "<td>" + item.Cantidad + "</td> ";

                        if (item.Productos.FamiliaProductos.IDFamiliaProducto != null)
                            html += "<td>" + item.Productos.FamiliaProductos.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";


                        if (item.Tarjetas.IDSocio != null)
                            html += "<td>" + item.Tarjetas.Socios.Nombre + "</td> ";
                        else
                            html += "<td>" + "</td> ";

                        switch (item.EstadosCanjes.Nombre.ToLower())
                        {
                            case "solicitado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "verificado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option selected value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "rechazado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option selected value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "preparado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option selected value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                            case "entregado":
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option selected value='Entregado'>Entregado</option></select></td>";
                                break;
                            default:
                                html += "<td> <select id='cmbEstado_" + item.IDCanje + "' class='form-control estado'><option selected value='Solicitado'>Solicitado</option><option value='Verificado'>Verificado</option><option value='Rechazado'>Rechazado</option> <option value='Preparado'>Preparado</option><option  value='Entregado'>Entregado</option></select> </td>";
                                break;
                        }

                        html += "</tr>";
                    }
                }
            }
            return html;
        }
        return "";
    }


    [System.Web.Services.WebMethod(true)]
    public static void GuardarEstados(string estados)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            int idComercio = int.Parse(ConfigurationManager.AppSettings["IDComercioCanjes"]);
            var list = estados.Split('#');
            using (var dbContext = new ACHEEntities())
            {
                var comercio = dbContext.Comercios.Include("Terminales").Where(x => x.IDComercio == idComercio).FirstOrDefault();
                foreach (var item in list)
                {
                    if (item != "")
                    {
                        var aux = item.Split('_')[1];
                        var id = int.Parse(aux.Split('-')[0]);
                        var valor = aux.Split('-')[1].ToString();
                        var canje = dbContext.Canjes.Include("Productos").Where(x => x.IDCanje == id).FirstOrDefault();

                        var marca = dbContext.Marcas.Where(x => x.IDMarca == idMarca).FirstOrDefault();
                        if (canje.EstadosCanjes.Nombre != valor)
                        {
                            int idestadoNuevo = dbContext.EstadosCanjes.Where(x => x.Nombre == valor && x.IDMarca == idMarca).Select(x => x.IDEstado).FirstOrDefault();
                            var hist = new CanjesHistorial();
                            hist.FechaAlta = DateTime.Now;
                            hist.IDEstado = idestadoNuevo;
                            hist.Usuario = usu.Usuario;
                            canje.CanjesHistorial.Add(hist);
                            canje.IDEstado = idestadoNuevo;
                            //Solicitado
                            if (valor.ToLower() == "solicitado")
                            {

                                if (canje.Cantidad <= canje.Productos.StockActual)
                                {
                                    canje.Productos.StockActual = (canje.Productos.StockActual - canje.Cantidad);

                                    //MAIL
                                    bool send = false;
                                    string mensaje = "Se solicito un canje del producto " + canje.Productos.Nombre + " realizado el " + canje.FechaAlta;
                                    ListDictionary replacements = new ListDictionary();
                                    replacements.Add("<MENSAJE>", mensaje);
                                    string mailTo = dbContext.UsuariosMarcas.Where(x => x.IDMarca == idMarca).FirstOrDefault().Email;
                                    string asunto = "Canje Solicitado";
                                    send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, mailTo, "RedIN :: " + asunto);

                                    //SMS
                                    if (marca.CelularAlertas != null && marca.EmpresaCelularAlertas != null)
                                    {
                                        TelefonoSMS tel = new TelefonoSMS();
                                        tel.Celular = marca.CelularAlertas;
                                        tel.EmpresaCelular = marca.EmpresaCelularAlertas;
                                        bool msjEnviado = PlusMobile.SendSMS(mensaje, tel);
                                    }

                                }
                                else
                                    throw new Exception("La cantidad solicitada no puede superar el stock actual");

                            }
                            //Preparado
                            else if (valor.ToLower() == "preparado")
                            {
                                var socio = dbContext.Tarjetas.Include("Socios").Where(x => x.IDTarjeta == canje.IDTarjeta).FirstOrDefault().Socios;
                                if (socio != null)
                                {
                                    //MAIL
                                    bool send = false;
                                    string mensaje = "El Canje del producto " + canje.Productos.Nombre + " realizado el " + canje.FechaAlta + "está listo para ser retirado";
                                    ListDictionary replacements = new ListDictionary();
                                    replacements.Add("<MENSAJE>", mensaje);
                                    string mailTo = socio.Email;
                                    string asunto = "Canje Preparado";
                                    if (mailTo != "" && mailTo != null)
                                        send = EmailHelper.SendMessage(EmailTemplate.Aviso, replacements, mailTo, "RedIN :: " + asunto);

                                    //SMS
                                    if (socio.Celular != null && socio.EmpresaCelular != null)
                                    {
                                        TelefonoSMS tel = new TelefonoSMS();
                                        tel.Celular = socio.Celular;
                                        tel.EmpresaCelular = socio.EmpresaCelular;
                                        bool msjEnviado = PlusMobile.SendSMS(mensaje, tel);
                                    }
                                }
                            }
                            //Rechazado
                            else if (valor.ToLower() == "rechazado")
                            {
                                canje.Productos.StockActual = (canje.Productos.StockActual + canje.Cantidad);

                                var tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == canje.IDTarjeta).FirstOrDefault();
                                if (tarj != null)
                                {
                                    var terminal = comercio.Terminales.Where(x => x.Activo).FirstOrDefault();
                                    if (terminal == null)
                                        throw new Exception("El comercio no tiene terminales activas");

                                    ACHE.Business.Common.CrearTransaccionCanje(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", canje.Observaciones, canje.Productos.Precio,
                                    terminal.NumEst, tarj.Numero, terminal.POSTerminal, "Carga", "000000000000",
                                    "2200", usu.Usuario, canje.Cantidad, canje.Productos.IDProducto, (canje.IDTransaccion.HasValue) ? canje.IDTransaccion.ToString() : "");
                                }
                            }

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }
}