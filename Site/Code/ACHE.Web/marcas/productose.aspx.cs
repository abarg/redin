﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

using System.Collections.Specialized;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections;
using System.Collections.Generic;


public partial class marcas_productose : PaginaMarcasBase
{
    private static string logo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            logo = "";
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");

            cargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }
        }
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null)
            {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string codigo, string nombre, int idFamilia, string descripcion, string precio, bool activo, int idSubFamilia,string stockActual,string stockMinimo,string puntos)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    Productos entity;
                    if (id > 0)
                        entity = dbContext.Productos.Where(x => x.IDProducto == id && x.IDMarca == idMarca).FirstOrDefault();
                    else
                    {
                        entity = new Productos();
                        entity.IDMarca = idMarca;
                        entity.FechaAlta = DateTime.Now;
                    }

                    entity.Nombre = nombre;
                    entity.IDFamilia = idFamilia;
                    entity.Descripcion = descripcion;
                    entity.Codigo = codigo;
                    entity.Precio = Convert.ToDecimal(precio);
                    entity.Activo = activo;
                    entity.StockActual = int.Parse(stockActual);
                    entity.StockMinimo = int.Parse(stockMinimo);
                    entity.Puntos = int.Parse(puntos);
                    entity.Foto = logo;



                    if (idSubFamilia > 0)
                        entity.IDSubFamilia = idSubFamilia;
                    else
                        entity.IDSubFamilia = null;

                    if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                        entity.Foto = HttpContext.Current.Session["CurrentLogo"].ToString();

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Productos.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Productos.Where(x => x.IDProducto == id && x.IDMarca == CurrentMarcasUser.IDMarca).FirstOrDefault();
            if (entity != null)
            {
                txtNombre.Text = entity.Nombre;
                txtDescripcion.Text = entity.Descripcion;
                txtPrecio.Text = entity.Precio.ToString();
                txtCodigo.Text = entity.Codigo;
                txtStockActual.Text = entity.StockActual.ToString();
                txtStockMinimo.Text = entity.StockMinimo.ToString();
                chkActivo.Checked = entity.Activo;
                txtPuntos.Text = entity.Puntos.ToString();
                cmbFamilia.SelectedValue = entity.IDFamilia.ToString();
                if (entity.IDSubFamilia > 0 && entity.IDFamilia >0) {
                    cargarSubFamilias(entity.IDFamilia);
                    cmbSubFamilia.SelectedValue = entity.IDSubFamilia.ToString();
                }


                #region Adjuntos

                if (entity.Foto != null) {
                    if (!string.IsNullOrEmpty(entity.Foto)) {
                        //lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.Foto;
                        lnkLogo.NavigateUrl = "/../files/marcas/fotos/" + entity.Foto;
                        //lnkLogo.NavigateUrl = "<a href=\"../files/tickets/temp" + entity.Foto + " download>" + entity.Foto + "</a>";
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.Foto + "', 'divLogo')");
                        divLogo.Visible = true;
                        logo = entity.Foto;
                        //imgLogo.ImageUrl = "/files/marcas/fotos/" + entity.Foto;
                    }
                    //else
                    //    logo = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
                }
                #endregion
            }//../files/tickets/temp
        }
    }

    private void cargarSubFamilias(int idFamiliaPadre) {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities()) {
                int id = idFamiliaPadre;
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == id).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();

                cmbSubFamilia.DataSource = listFamilias;
                cmbSubFamilia.DataTextField = "Nombre";
                cmbSubFamilia.DataValueField = "ID";
                cmbSubFamilia.DataBind();
                cmbSubFamilia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubFamilias(string idFamiliaPadre) {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities()) {
                int id=int.Parse(idFamiliaPadre);
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == id).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();
                ComboViewModel vacio = new ComboViewModel();
                vacio.ID = "0";
                vacio.Nombre = "";
                listFamilias.Insert(0, vacio);
            }
        }
        return listFamilias;
    }

    protected void uploadLogo(object sender, EventArgs e) {
        try {
            var button = (sender as Control).ClientID;
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            string ext = System.IO.Path.GetExtension(flpLogo.FileName);
            string uniqueName = "logo_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
            string path = System.IO.Path.Combine(Server.MapPath("~/files/marcas/fotos/"), uniqueName);

            flpLogo.SaveAs(path);
            logo = uniqueName;
        }
        catch (Exception ex) {
            throw new Exception("No se pudo guardar la imagen");
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static void eliminarImagen(int id, string archivo, string div) {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null) {
            var file = "//files//marcas//fotos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file))) {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0) {
                    using (var dbContext = new ACHEEntities()) {
                        var entity = dbContext.Productos.Where(x => x.IDProducto == id).FirstOrDefault();
                        if (entity != null) {
                            switch (div) {
                                case "divLogo":
                                    logo = "";
                                    entity.Foto = "";
                                    break;
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }
}