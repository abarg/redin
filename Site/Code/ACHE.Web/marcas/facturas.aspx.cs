﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.IO;

public partial class marcas_facturas : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo != "A")
                Response.Redirect("home.aspx");
            else
                CargarFacturas();
        }
    }

    private void CargarFacturas()
    {
        using (var dbContext = new ACHEEntities())
        {
            var result = dbContext.Facturas.Include("Comercios")
                .Where(x => x.FechaCAE.HasValue && x.Visible && x.Comercios.IDMarca.HasValue && x.Comercios.IDMarca == CurrentMarcasUser.IDMarca)
                .Select(x => new FacturasFrontViewModel()
                {
                    ID = x.IDFactura,
                    Comercio = x.Comercios.RazonSocial,
                    NroDocumento = x.Comercios.NroDocumento,
                    Numero = x.Numero,
                    //Importe = x.ImporteTotal,
                    Importe = x.ImporteTotal,
                    FechaEmision = x.PeriodoHasta,
                    FechaVto = x.PeriodoHasta,
                    CondicionIva = x.Tipo
                }).OrderByDescending(x => x.FechaEmision).ToList();

            rptFacturas.DataSource = result;
            rptFacturas.DataBind();

            if (!result.Any())
                trSinFacturas.Visible = true;
        }
    }
}