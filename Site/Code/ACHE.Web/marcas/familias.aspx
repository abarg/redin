﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="familias.aspx.cs" Inherits="marcas_familias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
         <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/familias.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Familias</li>
        </ul>
     </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Configuración de Familias</h3>

            <div id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-md-10">
                            &nbsp;
                        </div>
                    </div>
                    <br/>
                    <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                    <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button runat="server" id="btnAceptar" class="btn btn-success" type="button" onclick="Nuevo();">Nuevo</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>


     <div class="modal fade" id="modalCargarFamilia">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Edicion Familia</h4>
                        <input type="hidden" id="hdnID" value="0" />
                    </div>
                    <div class="modal-body">
                            <div class="container">
                                <form runat="server" id="formEdicion">
                                   <div class="row">                                
                                       <div class="col-sm-8">
                                            <label class="col-lg-4 control-label"><span class="f_req">*</span> Nombre</label>
                                            <div class="col-lg-6">
                                                <input type="text" id="txtNombreFamilia" class="form-control required" />
                                            </div>
                                        </div>
                                    </div>
                                   <div class="row">                                
                                       <div class="col-sm-8">
                                            <label class="col-lg-4 control-label">Familia Padre</label>
                                            <div class="col-lg-6">
                                                <asp:DropDownList runat="server" ID="ddlFamilias" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnGuardar" onclick="guardar();">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

</asp:Content>

