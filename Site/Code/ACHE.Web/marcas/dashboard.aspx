﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMarcas.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="marcas_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!--[if lt IE 9]>
		<script src="/lib/flot/excanvas.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            margin-right: -3px;
            width: 0px !important;
            background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }
        .ov_boxes li {
            height: 80px!important;
        }
        .p_canvas{
            height: 80px!important;
        }
        .small-box{
            height: 110px;
        }
   
         @media (max-width: 767px) {
        
            .inner {
               display: inline-grid!important;
               width: 100px;
            }
            .small-box{
                height:260px;
                width: 100px;
                background-color: white;
                text-align: center;
                margin-top:15px;
            }
            .info-box-icon{
                height: 90px!important;
            }
            .small-div{
                margin-top: 0px!important;
            }
            .small-indef{
                 margin-top: 0px!important;
            }
            .span-anterior{
                float:none!important;
            }
            .span-año{
                float:none!important;
                margin-right: 0!important;
            }
            .checkbox1{
                float:none!important;
                margin-left:0px!important;
            }
            .checkbox2{
                float:none!important;
                margin-right:0px!important;
                margin-top:0px!important;
            }
         }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <section class="content">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">  
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_ventas_blanco.png" /></span>
                <span class="info-box-text">Ventas</span>
                <div>
                    <div>
                <span class="info-box-text subtitle">Mes en curso</span></div>
                    <div>
                <span id="resultado_facturacion" class="info-box-number" style="color:#00a65a;" >-</span></div>
                   </div>
                <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
                    <span class="info-box-text subtitle">Mes anterior</span></div>
                    <div>
                    <span id="resultado_facturacionMesAnterior" class="info-box-number subtitle">-</span></div>
                </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
                <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="resultado_facturacionAnoAnterior" class="info-box-number subtitle">-</span></div>
                </div>
                    </div>
            </div>
          </div>
        </div>

          <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">               
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_TarjetasActivas_blanco.png" /></span>         
             <span class="info-box-text">Tarjetas</span>
                 <div>
                    <div class="span-anterior" style="float:left;">
                        <div>
                 <span class="info-box-text subtitle">Activas</span></div>
                        <div>
              <span id="resultado_total_tarjetas_activas" class="info-box-number" style="color:#00a65a;">-</span></div>
                 </div>
                    <div class="span-año" style="float:right;margin-right:8px;">
                        <div>
              <span class="info-box-text subtitle">Inactivas</span></div>
                        <div>
              <span id="resultado_total_tarjetas_inactivas" class="info-box-number">-</span></div>  
                    </div>
                    </div>
            </div>          
          </div>
        </div>

          <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">
                
            <span class="info-box-icon"  style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_Transacciones_blanco.png" /></span>
                     
              <span class="info-box-text">Transacciones</span>
                <div>
              <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="resultado_tr_mensual" class="info-box-number" style="color:#00a65a;">-</span></div>
                 <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes pasado</span></div>
                    <div>
              <span id="resultado_tr_mes_anterior" class="info-box-number subtitle">-</span></div>
                </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="resultado_tr_ano_anterior" class="info-box-number subtitle">-</span></div>
             </div>
            </div>
            </div>           
          </div>
        </div>

        
         <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">             
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_PuntosOtorgados_blanco.png" /></span>                
              <span class="info-box-text">Puntos otorgados</span>
                <div>
              <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="puntos_otorgados_mensual" class="info-box-number" style="color:#00a65a;">-</span></div>
                  <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes pasado</span></div>
                    <div>
              <span id="puntos_otorgados_mes_anterior" class="info-box-number subtitle">-</span></div>
               </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
                <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="puntos_otorgados_ano_anterior" class="info-box-number subtitle">-</span></div>
                </div>
            </div>
            </div>         
          </div>
        </div>
 
         <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">
                
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_PuntosCanjeados_blanco.png" /></span>                
              <span class="info-box-text">Puntos canjeados</span>
                <div>
              <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="puntos_canjeados_mensual" class="info-box-number" style="color:#00a65a;">-</span></div>
                 <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes anterior</span></div>
                    <div>
              <span id="puntos_canjeados_mes_anterior" class="info-box-number subtitle">-</span></div>
                    </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="puntos_canjeados_ano_anterior" class="info-box-number subtitle">-</span></div>
            </div>
            </div>
            </div>
          </div>
        </div>

         <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">
                
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_DescuentosOtorgados_blanco.png" /></span>                
              <span class="info-box-text">Descuentos otorgados</span>
                <div>
              <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="" class="info-box-number" style="color:#00a65a;">-</span></div>
                  <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes pasado</span></div>
                    <div>
              <span id="" class="info-box-number subtitle">-</span></div>
                </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="" class="info-box-number subtitle">-</span></div>
                  </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">            
            <span class="info-box-icon" style="background-color:#00a65a;height:110px;">
                <img src="../img/dashboard/Logo_PromedioTickets_blanco.png" /></span>                     
            <span class="info-box-text">Promedio de tickets</span>
                <div>
                <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="resultado_ticketpromedio_mensual" class="info-box-number" style="color:#00a65a;">-</span></div>
                 <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
                <span class="info-box-text subtitle">Mes anterior</span></div>
                    <div>
              <span id="resultado_ticketpromedio_mes_anterior" class="info-box-number subtitle">-</span></div>
                    </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
                <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="resultado_ticketpromedio_ano_anterior" class="info-box-number subtitle">-</span></div>
                </div>
            </div>
            </div>
          </div>
        </div>
       
         <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">                
            <span class="info-box-icon" style="background-color:#00c0ef;height:110px;">
                <img src="../img/dashboard/Logo_SociosActivos_blanco.png" /></span>
               <span class="info-box-text">Socios</span>
              
                <div class="span-anterior" style="float:left;">
                    <div>
                    <span class="info-box-text subtitle">Masculinos</span></div>
                    <div>
              <span id="sexo_masculino" class="info-box-number">-</span></div>
                    </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Femeninos</span></div>
                    <div>
              <span id="sexo_femenino" class="info-box-number">-</span></div>
                    </div>
  
                <div class="small-indef" style="margin-top:50px;">
                    <div>
              <span class="info-box-text subtitle ">Indefinido</span></div>
                    <div>
              <span id="sexo_indefinido" class="info-box-number subtitle">-</span></div>
                   </div>
            
            </div>       
          </div>
        </div>

         <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">
                
            <span class="info-box-icon" style="background-color:#00c0ef;height:110px;">
                <img src="../img/dashboard/Logo_NuevosUsuarios_blanco.png" /></span>                
              <span class="info-box-text">Nuevos usuarios</span>
                <div>
                  <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="socios_mes_actual" class="info-box-number" style="color:#00c0ef;">-</span></div>
                  <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes pasado</span></div>
                    <div>
              <span id="socios_mes_anterior" class="info-box-number subtitle">-</span></div>
                </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="socios_ano_anterior" class="info-box-number subtitle">-</span></div>
                </div>
            </div>
            </div>     
          </div>
        </div>
           <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">
                
            <span class="info-box-icon" style="background-color:#dd4b39;height:110px;">
                <img src="../img/dashboard/Logo_GiftcardsEmitidas_blanco.png" /></span>                
              <span class="info-box-text">Giftcards emitidas</span>
                <div>
              <span class="info-box-text subtitle">Mes en curso</span></div>
                <div>
              <span id="giftcardsMesActual" class="info-box-number" style="color:#dd4b39;">-</span></div>
                <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Mes pasado</span></div>
                    <div>
              <span id="giftcardsMesAnterior" class="info-box-number subtitle">-</span></div>
                </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Este año</span></div>
                    <div>
              <span id="giftcardsAno" class="info-box-number subtitle">-</span></div>
                </div>
            </div>
            </div>
          </div>
        </div>
           <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">               
            <span class="info-box-icon" style="background-color:#dd4b39;height:110px;">
                <img src="../img/dashboard/Logo_Giftcards_blanco.png" /></span>                
              <span class="info-box-text">Giftcards - Saldos</span>
                <div>
              <span class="info-box-text subtitle">Cargados</span></div>
                <div>
              <span id="giftcardSaldosCargados" class="info-box-number" style="color:#dd4b39;">-</span></div>
                 <div class="small-div" style="margin-top:13px;">
                <div class="span-anterior" style="float:left;">
                    <div>
              <span class="info-box-text subtitle">Canjeados</span></div>
                    <div>
              <span id="giftcardSaldosCanjeados" class="info-box-number">-</span></div>
                    </div>
                <div class="span-año" style="float:right;margin-right:8px;">
                    <div>
              <span class="info-box-text subtitle">Pendientes de canje</span></div>
                    <div>
              <span id="giftcardSaldoPendienteCanje" class="info-box-number">-</span></div>
                 </div>
            </div>
            </div>
          </div>
        </div>
          <div class="col-md-4 col-sm-6 col-xs-3">
          <!-- small box -->
         <div class="small-box">
            <div class="inner">               
            <span class="info-box-icon" style="background-color:#f39c12;height:110px;">
                <img src="../img/dashboard/Logo_Marketing_blanco.png" /></span>                
             <span class="info-box-text">Módulo de Marketing</span>
               
                    <label>
                    <span class="info-box-text">Campaña Email / Campaña SMS</span>
                  </label>
                <div class="checkbox1" style="float:left;margin-left: 25px;">
                <label class="switch">   
                  <input type="checkbox" value="1" id="checkboxID">
                  <span class="slider round"></span>
                </label> 
              </div>
                
                <br />
                <div class="checkbox2" style="float:right;margin-right:30px;margin-top: -18px">
                   <label class="switch">
                  <input type="checkbox" value="1" id="checkboxID2">
                  <span class="slider round"></span>
                </label>
                </div>
                    
            </div>
          </div>
        </div>

        <!--fin row-->
        </div> 
        </section>
    
   


	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/marcas/dashboard.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

