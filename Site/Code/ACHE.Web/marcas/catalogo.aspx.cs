﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Services;

public partial class marcas_catalogo : PaginaMarcasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentMarcasUser.Tipo == "D")
                Response.Redirect("home.aspx");
            cargarCombos();
        }
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var familias = dbContext.FamiliaProductos.Where(x => x.IDMarca == CurrentMarcasUser.IDMarca && x.IDPadre == null).OrderBy(x => x.Nombre).ToList();
            if (familias != null)
            {
                cmbFamilia.DataSource = familias;
                cmbFamilia.DataTextField = "Nombre";
                cmbFamilia.DataValueField = "IDFamiliaProducto";
                cmbFamilia.DataBind();
                cmbFamilia.Items.Insert(0, new ListItem("", ""));
            }

            var socios = dbContext.Tarjetas.Include("Socios").Where(x => x.Socios.IDSocio != null && x.IDMarca == CurrentMarcasUser.IDMarca).
                Select(x => new
                {
                    Socio = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre + " - " + (x.Numero) + " - DNI: " + (x.Socios.NroDocumento) : "",
                    IDTarjeta = x.IDTarjeta
                }).ToList();

            if (socios != null)
            {
                ddlSocios.DataSource = socios;
                ddlSocios.DataTextField = "Socio";
                ddlSocios.DataValueField = "IDTarjeta";
                ddlSocios.DataBind();
                ddlSocios.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubFamilias(string idFamiliaPadre)
    {
        List<ComboViewModel> listFamilias = new List<ComboViewModel>();
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            using (var dbContext = new ACHEEntities())
            {
                int id = int.Parse(idFamiliaPadre);
                listFamilias = dbContext.FamiliaProductos.OrderBy(x => x.Nombre).Where(x => x.IDMarca == idMarca && x.IDPadre == id).Select(x => new ComboViewModel { ID = x.IDFamiliaProducto.ToString(), Nombre = x.Nombre }).ToList();
                ComboViewModel vacio = new ComboViewModel();
                vacio.ID = "";
                vacio.Nombre = "";
                listFamilias.Insert(0, vacio);
            }
        }
        return listFamilias;
    }



    [System.Web.Services.WebMethod(true)]
    public static ResultadosCatalogoViewModel Buscar(int pageSize, int page, int puntosDesde, int puntosHasta, int idFamilia, int idSubFamilia, string nombre)
    {
        List<CatalogoViewModel> resultList = null;
        bool hayMas = false;
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
            int idMarca = usu.IDMarca;
            page--;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Productos
                    .Where(x => x.IDMarca == idMarca && x.Activo && x.StockActual > 0)
                    .OrderBy(x => x.Descripcion)
                    .Select(x => new CatalogoViewModel()
                    {
                        Nombre = x.Nombre,
                        IDPremio = x.IDProducto,
                        IDFamilia = x.IDFamilia,
                        Familia = x.IDFamilia != null ? x.FamiliaProductos1.Nombre : "",
                        IDSubFamilia = x.IDSubFamilia.HasValue ? x.IDSubFamilia.Value : 0,
                        Descripcion = x.Descripcion,
                        Codigo = x.Codigo,
                        StockActual = x.StockActual,
                        ValorPuntos = x.Puntos,
                        Precio = x.Precio,
                        Imagen = x.Foto == null ? "nogift.png" : x.Foto
                    });

                if (idFamilia > 0)
                    result = result.Where(x => x.IDFamilia == idFamilia);
                if (idSubFamilia > 0)
                    result = result.Where(x => x.IDSubFamilia == idSubFamilia);
                if (puntosDesde > 0)
                    result = result.Where(x => x.ValorPuntos >= puntosDesde);
                if (puntosHasta > 0)
                    result = result.Where(x => x.ValorPuntos <= puntosHasta);
                if (nombre != "" && nombre != null)
                    result = result.Where(x => x.Nombre.Contains(nombre));

                resultList = result.ToList();

                hayMas = ((page + 1) * pageSize) < resultList.Count;

                resultList = resultList.Skip(page * pageSize).Take(pageSize).ToList();
            }
        }

        ResultadosCatalogoViewModel resultado = new ResultadosCatalogoViewModel();
        resultado.Items = resultList;
        resultado.Cantidad = hayMas ? 1 : 0;

        return resultado;
    }

    [WebMethod(true)]
    public static int puntosPorTarjeta(int idTarjeta)
    {
        int result = 0;
        using (var dbContext = new ACHEEntities())
        {
            result = dbContext.Tarjetas.Where(x => x.IDTarjeta == idTarjeta).ToList() != null ? dbContext.Tarjetas.Where(x => x.IDTarjeta == idTarjeta).Sum(x => x.PuntosTotales) : 0;

        }
        return result;
    }
}