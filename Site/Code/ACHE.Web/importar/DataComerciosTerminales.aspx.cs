﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;
using ACHE.Extensions;
using System.Globalization;
using System.Text;

public partial class importar_DataComerciosTerminales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }



    protected void ImportarDataComerciosTerminales(object sender, EventArgs e)
    {
        int error = 0;
        List<string> errores = new List<string>();
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";
        try
        {
            if (flpArchivo.HasFile)
            {
                if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
                    ShowError("Formato incorrecto. El archivo debe ser CSV.");
                else
                {

                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    //ISO 8859 CON STREAMREADER PARA QUE FUNCIONEN CARACTERES ESPECIALES(Ñ, TILDES, ETC)
                    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        // Use stream
                        StreamReader sr = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
                        string csvContentStr = sr.ReadToEnd();

                        //string csvContentStr = File.ReadAllText(path);
                        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                        using (var dbContext = new ACHEEntities())
                        {

                            for (int i = 0; i < rows.Length; i++)
                            {

                                //Split the tab separated fields (comma separated split commented)
                                if (i > 0)
                                {
                                    string[] dr = rows[i].Split(new char[] { ';' });


                                    if (dr.Length == 3)
                                    {
                                        string tmpBeneficio = dr[0].Trim(); // Beneficio
                                        string tmpTerm = dr[1].Trim(); // IDTerminal
                                        string tmpImg = dr[2].Trim(); // IMG

                                        if (dr[1].Trim() != "")
                                        {


                                            int idTerminal = int.Parse(tmpTerm);

                                            // BUSCO Terminal
                                            Terminales Terminal = dbContext.Terminales.Where(x => x.IDTerminal == idTerminal).FirstOrDefault();


                                            if (Terminal != null)
                                            {
                                                try
                                                {

                                                    Terminal.DescuentoDescripcion = tmpBeneficio;
                                                    Terminal.Imagenes = tmpImg;

                                                    dbContext.SaveChanges();

                                                }
                                                catch (Exception ex)
                                                {

                                                    error++;

                                                }

                                            }
                                            else
                                            {
                                                error++;
                                            }
                                        }
                                        else
                                        {
                                            error++;
                                        }
                                    }
                                }
                                else
                                {
                                    error++;
                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "DNI?:", "fail");
                                }
                            }
                        }
                    }
                }

                File.Delete(path);

            }
            else
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "No file:", "fail");

            }
        }
        catch (Exception ex)
        {

            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            throw ex;

        }

        //devolverErroresAsignacion(errores);

        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ERRORES:", error.ToString());

    }


    [WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.SociosTmp.Where(x => x.Error.Length > 0).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Affinity + "</td>";
                        html += "<td>" + detalle.NroTarjeta + "</td>";
                        html += "<td>" + detalle.Apellido + " " + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }


    private void ShowError(string msg)
    {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg)
    {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}