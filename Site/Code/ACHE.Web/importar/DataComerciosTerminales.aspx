﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DataComerciosTerminales.aspx.cs" Inherits="importar_DataComerciosTerminales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li>Importación de socios</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Importación de Data</h3>
            
            <div class="alert alert-danger alert-dismissable" id="pnlError" runat="server" visible="false">
                <asp:Literal runat="server" ID="litError"></asp:Literal>

            </div>
            <div class="alert alert-success alert-dismissable" id="pnlOK" runat="server" visible="false">
                <asp:Literal runat="server" ID="litOk"></asp:Literal>
            </div>
            
            <form runat="server" id="formImportar" class="form-horizontal" role="form">

                <div class="form-group">
                    <label for="ddlFranquicias" class="col-lg-2 control-label"><span class="f_req">*</span> Archivo CSV</label>
                    <div class="col-lg-3">
                        <asp:FileUpload runat="server" ID="flpArchivo" />
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <asp:Button runat="server" ID="btnImportar" CssClass="btn btn-success" Text="Importar" OnClick="ImportarDataComerciosTerminales" />
                    </div>
                </div>
                 <div class="col-lg-10" runat="server" id="lnkPremiosPrueba" style="padding-left: 135px;">                                                                     
                    Descargar ejemplo CSV <a href="/files/importarsocios.csv" download>Descargar </a>  
                </div>



            </form>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle">Detalle de errores</h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>Affinity</th> 
                                <th>Nro Tarjeta</th> 
                                <th>Socio</th> 
                                <th>Error</th> 
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/common/importarSocios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>