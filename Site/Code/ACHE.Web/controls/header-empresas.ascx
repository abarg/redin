﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header-empresas.ascx.cs" Inherits="controls_header_empresas" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" style="width:400px" href="<%= ResolveUrl("~/empresas/home.aspx") %>"><asp:Label runat="server" ID="lblNombre" /></a>
                <ul class="nav navbar-nav" id="mobile-nav">
					<li runat="server" id="liDashboard" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Dashboard <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/empresas/dashboard.aspx") %>">Tarjetas</a></li>
                            <li runat="server" id="liDashGift"><a href="<%= ResolveUrl("~/empresas/dashboard-gift.aspx") %>">Giftcards</a></li>
                        </ul>
                    </li>
					<li runat="server" id="liTransacciones">
						<a href="<%= ResolveUrl("~/empresas/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span> Transacciones</a>	
                    </li>
                    <li runat="server" id="liFacturacion">
						<a href="<%= ResolveUrl("~/empresas/facturas.aspx") %>"><span class="glyphicon glyphicon-book"></span> Facturación</a>	
                    </li>
                  	<li runat="server" id="liAlertas" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;Alertas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li id="li2" runat="server"><a href="<%= ResolveUrl("~/empresas/alertas-config.aspx") %>">Configuración de Alertas</a></li>
                            <li id="li3" runat="server"><a href="<%= ResolveUrl("~/empresas/alertas.aspx") %>">Alertas</a></li>
                            <li id="li1" runat="server"><a href="<%= ResolveUrl("~/empresas/alertas-seguimiento.aspx") %>">Tarjetas en seguimiento</a></li>
                        </ul>
                    </li>
                    <li runat="server" id="liReportes" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Reportes <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/empresas/dashboard-terminales.aspx") %>">Estado de terminales</a></li>
                        </ul>
                    </li>

                   <li runat="server" id="li4" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Actividades <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/empresas/ListadoBeneficiarios.aspx") %>">Listado Beneficiarios</a></li>
                        </ul>
                    </li>
                    <%--<li>
						<a href="<%= ResolveUrl("~/empresas/giftweb.aspx") %>"><span class="glyphicon glyphicon-gift"></span> Gift Web</a>	
                    </li>--%>
				</ul>

                <ul class="nav navbar-nav user_menu pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown">Ayuda<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                        </ul>
                    </li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar"/><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/empresas/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/empresas/mis-datos.aspx") %>">Mis datos</a></li>
                            <li runat="server" id="liUsuarios"><a href="<%= ResolveUrl("~/empresas/usuarios.aspx") %>">Administrar usuarios</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login-empresas.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>