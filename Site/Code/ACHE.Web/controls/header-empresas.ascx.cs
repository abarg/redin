﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_header_empresas : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentEmpresasUser"] != null)
        {
            var oUsuario = (WebEmpresasUser)Session["CurrentEmpresasUser"];
            this.lblUsuario.Text = oUsuario.Usuario;
            lblNombre.Text = oUsuario.Empresa;

            if (oUsuario.Tipo == "B")
            {
                liFacturacion.Visible = liDashboard.Visible = liDashGift.Visible = liTransacciones.Visible = liUsuarios.Visible = false;
            }
        }
    }
}