﻿ <%@ Control Language="C#" AutoEventWireup="true" CodeFile="sidebar.ascx.cs" Inherits="controls_sidebar" %>


<a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">
    <div class="sidebar_inner_scroll">
        <div class="sidebar_inner">
            <div id="side_accordion" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseTwo" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-user"></i> Seguridad
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseTwo">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/Usuarios.aspx") %>">Usuarios</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseThree" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-cog"></i> Configuración
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseThree">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="nav-header">Favoritos</li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Franquicias.aspx") %>">Franquicias</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Marcas.aspx") %>">Marcas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Empresas.aspx") %>">Empresas</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Multimarcas.aspx") %>">Multimarcas</a></li>
                                 <li><a href="<%= ResolveUrl("~/modulos/configuracion/Dealer.aspx") %>">Dealer</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Rubros.aspx") %>">Rubros</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Provincias.aspx") %>">Provincias</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Ciudades.aspx") %>">Ciudades</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/Zonas.aspx") %>">Zonas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Profesion.aspx") %>">Profesiones</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Hobbys.aspx") %>">Hobbies</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/configuracion/TarjetasDeCredito.aspx") %>">Tarjetas de Crédito</a></li>
                                                            <li><a href="<%= ResolveUrl("~/common/Motivos.aspx") %>">Motivos alta/baja de puntos</a></li>
                                <li class="nav-header">Archivos</li>
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/Importacion.aspx") %>">Importación</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/GeneracionDat.aspx") %>">Generación de .dat</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/Galicia.aspx") %>">Gestión débito aut. Galicia</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/RendimientoCobrosGalicia.aspx") %>">Rendimiento de cobros en Galicia</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/seguridad/Archivo_Auditar.aspx") %>">Generación de archivos Auditar</a></li>

                                <li class="nav-header">Actividades</li>
                                <li><a href="<%= ResolveUrl("~/common/importarBeneficiarios.aspx") %>">Importacion Masiva</a></li>
                                <li><a href="<%= ResolveUrl("~/common/ActividadesSecretarias.aspx") %>">Secretarias</a></li>
                                <li><a href="<%= ResolveUrl("~/common/ActividadesCategorias.aspx") %>">Categorias Actividades</a></li>
                                <li><a href="<%= ResolveUrl("~/common/ActividadesSubCategorias.aspx") %>">Sub-Categorias Actividades</a></li>           
                                <li><a href="<%= ResolveUrl("~/common/Actividades.aspx") %>">Actividades</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseFour" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-th"></i> Gestión
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseFour">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="nav-header">Favoritos</li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/Transacciones.aspx") %>">Transacciones</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Socios.aspx") %>">Socios</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Comercios.aspx") %>">Comercios</a></li>	
                                <li class="nav-header">Tarjetas</li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/Tarjetas.aspx") %>">Tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/TarjetasSustitucion.aspx") %>">Anular/Sustituir Tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/AltaMasiva.aspx") %>">Alta Masiva de Tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/BajaMasiva.aspx") %>">Baja Masiva de Tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/ReasignacionTarjetas.aspx") %>">Reasignación de tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/common/AsociarTarjetas.aspx") %>">Asociar tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/ReactivarTarjetas.aspx") %>">Reactivar tarjetas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/bins.aspx") %>">BIN</a></li>

                                <li class="nav-header">Avanzadas</li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/msjPushe.aspx") %>">Notificaciones Push</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/historialTermEstab.aspx") %>">Historial de terminales/establecimientos</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/ModificacionPuntosArancel.aspx") %>">Modificacion de puntos y arancel</a></li>
                                <li><a href="<%= ResolveUrl("~/common/importarSocios.aspx") %>">Importar socios</a></li>

                                <li class="nav-header">Verificaciones POS</li>
                                <li><a href="<%= ResolveUrl("~/common/verificacionesPOS.aspx") %>">Nueva planilla</a></li>
                                <li><a href="<%= ResolveUrl("~/common/verificacionesPOSe.aspx") %>">Cargar resultado</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Reporte-Verificaciones.aspx") %>">Verificaciones realizadas</a></li>

                                <li class="nav-header">Otros</li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/mapa.aspx") %>">Mapa</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/sms/sms.aspx") %>">Campañas SMS</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Promociones.aspx") %>">Cupón check</a></li>
                                <li><a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>">Cupones</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/Beneficios.aspx") %>">Beneficios</a></li>
                                <li><a href="<%= ResolveUrl("~/common/Sorteos.aspx") %>">Sorteos</a></li>
                                

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseFive" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-search"></i> Reportes
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseFive">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/dashboard.aspx") %>">Dashboard</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/facturacion.aspx") %>">Facturación</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/facturacion-Detalle.aspx") %>">Facturación detalle</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/Terminales.aspx") %>">Terminales</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/reprogramaciones.aspx") %>">Reprogramaciones</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/rnk-socios.aspx") %>">Ranking socios</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/pines.aspx") %>">Pines</a></li>
                                <li><a href="<%= ResolveUrl("~/common/reporteSMS.aspx") %>">SMS</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/reportes/ReporteTransacciones.aspx") %>">Transacciones</a></li>
                                <li><a href="<%= ResolveUrl("~/common/UsuariosDormidos.aspx") %>">Usuarios Dormidos</a></li>
                                                                <li><a href="<%= ResolveUrl("~/modulos/reportes/rptSorteos.aspx") %>">Sorteos</a></li>

                              
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="pnlContable">
                    <div class="panel-heading">
                        <a href="#collapseSix" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-folder-close"></i> Administración
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseSix">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/movimientos.aspx") %>">Movimientos manuales</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/notas.aspx") %>">ND/NC</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas.aspx") %>">Fact. recurrentes</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas-socios.aspx") %>">Fact. socios</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas-manual.aspx") %>">Fact. manuales</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas-PiePagina.aspx") %>">Fact. pie página</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturasenvio.aspx") %>">Envío de facturas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/pagos.aspx") %>">Pagos recibidos</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/reporteDeEnvios.aspx") %>">Reporte de envios</a></li>
                                <li><a href="<%= ResolveUrl("~/common/FormularioAprobacionTarjetas.aspx") %>">Formulario aprobacion tarjetas</a></li>
                                <li class="nav-header">Liquidaciones</li>
                                <li><a href="<%= ResolveUrl("~/modulos/gestion/Liquidacion-Franquicias.aspx") %>">Generar</a></li>	
                                <li><a href="<%= ResolveUrl("~/common/Liquidaciones.aspx") %>">Listado</a></li>	
                                
                                <li class="nav-header">Proveedores</li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/proveedores.aspx") %>">Proveedores</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/movimientos-prov.aspx") %>">Movimientos manuales</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/notas-prov.aspx") %>">ND/NC</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas-prov.aspx") %>">Facturas</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/pagos-prov.aspx") %>">Pagos recibidos</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/administracion/facturasenvio-prov.aspx") %>">Envío de facturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseSeven" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-shopping-cart"></i> Catálogo
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseSeven">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<%= ResolveUrl("~/modulos/catalogo/premios.aspx") %>">Premios</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/catalogo/pines.aspx") %>">Pines</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#collapseCuponin" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                            <i class="glyphicon glyphicon-phone"></i> Cupon IN
                        </a>
                    </div>
                    <div class="accordion-body collapse" id="collapseCuponin">
                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="<%= ResolveUrl("~/common/keywords.aspx") %>">Keywords</a></li>
                                <li><a href="<%= ResolveUrl("~/modulos/cuponin/consumos.aspx") %>">Consumos</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="push"></div>
        </div>
    </div>
</div>