﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header-marcas.ascx.cs" Inherits="controls_header_marcas" %>
 
<header class="main-header">
    <nav class="navbar navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" href="<%= ResolveUrl("~/marcas/home.aspx") %>"><asp:Label runat="server" ID="lblNombre" /></a>
                <a href="#menu-toggle" class="btn btn-default btn-burger" id="menu-toggle" onclick="marginToggle()">&#9776;</a>
                
                <ul class="nav navbar-nav user_menu pull-right">
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown">Ayuda<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                        </ul>
                    </li>
                     <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tint"></span><b class="caret"></b></a>
                        <ul class="dropdown-menu list-unstyled clearfix">                             
                              <li style="float:left; width: 33.33333%; padding: 5px;">
                                  <a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                      <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                          <span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span>
                                          <span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span>
                                      </div>
                                      <div>
                                          <span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span>
                                          <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>

                                      </div>
                                  </a>
                                  <p class="text-center no-margin">White</p></li>

                                <li style="float:left; width: 33.33333%; padding: 5px;">
                                    <a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                        <div>
                                            <span style="display:block; width: 20%; float: left; height: 7px;background-color:purple;" class="bg-purple-active"></span>
                                            <span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;background-color:purple;"></span>
                                        </div>
                                        <div>
                                            <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
                                            <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
                                        </div>
                                    </a>
                                    <p class="text-center no-margin">Purple</p>
                                </li>
                                <li style="float:left; width: 33.33333%; padding: 5px;">
                                    <a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                        <div>
                                            <span style="display:block; width: 20%; float: left; height: 7px;background-color:green;" class="bg-green-active"></span>
                                            <span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;background-color:green;"></span>

                                        </div>
                                        <div>
                                            <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
                                            <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
                                        </div>
                                    </a>
                                    <p class="text-center no-margin">Green</p>
                                </li>
                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                <a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 7px;background-color:red;" class="bg-red-active"></span>
                                        <span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;background-color:red;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
                                        <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
                                    </div>
                                </a>
                                <p class="text-center no-margin">Red</p>
                            </li>
                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                <a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 7px;background-color:yellow" class="bg-yellow-active"></span>
                                        <span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;background-color:yellow;"></span>
                                               
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
                                        <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
                                    </div>
                                </a>
                                <p class="text-center no-margin">Yellow</p>
                            </li>
                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                <a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span>
                                        <span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;background: #367fa9;"></span>
                                    </div>
                                    <div>
                                        <span style="display:block; width: 20%; float: left; height: 20px; background-color: black;"></span>
                                        <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
                                    </div>
                                </a>
                                <p class="text-center no-margin" style="font-size: 12px">Blue</p>
                            </li>                        
                        </ul>           
                    </li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar" />
                            <asp:Label runat="server" ID="lblUsuario" /><b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/marcas/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/marcas/mis-datos.aspx") %>">Mis datos</a></li>
                            <li runat="server" id="liUsuarios"><a href="<%= ResolveUrl("~/marcas/usuarios.aspx") %>">Administrar usuarios</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login-marcas.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                   
                </ul>
            </div>
        </div>
    </nav>
 
   
</header>

 <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav treeview-menu" id="mobile-nav">
                <li class="treeview" runat="server" id="liDashboard">
                    <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-stats"></span>&nbsp;Dashboard <b class="caret"></b></a>
                     <ul class="treeview-menu nested">
                            <li><a href="<%= ResolveUrl("~/marcas/dashboard.aspx") %>">Generales</a></li>
                         <li><a href="<%= ResolveUrl("~/marcas/dashboard-facturacion.aspx") %>">Facturación</a></li>
                            <li><a href="<%= ResolveUrl("~/marcas/dashboard-tarjetas.aspx") %>">Tarjetas y socios</a></li>
                         <li><a href="<%= ResolveUrl("~/marcas/dashboard-beneficios.aspx") %>">Beneficios</a></li>
                            <li runat="server" id="liDashGift"><a href="<%= ResolveUrl("~/marcas/dashboard-gift.aspx") %>">Giftcards</a></li>
                         <li runat="server" id="liDashMarketing"><a href="<%= ResolveUrl("~/marcas/dashboard-marketing.aspx") %>">Marketing</a></li>
                         <li runat="server" id="liMapa">
                        <a href="<%= ResolveUrl("~/marcas/mapa.aspx") %>"><%--<span class="glyphicon glyphicon-map-marker"></span>--%>Geolocalización</a>
                </li>
                    </ul>
                </li>
                <li class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-user"></span>&nbsp;Transacciones <b class="caret"></b></a>
                        <ul class="treeview-menu nested" id="liTransacciones" runat="server">
                            <li><a href="<%= ResolveUrl("~/marcas/transacciones.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/importarTransacciones.aspx") %>">Importar Transacciones</a></li>
                        </ul>
                </li>
                <li class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-user"></span>&nbsp;Socios <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li><a href="<%= ResolveUrl("~/common/Socios.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Socios-alta.aspx") %>">Nuevo</a></li>
                            <%--                            <li><a href="<%= ResolveUrl("~/common/BajaSocio.aspx") %>">Baja</a></li>	--%>
                            <li runat="server" id="liTranPunt"><a href="<%= ResolveUrl("~/common/TransferirPuntos.aspx") %>">Transferir Puntos</a></li>
                            <li id="liUsuariosDormidos"><a href="<%= ResolveUrl("~/common/UsuariosDormidos.aspx") %>">Usuarios Dormidos</a></li>
                            <li><a href="<%= ResolveUrl("~/common/importarSocios.aspx") %>">Importar socios</a></li>
                            <li><a href="<%= ResolveUrl("~/common/RankingSocios.aspx") %>">Ranking socios</a></li>
                        </ul>
                </li>
                <li runat="server" id="li4" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;Tarjetas <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li><a href="<%= ResolveUrl("~/marcas/tarjetas.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/TarjetasAnulacion.aspx") %>">Anular/Sustituir</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Motivos.aspx") %>">Motivos alta/baja de puntos</a></li>
                        </ul>
                </li>
                <li runat="server" id="liComercios">
                        <a href="<%= ResolveUrl("~/marcas/Comercios.aspx") %>"><span class="glyphicon glyphicon-book"></span>&nbsp;Comercios</a>
                </li>
                <li runat="server" id="liFacturacion">
                        <a href="<%= ResolveUrl("~/marcas/facturas.aspx") %>"><span class="glyphicon glyphicon-book"></span>&nbsp;Facturación</a>
                </li>
              
                <li runat="server" id="liAlertas" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;Alertas <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li id="li2" runat="server"><a href="<%= ResolveUrl("~/marcas/alertas-config.aspx") %>">Configuración de Alertas</a></li>
                            <li id="li3" runat="server"><a href="<%= ResolveUrl("~/marcas/alertas.aspx") %>">Alertas</a></li>
                            <li id="li1" runat="server"><a href="<%= ResolveUrl("~/marcas/alertas-seguimiento.aspx") %>">Tarjetas en seguimiento</a></li>
                        </ul>
                </li>
                <li id="liPos" runat="server" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-flag"></span> POS <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li id="liPosWeb" runat="server"><a href="<%= ResolveUrl("~/marcas/posweb.aspx") %>">POS Web</a></li>
                            <li id="ligiftWeb" runat="server"><a href="<%= ResolveUrl("~/common/giftweb.aspx") %>">Gift Web</a></li>
                        </ul>
                </li>
                <li runat="server" id="liMarketing" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Marketing <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li class="treeview">
                                <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-envelope"></span>&nbsp;SMS <b class="caret"></b></a>
                                <ul class="treeview-menu nested">
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/sms.aspx") %>">Campañas SMS</a>
                                    </li>
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/sms-bienvenida.aspx") %>">Mensaje de bienvenida</a>
                                    </li>
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/sms-cumpleanios.aspx") %>">Mensaje de cumpleaños</a>
                                    </li>
                                    <li>
                                        <a href="<%= ResolveUrl("~/common/reporteSMS.aspx") %>">Reporte SMS</a>
                                    </li>
                                      <li>
                                <a href="<%= ResolveUrl("~/common/Keywords.aspx") %>">Keywords</a>
                            </li>
                                </ul>
                            </li>                           
                          
                            <li class="treeview">
                                <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-inbox"></span>&nbsp;Email <b class="caret"></b></a>
                                <ul class="treeview-menu nested">
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/email-socio.aspx") %>">Email bienvenida a socios</a>
                                    </li>
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/email-cumpleanios.aspx") %>">Email cumpleaños a socios</a>
                                    </li>
                                    <li>
                                        <a href="<%= ResolveUrl("~/marcas/email-comercio.aspx") %>">Email bienvenida a comercios</a>
                                    </li>
                                </ul>         
                            </li>
                            <li><a href="<%= ResolveUrl("~/common/Sorteos.aspx") %>">Sorteos</a></li>
                            <li><a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>">Promociones</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Promociones.aspx") %>">Cupón check</a></li>
                        </ul>
                    </li>
                    <li runat="server" id="liProductos" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Productos <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li><a href="<%= ResolveUrl("~/marcas/productos.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/marcas/productose.aspx") %>">Nuevo</a></li>
                            <li><a href="<%= ResolveUrl("~/marcas/familias.aspx") %>">Familias</a></li>
                            <li><a href="<%= ResolveUrl("~/marcas/stocke.aspx") %>">Editar stock masivo</a></li>
                        </ul>
                    </li>

                    <li runat="server" id="liCatalogo" class="treeview">
                        <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Catálogo <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li id="liPremios" runat="server"><a href="<%= ResolveUrl("~/marcas/premios.aspx") %>">Adm de Premios</a></li>
                            <li id="liCanje" runat="server"><a href="<%= ResolveUrl("~/marcas/catalogo.aspx") %>">Canje</a></li>
                            <li id="liSeguimiento" runat="server"><a href="<%= ResolveUrl("~/marcas/canjes.aspx") %>">Seguimiento de canjes</a></li>
                            <li id="liEstadosMasivos" runat="server"><a href="<%= ResolveUrl("~/marcas/cambiar-estados-masivo.aspx") %>">Cambiar estado masivo</a></li>
                        </ul>
                    </li>

                    <li runat="server" id="li5" class="treeview">
                         <a data-toggle="treeview" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Integraciones <b class="caret"></b></a>
                        <ul class="treeview-menu nested">
                            <li id="liDokka" runat="server"><a href="<%= ResolveUrl("~/marcas/dokka.aspx") %>">Doka</a></li>
                        </ul>
                    </li>              
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
    </div>