﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header-comercios.ascx.cs" Inherits="controls_header_comercios" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" style="width:400px" href="<%= ResolveUrl("~/comercios/home.aspx") %>"><asp:Label runat="server" ID="lblNombre" /></a>
                <ul class="nav navbar-nav" id="mobile-nav">
					<li runat="server" id="liDashboard" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Dashboard <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/comercios/dashboard.aspx") %>">Tarjetas</a></li>
                            <li runat="server" id="liDashGift"><a href="<%= ResolveUrl("~/comercios/dashboard-gift.aspx") %>">Giftcards</a></li>
                        </ul>
                    </li>
					<li runat="server" id="liTransacciones">
						<a href="<%= ResolveUrl("~/comercios/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span> Transacciones</a>	
                    </li>
                    <li runat="server" id="liFacturacion">
						<a href="<%= ResolveUrl("~/comercios/facturas.aspx") %>"><span class="glyphicon glyphicon-book"></span> Facturación</a>	
                    </li>
                    <li runat="server" id="liPosWeb">
						<a href="<%= ResolveUrl("~/comercios/posweb.aspx") %>"><span class="glyphicon glyphicon-flag"></span> POS Web</a>	
                    </li>
                    <li runat="server" id="liEncuestas">
						<a href="<%= ResolveUrl("~/comercios/encuestas.aspx") %>"><span class="glyphicon glyphicon-file"></span>Encuestas</a>	
                    </li>
                    <li runat="server" id="liGiftweb">
						<a href="<%= ResolveUrl("~/comercios/giftweb.aspx") %>"><span class="glyphicon glyphicon-gift"></span> Gift Web</a>	
                    </li>
                    <li runat="server" id="liAlertas" class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-warning-sign"></span> Alertas <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li id="li2" runat="server"><a href="<%= ResolveUrl("~/comercios/alertas-config.aspx") %>">Configuración de Alertas</a></li>
                            <li id="li3" runat="server"><a href="<%= ResolveUrl("~/comercios/alertas.aspx") %>">Alertas</a></li>
                            <li id="li1" runat="server"><a href="<%= ResolveUrl("~/comercios/alertas-seguimiento.aspx") %>">Tarjetas en seguimiento</a></li>
                        </ul>
                    </li>
                    <li runat="server" id="liContrato">
						<asp:HyperLink runat="server" id="lnkContrato" download>
                            <span class="glyphicon glyphicon-file"></span> Contrato
						</asp:HyperLink>
                    </li>
					<li runat="server" id="liPromos">
						<a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span> Cupones</a>	
                    </li>
				</ul>

                <ul class="nav navbar-nav user_menu pull-right">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown">Ayuda<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                        </ul>
                    </li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar"/><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/comercios/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/comercios/mis-datos.aspx") %>">Mis datos</a></li>
                            <li runat="server" id="liUsuarios"><a href="<%= ResolveUrl("~/comercios/usuarios.aspx") %>">Administrar usuarios</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login-comercios.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>