﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_header_marcas : System.Web.UI.UserControl {

    protected void Page_Load(object sender, EventArgs e) {
        if (Session["CurrentMarcasUser"] != null) {
            var oUsuario = (WebMarcasUser)Session["CurrentMarcasUser"];
            this.lblUsuario.Text = oUsuario.Usuario;
            lblNombre.Text = oUsuario.Marca;            

            if (oUsuario.Tipo != "D") {
                if (oUsuario.Tipo == "B")
                {
                    liFacturacion.Visible = liUsuarios.Visible = liAlertas.Visible = false;
                    liCatalogo.Visible = false;
                    liComercios.Visible = false;
                    liMarketing.Visible = false;
                }
                else
                {
                    liDashGift.Visible = oUsuario.POSWeb;

                }

                if (oUsuario.IDMarca == 22)
                    liFacturacion.Visible = false;

                liCatalogo.Visible = oUsuario.Catalogo;
                liPosWeb.Visible = oUsuario.POSWeb;
               
            }
            else {   //Si es dataentry               
                liFacturacion.Visible = liCatalogo.Visible = liPosWeb.Visible = false;
                liAlertas.Visible = false;
            }

            if (oUsuario.Tipo != "A") {//Si NO es ADMIN (ES BACKOFFICE o DATAENTRY)
                liEstadosMasivos.Visible = liFacturacion.Visible = liUsuarios.Visible = liPremios.Visible = liSeguimiento.Visible = false;
                liDashboard.Visible = liDashGift.Visible = liTransacciones.Visible = liMapa.Visible = false;
                //liSMSCumpleanios.Visible = liSMSBievenida.Visible = liSMSCampanias.Visible = false;
                //liEmailRegistroComercio.Visible = liEmailRegistroSocio.Visible = liEmailCumpleanios.Visible = false;
                
            }
            else {
                liEstadosMasivos.Visible = liFacturacion.Visible = liUsuarios.Visible = liPremios.Visible = liCanje.Visible = liSeguimiento.Visible = true;
                liDashboard.Visible = liDashGift.Visible = liTransacciones.Visible = liMapa.Visible = true;
                //liSMSCumpleanios.Visible = liSMSBievenida.Visible = liSMSCampanias.Visible = true;
                //liEmailRegistroComercio.Visible = liEmailRegistroSocio.Visible = liEmailCumpleanios.Visible = true;
            }

            using (var dbContext = new ACHEEntities()) {
                var marca = dbContext.Marcas.Where(x => x.IDMarca == oUsuario.IDMarca).FirstOrDefault();
                if(!marca.HabilitarGiftcard)
                    ligiftWeb.Visible = false;

                if (ligiftWeb.Visible == false && liPosWeb.Visible == false)
                    liPos.Visible = false;

                if (oUsuario.Tipo != "A" || !marca.MostrarProductos) {
                    liProductos.Visible = false;
                }
                if (marca.TipoCatalogo != "C") {
                    liTranPunt.Visible = false;
                }
                if (marca.TipoCatalogo == "C") {
                    liPremios.Visible = false;
                }
            }
        }
    }
}