﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header-multimarcas.ascx.cs" Inherits="controls_header_multimarcas" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" href="<%= ResolveUrl("~/multimarcas/home.aspx") %>">
                    <asp:Label runat="server" ID="lblNombre" /></a>
                <ul class="nav navbar-nav" id="mobile-nav">
<%--                    <li runat="server" id="liDashboard" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-signal"></span>&nbsp;Dashboard <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/marcas/dashboard.aspx") %>">Generales</a></li>
                                <li><a href="<%= ResolveUrl("~/marcas/dashboard-tarjetas.aspx") %>">Tarjetas</a></li>
                            <li runat="server" id="liDashGift"><a href="<%= ResolveUrl("~/marcas/dashboard-gift.aspx") %>">Giftcards</a></li>                            
                        </ul>
                    </li>--%>
                    <li runat="server" id="liTransacciones">
                        <a href="<%= ResolveUrl("~/multimarcas/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Transacciones</a>
                    </li>
                    <li class="dropdown" id="liSocios">
                        <a href="<%= ResolveUrl("~/common/Socios.aspx") %>"><span class="glyphicon glyphicon-user"></span> &nbsp; Socios</a>
                    </li>
                    <li runat="server" id="liComercios" >
                        <a href="<%= ResolveUrl("~/multimarcas/Comercios.aspx") %>"><span class="glyphicon glyphicon-book" style="right:10px;"></span>&nbsp;Comercios</a>
                    </li>
<%--                    <li runat="server" id="liMapa" >
                        <a href="<%= ResolveUrl("~/marcas/mapa.aspx") %>"><span class="glyphicon glyphicon-map-marker" style="right:10px;"></span>&nbsp;Mapa</a>
                    </li>--%>
                    <li runat="server" id="liTarjetas">
                        <a href="<%= ResolveUrl("~/multimarcas/tarjetas.aspx") %>"><span class="glyphicon glyphicon-book"></span>&nbsp;Tarjetas</a>
                    </li>           
                </ul>

                <ul class="nav navbar-nav user_menu pull-right">
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav_condensed" style="display:none" data-toggle="dropdown">Ayuda<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                        </ul>
                    </li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar" /><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/multimarcas/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/multimarcas/mis-datos.aspx") %>">Mis datos</a></li>
                            <li runat="server" id="liUsuarios"><a href="<%= ResolveUrl("~/marcas/usuarios.aspx") %>">Administrar usuarios</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login-multimarcas.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
