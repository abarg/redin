﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_header_comercios : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentComerciosUser"] != null)
        {
            var oUsuario = (WebComerciosUser)Session["CurrentComerciosUser"];
            this.lblUsuario.Text = oUsuario.Usuario;
            lblNombre.Text = oUsuario.Comercio;

            if (oUsuario.Tipo == "B")
            {
                liFacturacion.Visible = liDashboard.Visible = liTransacciones.Visible = liUsuarios.Visible = false;
                liDashGift.Visible = liAlertas.Visible = false;
            }
            else
                liDashGift.Visible = oUsuario.POSWeb;

            liPosWeb.Visible = oUsuario.POSWeb;
            liGiftweb.Visible = oUsuario.GiftWeb;

            if (!string.IsNullOrEmpty(oUsuario.FichaAlta))
                lnkContrato.NavigateUrl = "/files/fichas/" + oUsuario.FichaAlta;
            else
                liContrato.Visible = false;

            using (var dbContext = new ACHEEntities())
            {
                var comercio = dbContext.Comercios.First(x => x.IDComercio == oUsuario.IDComercio);
                if (comercio.IDMarca == 171 || comercio.IDMarca == 175 || comercio.IDMarca == 174 || comercio.IDMarca == 170 || comercio.IDMarca == 169)
                    liFacturacion.Visible = false;
            }
        }
    }
}