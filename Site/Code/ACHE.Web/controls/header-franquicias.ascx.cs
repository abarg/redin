﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_header_franquicias : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentFranquiciasUser"] != null)
        {
            var oUsuario = (WebFranquiciasUser)Session["CurrentFranquiciasUser"];
            this.lblUsuario.Text = oUsuario.Usuario;
            lblNombre.Text = oUsuario.Franquicia;

            if (oUsuario.Tipo == "B")
            {
                liDashboard.Visible = liTransacciones.Visible = liMapa.Visible = false;
            }
        }
    }
}