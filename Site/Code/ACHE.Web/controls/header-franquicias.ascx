﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header-franquicias.ascx.cs" Inherits="controls_header_franquicias" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" href="<%= ResolveUrl("~/franquicias/home.aspx") %>">
                    <asp:Label runat="server" ID="lblNombre" /></a>
                <ul  style="padding-left: 0px;" class="nav navbar-nav" id="mobile-nav">
                    <li runat="server" id="liDashboard" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Reportes <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Dashboard-franquicias.aspx") %>">Dashboard tarjetas</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Dashboard-PlusIN.aspx") %>">Dashboard Plus IN</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/dashboard-gift.aspx") %>">Dashboard giftcards</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/rpt-terminales.aspx") %>">Estado terminales</a></li>

                        </ul>
                    </li>
                    <%--<li >
                        <a href="<%= ResolveUrl("~/franquicias/dashboard.aspx") %>"><span class="glyphicon glyphicon-signal"></span> Dashboard</a>	
                    </li>--%>
                    <li runat="server" id="liTransacciones">
                        <a href="<%= ResolveUrl("~/franquicias/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span>Transacciones</a>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-user"></span> Socios <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Socios.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Socios-alta.aspx") %>">Nuevo</a></li>
                            <li id="liUsuariosDormidos"><a href="<%= ResolveUrl("~/common/UsuariosDormidos.aspx") %>">Usuarios Dormidos</a></li>
                           <li><a href="<%= ResolveUrl("~/common/importarSocios.aspx") %>">Importar socios</a></li> 
                            <li><a href="<%= ResolveUrl("~/common/RankingSocios.aspx") %>">Ranking socios</a></li> 

                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Comercios <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Comercios.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Comerciose.aspx") %>">Nuevo</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Terminales.aspx") %>">Terminales</a></li> 
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Marcas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Marcas.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Marcase.aspx") %>">Nuevo</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Sorteos.aspx") %>">Sorteos</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Multimarcas.aspx") %>">Multimarcas</a></li>
                        </ul>
                    </li>
                    <li runat="server" id="liMapa">
                        <a href="<%= ResolveUrl("~/franquicias/mapa.aspx") %>"><span class="glyphicon glyphicon-map-marker"></span> Mapa</a>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Facturación <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/franquicias/rpt-facturacion.aspx") %>">Facturación</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/rpt-facturacion-detalle.aspx") %>">Facturación Detalle</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/HistoricoFacturacion.aspx") %>">Historico facturación</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Liquidaciones.aspx") %>">Liquidaciones</a></li>
                        </ul>
                    </li>

                    <%--<li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Liquidaciones <b class="caret"></b></a>
						<ul class="dropdown-menu">
                             
              
                        </ul>
                    </li>--%>
 <%--                   <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Verificaciones POS <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/verificacionesPOS.aspx") %>">Nueva planilla</a></li>
                            <li><a href="<%= ResolveUrl("~/common/verificacionesPOSe.aspx") %>">Cargar resultado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Reporte-Verificaciones.aspx") %>">Ver todas</a></li>
                        </ul>
                    </li>--%>

                   <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Actividades <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/ImportarBeneficiarios.aspx") %>">Importacion Masiva</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Dashboard-actividades.aspx") %>">Dashboard Beneficiarios</a></li>
                            <li><a href="<%= ResolveUrl("~/common/ListadoBeneficiarios.aspx") %>">Listado Beneficiarios</a></li> 
                            <li><a href="<%= ResolveUrl("~/common/ActividadesSecretarias.aspx") %>">Listado Secretarias</a></li> 
                            <li><a href="<%= ResolveUrl("~/common/ActividadesCategorias.aspx") %>">Listado Categorias</a></li> 
                            <li><a href="<%= ResolveUrl("~/common/ActividadesSubCategorias.aspx") %>">Listado Subcategorias</a></li> 
                            <li><a href="<%= ResolveUrl("~/common/Actividades.aspx") %>">Listado Actividades</a></li> 
                        </ul>
                    </li>
                     <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Formulario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/FormularioAprobacionTarjetas.aspx") %>">Listado</a></li>
                        </ul>
                    </li>
                    <li runat="server" id="li7" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="padding-left: 0px;"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Beneficios <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Promociones.aspx") %>">Promociones</a></li>
                        </ul>
                    </li>   
                    <li runat="server" id="liAlertas" class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-warning-sign"></span> Alertas <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li id="li2" runat="server"><a href="<%= ResolveUrl("~/common/alertas-config.aspx") %>">Configuración de Alertas</a></li>
                            <li id="li3" runat="server"><a href="<%= ResolveUrl("~/common/alertas.aspx") %>">Alertas</a></li>
                            <li id="li1" runat="server"><a href="<%= ResolveUrl("~/common/alertas-seguimiento.aspx") %>">Tarjetas en seguimiento</a></li>
                        </ul>
                    </li>
                    <li>
                        <div id="click2call" align="center">
                            <a id="click2call_callbtn">
                                <img src="http://webrtc.anura.com.ar/click2call/img/click2call.png" />
                            </a>

                            <a id="click2call_hupbtn">
                                <img src="https://webrtc.anura.com.ar/click2call/img/phone_hang.png" />
                            </a>
                            <div id="click2call_msgdiv"></div>

                            <div style="visibility: hidden; display: none;">
                                <input id="click2call_user" value="103" />
                                <input id="click2call_domain" value="pounamu.grancentral.com.ar" />
                                <input id="click2call_password" value="103@7e37" />
                                <input id="click2call_number" value="910" />
                                <input id="click2call_host" value="wss://webrtc.anura.com.ar:9084" />
                            </div>

                            <div id="media" style="visibility: hidden; display: none;">
                                <video width="800" id="webcam" autoplay="autoplay" hidden="true"></video>
                            </div>
                        </div>

                    </li>
                    <%--<li>
						<a href="<%= ResolveUrl("~/marcas/facturas.aspx") %>"><span class="glyphicon glyphicon-book"></span> Facturas</a>	
                    </li>--%>
                </ul>



                <ul class="nav navbar-nav user_menu pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown">Ayuda<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/franquicias/Ticket.aspx") %>">Tickets</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/Ticketn.aspx") %>">Nuevo ticket</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/fileexplorer.aspx") %>">File Explorer</a></li>
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                        </ul>
                    </li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar" /><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/franquicias/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/franquicias/mis-datos.aspx") %>">Mis datos</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login-franquicias.aspx?logOut=true") %>">Cerrar sesión</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
