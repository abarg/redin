﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="controls_header" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" href="<%= ResolveUrl("~/home.aspx") %>">Red In</a>
                <ul class="nav navbar-nav" id="mobile-nav">
					<li id="liDashboard" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-signal"></span> Reportes <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/dashboard.aspx") %>">Dashboard otros</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/dashboard-fact.aspx") %>">Dashboard Facturación</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/dashboard-gift.aspx") %>">Dashboard Giftcard</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/dashboard-tarjetas.aspx") %>">Dashboard Tarjetas</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/rptTarjetasImpresas.aspx") %>">Reporte Tarjetas Impresas</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Dashboard-franquicias.aspx") %>">Dashboard Franquicias</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Dashboard-PlusIN.aspx") %>">Dashboard Plus IN</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/terminales.aspx") %>">Estado terminales</a></li>	
                            <li><a href="<%= ResolveUrl("~/modulos/reportes/rnk-socios.aspx") %>">Ranking socios</a></li>
                            
                        </ul>
                    </li>
                    <li>
						<a href="<%= ResolveUrl("~/modulos/gestion/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span> Transacciones</a>	
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-user"></span> Socios <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Socios.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Socios-alta.aspx") %>">Nuevo</a></li>	
                            <li><a href="<%= ResolveUrl("~/common/Socios-Unificar.aspx") %>">Unificar</a></li>
                            <li><a href="<%= ResolveUrl("~/common/TransferirPuntos.aspx") %>">Transferir Puntos</a></li>		
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Comercios <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/common/Comercios.aspx") %>">Listado</a></li>
                            <li><a href="<%= ResolveUrl("~/common/Comerciose.aspx") %>">Nuevo</a></li>	
                            <li><a href="<%= ResolveUrl("~/common/Terminales.aspx") %>">Terminales</a></li> 	
                            <li><a href="<%= ResolveUrl("~/common/PromocionesPuntuales.aspx") %>">Cupones</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/gestion/ArancelPuntosMarcase.aspx") %>">Actualizacion de arancel, puntos y descuentos por marcas</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/gestion/Arancel-puntos-empresase.aspx") %>">Actualizacion de arancel, puntos y descuentos por empresas</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/gestion/DuplicarComercio.aspx") %>">Duplicar Comercio</a></li>

                        </ul>
                    </li>
                    <li>
						<a href="<%= ResolveUrl("~/modulos/gestion/fileexplorer.aspx") %>"><span class="glyphicon glyphicon-folder-open"></span> File Explorer</a>	
                    </li>
                    <li>
						<a href="<%= ResolveUrl("~/modulos/gestion/mapa.aspx") %>"><span class="glyphicon glyphicon-map-marker"></span> Mapa</a>	
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-flag"></span> POS <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/modulos/gestion/posweb.aspx") %>"><span class="glyphicon glyphicon-flag"></span> POS Web</a>	</li>
                            <li><a href="<%= ResolveUrl("~/modulos/gestion/giftweb.aspx") %>"><span class="glyphicon glyphicon-gift"></span> Gift Web</a></li>
                        </ul>
                    </li>                    
                     <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-book"></span> Facturación <b class="caret"></b></a>
						<ul class="dropdown-menu">
                             <li><a href="<%= ResolveUrl("~/modulos/reportes/facturacion.aspx") %>">Facturación</a></li>	
                             <li><a href="<%= ResolveUrl("~/modulos/reportes/facturacion-detalle.aspx") %>">Facturación Detalle</a></li>	
                             <li><a href="<%= ResolveUrl("~/modulos/reportes/HistoricoFacturacion.aspx") %>">Historico facturación</a></li>	
                             <li><a href="<%= ResolveUrl("~/modulos/reportes/cc-general.aspx") %>">Cuenta corriente</a></li>	
                             <li><a href="<%= ResolveUrl("~/modulos/reportes/reporte-saldo.aspx") %>">Saldos de Facturación</a></li>
                             <li><a href="<%= ResolveUrl("~/modulos/gestion/Liquidacion-Franquicias.aspx") %>">Generar Liquidaciones</a></li>
                             <li><a href="<%= ResolveUrl("~/common/Liquidaciones.aspx") %>">Liquidaciones</a></li>	
                        </ul>
                    </li>
                    <li runat="server" id="liAlertas" class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-warning-sign"></span> Alertas <b class="caret"></b></a>
						<ul class="dropdown-menu">
                            <li id="li2" runat="server"><a href="<%= ResolveUrl("~/common/alertas-config.aspx") %>">Configuración de Alertas</a></li>
                            <li id="li3" runat="server"><a href="<%= ResolveUrl("~/common/alertas.aspx") %>">Alertas</a></li>
                            <li id="li1" runat="server"><a href="<%= ResolveUrl("~/common/alertas-seguimiento.aspx") %>">Tarjetas en seguimiento</a></li>
                        </ul>
                    </li>
				</ul>
                <ul class="nav navbar-nav user_menu pull-right">
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
						<a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown">
                            Ayuda<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<%= ResolveUrl("~/modulos/tickets/Ticket.aspx") %>">Tickets</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/tickets/Ticketn.aspx") %>">Nuevo ticket</a></li>
                            <li><a id="lnkPuntos" href="/ayuda/puntos.jpg" target="_blank">Equivalencia puntos</a></li>
                            <li><a href="<%= ResolveUrl("~/common/ManualAyuda.aspx") %>">Manual de ayuda</a></li>
                            <li><a href="<%= ResolveUrl("~/modulos/administracion/seccion-manual-ayuda.aspx") %>">Edición del manual ayuda</a></li>

						</ul>
					</li>
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/user_avatar.png" CssClass="user_avatar"/><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/modulos/seguridad/MisDatos.aspx") %>">Mis datos</a></li>
                            <li class="divider"></li>
                            <li><a href="<%= ResolveUrl("~/login.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>