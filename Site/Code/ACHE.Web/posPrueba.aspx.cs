﻿using ACHE.Model;
using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Services.Protocols;
using System.Configuration;
using ACHE.Business;


public partial class posPrueba : System.Web.UI.Page
{
    public enum ErrorIntegracion {
        SessionIDInvalido = -1,
        NroTarjetaInvalido = -100,
        ComercioNoEncontrado = -101,
        TransaccionAnulada = -102,
        TransaccionNoEncontrada = -103,
        ProductoNoEncontrado = -104,
        TicketUtilizadoPreviamente = -105,
        TransaccionReversada = -106,
        OperacionDeTransaccionInvalida = -107,
        NroTicketInexistente = -108,
        MontoInsuficiente = -109,
        EmpresaCelularIncorrecta = -111,
        FormatoCelularIncorrecto = -112,
        PuntosImporteInsuficientes = -113,
        TipoCanjeIncorrecto = -114,
        ValorACanjearIncorrecto = -115,
        PremioNoDisponible = -116,
        SorteoNoPermitido = -117,
        ComercioSinMarca = -118,
        MarcaInexistente = -119,
        SorteoInexistente = -120,
        PromocionNoEncontrada = -121
    }

    protected void Page_Load(object sender, EventArgs e)
    { 
        NewTransactionRequest datos = new NewTransactionRequest();
        datos.sessionID = "77817960-c1c0-4d7d-9be2-a0404d8e6b32";
        datos.terminal = "32761029";
        datos.cardNumber = "6371170307000017";

        SorteoRequest datosSorteo= new SorteoRequest();
        datosSorteo.sessionID = "77817960-c1c0-4d7d-9be2-a0404d8e6b32";
        datosSorteo.terminal = "32761029";
        datosSorteo.cardNumber = "6371170304000010";
        //Sorteo(datosSorteo);
    //NewTransaction(datos);
        GetShopInfoRequest datosShop= new GetShopInfoRequest();
        datosShop.sessionID = "77817960-c1c0-4d7d-9be2-a0404d8e6b32";
        datosShop.terminal = "32761029";
        GetShopInfo(datosShop);
    }

    private void RenovarFechaValida(string sessionID, ACHEEntities dbContext) {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null) {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }

    private bool ValidarSessionID(string sessionID, ACHEEntities dbContext) {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null) {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
           // if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    public NewTransactionResponse NewTransaction(NewTransactionRequest datos) {
        NewTransactionResponse info = new NewTransactionResponse();
        using (var dbContext = new ACHEEntities()) {
            if (ValidarSessionID(datos.sessionID, dbContext)) {
                RenovarFechaValida(datos.sessionID, dbContext);

                var POSTerminal = datos.terminal;
                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == POSTerminal).FirstOrDefault();
                if (terminal != null) {

                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                    if (oTarjeta != null) {
                        bComercio bComercio = new bComercio();
                        Transacciones tr = new Transacciones();
                        tr.FechaTransaccion = DateTime.Now;
                        tr.Origen = "POS";
                        tr.CodigoPremio = "";
                        tr.Descripcion = datos.observation;
                        int descuento = bComercio.obtenerDescuento(terminal, dbContext, oTarjeta);
                        tr.Descuento = descuento;
                        tr.Importe = datos.totalPrice;
                        if (descuento > 0)
                            tr.ImporteAhorro = (datos.totalPrice * descuento) / 100;
                        else
                            tr.ImporteAhorro = 0;
                        tr.NumCupon = "";
                        tr.NumEst = terminal.NumEst.PadLeft(15, '0');
                        tr.NumReferencia = "";
                        tr.NumRefOriginal = "";
                        tr.NumTarjetaCliente = datos.cardNumber;
                        tr.NumTerminal = datos.terminal;
                        tr.Operacion = "Venta";
                        //tr.PuntosAContabilizar = (int)(datos.totalPrice - tr.ImporteAhorro);

                        int puntosAContabilizar = (int)(tr.Importe - tr.ImporteAhorro);
                        decimal POSpuntos = bComercio.obtenerPuntos(terminal, oTarjeta, dbContext);
                        tr.Puntos = POSpuntos;
                        tr.PuntosAContabilizar = puntosAContabilizar * bComercio.obtenerMulPuntos(terminal, dbContext, tr.NumTarjetaCliente);

                        tr.PuntosDisponibles = "000000000000";
                        tr.PuntosIngresados = (datos.totalPrice.ToString().Replace(",", "")).PadLeft(12, '0');
                        tr.TipoMensaje = "1100";
                        tr.PuntoDeVenta = "";
                        tr.NroComprobante = datos.ticket;
                        tr.TipoComprobante = "Ticket";
                        tr.TipoTransaccion = "000000";
                        tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                        tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, oTarjeta);
                        tr.IDMarca = terminal.Comercios.IDMarca;
                        tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                        tr.Usuario = "POS";
                        //dbContext.Transacciones.Add(tr);

                        tr.TransaccionesDetalle = new List<TransaccionesDetalle>();

                        bool isValid = true;



                        foreach (var product in datos.products) {
                            var p = dbContext.Productos.Where(x => x.IDProducto == product.productID && x.IDMarca == terminal.Comercios.IDMarca);
                            if (p.Any()) {
                                TransaccionesDetalle trDetalle = new TransaccionesDetalle();
                                trDetalle.IDProducto = product.productID;
                                trDetalle.Precio = product.price;
                                trDetalle.Cantidad = product.quantity;
                                tr.TransaccionesDetalle.Add(trDetalle);

                                //dbContext.TransaccionesDetalle.Add(trDetalle);
                            }
                            else {
                                isValid = false;
                            }
                        }

                        if (isValid) {
                            dbContext.Transacciones.Add(tr);
                            dbContext.SaveChanges();
                            dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                            info.SessionID = datos.sessionID;
                            info.AnswerCode = 0;
                            info.Discount = tr.Descuento ?? 0;
                            info.DiscountAmount = tr.ImporteAhorro ?? 0;
                            info.Points = tr.PuntosAContabilizar ?? 0;
                            info.TotalPoints = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).Select(x => x.PuntosTotales).FirstOrDefault();
                            info.NetworkCost = tr.UsoRed;
                            info.Credits = tr.Importe ?? 0;
                            info.IdTransaction = tr.IDTransaccion;
                            info.TotalFinal = info.Credits - info.DiscountAmount + info.NetworkCost;
                            //agregar fecha a validar ( no se muestra la promo) no se tira error si no esta en la fecha

                            var promo = dbContext.Promociones.Where(x => x.IDMarca == terminal.Comercios.IDMarca).FirstOrDefault();

                                if (promo != null && promo.FechaHasta >= DateTime.Now && promo.FechaDesde <= promo.FechaDesde) {
                                //info.Titulo = promo.Titulo;
                                //info.Mensaje1 = promo.Mensaje1;
                                //info.Mensaje2 = promo.Mensaje2;
                                //info.Mensaje3 = promo.Mensaje3;
                                //info.Mensaje4 = promo.Mensaje4;
                                //info.TipoCodigo = promo.TipoCodigo;
                                //info.InformacionACodificar = promo.InformacionACodificar;
                                //info.FechaDesde = promo.FechaDesde;
                                //info.FechaHasta = promo.FechaHasta;
                            }
                            else {
                                info.AnswerCode = (int)ErrorIntegracion.PromocionNoEncontrada;
                            }
                            
                        }
                        else {
                            info.AnswerCode = (int)ErrorIntegracion.ProductoNoEncontrado;
                        }
                    }
                    else {
                        info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
                //}
            }
            else {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public LotResponse Lot(LotRequest datos) {
        LotResponse info = new LotResponse();
        using (var dbContext = new ACHEEntities()) {
            if (ValidarSessionID(datos.sessionID, dbContext)) {
                RenovarFechaValida(datos.sessionID, dbContext);

                string posTerminal = datos.terminal;
                var terminal = dbContext.Terminales.Where(x => x.POSTerminal == posTerminal).FirstOrDefault();
                if (terminal != null) {

                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, true);
                    if (oTarjeta != null && oTarjeta.FechaBaja == null) {
                        bComercio bComercio = new bComercio();

                        var sorteo = dbContext.Sorteos.Where(x => x.IDMarca == terminal.Comercios.IDMarca.Value && x.Activo).FirstOrDefault();

                        if (sorteo != null && sorteo.FechaHasta >= DateTime.Now && sorteo.FechaDesde <= sorteo.FechaDesde) {
                            TransaccionesSorteos trSorteo = new TransaccionesSorteos();
                            trSorteo.IDSorteo = sorteo.IDSorteo;
                            trSorteo.IDTarjeta = oTarjeta.IDTarjeta;
                            trSorteo.Fecha = DateTime.Now;
                            dbContext.TransaccionesSorteos.Add(trSorteo);
                            dbContext.SaveChanges();
                            info.sessionID = datos.sessionID;

                            info.Title = sorteo.Titulo;
                            info.Message1 = sorteo.Mensaje1;
                            info.Message2 = sorteo.Mensaje2;
                            info.Message3 = sorteo.Mensaje3;
                            info.Message4 = sorteo.Mensaje4;
                            info.CardNumber = oTarjeta.Numero;
                            info.DateFrom = sorteo.FechaDesde.ToString();
                            info.DateUp = sorteo.FechaHasta.ToString();
                            info.Customer = oTarjeta.Socios.Nombre +"  "  + oTarjeta.Socios.Apellido;
                            
                        }
                        else {
                            info.AnswerCode = (int)ErrorIntegracion.SorteoInexistente;
                        }
                    }
                    else {
                        info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public GetShopInfoResponse GetShopInfo(GetShopInfoRequest datos) {
        GetShopInfoResponse info = new GetShopInfoResponse();
        using (var dbContext = new ACHEEntities()) {
            if (ValidarSessionID(datos.sessionID, dbContext)) {
                RenovarFechaValida(datos.sessionID, dbContext);
                var terminal = dbContext.Terminales.Include("Comercios").Include("Domicilios").Include("Marcas").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null) {
                    info.AnswerCode = 0;
                    info.ShopId = terminal.IDComercio;
                    info.Name = terminal.Comercios.NombreFantasia;
                    info.Address = terminal.Comercios.Domicilios.Domicilio;
                    info.Phone = terminal.Comercios.Telefono;
                    info.DocNumber = terminal.Comercios.NroDocumento;
                    info.Terminal = datos.terminal;
                    info.Establishment = terminal.POSEstablecimiento;
                    if (terminal.Comercios.IDMarca.HasValue) {
                        info.ShowProducts = dbContext.Productos.Any(x => x.Activo == true && x.IDMarca == terminal.Comercios.IDMarca.Value);
                        if (terminal.Comercios.Marcas != null) {
                            //info.promocion.wayToPay = comercio.Marca.FormaPago;
                            //info.promocion.showTicketNumber = comercio.Marca.NumeroTicket;
                            //info.promocion.footer1 = comercio.Marca.Footer1 ?? "";
                            //info.promocion.footer2 = comercio.Marca.Footer2 ?? "";
                            //info.promocion.footer3 = comercio.Marca.Footer3 ?? "";
                            //info.promocion.footer4 = comercio.Marca.Footer4 ?? "";
                            //info.promocion.showMenuFidelidad = comercio.Marca.showMenuFidelidad;
                            //info.promocion.showMenuGift = comercio.Marca.showMenuGift;
                            //info.promocion.showChargeGift = comercio.Marca.showChargeGift;
                            //info.promocion.printLOGO = comercio.Marca.printLOGO;
                            //info.promocion.inputPayments = comercio.Marca.inputPayments;
                            //info.promocion.inputTicket = comercio.Marca.inputTicket;
                        }
                        else {
                            info.AnswerCode = (int)ErrorIntegracion.MarcaInexistente;
                        }
                    }
                    else {
                        info.AnswerCode = (int)ErrorIntegracion.ComercioSinMarca;
                    }

                    bComercio bComercio = new bComercio();
                    info.Discount = bComercio.obtenerDescuento(terminal, dbContext, null);
                    info.SessionID = datos.sessionID;

                }
                else {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [Serializable]
    public class GetShopInfoResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string DocNumber { get; set; }
        public string Terminal { get; set; }
        public string Establishment { get; set; }
        public int Discount { get; set; }
        public bool ShowProducts { get; set; }
        public Promocion promocion { get; set; }
    }

    [Serializable]
    public class Promocion {
    public bool wayToPay { get; set; }
    public bool showTicketNumber { get; set; }
    public string footer1 { get; set; }
    public string footer2 { get; set; }
    public string footer3 { get; set; }
    public string footer4 { get; set; }
    public bool showMenuFidelidad { get; set; }
    public bool showMenuGift { get; set; }
    public bool showChargeGift { get; set; }
    public bool printLOGO { get; set; }
    public bool inputPayments { get; set; }
    public bool inputTicket { get; set; }
    }

    [Serializable]
    public class GetShopInfoRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
    }
}