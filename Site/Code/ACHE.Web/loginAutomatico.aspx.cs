﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections.Specialized;
public partial class loginAutomatico : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static void loginFranquiciaAutomatico(string usuario, string pwd)
    {
        using (var dbContext = new ACHEEntities())
        {
            loginFranquicias(dbContext, usuario, pwd);
        }
    }
    [WebMethod(true)]
    public static void loginFranquiciaAutomaticoAdmin(int IDFranquicia)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosFranquicias.Where(x => x.IDFranquicia == IDFranquicia && x.Tipo == "A").FirstOrDefault();
            if (usu != null)
                loginFranquicias(dbContext, usu.Usuario, usu.Pwd);
            else
            {
                usu = dbContext.UsuariosFranquicias.Where(x => x.IDFranquicia == IDFranquicia && x.Tipo == "B").FirstOrDefault();
                if (usu != null)
                    loginFranquicias(dbContext, usu.Usuario, usu.Pwd);
                else
                    throw new Exception("No hay usuarios.");
            }
        }
    }

    private static void loginFranquicias(ACHEEntities dbContext, string usuario, string pwd)
    {
        var usu = dbContext.UsuariosFranquicias.Include("Franquicias").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
        if (usu != null)
        {
            HttpContext.Current.Session.Remove("CurrentUser");
            HttpContext.Current.Session["CurrentFranquiciasUser"] = new WebFranquiciasUser(usu.IDUsuario, usu.Email,
                usu.Franquicias.NombreFantasia, usu.IDFranquicia, usu.Usuario, usu.Tipo, "blue",
                usu.Franquicias.PublicidadLocal, usu.Franquicias.PublicidadNacional, "",
                usu.Franquicias.ComisionTtCp, usu.Franquicias.ComisionTpCp, usu.Franquicias.ComisionTpCt);
        }
        else
            throw new Exception("Usuario y/o contraseña incorrecta.");
    }



    [WebMethod(true)]
    public static void loginEmpresaAutomatico(string usuario, string pwd)
    {
        using (var dbContext = new ACHEEntities())
        {
            loginEmpresas(dbContext, usuario, pwd);

        }
    }

    [WebMethod(true)]
    public static void loginEmpresaAutomaticoAdmin(int IDEmpresa)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosEmpresas.Where(x => x.IDEmpresa == IDEmpresa && x.Tipo == "A").FirstOrDefault();
            if (usu != null)
                loginEmpresas(dbContext, usu.Usuario, usu.Pwd);
            else
            {
                usu = dbContext.UsuariosEmpresas.Where(x => x.IDEmpresa == IDEmpresa && x.Tipo == "B").FirstOrDefault();
                if (usu != null)
                    loginEmpresas(dbContext, usu.Usuario, usu.Pwd);
                else
                    throw new Exception("No hay usuarios.");
            }

        }
    }

    private static void loginEmpresas(ACHEEntities dbContext, string usuario, string pwd)
    {
        var usu = dbContext.UsuariosEmpresas.Include("Empresas").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
        if (usu != null)
        {
            HttpContext.Current.Session.Remove("CurrentUser");
            usu.FechaUltLogin = DateTime.Now;
            dbContext.SaveChanges();

            HttpContext.Current.Session["CurrentEmpresasUser"] = new WebEmpresasUser(usu.IDUsuario, usu.Email, usu.Empresas.Nombre, usu.IDEmpresa, usu.Usuario, usu.Tipo, "eastern_blue");
        }
        else
            throw new Exception("Usuario y/o contraseña incorrecta.");
    }


    [WebMethod(true)]
    public static void loginComercioAutomatico(string usuario, string pwd)
    {
        using (var dbContext = new ACHEEntities())
        {
            loginComercios(dbContext, usuario, pwd);
        }
    }
    [WebMethod(true)]
    public static void loginComercioAutomaticoAdmin(int IDComercio)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosComercios.Where(x => x.IDComercio == IDComercio && x.Tipo == "A").FirstOrDefault();
            if (usu != null)
                loginComercios(dbContext, usu.Usuario, usu.Pwd);
            else
            {
                usu = dbContext.UsuariosComercios.Where(x => x.IDComercio == IDComercio && x.Tipo == "B").FirstOrDefault();
                if (usu != null)
                    loginComercios(dbContext, usu.Usuario, usu.Pwd);
                else
                    throw new Exception("No hay usuarios.");
            }

        }
    }

    private static void loginComercios(ACHEEntities dbContext, string usuario, string pwd)
    {
        var usu = dbContext.UsuariosComercios.Include("Comercios").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
        if (usu != null)
        {
            HttpContext.Current.Session.Remove("CurrentUser");
            usu.FechaUltLogin = DateTime.Now;
            dbContext.SaveChanges();

            var posWeb = dbContext.Terminales.Any(x => x.IDComercio == usu.IDComercio && x.HabilitarPOSWeb);
            var giftWeb = dbContext.Terminales.Any(x => x.IDComercio == usu.IDComercio && x.HabilitarGifcard);

            HttpContext.Current.Session["CurrentComerciosUser"] = new WebComerciosUser(usu.IDUsuario, usu.Email,
                usu.Comercios.NombreFantasia, usu.IDComercio, usu.Usuario, usu.Tipo, "eastern_blue",
                usu.Comercios.NroDocumento, usu.Comercios.FichaAlta, posWeb, giftWeb);
        }
        else
            throw new Exception("Usuario y/o contraseña incorrecta.");
    }

    [WebMethod(true)]
    public static void loginMarcaAutomatico(string usuario, string pwd)
    {
        using (var dbContext = new ACHEEntities())
        {
            loginMarcas(dbContext, usuario, pwd);
        }
    }

    [WebMethod(true)]
    public static void loginMarcaAutomaticoAdmin(int idMarca)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usu = dbContext.UsuariosMarcas.Where(x => x.IDMarca == idMarca && x.Tipo == "A").FirstOrDefault();
            if (usu != null)
                loginMarcas(dbContext, usu.Usuario, usu.Pwd);
            else
            {
                usu = dbContext.UsuariosMarcas.Where(x => x.IDMarca == idMarca && x.Tipo == "B").FirstOrDefault();
                if (usu != null)
                    loginMarcas(dbContext, usu.Usuario, usu.Pwd);
                else
                    throw new Exception("No hay usuarios.");
            }
        }
    }

    private static void loginMarcas(ACHEEntities dbContext, string usuario, string pwd)
    {
        var usu = dbContext.UsuariosMarcas.Include("Marcas").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
        if (usu != null)
        {
            usu.FechaUltLogin = DateTime.Now;
            dbContext.SaveChanges();
            HttpContext.Current.Session.Remove("CurrentUser");

            HttpContext.Current.Session["CurrentMarcasUser"] = new WebMarcasUser(
                usu.IDUsuario, usu.Email, usu.Marcas.Nombre, usu.IDMarca, usu.Usuario, usu.Tipo,
                usu.Marcas.Color, usu.Marcas.MostrarSoloPOSPropios,
                usu.Marcas.MostrarSoloTarjetasPropias, usu.Marcas.HabilitarPOSWeb,
                usu.Marcas.TipoCatalogo == "C", usu.Marcas.HabilitarSMS,
                usu.Marcas.EnvioMsjBienvenida, usu.Marcas.MsjBienvenida,
                usu.Marcas.EnvioMsjCumpleanios, usu.Marcas.MsjCumpleanios,
                usu.Marcas.CostoSMS, usu.Marcas.EnvioEmailRegistroSocio, usu.Marcas.EnvioEmailCumpleanios, usu.Marcas.EnvioEmailRegistroComercio, usu.Marcas.EmailRegistroSocio, usu.Marcas.EmailCumpleanios, usu.Marcas.EmailRegistroComercio);
        }
        else
            throw new Exception("Usuario y/o contraseña incorrecta.");
    }

    [WebMethod(true)]
    public static void loginMultimarcaAutomaticoAdmin(int idMultimarca) {
        using (var dbContext = new ACHEEntities()) {
            var usu = dbContext.UsuariosMultimarcas.Where(x => x.IDMultimarca == idMultimarca).FirstOrDefault();
            if (usu != null)
                loginMultimarcas(dbContext, usu.Usuario, usu.Pwd);
                else
                    throw new Exception("No hay usuarios.");
        }
    }

    private static void loginMultimarcas(ACHEEntities dbContext, string usuario, string pwd) {
        var usu = dbContext.UsuariosMultimarcas.Include("Multimarcas").Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
        if (usu != null) {
            usu.FechaUltLogin = DateTime.Now;
            dbContext.SaveChanges();
            HttpContext.Current.Session.Remove("CurrentUser");

            HttpContext.Current.Session["CurrentMultimarcasUser"] = new WebMultimarcasUser(
                usu.IDUsuario, usu.Email, usu.Multimarcas.Nombre, usu.Multimarcas.IDMultimarca, usu.Usuario, "",
                usu.Multimarcas.Color, usu.Multimarcas.MostrarSoloPOSPropios,
                usu.Multimarcas.MostrarSoloTarjetasPropias, usu.Multimarcas.HabilitarPOSWeb,
                usu.Multimarcas.TipoCatalogo == "C", usu.Multimarcas.HabilitarSMS,
                usu.Multimarcas.EnvioMsjBienvenida, usu.Multimarcas.MsjBienvenida,
                usu.Multimarcas.EnvioMsjCumpleanios, usu.Multimarcas.MsjCumpleanios,
                usu.Multimarcas.CostoSMS, usu.Multimarcas.EnvioEmailRegistroSocio, usu.Multimarcas.EnvioEmailCumpleanios, usu.Multimarcas.EnvioEmailRegistroComercio, usu.Multimarcas.EmailRegistroSocio, usu.Multimarcas.EmailCumpleanios, usu.Multimarcas.EmailRegistroComercio,usu.Multimarcas.IDFranquicia??0);
        }
        else
            throw new Exception("Usuario y/o contraseña incorrecta.");
    }

}