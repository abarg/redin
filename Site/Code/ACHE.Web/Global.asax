﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {

        var context = HttpContext.Current;
		var response = context.Response;
		
		// enable CORS
		response.AddHeader("Access-Control-Allow-Origin", "*");
		response.AddHeader("X-Frame-Options", "ALLOW-FROM *");

        if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://www.redin.com.ar/login.aspx"))
        {
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://www.redin.com.ar/login.aspx", "https://www.redin.com.ar/login.aspx"));
        }
        else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://www.redin.com.ar/login-marcas.aspx"))
        {
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://www.redin.com.ar/login-marcas.aspx", "https://www.redin.com.ar/login-marcas.aspx"));
        }
        else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://www.redin.com.ar/login-comercios.aspx"))
        {
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://www.redin.com.ar/login-comercios.aspx", "https://www.redin.com.ar/login-comercios.aspx"));
        }
        else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://www.redin.com.ar/login-empresas.aspx"))
        {
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://www.redin.com.ar/login-empresas.aspx", "https://www.redin.com.ar/login-empresas.aspx"));
        }
        else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://www.redin.com.ar/login-franquicias.aspx"))
        {
            HttpContext.Current.Response.Status = "301 Moved Permanently";
            HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://www.redin.com.ar/login-franquicias.aspx", "https://www.redin.com.ar/login-franquicias.aspx"));
        }
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
