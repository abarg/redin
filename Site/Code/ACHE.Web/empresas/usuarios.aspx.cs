﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class empresas_usuarios : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
            int idEmpresa = usu.IDEmpresa;

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.UsuariosEmpresas
                    .Where(x => x.IDEmpresa == idEmpresa)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
            {
                var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
                int IDEmpresa = usu.IDEmpresa;

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == id && x.IDEmpresa == IDEmpresa).FirstOrDefault();
                    if (entity != null)
                    {

                        dbContext.UsuariosEmpresas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDUsuario, string usuario, string email, string pwd, string tipo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
            {
                var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
                int IDEmpresa = usu.IDEmpresa;

                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.UsuariosEmpresas.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosEmpresas.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosEmpresas entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosEmpresas();
                    else
                        entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDEmpresa = IDEmpresa;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosEmpresas.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}