﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.Data;
using System.IO;

public partial class empresas_transacciones : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentEmpresasUser.Tipo == "B")
                Response.Redirect("home.aspx");

            int diasHastaHoy = DateTime.Now.Day;
            int diasMes = DateTime.Now.GetCountDaysOfMonth();
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
            int IDEmpresa = usu.IDEmpresa;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesEmpresasView
                    .Where(x => x.IDEmpresa == IDEmpresa && x.ImporteOriginal > 1)// && x.FechaTransaccion >= fechaDesdeBase)
                    .OrderByDescending(x => x.FechaTransaccion)
                    .Select(x => new
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Comercio = x.NombreFantasia,
                        Domicilio = x.Domicilio,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        Credito = (x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.PuntosAContabilizar ?? 0) / 100) : (((x.PuntosAContabilizar ?? 0) / 100) * -1),
                        //PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string documento)//, string comercio)
    {
        string fileName = "Transacciones";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
                int IDEmpresa = usu.IDEmpresa;

                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.TransaccionesEmpresasView
                        .Where(x => x.IDEmpresa == IDEmpresa && x.ImporteOriginal > 1)// && x.FechaTransaccion >= fechaDesdeBase)
                        .OrderByDescending(x => x.FechaTransaccion).AsQueryable();

                    if (tarjeta != "")
                        info = info.Where(x => x.Numero.ToLower().Contains(tarjeta.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumentoSocio.ToLower().Contains(documento.ToLower()));
                    //if (comercio != "")
                    //    info = info.Where(x => x.NombreFantasia.ToString().ToLower().Contains(comercio.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }

                    dt = info.ToList().Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Comercio = x.NombreFantasia,
                        Operacion = x.Operacion,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Credito = x.Credito
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjeta(string tarjeta)
    {
        return GetTrByTarjeta(tarjeta, null);
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjetaMax(string tarjeta, int max)
    {
        return GetTrByTarjeta(tarjeta, max);
    }

    private static List<TransaccionesViewModel> GetTrByTarjeta(string tarjeta, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesEmpresasView.Where(x => x.Numero == tarjeta && x.ImporteOriginal > 1)
                     .OrderByDescending(x => x.FechaTransaccion)
                     .Select(x => new TransaccionesViewModel
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0
                     }).ToList();

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.Fecha).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        return GetTrBySocio(id, null);

    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocioMax(int id, int max)
    {
        return GetTrBySocio(id, max);
    }

    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesEmpresasView.Where(x => x.IDSocio.HasValue && x.IDSocio == id && x.ImporteOriginal > 1)
                     .OrderByDescending(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0
                     }).ToList();

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }
}