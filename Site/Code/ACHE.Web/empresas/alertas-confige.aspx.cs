﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class empresas_alertas_confige : PaginaEmpresasBase{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentEmpresasUser.Tipo != "A")
                Response.Redirect("home.aspx");

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }
        }
    }

     [WebMethod(true)]
    public static void guardar(int id, string nombre, string prioridad, string tipo, string montoDesde,
        string montoHasta, string cantidad, int fecha, bool activa, bool restringida)
    {
        try
        {
              if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
              {
                var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
                int IDEmpresa = usu.IDEmpresa;
                using (var dbContext = new ACHEEntities())
                {
                    Alertas entity;
                    if (id > 0)
                        entity = dbContext.Alertas.Where(x => x.IDAlerta == id && x.IDEmpresa == IDEmpresa).FirstOrDefault();
                    else
                    {
                        entity = new Alertas();
                        entity.IDEmpresa =  IDEmpresa;
                    }

                    entity.Nombre = nombre;
                    entity.Prioridad = prioridad;
                    entity.Tipo = tipo;
                    if (montoDesde != string.Empty)
                        entity.MontoDesde = int.Parse(montoDesde);
                    else
                        entity.MontoDesde = null;
                    if (montoDesde != string.Empty)
                        entity.MontoHasta = int.Parse(montoHasta);
                    else
                        entity.MontoHasta = null;
                    if (cantidad != string.Empty)
                        entity.CantTR = int.Parse(cantidad);
                    else
                        entity.CantTR = null;
                    if (fecha > 0)
                        entity.FechaDesde = fecha;
                    else
                        entity.FechaDesde = null;
                    /*
                    if (!(fecha2 == ""))
                    {
                        DateTime fechaActual = DateTime.Now;
                        DateTime fechaDesde = Convert.ToDateTime(fecha2);
                        TimeSpan dateDiff = fechaActual - fechaDesde;
                        entity.FechaDesde = Convert.ToInt32(dateDiff.Days);
                    }
                     * */
                    entity.Activa = activa;
                    entity.Restringido = restringida;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Alertas.Add(entity);
                        dbContext.SaveChanges();
                 
                    }
                }
            }
            
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Alertas.Where(x => x.IDAlerta == id).FirstOrDefault();
            if (entity != null)
            {
                txtNombre.Text = entity.Nombre;
                ddlTipo.Text = entity.Tipo;
                ddlPrioridad.Text = entity.Prioridad;
                txtCantTR.Text = entity.CantTR.HasValue ? entity.CantTR.Value.ToString() : "";
                txtMontoDesde.Text = entity.MontoDesde.HasValue ? entity.MontoDesde.Value.ToString() : "";
                txtMontoHasta.Text = entity.MontoHasta.HasValue ? entity.MontoHasta.Value.ToString() : "";
                ddlFecha.SelectedValue = entity.FechaDesde.HasValue ? entity.FechaDesde.Value.ToString() : "";
                /*
                if (entity.FechaDesde.HasValue)
                {
                    if (ddlFecha.Text == "0")
                    {
                        txtFecha.Text = DateTime.Now.AddDays((entity.FechaDesde.Value) * -1).ToString("dd/MM/yyy");
                    }
                }
                 * */
                chkActiva.Checked = entity.Activa;
                if (entity.Restringido)
                {
                    rdbDeterminadas.Checked = true;
                    rdbTodas.Checked = false;
                }
                else
                {
                    rdbDeterminadas.Checked = false;
                    rdbTodas.Checked = true;
                }
            }
        }
    }
}