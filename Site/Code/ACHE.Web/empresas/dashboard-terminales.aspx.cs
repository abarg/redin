﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;


public partial class empresas_dashboard_terminales : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerEstadoTerminales()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
            int idEmpresa = usu.IDEmpresa;
            using (var dbContext = new ACHEEntities())
            {

                var aux = dbContext.GetReporteTerminalesEmpresas("", "", "", idEmpresa)
                    .Select(x => new TerminalesViewModel()
                    {
                        Estado = "",
                        Fecha_Activ = x.Fecha_Activacion,
                        Fecha_Reprog = x.Fecha_Reprogramacion,
                        Reprogramado = x.Reprogramado ? "Si" : "No",
                        Invalido = x.Comercio_Inválido ? "Si" : "No",
                        Activo = x.Activo.HasValue ? (x.Activo.Value ? "Si" : "No") : "No",
                        CantTR = x.CantTR,
                        UltimoImporte = x.UltimoImporte,
                        Tipo = x.Tipo
                    }).ToList();

                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (row.Activo == "No")
                        row.Estado = "Negro";
                    else if (row.Invalido == "Si" && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.Invalido == "Si" && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo == "Si" && row.Fecha_Reprog != string.Empty && row.Reprogramado == "Si" && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo == "Si" && row.Reprogramado == "No" && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo == "Si" && row.Reprogramado == "Si" && row.Fecha_Reprog != string.Empty && row.CantTR == 0 && row.Fecha_Activ == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo == "Si" && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo == "Si" && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else
                        row.Estado = "Desconocido";
                }

                //var prevList = aux.ToList();
                int negro = aux.Count(x => x.Estado == "Negro");
                int naranja = aux.Count(x => x.Estado == "Naranja");
                int verde = aux.Count(x => x.Estado == "Verde");
                int rojo = aux.Count(x => x.Estado == "Rojo");
                int amarillo = aux.Count(x => x.Estado == "Amarillo");
                int azul = aux.Count(x => x.Estado == "Azul");
                int desconocido = aux.Count(x => x.Estado == "Desconocido");
                int noprobado = aux.Count(x => x.Estado == "Marron");

                list.Add(new Chart() { data = negro, label = "<a href=\"javascript:verDetalleTerminales('Negro');\">Baja (" + negro + ")</a>" });
                list.Add(new Chart() { data = naranja, label = "<a href=\"javascript:verDetalleTerminales('Naranja');\">Inválido (" + naranja + ")</a>" });
                list.Add(new Chart() { data = verde, label = "<a href=\"javascript:verDetalleTerminales('Verde');\">Activo Mayor a $1 (" + verde + ")</a>" });
                list.Add(new Chart() { data = rojo, label = "<a href=\"javascript:verDetalleTerminales('Rojo');\">No Reprogramado (" + rojo + ")</a>" });
                list.Add(new Chart() { data = amarillo, label = "<a href=\"javascript:verDetalleTerminales('Amarillo');\">Activo Menor a $1 (" + amarillo + ")</a>" });
                list.Add(new Chart() { data = azul, label = "<a href=\"javascript:verDetalleTerminales('Azul');\">Web (" + azul + ")</a>" });
                list.Add(new Chart() { data = desconocido, label = "<a href=\"javascript:verDetalleTerminales('Desconocido');\">Desconocido (" + desconocido + ")</a>" });
                list.Add(new Chart() { data = noprobado, label = "<a href=\"javascript:verDetalleTerminales('Marron');\">No Probado (" + noprobado + ")</a>" });
            }
        }

        return list;
    }


    [WebMethod(true)]
    public static string obtenerDetalleTerminales(string estado)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
             var usu = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];
            int idEmpresa = usu.IDEmpresa;
            using (var dbContext = new ACHEEntities())
            {

                var list = dbContext.GetReporteTerminalesEmpresas("", "", "", idEmpresa).ToList();
                var aux = list.Select(x => new TerminalesViewModel()
                {
                    SDS = x.SDS,
                    Nombre = x.Nombre,
                    Estado = "",
                    Domicilio = x.Domicilio,
                    Localidad = x.Localidad,
                    Fecha_Carga = x.Fecha_Carga,
                    Fecha_Alta = x.Fecha_Alta,
                    Dealer = x.Dealer,
                    Tipo = x.Tipo + " " + x.Tipo_Terminal,
                    Terminal = x.Terminal,
                    Establecimiento = x.Establecimiento,
                    Fecha_Activ = x.Fecha_Activacion,
                    Fecha_Reprog = x.Fecha_Reprogramacion,
                    Reprogramado = x.Reprogramado ? "Si" : "No",
                    Invalido = x.Comercio_Inválido ? "Si" : "No",
                    Activo = x.Activo.HasValue ? (x.Activo.Value ? "Si" : "No") : "No",
                    Observaciones = x.Observaciones,
                    CantTR = x.CantTR,
                    UltimoImporte = x.UltimoImporte
                }).ToList(); //.ToDataTable();

                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (row.Activo == "No")
                        row.Estado = "Negro";
                    else if (row.Invalido == "Si" && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.Invalido == "Si" && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo == "Si" && row.Fecha_Reprog != string.Empty && row.Reprogramado == "Si" && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo == "Si" && row.Reprogramado == "No" && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo == "Si" && row.Reprogramado == "Si" && row.Fecha_Reprog != string.Empty && row.CantTR == 0 && row.Fecha_Activ == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo == "Si" && row.UltimoImporte < 1)
                        row.Estado = "Amarillo";
                    else if (row.Activo == "Si" && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else
                        row.Estado = "Desconocido";
                }

                aux = aux.Where(x => x.Estado.ToLower() == estado.ToLower()).ToList();

                if (aux.Any())
                {
                    foreach (var detalle in aux)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.SDS + "</td>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Domicilio + "</td>";
                        html += "<td>" + detalle.Fecha_Carga + "</td>";
                        html += "<td>" + detalle.Fecha_Alta + "</td>";
                        html += "<td>" + detalle.Terminal + "</td>";
                        html += "<td>" + detalle.Establecimiento + "</td>";
                        html += "<td>" + detalle.Fecha_Activ + "</td>";
                        html += "<td>" + detalle.Fecha_Reprog + "</td>";
                        html += "<td>" + detalle.Reprogramado + "</td>";
                        html += "<td>" + detalle.Invalido + "</td>";
                        html += "<td>" + detalle.Activo + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='12'>No hay un detalle disponible</td></tr>";

            }
        }

        return html;
    }

}