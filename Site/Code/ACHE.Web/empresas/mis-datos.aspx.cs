﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class empresas_mis_datos : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            this.cargarDatosUsuario();
    }

    [WebMethod(true)]
    public static void grabar(string email)
    {
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            var user = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.UsuariosEmpresas.Where(x => x.Email == email.Trim()).FirstOrDefault();
                if (aux != null && aux.IDUsuario != user.IDUsuario)
                    throw new Exception("El email ya se encuentra registrado.");

                UsuariosEmpresas entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                //entity.Name = name;
                //entity.LastName = lastName;
                entity.Email = email;
                //if (pwd != string.Empty)
                //    entity.Pwd = pwd;

                user.Email = email;

                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentEmpresasUser"] = user;
        }
    }

    private void cargarDatosUsuario()
    {
        WebEmpresasUser user = CurrentEmpresasUser;
        //this.txtName.Text = user.Nombre;
        //this.txtLastname.Text = user.Apellido;
        this.litUsuario.Text = user.Usuario;
        this.txtEmail.Text = user.Email;
    }
}