﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.IO;

public partial class empresas_facturas : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentEmpresasUser.Tipo == "B")
                Response.Redirect("home.aspx");

            CargarFacturas();
        }
    }

    private void CargarFacturas()
    {
        using (var dbContext = new ACHEEntities())
        {
            var result = dbContext.Facturas.Include("Comercios")
                .Where(x => x.FechaCAE.HasValue && x.Visible && x.Comercios.EmpresasComercios.Any(y => y.IDEmpresa == CurrentEmpresasUser.IDEmpresa))
                .Select(x => new FacturasFrontViewModel()
                {
                    ID = x.IDFactura,
                    Comercio = x.Comercios.RazonSocial,
                    NroDocumento = x.Comercios.NroDocumento,
                    Numero = x.Numero,
                    //Importe = x.Tipo == "RI" ? (x.ImporteTotal + (x.ImporteTotal * 0.21M)) : x.ImporteTotal,
                    Importe = x.ImporteTotal,
                    //Importe = x.ImporteTotal + (x.ImporteTotal * 0.21M),
                    FechaEmision = x.PeriodoHasta,
                    FechaVto = x.PeriodoHasta,
                    CondicionIva = x.Tipo
                }).OrderByDescending(x => x.FechaEmision).ToList();

            /*FacturasFrontViewModel list = new FacturasFrontViewModel();
            foreach (var fc in result)
            {
                var aux = fc;
                if (aux.CondicionIva == "RI")
                    aux.Importe += aux.Importe * 0.21M;
            }*/

            rptFacturas.DataSource = result;
            rptFacturas.DataBind();

            if (!result.Any())
                trSinFacturas.Visible = true;
        }
    }
}