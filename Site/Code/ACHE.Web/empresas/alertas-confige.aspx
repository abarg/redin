﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageEmpresas.master" AutoEventWireup="true" CodeFile="alertas-confige.aspx.cs" Inherits="empresas_alertas_confige" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/empresas/alertas-confige.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/empresas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/empresas/alertas-config.aspx") %>">Alertas</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Alerta</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />

                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required"  MinLength="3" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Prioridad</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlPrioridad">
                            <asp:ListItem Text="Baja" Value="B"></asp:ListItem>
                            <asp:ListItem Text="Media" Value="M"></asp:ListItem>
                            <asp:ListItem Text="Alta" Value="A"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Tipo de alerta</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTipo" onchange="toggleTipoAlerta();">
                            <asp:ListItem Text="Por monto" Value="Por monto"></asp:ListItem>
                            <asp:ListItem Text="Por cantidad" Value="Por cantidad"></asp:ListItem>
                            <asp:ListItem Text="Por monto y cantidad" Value="Por monto y cantidad"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group" id="divMontoDesde">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Monto $ desde</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtMontoDesde" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divMontoHasta">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Monto $ hasta</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtMontoHasta" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divCantTR">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Cant. de TR x Tarjeta</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtCantTR" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divFecha">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Fecha</label>
                    <div class="col-sm-4 col-md-4 col-lg-3" >
                        <asp:DropDownList runat="server" class="form-control" ID="ddlFecha">
                            <asp:ListItem Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Último día" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Últimos 5 días" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Últimos 10 días" Value="10"></asp:ListItem>
                            <asp:ListItem Text="Últimos 15 días" Value="15"></asp:ListItem>
                            <asp:ListItem Text="Últimos 20 días" Value="20"></asp:ListItem>
                            <asp:ListItem Text="Últimos 30 días" Value="30"></asp:ListItem>
                            <asp:ListItem Text="Últimos 45 días" Value="45"></asp:ListItem>
                            <asp:ListItem Text="Últimos 60 días" Value="60"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Activa</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                       <asp:CheckBox runat="server" ID="chkActiva" Text="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Válido para</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <label class="radio-inline">
                            <asp:RadioButton runat="server" ID="rdbTodas" Text="Todas las tarjetas" GroupName="Valido" Checked="true" />
                        </label>
                        <label class="radio-inline" style="padding-left: 10px;">
                            <asp:RadioButton runat="server" ID="rdbDeterminadas" Text="Tarjetas en seguimiento" GroupName="Valido" />
                            <a href="alertas-seguimiento.aspx">Configurar</a>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a href="alertas-config.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</asp:Content>

