﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class empresas_cambiar_pwd : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            litUsuario.Text = CurrentEmpresasUser.Usuario;
    }

    [WebMethod(true)]
    public static void grabar(string pwdActual, string pwd)
    {
        if (HttpContext.Current.Session["CurrentEmpresasUser"] != null)
        {
            var user = (WebEmpresasUser)HttpContext.Current.Session["CurrentEmpresasUser"];

            using (var dbContext = new ACHEEntities())
            {
                UsuariosEmpresas entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                if (entity.Pwd != pwdActual)
                    throw new Exception("La contraseña actual ingresada es inválida");

                entity.Pwd = pwd;
                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentEmpresasUser"] = user;
        }
    }
}