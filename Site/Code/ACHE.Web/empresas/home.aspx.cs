﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;

public partial class empresas_home : PaginaEmpresasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litNombre.Text = CurrentEmpresasUser.Empresa;
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Empresas.Where(x => x.IDEmpresa == CurrentEmpresasUser.IDEmpresa).FirstOrDefault();
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Logo))
                    imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                else
                    imgLogo.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";
            }
        }
    }
}