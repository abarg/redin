﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageEmpresas.master" AutoEventWireup="true" CodeFile="factura-detalle.aspx.cs" Inherits="empresas_facturas_detalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--<link rel="stylesheet" href="<%= ResolveUrl("~/lib/datatables/extras/TableTools/media/css/TableTools.css") %>" />--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/empresas/facturas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <%--<style type="text/css">
        th, td {
            text-align: center
        }
        th {
            height: 35px;
        }
    </style>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/empresas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/empresas/facturas.aspx") %>">Facturación</a></li>
            <li class="last">Detalle</li>
        </ul>
    </div>

    <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>

    <div class="row">
        <div class="col-sm-8 col-md-8">
		    <h1 class="invoice_heading">Factura #<asp:Literal runat="server" ID="litNroFc"></asp:Literal></h1>
            <br />
            <a class="btn btn-default" href="/fileHandler.ashx?type=facturas&module=empresas&file=<%= Request.QueryString["Id"] %>">Descargar factura</a>
            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar(<%= Request.QueryString["Id"] %>);">Exportar a Excel</button>
            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
            <a href="" id="lnkDownload" download="Detalle_Fc" style="display:none">Descargar</a>
            
	    </div>
	    <div class="col-sm-4 col-md-4">
		    <p class="sepH_a"><span class="sepV_b text-muted">Periodo desde</span><strong><asp:Literal runat="server" ID="litPeriodoDesde"></asp:Literal></strong></p>
		    <p class="sepH_a"><span class="sepV_b text-muted">Periodo hasta</span><strong><asp:Literal runat="server" ID="litPeriodoHasta"></asp:Literal></strong></p>
		    <asp:Panel runat="server" ID="pnlFcA">
                <p><span class="sepV_b text-muted">Subtotal</span><strong>$ <asp:Literal runat="server" ID="litSubtotal"></asp:Literal></strong></p>
                <p><span class="sepV_b text-muted">Iva</span><strong>$ <asp:Literal runat="server" ID="litIva"></asp:Literal></strong></p>
                <p><span class="sepV_b text-muted">Total</span><strong>$ <asp:Literal runat="server" ID="litImporte2"></asp:Literal></strong></p>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlFcB">
                <p><span class="sepV_b text-muted">Importe total</span><strong>$ <asp:Literal runat="server" ID="litImporte"></asp:Literal></strong></p>
            </asp:Panel>
	    </div>
    </div>

    <div class="row">
	    <div class="col-sm-12 col-md-12">
		    <table class="table" id="tbDetalle">
			    <thead>
				    <tr>
					    <th>Fecha</th> 
                        <th>Hora</th> 
                        <th>Operacion</th> 
                        <%--<th>Comercio</th> 
                        <th>Nro Establecimiento</th> 
                        <th>Terminal</th> --%>
                        <th>Tarjeta</th> 
                        <%--<th>Socio</th> 
                        <th>Importe Original</th> 
                        <th>Importe Ahorro</th> --%>
                        <th>Ticket</th>
                        <th>Arancel</th>
                        <th>Puntos</th>
                        <th>Costo Red</th>
                        <th>Neto Gravado</th>
                        <th>IVA</th>
                        <th>Total</th>
				    </tr>
			    </thead>
			    <tbody>
                    <asp:Repeater runat="server" ID="rptDetalle">
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("Fecha").ToString() %></td>
                                <td><%# Eval("Hora").ToString() %></td>
                                <td><%# Eval("Operacion").ToString() %></td>
                                <%--<td style="text-align: left"><%# Eval("Comercio").ToString() %></td>
                                <td><%# Eval("NroEstablecimiento").ToString() %></td>
                                <td><%# Eval("POSTerminal").ToString() %></td>--%>
                                <td><%# Eval("Tarjeta").ToString() %></td>
                                <%--<td><%# Eval("Socio") %></td>
                                <td>$ <%# Decimal.Parse(Eval("ImporteOriginal").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("ImporteAhorro").ToString()).ToString("N2") %></td>--%>
                                <td>$ <%# Decimal.Parse(Eval("Ticket").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("Arancel").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("Puntos").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("CostoRed").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("NetoGrabado").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("Iva").ToString()).ToString("N2") %></td>
                                <td>$ <%# Decimal.Parse(Eval("Total").ToString()).ToString("N2") %></td>
                            </tr>
                        </ItemTemplate>
					</asp:Repeater>
		        </tbody>
		    </table>
	    </div>
    </div>
    
    <!-- datatable -->
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<%--<!-- datatable table tools -->
    <script src="<%= ResolveUrl("~/lib/datatables/extras/TableTools/media/js/TableTools.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/TableTools/media/js/ZeroClipboard.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <!-- datatables bootstrap integration -->
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            setearTabla();
        });
    </script>

</asp:Content>
