﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageEmpresas.master" AutoEventWireup="true" CodeFile="cambiar-pwd.aspx.cs" Inherits="empresas_cambiar_pwd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/empresas/cambiarPwd.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/empresas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Cambiar contraseña</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Cambiar contraseña</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

			<form class="form-horizontal" id="form_misDatos" runat="server">
				<fieldset>
					<div class="form-group">
						<label class="control-label col-sm-2">Usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<strong><asp:Literal runat="server" ID="litUsuario"></asp:Literal></strong>
							</p>
						</div>
					</div>
                    <div class="form-group">
						<label for="u_password" class="control-label col-sm-2"><span class="f_req">*</span>Contraseña actual</label>
						<div class="col-sm-4">
							<asp:TextBox runat="server" ID="txtPasswordActual" CssClass="form-control required" TextMode="Password" MaxLength="10" MinLength="3" />
						</div>
					</div>
					<div class="form-group">
						<label for="u_password" class="control-label col-sm-2"><span class="f_req">*</span>Nueva Contraseña</label>
						<div class="col-sm-4">
							<asp:TextBox runat="server" ID="txtPassword" CssClass="form-control required" TextMode="Password" MaxLength="10" MinLength="3" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-2">
							<button id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
						</div>
					</div>
				</fieldset>
			</form>
        </div>
    </div>      
    
            
</asp:Content>



