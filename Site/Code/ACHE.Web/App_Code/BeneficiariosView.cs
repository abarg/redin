﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BeneficiariosView {

    public string Ciudad { get; set; }
    public string Nombre { get; set; }
    public string Apellido { get; set; }
    public string NumDoc { get; set; }
    public DateTime FechaNacimiento { get; set; }
    public string Actividad { get; set; }
    public string Nivel { get; set; }
    public string SubCategoria { get; set; }
    public string Categoria { get; set; }
    public string Secretaria { get; set; }
    public DateTime FechaAlta { get; set; }
    public DateTime? FechaInscripcion { get; set; }
    public string Sede { get; set; }
    public string Horario { get; set; }
    public string Email { get; set; }
    public string Celular { get; set; }
    public string Telefono { get; set; }

}
