﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for PaginaMultimarcasBase
/// </summary>
public class PaginaMultimarcasBase : System.Web.UI.Page {

    public WebMultimarcasUser CurrentMultimarcasUser {

        get { return (Session["CurrentMultimarcasUser"] != null) ? (WebMultimarcasUser)Session["CurrentMultimarcasUser"] : null; }
        set { Session["CurrentMultimarcasUser"] = value; }

    }

    private void ValidateUser() {

        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login-multimarcas.aspx") {
            if (CurrentMultimarcasUser == null) {
                Response.Redirect("~/login-multimarcas.aspx");
            }
        }
    }

    public string GetUserIP() {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList)) {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e) {
        ValidateUser();
    }
}