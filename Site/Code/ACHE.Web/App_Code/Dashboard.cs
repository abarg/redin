﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model {
    public class Chart {
        public string label { get; set; }
        public int data { get; set; }
    }

    public class ChartDate {
        public DateTime fecha { get; set; }
        public int importe { get; set; }
    }

    public class ChartString {
        public string label { get; set; }
        public string data { get; set; }
    }

    public class ChartDecimal {
        public string label { get; set; }
        public decimal data { get; set; }
    }

    public class ChartDouble {
        public string label { get; set; }
        public double? data { get; set; }
    }

    public class TableHtml {
        public string uno { get; set; }
        public string dos { get; set; }
        public string tres { get; set; }
        public decimal cuatro { get; set; }
    }

    public class TableIntHtml
    {
        public string uno { get; set; }
        public int dos { get; set; }
        public int tres { get; set; }
    }

    public class NullableChart
    {
        public int? data { get; set; }
    }

    public class Fila {
        public string titulo { get; set; }
        public decimal valor0 { get; set; }
        public decimal valor1 { get; set; }
        public decimal valor2 { get; set; }
        public decimal valor3 { get; set; }
        public decimal valor4 { get; set; }
        public decimal valor5 { get; set; }
        public decimal valor6 { get; set; }
        public decimal valor7 { get; set; }
        public decimal valor8 { get; set; }
        public decimal valor9 { get; set; }
        public decimal valor10 { get; set; }
        public decimal valor11 { get; set; }
        public decimal valor12 { get; set; }

    }
    public class DatosSMS
    {
        public string Nombre { get; set; }
        public int cantEnviados { get; set; }
        public int cantNoenviados { get; set; }
        public decimal Costo { get; set; }
        public int IDMarca { get; set; }
        public int cant { get; set; }
    }
    public class TarjetasTabla {
        public string Marca { get; set; }
        public int Tarjetas { get; set; }
    }

    public class TarjetasImpresas {
        public string Marca { get; set; }
        public int Activas { get; set; }
        public int Inactivas { get; set; }
        public int Total { get; set; }
        public int Desde { get; set; }
        public int Hasta { get; set; }
    }

    public class DatosSMSEnviados
    {
        public string Nombre { get; set; }
        public int cantenviados { get; set; }
       
        public decimal Costo { get; set; }
        public int IDMarca { get; set; }
       
    }
}