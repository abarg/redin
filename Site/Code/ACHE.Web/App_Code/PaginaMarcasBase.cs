﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaMarcasBase : System.Web.UI.Page
{
    public WebMarcasUser CurrentMarcasUser
    {
        get { return (Session["CurrentMarcasUser"] != null) ? (WebMarcasUser)Session["CurrentMarcasUser"] : null; }
        set { Session["CurrentMarcasUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login-marcas.aspx")
        {
            if (CurrentMarcasUser == null)
            {
                Response.Redirect("~/login-marcas.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}