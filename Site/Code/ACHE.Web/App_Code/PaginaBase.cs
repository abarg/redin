﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaBase : System.Web.UI.Page
{
    public Usuarios CurrentUser
    {
        get { return (Session["CurrentUser"] != null) ? (Usuarios)Session["CurrentUser"] : null; }
        set { Session["CurrentUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login.aspx")
        {
            if (CurrentUser == null)
            {
                Response.Redirect("~/login.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}