﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebMultimarcasUser
/// </summary>
public class WebMultimarcasUser{
        public int IDUsuario { get; set; }
        public string Email { get; set; }
        public string Marca { get; set; }
        public int IDMultimarca { get; set; }
        public int IDFranquicia { get; set; }
        public string Usuario { get; set; }
        public string Tipo { get; set; }
        public string Theme { get; set; }
        public bool SoloPOSPropios { get; set; }
        public bool SoloTarjetasPropias { get; set; }
        public bool POSWeb { get; set; }
        public bool Catalogo { get; set; }
        public bool SMS { get; set; }
        public bool SMSBienvenida { get; set; }
        public string MensajeBienvenida { get; set; }
        public bool SMSCumpleanios { get; set; }
        public string MensajeCumpleanios { get; set; }
        public decimal CostoSMS { get; set; }

        public bool EnvioEmailRegistroSocio { get; set; }
        public bool EnvioEmailCumpleanios { get; set; }
        public bool EnvioEmailBienvenidaComercio { get; set; }
        public string MensajeEmailSocio { get; set; }
        public string MensajeEmailCumpleanios { get; set; }
        public string MensajeEmailComercio { get; set; }
    

        //public string NroDocumento { get; set; }

        public WebMultimarcasUser(int idUsuario, string email, string marca, int idMultimarca, string usuario, string tipo,
            string theme, bool posPropios, bool tarjPropias, bool posWeb, bool catalogo, bool sms, bool smsBienvenida,
            string mensajeBienvenida, bool smsCumpleanios, string mensajeCumpleanios, decimal costoSMS,bool emailBienvenidaSocio, bool emailCumpleanios,bool emailBienvenidaComercio,string mensajeEmailSocio,string mensajeEmailCumpleanios,string mensajeEmailComercio,int idFranquicia)//, string nroDocumento)
        {
            this.IDUsuario = idUsuario;
            this.Marca = marca;
            this.IDMultimarca = idMultimarca;
            //this.IDFranquicia = idFranquicia;
            this.Email = email;
            this.Usuario = usuario;
            this.Tipo = tipo;
            this.Theme = theme;
            this.SoloPOSPropios = posPropios;
            this.SoloTarjetasPropias = tarjPropias;
            this.POSWeb = posWeb;
            this.Catalogo = catalogo;
            this.SMS = sms;
            this.SMSBienvenida = smsBienvenida;
            this.MensajeBienvenida = mensajeBienvenida;
            this.SMSCumpleanios = smsCumpleanios;
            this.MensajeCumpleanios = mensajeCumpleanios;
            this.CostoSMS = costoSMS;

            this.EnvioEmailRegistroSocio = emailBienvenidaSocio;
            this.EnvioEmailCumpleanios = emailCumpleanios;
            this.EnvioEmailBienvenidaComercio = emailBienvenidaComercio;

            this.MensajeEmailSocio = mensajeEmailSocio;
            this.MensajeEmailCumpleanios = mensajeEmailCumpleanios;
            this.MensajeEmailComercio = mensajeEmailComercio;
            //this.NroDocumento = nroDocumento;
        }
    }