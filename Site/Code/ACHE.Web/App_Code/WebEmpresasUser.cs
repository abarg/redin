﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class WebEmpresasUser
    {
        public int IDUsuario { get; set; }
        public string Email { get; set; }
        public string Empresa { get; set; }
        public int IDEmpresa { get; set; }
        public string Usuario { get; set; }
        public string Tipo { get; set; }
        public string Theme { get; set; }

        public WebEmpresasUser(int idUsuario, string email, string empresa, int idEmpresa, string usuario, string tipo, string theme)
        {
            this.IDUsuario = idUsuario;
            this.Empresa = empresa;
            this.IDEmpresa = idEmpresa;
            this.Email = email;
            this.Usuario = usuario;
            this.Tipo = tipo;
            this.Theme = theme;
        }
    }
}