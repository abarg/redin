﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class WebComerciosUser
    {
        public int IDUsuario { get; set; }
        public string Email { get; set; }
        public string Comercio { get; set; }
        public int IDComercio { get; set; }
        public string Usuario { get; set; }
        public string Tipo { get; set; }
        public string Theme { get; set; }
        public string NroDocumento { get; set; }
        public bool POSWeb { get; set; }
        public bool GiftWeb { get; set; }
        public string FichaAlta { get; set; }


        public WebComerciosUser(int idUsuario, string email, string comercio, int idComercio, string usuario, string tipo, string theme, string nroDocumento, string fichaAlta, bool posWeb, bool giftWeb)
        {
            this.IDUsuario = idUsuario;
            this.Comercio = comercio;
            this.IDComercio = idComercio;
            this.Email = email;
            this.Usuario = usuario;
            this.Tipo = tipo;
            this.Theme = theme;
            this.NroDocumento = nroDocumento;
            this.POSWeb = posWeb;
            this.GiftWeb = giftWeb;
            this.FichaAlta = fichaAlta;
        }
    }
}