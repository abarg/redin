﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaFranquiciasBase : System.Web.UI.Page
{
    public WebFranquiciasUser CurrentFranquiciasUser
    {
        get { return (Session["CurrentFranquiciasUser"] != null) ? (WebFranquiciasUser)Session["CurrentFranquiciasUser"] : null; }
        set { Session["CurrentFranquiciasUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login-franquicias.aspx")
        {
            if (CurrentFranquiciasUser == null)
            {
                Response.Redirect("~/login-franquicias.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}