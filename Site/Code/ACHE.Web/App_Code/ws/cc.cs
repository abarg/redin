﻿using ACHE.Business;
using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for cc
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class cc : System.Web.Services.WebService
{

    public cc()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public SynchroAndLoginResponse SynchroAndLogin(SynchroAndLoginRequest request)
    {
        SynchroAndLoginResponse response = new SynchroAndLoginResponse();

        if (request.username == "opEwave" && request.password == "4567")
        {
            //if (request.serialNumber == "AN1")
            //{
            response.answerCode = 0;
            response.sessionID = Guid.NewGuid().ToString();
            var @operator = new Operator();
            @operator.username = "opEwave";
            @operator.name = "Ewave";
            @operator.surname = "Ewave";
            @operator.language = 3;

            response.@operator = @operator;
            //}
            //else
            //    response.answerCode = 69;
        }
        else
            response.answerCode = 77;

        return response;
    }

    [WebMethod]
    public SaleByCardResponse SalesByCard(SaleByCardRequest request)
    {
        SaleByCardResponse response = new SaleByCardResponse();
        if (request.sessionID != string.Empty)
        {
            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(request.card.ToString(), true);
            if (oTarjeta != null && oTarjeta.IDSocio.HasValue)
            {
                if (request.totalMoney > 0)
                {
                    string tipo = "Venta";
                    int puntos = (int)request.totalMoney;

                    /*bComercio bComercio = new bComercio();
                    Comercios comercio = bComercio.getComercio(115);

                    Transacciones tr = new Transacciones();
                    tr.FechaTransaccion = DateTime.Now;
                    tr.Origen = "Web";
                    tr.CodigoPremio = "";
                    tr.Descripcion = request.notes;
                    tr.Descuento = 0;
                    tr.Importe = decimal.Parse(request.totalMoney.ToString());
                    tr.ImporteAhorro = 0;
                    tr.NumCupon = "";
                    tr.NumEst = comercio.NumEst.PadLeft(15, '0');//"Web";
                    tr.NumReferencia = "";
                    tr.NumRefOriginal = "";
                    tr.NumTarjetaCliente = oTarjeta.Numero;
                    tr.NumTerminal = comercio.POSTerminal;// "Web";
                    tr.Operacion = tipo;
                    tr.PuntosAContabilizar = (tipo == "Venta" ? (puntos) : (puntos * -1));
                    tr.PuntosDisponibles = "";
                    tr.PuntosIngresados = "";
                    tr.TipoMensaje = "1100";
                    tr.TipoTransaccion = (tipo == "Venta" ? "000000" : "220000");
                    tr.UsoRed = 0;
                    tr.Puntos = comercio.POSPuntos;
                    tr.Arancel = comercio.POSArancel;
                    tr.IDMarca = oTarjeta.IDMarca;
                    tr.IDFranquicia = oTarjeta.IDFranquicia;
                    tr.Usuario = "";

                    using (var dbContext = new ACHEEntities())
                    {
                        dbContext.Transacciones.Add(tr);
                        dbContext.SaveChanges();

                        dbContext.ActualizarPuntosPorTarjeta(oTarjeta.Numero);
                    }*/

                    response.answerCode = 0;
                    response.chargedPoints = (int)request.totalMoney;

                    response.customer = new Customer();
                    response.customer.card = request.card.ToString();
                    response.customer.id = oTarjeta.Socios.IDSocio;
                    response.customer.name = oTarjeta.Socios.Nombre;
                    response.customer.surname = oTarjeta.Socios.Apellido;
                    response.customer.balance_points = oTarjeta.PuntosTotales + puntos;

                    var msg = "WS SalesByCard - Card: " + request.card + " - Money: " + request.totalMoney.ToString();
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["WSLogError"]), "Ws recibido", msg);
                }
                else
                    response.answerCode = 221;
            }
            else
                response.answerCode = 178;
        }
        else
            response.answerCode = 998;

        return response;
    }

    [WebMethod]
    public GetInfoResponse GetInfo(GetInfoRequest request)
    {
        GetInfoResponse response = new GetInfoResponse();
        if (request.sessionID != string.Empty)
        {
            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(request.card.ToString(), true);
            if (oTarjeta != null && oTarjeta.IDSocio.HasValue)
            {
                response.answerCode = 0;

                response.customer = new Customer();
                response.customer.card = request.card.ToString();
                response.customer.id = oTarjeta.Socios.IDSocio;
                response.customer.name = oTarjeta.Socios.Nombre;
                response.customer.surname = oTarjeta.Socios.Apellido;
                response.customer.balance_points = oTarjeta.PuntosTotales;

                var msg = "WS GetInfo - Card: " + request.card;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["WSLogError"]), "Ws GetInfo", msg);
            }
            else
                response.answerCode = 178;
        }
        else
            response.answerCode = 998;

        return response;
    }

    #region clases

    public class SynchroAndLoginRequest
    {
        public string serialNumber { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public long foreignID { get; set; }
    }

    public class Operator
    {
        public long id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public long language { get; set; }
    }

    public class SynchroAndLoginResponse
    {

        public int answerCode { get; set; }
        public string sessionID { get; set; }
        public Operator @operator { get; set; }
    }

    public class SaleByCardRequest
    {
        public string sessionID { get; set; }
        public long card { get; set; }
        public float dischargedCredits { get; set; }
        public float dischargedPoints { get; set; }
        public float totalMoney { get; set; }
        public string ticketID { get; set; }
        public long voucherCode { get; set; }
        public int paymentMethod { get; set; }
        public string notes { get; set; }
    }

    public class SaleByCardResponse
    {
        public int answerCode { get; set; }
        public int chargedPoints { get; set; }
        public Customer customer { get; set; }
    }

    public class Customer
    {
        public string card { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int id { get; set; }
        public int balance_points { get; set; }
    }

    public class GetInfoRequest
    {
        public string sessionID { get; set; }
        public long card { get; set; }
    }

    public class GetInfoResponse
    {
        public int answerCode { get; set; }
        public Customer customer { get; set; }
    }

    #endregion
}
