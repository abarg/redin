﻿using ACHE.Model;
using ACHE.Extensions;
using ACHE.Business;
using WS = ACHE.Model.WS4;
using PaymentGateaway;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Services.Protocols;
using System.Configuration;


using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


/// <summary>
/// Summary description for pds
/// </summary>
[WebService(Namespace = "https://fidely.azurewebsites.net/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class posv4 : System.Web.Services.WebService
{

    public enum ErrorIntegracion
    {
        SessionIDInvalido = -1,
        TerminalInvalida = -11,
        NroTarjetaInvalido = -100,
        ComercioNoEncontrado = -101,
        TransaccionAnulada = -102,
        TransaccionNoEncontrada = -103,
        ProductoNoEncontrado = -104,
        TicketUtilizadoPreviamente = -105,
        TransaccionReversada = -106,
        OperacionDeTransaccionInvalida = -107,
        NroTicketInexistente = -108,
        MontoInsuficiente = -109,
        EmpresaCelularIncorrecta = -111,
        FormatoCelularIncorrecto = -112,
        PuntosImporteInsuficientes = -113,
        TipoCanjeIncorrecto = -114,
        ValorACanjearIncorrecto = -115,
        PremioNoDisponible = -116,
        ComercioSinMarca = -117,
        MarcaInexistente = -118,
        SorteoInexistente = -119,
        SMSNoEnviado = -120,
        FechaVacia = -121,
        IntervaloFechasGrande = -122,
        ErrorEnvioSMS = -3,
        ErrorEnvioEmail = -4,
        ErrorInterno = -5,
        dniYTarjeta = -222,
        SinNotificaciones = -656,
        TarjetaAsignada = -667,
        NroDocumentoInvalido = -700,
    }

    public posv4()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Metodos Comercios

    [WebMethod]
    public WS.GetPollResponse GetPolls(WS.GetPollRequest datos)
    {
        WS.GetPollResponse info = new WS.GetPollResponse();

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                var ter = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();

                if (ter != null)
                {
                    info.sessionID = datos.sessionID;

                    var aux = dbContext.ComerciosEncuestas.Where(x => x.IDComercio == ter.IDComercio).ToList();

                    if (aux != null)
                    {

                        info.polls = aux.Select(x => new WS.Poll()
                        {
                            pollID = x.IDEncuesta,
                            question = x.Pregunta,
                            minRank = x.RangoMin,
                            maxRank = x.RangoMax,

                        }).ToList();

                        info.answerCode = 0;
                    }
                    else
                    {
                        info.answerCode = -150; // El comercio no tiene encuestas programadas
                    }

                }
                else
                {
                    info.answerCode = -1;
                }

            }
            else
            {
                info.answerCode = -1;
            }

        }

        return info;

    }

    [WebMethod]
    public WS.GetInfoResponse GetInfo(WS.GetInfoRequest datos)
    {
        WS.GetInfoResponse info = new WS.GetInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null)
                {
                    info.answerCode = 0;
                    info.customerName = aux.IDSocio.HasValue ? aux.Socios.Nombre : "";
                    info.customerLastName = aux.IDSocio.HasValue ? aux.Socios.Apellido : "";
                    info.customerID = aux.IDSocio ?? 0;
                    info.points = aux.PuntosTotales;
                    info.credits = aux.Credito;
                    info.sessionID = datos.sessionID;
                    info.type = aux.TipoTarjeta;
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                }
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
            return info;
        }

    }

    [WebMethod]
    public WS.GetShopInfoResponse GetShopInfo(WS.GetShopInfoRequest datos)
    {
        WS.GetShopInfoResponse info = new WS.GetShopInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null)
                {
                    info.answerCode = 0;
                    info.shopID = terminal.Comercios.IDComercio;
                    info.name = terminal.Comercios.NombreFantasia;
                    info.address = terminal.Comercios.Domicilios.Domicilio;
                    info.phone = terminal.Comercios.Telefono;
                    info.docNumber = terminal.Comercios.NroDocumento;
                    info.terminal = datos.terminal;
                    info.establishment = terminal.POSEstablecimiento;

                    if (terminal.Comercios.IDMarca.HasValue)
                    {
                        info.showProducts = dbContext.Productos.Any(x => x.Activo == true && x.IDMarca == terminal.Comercios.IDMarca.Value);
                        var marca = dbContext.Marcas.Where(x => x.IDMarca == terminal.Comercios.IDMarca).FirstOrDefault();
                        if (marca != null)
                        {
                            info.inputPayments = marca.POSMostrarFormaPago;
                            info.inputTicket = marca.POSMostrarNumeroTicket;
                            info.footer1 = marca.POSFooter1 ?? "";
                            info.footer2 = marca.POSFooter2 ?? "";
                            info.footer3 = marca.POSFooter3 ?? "";
                            info.footer4 = marca.POSFooter4 ?? "";
                            info.showMenuFidelity = marca.POSMostrarMenuFidelidad;
                            info.showMenuGift = marca.POSMostrarMenuGift;
                            info.showChargeGift = marca.POSMostrarChargeGift;
                            info.showLot = true; //comercio.Marca.POSMostrarChargeGift;
                            info.printLogo = marca.POSMostrarLOGO;
                            info.urlGestion = marca.urlGestion;

                            info.logo = marca.IDMarca;

                            if (marca.DefaultViewID.HasValue)
                            {
                                info.defaultView = marca.TerminalDefaultViewOptions.View;
                            }
                            else
                                info.defaultView = "NONE";

                            info.logoAppRoute = "https://redin.com.ar" + marca.rutaLogoApp ?? "";
                            info.logoTicketRoute = "https://redin.com.ar" + marca.rutaLogoTicket ?? "";
                            info.colorApp = marca.colorApp ?? "";

                            info.showEmailNotification = 
                            info.showSMSNotification = marca.notiShowSMS;

                            info.showPoll = marca.showPoll ?? false;


                            var auxMenu = dbContext.MarcasMenu.Where(x => x.IDMarca == marca.IDMarca).FirstOrDefault();

                            if (auxMenu != null)
                            {

                                info.menu = dbContext.MarcasMenu.Where(x => x.IDMarca == marca.IDMarca).Select(x => new WS.MarcaMenu()
                                {
                                    nombre = x.Nombre,
                                    posicion = x.posicion

                                }).ToList();

                            }

                            if (terminal.Comercios.PaymentGateawayTokens.Any())
                            {
                                info.paymentGateaways = dbContext.PaymentGateawayTokens
                                    .Where(x => x.IDComercio == terminal.IDComercio)
                                    .Select(x => new WS.PaymentGateaway()
                                    {
                                        gateaway = x.PaymentGateaways.Nombre,
                                        publicKey = x.PublicKey,
                                        customColor = "#fff",
                                        logo = x.PaymentGateaways.logo,
                                        requiredPayerIdentity = x.PaymentGateaways.requiredPayerIdentity,
                                        hasInstallments = x.PaymentGateaways.hasInstallments,
                                        isCrypto = x.PaymentGateaways.isCrypto,
                                        allowEmail = marca.notiShowEmail ?? false,
                                        allowSMS = marca.notiShowEmail ?? false

                                    }).ToList();
                            }


                        }
                        else
                        {
                            info.answerCode = (int)ErrorIntegracion.MarcaInexistente;
                        }
                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.ComercioSinMarca;
                    }

                    bComercio bComercio = new bComercio();
                    info.discount = bComercio.obtenerDescuento(terminal, dbContext, null);
                    info.sessionID = datos.sessionID;

                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public WS.GetDetailedTransactionsResponse GetDetailedTransactions(WS.GetDetailedTransactionsRequest datos)
    {
        WS.GetDetailedTransactionsResponse info = new WS.GetDetailedTransactionsResponse();
        try
        {

            int idComercioAjustesPuntos = 0;
            //string nombreComercioAjustesPuntos = ConfigurationManager.AppSettings["ComercioAjustesPuntos.Nombre"];
            int idTerminalAjustesPuntos = int.Parse(ConfigurationManager.AppSettings["TransaccionPuntos.IDTerminal"]);
            int idcanjes = int.Parse(ConfigurationManager.AppSettings["IDComercioCanjes"]);

            using (var dbContext = new ACHEEntities())
            {

                var user = dbContext.MarcasTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault();

                if (user != null)
                {
                    int idMarca = user.UsuariosMarcas.IDMarca;

                    var transaccionesQuery = dbContext.TransaccionesMarcasView
                    .Where(x => x.IDMarca == idMarca && x.ImporteOriginal > 1)
                    .OrderByDescending(x => x.FechaTransaccion)
                    .AsQueryable();

                    if (idTerminalAjustesPuntos > 0)
                        idComercioAjustesPuntos = dbContext.Terminales.Where(x => x.IDTerminal == idTerminalAjustesPuntos).FirstOrDefault().IDComercio;

                    transaccionesQuery = transaccionesQuery.Where(x => x.FechaTransaccion >= datos.dateFrom);

                    transaccionesQuery = transaccionesQuery.Where(x => x.FechaTransaccion <= datos.dateTo);

                    info.transacciones = transaccionesQuery.ToList();

                    info.sessionID = datos.sessionID;
                    info.answerCode = 0;

                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
                }
            }
        }
        catch (Exception e)
        {
            info.answerCode = 500;
        }

            return info;
        }

    [WebMethod]
    public WS.GetProductsInfoResponse GetProducts(WS.GetProductosInfoRequest datos)
    {
        WS.GetProductsInfoResponse info = new WS.GetProductsInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null)
                {
                    info.AnswerCode = 0;
                    var familiasProd = dbContext.FamiliaProductos.Where(x => x.IDMarca == terminal.Comercios.IDMarca).Take(10).ToList();
                    info.SessionID = datos.sessionID;
                    Products productos = new Products();
                    List<Family> familyList = new List<Family>();
                    foreach (var fam in familiasProd)
                    {
                        Family familia = new Family();
                        familia.familyID = fam.IDFamiliaProducto;
                        familia.name = fam.Nombre;
                        var prodFamilia = dbContext.Productos.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.IDFamilia == fam.IDFamiliaProducto).Take(20).ToList();
                        List<Product> listProd = new List<Product>();
                        foreach (var prod in prodFamilia)
                        {
                            Product producto = new Product();
                            producto.productID = prod.IDProducto;
                            producto.Name = prod.Nombre;
                            producto.price = prod.Precio;
                            producto.Code = prod.Codigo;
                            producto.Description = prod.Descripcion;
                            listProd.Add(producto);
                        }
                        familia.list = listProd;
                        familyList.Add(familia);
                        productos.FamilyList = familyList;
                    }
                    info.Products = productos;
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public WS.GetTotalTransactionsResponse GetTotalTransactions(WS.GetTotalTransactionsRequest datos)
    {
        WS.GetTotalTransactionsResponse info = new WS.GetTotalTransactionsResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                //var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                var aux = dbContext.Transacciones.Where(x => x.IDTerminal == datos.idTerminal).ToList();

                if (aux != null)
                {
                    DateTime fechaDsd;
                    DateTime fechaHst;
                  
                    if (!string.IsNullOrEmpty(datos.dateSince) || !string.IsNullOrEmpty(datos.dateUntil))
                    {
                        fechaDsd = DateTime.Parse(datos.dateSince);
                        fechaHst = DateTime.Parse(datos.dateUntil);
                        var aux2 = (fechaHst - fechaDsd).Days;
                        if (aux2 > 90)
                        {
                            info.answerCode = (int)ErrorIntegracion.IntervaloFechasGrande;
                            return info;
                        }
                        else
                        {
                            fechaHst = fechaHst.AddDays(1);
                            aux = aux.Where(x => x.FechaTransaccion <= fechaHst && x.FechaTransaccion >= fechaDsd).ToList();
                        }

                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.FechaVacia;
                        return info;
                    }

                    var compras = aux.Where(x => x.Operacion.ToLower() == "compra").ToList();
                    info.amountPurchases = compras.Sum(x => x.Importe).ToString();
                    info.quantityPurchases = compras.Count();


                    var ventas = aux.Where(x => x.Operacion.ToLower() == "venta").ToList();
                    info.amountSales = ventas.Sum(x => x.Importe).ToString();
                    info.quantitySales = ventas.Count();

                    var anulacion = aux.Where(x => x.Operacion.ToLower() == "anulacion").ToList();
                    info.amountAnnulment = anulacion.Sum(x => x.Importe).ToString();
                    info.quantityAnnulment = anulacion.Count();

                    var carga = aux.Where(x => x.Operacion.ToLower() == "carga").ToList();
                    info.amountLoads = carga.Sum(x => x.Importe).ToString();
                    info.quantityLoads = carga.Count();

                    var descarga = aux.Where(x => x.Operacion.ToLower() == "descarga").ToList();
                    info.amountDownloads = descarga.Sum(x => x.Importe).ToString();
                    info.quantityDownloads = descarga.Count();

                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    #endregion

    #region Metodos Beneficios

    [WebMethod]
    public WS.NewReviewResponse NewReview(WS.NewReviewRequest datos)
    {
        WS.NewReviewResponse info = new WS.NewReviewResponse();

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                // TODO: VALIDAR TERMINAL CONTRA SESSIONID
                var auxTerminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();


                // busco encuesta
                var encuesta = dbContext.ComerciosEncuestas.Where(x => x.IDEncuesta == datos.pollID).FirstOrDefault();

                if(encuesta != null) { 

                    ComerciosReviews review = new ComerciosReviews();

                    review.Rating = datos.rating;
                    review.IDTerminal = auxTerminal.IDTerminal;
                    review.IDEncuesta = encuesta.IDEncuesta;

                    if (datos.cardNumber != "")
                    {

                        Tarjetas tarjeta = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).FirstOrDefault();
                        if (tarjeta != null)
                        {
                            review.IDSocio = tarjeta.IDSocio;
                        }
                        else
                        {
                            info.answerCode = -100;
                            return info;
                        }

                    }

                    dbContext.ComerciosReviews.Add(review);
                    dbContext.SaveChanges();

                    info.answerCode = 0;

                    return info;
                }
                else
                {
                    info.answerCode = -223;
                    return info;
                }
            }
            else
            {
                info.answerCode = -1;
                return info;
            }
        }
    }


    [WebMethod]
    public WS.CancelTransactionResponse CancelTransactionByID(WS.CancelTransactionRequest datos)
    {
        WS.CancelTransactionResponse info = new WS.CancelTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = null;

                if (datos.txMethod == "DNI")
                {
                    oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                }
                else if (datos.txMethod == "NFC")
                {
                    oTarjeta = bTarjeta.getTarjetaPorNFC(datos.nfcCode);
                }
                else
                {
                    oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                }

                if (oTarjeta == null)
                {
                    info.answerCode = -100;
                    return info;
                }

                var aux = dbContext.Transacciones.Where(x => x.IDTransaccion == datos.transactionID && x.NumTarjetaCliente == oTarjeta.Numero && x.Terminales.POSTerminal == datos.terminal).FirstOrDefault();
                if (aux != null)
                {
                    if (aux.Operacion == "Venta")
                    {
                        //Valido que no se haya anulado previamente
                        var trVieja = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == aux.IDTransaccion.ToString() && x.NumTarjetaCliente == oTarjeta.Numero && x.NumTerminal == datos.terminal).FirstOrDefault();
                        if (trVieja == null)
                        {
                            var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                            if (terminal != null)
                            {
                                Transacciones tr = new Transacciones();
                                tr.FechaTransaccion = DateTime.Now;
                                tr.Origen = "POS";
                                tr.CodigoPremio = aux.CodigoPremio;
                                tr.Descripcion = "Anulacion via Pds de ID " + datos.transactionID;
                                tr.Descuento = aux.Descuento;
                                tr.Importe = aux.Importe;
                                tr.ImporteAhorro = aux.ImporteAhorro;
                                tr.NumCupon = "";
                                tr.NumEst = aux.NumEst.PadLeft(15, '0'); ;
                                tr.NumReferencia = "";
                                tr.NumRefOriginal = aux.IDTransaccion.ToString();
                                tr.NumTarjetaCliente = aux.NumTarjetaCliente;
                                tr.NumTerminal = aux.NumTerminal;
                                tr.Operacion = "Anulacion";

                                tr.PuntosAContabilizar = aux.PuntosAContabilizar * -1;
                                tr.PuntosDisponibles = aux.PuntosDisponibles;
                                tr.PuntosIngresados = aux.PuntosIngresados;
                                tr.TipoMensaje = "1100";

                                tr.PuntoDeVenta = aux.PuntoDeVenta;
                                tr.NroComprobante = "";
                                tr.TipoComprobante = "";


                                tr.TipoTransaccion = "220000";
                                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;
                                tr.Puntos = aux.Puntos;
                                tr.Arancel = aux.Arancel;

                                //var comercio = dbContext.Comercios.Where(x => x.IDComercio == Terminal.IDComercio).FirstOrDefault();

                                tr.IDMarca = terminal.Comercios.IDMarca;
                                tr.IDTerminal = terminal.IDTerminal;
                                tr.IDComercio = terminal.IDComercio;
                                tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                                tr.Usuario = "POS";

                                dbContext.Transacciones.Add(tr);
                                dbContext.SaveChanges();
                                dbContext.ActualizarPuntosPorTarjeta(aux.NumTarjetaCliente);

                                info.sessionID = datos.sessionID;
                                info.answerCode = 0;
                                info.discount = tr.Descuento ?? 0;
                                info.discountAmount = tr.ImporteAhorro ?? 0;
                                info.points = tr.PuntosAContabilizar ?? 0;
                                info.totalPoints = dbContext.Tarjetas.Where(x => x.Numero == oTarjeta.Numero).Select(x => x.PuntosTotales).FirstOrDefault();
                                info.networkCost = tr.UsoRed;
                                info.credits = tr.Importe ?? 0;
                                info.transactionID = tr.IDTransaccion;
                                info.totalFinal = info.credits - info.discountAmount + info.networkCost;

                            }
                            else
                            {
                                info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                            }
                        }
                        else
                        {
                            info.answerCode = (int)ErrorIntegracion.TransaccionAnulada;
                        }
                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                    }
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.TransaccionNoEncontrada;
                }
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public WS.NewTransactionResponse NewTransaction(WS.NewTransactionRequest datos)
    {
        WS.NewTransactionResponse info = new WS.NewTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                RenovarFechaValida(datos.sessionID, dbContext);

                //var terminal = datos.terminal;
                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {

                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = null;

                    if (datos.txMethod == "DNI")
                    {
                        oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                    }
                    else if(datos.txMethod == "NFC")
                    {
                        oTarjeta = bTarjeta.getTarjetaPorNFC(datos.nfcCode);
                    }
                    else
                    {
                        oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                    }

                    if (oTarjeta != null && oTarjeta.TipoTarjeta == "B" && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                    {
                        bComercio bComercio = new bComercio();
                        Transacciones tr = new Transacciones();
                        tr.FechaTransaccion = DateTime.Now;
                        tr.Origen = "POS";
                        tr.CodigoPremio = "";
                        tr.Descripcion = datos.observation;
                        int descuento = bComercio.obtenerDescuento(terminal, dbContext, oTarjeta);
                        tr.Descuento = descuento;
                        tr.Importe = datos.totalPrice;
                        if (descuento > 0)
                            tr.ImporteAhorro = (datos.totalPrice * descuento) / 100;
                        else
                            tr.ImporteAhorro = 0;
                        tr.NumCupon = "";
                        tr.NumEst = terminal.NumEst.PadLeft(15, '0');
                        tr.NumReferencia = "";
                        tr.NumRefOriginal = "";
                        tr.NumTarjetaCliente = oTarjeta.Numero;
                        tr.Operacion = "Venta";
                        //tr.PuntosAContabilizar = (int)(datos.totalPrice - tr.ImporteAhorro);

                        int puntosAContabilizar = (int)(tr.Importe - tr.ImporteAhorro);
                        decimal POSpuntos = bComercio.obtenerPuntos(terminal, oTarjeta, dbContext);
                        tr.Puntos = POSpuntos;
                        tr.PuntosAContabilizar = puntosAContabilizar * bComercio.obtenerMulPuntos(terminal, dbContext, tr.NumTarjetaCliente);

                        tr.PuntosDisponibles = "000000000000";
                        tr.PuntosIngresados = (datos.totalPrice.ToString().Replace(",", "")).PadLeft(12, '0');
                        tr.TipoMensaje = "1100";
                        tr.PuntoDeVenta = "";
                        tr.NroComprobante = datos.ticket;
                        tr.TipoComprobante = "Ticket";
                        tr.TipoTransaccion = "000000";
                        tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                        tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, oTarjeta);
                        tr.IDMarca = terminal.Comercios.IDMarca;
                        tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                        tr.IDComercio = terminal.IDComercio;
                        tr.IDTerminal = terminal.IDTerminal;
                        //dbContext.Transacciones.Add(tr);

                        tr.TransaccionesDetalle = new List<TransaccionesDetalle>();

                        bool isValid = true;
                        foreach (var product in datos.products)
                        {
                            var p = dbContext.Productos.Where(x => x.IDProducto == product.productID && x.IDMarca == terminal.Comercios.IDMarca).ToList();
                            if (p.Any())
                            {
                                TransaccionesDetalle trDetalle = new TransaccionesDetalle();
                                trDetalle.IDProducto = product.productID;
                                trDetalle.Precio = product.price;
                                trDetalle.Cantidad = product.quantity;
                                tr.TransaccionesDetalle.Add(trDetalle);

                                //dbContext.TransaccionesDetalle.Add(trDetalle);
                            }
                            else
                            {
                                isValid = false;
                            }
                        }

                        if (isValid)
                        {

                            // agregamos operador a la tx en caso de que sea un usuario de comercio
                            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                            {
                                tr.UsuarioComercioID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;
                                tr.Usuario = "Comercio";
                                tr.NumTerminal = null;
                            }
                            else
                            {
                                tr.Usuario = "POS";
                                tr.NumTerminal = datos.terminal;
                            }

                            dbContext.Transacciones.Add(tr);
                            dbContext.SaveChanges();
                            dbContext.ActualizarPuntosPorTarjeta(oTarjeta.Numero);

                            info.sessionID = datos.sessionID;
                            info.answerCode = 0;
                            info.discount = tr.Descuento ?? 0;
                            info.discountAmount = tr.ImporteAhorro ?? 0;
                            info.points = tr.PuntosAContabilizar ?? 0;
                            info.totalPoints = dbContext.Tarjetas.Where(x => x.Numero == oTarjeta.Numero).Select(x => x.PuntosTotales).FirstOrDefault();
                            info.networkCost = tr.UsoRed;
                            info.credits = tr.Importe ?? 0;
                            info.transactionID = tr.IDTransaccion;
                            info.totalFinal = info.credits - info.discountAmount + info.networkCost;
                            info.hasSale = false;

                            var dtHoy = DateTime.Now;
                            var promo = dbContext.Promociones.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.FechaHasta >= dtHoy && x.FechaDesde <= dtHoy && x.Activo).FirstOrDefault();
                            if (promo != null)
                            {
                                info.hasSale = true;
                                info.sale = new ACHE.Model.Sale();
                                info.sale.Title = promo.Titulo;
                                info.sale.Message1 = promo.Mensaje1;
                                info.sale.Message2 = promo.Mensaje2;
                                info.sale.Message3 = promo.Mensaje3;
                                info.sale.Message4 = promo.Mensaje4;
                                info.sale.TypeCode = promo.TipoCodigo;
                                info.sale.InformationEncoded = promo.InformacionACodificar;
                                info.sale.DateFrom = promo.FechaDesde;
                                info.sale.DateUp = promo.FechaHasta;
                            }

                        }
                        else
                        {
                            info.answerCode = (int)ErrorIntegracion.ProductoNoEncontrado;
                        }

                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
                //}
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public WS.NewNotificationsResponse SendNotifications(WS.NewNotificationsRequest datos)
    {

        WS.NewNotificationsResponse info = new WS.NewNotificationsResponse();

        string nombreSocio = "";

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                var tr = dbContext.Transacciones.Where(t => t.IDTransaccion == datos.transactionID).FirstOrDefault();

                if (tr != null)
                {

                    // recupero socio y comercio para personalizar
                    var socio = dbContext.Tarjetas.Where(t => t.Numero == tr.NumTarjetaCliente).FirstOrDefault().Socios;
                    string nombreComercio = dbContext.Comercios.Where(c => c.IDComercio == tr.IDComercio).FirstOrDefault().NombreFantasia;

                    if (socio != null)
                    {
                        nombreSocio = socio.Nombre;
                    }

                    if (datos.sendSMS)
                    {

                        string body = "";

                        try
                        {

                            TwilioClient.Init(ConfigurationManager.AppSettings["Twilio.SID"], ConfigurationManager.AppSettings["Twilio.Auth"]);

                            switch (tr.Operacion)
                            {
                                case "Carga":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;
                                case "Venta":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;
                                case "Canje":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;

                                default:
                                    Console.WriteLine("Default case");
                                    break;
                            }



                            var message = MessageResource.Create(
                                 new PhoneNumber(datos.phoneNumber),
                                 from: new PhoneNumber("+18652902590"),
                                 body: body
                             );

                            info.sentSMS = true;

                        }
                        catch (Exception e)
                        {
                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le sms error", e.ToString());
                            info.answerCode = -3;
                        }

                    }
                    else
                    {
                        info.sentSMS = false;
                    }

                    if (datos.sendEmail)
                    {

                        string body = @"Estimado/a " + nombreSocio + ", <br> <br> Has sumado " + tr.PuntosAContabilizar +
                                            " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";

                        try
                        {
                            ListDictionary replacements = new ListDictionary();
                            replacements.Add("<MENSAJE>", body);

                            info.sentEmail = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, replacements, datos.email, ConfigurationManager.AppSettings["Email.Transaccional"], "Gracias por elegirnos - " + nombreComercio);
                        }
                        catch (Exception e)
                        {
                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le error", e.ToString());
                            info.answerCode = -4;
                        }
                    }
                    else
                    {
                        info.sentEmail = false;
                    }


                    return info;
                }
                else
                {
                    info.answerCode = -103; // transaccion no encontrada
                }
            }
        }


        info.answerCode = -1;
        return info;
    }

    [WebMethod]
    public ReverseTransactionResponse ReverseTransaction(ReverseTransactionRequest datos)
    {

        ReverseTransactionResponse info = new ReverseTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = null;

                if (datos.txMethod == "DNI")
                {
                    oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                }
                else if (datos.txMethod == "NFC")
                {
                    oTarjeta = bTarjeta.getTarjetaPorNFC(datos.nfcCode);
                }
                else
                {
                    oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                }

                if(oTarjeta == null)
                {
                    info.AnswerCode = -100;
                    return info;
                }

                Transacciones transaccion;
                //Busco la TR a anular
                transaccion = dbContext.Transacciones.Where(x => x.NroComprobante == datos.ticket && x.Importe == datos.totalPrice && x.Terminales.POSTerminal == datos.terminal &&
                                                x.NumTarjetaCliente == oTarjeta.Numero && x.FechaTransaccion.Year == DateTime.Now.Year
                                                            && x.FechaTransaccion.Month == DateTime.Now.Month
                                                            && x.FechaTransaccion.Day == DateTime.Now.Day)
                                                            .OrderByDescending(x => x.IDTransaccion).FirstOrDefault();

                if (transaccion != null)
                {

                    bool validType = ACHE.Business.Common.validateTypeAgainstTx(datos.type, transaccion.Operacion);

                    if (validType == true)
                    {

                        int tx = ACHE.Business.Common.Reverse(dbContext, transaccion, datos.type);

                        if (tx == 0)
                        {
                            info.SessionID = datos.sessionID;
                            info.AnswerCode = 0;
                        }
                        else
                        {
                            info.AnswerCode = tx;
                        }

                    }
                    else
                    {
                        info.AnswerCode = -405; // operacion invalida
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.TransaccionNoEncontrada;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

            }
        }
        return info;
    }

    [WebMethod]
    public WS.SwapResponse Swap(WS.SwapRequest datos)
    {
        WS.SwapResponse info = new WS.SwapResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {

                    bTarjeta bTarjeta = new bTarjeta();

                    Tarjetas oTarjeta = null;

                    if (datos.txMethod == "DNI")
                    {
                        oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                    }
                    else if(datos.txMethod == "NFC")
                    {
                        oTarjeta = bTarjeta.getTarjetaPorNFC(datos.nfcCode);
                    }
                    else
                    {
                        oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                    }

                    if (oTarjeta != null && oTarjeta.TipoTarjeta == "B" && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                    {
                        decimal currentImporte = 0;

                        if (datos.type == "P")//por puntos
                        {
                            currentImporte = decimal.Parse((int.Parse(datos.value) / 100).ToString());
                        }
                        else if (datos.type == "I")//por importe
                        {
                            currentImporte = decimal.Parse(datos.value);
                        }
                        else
                            info.answerCode = (int)ErrorIntegracion.TipoCanjeIncorrecto;

                        if (datos.value == "" || datos.value == "0")
                            info.answerCode = (int)ErrorIntegracion.ValorACanjearIncorrecto;

                        else
                        {

                            try
                            {
                                string idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", currentImporte, "", terminal.NumEst, "", "", oTarjeta.Numero, datos.terminal, "Canje", "000000000000", "1100", "", "", "", "POS");

                                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                {
                                    int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;
                                    int idTr = int.Parse(idTransaccion);
                                    Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == idTr).FirstOrDefault();
                                    tr.UsuarioComercioID = operadorID;
                                    dbContext.SaveChanges();
                                }

                                if (idTransaccion != "" && int.Parse(idTransaccion) > 0)
                                {
                                    //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                                    var aux = dbContext.Tarjetas.Where(x => x.IDTarjeta == oTarjeta.IDTarjeta).FirstOrDefault();

                                    info.sessionID = datos.sessionID;
                                    info.answerCode = 0;
                                    info.totalPoints = aux.PuntosTotales;
                                    info.networkCost = 0;
                                    info.credits = aux.Credito;
                                    info.transactionID = int.Parse(idTransaccion);
                                }
                                else
                                    info.answerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                            }
                            catch (Exception ex)
                            {

                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ex ", ex.ToString());


                                if (ex.Message.Contains("premio"))
                                    info.answerCode = (int)ErrorIntegracion.PremioNoDisponible;
                                else if (ex.Message.Contains("suficiente"))
                                    info.answerCode = (int)ErrorIntegracion.PuntosImporteInsuficientes;
                                else
                                    info.answerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                            }
                        }
                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
                //}
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }


    [WebMethod]
    public WS.NewMemberResponse CreateMember(WS.NewMemberRequest datos)
    {

        WS.NewMemberResponse info = new WS.NewMemberResponse();

        try
        {

            using (var dbContext = new ACHEEntities())
            {

                // validamos IDSession
                if (!ValidarSessionIDComercio(datos.sessionID, dbContext))
                {
                    info.answerCode = -1;
                    return info;
                }

                int idSocio = 0;

                // validamos que no exista el socio con el mismo doc, si ya existe, le asignamos una nueva tarjeta
                if (datos.nroDocumento != "")
                {
                    var aux = dbContext.Socios.FirstOrDefault(s => s.NroDocumento == datos.nroDocumento && s.TipoDocumento == datos.tipoDoc);
                    if (aux != null)
                    {
                        idSocio = aux.IDSocio;
                    }
                }

                // recuperamos terminal
                Terminales terminal = dbContext.Terminales.FirstOrDefault(x => x.POSTerminal == datos.terminal);

                // insertamos socio
                if (idSocio == 0)
                {
                    Socios socio = bSocio.crearSocioWS(datos, terminal, dbContext);

                    idSocio = socio.IDSocio;
                }

                int idMarca = terminal.Comercios.IDMarca ?? 0;

                Tarjetas tarjeta = null;
                // creamos y/o asignamos tarjeta
                if(datos.hasCard == false)
                {
                    tarjeta = bTarjeta.CrearTarjetaWS(idSocio, idMarca, dbContext);
                }
                else
                {
                    tarjeta = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).FirstOrDefault();
                    if(tarjeta == null)
                    {
                        info.answerCode = -100;
                        return info;
                    }


                    if (tarjeta.IDSocio == null)
                    {
                        tarjeta.IDSocio = idSocio;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.TarjetaAsignada; // la tarjeta ya tiene un socio asignado
                        return info;
                    }

                }

                info.memberID = idSocio;
                info.nroTarjeta = tarjeta.Numero;
                info.answerCode = 0;

                return info;
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod]
    public WS.LotResponse Lot(WS.LotRequest datos)
    {
        WS.LotResponse info = new WS.LotResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {

                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, true);
                    if (oTarjeta != null && oTarjeta.FechaBaja == null)
                    {
                        bComercio bComercio = new bComercio();

                        var dtHoy = DateTime.Now;
                        var sorteo = dbContext.Sorteos.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.Activo && x.FechaHasta >= dtHoy && x.FechaDesde <= dtHoy).FirstOrDefault();
                        if (sorteo != null)
                        {
                            TransaccionesSorteos trSorteo = new TransaccionesSorteos();
                            trSorteo.IDSorteo = sorteo.IDSorteo;
                            trSorteo.IDTarjeta = oTarjeta.IDTarjeta;
                            trSorteo.Fecha = DateTime.Now;
                            dbContext.TransaccionesSorteos.Add(trSorteo);
                            dbContext.SaveChanges();

                            info.sessionID = datos.sessionID;
                            info.title = sorteo.Titulo;
                            info.message1 = sorteo.Mensaje1;
                            info.message2 = sorteo.Mensaje2;
                            info.message3 = sorteo.Mensaje3;
                            info.message4 = sorteo.Mensaje4;
                            info.cardNumber = oTarjeta.Numero;
                            if (sorteo.FechaDesde.HasValue)
                                info.dateFrom = sorteo.FechaDesde.Value.ToString("dd/MM/yyyy");
                            if (sorteo.FechaHasta.HasValue)
                                info.dateUp = sorteo.FechaHasta.Value.ToString("dd/MM/yyyy");
                            if (oTarjeta.IDSocio != null)
                                info.customer = oTarjeta.Socios.Nombre + "  " + oTarjeta.Socios.Apellido;
                            else
                                info.customer = "";
                        }
                        else
                        {
                            info.answerCode = (int)ErrorIntegracion.SorteoInexistente;
                        }
                    }
                    else
                    {
                        info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    #endregion

    #region Metodos Giftcard

    [WebMethod]
    public WS.ChargeGiftcardResponse ChargeGiftcard(WS.ChargeGiftcardRequest datos)
    {
        WS.ChargeGiftcardResponse info = new WS.ChargeGiftcardResponse();
        info.sessionID = datos.sessionID;

        bool enviarSMS = false;

        using (var dbContext = new ACHEEntities())
        {


            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == "G" && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null && (aux.FechaBaja == null || (aux.FechaBaja.HasValue && aux.FechaBaja.Value > DateTime.Now)))
                {
                    if (datos.amount > 0)
                    {
                        var terminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                        if (terminal != null)
                        {
                            if (datos.cellPhone != string.Empty)
                            {
                                var compania = datos.cellPhoneCompany.ToLower();
                                if (compania == "claro" || compania == "movistar" || compania == "nextel" || compania == "personal")
                                {
                                    int i;
                                    bool bNum = int.TryParse(datos.cellPhone, out i);
                                    if (datos.cellPhone.Length < 10 || !bNum)
                                    {
                                        enviarSMS = true;
                                    }
                                    else
                                        info.answerCode = (int)ErrorIntegracion.FormatoCelularIncorrecto;
                                }
                                else
                                    info.answerCode = (int)ErrorIntegracion.EmpresaCelularIncorrecta;
                            }

                            try
                            {
                                var idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", datos.amount,
                                        "", terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Carga", "000000000000", "2200", "", "", "", "POS");


                                //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber); Lo hace el metodo CrearTransaccion

                                info.answerCode = 0;
                                info.transactionID = int.Parse(idTr);

                                //Vuelvo a chequear los datos
                                //var newAux = dbContext.Tarjetas.Where(x => x.IDTarjeta == aux.IDTarjeta).FirstOrDefault();
                                //info.Points = newAux.PuntosTotales;
                                info.credits = aux.Giftcard + datos.amount;// hago esto ya que el actualizar puntos no funciona

                                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                {
                                    int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;

                                    Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == info.transactionID).FirstOrDefault();
                                    tr.UsuarioComercioID = operadorID;
                                    dbContext.SaveChanges();
                                }
                            }
                            catch (Exception e)
                            {
                                info.answerCode = -5;
                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le chargegif1t error", e.ToString());
                            }

                            if (enviarSMS)
                            {

                                //TelefonoSMS telefono = new TelefonoSMS();
                                //telefono.Celular = "541128268507";
                                //telefono.EmpresaCelular = "movistar";

                                //PlusMobile.SendSMS("nueva transaccion", telefono);
                            }
                        }
                        else
                            info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                    }
                    else
                        info.answerCode = (int)ErrorIntegracion.MontoInsuficiente;
                }
                else
                    info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;

        }
        return info;
    }

    [WebMethod]
    public WS.DischargeGiftcardResponse DischargeGiftcard(WS.DischargeGiftcardRequest datos)
    {
        WS.DischargeGiftcardResponse info = new WS.DischargeGiftcardResponse();
        info.sessionID = datos.sessionID;


        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == "G" && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null && (aux.FechaBaja == null || (aux.FechaBaja.HasValue && aux.FechaBaja.Value > DateTime.Now)))
                {
                    if (datos.amount > 0 && datos.amount <= aux.Giftcard)
                    {
                        var Terminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                        if (Terminal != null)
                        {

                            var idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, Terminal.IDTerminal, "POS", "", "", datos.amount,
                            "", Terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Descarga", "000000000000", "2200", "", "", "", "POS");

                            //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber); Lo hace el metodo CrearTransaccion

                            info.answerCode = 0;
                            info.transactionID = int.Parse(idTr);

                            //Vuelvo a chequear los datos
                            //var newAux = dbContext.Tarjetas.Where(x => x.IDTarjeta == aux.IDTarjeta).FirstOrDefault();
                            //info.Points = newAux.PuntosTotales;
                            info.credits = aux.Giftcard - datos.amount;// hago esto ya que el actualizar puntos no funciona

                            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                            {
                                int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;

                                Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == info.transactionID).FirstOrDefault();
                                tr.UsuarioComercioID = operadorID;
                                dbContext.SaveChanges();
                            }

                        }
                        else
                            info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                    }
                    else
                        info.answerCode = (int)ErrorIntegracion.MontoInsuficiente;
                }
                else
                    info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
        }

        return info;
    }


    #endregion

    #region Consulta

    [WebMethod]
    public WS.GetMemberResponse GetMember(WS.GetMemberRequest datos)
    {

        var info = new WS.GetMemberResponse();

        using (var dbContext = new ACHEEntities())
        {
            var user = dbContext.MarcasTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault();

            if (user != null)
            {
                var marca = dbContext.Marcas.First(x => x.IDMarca == user.UsuariosMarcas.IDMarca);

                if (datos.nationalID != "")
                {

                    var member = dbContext.Socios.Where(x => x.NroDocumento == datos.nationalID).FirstOrDefault();

                    if (member != null)
                    {
                        info.memberName = member.Nombre;
                        info.memberLastName = member.Apellido;
                        info.memberCellphone = member.Celular;
                        info.memberEmail = member.Email;
                        info.memberID = member.IDSocio;

                        info.cards = member.Tarjetas.Select(x => new WS.Card()
                        {
                            number = x.Numero,
                            points = x.PuntosTotales,
                            type = x.TipoTarjeta
                        }).ToList();

                        info.answerCode = 0;


                    }
                    else
                    {
                        info.answerCode = -404; // no se encontro el socio
                    }
                }

                info.sessionID = datos.sessionID;

                return info;
                
            }
            else
            {
                info.answerCode = -1; // answer code -1
                return info;
            }

        }
    }

    [WebMethod]
    public WS.GetShopsResponse GetShops(WS.GetTerminalRequest datos)
    {

        var info = new WS.GetShopsResponse();
        try { 
            using (var dbContext = new ACHEEntities())
            {
                var user = dbContext.MarcasTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault();

                if(user != null)
                {
                    var marca = dbContext.Marcas.First(x => x.IDMarca == user.UsuariosMarcas.IDMarca);

                    if(String.IsNullOrEmpty(datos.filters.location)) {

                        info.shops = marca.Comercios.Where(x => x.Activo).Select( x => new WS.Shop()
                        {
                            shopID = x.IDComercio,
                            shopName = x.NombreFantasia,
                            address = x.Domicilios.Domicilio ?? " " + "," + x.Domicilios.Domicilio ?? " ",
                            category = x.Rubros !=null ? x.Rubros.Nombre : "",
                            logoURL = "https://www.redin.com.ar/files/logos/" + x.Logo,
                            city = x.Domicilios.Ciudad == null ? "" : x.Domicilios.Ciudades.Nombre,
                            latitude = x.Domicilios.Latitud ?? " ",
                            longitude = x.Domicilios.Longitud ?? " ",
                            pointsMultiplier1 = (x.Terminales.FirstOrDefault() != null ) ? x.Terminales.FirstOrDefault().MultiplicaPuntos1 : 0,
                            pointsMultiplier2 = (x.Terminales.FirstOrDefault() != null ) ? x.Terminales.FirstOrDefault().MultiplicaPuntos2 : 0,
                            pointsMultiplier3 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().MultiplicaPuntos3 : 0,
                            pointsMultiplier4 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().MultiplicaPuntos4 : 0,
                            pointsMultiplier5 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().MultiplicaPuntos5 : 0,
                            pointsMultiplier6 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().MultiplicaPuntos6 : 0,
                            pointsMultiplier7 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().MultiplicaPuntos7 : 0,
                            discounts1 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento : 0,
                            discounts2 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento2 : 0,
                            discounts3 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento3 : 0,
                            discounts4 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento4 : 0,
                            discounts5 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento5 : 0,
                            discounts6 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento6 : 0,
                            discounts7 = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().Descuento7 : 0,
                            benefitDescription = (x.Terminales.FirstOrDefault() != null) ? x.Terminales.FirstOrDefault().DescuentoDescripcion : ""
                        }).ToList();

                    }
                    else
                    {

                        info.shops = marca.Comercios.Where(x => x.Domicilios.Ciudades != null &&  x.Domicilios.Ciudades.Nombre == datos.filters.location).Select(x => new WS.Shop()
                        {
                            shopName = x.NombreFantasia,
                            address = x.Domicilios.Domicilio + "," + x.Domicilios.Ciudades.Nombre
                        }).ToList();

                    }

                    info.answerCode = 0;
                    info.sessionID = datos.sessionID;

                    return info;
                }
                else
                {
                    info.answerCode = -1; // answer code -1
                    return info;
                }

            }
        }
        catch(Exception e)
        {
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "error get shops", e.ToString());
            info.answerCode = -500;
            return info;
        }
    }

    [WebMethod]
    public WS.GetTerminalResponse GetTerminalBySessionID(WS.GetTerminalRequest datos)
    {

        WS.GetTerminalResponse info = new WS.GetTerminalResponse();

        using (var dbContext = new ACHEEntities())
        {

            var usu = dbContext.ComerciosTokens.Where(p => p.SessionID == datos.sessionID).FirstOrDefault();

            if (usu != null)
            {

                var ter = dbContext.Terminales.Where(t => t.POSTerminal == usu.POSTerminal && t.Activo).FirstOrDefault();

                if (ter != null)
                {

                    info.answerCode = 0;
                    info.terminal = ter.POSTerminal;

                }
                else
                    info.answerCode = -11;
            }
            else
                info.answerCode = -1;
        }

        return info;
    }

    [WebMethod]
    public WS.GetLocationsResponse GetLocations(WS.GetLocationsRequest datos)
    {

        WS.GetLocationsResponse info = new WS.GetLocationsResponse();

        using(var dbContext = new ACHEEntities())
            if(ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            { 
                try
                {
                    info.locations = Common.GetLocations(datos);
                    info.answerCode = 0;
                    info.sessionID = datos.sessionID;
                }
                catch(Exception e)
                {
                    info.answerCode = 500;
                }
            }
           else
            {
                info.answerCode = -1;
            }

        return info;
    }

    [WebMethod]
    public WS.CheckPointsResponse CheckPoints(WS.CheckPointsRequest datos)
    {
        WS.CheckPointsResponse info = new WS.CheckPointsResponse();
        info.sessionID = datos.sessionID;

        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == datos.type && !x.FechaBaja.HasValue && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null)
                {
                    info.answerCode = 0;
                    if (datos.type == "G")
                        info.credits = aux.Giftcard;
                    else
                        info.credits = aux.Credito;

                    info.points = aux.PuntosTotales;
                    if (info.credits < 0)
                        info.credits = 0;
                    if (info.points < 0)
                        info.points = 0;
                }
                else
                    info.answerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
        }
        return info;
    }

    [WebMethod]
    public WS.CheckPointsByDocumentResponse CheckPointsByDocument(WS.CheckPointsByDocumentRequest datos)
    {
        WS.CheckPointsByDocumentResponse info = new WS.CheckPointsByDocumentResponse();
        info.sessionID = datos.sessionID;

        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.Socios.NroDocumento == datos.nroDocument && !x.FechaBaja.HasValue).FirstOrDefault();
                if (aux != null)
                {
                    info.answerCode = 0;
                    if (aux.TipoTarjeta == "G")
                        info.credits = aux.Giftcard;
                    else
                        info.credits = aux.Credito;

                    info.points = aux.PuntosTotales;
                    if (info.credits < 0)
                        info.credits = 0;
                    if (info.points < 0)
                        info.points = 0;
                }
                else
                    info.answerCode = (int)ErrorIntegracion.NroDocumentoInvalido;
            }
            else
                info.answerCode = (int)ErrorIntegracion.SessionIDInvalido;
        }
        return info;
    }

    #endregion

    #region Metodos Login

    [WebMethod]
    public WS.LoginResponse Login(WS.LoginRequest datos)
    {

        WS.LoginResponse info = new WS.LoginResponse();
        using (var dbContext = new ACHEEntities())
        {
            var aux = dbContext.Integraciones.Where(x => x.Usuario == datos.userName && x.Pwd == datos.pwd && x.Activo).FirstOrDefault();
            if (aux != null)
            {
                Guid g = Guid.NewGuid();
                IntegracionesTokens entity = new IntegracionesTokens();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.IDUsuario = aux.IDUsuario;
                entity.SessionID = g.ToString();
                dbContext.IntegracionesTokens.Add(entity);
                dbContext.SaveChanges();
                info.answerCode = 0;
                info.sessionID = entity.SessionID;
                Operador op = new Operador();
                op.Name = datos.userName;
                op.LastName = datos.userName;
                info.operador = op;
            }
            else
            {
                info.answerCode = -10;
            }
        }
        return info;
    }

    [WebMethod]
    public WS.LoginResponse LoginMarca(WS.LoginRequest datos)
    {
        WS.LoginResponse info = new WS.LoginResponse();

        try
        { 
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.UsuariosMarcas.Where(x => x.Usuario == datos.userName && x.Pwd == datos.pwd && x.Activo).FirstOrDefault();
                if (aux != null)
                {
                    Guid g = Guid.NewGuid();
                    MarcasTokens entity = new MarcasTokens();
                    entity.FechaValidez = DateTime.Now.AddHours(3);
                    entity.IDUsuario = aux.IDUsuario;
                    entity.SessionID = g.ToString();
                    dbContext.MarcasTokens.Add(entity);
                    dbContext.SaveChanges();
                    info.answerCode = 0;
                    info.sessionID = entity.SessionID;
                    Operador op = new Operador();
                    op.Name = datos.userName;
                    op.LastName = datos.userName;
                    info.operador = op;
                }
                else
                {
                    info.answerCode = -10;
                }
            }
            return info;

        }
        catch (Exception e)
        {
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "error login marca", e.ToString());
            info.answerCode = -500;
            return info;
        }
    }

    [WebMethod]
    public WS.LoginComercioResponse LoginComercio(WS.LoginComercioRequest datos)
    {

        WS.LoginComercioResponse info = new WS.LoginComercioResponse();

        using (var dbContext = new ACHEEntities())
        {

            var usu = dbContext.UsuariosComercios.Where(
                    p => p.Usuario == datos.userName && p.Pwd == datos.pwd && p.Activo && p.Comercios.Terminales.Any(t => t.POSTerminal == datos.terminal)).FirstOrDefault();

            if (usu != null)
            {
                // valido que la terminal sea del comercio iniciando sesion
                var ter = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal && x.IDComercio == usu.IDComercio).FirstOrDefault();

                if (ter != null)
                {
                    Guid g = Guid.NewGuid();
                    ComerciosTokens entity = new ComerciosTokens();
                    entity.FechaValidez = DateTime.Now.AddHours(3);
                    entity.IDUsuario = usu.IDUsuario;
                    entity.SessionID = g.ToString();
                    entity.POSTerminal = datos.terminal;
                    dbContext.ComerciosTokens.Add(entity);
                    dbContext.SaveChanges();


                    info.answerCode = 0;
                    info.sessionID = entity.SessionID;
                    info.terminalID = ter.IDTerminal;
                    Operador op = new Operador();
                    op.Name = datos.userName;
                    op.LastName = datos.userName;
                    info.operador = op;
                }
                else
                    info.answerCode = -10;


            }
            else
                info.answerCode = -10;
        }

        return info;
    }

    #endregion

    #region notificaciones

    [WebMethod]
    public WS.RegisterFCMTokenResponse RegisterFCMToken(WS.RegisterFCMTokenRequest datos)
    {
        WS.RegisterFCMTokenResponse info = new WS.RegisterFCMTokenResponse();

        using (var dbContext = new ACHEEntities())
        {

            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                var ter = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();

                if(ter != null)
                {

                    // valido que el ultimo token de la terminal no esta activo
                    var tokens = ter.TerminalFCMTokens;
                    if (tokens.Any() == false || (tokens.Any() && tokens.OrderByDescending(x => x.IDToken).FirstOrDefault().Activo == false) )
                    {

                        TerminalFCMTokens entity;

                        entity = new TerminalFCMTokens();

                        entity.Token = datos.token;
                        entity.Activo = true;
                        entity.IDTerminal = ter.IDTerminal;

                        dbContext.TerminalFCMTokens.Add(entity);
                   
                        dbContext.SaveChanges();

                        info.sessionID = datos.sessionID;
                        info.answerCode = 0;

                    }
                    else
                    {

                        info.answerCode = -13; // la terminal ya esta registrada
                    }
     
                }
                else
                {
                    info.answerCode = -11; // no se encontro la terminal
                }

            }
            else
            {
                info.answerCode = -10; // sessionID invalido
            }

        }

        return info;
    }

    [WebMethod]
    public WS.UnRegisterFCMTokenResponse UnRegisterFCMToken(WS.UnRegisterFCMTokenRequest datos)
    {
        WS.UnRegisterFCMTokenResponse info = new WS.UnRegisterFCMTokenResponse();

        using (var dbContext = new ACHEEntities())
        {

            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                var ter = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();

                if (ter != null)
                {

                    //valido que el ultimo token de la terminal este activo
                    if(ter.TerminalFCMTokens.Any() && ter.TerminalFCMTokens.OrderByDescending(x => x.IDToken).FirstOrDefault().Activo)
                    {
                        var token = ter.TerminalFCMTokens.OrderByDescending(x => x.IDToken).FirstOrDefault();

                        token.Activo = false;

                        dbContext.SaveChanges();

                        info.sessionID = datos.sessionID;
                        info.answerCode = 0;
                    }
                    else
                    {
                        info.answerCode = -12; // la terminal no tiene un token activo
                    }



                }
                else
                {
                    info.answerCode = -11; // no se encontro la terminal
                }

            }
            else
            {
                info.answerCode = -10; // sessionID invalido
            }

        }

        return info;
    }

    [WebMethod]
    public WS.GetNotificationsResponse GetNotificationsByTerminal(WS.GetNotificationsRequest datos)
    {
        WS.GetNotificationsResponse info = new WS.GetNotificationsResponse();

        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var terminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null)
                {
                    var notificaciones = terminal.NotificacionesFirebase;
                    if (notificaciones.Any())
                    {
                        info.notificaciones = terminal.NotificacionesFirebase.Select(x => new WS.Notification()
                        {
                            notificationID = x.IDNotificacion,
                            title = x.title,
                            message = x.Mensaje,
                            action = x.action,
                            isRead = x.Leido,
                            dateCreated = x.FechaAlta
                        }).ToList();
                    }
                    else
                    {
                        info.notificaciones = null;
                    }
                }
                else
                {
                    info.answerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
        }
        catch (Exception e)
        {
            info.answerCode = -501;
        }

        return info;
    }

    [WebMethod]
    public WS.MarkNotificationResponse MarkNotificationAsRead(WS.MarkNotificationRequest datos)
    {
        WS.MarkNotificationResponse info = new WS.MarkNotificationResponse();

        try
        {
            using (var dbContext = new ACHEEntities())
            {

                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                {

                    NotificacionesFirebase notificacion = dbContext.NotificacionesFirebase.FirstOrDefault(x => x.IDNotificacion == datos.notificationID);

                    if (notificacion != null)
                    {
                        notificacion.Leido = true;

                        dbContext.SaveChanges();
                        info.answerCode = 0;
                        info.sessionID = datos.sessionID;
                    }
                    else
                    {
                        info.answerCode = -404; // no se encontro la notificacion
                    }
                }
                else
                {
                    info.answerCode = -1;
                }
            }

        }
        catch (Exception e)
        {
            info.answerCode = -5;
        }

        return info;
    }

    #endregion

    #region metodos validacion/autenticacion

    [WebMethod]
    public WS.RenewSessionResponse RenewSession(WS.RenewSessionRequest datos)
    {

        WS.RenewSessionResponse info = new WS.RenewSessionResponse();
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault();
            if (entity != null)
            {
                Guid g = Guid.NewGuid();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.SessionID = g.ToString();
                dbContext.SaveChanges();

                info.answerCode = 0;
                info.sessionID = entity.SessionID;
                info.validFor = 10800;

            }
            else
                info.answerCode = -1;
        }

        return info;

    }

    private bool ValidarSessionID(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    private bool ValidarSessionIDComercio(string sessionID, ACHEEntities dbContext)
    {
        var aux = dbContext.ComerciosTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }


    private void RenovarFechaValida(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }

    #endregion

    #region payment gateaways

    [WebMethod]
    public WS.CreatePaymentResponse OpenGateaway(WS.CreatePaymentRequest datos)
    {
        var info = new WS.CreatePaymentResponse();

        try
        {
            using (var dbContext = new ACHEEntities())
            {
                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                {

                    PaymentGateawayTokens tokens = bTokens.getTokens(datos.publicKey);

                    if (tokens != null)
                    {

                        WS.CreatePaymentResponse paymentStatus = new WS.CreatePaymentResponse();

                        switch (datos.paymentGateaway)
                        {
                            case ("mercadopago"):
                                paymentStatus = PaymentGateaway.Common.createMercadoPagoPayment(dbContext, datos, tokens);
                                break;
                        }


                        if (paymentStatus.paymentID != null)
                        {
                            info.paymentID = paymentStatus.paymentID;
                            info.paymentStatus = paymentStatus.paymentStatus;
                            info.paymentStatusDetail = paymentStatus.paymentStatusDetail;
                        }
                        else
                        {
                            info.answerCode = -22; // error integracion gateaway
                        }
                    }
                    else
                    {
                        info.answerCode = -11; // no existe la public
                    }
                }
                else
                {
                    info.answerCode = -1;
                }
            }
        }
        catch(Exception e)
        {
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le payment error in posv4", e.ToString());
            info.answerCode = -22;
        }


        return info;
    }

    #endregion

}
