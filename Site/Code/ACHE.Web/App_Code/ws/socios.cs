﻿using ACHE.Model;
using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Services.Protocols;
using System.Configuration;

/// <summary>
/// Summary description for socios
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class socios : System.Web.Services.WebService
{

    // Visual studio will append a "UserCredentialsValue" property to the proxy class
    public wsUserCredentials client;

    public socios()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Socios

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosAction UpdatePwd(int idSocio, string pwdActual, string pwd)
    {
        FrontSociosAction action = new FrontSociosAction();
        action.Action = false;
        action.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                Socios entity = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
                if (entity.Pwd != pwdActual)
                    throw new Exception("La contraseña actual ingresada es inválida");

                entity.Pwd = pwd;
                dbContext.SaveChanges();
                action.Action = true;
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }

        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosAction UpdateInfo(int idSocio, string email, string foto)
    {
        FrontSociosAction action = new FrontSociosAction();
        action.Action = false;
        action.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Socios.Where(x => x.Email == email.Trim()).FirstOrDefault();
                if (aux != null && aux.IDSocio != idSocio)
                    throw new Exception("El email ya se encuentra registrado.");

                Socios entity = dbContext.Socios.Where(x => x.IDSocio == idSocio).FirstOrDefault();
                //entity.Name = name;
                //entity.LastName = lastName;
                entity.Email = email;
                //if (pwd != string.Empty)
                //    entity.Pwd = pwd;

                dbContext.SaveChanges();
                action.Action = true;
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }

        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosAction RecuperarPwd(string nroDocumento)
    {
        FrontSociosAction action = new FrontSociosAction();
        action.Action = false;
        action.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var usu = dbContext.Socios.Where(x => x.NroDocumento == nroDocumento).FirstOrDefault();
                if (usu != null)
                {
                    if (usu.Email != string.Empty)
                    {

                        string newPwd = string.Empty;
                        newPwd = newPwd.GenerateRandom(6);

                        ListDictionary replacements = new ListDictionary();
                        replacements.Add("<USUARIO>", usu.Apellido + ", " + usu.Nombre);
                        replacements.Add("<PASSWORD>", newPwd);

                        bool send = EmailHelper.SendMessage(EmailTemplate.RecuperoPwd, replacements, usu.Email, "RedIN: Recupero de contraseña");
                        if (!send)
                            throw new Exception("El email con su nueva contraseña no pudo ser enviado.");
                        else
                        {
                            usu.Pwd = newPwd;
                            dbContext.SaveChanges();
                            action.Action = true;
                        }
                    }
                    else
                        throw new Exception("Usted no posee un email registrados. Por favor comuníquese con atención al cliente: 0800-220-4646 o envíe un mail a <a href='mailto:socios@redin.com.ar'>socios@redin.com.ar</a>");
                }
                else
                    throw new Exception("El documento ingresado es inexistente.");
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }

        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosInfo Login(string tipo, string numero, string pwd)
    {

        FrontSociosInfo info = new FrontSociosInfo();
        info.Info = null;
        info.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                if (tipo == "DNI")
                {
                    var socio = dbContext.Socios.Include("Domicilios").Where(x => x.NroDocumento == numero && x.Pwd == pwd && x.Tarjetas.Any()).FirstOrDefault();
                    if (socio == null)
                        throw new Exception("DNI y/o contraseña incorrecta.");
                    else
                    {
                        string dom = "";
                        if (socio.Domicilios != null)
                            dom = socio.Domicilios.Provincia + ", " + socio.Domicilios.Ciudad + ", " + socio.Domicilios.Domicilio + ", " + " " + socio.Domicilios.PisoDepto;

                        info.Info = new SociosViewModel();
                        info.Info.IDSocio = socio.IDSocio;
                        info.Info.Nombre = socio.Nombre;
                        info.Info.Apellido = socio.Apellido;
                        info.Info.Email = socio.Email;
                        info.Info.NroDocumento = socio.NroDocumento;
                        info.Info.Telefono = socio.Telefono;
                        info.Info.Celular = socio.Celular;
                        info.Info.Foto = socio.Foto;
                        info.Info.Domicilio = dom;
                    }
                }
                else
                {
                    var tarjeta = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == numero && !x.FechaBaja.HasValue).FirstOrDefault();
                    if (tarjeta == null)
                        throw new Exception("Tarjeta y/o contraseña incorrecta.");
                    else if (tarjeta.Socios == null)
                        throw new Exception("SOLICTAR");
                    else if (tarjeta.Socios.Pwd == pwd)
                    {
                        var socio = tarjeta.Socios;
                        string dom = "";
                        if (socio.Domicilios != null)
                            dom = socio.Domicilios.Provincia + ", " + socio.Domicilios.Ciudad + ", " + socio.Domicilios.Domicilio + ", " + " " + socio.Domicilios.PisoDepto;

                        info.Info = new SociosViewModel();
                        info.Info.IDSocio = socio.IDSocio;
                        info.Info.Nombre = socio.Nombre;
                        info.Info.Apellido = socio.Apellido;
                        info.Info.Email = socio.Email;
                        info.Info.NroDocumento = socio.NroDocumento;
                        info.Info.Telefono = socio.Telefono;
                        info.Info.Celular = socio.Celular;
                        info.Info.Foto = socio.Foto;
                        info.Info.Domicilio = dom;
                    }
                    else
                        throw new Exception("Tarjeta y/o contraseña incorrecta.");
                }


            }

        }
        catch (Exception ex)
        {
            info.Error = ex.Message;
        }

        return info;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosInfo LoginFb(string email)
    {

        FrontSociosInfo info = new FrontSociosInfo();
        info.Info = null;
        info.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Socios.Include("Domicilios").Where(x => x.Email == email && x.Tarjetas.Any()).FirstOrDefault();
                if (aux == null)
                    throw new Exception("Socio inexistente.");
                else
                {
                    string dom = "";
                    if (aux.Domicilios != null)
                        dom = aux.Domicilios.Provincia + ", " + aux.Domicilios.Ciudad + ", " + aux.Domicilios.Domicilio + ", " + " " + aux.Domicilios.PisoDepto;

                    info.Info = new SociosViewModel();
                    info.Info.IDSocio = aux.IDSocio;
                    info.Info.Nombre = aux.Nombre;
                    info.Info.Apellido = aux.Apellido;
                    info.Info.Email = aux.Email;
                    info.Info.NroDocumento = aux.NroDocumento;
                    info.Info.Telefono = aux.Telefono;
                    info.Info.Celular = aux.Celular;
                    info.Info.Foto = aux.Foto;
                    info.Info.Domicilio = dom;
                }
            }

        }
        catch (Exception ex)
        {
            info.Error = ex.Message;
        }

        return info;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosAction ActualizarInfoFb(int idSocio, string idAuth)
    {

        FrontSociosAction action = new FrontSociosAction();
        action.Action = false;
        action.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var socio = dbContext.Socios.Include("Domicilios").Where(x => x.IDSocio == idSocio).FirstOrDefault();
                if (socio == null)
                    throw new Exception("Usuario y/o contraseña incorrecta.");
                else
                {
                    socio.AuthTipo = "FB";
                    socio.AuthID = idAuth;

                    dbContext.SaveChanges();

                    action.Action = true;
                }
            }

        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }

        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosInfo GetSocio(int idSocio)
    {
        FrontSociosInfo info = new FrontSociosInfo();
        info.Info = null;
        info.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Socios.Include("Domicilios").Where(x => x.IDSocio == idSocio && x.Tarjetas.Any()).FirstOrDefault();
                if (aux == null)
                    throw new Exception("Usuario y/o contraseña incorrecta.");
                else
                {
                    string dom = "";
                    if (aux.Domicilios != null)
                        dom = aux.Domicilios.Provincia + ", " + aux.Domicilios.Ciudad + ", " + aux.Domicilios.Domicilio + ", " + " " + aux.Domicilios.PisoDepto;

                    info.Info = new SociosViewModel();
                    info.Info.IDSocio = aux.IDSocio;
                    info.Info.Nombre = aux.Nombre;
                    info.Info.Apellido = aux.Apellido;
                    info.Info.Email = aux.Email;
                    info.Info.NroDocumento = aux.NroDocumento;
                    info.Info.Telefono = aux.Telefono;
                    info.Info.Celular = aux.Celular;
                    info.Info.Foto = aux.Foto;
                    info.Info.Domicilio = dom;
                }
            }

        }
        catch (Exception ex)
        {
            info.Error = ex.Message;
        }

        return info;

    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosInfo GetInfoConsulta(string tipo, string numero)
    {

        FrontSociosInfo info = new FrontSociosInfo();
        info.Info = null;
        info.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                if (tipo == "DNI")
                {
                    var socio = dbContext.Socios.Include("Tarjetas").Where(x => x.NroDocumento == numero && x.Tarjetas.Any()).FirstOrDefault();
                    if (socio == null)
                        throw new Exception("DNI incorrecto.");
                    else
                    {
                        info.Info = new SociosViewModel();
                        info.Info.IDSocio = socio.IDSocio;
                        info.Info.Nombre = socio.Nombre;
                        info.Info.Apellido = socio.Apellido;
                        info.Info.Email = socio.Email;
                        info.Info.NroDocumento = socio.NroDocumento;
                        info.Info.Telefono = socio.Telefono;
                        info.Info.Celular = socio.Celular;
                        info.Info.Foto = socio.Foto;
                        info.Info.Total = socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).Sum(x => x.Credito + x.Giftcard);
                        if (info.Info.Total < 0)
                            info.Info.Total = 0;
                        info.Info.Puntos = socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).Sum(x => x.PuntosTotales);
                        if (info.Info.Puntos < 0)
                            info.Info.Puntos = 0;

                        foreach (var tar in socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).ToList())
                        {
                            decimal? aux = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == tar.Numero && x.ImporteAhorro.HasValue).AsEnumerable().Sum(x => x.ImporteAhorro.Value);
                            if (aux.HasValue)
                                info.Info.TotalAhorro += aux.Value;
                        }

                        if (info.Info.TotalAhorro < 0)
                            info.Info.TotalAhorro = 0;
                    }
                }
                else
                {
                    var tarjeta = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == numero && !x.FechaBaja.HasValue).FirstOrDefault();
                    if (tarjeta == null)
                        throw new Exception("Tarjeta incorrecta.");
                    else
                    {
                        var socio = tarjeta.Socios;
                        string dom = "";

                        info.Info = new SociosViewModel();
                        if (socio != null)
                        {
                            info.Info.IDSocio = socio.IDSocio;
                            info.Info.Nombre = socio.Nombre;
                            info.Info.Apellido = socio.Apellido;
                            info.Info.Email = socio.Email;
                            info.Info.NroDocumento = socio.NroDocumento;
                            info.Info.Telefono = socio.Telefono;
                            info.Info.Celular = socio.Celular;
                            info.Info.Foto = socio.Foto;
                            info.Info.Domicilio = "";
                        }

                        info.Info.Puntos = tarjeta.PuntosTotales;
                        if (info.Info.Puntos < 0)
                            info.Info.Puntos = 0;
                        info.Info.Total = tarjeta.Credito + tarjeta.Giftcard;
                        if (info.Info.Total < 0)
                            info.Info.Total = 0;

                        decimal? aux = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == numero && x.ImporteAhorro.HasValue).AsEnumerable().Sum(x => x.ImporteAhorro.Value);
                        if (aux.HasValue)
                        {
                            info.Info.TotalAhorro += aux.Value;
                            //info.Info.TotalAhorro = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == numero && x.ImporteAhorro.HasValue).Sum(x => x.ImporteAhorro.Value);
                            if (info.Info.TotalAhorro < 0)
                                info.Info.TotalAhorro = 0;
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
            info.Error = ex.Message;
        }

        return info;
    }

    #endregion

    #region Tarjetas

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosTarjetas GetTarjetas(int idSocio)
    {
        FrontSociosTarjetas listado = new FrontSociosTarjetas();
        listado.Tarjetas = null;
        listado.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                listado.Tarjetas = dbContext.Tarjetas.Where(x => x.IDSocio == idSocio && !x.FechaBaja.HasValue)
                    .Select(x => new TarjetasViewModel
                    {
                        Credito = x.Credito,
                        Giftcard = x.Giftcard,
                        Puntos = x.PuntosTotales,
                        Numero = x.Numero,
                        IDTarjeta = x.IDTarjeta,
                        IDMarca = x.IDMarca,
                        IDFranquicia = x.IDFranquicia,
                    }).ToList();

                foreach (var tar in listado.Tarjetas)
                {
                    tar.TotalAhorro = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == tar.Numero && x.ImporteAhorro.HasValue).AsEnumerable().Sum(x => x.ImporteAhorro.Value);
                    if (tar.TotalAhorro < 0)
                        tar.TotalAhorro = 0;
                }
            }
        }
        catch (Exception ex)
        {
            listado.Error = ex.Message;
        }

        return listado;
    }

    #endregion

    #region Transacciones

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontSociosTransacciones GetTransacciones(int idSocio, string fechaDesde, string fechaHasta)
    {
        FrontSociosTransacciones listado = new FrontSociosTransacciones();
        listado.Transacciones = null;
        listado.Error = string.Empty;

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesView
                    .Where(x => x.IDSocio == idSocio && x.ImporteOriginal > 1)// && x.FechaTransaccion >= fechaDesdeBase)
                    .OrderBy(x => x.FechaTransaccion)
                    .Select(x => new TransaccionesSociosViewModel
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion == "Venta" ? "Compra" : x.Operacion,
                        Comercio = x.NombreFantasia,
                        Tarjeta = x.Numero,
                        Marca = x.Marca,
                        ImporteOriginal = x.Operacion == "Venta" ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = x.Operacion == "Venta" ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        //Credito = x.Operacion != "Carga" && x.Operacion != "Descarga" ? (x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0) : 0,
                        //Giftcard = x.Operacion == "Carga" && x.Operacion == "Descarga" ? (x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0) : 0,

                        /*Credito = x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0,
                        Giftcard = x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0,
                        POS = x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0,
                        Total = x.PuntosAContabilizar.HasValue ? x.PuntosAContabilizar.Value / 100 : 0*/
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }

                var newList = new List<TransaccionesSociosViewModel>();
                foreach (var tr in result)
                {
                    decimal aux = decimal.Parse((tr.Puntos / 100).ToString());

                    if (tr.Operacion != "Carga" && tr.Operacion != "Descarga")
                        tr.Credito = aux;
                    else
                        tr.Giftcard = aux;
                    tr.POS = Math.Round(aux);
                    tr.Total = aux;

                    newList.Add(tr);
                }

                listado.Transacciones = newList.ToList();
            }
        }
        catch (Exception ex)
        {
            listado.Error = ex.Message;
        }

        return listado;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontTransaccionesAction InsertTransaccionesCanje(List<TarjetasViewModel> tarjetas, int puntos)
    {
        FrontTransaccionesAction action = new FrontTransaccionesAction();
        action.Action = false;
        action.Transacciones = new List<TransaccionCarga>();
        action.Error = string.Empty;

        string numEst = ConfigurationManager.AppSettings["Canje.NumEst"] ?? "000000000004606";
        string numTerminal = ConfigurationManager.AppSettings["Canje.NumTerminal"] ?? "00004606";
        string premio = ConfigurationManager.AppSettings["Canje.CodigoPremio"] ?? "";

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            if (!tarjetas.Any())
                throw new Exception("No hay tarjetas");

            using (var dbContext = new ACHEEntities())
            {
                decimal credito = decimal.Divide(puntos, 100);
                foreach (var tarj in tarjetas)
                {
                    if (puntos > 0)
                    {
                        TransaccionCarga transaccion = new TransaccionCarga();
                        transaccion.NroTarjeta = tarj.Numero;
                        transaccion.Error = false;
                        try
                        {

                            #region transaccion
                            Transacciones tr = new Transacciones();
                            tr.FechaTransaccion = DateTime.Now;
                            tr.Origen = "Web";
                            tr.CodigoPremio = "";
                            tr.Descripcion = string.Empty;
                            tr.Descuento = 0;
                            tr.Importe = credito;
                            tr.ImporteAhorro = 0;
                            tr.NumCupon = string.Empty;
                            tr.NumEst = numEst;
                            tr.NumReferencia = string.Empty;
                            tr.NumRefOriginal = string.Empty;
                            tr.NumTarjetaCliente = tarj.Numero;
                            tr.NumTerminal = numTerminal;
                            tr.Operacion = "Canje";
                            //tr.PuntosAContabilizar = ((int)(credito * 100)) * -1;
                            tr.PuntosDisponibles = "000000000000";
                            //tr.PuntosIngresados = (credito.ToString().Replace(",", "")).PadLeft(12, '0');
                            tr.TipoMensaje = "1100";
                            tr.PuntoDeVenta = string.Empty;
                            tr.NroComprobante = string.Empty;
                            tr.TipoComprobante = string.Empty;
                            tr.TipoTransaccion = "000005";
                            tr.CodigoPremio = premio;
                            tr.UsoRed = 0;
                            tr.Puntos = 1;
                            tr.Arancel = 0;
                            tr.UsoRed = 0;
                            tr.Usuario = "ws";
                            tr.IDMarca = tarj.IDMarca;
                            tr.IDFranquicia = tarj.IDFranquicia;

                            if (tarj.Puntos > 0)
                            {
                                if (tarj.Puntos >= puntos) {
                                    tr.PuntosAContabilizar = puntos * -1;
                                    tr.PuntosIngresados = (credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                    puntos = 0;
                                }
                                else {
                                    tr.PuntosAContabilizar = tarj.Puntos * -1;
                                    tr.PuntosIngresados = (tarj.Credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                    puntos -= tarj.Puntos;
                                }
                            }
                            dbContext.Transacciones.Add(tr);
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            transaccion.Error = true;
                        }
                        action.Transacciones.Add(transaccion);
                    }
                }

                dbContext.SaveChanges();

                foreach (var tarj in tarjetas)
                    dbContext.ActualizarPuntosPorTarjeta(tarj.Numero);
                action.Action = true;
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }
        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontTransaccionesAction InsertTransaccionesCanjePin(List<TarjetasViewModel> tarjetas, int puntos)
    {
        FrontTransaccionesAction action = new FrontTransaccionesAction();
        action.Action = false;
        action.Error = string.Empty;
        action.Transacciones = new List<TransaccionCarga>();

        string numEst = ConfigurationManager.AppSettings["Canje.NumEst"] ?? "000000000000276";
        string numTerminal = ConfigurationManager.AppSettings["Canje.NumTerminal"] ?? "17765203";
        string premio = ConfigurationManager.AppSettings["Canje.CodigoPremio"] ?? "";

        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");
            using (var dbContext = new ACHEEntities())
            {
                decimal credito = decimal.Divide(puntos, 100);
                foreach (var tarj in tarjetas)
                {
                    if (puntos > 0)
                    {
                        TransaccionCarga transaccion = new TransaccionCarga();
                        transaccion.NroTarjeta = tarj.Numero;
                        transaccion.Error = false;

                        #region transacción
                        Transacciones tr = new Transacciones();
                        tr.FechaTransaccion = DateTime.Now;
                        tr.Origen = "Web";
                        tr.CodigoPremio = "";
                        tr.Descripcion = string.Empty;
                        tr.Descuento = 0;
                        tr.Importe = credito;
                        tr.ImporteAhorro = 0;
                        tr.NumCupon = string.Empty;
                        tr.NumEst = numEst;
                        tr.NumReferencia = string.Empty;
                        tr.NumRefOriginal = string.Empty;
                        tr.NumTarjetaCliente = tarj.Numero;
                        tr.NumTerminal = numTerminal;
                        tr.Operacion = "Canje";
                        tr.PuntosDisponibles = "000000000000";
                        tr.TipoMensaje = "1100";
                        tr.PuntoDeVenta = string.Empty;
                        tr.NroComprobante = string.Empty;
                        tr.TipoComprobante = string.Empty;
                        tr.TipoTransaccion = "000005";
                        tr.CodigoPremio = premio;
                        tr.UsoRed = 0;
                        tr.Puntos = 1;
                        tr.Arancel = 0;
                        tr.Usuario = string.Empty;
                        tr.IDMarca = tarj.IDMarca;
                        tr.IDFranquicia = tarj.IDFranquicia;
                        tr.Usuario = "ws";

                        if (tarj.Puntos > 0)
                        {
                            if (tarj.Puntos >= puntos) {
                                tr.PuntosAContabilizar = puntos * -1;
                                tr.PuntosIngresados = (credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                puntos = 0;
                            }
                            else {
                                tr.PuntosAContabilizar = tarj.Puntos * -1;
                                tr.PuntosIngresados = (tarj.Credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                puntos -= tarj.Puntos;
                            }
                        }
                        dbContext.Transacciones.Add(tr);
                        #endregion
                        action.Transacciones.Add(transaccion);
                    }
                }

                dbContext.SaveChanges();

                foreach (var tarj in tarjetas)
                    dbContext.ActualizarPuntosPorTarjeta(tarj.Numero);
                action.Action = true;
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }
        return action;
    }

    #endregion

    #region Pines

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontPinesAction GetCostosPines(int idEmpresa)
    {
        FrontPinesAction listado = new FrontPinesAction();
        listado.Pines = null;
        listado.Error = string.Empty;
        listado.Action = false;
        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");
            using (var dbContext = new ACHEEntities())
            {
                string sql = "SELECT DISTINCT(CostoPuntos) FROM Pines WHERE IDEmpresaCanje = " + idEmpresa + " AND IDSocio IS NULL ORDER BY CostoPuntos";
                var pines = dbContext.Database.SqlQuery<int>(sql, new object[] { }).ToList();
                if (pines != null && pines.Count() > 0)
                {
                    listado.Pines = pines;
                    listado.Action = true;
                }
            }
        }
        catch (Exception ex)
        {
            listado.Error = ex.Message;
        }
        return listado;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontEmpresasCanje EmpresasCanje()
    {
        FrontEmpresasCanje listado = new FrontEmpresasCanje();
        listado.Empresas = null;
        listado.Error = string.Empty;
        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");

            using (var dbContext = new ACHEEntities())
            {
                listado.Empresas = dbContext.EmpresasCanje
                .Where(x => x.Activo)
                .Select(x => new EmpresasCanjeViewModel
                {
                    Empresa = x.Nombre,
                    Descripcion = x.Descripcion,
                    Link = x.UsaPines ? "pines.aspx?Id=" + x.IDEmpresaCanje : x.UrlInterna,
                    Logo = x.Logo != null ? "/files/canjes/" + x.Logo : "/files/canjes/no-photo.jpg",
                }).ToList();
            }
        }
        catch (Exception ex)
        {
            listado.Error = ex.Message;
        }
        return listado;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontPinInfo GetPin(int costo, int idEmpresa)
    {
        FrontPinInfo action = new FrontPinInfo();
        action.Error = string.Empty;
        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Pines
                    .Include("EmpresasCanje")
                    .Where(x => x.CostoPuntos == costo && x.IDEmpresaCanje == idEmpresa && x.IDSocio == null)
                    .Select(x => new PinesViewModel()
                    {
                        IDPin = x.IDPin,
                 //       Codigo = x.Codigo,
                        EmpresaCanje = x.EmpresasCanje.Nombre,
                        CostoInterno = x.CostoInterno,
                        CostoPuntos = x.CostoPuntos
                    }).FirstOrDefault();
                if (entity != null)
                {
                    action.Info = entity;
                }
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }
        return action;
    }

    [WebMethod]
    [SoapHeader("client", Required = true)]
    public FrontPinAction AsignarSocio(int idPin, int idSocio)
    {
        FrontPinAction action = new FrontPinAction();
        action.Action = false;
        action.Error = string.Empty;
        try
        {
            if (!checkAccess())
                throw new Exception("Error in authentication");
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Pines.Where(x => x.IDPin == idPin && !x.IDSocio.HasValue).FirstOrDefault();
                if (entity != null)
                {
                    entity.IDSocio = idSocio;
                    entity.FechaCompra = DateTime.Now;
                }
                else
                    throw new Exception("No se ha encontrado el pin");
            }
        }
        catch (Exception ex)
        {
            action.Error = ex.Message;
        }
        return action;
    }

    #endregion

    private bool checkAccess()
    {
        // In this method you can check the username and password 
        // with your database or something
        // You could also encrypt the password for more security
        if (client != null)
        {
            if (client.userName == "socios" && client.password == "Red!n2014")
                return true;
            else
                return false;
        }
        else
            return false;

    }

    #region Clases

    [Serializable]
    public class FrontSociosTransacciones
    {
        public string Error { get; set; }
        public List<TransaccionesSociosViewModel> Transacciones { get; set; }
    }

    [Serializable]
    public class FrontSociosTarjetas
    {
        public string Error { get; set; }
        public List<TarjetasViewModel> Tarjetas { get; set; }
    }

    [Serializable]
    public class FrontSociosInfo
    {
        public string Error { get; set; }
        public SociosViewModel Info { get; set; }
    }

    [Serializable]
    public class FrontSociosAction
    {
        public string Error { get; set; }
        public bool Action { get; set; }
    }

    [Serializable]
    public class FrontTransaccionesAction
    {
        public string Error { get; set; }
        public bool Action { get; set; }
        public List<TransaccionCarga> Transacciones { get; set; }
    }

    [Serializable]
    public class TransaccionCarga
    {
        public string NroTarjeta { get; set; }
        public bool Error { get; set; }
    }

    [Serializable]
    public class FrontPinInfo
    {
        public string Error { get; set; }
        public PinesViewModel Info { get; set; }
    }

    [Serializable]
    public class FrontPinAction
    {
        public string Error { get; set; }
        public bool Action { get; set; }
    }

    [Serializable]
    public class FrontPinesAction
    {
        public string Error { get; set; }
        public bool Action { get; set; }
        public List<int> Pines { get; set; }
    }

    [Serializable]
    public class FrontEmpresasCanje
    {
        public string Error { get; set; }
        public List<EmpresasCanjeViewModel> Empresas { get; set; }
    }

    #endregion
}
