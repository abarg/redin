﻿using ACHE.Model;
using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Services.Protocols;
using System.Configuration;
using ACHE.Business;
using WS = ACHE.Model.Ws;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

/// <summary>
/// Summary description for pds
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class posv3 : System.Web.Services.WebService
{

    public enum ErrorIntegracion
    {
        SessionIDInvalido = -1,
        TerminalInvalida = -11,
        NroTarjetaInvalido = -100,
        ComercioNoEncontrado = -101,
        TransaccionAnulada = -102,
        TransaccionNoEncontrada = -103,
        ProductoNoEncontrado = -104,
        TicketUtilizadoPreviamente = -105,
        TransaccionReversada = -106,
        OperacionDeTransaccionInvalida = -107,
        NroTicketInexistente = -108,
        MontoInsuficiente = -109,
        EmpresaCelularIncorrecta = -111,
        FormatoCelularIncorrecto = -112,
        PuntosImporteInsuficientes = -113,
        TipoCanjeIncorrecto = -114,
        ValorACanjearIncorrecto = -115,
        PremioNoDisponible = -116,
        ComercioSinMarca = -117,
        MarcaInexistente = -118,
        SorteoInexistente = -119,
        SMSNoEnviado = -120,
        FechaVacia = -121,
        IntervaloFechasGrande = -122,
        ErrorEnvioSMS = -3,
        ErrorEnvioEmail = -4,
        ErrorInterno = -5,
        dniYTarjeta = -222
    }

    public posv3()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Metodos Comercios

    [WebMethod]
    public GetPollResponse GetEncuesta(GetPollRequest datos)
    {
        GetPollResponse info = new GetPollResponse();

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                var ter = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();

                if (ter != null)
                {

                    var aux = dbContext.ComerciosEncuestas.Where(x => x.IDComercio == ter.IDComercio).FirstOrDefault();


                    info.question = aux.Pregunta;
                    info.minRank = aux.RangoMin;
                    info.maxRank = aux.RangoMax;
                    info.answerCode = 0;

                }
                else
                {
                    info.answerCode = -1;
                }

            }
            else
            {
                info.answerCode = -1;
            }

        }

        return info;

    }

    [WebMethod]
    public GetInfoResponse GetInfo(GetInfoRequest datos)
    {
        GetInfoResponse info = new GetInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null)
                {
                    info.AnswerCode = 0;
                    info.CustomerName = aux.IDSocio.HasValue ? aux.Socios.Nombre : "";
                    info.CustomerLastName = aux.IDSocio.HasValue ? aux.Socios.Apellido : "";
                    info.CustomerId = aux.IDSocio ?? 0;
                    info.Points = aux.PuntosTotales;
                    info.Credits = aux.Credito;
                    info.SessionID = datos.sessionID;
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
            return info;
        }

    }

    [WebMethod]
    public GetShopInfoResponse GetShopInfo(GetShopInfoRequest datos)
    {
        GetShopInfoResponse info = new GetShopInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null)
                {
                    info.AnswerCode = 0;
                    info.ShopId = terminal.Comercios.IDComercio;
                    info.Name = terminal.Comercios.NombreFantasia;
                    info.Address = terminal.Comercios.Domicilios.Domicilio;
                    info.Phone = terminal.Comercios.Telefono;
                    info.DocNumber = terminal.Comercios.NroDocumento;
                    info.Terminal = datos.terminal;
                    info.Establishment = terminal.POSEstablecimiento;
                    if (terminal.Comercios.IDMarca.HasValue)
                    {
                        info.ShowProducts = dbContext.Productos.Any(x => x.Activo == true && x.IDMarca == terminal.Comercios.IDMarca.Value);
                        var marca = dbContext.Marcas.Where(x => x.IDMarca == terminal.Comercios.IDMarca).FirstOrDefault();
                        if (marca != null)
                        {
                            info.InputPayments = marca.POSMostrarFormaPago;
                            info.InputTicket = marca.POSMostrarNumeroTicket;
                            info.Footer1 = marca.POSFooter1 ?? "";
                            info.Footer2 = marca.POSFooter2 ?? "";
                            info.Footer3 = marca.POSFooter3 ?? "";
                            info.Footer4 = marca.POSFooter4 ?? "";
                            info.ShowMenuFidelity = marca.POSMostrarMenuFidelidad;
                            info.ShowMenuGift = marca.POSMostrarMenuGift;
                            info.ShowChargeGift = marca.POSMostrarChargeGift;
                            info.ShowLot = true; //comercio.Marca.POSMostrarChargeGift;
                            info.PrintLOGO = marca.POSMostrarLOGO;

                            info.Logo = marca.IDMarca;

                            info.logoAppRoute = marca.rutaLogoApp ?? "";
                            info.logoTicketRoute = marca.rutaLogoTicket ?? "";
                            info.colorApp = marca.colorApp ?? "";

                            //info.showEmailNotification = marca.notiShowEmail;
                            //info.showSMSNotification = marca.notiShowSMS;

                            var auxMenu = dbContext.MarcasMenu.Where(x => x.IDMarca == marca.IDMarca).FirstOrDefault();

                            if (auxMenu != null)
                            {

                                info.Menu = dbContext.MarcasMenu.Where(x => x.IDMarca == marca.IDMarca).Select(x => new MarcaMenu()
                                {
                                    Nombre = x.Nombre,
                                    Posicion = x.posicion

                                }).ToList();

                            }

                        }
                        else
                        {
                            info.AnswerCode = (int)ErrorIntegracion.MarcaInexistente;
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.ComercioSinMarca;
                    }

                    bComercio bComercio = new bComercio();
                    info.Discount = bComercio.obtenerDescuento(terminal, dbContext, null);
                    info.SessionID = datos.sessionID;

                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public GetProductsInfoResponse GetProducts(GetProductosInfoRequest datos)
    {
        GetProductsInfoResponse info = new GetProductsInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                if (terminal != null)
                {
                    info.AnswerCode = 0;
                    var familiasProd = dbContext.FamiliaProductos.Where(x => x.IDMarca == terminal.Comercios.IDMarca).Take(10).ToList();
                    info.SessionID = datos.sessionID;
                    Products productos = new Products();
                    List<Family> familyList = new List<Family>();
                    foreach (var fam in familiasProd)
                    {
                        Family familia = new Family();
                        familia.familyID = fam.IDFamiliaProducto;
                        familia.name = fam.Nombre;
                        var prodFamilia = dbContext.Productos.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.IDFamilia == fam.IDFamiliaProducto).Take(20).ToList();
                        List<Product> listProd = new List<Product>();
                        foreach (var prod in prodFamilia)
                        {
                            Product producto = new Product();
                            producto.productID = prod.IDProducto;
                            producto.Name = prod.Nombre;
                            producto.price = prod.Precio;
                            producto.Code = prod.Codigo;
                            producto.Description = prod.Descripcion;
                            listProd.Add(producto);
                        }
                        familia.list = listProd;
                        familyList.Add(familia);
                        productos.FamilyList = familyList;
                    }
                    info.Products = productos;
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public GetTotalTransactionsResponse GetTotalTransactions(GetTotalTransactionsRequest datos)
    {
        GetTotalTransactionsResponse info = new GetTotalTransactionsResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                //var terminal = dbContext.Terminales.Include("Domicilios").Include("Comercios").Where(x => x.POSTerminal == datos.terminal).FirstOrDefault();
                var aux = dbContext.Transacciones.Where(x => x.IDTerminal == datos.idTerminal).ToList();

                if (aux != null)
                {
                    DateTime fechaDsd;
                    DateTime fechaHst;


                    if (!string.IsNullOrEmpty(datos.dateSince) || !string.IsNullOrEmpty(datos.dateUntil))
                    {
                        fechaDsd = DateTime.Parse(datos.dateSince);
                        fechaHst = DateTime.Parse(datos.dateUntil);
                        var aux2 = (fechaHst - fechaDsd).Days;
                        if (aux2 > 7)
                        {
                            info.AnswerCode = (int)ErrorIntegracion.IntervaloFechasGrande;
                            return info;
                        }
                        else
                        {
                            fechaHst = fechaHst.AddDays(1);
                            aux = aux.Where(x => x.FechaTransaccion <= fechaHst && x.FechaTransaccion >= fechaDsd).ToList();
                        }

                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.FechaVacia;
                        return info;
                    }

                    var compras = aux.Where(x => x.Operacion.ToLower() == "compra").ToList();
                    info.amountPurchases = compras.Sum(x => x.Importe).ToString();
                    info.quantityPurchases = compras.Count();


                    var ventas = aux.Where(x => x.Operacion.ToLower() == "venta").ToList();
                    info.amountSales = ventas.Sum(x => x.Importe).ToString();
                    info.quantitySales = ventas.Count();

                    var anulacion = aux.Where(x => x.Operacion.ToLower() == "anulacion").ToList();
                    info.amountAnnulment = anulacion.Sum(x => x.Importe).ToString();
                    info.quantityAnnulment = anulacion.Count();

                    var carga = aux.Where(x => x.Operacion.ToLower() == "carga").ToList();
                    info.amountLoads = carga.Sum(x => x.Importe).ToString();
                    info.quantityLoads = carga.Count();

                    var descarga = aux.Where(x => x.Operacion.ToLower() == "descarga").ToList();
                    info.amountDownloads = descarga.Sum(x => x.Importe).ToString();
                    info.quantityDownloads = descarga.Count();

                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    #endregion

    #region Metodos Beneficios

    [WebMethod]
    public NewReviewResponse NewReview(NewReviewRequest datos)
    {
        NewReviewResponse info = new NewReviewResponse();

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                // TODO: VALIDAR TERMINAL CONTRA SESSIONID
                int auxTerminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal).FirstOrDefault().IDTerminal;

                ComerciosReviews review = new ComerciosReviews();

                review.Rating = datos.rating;
                review.IDTerminal = auxTerminal;

                if (datos.cardNumber != "")
                {

                    Tarjetas tarjeta = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).FirstOrDefault();
                    if (tarjeta != null)
                    {
                        review.IDSocio = tarjeta.IDSocio;
                    }
                    else
                    {
                        info.answerCode = -100;
                        return info;
                    }

                }

                dbContext.ComerciosReviews.Add(review);
                dbContext.SaveChanges();

                info.answerCode = 0;

                return info;
            }
            else
            {
                info.answerCode = -1;
                return info;
            }
        }
    }


    [WebMethod]
    public CancelTransactionResponse CancelTransactionByID(CancelTransactionRequest datos)
    {
        CancelTransactionResponse info = new CancelTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Transacciones.Where(x => x.IDTransaccion == datos.idTransaction && x.NumTarjetaCliente == datos.cardNumber && x.Terminales.POSTerminal == datos.terminal).FirstOrDefault();
                if (aux != null)
                {
                    if (aux.Operacion == "Venta" || aux.Operacion == "Canje" || aux.Operacion == "Carga" || aux.Operacion == "Descarga")
                    {
                        //Valido que no se haya anulado previamente
                        var trVieja = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == aux.IDTransaccion.ToString() && x.NumTarjetaCliente == datos.cardNumber && x.Terminales.POSTerminal == datos.terminal).OrderByDescending(x => x.IDTransaccion).FirstOrDefault();

                        if(trVieja != null) {
                            // parche para admitir reversos de transacciones de anulacion ( Si se reverso la anulacion se permite anular nuevamente la transaccion original)
                            var reverso = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == trVieja.IDTransaccion.ToString()).OrderByDescending(x => x.IDTransaccion).FirstOrDefault();
                            if (reverso != null)
                            {
                                // solo se permite cancelar una vez
                                if(trVieja.IDTransaccion < reverso.IDTransaccion)
                                    trVieja = null;
                            }
                         }
                        if (trVieja == null)
                        {
                            var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                            if (terminal != null)
                            {
                                Transacciones tr = new Transacciones();
                                tr.FechaTransaccion = DateTime.Now;
                                tr.Origen = "POS";
                                tr.CodigoPremio = aux.CodigoPremio;
                                tr.Descripcion = "Anulacion via posv3 de ID " + datos.idTransaction;
                                tr.Descuento = aux.Descuento;
                                tr.Importe = aux.Importe;
                                tr.ImporteAhorro = aux.ImporteAhorro;
                                tr.NumCupon = "";
                                tr.NumEst = aux.NumEst.PadLeft(15, '0'); ;
                                tr.NumReferencia = "";
                                tr.NumRefOriginal = aux.IDTransaccion.ToString();
                                tr.NumTarjetaCliente = aux.NumTarjetaCliente;
                                tr.NumTerminal = aux.Terminales.POSTerminal;
                                tr.Operacion = "Anulacion";

                                tr.PuntosAContabilizar = aux.PuntosAContabilizar * -1;
                                tr.PuntosDisponibles = aux.PuntosDisponibles;
                                tr.PuntosIngresados = aux.PuntosIngresados;
                                if (aux.Operacion == "Venta" || aux.Operacion == "Canje")
                                    tr.TipoMensaje = "1100";
                                else
                                    tr.TipoMensaje = "2200";

                                tr.PuntoDeVenta = aux.PuntoDeVenta;
                                tr.TipoComprobante = "";


                                tr.TipoTransaccion = "220000";
                                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;
                                tr.Puntos = aux.Puntos;
                                tr.Arancel = aux.Arancel;

                                //var comercio = dbContext.Comercios.Where(x => x.IDComercio == Terminal.IDComercio).FirstOrDefault();

                                tr.IDMarca = terminal.Comercios.IDMarca;
                                tr.IDTerminal = terminal.IDTerminal;
                                tr.IDComercio = terminal.IDComercio;
                                tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                                tr.Usuario = "POS";

                                if(datos.ticket != null)
                                {
                                    tr.NroComprobante = datos.ticket;
                                }
                                else
                                {
                                    tr.NroComprobante = "";
                                }

                                dbContext.Transacciones.Add(tr);
                                dbContext.SaveChanges();
                                dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                                info.SessionID = datos.sessionID;
                                info.AnswerCode = 0;
                                info.Discount = tr.Descuento ?? 0;
                                info.DiscountAmount = tr.ImporteAhorro ?? 0;
                                info.Points = tr.PuntosAContabilizar ?? 0;
                                info.TotalPoints = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).Select(x => x.PuntosTotales).FirstOrDefault();
                                info.NetworkCost = tr.UsoRed;
                                info.Credits = tr.Importe ?? 0;
                                info.IdTransaction = tr.IDTransaccion;
                                info.TotalFinal = info.Credits - info.DiscountAmount + info.NetworkCost;

                            }
                            else
                            {
                                info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                            }
                        }
                        else
                        {
                            info.AnswerCode = (int)ErrorIntegracion.TransaccionAnulada;
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.TransaccionNoEncontrada;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public NewTransactionResponse NewTransaction(NewTransactionRequest datos)
    {
        NewTransactionResponse info = new NewTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                try
                { 
                    RenovarFechaValida(datos.sessionID, dbContext);

                    //var terminal = datos.terminal;
                    var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                    if (terminal != null)
                    {

                        bTarjeta bTarjeta = new bTarjeta();
                        Tarjetas oTarjeta = null;

                        if (datos.txMethod == "DNI")
                        {
                            oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                        }
                        else
                        {
                            oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                        }

                        if (oTarjeta != null && oTarjeta.TipoTarjeta == "B" && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                        {
                            bComercio bComercio = new bComercio();
                            Transacciones tr = new Transacciones();
                            tr.FechaTransaccion = DateTime.Now;
                            tr.Origen = "POS";
                            tr.CodigoPremio = "";
                            tr.Descripcion = datos.observation;
                            int descuento = bComercio.obtenerDescuento(terminal, dbContext, oTarjeta);
                            tr.Descuento = descuento;
                            tr.Importe = datos.totalPrice;
                            if (descuento > 0)
                                tr.ImporteAhorro = (datos.totalPrice * descuento) / 100;
                            else
                                tr.ImporteAhorro = 0;
                            tr.NumCupon = "";
                            tr.NumEst = terminal.NumEst.PadLeft(15, '0');
                            tr.NumReferencia = "";
                            tr.NumRefOriginal = "";
                            tr.NumTarjetaCliente = oTarjeta.Numero;
                            tr.Operacion = "Venta";
                            //tr.PuntosAContabilizar = (int)(datos.totalPrice - tr.ImporteAhorro);

                            int puntosAContabilizar = (int)(tr.Importe - tr.ImporteAhorro);
                            decimal POSpuntos = bComercio.obtenerPuntos(terminal, oTarjeta, dbContext);
                            tr.Puntos = POSpuntos;
                            tr.PuntosAContabilizar = puntosAContabilizar * bComercio.obtenerMulPuntos(terminal, dbContext, tr.NumTarjetaCliente);

                            tr.PuntosDisponibles = "000000000000";
                            tr.PuntosIngresados = (datos.totalPrice.ToString().Replace(",", "")).PadLeft(12, '0');
                            tr.TipoMensaje = "1100";
                            tr.PuntoDeVenta = "";
                            tr.NroComprobante = datos.ticket;
                            tr.TipoComprobante = "Ticket";
                            tr.TipoTransaccion = "000000";
                            tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                            tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, oTarjeta);
                            tr.IDMarca = terminal.Comercios.IDMarca;
                            tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                            tr.IDComercio = terminal.IDComercio;
                            tr.IDTerminal = terminal.IDTerminal;
                            //dbContext.Transacciones.Add(tr);

                            tr.TransaccionesDetalle = new List<TransaccionesDetalle>();

                            bool isValid = true;
                            foreach (var product in datos.products)
                            {
                                var p = dbContext.Productos.Where(x => x.IDProducto == product.productID && x.IDMarca == terminal.Comercios.IDMarca).ToList();
                                if (p.Any())
                                {
                                    TransaccionesDetalle trDetalle = new TransaccionesDetalle();
                                    trDetalle.IDProducto = product.productID;
                                    trDetalle.Precio = product.price;
                                    trDetalle.Cantidad = product.quantity;
                                    tr.TransaccionesDetalle.Add(trDetalle);

                                    //dbContext.TransaccionesDetalle.Add(trDetalle);
                                }
                                else
                                {
                                    isValid = false;
                                }
                            }

                            if (isValid)
                            {

                                // agregamos operador a la tx en caso de que sea un usuario de comercio
                                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                {
                                    tr.UsuarioComercioID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;
                                    tr.Usuario = "Comercio";
                                    tr.NumTerminal = null;
                                }
                                else
                                {
                                    tr.Usuario = "POS";
                                    tr.NumTerminal = datos.terminal;
                                }

                                dbContext.Transacciones.Add(tr);
                                dbContext.SaveChanges();
                                dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                                info.SessionID = datos.sessionID;
                                info.AnswerCode = 0;
                                info.Discount = tr.Descuento ?? 0;
                                info.DiscountAmount = tr.ImporteAhorro ?? 0;
                                info.Points = tr.PuntosAContabilizar ?? 0;
                                info.TotalPoints = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).Select(x => x.PuntosTotales).FirstOrDefault();
                                info.NetworkCost = tr.UsoRed;
                                info.Credits = tr.Importe ?? 0;
                                info.IdTransaction = tr.IDTransaccion;
                                info.TotalFinal = info.Credits - info.DiscountAmount + info.NetworkCost;
                                info.HasSale = false;

                                var dtHoy = DateTime.Now;
                                var promo = dbContext.Promociones.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.FechaHasta >= dtHoy && x.FechaDesde <= dtHoy && x.Activo).FirstOrDefault();
                                if (promo != null)
                                {
                                    info.HasSale = true;
                                    info.Sale = new ACHE.Model.Sale();
                                    info.Sale.Title = promo.Titulo;
                                    info.Sale.Message1 = promo.Mensaje1;
                                    info.Sale.Message2 = promo.Mensaje2;
                                    info.Sale.Message3 = promo.Mensaje3;
                                    info.Sale.Message4 = promo.Mensaje4;
                                    info.Sale.TypeCode = promo.TipoCodigo;
                                    info.Sale.InformationEncoded = promo.InformacionACodificar;
                                    info.Sale.DateFrom = promo.FechaDesde;
                                    info.Sale.DateUp = promo.FechaHasta;
                                }

                        

                            }
                            else
                            {
                                info.AnswerCode = (int)ErrorIntegracion.ProductoNoEncontrado;
                            }

                        }
                        else
                        {
                            info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                    }
                }
                catch(Exception e)
                {
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le transaction error", e.ToString());

                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    [WebMethod]
    public NewNotificationsResponse SendNotifications(NewNotificationsRequest datos)
    {

        NewNotificationsResponse info = new NewNotificationsResponse();

        string nombreSocio = "";

        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {

                var tr = dbContext.Transacciones.Where(t => t.IDTransaccion == datos.IdTransaction).FirstOrDefault();

                if (tr != null)
                {

                    // recupero socio y comercio para personalizar
                    var socio = dbContext.Tarjetas.Where(t => t.Numero == tr.NumTarjetaCliente).FirstOrDefault().Socios;
                    string nombreComercio = dbContext.Comercios.Where(c => c.IDComercio == tr.IDComercio).FirstOrDefault().NombreFantasia;

                    if (socio != null)
                    {
                        nombreSocio = socio.Nombre;
                    }

                    if (datos.sendSMS)
                    {

                        string body = "";

                        try
                        {

                            TwilioClient.Init(ConfigurationManager.AppSettings["Twilio.SID"], ConfigurationManager.AppSettings["Twilio.Auth"]);

                            switch (tr.Operacion)
                            {
                                case "Carga":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;
                                case "Venta":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;
                                case "Canje":
                                    body = @"Estimado/a " + nombreSocio + ", Has sumado " + tr.PuntosAContabilizar +
                                           " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";
                                    break;

                                default:
                                    Console.WriteLine("Default case");
                                    break;
                            }



                            var message = MessageResource.Create(
                                 new PhoneNumber(datos.numeroTelefono),
                                 from: new PhoneNumber("+18652902590"),
                                 body: body
                             );

                            info.sentSMS = true;

                        }
                        catch (Exception e)
                        {
                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le sms error", e.ToString());
                            info.AnswerCode = -3;
                        }

                    }
                    else
                    {
                        info.sentSMS = false;
                    }

                    if (datos.sendEmail)
                    {

                        string body = @"Estimado/a " + nombreSocio + ", <br> <br> Has sumado " + tr.PuntosAContabilizar +
                                            " puntos. El importe de la transaccion fue " + (tr.Importe - tr.ImporteAhorro) + ", has ahorrado  " + tr.ImporteAhorro + ".";

                        try
                        {
                            ListDictionary replacements = new ListDictionary();
                            replacements.Add("<MENSAJE>", body);

                            info.sentEmail = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, replacements, datos.email, ConfigurationManager.AppSettings["Email.Transaccional"], "Gracias por elegirnos - " + nombreComercio);
                        }
                        catch (Exception e)
                        {
                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le error", e.ToString());
                            info.AnswerCode = -4;
                        }
                    }
                    else
                    {
                        info.sentEmail = false;
                    }


                    return info;
                }
                else
                {
                    info.AnswerCode = -103; // transaccion no encontrada
                }
            }
        }


        info.AnswerCode = -1;
        return info;
    }

    [WebMethod]
    public NewTransactionResponse ReverseTransaction(NewTransactionRequest datos)
    {
        NewTransactionResponse info = new NewTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                //string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                //string fecha = DateTime.Now.ToString(formato);

                //Busco la TR a anular
                var aux = dbContext.Transacciones.Where(x => x.NroComprobante == datos.ticket && x.Operacion == "Venta" && x.Importe == datos.totalPrice && x.Terminales.POSTerminal == datos.terminal &&
                x.NumTarjetaCliente == datos.cardNumber && x.FechaTransaccion.Year == DateTime.Now.Year
                            && x.FechaTransaccion.Month == DateTime.Now.Month
                            && x.FechaTransaccion.Day == DateTime.Now.Day).FirstOrDefault();
                if (aux != null)
                {
                    //Valido que no se haya anulado previamente
                    var trVieja = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == aux.IDTransaccion.ToString() && x.NumTarjetaCliente == datos.cardNumber && x.Terminales.POSTerminal == datos.terminal).FirstOrDefault();
                    if (trVieja == null)
                    {
                        var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                        if (terminal != null)
                        {
                            bComercio bComercio = new bComercio();
                            bTarjeta bTarjeta = new bTarjeta();
                            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                            if (oTarjeta != null && oTarjeta.TipoTarjeta == "B" && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                            {
                                Transacciones tr = new Transacciones();
                                tr.FechaTransaccion = DateTime.Now;
                                tr.Origen = "POS";
                                tr.CodigoPremio = "";
                                tr.Descripcion = datos.observation;

                                tr.Descuento = aux.Descuento;
                                tr.Importe = datos.totalPrice;
                                if (tr.Descuento.HasValue && tr.Descuento.Value > 0)
                                    tr.ImporteAhorro = (datos.totalPrice * tr.Descuento.Value) / 100;
                                else
                                    tr.ImporteAhorro = 0;
                                tr.NumCupon = "";
                                tr.NumEst = terminal.NumEst.PadLeft(15, '0');
                                tr.NumReferencia = "";
                                tr.NumRefOriginal = aux.IDTransaccion.ToString();
                                tr.NumTarjetaCliente = datos.cardNumber;
                                tr.NumTerminal = datos.terminal;
                                tr.Operacion = "Anulacion";
                                //tr.PuntosAContabilizar = (int)(datos.totalPrice) * -1;
                                tr.PuntosAContabilizar = aux.PuntosAContabilizar * -1;
                                tr.PuntosDisponibles = aux.PuntosDisponibles;
                                tr.PuntosIngresados = (datos.totalPrice.ToString().Replace(",", "")).PadLeft(12, '0');
                                tr.TipoMensaje = "1100";

                                tr.PuntoDeVenta = "";

                                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                                tr.NroComprobante = datos.ticket;
                                tr.TipoComprobante = "Ticket";
                                tr.TipoTransaccion = "220000";//preguntar
                                tr.Puntos = aux.Puntos;
                                tr.Arancel = aux.Arancel;
                                tr.IDMarca = terminal.Comercios.IDMarca;
                                tr.IDFranquicia = terminal.Comercios.IDFranquicia;
                                tr.IDComercio = terminal.IDComercio;
                                tr.IDTerminal = terminal.IDTerminal;
                                tr.Usuario = "POS";
                                tr.TransaccionesDetalle = new List<TransaccionesDetalle>();

                                bool isValid = true;

                                foreach (var product in datos.products)
                                {
                                    var p = dbContext.Productos.Where(x => x.IDProducto == product.productID && x.IDMarca == terminal.Comercios.IDMarca);
                                    if (p.Any())
                                    {
                                        TransaccionesDetalle trDetalle = new TransaccionesDetalle();
                                        trDetalle.Transacciones = tr;
                                        trDetalle.IDProducto = product.productID;
                                        trDetalle.Precio = product.price;
                                        trDetalle.Cantidad = product.quantity;
                                        //dbContext.TransaccionesDetalle.Add(trDetalle);
                                        tr.TransaccionesDetalle.Add(trDetalle);
                                    }
                                    else
                                    {
                                        isValid = false;
                                    }
                                }
                                if (isValid)
                                {

                                    if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                    {
                                        int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;
                                        tr.UsuarioComercioID = operadorID;
                                    }
                                    dbContext.Transacciones.Add(tr);
                                    dbContext.SaveChanges();
                                    dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                                    info.SessionID = datos.sessionID;
                                    info.AnswerCode = 0;
                                    info.Discount = tr.Descuento ?? 0;
                                    info.DiscountAmount = tr.ImporteAhorro ?? 0;
                                    info.Points = tr.PuntosAContabilizar ?? 0;
                                    info.TotalPoints = dbContext.Tarjetas.Where(x => x.Numero == datos.cardNumber).Select(x => x.PuntosTotales).FirstOrDefault();
                                    info.NetworkCost = tr.UsoRed;
                                    info.Credits = tr.Importe ?? 0;
                                    info.IdTransaction = tr.IDTransaccion;
                                    info.TotalFinal = info.Credits - info.DiscountAmount + info.NetworkCost;
                                }
                                else
                                {
                                    info.AnswerCode = (int)ErrorIntegracion.ProductoNoEncontrado;
                                }
                            }
                            else
                            {
                                info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                            }
                        }
                        else
                        {
                            info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.TransaccionReversada;
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.TransaccionNoEncontrada;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

            }
        }
        return info;
    }

    [WebMethod]
    public ReverseTransactionResponse ReverseTransactionTA(ReverseTransactionRequest datos)
    {

        ReverseTransactionResponse info = new ReverseTransactionResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                Transacciones transaccion;
                //Busco la TR a anular
                transaccion = dbContext.Transacciones.Where(x => x.NroComprobante == datos.ticket && x.Importe == datos.totalPrice && x.Terminales.POSTerminal == datos.terminal &&
                                                x.NumTarjetaCliente == datos.cardNumber && x.FechaTransaccion.Year == DateTime.Now.Year
                                                            && x.FechaTransaccion.Month == DateTime.Now.Month
                                                            && x.FechaTransaccion.Day == DateTime.Now.Day)
                                                            .OrderByDescending(x => x.IDTransaccion).FirstOrDefault();

                if (transaccion != null)
                {

                    bool valid = validateTypeAgainstTx(datos.type, transaccion.Operacion);

                    if(valid == true)
                    {

                        int tx = ACHE.Business.Common.Reverse(dbContext, transaccion, datos.type);

                        if(tx == 0)
                        {
                            info.SessionID = datos.sessionID;
                            info.AnswerCode = 0;
                        }
                        else
                        {
                            info.AnswerCode = tx;
                        }

                    }
                    else
                    {
                        info.AnswerCode = -405; // operacion invalida
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.TransaccionNoEncontrada;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

            }
        }
        return info;
    }

    private static bool validateTypeAgainstTx(string type, string operacion)
    {

        bool valid = false;

        switch (type)
        {
            case "Swap":
                valid = operacion == "Canje";
                break;
            case "Venta":
                valid = operacion == "Venta";
                break;
            case "Charge":
                valid = operacion == "Carga";
                break;
            case "Discharge":
                valid = operacion == "Descarga";
                break;
            case "Cancel":
                valid = operacion == "Anulacion";
                break;
        }

        return valid;
    }

    [WebMethod]
    public SwapResponse Swap(SwapRequest datos)
    {
        SwapResponse info = new SwapResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {

                    bTarjeta bTarjeta = new bTarjeta();

                    Tarjetas oTarjeta = null;

                    if (datos.txMethod == "DNI")
                    {
                        oTarjeta = bTarjeta.getTarjetaPorDNI(datos.dniNumber);
                    }
                    else
                    {
                        oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, false);
                    }

                    if (oTarjeta != null && oTarjeta.TipoTarjeta == "B" && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                    {
                        decimal currentImporte = 0;

                        if (datos.type == "P")//por puntos
                        {
                            currentImporte = decimal.Parse((int.Parse(datos.value) / 100).ToString());
                        }
                        else if (datos.type == "I")//por importe
                        {
                            currentImporte = decimal.Parse(datos.value);
                        }
                        else
                            info.AnswerCode = (int)ErrorIntegracion.TipoCanjeIncorrecto;

                        if (datos.value == "" || datos.value == "0")
                            info.AnswerCode = (int)ErrorIntegracion.ValorACanjearIncorrecto;

                        else
                        {

                            try
                            {
                                string idTransaccion = "";

                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ex ", datos.ticket);


                                // implementacion ticket para transact
                                if (datos.ticket == null)
                                { 
                                     idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", currentImporte, "", terminal.NumEst, "", "", oTarjeta.Numero, datos.terminal, "Canje", "000000000000", "1100", "", "", "", "POS");
                                }
                                else
                                {
                                     idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", currentImporte, "", terminal.NumEst, "", "", oTarjeta.Numero, datos.terminal, "Canje", "000000000000", "1100", "", datos.ticket, "", "POS");
                                }


                                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                {
                                    int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;
                                    int idTr = int.Parse(idTransaccion);
                                    Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == idTr).FirstOrDefault();
                                    tr.UsuarioComercioID = operadorID;
                                    dbContext.SaveChanges();
                                }

                                if (idTransaccion != "" && int.Parse(idTransaccion) > 0)
                                {
                                    //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber);

                                    var aux = dbContext.Tarjetas.Where(x => x.IDTarjeta == oTarjeta.IDTarjeta).FirstOrDefault();

                                    info.SessionID = datos.sessionID;
                                    info.AnswerCode = 0;
                                    info.TotalPoints = aux.PuntosTotales;
                                    info.NetworkCost = 0;
                                    info.Credits = aux.Credito;
                                    info.IdTransaction = int.Parse(idTransaccion);
                                }
                                else
                                    info.AnswerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                            }
                            catch (Exception ex)
                            {

                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "ex ", ex.ToString());


                                if (ex.Message.Contains("premio"))
                                    info.AnswerCode = (int)ErrorIntegracion.PremioNoDisponible;
                                else if (ex.Message.Contains("suficiente"))
                                    info.AnswerCode = (int)ErrorIntegracion.PuntosImporteInsuficientes;
                                else
                                    info.AnswerCode = (int)ErrorIntegracion.OperacionDeTransaccionInvalida;
                            }
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
                //}
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }


    [WebMethod]
    public LotResponse Lot(LotRequest datos)
    {
        LotResponse info = new LotResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);

                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {

                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(datos.cardNumber, true);
                    if (oTarjeta != null && oTarjeta.FechaBaja == null)
                    {
                        bComercio bComercio = new bComercio();

                        var dtHoy = DateTime.Now;
                        var sorteo = dbContext.Sorteos.Where(x => x.IDMarca == terminal.Comercios.IDMarca && x.Activo && x.FechaHasta >= dtHoy && x.FechaDesde <= dtHoy).FirstOrDefault();
                        if (sorteo != null)
                        {
                            TransaccionesSorteos trSorteo = new TransaccionesSorteos();
                            trSorteo.IDSorteo = sorteo.IDSorteo;
                            trSorteo.IDTarjeta = oTarjeta.IDTarjeta;
                            trSorteo.Fecha = DateTime.Now;
                            dbContext.TransaccionesSorteos.Add(trSorteo);
                            dbContext.SaveChanges();

                            info.sessionID = datos.sessionID;
                            info.Title = sorteo.Titulo;
                            info.Message1 = sorteo.Mensaje1;
                            info.Message2 = sorteo.Mensaje2;
                            info.Message3 = sorteo.Mensaje3;
                            info.Message4 = sorteo.Mensaje4;
                            info.CardNumber = oTarjeta.Numero;
                            if (sorteo.FechaDesde.HasValue)
                                info.DateFrom = sorteo.FechaDesde.Value.ToString("dd/MM/yyyy");
                            if (sorteo.FechaHasta.HasValue)
                                info.DateUp = sorteo.FechaHasta.Value.ToString("dd/MM/yyyy");
                            if (oTarjeta.IDSocio != null)
                                info.Customer = oTarjeta.Socios.Nombre + "  " + oTarjeta.Socios.Apellido;
                            else
                                info.Customer = "";
                        }
                        else
                        {
                            info.AnswerCode = (int)ErrorIntegracion.SorteoInexistente;
                        }
                    }
                    else
                    {
                        info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
                    }
                }
                else
                {
                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                }
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
            }
        }
        return info;
    }

    #endregion

    #region Metodos Giftcard

    [WebMethod]
    public ChargeGiftcardResponse ChargeGiftcard(ChargeGiftcardRequest datos)
    {
        ChargeGiftcardResponse info = new ChargeGiftcardResponse();
        info.SessionID = datos.sessionID;

        bool enviarSMS = false;

        using (var dbContext = new ACHEEntities())
        {


            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == "G" && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null && (aux.FechaBaja == null || (aux.FechaBaja.HasValue && aux.FechaBaja.Value > DateTime.Now)))
                {
                    if (datos.amount > 0)
                    {
                        var terminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                        if (terminal != null)
                        {
                            if (datos.cellPhone != string.Empty)
                            {
                                var compania = datos.cellPhoneCompany.ToLower();
                                if (compania == "claro" || compania == "movistar" || compania == "nextel" || compania == "personal")
                                {
                                    int i;
                                    bool bNum = int.TryParse(datos.cellPhone, out i);
                                    if (datos.cellPhone.Length < 10 || !bNum)
                                    {
                                        enviarSMS = true;
                                    }
                                    else
                                        info.AnswerCode = (int)ErrorIntegracion.FormatoCelularIncorrecto;
                                }
                                else
                                    info.AnswerCode = (int)ErrorIntegracion.EmpresaCelularIncorrecta;
                            }

                            try
                            {
                                string idTr = "";
                                if(datos.ticket == null)
                                { 
                                     idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", datos.amount,
                                         "", terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Carga", "000000000000", "2200", "", "", "", "POS");
                                }
                                else
                                {
                                     idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDTerminal, "POS", "", "", datos.amount,
                                          "", terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Carga", "000000000000", "2200", "", datos.ticket, "", "POS");
                                }

                                //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber); Lo hace el metodo CrearTransaccion

                                info.AnswerCode = 0;
                                info.IdTransaction = int.Parse(idTr);

                                //Vuelvo a chequear los datos
                                //var newAux = dbContext.Tarjetas.Where(x => x.IDTarjeta == aux.IDTarjeta).FirstOrDefault();
                                //info.Points = newAux.PuntosTotales;
                                info.Credits = aux.Giftcard + datos.amount;// hago esto ya que el actualizar puntos no funciona

                                if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                                {
                                    int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;

                                    Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == info.IdTransaction).FirstOrDefault();
                                    tr.UsuarioComercioID = operadorID;
                                    dbContext.SaveChanges();
                                }
                            }
                            catch (Exception e)
                            {
                                info.AnswerCode = -5;
                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le chargegif1t error", e.ToString());
                            }

                            if (enviarSMS)
                            {

                                //TelefonoSMS telefono = new TelefonoSMS();
                                //telefono.Celular = "541128268507";
                                //telefono.EmpresaCelular = "movistar";

                                //PlusMobile.SendSMS("nueva transaccion", telefono);
                            }
                        }
                        else
                            info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                    }
                    else
                        info.AnswerCode = (int)ErrorIntegracion.MontoInsuficiente;
                }
                else
                    info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

        }
        return info;
    }

    [WebMethod]
    public DischargeGiftcardResponse DischargeGiftcard(DischargeGiftcardRequest datos)
    {
        DischargeGiftcardResponse info = new DischargeGiftcardResponse();
        info.SessionID = datos.sessionID;


        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == "G" && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null && (aux.FechaBaja == null || (aux.FechaBaja.HasValue && aux.FechaBaja.Value > DateTime.Now)))
                {
                    if (datos.amount > 0 && datos.amount <= aux.Giftcard)
                    {
                        var Terminal = dbContext.Terminales.Where(x => x.POSTerminal == datos.terminal && x.Activo).FirstOrDefault();
                        if (Terminal != null)
                        {

                            var idTr = "";

                            if(datos.ticket != null) { 
                                 idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, Terminal.IDTerminal, "POS", "", "", datos.amount,
                                "", Terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Descarga", "000000000000", "2200", "", datos.ticket, "", "POS");
                            }
                            else
                            {
                                 idTr = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, Terminal.IDTerminal, "POS", "", "", datos.amount,
                               "", Terminal.NumEst, "", "", datos.cardNumber, datos.terminal, "Descarga", "000000000000", "2200", "", "", "", "POS");
                            }
                            //dbContext.ActualizarPuntosPorTarjeta(datos.cardNumber); Lo hace el metodo CrearTransaccion

                            info.AnswerCode = 0;
                            info.IdTransaction = int.Parse(idTr);

                            //Vuelvo a chequear los datos
                            //var newAux = dbContext.Tarjetas.Where(x => x.IDTarjeta == aux.IDTarjeta).FirstOrDefault();
                            //info.Points = newAux.PuntosTotales;
                            info.Credits = aux.Giftcard - datos.amount;// hago esto ya que el actualizar puntos no funciona

                            if (ValidarSessionIDComercio(datos.sessionID, dbContext))
                            {
                                int operadorID = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault().IDUsuario;

                                Transacciones tr = dbContext.Transacciones.Where(t => t.IDTransaccion == info.IdTransaction).FirstOrDefault();
                                tr.UsuarioComercioID = operadorID;
                                dbContext.SaveChanges();
                            }

                        }
                        else
                            info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                    }
                    else
                        info.AnswerCode = (int)ErrorIntegracion.MontoInsuficiente;
                }
                else
                    info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
        }

        return info;
    }


    #endregion

    #region Consulta

    [WebMethod]
    public CheckPointsResponse CheckPoints(CheckPointsRequest datos)
    {
        CheckPointsResponse info = new CheckPointsResponse();
        info.SessionID = datos.sessionID;

        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.sessionID, dbContext) || ValidarSessionIDComercio(datos.sessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.sessionID, dbContext);
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == datos.type && !x.FechaBaja.HasValue && x.Numero == datos.cardNumber).FirstOrDefault();
                if (aux != null)
                {
                    info.AnswerCode = 0;
                    if (datos.type == "G")
                        info.Credits = aux.Giftcard;
                    else
                        info.Credits = aux.Credito;

                    info.Points = aux.PuntosTotales;
                    if (info.Credits < 0)
                        info.Credits = 0;
                    if (info.Points < 0)
                        info.Points = 0;
                }
                else
                    info.AnswerCode = (int)ErrorIntegracion.NroTarjetaInvalido;
            }
            else
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;
        }
        return info;
    }

    #endregion

    #region Metodos Login

    [WebMethod]
    public LoginResponse Login(LoginRequest datos)
    {

        LoginResponse info = new LoginResponse();
        using (var dbContext = new ACHEEntities())
        {
            var aux = dbContext.Integraciones.Where(x => x.Usuario == datos.userName && x.Pwd == datos.pwd && x.Activo).FirstOrDefault();
            if (aux != null)
            {
                Guid g = Guid.NewGuid();
                IntegracionesTokens entity = new IntegracionesTokens();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.IDUsuario = aux.IDUsuario;
                entity.SessionID = g.ToString();
                dbContext.IntegracionesTokens.Add(entity);
                dbContext.SaveChanges();
                info.AnswerCode = 0;
                info.SessionID = entity.SessionID;
                Operador op = new Operador();
                op.Name = datos.userName;
                op.LastName = datos.userName;
                info.Operador = op;
            }
            else
            {
                info.AnswerCode = -10;
            }
        }
        return info;
    }


    [WebMethod]
    public LoginComercioResponse LoginComercio(LoginComercioRequest datos)
    {

        LoginComercioResponse info = new LoginComercioResponse();

        using (var dbContext = new ACHEEntities())
        {


            var usu = dbContext.UsuariosComercios.Where(
                    p => p.Usuario == datos.userName && p.Pwd == datos.pwd && p.Activo && p.Comercios.Terminales.Any(t => t.POSTerminal == datos.terminal)).FirstOrDefault();


            if (usu != null)
            {
                Guid g = Guid.NewGuid();
                ComerciosTokens entity = new ComerciosTokens();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.IDUsuario = usu.IDUsuario;
                entity.SessionID = g.ToString();
                entity.POSTerminal = datos.terminal;
                dbContext.ComerciosTokens.Add(entity);
                dbContext.SaveChanges();
                info.AnswerCode = 0;
                info.SessionID = entity.SessionID;
                Operador op = new Operador();
                op.Name = datos.userName;
                op.LastName = datos.userName;
                info.Operador = op;
            }
            else
                info.AnswerCode = -10;
        }

        return info;
    }


    [WebMethod]
    public GetTerminalResponse GetTerminalBySessionID(GetTerminalRequest datos)
    {

        GetTerminalResponse info = new GetTerminalResponse();

        using (var dbContext = new ACHEEntities())
        {

            var usu = dbContext.ComerciosTokens.Where(p => p.SessionID == datos.sessionID).FirstOrDefault();

            if (usu != null)
            {

                var ter = dbContext.Terminales.Where(t => t.POSTerminal == usu.POSTerminal && t.Activo).FirstOrDefault();

                if (ter != null)
                {

                    info.AnswerCode = 0;
                    info.Terminal = ter.POSTerminal;

                }
                else
                    info.AnswerCode = -11;
            }
            else
                info.AnswerCode = -1;
        }

        return info;
    }


    [WebMethod]
    public RenewSessionResponse RenewSession(RenewSessionRequest datos)
    {

        RenewSessionResponse info = new RenewSessionResponse();
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.ComerciosTokens.Where(x => x.SessionID == datos.sessionID).FirstOrDefault();
            if (entity != null)
            {
                Guid g = Guid.NewGuid();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.SessionID = g.ToString();
                dbContext.SaveChanges();

                info.AnswerCode = 0;
                info.SessionID = entity.SessionID;
                info.validFor = 10800;

            }
            else
                info.AnswerCode = -1;
        }

        return info;

    }

    private bool ValidarSessionID(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    private bool ValidarSessionIDComercio(string sessionID, ACHEEntities dbContext)
    {
        var aux = dbContext.ComerciosTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }


    private void RenovarFechaValida(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }

    #endregion

}




