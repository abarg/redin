﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaComerciosBase : System.Web.UI.Page
{
    public WebComerciosUser CurrentComerciosUser
    {
        get { return (Session["CurrentComerciosUser"] != null) ? (WebComerciosUser)Session["CurrentComerciosUser"] : null; }
        set { Session["CurrentComerciosUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login-comercios.aspx")
        {
            if (CurrentComerciosUser == null)
            {
                Response.Redirect("~/login-comercios.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}