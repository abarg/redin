﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class WebUser
    {
        public int IDUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }

        public WebUser(int idUsuario, string email, string nombre, string apellido, string usuario)
        {
            this.IDUsuario = idUsuario;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Usuario = usuario;
            this.Email = email;
        }
    }
}