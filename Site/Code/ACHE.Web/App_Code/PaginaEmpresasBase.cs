﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaEmpresasBase : System.Web.UI.Page
{
    public WebEmpresasUser CurrentEmpresasUser
    {
        get { return (Session["CurrentEmpresasUser"] != null) ? (WebEmpresasUser)Session["CurrentEmpresasUser"] : null; }
        set { Session["CurrentEmpresasUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "login-empresas.aspx")
        {
            if (CurrentEmpresasUser == null)
            {
                Response.Redirect("~/login-empresas.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}