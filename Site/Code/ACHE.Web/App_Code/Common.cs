﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using WS = ACHE.Model.WS4;
using System.Diagnostics;

/// <summary>
/// Summary description for Common
/// </summary>
public static class Common
{
    public static List<Combo2ViewModel> LoadComercios()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.Comercios.Include("Domicilios")
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDComercio,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }

    public static List<ComboViewModel> LoadPaises() {
        List<ComboViewModel> entities = new List<ComboViewModel>();

        //using (var dbContext = new ACHEEntities()) {
          //  entities = dbContext.Paises.Include("Provincias").Where(x => x.Provincias.FirstOrDefault() != null)
            //    .Select(x => new ComboViewModel() {
             //       ID = x.IDPais.ToString(),
              //      Nombre = x.Nombre
              //  }).OrderBy(x => x.Nombre).ToList();
      //  }
      //  return entities;

      

        ///TRAE LISTA DE TODOS LOS PAISES

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Paises.Select(x => new ComboViewModel()
                {
                    ID = x.IDPais.ToString(),
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }
        return entities;

    }

    public static WS.Locations GetLocations(WS.GetLocationsRequest datos)
    {
        WS.Locations locations = new WS.Locations();

        using (var dbContext = new ACHEEntities()) {

            if (datos.countryID == null && datos.provinceID == null) // devuelve paises
            {
                locations.countries = dbContext.Paises.Select(x => new WS.Country
                {
                    countryID = x.IDPais,
                    name = x.Nombre
                }).ToList();
            }
            else if(datos.countryID != null) // devuelve provincias
            {
                locations.provincies = dbContext.Provincias.Where(x => x.IDPais == datos.countryID).Select(x => new WS.Province{
                   countryID = x.IDPais,
                   provinceID = x.IDProvincia,
                   name = x.Nombre }).ToList();       
            }         
            else if(datos.provinceID != null) // devuelve ciudades
            {
                locations.cities = dbContext.Ciudades.Where(x => x.IDProvincia == datos.provinceID).Select(x => new WS.City
                {
                    provinceID = x.IDProvincia,
                    cityID = x.IDCiudad,
                    name = x.Nombre
                }).ToList();
            }

        }


        return locations;

    }

    public static List<ComboViewModel> LoadProvinciasByPais(int idPais)
    {
        List<ComboViewModel> entities = new List<ComboViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Provincias.Where(x=>x.IDPais==idPais).ToList()
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDProvincia.ToString(),
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();

            entities.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });
        }

        return entities;
    }

    public static List<ComboViewModel> LoadProvincias()
    {
        List<ComboViewModel> entities = new List<ComboViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Provincias.ToList()
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDProvincia.ToString(),
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();

            entities.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });
        }

        return entities;
    }/*
    public static List<ComboViewModel> LoadProvinciasByPais(int idPais)
    {
        List<ComboViewModel> entities = new List<ComboViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Provincias.Where(x=>x.IDPais==idPais).ToList()
                .Select(x => new ComboViewModel()
                {
                    ID = x.IDProvincia.ToString(),
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();

            entities.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });
        }

        return entities;
    }
    */
    public static List<Combo2ViewModel> LoadCiudades(int idProvincia)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDCiudad,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();


        }
        return entities;
    }

    public static int obtenerPuntosGrupoFamiliar(ACHEEntities dbContext, Socios socio)
    {
        int puntos = 0;
        if (socio.Responsable == true)
            puntos = (dbContext.Tarjetas.Where(x => x.Socios.IDSocioPadre == socio.IDSocio).Sum(x => x.PuntosTotales)) + socio.Tarjetas.Sum(x => x.PuntosTotales);
        else if (socio.IDSocioPadre.HasValue)
            puntos = dbContext.Tarjetas.Where(x => x.Socios.IDSocioPadre == socio.IDSocioPadre || x.Socios.IDSocio == socio.IDSocioPadre).Sum(x => x.PuntosTotales);

        return puntos;
    }

    public static List<Combo2ViewModel> LoadZonas(int idCiudad)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Zonas.Where(x => x.IDCiudad == idCiudad)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDZona,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();


        }
        return entities;
    }

    public static List<Combo2ViewModel> LoadProveedores()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.Proveedores
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDProveedor,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }

    public static List<Combo2ViewModel> LoadCasasMatrices()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.ObtenerComerciosParaFC().Where(x=>x.IDComercio !=null)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDComercio.Value,
                        Nombre = x.NroDocumento + " - " + x.NombreFantasia
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }

    public static string CrearPlanillaVerificacion(ACHEEntities dbContext, HttpContext context, string[] ids, string zona, string rutaMapa)
    {
        //Creo el pdf
        string path = context.Server.MapPath("/files/planillas/");
        Document doc = new Document();
        doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
        string result = "/files/planillas/planillaPOS_" + DateTime.Now.ToString("ddMMyyyyhhss") + ".pdf";
        string filePath = path + "planillaPOS_" + DateTime.Now.ToString("ddMMyyyyhhss") + ".pdf";
        PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
        doc.Open();

        List<Terminales> terminales = new List<Terminales>();
        foreach (string id in ids)
        {
            int aux = int.Parse(id);
              var terminalesAux = dbContext.Terminales.Where(x => x.IDComercio == aux && x.Activo).ToList();
              terminales.AddRange(terminalesAux);
        }

        #region Header

        //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(context.Server.MapPath("~/img/redinlogo.jpg"));
        //gif.Alignment = iTextSharp.text.Image.MIDDLE_ALIGN;
        //gif.ScalePercent(50f);
        //doc.Add(gif);
        string nroReferencia = DateTime.Now.ToString("mmHHddMMyyyy");
        Paragraph p = new Paragraph("Num Ref: " + nroReferencia, new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        p = new Paragraph("Fecha: " + DateTime.Now.ToShortDateString(), new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        p = new Paragraph("Zona: " + zona, new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        p = new Paragraph("Verificador:  ", new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        p = new Paragraph("Responsable Verificador:                               Responsable Backoffice:                               Responsable Supervisor Backoffice:  ", new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        p = new Paragraph(" ");
        doc.Add(p);

        #endregion

        #region Detalle

        string[] col = { "Nº", "Compras", "Canjes", "Gift", "Razón Social", "Nombre de Fantasía", "CUIT", "Domicilio"
                           , "Nº Term", "SDS" , "Nº Estab.", "% DESC.", "% PUNTOS","Cantidad Transacciones", "Calco", "Display", "Calco puerta"
                           , "Verif Compra", "Verif Canjes", "Verif Gift", "Folletos", "Entrega Tarjetas"};
        PdfPTable table = new PdfPTable(22);
        table.HorizontalAlignment = 1;
        float[] widths = new float[] { 30f, 45f, 45f, 45f, 150f, 100f, 100f, 200f, 100f, 90f, 150f, 50f, 50f,50f, 50f, 50f, 50f, 50f, 50f, 50f,50f,50f};
        table.SetWidths(widths);
        table.WidthPercentage = 100;
        table.SpacingBefore = 10;
        for (int i = 0; i < col.Length; ++i)
        {
            PdfPCell cell = new PdfPCell(new Phrase(col[i], new Font(Font.HELVETICA, 7F)));
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);
        }

        string dia = DateTime.Now.DayOfWeek.ToString();

        int j = 1;

        foreach (var ter in terminales)
        {
            Stopwatch sw3 = new Stopwatch();
            sw3.Start();
            string descuento = string.Empty;
            switch (dia.ToLower())
            {
                case "monday":
                    descuento = ter.Descuento.ToString();
                    break;
                case "tuesday":
                    descuento = ter.Descuento2.ToString();
                    break;
                case "wednesday":
                    descuento = ter.Descuento3.ToString();
                    break;
                case "thursday":
                    descuento = ter.Descuento4.ToString();
                    break;
                case "friday":
                    descuento = ter.Descuento5.ToString();
                    break;
                case "saturday":
                    descuento = ter.Descuento6.ToString();
                    break;
                case "sunday":
                    descuento = ter.Descuento7.ToString();
                    break;
            }



            PdfPCell cell = new PdfPCell(new Phrase(j.ToString(), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

         //   var verifCompra = com.VerificacionesPOS.OrderByDescending(x => x.FechaPrueba).FirstOrDefault();
            cell = new PdfPCell(new Phrase(ter.EstadoCompras != null ? ter.EstadoCompras : "", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

           // var verifCanjes = com.VerificacionesPOS.OrderByDescending(x => x.FechaPrueba).FirstOrDefault();
            cell = new PdfPCell(new Phrase(ter.EstadoCanjes != null ? ter.EstadoCanjes : "NO", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

          //  var verifGift = com.VerificacionesPOS.OrderByDescending(x => x.FechaPrueba).FirstOrDefault();
            cell = new PdfPCell(new Phrase(ter.EstadoGift != null ? ter.EstadoGift : "NO", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.Comercios.RazonSocial, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.Comercios.NombreFantasia, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.Comercios.NroDocumento, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.Domicilios.Domicilio, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.POSTerminal, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.Comercios.SDS, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.NumEst, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(descuento, new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(ter.POSPuntos.ToString(), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            DateTime fechaDesde = DateTime.Now.AddDays(-30);
            DateTime fechaHasta = DateTime.Now;

            var cantTransacciones = dbContext.Transacciones.Where(x => x.NumEst == ter.NumEst && x.NumTerminal == ter.POSTerminal && x.FechaTransaccion>=fechaDesde&&x.FechaTransaccion<=fechaHasta).Count();
            cell = new PdfPCell(new Phrase(cantTransacciones.ToString(), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            table.AddCell(cell);

       
            j++;
        }
        doc.Add(table);

        #endregion

        #region Observaciones
        Stopwatch sw4 = new Stopwatch();
        sw4.Start();
        string[] col2 = { "Nº", "Observación" };
        PdfPTable table2 = new PdfPTable(2);
        table.HorizontalAlignment = 1;
        float[] widths2 = new float[] { 50f, 300f };
        table2.SetWidths(widths2);
        table2.WidthPercentage = 100;
        table2.SpacingBefore = 10;
        for (int k = 0; k < col2.Length; ++k)
        {
            PdfPCell cell2 = new PdfPCell(new Phrase(col2[k], new Font(Font.HELVETICA, 7F)));
            cell2.BackgroundColor = new BaseColor(204, 204, 204);
            cell2.HorizontalAlignment = 1;
            table2.AddCell(cell2);
        }


        for (int l = 0; l <= 9; l++)
        {
            PdfPCell cell3 = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell3.HorizontalAlignment = 2;
            cell3.MinimumHeight = 15;
            table2.AddCell(cell3);

            cell3 = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell3.HorizontalAlignment = 2;
            table2.AddCell(cell3);
        }

        doc.Add(table2);

        #endregion

        #region firmas

        p = new Paragraph(" ");
        doc.Add(p);

        p = new Paragraph("Firma Verificador:                                     Firma Backoffice:                                     Firma Supervisor Backoffice:  ", new Font(Font.BOLD, 9F));
        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
        doc.Add(p);

        #endregion

        #region mapa
        if (!string.IsNullOrEmpty(rutaMapa))
        {

            p = new Paragraph(" ");
            p = new Paragraph("Mapa de los comercios:", new Font(Font.BOLD, 9F));
            doc.Add(p);

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(rutaMapa);

            p.Alignment = 0;//0=Left, 1=Centre, 2=Right 
            doc.Add(jpg);
        }

        #endregion

        #region guardar
        foreach (var ter in terminales)
        {
            VerificacionesPOSComercios entity = new VerificacionesPOSComercios();
            entity.Fecha = DateTime.Now;
            entity.IDComercio = ter.IDComercio;
            entity.IDTerminal = ter.IDTerminal;
            entity.IDFranquicia = ter.Comercios.IDFranquicia ?? 0;
            entity.NroReferencia = nroReferencia;
            entity.Estado = "Pendiente";
            dbContext.VerificacionesPOSComercios.Add(entity);
        }

        dbContext.SaveChanges();
        #endregion

        doc.Close();
        return result;
    }

    public static bool ValidarSessionID(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    public static void RenovarFechaValida(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }


}