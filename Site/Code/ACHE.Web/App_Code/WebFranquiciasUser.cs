﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class WebFranquiciasUser
    {
        public int IDUsuario { get; set; }
        public string Email { get; set; }
        public string Franquicia { get; set; }
        public int IDFranquicia { get; set; }
        public string Usuario { get; set; }
        public string Tipo { get; set; }
        public string Theme { get; set; }
        public decimal PubLocal { get; set; }
        public decimal PubNacional { get; set; }
        public string NroDocumento { get; set; }
        public decimal ComisionTtCp { get; set; }
        public decimal ComisionTpCp { get; set; }
        public decimal ComisionTpCt { get; set; }

        public WebFranquiciasUser(int idUsuario, string email, string franquicia, int idFranquicia, string usuario, string tipo, string theme, decimal pubLocal, decimal pubNacional, string nroDocumento, decimal comisionTtCp, decimal comisionTpCp, decimal comisionTpCt)
        {
            this.IDUsuario = idUsuario;
            this.Franquicia = franquicia;
            this.IDFranquicia = idFranquicia;
            this.Email = email;
            this.Usuario = usuario;
            this.Tipo = tipo;
            this.Theme = theme;
            this.PubLocal = pubLocal;
            this.PubNacional = pubNacional;
            this.NroDocumento = nroDocumento;
            this.ComisionTtCp = comisionTtCp;
            this.ComisionTpCp = comisionTpCp;
            this.ComisionTpCt = comisionTpCt;
        }
    }
}