﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_facturas_PiePagina : PaginaBase {
    protected void Page_Load(object sender, EventArgs e) {
        divError.Visible = false;
        //btnGenerar.Enabled = false;
        if (!IsPostBack) {
        }        
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                var result = dbContext.FacturasPiePagina.OrderBy(x=>x.IDFacturaPiePagina)
                    .Select(x => new {
                        ID = x.IDFacturaPiePagina,
                        FacturaDesde = x.FacturaDesde,
                        FacturaHasta = x.FacturaHasta,
                        Imagen = x.ImagenPie
                    });
                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    protected void Generar(Object sender, EventArgs e) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
            try {
                using (var dbContext = new ACHEEntities()) {

                    int factDsd = txtFactDesde.Value != null && txtFactDesde.Value != "" ? int.Parse(txtFactDesde.Value) : 0;
                    int factHst = txtFactHasta.Value != null && txtFactHasta.Value != "" ? int.Parse(txtFactHasta.Value) : 0;

                    if (factDsd > 0 && factHst > 0) {
                        if (factDsd <= factHst) {
                            int id = int.Parse(hdnIDFacturaPiePagina.Value);
                            if (id > 0) {//editar 
                                var entity = dbContext.FacturasPiePagina.Where(x => x.IDFacturaPiePagina == id).FirstOrDefault();
                                if (flpArch.HasFile && extensionValida(flpArch.FileName)) {
                                    entity.ImagenPie = guardarArchivo();
                                }
                                else {
                                    entity.ImagenPie = entity.ImagenPie;
                                }
                                entity.FacturaDesde = factDsd;
                                entity.FacturaHasta = factHst;
                                dbContext.SaveChanges();
                            }
                            else if (flpArch.HasFile && extensionValida(flpArch.FileName)) { //Nuevo
                                divError.Visible = false;
                                FacturasPiePagina FactPiePag = new FacturasPiePagina();
                                FactPiePag.FacturaDesde = factDsd;
                                FactPiePag.FacturaHasta = factHst;
                                FactPiePag.ImagenPie = guardarArchivo();
                                dbContext.FacturasPiePagina.Add(FactPiePag);
                                dbContext.SaveChanges();
                                divOK.Visible = true;
                            }
                        }
                    }

                }
            }
            catch {
                divError.InnerHtml = "Los datos no se actualizaron correctamente";
                divError.Visible = true;
                //throw new Exception("no se pudo guardar el archivo correctamente");
            }
        }
    
    }

    [WebMethod(true)]
    public static bool validarFacturaPiePaginaExistente(string id, int factDsd, int factHst) {
        using (var dbContext = new ACHEEntities()) {
            int idAux = int.Parse(id);
            if (idAux == 0 && dbContext.FacturasPiePagina.All(x => (factDsd < x.FacturaDesde && factHst < x.FacturaHasta) || (factDsd > x.FacturaDesde && factHst > x.FacturaHasta))) //nueva
                return true;
            else if (idAux > 0) { //edita           
                //if (dbContext.FacturasPiePagina.Any(x => (factDsd <= x.FacturaDesde && factHst <= x.FacturaHasta && x.IDFacturaPiePagina != id) || (factDsd >= x.FacturaDesde && factHst >= x.FacturaHasta && x.IDFacturaPiePagina == id)))
                if (dbContext.FacturasPiePagina.Any(x => (factDsd >= x.FacturaDesde && factHst <= x.FacturaHasta) && x.IDFacturaPiePagina != idAux))
                    return false;
                else
                    return true;
            }
            return false;
        }
    }

    [WebMethod(true)]
    public static void Eliminar(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.FacturasPiePagina.Where(x => x.IDFacturaPiePagina == id).FirstOrDefault();
                    if (entity != null) {
                            dbContext.FacturasPiePagina.Remove(entity);
                             dbContext.SaveChanges();
                        }
                    }
                }
            }        
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static FacturasPiePagina cargarFacturasPiePagina(int id) { 
        try{
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.FacturasPiePagina.Where(x => x.IDFacturaPiePagina == id).FirstOrDefault();
                    if (entity != null) {
                        entity.ImagenPie = "../../files/facturas/PiePagina/" + entity.ImagenPie;
                        return entity;
                    }
                    else
                        return null;
                }
            }
            return null;
        }        
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    protected bool extensionValida(string flname) {
        string ext =Path.GetExtension(flname);
        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".pdf")
            return true;
        else
            return false;
    }

    protected string guardarArchivo(){
        string nombreArchivoRuta = ConfigurationManager.AppSettings["FacturasPiePagina"];
        string ext = System.IO.Path.GetExtension(flpArch.FileName);
        string uniqueName = "PiePagina_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
        string path = System.IO.Path.Combine(nombreArchivoRuta, uniqueName);
        path = path.Replace("//", "/");
        flpArch.SaveAs(path);
        return uniqueName;      
    }

}