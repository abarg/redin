﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="seccion-manual-ayudae.aspx.cs" Inherits="modulos_administracion_seccion_manual_ayudae" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/seccion-manual-ayudae.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#" >Manual ayuda</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

      <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición Manual Ayuda</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
             <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabArticulos"  class="hide" data-toggle="tab">Articulos</a></li>
                </ul>
             </div>
          
            <div class="tab-content">
                <div class="tab-pane active" id="tabDatosPrincipales">
                    <br/>
                     <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                           <div class="form-group">
                                <label for="txtTitulo" class="col-lg-2 control-label"><span class="f_req">*</span> Titulo</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTitulo" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Orden</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtOrden" CssClass="form-control required" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Perfil </label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="ddlPerfil" CssClass="form-control required">
                                        <asp:ListItem Text="" Value="" />
                                        <asp:ListItem Text="Franquicias" Value="Franquicias" />
                                        <asp:ListItem Text="Marcas" Value="Marcas" />
                                        <asp:ListItem Text="Comercios" Value="Comercios" />
                                        <asp:ListItem Text="Empresas" Value="Empresas" />
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();" >Aceptar</button>
                                    <a href="seccion-manual-ayuda.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>

                            <asp:HiddenField runat="server" value="0" ID="hfIDSeccion" />
                        </form>
                </div>
                <div class="tab-pane" id="tabArticulos">

                    <br /> 
                         <button  class="btn btn-success" type="button"  onclick="nuevo();">Nuevo</button><br/><br/>

                     <div class="row">
                        <div class="col-sm-12 col-md-12" id="tblArticulos">
                            <table class="table table-striped table-bordered mediaTable" >
				                <thead>
					                <tr>
                                        <th class="essential" style="width:50px">Editar</th>
                                        <th class="essential" style="width:50px">Eliminar</th>
                                        <th class="essential">Fecha</th>
                                        <th class="essential">Usuario</th>
                                        <th class="essential">Titulo</th>
					                </tr>
				                </thead>
				                <tbody id="bodyArticulos">
                                    <tr><td colspan="5">Calculando...</td></tr>
				                </tbody>
			                </table>
                        </div>
                    </div>     
                </div>
            </div>
        </div>
    </div>
  
</asp:Content>

