﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using ClosedXML.Excel;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class modulos_administracion_pagos : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Pagos.Include("Comercios")
                    .Where(x => x.IDComercio.HasValue)
                    .OrderBy(x => x.Fecha)
                    .Select(x => new PagosViewModel()
                    {
                        ID = x.IDPago,
                        IDEntidad = x.IDComercio.Value,
                        Nombre = x.Comercios.NombreFantasia,
                        Fecha = x.Fecha,
                        Importe = x.Importe,
                        FormaDePago = x.FormaDePago,
                        NroComprobante = x.NroFactura,
                        CUIT = x.Comercios.NroDocumento,
                        Observaciones = x.Observaciones,
                        EnContabilium = x.EnContabilium == 1 ? "Si" : "No"
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.Fecha <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Pagos.Where(x => x.IDPago == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Pagos.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void pasarAContabilium()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var pasaje = dbContext.Database.ExecuteSqlCommand("exec IntegracionContabiliumPagos", new object[] { });
            }

        }

    }

    [WebMethod(true)]
    public static string Exportar(string FechaDesde, string FechaHasta, string Comercio, string Cuit) {

        string fileName = "Pagos";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Pagos.Include("Comercios")
                        .Where(x => x.IDComercio.HasValue)
                        .OrderBy(x => x.Fecha).AsEnumerable();
                    
                    if (FechaDesde != "") {
                        DateTime dtDsd = Convert.ToDateTime(FechaDesde);
                        info = info.Where(x => x.Fecha >= dtDsd);
                    }
                    if (FechaHasta != "") {
                        DateTime dtHst = Convert.ToDateTime(FechaHasta);
                        dtHst = dtHst.AddDays(1);
                        info = info.Where(x => x.Fecha <= dtHst);
                    }
                    if (Comercio != "")
                        info = info.Where(x => x.IDComercio == int.Parse(Comercio));

                    if (Cuit != "")
                        info = info.Where(x => x.Comercios.NroDocumento == Cuit);

                    dt = info.Select(x => new {
                            ID = x.IDPago,
                            Comercio = x.Comercios.NombreFantasia,
                            Fecha = x.Fecha,
                            Importe = x.Importe,
                            FormaDePago = x.FormaDePago,
                            NroComprobante = x.NroFactura,
                            EnContabilium = x.EnContabilium == 1 ? "Si" : "No",
                            Observaciones = x.Observaciones

                        }).ToList().ToDataTable();


                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}