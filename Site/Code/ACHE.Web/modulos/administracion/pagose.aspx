﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pagose.aspx.cs" Inherits="modulos_administracion_pagose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/pagose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li><a id="lnkTitulo" href="<%= ResolveUrl("~/modulos/administracion/pagos.aspx") %>">Pagos recibidos</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Edición de pago recibido</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" Value="" />

                <div class="form-group" id="divComercio" runat="server">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Comercio</label>
                    <div class="col-lg-6">
                         <asp:DropDownList runat="server" ID="ddlComercio" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un comercio"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" id="divProveedor" runat="server">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Proveedor</label>
                    <div class="col-lg-6">
                         <asp:DropDownList runat="server" ID="ddlProveedor" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un proveedor"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Fecha de Pago</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Forma de Pago</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="ddlFormaDePago" CssClass="form-control required">
                            <asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>
                            <asp:ListItem Text="Débito Bancario" Value="Debito Bancario"></asp:ListItem>
                            <asp:ListItem Text="Débito Tarjeta" Value="Debito Tarjeta"></asp:ListItem>
                            <asp:ListItem Text="Depósito" Value="Deposito"></asp:ListItem>
                            <asp:ListItem Text="Efectivo" Value="Efectivo"></asp:ListItem>
                            <asp:ListItem Text="Transferencia" Value="Transferencia"></asp:ListItem>
                            <asp:ListItem Text="Otro" Value="Otro"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Nro Comprobante</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNroComprobante" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Importe</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtImporte" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Ret. Ganancias</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtGanancias" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">IIBB</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtIIBB" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">SUSS</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtSUSS" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Redondeo</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtRedondeo" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Otros gastos</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtOtros" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Nro Fc</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNroFactura" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"> Observaciones</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtObservaciones" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a id="lnkCancelar" href="pagos.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</asp:Content>

