﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_facturas_socios : PaginaBase
{
    private static string nombreArchivoRuta;//= ConfigurationManager.AppSettings["FacturasPiePagina"];//= "~/files/facturas/PiePagina/LIQUIDO_PRODUCTO_B_-_01_MAYO_2016_(3)";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Facturas.Include("Socios").Where(x => x.Modo == "S")
                    .OrderByDescending(x => x.PeriodoDesde)
                    .Select(x => new FacturasViewModel()
                    {
                        ID = x.IDFactura,
                        IDEntidad = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                        Numero = x.Numero.Trim(),
                        Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : "",
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        CUIT = x.IDSocio.HasValue ? x.Socios.NroDocumento : "",
                        EnContabilium = x.EnContabilium.HasValue && x.EnContabilium.Value ? "Si" : "No"
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaDesde >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaHasta <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Facturas.Where(x => x.IDFactura == id).FirstOrDefault();
                    if (entity != null)
                    {
                        int aux = dbContext.EliminarFactura(id);
                        //dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string obtenerDetalle(int idFactura)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (idFactura > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.FacturasDetalle.Where(x => x.IDFactura == idFactura).ToList();
                    if (list.Any())
                    {
                        foreach (var detalle in list)
                        {
                            html += "<tr>";
                            html += "<td>" + detalle.Concepto + "</td>";
                            html += "<td>" + detalle.PrecioUnitario.ToString("N2") + "</td>";
                            html += "<td>" + detalle.Iva.ToString("N2") + "</td>";
                            html += "<td>" + (detalle.PrecioUnitario * 1.21M).ToString("N2") + "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
                }
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static void Generar(string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 640;
                dbContext.Database.ExecuteSqlCommand("exec GenerarFacturasSocios '" + DateTime.Parse(fechaDesde).ToString("yyyyMMdd") + "','" + DateTime.Parse(fechaHasta).ToString("yyyyMMdd") + "','" + usu.Usuario + "'", new object[] { });
            }
        }
    }


    [WebMethod(true)]
    public static void Descargar(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var factura = dbContext.Facturas.Include("Socios").Where(x => x.IDFactura == id && x.Modo == "S").FirstOrDefault();
            string path = HttpContext.Current.Server.MapPath("/files/liqProducto/" + factura.Numero + ".pdf");
            int numero;
            if (factura.Numero.Contains("-"))
            {
                string aux = factura.Numero.Split('-')[1];
                numero = int.Parse(aux);
            }
            else
                numero = int.Parse(factura.Numero);
            nombreArchivoRuta = ConfigurationManager.AppSettings["FacturasPiePagina"];

            string fileName = "";
            if (dbContext.FacturasPiePagina.Any(x => numero >= x.FacturaDesde && numero <= x.FacturaHasta))
                fileName = dbContext.FacturasPiePagina.Where(x => numero >= x.FacturaDesde && numero <= x.FacturaHasta).FirstOrDefault().ImagenPie;
            else
                throw new Exception("No existe el pie de pagina cargado");

            nombreArchivoRuta += fileName;

            //throw new Exception(nombreArchivoRuta);

            ACHE.Business.Common.CrearLiqProducto(factura.Numero, factura.FechaProceso.Value, factura.Socios, factura.ImporteTotal, factura.TotalIva, path, nombreArchivoRuta);
        }
    }

    [WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string cuit, string fc)
    {
        string fileName = "FacturasProv";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.Facturas.Include("FacturasDetalle").Include("Socios")
                        .Where(x => x.Modo == "S").AsQueryable();
                    if (cuit != "")
                        info = info.Where(x => x.NroDocumento.ToLower().Contains(cuit.ToLower()));
                    if (fc != "")
                        info = info.Where(x => x.Numero != null && x.Numero.ToLower().Contains(fc.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.PeriodoDesde >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.PeriodoHasta <= dtHasta);
                    }

                    dt = info.Select(x => new
                    {
                        NroDocumento = x.IDSocio.HasValue ? x.Socios.NroDocumento : "",
                        RazonSocial = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : "",
                        Domicilio = x.IDSocio.HasValue && x.Socios.Domicilios != null ? x.Socios.Domicilios.Domicilio + " " + x.Socios.Domicilios.PisoDepto : "",
                        Localidad = x.IDSocio.HasValue && x.Socios.Domicilios != null && x.Socios.Domicilios.Ciudad.HasValue ? x.Socios.Domicilios.Ciudades.Nombre : "",
                        Provincia = x.IDSocio.HasValue && x.Socios.Domicilios != null ? x.Socios.Domicilios.Provincias.Nombre : "",
                        CP = x.IDSocio.HasValue && x.Socios.Domicilios != null ? x.Socios.Domicilios.CodigoPostal : "",
                        CondicionIva = x.Tipo,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        Email = x.IDSocio.HasValue ? x.Socios.Email : "",
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Numero = x.Numero.Trim(),
                        EnContabilium = x.EnContabilium.HasValue && x.EnContabilium.Value ? "Si" : "No"
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}