﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Collections.Specialized;
using System.IO;

public partial class modulos_administracion_facturasenvio : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtFechaDesde.Text = "";
            txtFechaHasta.Text = "";
            cargarEmpresas();
            CargarFacturas(txtFechaDesde.Text, txtFechaHasta.Text);
            

        }
    }

    private void cargarEmpresas()
    {
        using (var dbContext = new ACHEEntities())
        {
            var empresas = dbContext.Empresas.OrderBy(x=>x.Nombre).Select(x => new { x.Nombre, x.IDEmpresa }).ToList(); 
            if (empresas != null && empresas.Count() > 0)
            {
                ddlEmpresas.DataSource = empresas;
                ddlEmpresas.DataValueField = "IDEmpresa";
                ddlEmpresas.DataTextField = "Nombre";
                ddlEmpresas.DataBind();
                ddlEmpresas.Items.Insert(0, new ListItem("", "0"));
                
            }
        }
    }

    private void CargarFacturas(string fechaDesde, string fechaHasta)
    {
        using (var dbContext = new ACHEEntities())
        {
            List<ComboViewModel> facturasCombo = new List<ComboViewModel>();

          
                var facturas = dbContext.Facturas.Include("Comercios")
                .Where(x => x.FechaCAE.HasValue && !x.Enviada && x.IDComercio.HasValue).ToList()
                    .Select(x => new FacturasProveedoresViewModel
                    {
                        IDComercio = (int)(x.IDComercio??0),
                        IDFactura = x.IDFactura,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        Numero = x.Numero,
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                    });
                var aux = facturas.ToList();
                if (fechaDesde != null && fechaDesde != "")
                {
                    DateTime dsd = Convert.ToDateTime(fechaDesde).AddDays(-1);
                    facturas = facturas.Where(x => x.FechaDesde > dsd);
                }
                if (fechaHasta != null && fechaHasta != "")
                {
                    DateTime hst = Convert.ToDateTime(fechaHasta).AddDays(1);
                    facturas = facturas.Where(x => x.FechaHasta < hst);
                }
                var idEmpresa = Convert.ToInt32(ddlEmpresas.SelectedValue);
                if (idEmpresa > 0)
                {
                  var ids = dbContext.EmpresasComercios.Where(x => x.IDEmpresa == idEmpresa).Select(x => x.IDComercio).ToList();

                    facturas = facturas.Where(x => ids.Contains(x.IDComercio));
                }
                foreach (var x in facturas)
                {
                    ComboViewModel factu = new ComboViewModel();
                    factu.ID = x.IDComercio.ToString() + "_" + x.IDFactura.ToString() + "_" + x.Numero;
                    factu.Nombre = x.NombreFantasia + " - " + x.Numero + " - " + x.FechaDesde.ToString("dd/MM/yyyy") + " - " + x.FechaHasta.ToString("dd/MM/yyyy");
                    facturasCombo.Add(factu);
                }

                searchable.DataSource = facturasCombo.OrderBy(x => x.Nombre).ToArray();
                searchable.DataBind();
          
        }
    }

    [WebMethod(true)]
    public static EnvioMailsViewModel enviar(string ids, string items, string asunto, string mensaje)
    {
        using (var dbContext = new ACHEEntities())
        {
            string[] aux = ids.Split(',');
            string[] item;
            MailAddressCollection listaMail;
            int idComercio = 0;
            int idFactura = 0;
            int cantErrores = 0;
            int cantOK = 0;
            string detalleErrores = string.Empty;
            string titulo = string.Empty;

            if (aux.Length > 0)
            {
                #region EnvioMasivo

                foreach (var id in aux)
                {
                    if (id != "")
                    {
                        item = id.Split('_');
                        if (item.Length >= 2)
                        {
                            idComercio = int.Parse(item[0]);
                            idFactura = int.Parse(item[1]);

                            var entity = dbContext.Comercios.Where(x => x.IDComercio == idComercio).FirstOrDefault();
                            if (entity != null)
                            {
                                string mails = entity.EmailsEnvioFc;
                                if (!string.IsNullOrEmpty(mails))
                                {
                                    listaMail = new MailAddressCollection();
                                    foreach (string mail in mails.Trim().Split(";"))
                                    {
                                        if (mail.Trim() != string.Empty)
                                        {
                                            if (mail.Trim().IsValidEmailAddress())
                                            {
                                                //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), mail,"");
                                                listaMail.Add(new MailAddress(mail.Trim()));
                                            }
                                            else
                                                detalleErrores += "La direccion " + mail.Trim() + " del comercio " + entity.NombreFantasia + " no es válida<br/>";
                                        }
                                    }

                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), listaMail.Count.ToString(), "");
                                    ListDictionary replacements = new ListDictionary();
                                    replacements.Add("<CLIENTE>", entity.NombreFantasia);
                                    replacements.Add("<MENSAJE>", mensaje);
                                    if (entity.FormaPago == "M")
                                    {
                                        replacements.Add("<MERCADOPAGO>", "<p>Para pagar su factura ingrese al siguiente link: ");
                                        var linkMP = ConfigurationManager.AppSettings["URLFact"] + "Home/Index/" + Cryptography.Encrypt(idFactura);
                                        replacements.Add("<URLPAGO>", "<a href='" + linkMP + "'>" + linkMP + "</a></p><br>");
                                    }
                                    else
                                    {
                                        replacements.Add("<MERCADOPAGO>", "");
                                        replacements.Add("<URLPAGO>", "");
                                    }

                                    List<string> attachments = new List<string>();
                                    if (item[2] != string.Empty)
                                        attachments.Add(HttpContext.Current.Server.MapPath("~/files/facturas/" + item[2] + ".pdf"));
                                    if (File.Exists(HttpContext.Current.Server.MapPath("~/files/fc-detalles/" + idFactura.ToString("#00000000") + ".pdf")))
                                        attachments.Add(HttpContext.Current.Server.MapPath("~/files/fc-detalles/" + idFactura.ToString("#00000000") + ".pdf"));

                                    bool send = EmailHelper.SendMessage(EmailTemplate.EnvioFactura, replacements, ConfigurationManager.AppSettings["Email.Facturacion"], listaMail, "RedIN :: " + asunto, attachments);
                                    if (!send)
                                    {
                                        cantErrores++;
                                        if (item.Length == 3)
                                            detalleErrores += "No se pudo enviar la Fc Nro " + item[2] + " del comercio " + entity.NombreFantasia + ".<br/>";
                                        else
                                            detalleErrores += "No se pudo enviar la Fc ID " + item[1] + " del comercio " + entity.NombreFantasia + ".<br/>";
                                    }
                                    else
                                    {
                                        cantOK++;
                                        dbContext.Database.ExecuteSqlCommand(@"UPDATE FACTURAS SET Enviada=1, FechaEnvio=GETDATE() WHERE IDFactura=" + item[1]);
                                    }
                                    //else
                                    //    ActualizarFechaTarea(int.Parse(item.Value), idsVencimientos);
                                }

                                else
                                {
                                    cantErrores++;
                                    detalleErrores += "El comercio " + entity.NombreFantasia + " no tiene cargados mails.<br/>";
                                }
                            }
                        }
                    }
                }

                if (cantOK < aux.Length)
                    titulo = "Se han enviado " + cantOK + "/" + aux.Length + " correctamente. Hubo " + cantErrores + " errores. Haga click <a href=\"javascript:verEnvioErrores('" + detalleErrores + "');\">aquí</a> para ver los errores";

                #endregion
            }

            return new EnvioMailsViewModel() { CantErrors = cantErrores, CantEnvios = cantOK, Mensaje = titulo };
        }
    }

    protected void Buscar(Object sender, EventArgs e) {
        CargarFacturas(txtFechaDesde.Text, txtFechaHasta.Text);
    }

}