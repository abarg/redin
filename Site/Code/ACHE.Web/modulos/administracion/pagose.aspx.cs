﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_pagose : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");

            hdnTipo.Value = Request.QueryString["Tipo"];

            CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }

            if (hdnTipo.Value == "P")
                divComercio.Attributes.Add("style", "display:none");
            else
                divProveedor.Attributes.Add("style", "display:none");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string formaDePago, string nroComprobante, string nroFactura, string observaciones, string fecha, int idEntidad,
        string importe, string ganancias, string iibb, string suss, string otros, string redondeo, string tipoMov)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                Pagos entity;
                if (id > 0)
                    entity = dbContext.Pagos.Where(x => x.IDPago == id).FirstOrDefault();
                else
                {
                    entity = new Pagos();
                }

                entity.Importe = decimal.Parse(importe);
                if (ganancias != string.Empty)
                    entity.RetGanancias = decimal.Parse(ganancias);
                else
                    entity.RetGanancias = 0;
                if (iibb != string.Empty)
                    entity.IIBB = decimal.Parse(iibb);
                else
                    entity.IIBB = 0;
                if (suss != string.Empty)
                    entity.SUSS = decimal.Parse(suss);
                else
                    entity.SUSS = 0;
                if (otros != string.Empty)
                    entity.Otros = decimal.Parse(otros);
                else
                    entity.Otros = 0;
                if (redondeo != string.Empty)
                    entity.Redondeo = decimal.Parse(redondeo);
                else
                    entity.Redondeo = 0;
                if (tipoMov == "P")
                {
                    entity.IDProveedor = idEntidad;
                    entity.IDComercio = null;
                }
                else
                {
                    entity.IDComercio = idEntidad;
                    entity.IDProveedor = null;
                }
                entity.FormaDePago = formaDePago;
                entity.NroComprobante = nroComprobante;
                entity.Observaciones = observaciones;
                entity.Fecha = DateTime.Parse(fecha);
                entity.NroFactura = nroFactura;

                if (id > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Pagos.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Pagos.Where(x => x.IDPago == id).FirstOrDefault();
            if (entity != null)
            {
                txtFecha.Text = entity.Fecha.ToShortDateString();
                if (entity.IDComercio.HasValue)
                    ddlComercio.SelectedValue = entity.IDComercio.Value.ToString();
                else
                    ddlProveedor.SelectedValue = entity.IDProveedor.Value.ToString();
                ddlFormaDePago.SelectedValue = entity.FormaDePago;
                txtNroComprobante.Text = entity.NroComprobante;
                txtObservaciones.Text = entity.Observaciones;
                txtImporte.Text = entity.Importe.ToString();
                txtGanancias.Text = entity.RetGanancias.ToString();
                txtIIBB.Text = entity.IIBB.ToString();
                txtSUSS.Text = entity.SUSS.ToString();
                txtOtros.Text = entity.Otros.ToString();
                txtRedondeo.Text = entity.Redondeo.ToString();
                txtNroFactura.Text = entity.NroFactura;
            }
        }
    }

    private void CargarCombos()
    {
        ddlComercio.DataSource = Common.LoadCasasMatrices();
        ddlComercio.DataBind();

        ddlProveedor.DataSource = Common.LoadProveedores();
        ddlProveedor.DataBind();
    }
}