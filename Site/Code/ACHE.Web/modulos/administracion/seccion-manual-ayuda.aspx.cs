﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Model;

public partial class modulos_administracion_seccion_manual_ayuda : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    [System.Web.Services.WebMethod(true)]
    public static void eliminar(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var articulos = dbContext.ArticulosManualAyuda.Where(x=>x.IDManualAyuda==id).ToList();
            if (articulos.Any())
                dbContext.ArticulosManualAyuda.RemoveRange(articulos);
            var seccion = dbContext.ManualAyuda.Where(x=>x.IDManualAyuda==id).FirstOrDefault();
            dbContext.ManualAyuda.Remove(seccion);
            dbContext.SaveChanges();
        }
    }
    [System.Web.Services.WebMethod(true)]
    public static string generarTabla()
    {
        var html = string.Empty;
        using (var dbContext = new ACHEEntities())
        {
            var list = dbContext.ManualAyuda.OrderBy(x => x.Orden).ToList();
            if (list.Any())
            {
                int n = 1;
                foreach (var item in list)
                {

                    html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                    html += "<td style='max-width:100px'><div align='center'><a href='seccion-manual-ayudae.aspx?IDSeccion=" + item.IDManualAyuda + "'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Ver' class='editColumn'/><a/></div></td> ";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='eliminar(" + item.IDManualAyuda + ");'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='gridDelete'/><a/></div></td> ";
                    html += "<td>" + item.Titulo + "</td> ";
                    html += "<td>" + item.Orden.ToString() + "</td> ";
                    html += "<td>" + item.Perfil + "</td> ";
                    html += "</tr>";
                    n++;
                }

            }
            else
            {
                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

}