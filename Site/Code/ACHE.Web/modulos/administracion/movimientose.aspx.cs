﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_movimientose : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");

            hdnTipo.Value = Request.QueryString["Tipo"];

            CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }
            if (hdnTipo.Value == "P")
                divComercio.Attributes.Add("style", "display:none");
            else
                divProveedor.Attributes.Add("style", "display:none");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string concepto, string fecha, int idEntidad, string precio, string tipo, string tipoMov)
    {
        using (var dbContext = new ACHEEntities())
        {
            Movimientos entity;
            if (id > 0)
                entity = dbContext.Movimientos.Where(x => x.IDMovimiento == id).FirstOrDefault();
            else
            {
                entity = new Movimientos();
                entity.FechaAlta = DateTime.Now;
            }

            //if (tipo == "NC")
            //    entity.Precio = decimal.Parse(precio) * -1;
            //else
            entity.Precio = decimal.Parse(precio);
            if (tipoMov == "P")
            {
                entity.IDProveedor = idEntidad;
                entity.IDComercio = null;
            }
            else
            {
                entity.IDComercio = idEntidad;
                entity.IDProveedor = null;
            }
            entity.Concepto = concepto;
            entity.Fecha = DateTime.Parse(fecha);
            entity.TipoMovimiento = "Venta";

            if (id > 0)
                dbContext.SaveChanges();
            else
            {
                dbContext.Movimientos.Add(entity);
                dbContext.SaveChanges();
            }
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Movimientos.Where(x => x.IDMovimiento == id).FirstOrDefault();
            if (entity != null)
            {
                txtFecha.Text = entity.Fecha.ToShortDateString();
                if (entity.IDComercio.HasValue)
                    ddlComercio.SelectedValue = entity.IDComercio.Value.ToString();
                else
                    ddlProveedor.SelectedValue = entity.IDProveedor.Value.ToString();
                txtConcepto.Text = entity.Concepto;
                chkFacturada.Checked = entity.IDFactura.HasValue;
                txtImporte.Text = entity.Precio.ToString();
                if (entity.Precio < 0)
                    rdbNC.Checked = true;
                else
                    rdbND.Checked = true;

                if (entity.IDFactura.HasValue)
                    txtFecha.Enabled = ddlProveedor.Enabled = ddlComercio.Enabled = txtConcepto.Enabled = txtImporte.Enabled = rdbND.Enabled = rdbNC.Enabled = false;
            }
        }
    }

    private void CargarCombos()
    {
        ddlComercio.DataSource = Common.LoadCasasMatrices();
        ddlComercio.DataBind();

        ddlProveedor.DataSource = Common.LoadProveedores();
        ddlProveedor.DataBind();
    }
}