﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="articulo-manual-ayudae.aspx.cs" Inherits="modulos_administracion_articulo_manual_ayudae" %>
  <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/admin/css/chosen.css") %>" type="text/css" media="screen"/>
    <script type="text/javascript" src="<%= ResolveUrl("~/admin/js/chosen.jquery.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="Css/EditorCssFile.css" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <script type="text/javascript">
          $(document).ready(function () {
              $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
           
              var tags = $("#hdnTags").val().split(",");
              $.each(tags, function (idx, val) {
                  if (val != "") {
                      $('#ddlTags option[value="' + val + '"]').attr('selected', 'selected');
                  }
              });

              $('#ddlTags').chosen().change();
              $("#ddlTags").trigger("liszt:updated");

          });
    </script>
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#" >Manual ayuda</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>
    
      <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición Articulo</h3>
     <!--       <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>-->
           

            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                 <asp:Panel runat="server" ID="pnlError" Visible="false" class="notification-wrap failure" style="margin-left:-5px">
				<span class="errorRequired">
				<asp:Literal runat="server" ID="litError"></asp:Literal></span>
                     <br />
			</asp:Panel>
            			<asp:CustomValidator ID="valCustom" runat="server" Display="None" OnServerValidate="ServerValidate"></asp:CustomValidator>
                <div class="form-group">
                    <label for="ddlSeccion" class="col-lg-2 control-label"><span class="f_req">*</span> Seccion</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="ddlSeccion" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlSeccion"
							    ErrorMessage="Este campo es obligatorio." CssClass="errorRequired"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtTitulo" class="col-lg-2 control-label"><span class="f_req">*</span> Titulo</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtTitulo" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtTitulo"
							    ErrorMessage="Este campo es obligatorio." CssClass="errorRequired"></asp:RequiredFieldValidator>
                    </div>
                </div>
                  <div class="form-group">
                    <label for="txtTags" class="col-lg-2 control-label"> Tags</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtTags" CssClass="form-control small" MaxLength="100"></asp:TextBox>
                        <span class="help-block">Separe las palabras por coma (,)<br/><br/>Ejemplo: transaccion,visa,canje,ventas</span>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Articulo</label><br/><br/><br/>
                    <div class="col-lg-12">
                            <telerik:RadScriptManager ID="RadScriptManager1" runat="Server"></telerik:RadScriptManager>
                             <telerik:RadEditor EditModes="All" ID="radText" ClientIDMode="Static" runat="server" Height="250px" Width="99%" 
                                                ToolbarMode="Default" NewLineBr="true"
                                                ToolsFile="~/BasicToolsFile.xml"
                                                Language="es-AR" EnableViewState="false">
                                                <DocumentManager MaxUploadFileSize="5024000"
                                                    DeletePaths="~/files/ayuda"
                                                    UploadPaths="~/files/ayuda"
                                                    ViewPaths="~/files/ayuda" />
                                                <ImageManager MaxUploadFileSize="5024000"
                                                    DeletePaths="~/files/ayuda"
                                                    UploadPaths="~/files/ayuda"
                                                    ViewPaths="~/files/ayuda" />
                                    <Content>
								        <div id="editor">
								        </div>
                                    </Content>
                            </telerik:RadEditor>
                        
                    </div>
                </div>
              <br/><br/>
                 <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                       <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" CssClass="btn btn-success"  OnClick="btnAceptar_OnClick" TabIndex="20" />
                       <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CssClass="btn btn-link" OnClick="btnCancelar_OnClick" CausesValidation="false" TabIndex="21" />

                    </div>
                </div>
                <asp:HiddenField runat="server" value="0" ID="hfIDArticulo" />
                <asp:HiddenField runat="server" value="0" ID="hfIDSeccion" />
              
            </form>

          
        </div>
    </div>

</asp:Content>

