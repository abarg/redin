﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;


public partial class modulos_administracion_reporteDeEnvios : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToShortDateString();
            this.txtFechaHasta.Text = DateTime.Now.ToShortDateString();
        }
    }
    
    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string PeriodoDesde, string PeriodoHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {   
                DateTime dtHasta = DateTime.Parse(PeriodoHasta);
                DateTime dtDesde = DateTime.Parse(PeriodoDesde);
                
                var result = dbContext.ReporteDeEnviosView.Where(x => x.PeriodoDesde >= dtDesde && x.PeriodoHasta <= dtHasta)
                       .OrderBy(x => x.PeriodoDesde)
                       .Select(x => new
                    {
                        PeriodoDesde = x.PeriodoDesde,
                        PeriodoHasta = x.PeriodoHasta,
                        Enviadas = x.Enviadas,
                        NoEnviadas = x.NoEnviadas,
                        Leidas = x.Leidas
                    }).ToList();
                return result.AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList
            }
        }
        else
            return null;
    }



    [WebMethod(true)]
    public static string obtenerDetalle(string tipo, string desde, string hasta) {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {

                    desde = convertirFecha(desde);
                    hasta = convertirFecha(hasta);
                    DateTime dtDesde = DateTime.Parse(desde);
                    DateTime dtHasta = DateTime.Parse(hasta);

                    var list = dbContext.Facturas.Include("Comercios").Where(x => x.PeriodoDesde == dtDesde && x.PeriodoHasta == dtHasta && x.Modo == "R").ToList();

                    if (tipo == "E") {
                        list = list.Where(x => x.PeriodoDesde >= dtDesde && x.PeriodoHasta <= dtHasta && x.Modo == "R" && x.Enviada == true).ToList();
                    }
                    if (tipo == "N") {
                        list = list.Where(x => x.PeriodoDesde >= dtDesde && x.PeriodoHasta <= dtHasta && x.Modo == "R" && x.Enviada == false).ToList();
                    }
                    if (tipo == "L") {
                        list = list.Where(x => x.PeriodoDesde >= dtDesde && x.PeriodoHasta <= dtHasta && x.Modo == "R" && x.FechaRecibida != null).ToList();
                    }

                    if (list.Any()) {
                        foreach (var detalle in list) {
                            html += "<tr>";
                            html += "<td>" + detalle.Numero + "</td>";
                            if (detalle.Comercios != null) 
                                html += "<td>" + detalle.Comercios.NombreFantasia+ "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
                }
        }

        return html;
    }


    public static string convertirFecha(string fecha) { 
        var input = fecha.Substring(4, 11);//formato MMM dd YYYY
        string[] fechaSplit = input.Split(' ');

        string numeroMes=convertirANumeroMes(fechaSplit[0]);

        var format = "MM/dd/YYYY";

        input = fechaSplit[1] + "/" + numeroMes + "/" + fechaSplit[2];
        return input;
        //var date = DateTime.Parse(input);
        //return date.ToString("dd/MM/yyyy");
    }

    public static string convertirANumeroMes(string nombreMes) {

        switch (nombreMes) {
            case "Jan": return "01";
            case "Feb": return "02";
            case "Mar": return "03";
            case "Apr": return "04";
            case "May": return "05";
            case "Jun": return "06";
            case "Jul": return "07";
            case "Aug": return "08";
            case "Sep": return "09";
            case "Oct": return "10";
            case "Nov": return "11";
            case "Dec": return "12";
        }
        return "";

    }



}
