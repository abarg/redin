﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using ACHE.Business;
using System.Configuration;
using System.Collections;


public partial class modulos_administracion_facturase : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnModo.Value = Request.QueryString["Modo"];
            cargarEmpresas();

            CargarCombo();
        }
    }

    private void cargarEmpresas()
    {
        using (var dbContext = new ACHEEntities())
        {
            var empresas = dbContext.Empresas.OrderBy(x => x.Nombre).Select(x => new { x.Nombre, x.IDEmpresa }).ToList();
            if (empresas != null && empresas.Count() > 0)
            {
                ddlEmpresas.DataSource = empresas;
                ddlEmpresas.DataValueField = "IDEmpresa";
                ddlEmpresas.DataTextField = "Nombre";
                ddlEmpresas.DataBind();
                ddlEmpresas.Items.Insert(0, new ListItem("", "0"));

            }
        }
    }

    [WebMethod(true)]
    public static void guardar(string ids, string fechaDesde, string fechaHasta, string concepto, string modo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];

            DateTime dtDesde = DateTime.Parse(fechaDesde + " 00:00:00.000");
            DateTime dtHasta = DateTime.Parse(fechaHasta + " 00:00:00.000");

            int cantError = 0;
            int cantTotal = 0;
            int cantOK = 0;

            using (var dbContext = new ACHEEntities())
            {
                string[] aux = ids.Split(',');
                if (aux.Length > 0)
                {
                    cantTotal = aux.Length;
                    int? generada = 0;

                    var dtHasta2 = dtHasta.AddDays(1);
                    List<FacturasDetViewModel> auxList = new List<FacturasDetViewModel>();

                    if (modo == "R")
                    {
                        //Guardo en memorias las TR del mes

                        auxList = dbContext.TransaccionesFacturacionView
                                    .Where(x => x.FechaTransaccion >= dtDesde && x.FechaTransaccion <= dtHasta2
                                    && x.ImporteOriginal > 1)
                                    .OrderBy(x => x.FechaTransaccion)
                                    .Select(x => new FacturasDetViewModel
                                    {
                                        Fecha = x.Fecha,
                                        Hora = x.Hora,
                                        Operacion = x.Operacion,
                                        Tarjeta = x.Numero,
                                        NroDocumento = x.NroDocumento,
                                        Ticket = x.Ticket.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? (x.Ticket.Value - x.CostoRedIn) : ((x.Ticket.Value - x.CostoRedIn) * -1) : 0,
                                        Arancel = x.Arancel.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.Arancel.Value : (x.Arancel.Value * -1) : 0,
                                        Puntos = x.Puntos.HasValue ? (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.Puntos.Value : (x.Operacion == "Canje" ? 0 : (x.Puntos.Value * -1)) : 0,
                                        CostoRed = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.CostoRedIn : (x.CostoRedIn * -1),
                                    }).ToList();
                    }
                    try
                    {
                        foreach (var id in aux)
                        {
                            if (id != "")
                            {
                                if (modo == "R")
                                {
                                    var idComercio = int.Parse(id);
                                    var comercio = dbContext.Comercios.Include("Terminales").Where(x => x.IDComercio == idComercio).First();

                                    #region Obtengo las TR

                                    var detalle = auxList.Where(x => x.NroDocumento == comercio.NroDocumento).ToList();

                                    foreach (var det in detalle)
                                    {
                                        det.Ticket = MathExt.Round(det.Ticket, 2);
                                        det.Arancel = MathExt.Round(det.Arancel, 2);
                                        det.Puntos = MathExt.Round(det.Puntos, 2);
                                        det.CostoRed = MathExt.Round(det.CostoRed, 2);

                                        if (det.Operacion == "Canje")
                                        {
                                            det.NetoGrabado = MathExt.Round(det.Ticket, 2);
                                            det.Iva = MathExt.Round(det.NetoGrabado * 0.21M, 4);
                                            det.Total = MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 4);

                                            det.NetoGrabado = Math.Abs(det.NetoGrabado);
                                            det.Iva = Math.Abs(det.Iva);
                                            det.Total = Math.Abs(det.Total);
                                            det.Ticket = Math.Abs(det.Ticket);
                                            det.Arancel = Math.Abs(det.Arancel);
                                            det.Puntos = Math.Abs(det.Puntos);
                                            det.CostoRed = Math.Abs(det.CostoRed);
                                        }
                                        else
                                        {
                                            det.NetoGrabado = MathExt.Round(det.Puntos + det.Arancel + det.CostoRed, 2);
                                            det.Iva = MathExt.Round(det.NetoGrabado * 0.21M, 4);
                                            det.Total = MathExt.Round(det.NetoGrabado, 2) + Math.Round(det.Iva, 4);
                                        }
                                    }

                                    #endregion

                                    var tieneCostoPosPropio = false;
                                    if (comercio.Terminales.Any() && comercio.Terminales.Sum(x => x.CostoPOSPropio) > 0)
                                        tieneCostoPosPropio = true;

                                    bool tieneCostoFijo = (comercio.CostoFijo ?? 0) > 0;

                                    if (detalle.Any() || tieneCostoPosPropio || tieneCostoFijo)
                                    {
                                        generada = ACHE.Business.Common.GenerarFcRecurrente(dbContext, detalle, comercio, idComercio, dtDesde, dtHasta, usu.Usuario);
                                        if (generada.HasValue)
                                        {
                                            ACHE.Business.Common.CrearArchivoDetalleFacturacion(dbContext, HttpContext.Current, detalle, comercio, idComercio, generada.Value, dtDesde, dtHasta);
                                        }
                                    }
                                }

                                //else if (modo == "P")
                                //    generada = dbContext.GenerarFacturasProveedores(int.Parse(id), dtDesde, dtHasta, "", usu.Usuario);
                                //else
                                //    generada = dbContext.GenerarFacturasManuales(int.Parse(id), dtDesde, dtHasta, "", usu.Usuario);
                                cantOK++;
                            }
                        }

                        dbContext.Database.ExecuteSqlCommand("delete Facturas where ImporteTotal=0 and CAE is null", new object[] { });
                    }
                    catch (Exception ex)
                    {
                        cantError++;
                        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                    }
                }
            }

            if (cantError > 0)
                throw new Exception("Se han generado " + cantOK + "/" + cantTotal + " correctamente.");
        }
    }

    protected void Buscar(Object sender, EventArgs e)
    {
        CargarCombo();
    }

    private void CargarCombo()
    {
        using (var dbContext = new ACHEEntities())
        {
            if (hdnModo.Value == "P")
                searchable.DataSource = Common.LoadProveedores().ToArray();
            else
            {
                var aux = Common.LoadCasasMatrices().ToArray();
                /*
                
                var comerciosMatrices = Common.LoadCasasMatrices().Select(x=> x.ID).ToArray();
                var aux = dbContext.Comercios.Include("Marcas").Where(x => x.Marca.SeFactura && comerciosMatrices.Contains(x.IDComercio))
                  .Select(x => new Combo2ViewModel() {
                      ID = x.IDComercio,
                      Nombre = x.NroDocumento + " - " + x.NombreFantasia
                  }).Distinct().OrderBy(x => x.Nombre).ToArray();*/

                var idEmpresa = 0;
                if (ddlEmpresas.SelectedValue != "")
                    idEmpresa = Convert.ToInt32(ddlEmpresas.SelectedValue);
                if (idEmpresa > 0)
                {
                    var ids = dbContext.EmpresasComercios.Where(x => x.IDEmpresa == idEmpresa).Select(x => x.IDComercio).ToList();
                    aux = aux.Where(x => ids.Contains(x.ID)).ToArray();
                }
                searchable.DataSource = aux;
            }
            searchable.DataBind();
        }
    }
}