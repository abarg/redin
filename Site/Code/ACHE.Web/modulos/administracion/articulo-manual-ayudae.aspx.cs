﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

using System.Web.UI.HtmlControls;




public partial class modulos_administracion_articulo_manual_ayudae : PaginaBase{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["IDArticulo"]))
            {
                this.limpiarControles();
                this.hfIDArticulo.Value = Request.QueryString["IDArticulo"];

                if (!this.hfIDArticulo.Value.Equals("0"))
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDArticulo"]));
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["IDSeccion"]))
            {
                this.hfIDSeccion.Value = Request.QueryString["IDSeccion"];
                this.ddlSeccion.SelectedValue = Request.QueryString["IDSeccion"];
            }
        }
    }
    private void limpiarControles()
    {
        this.txtTitulo.Text = "";
        this.txtTags.Text = "";
        ddlSeccion.SelectedValue = "";

    }
    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var secciones = dbContext.ManualAyuda.OrderBy(x => x.Titulo).ToList();
            if (secciones != null)
            {
                ddlSeccion.DataSource = secciones;
                ddlSeccion.DataTextField = "Titulo";
                ddlSeccion.DataValueField = "IDManualAyuda";
                ddlSeccion.DataBind();
                ddlSeccion.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarDatos(int IDArticulo)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var articulo = dbContext.ArticulosManualAyuda.Where(x => x.IDArticulosManualAyuda == IDArticulo).FirstOrDefault();
                if (articulo != null)
                {
                    this.txtTitulo.Text = articulo.Titulo;
                    this.radText.Html = articulo.HTML;
                    this.txtTags.Text = articulo.Tags;
                    this.ddlSeccion.SelectedValue = articulo.IDManualAyuda.ToString();
                    this.hfIDSeccion.Value = articulo.IDManualAyuda.ToString();
                }
                else Response.Redirect("~/error.aspx");
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    protected void btnAceptar_OnClick(object sender, EventArgs e)
    {
        if (IsValid == true)
        {
            Response.Redirect("seccion-manual-ayudae.aspx?IDSeccion=" + this.ddlSeccion.Text);
        }
    }
    protected void ServerValidate(object sender, ServerValidateEventArgs args)
    {
        args.IsValid = Process();
    }

    private bool Process()
    {
        bool value = false;

        if (IsValid)
        {
            try
            {
                if (radText.Html != "<div id=\"editor\">\r\n</div>")
                {
                    CreateEntity();
                    value = true;
                }
                else
                    ShowError("El campo Articulo es obligatorio.");
            }
            catch (Exception e)
            {
                value = false;
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                ShowError(e.Message);
            }
        }
        return value;
    }
    private void CreateEntity()
    {
        try
        {
           
            using (var dbContext = new ACHEEntities())
            {
                ArticulosManualAyuda entity;
                int idArticulo=int.Parse(hfIDArticulo.Value);
                if (idArticulo> 0)
                    entity = dbContext.ArticulosManualAyuda.FirstOrDefault(x => x.IDArticulosManualAyuda == idArticulo);
                else
                {
                    entity = new ArticulosManualAyuda();
                    entity.Fecha = DateTime.Now;

                    if (HttpContext.Current.Session["CurrentUser"] != null)
                    {
                        var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                        entity.IDUsuario = usu.IDUsuario;
                    }
                }

                entity.Titulo = txtTitulo.Text;
                entity.HTML = radText.Html;

                entity.Tags = txtTags.Text;
                entity.IDManualAyuda = int.Parse(ddlSeccion.SelectedValue);
                if (idArticulo > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ArticulosManualAyuda.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    protected void btnCancelar_OnClick(object sender, EventArgs e)
    {
            Response.Redirect("seccion-manual-ayudae.aspx?IDSeccion=" + this.hfIDSeccion.Value);

    }

    private void ShowError(string msg)
    {
        litError.Text = msg;
        pnlError.Visible = true;
    }

    /*
    [WebMethod(true)]
    public static int grabar(int idArticulo, string idseccion, string titulo, string articulo,string tags)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                ArticulosManualAyuda entity;
                if (idArticulo > 0)
                    entity = dbContext.ArticulosManualAyuda.FirstOrDefault(x => x.IDArticulosManualAyuda == idArticulo);
                else
                {
                    entity = new ArticulosManualAyuda();
                    entity.Fecha = DateTime.Now;

                    if (HttpContext.Current.Session["CurrentUser"] != null)
                    {
                        var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                        entity.IDUsuario = usu.IDUsuario;
                    }
                }

                entity.Titulo = titulo;
                entity.HTML = articulo;
                entity.Tags = tags;
                entity.IDManualAyuda =int.Parse(idseccion);
                if (idArticulo > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ArticulosManualAyuda.Add(entity);
                    dbContext.SaveChanges();
                }
            }
            return int.Parse(idseccion);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }*/
}