﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_proveedorese : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaAlta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            cargarProvincias();

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                this.hdnID.Value = Request.QueryString["ID"];
                if (!this.hdnID.Value.Equals("0"))
                {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["ID"]));
                }
            }
        }
    }

    private void cargarProvincias()
    {
        ddlProvincia.DataSource = Common.LoadProvincias();
        ddlProvincia.DataTextField = "Nombre";
        ddlProvincia.DataValueField = "ID";
        ddlProvincia.DataBind();

    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                Proveedores entity = dbContext.Proveedores.FirstOrDefault(s => s.IDProveedor == id);
                if (entity != null)
                {
                    this.txtFechaAlta.Text = entity.FechaAlta.ToString("dd/MM/yyyy");
                    this.txtNombreFantasia.Text = entity.NombreFantasia;
                    this.txtRazonSocial.Text = entity.RazonSocial;
                    this.ddlTipoDoc.SelectedValue = entity.TipoDocumento;
                    this.txtNroDocumento.Text = entity.NroDocumento;
                    this.ddlIVA.SelectedValue = entity.CondicionIva;
                    this.txtEnvioFc.Text = entity.EmailsEnvioFc;
                    this.txtTelefono.Text = entity.Telefono;
                    this.txtCelular.Text = entity.Celular;
                    this.txtWeb.Text = entity.Web;
                    this.txtEmail.Text = entity.Email;
                    this.txtObservaciones.Text = entity.Observaciones;
                    //Domicilio Fiscal
                    if (entity.IDDomicilioFiscal != 0)
                    {
                        Domicilios oDomicilioF = entity.Domicilios;

                        this.ddlPais.SelectedValue = "1";
                        this.ddlProvincia.SelectedValue = oDomicilioF.Provincias.Nombre;
                        ddlCiudad.DataSource = Common.LoadCiudades(oDomicilioF.Provincia);
                        ddlCiudad.DataTextField = "Nombre";
                        ddlCiudad.DataValueField = "ID";
                        ddlCiudad.DataBind();
                        ddlCiudad.Items.Insert(0, new ListItem("", ""));

                        if (oDomicilioF.Ciudad.HasValue)
                            ddlCiudad.SelectedValue = oDomicilioF.Ciudad.Value.ToString();
                        this.txtDomicilio.Text = oDomicilioF.Domicilio;
                        this.txtCodigoPostal.Text = oDomicilioF.CodigoPostal;
                        this.txtTelefonoDom.Text = oDomicilioF.Telefono;
                        this.txtFax.Text = oDomicilioF.Fax;
                        this.txtPisoDepto.Text = oDomicilioF.PisoDepto;
                        this.txtLatitud.Text = oDomicilioF.Latitud;
                        this.txtLongitud.Text = oDomicilioF.Longitud;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static void grabar(int ID, string RazonSocial, string TipoDoc, string NroDocumento, string Telefono, string Celular
        , string NombreFantasia, string IVA, string Web, string Email
        , string Observaciones
        , string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax, string PisoDepto, string Lat, string Long
        , string EnvioMailsFc)
    {
        try
        {
            Proveedores entity;
            using (var dbContext = new ACHEEntities())
            {
                if (ID > 0)
                    entity = dbContext.Proveedores.FirstOrDefault(s => s.IDProveedor == ID);
                else
                {
                    entity = new Proveedores();
                    entity.FechaAlta = DateTime.Now;
                }

                entity.NombreFantasia = NombreFantasia;
                entity.RazonSocial = RazonSocial;
                entity.TipoDocumento = TipoDoc;
                entity.NroDocumento = NroDocumento;
                entity.CondicionIva = IVA;
                entity.EmailsEnvioFc = EnvioMailsFc;
                entity.Telefono = Telefono;
                entity.Celular = Celular;
                entity.Web = Web;
                entity.Email = Email;
                entity.Observaciones = Observaciones;
                //Domicilio Fiscal
                if (ID == 0)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    entity.Domicilios.Estado = "A";
                }
                entity.Domicilios.TipoDomicilio = "F";
                entity.Domicilios.Entidad = "P";
                entity.Domicilios.Pais = Pais;
                entity.Domicilios.Provincia = int.Parse(Provincia);
                if (Ciudad != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio;
                entity.Domicilios.CodigoPostal = CodigoPostal;
                entity.Domicilios.Telefono = TelefonoDom;
                entity.Domicilios.Fax = Fax;
                entity.Domicilios.PisoDepto = PisoDepto;
                entity.Domicilios.Latitud = Lat;
                entity.Domicilios.Longitud = Long;


                if (ID > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Proveedores.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}