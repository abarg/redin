﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="facturasenvio-prov.aspx.cs" Inherits="modulos_administracion_facturasenvio_prov" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/facturasenvio-prov.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <!-- multiselect -->
	<script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/administracion/facturas-prov.aspx") %>">Facturas</a></li>
            <li>Envío masivo</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Envío masivo</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Las facturas se han envíado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                
                <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <asp:TextBox runat="server" id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"></asp:TextBox>
                            <%--<input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>--%>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <asp:TextBox runat="server" id="txtFechaHasta" value="" maxlength="10" class="form-control validDate greaterThan"></asp:TextBox>
                            <%--<input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>--%>
						    <span class="help-block"></span>
					    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-md-8">
                         <asp:Button runat="server" ID="buscar" class="btn" type="button" Text="Buscar" OnClick="Buscar"></asp:Button>
<%--                        <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>--%>
                    </div>
                </div>                        

                <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Facturas pendientes</label>
                    <div class="col-lg-9">
                        <asp:DropDownList runat="server" ID="searchable" multiple="multiple" 
                            DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>

                        <br />
                        <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                    </div>
                </div>
                <%--<div class="form-group">
                    <label class="col-lg-3 control-label">F931</label>
                    <div class="col-lg-4">
                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpF931" CssClass="form-control" PersistFile="true"
                            ThrobberID="throbberF931" OnClientUploadComplete="UploadCompleted" Width="200px"
                            ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                            OnUploadedComplete="uploadF931" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                        <asp:Label runat="server" ID="throbberF931" Style="display: none;">
                            <img alt="" src="../../img/ajax_loader.gif" />
                        </asp:Label>
                        <span class="help-block">Extensions: jpg/png/pdf. Max size: 3mb</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Certificado ART</label>
                    <div class="col-lg-4">
                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpART" CssClass="form-control" PersistFile="true"
                            ThrobberID="throbberART" OnClientUploadComplete="UploadCompleted" Width="200px"
                            ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                            OnUploadedComplete="uploadArt" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                        <asp:Label runat="server" ID="throbberART" Style="display: none;">
                            <img alt="" src="../../img/ajax_loader.gif" />
                        </asp:Label>
                        <span class="help-block">Extensions: jpg/png/pdf. Max size: 3mb</span>
                    </div>
                </div>--%>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Asunto Mail</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtAsunto" CssClass="form-control required" MaxLength="200" Text="Facturación"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mensaje Mail</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" CssClass="form-control required" TextMode="MultiLine"
                        Text="Le enviamos la factura correspondiente por los servicios prestados."></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnEnviar" class="btn btn-success" type="button" onclick="enviar();" >Enviar</button>
                       <a href="facturas.aspx" class="btn btn-link" id="lnkVolver">Volver</a>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

