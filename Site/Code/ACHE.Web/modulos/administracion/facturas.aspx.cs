﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_facturas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta, string CBU)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Facturas.Include("Comercios")
                    .Where(x => x.Modo == "R" && x.IDComercio.HasValue).OrderByDescending(x => x.PeriodoDesde)
                    .Select(x => new FacturasViewModel()
                    {
                        ID = x.IDFactura,
                        IDEntidad = x.IDComercio.Value,
                        Numero = x.Numero.Trim(),
                        Nombre = x.Comercios.NombreFantasia,
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        CAE = x.CAE,
                        CBU = x.Comercios.FormaPago_CBU,
                        CUIT = x.Comercios.NroDocumento,
                        Enviada = x.FechaEnvio.HasValue ? "Si" : "No",
                        Descargada = x.FechaRecibida.HasValue ? "Si" : "No",
                        ArchivoDetalle = x.ArchivoDetalle,
                        EnContabilium = x.EnContabilium.HasValue && x.EnContabilium.Value ? "Si" : "No",
                        ImporteDebitar = x.ImporteADebitar,
                        Tipo = x.Comercios.IDMarca.HasValue ? x.Comercios.Marcas.TipoTarjeta : "",
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaDesde >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaHasta <= dtHasta);
                }
                if (CBU != string.Empty)
                {
                    result = result.Where(x => x.CBU.Contains(CBU));
                }

                return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static List<FacturasSinCaeViewModel> obtenerFacturasSinCAE()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Facturas.Include("Comercios").Include("FacturasDetalle").Where(x => x.CAE == null && x.Modo == "R" && x.IDComercio.HasValue).Select(x => new FacturasSinCaeViewModel
                    {
                        cliente = new ClientesSinCaeViewModel
                        {
                            RazonSocial = x.Comercios.RazonSocial,
                            TipoDocumento = x.Comercios.TipoDocumento,
                            NroDocumento = x.Comercios.NroDocumento,
                            Domicilio = x.Comercios.Domicilios.Domicilio,
                            CoindicionIva = x.Comercios.CondicionIva,
                            Localidad = x.Comercios.Domicilios.Ciudades.Nombre,
                            Provincia = x.Comercios.Domicilios.Provincias.Nombre
                        },
                        factDet = x.FacturasDetalle.Select(y => new FacturasDetalleSinCAEViewModel
                        {
                            IDFacturaDetalle = y.IDFacturaDetalle,
                            Cantidad = y.Cantidad,
                            Concepto = y.Concepto,
                            PrecioUnitario = y.PrecioUnitario,
                            Iva = y.Iva
                        }).ToList(),
                        Tipo = x.Tipo == "RI" ? "FCA " : "FCB"
                    }).ToList();
                    if (entity.Any())
                        return entity;
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return null;
    }


    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Facturas.Where(x => x.IDFactura == id).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.FechaCAE.HasValue)
                            throw new Exception("No se puede eliminar una factura con CAE");
                        else
                        {
                            var file = HttpContext.Current.Server.MapPath("/files/fc-detalles/" + entity.ArchivoDetalle);
                            int aux = dbContext.EliminarFactura(id);
                            if (File.Exists(file))
                                File.Delete(file);

                            //dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string obtenerDetalle(int idFactura)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (idFactura > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.FacturasDetalle.Where(x => x.IDFactura == idFactura).ToList();
                    if (list.Any())
                    {
                        foreach (var detalle in list)
                        {
                            html += "<tr>";
                            html += "<td>" + detalle.Concepto + "</td>";
                            html += "<td>" + detalle.PrecioUnitario.ToString("N2") + "</td>";
                            html += "<td>" + detalle.Iva.ToString("N2") + "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
                }
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Facturas.Include("Comercios").Where(x => x.FechaError.HasValue && !x.FechaCAE.HasValue && x.Modo == "R" && x.IDComercio.HasValue).ToList();
                if (list.Any())
                {
                    foreach (var fc in list)
                    {
                        html += "<tr>";
                        html += "<td>" + fc.Comercios.NombreFantasia + "</td>";
                        html += "<td>" + fc.PeriodoDesde.ToShortDateString() + "-" + fc.PeriodoHasta.ToShortDateString() + "</td>";
                        if (fc.FechaError.HasValue)
                            html += "<td>" + fc.FechaError.Value.ToShortDateString() + "</td>";
                        else
                            html += "<td></td>";
                        html += "<td>" + fc.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    [WebMethod(true)]
    public static string ObtenerCAE(int importe, string fechaDesde, string fechaHasta, string tipo)
    {
        var desc = "";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var facturas = dbContext.Facturas.Include("Comercios").Include("comercios.Marcass").Include("FacturasDetalle")
                    .Where(x => !x.FechaCAE.HasValue && x.Modo == "R" && x.ImporteTotal > importe && x.IDComercio.HasValue)
                    .OrderBy(x => x.PeriodoDesde)
                    .ToList();

                if (tipo != string.Empty)
                {
                    facturas = facturas.Where(x => x.Comercios.IDMarca.HasValue && x.Comercios.Marcas.TipoTarjeta == tipo).ToList();
                }
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    facturas = facturas.Where(x => x.PeriodoDesde >= dtDesde).ToList();
                }

                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    facturas = facturas.Where(x => x.PeriodoHasta <= dtHasta).ToList();
                }

                int cantError = 0;
                int cantTotal = facturas.Count();
                int cantOK = 0;

                if (cantTotal > 0)
                {
                    //Pre validacion
                    string cuitCliente = string.Empty;
                    foreach (var fc in facturas)
                    {
                        var condicionIva = fc.Comercios.CondicionIva;
                        cuitCliente = fc.Comercios.NroDocumento.ReplaceAll("-", "").Trim();
                        if (cuitCliente == string.Empty)
                        {
                            throw new Exception("El cliente " + fc.Comercios.NombreFantasia + " (" + condicionIva + ") no tiene un CUIT cargado");
                        }
                    }

                    FEFacturaElectronica fe = new FEFacturaElectronica();
                    FEComprobante comprobante = null;
                    var pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CertificadoAFIP"]);
                    //var pathCertificado = ConfigurationManager.AppSettings["CertificadoAFIP"];
                    var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Template"]);
                    int puntoVenta = int.Parse(ConfigurationManager.AppSettings["FE.PtoVta"]);
                    long cuit = long.Parse(ConfigurationManager.AppSettings["FE.CUIT"]);
                    double total = 0;
                    double totalSinIva = 0;
                    //double totalNeto = 0;
                    //double iva = 0;

                    foreach (var fc in facturas)
                    {
                        #region Seteo de datos

                        //EL DETALLE TIENE CONCEPTS EN IVA 21 Y EN IVA 0
                        //LO QUE DICE IVA 0, TIENE QUE IR A NO GRAVADO.

                        //EL QUE EMITE ES SIEMPRE RI.


                        //total = Math.Round(double.Parse(fc.ImporteTotal.ToString()), 2);
                        //iva = Math.Round(total * 0.21, 2);
                        total = double.Parse(fc.ImporteTotal.ToString());
                        totalSinIva = Math.Round(total, 2);
                        total += (total * 0.21);
                        total = Math.Round(total, 2);
                        cuitCliente = fc.Comercios.NroDocumento;

                        comprobante = new FEComprobante();
                        comprobante.DetalleIva = new List<FERegistroIVA>();
                        comprobante.Tributos = new List<FERegistroTributo>();
                        comprobante.Concepto = FEConcepto.Servicio;
                        comprobante.Fecha = DateTime.Now;
                        comprobante.Cuit = cuit;
                        comprobante.PtoVta = puntoVenta;
                        comprobante.FchServDesde = fc.PeriodoDesde;// DateTime.Now.AddDays(-1);
                        comprobante.FchServHasta = fc.PeriodoHasta;//DateTime.Now;
                        comprobante.FchVtoPago = DateTime.Now.AddDays(5); //fc.PeriodoHasta.AddDays(120);//DateTime.Now.AddDays(5);
                        comprobante.CodigoMoneda = "PES";
                        comprobante.CotizacionMoneda = 1;
                        comprobante.CondicionVenta = "Contado";
                        if (fc.Tipo == "RI")
                            comprobante.ClienteCondiionIva = "Responsable Inscripto";
                        else
                            comprobante.ClienteCondiionIva = fc.Comercios.CondicionIva;

                        #region Seteo los datos en base a la condicion iva

                        if (fc.Tipo == "RI" || fc.Tipo == "EX" || fc.Tipo == "MONOTRIBUTISTA")
                        {
                            if (fc.Comercios.TipoDocumento == "CUIT")//TODO: MEJORAR ESTO.
                                comprobante.DocTipo = 80;// 96= DNI//80=CUIT
                            else
                                comprobante.DocTipo = 96;// 96= DNI//80=CUIT

                            comprobante.DocNro = long.Parse(cuitCliente);
                            comprobante.ClienteNombre = fc.Comercios.RazonSocial;
                            comprobante.ClienteDomicilio = fc.Comercios.DomiciliosFiscal.Domicilio + " " + fc.Comercios.DomiciliosFiscal.PisoDepto;
                            comprobante.ClienteLocalidad = fc.Comercios.DomiciliosFiscal.Ciudad.HasValue ? fc.Comercios.DomiciliosFiscal.Ciudades.Nombre : "";
                        }
                        /*else
                        {
                            comprobante.DocTipo = 99;//Sin identificar/venta global diaria
                            comprobante.DocNro = 0;
                            comprobante.ClienteNombre = "Cliente final";
                            comprobante.ClienteDomicilio = "No informado";
                            comprobante.ClienteLocalidad = "No informado";
                        }*/

                        if (fc.Tipo == "RI")
                        {
                            //totalNeto = totalSinIva;
                            comprobante.TipoComprobante = FETipoComprobante.FACTURAS_A;
                            //comprobante.ImpTotal = total;// Math.Round(total + iva, 2);
                            //comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(totalSinIva, 2), TipoIva = FETipoIva.Iva21 });
                        }
                        else
                        {
                            //total += (total * 0.21);
                            //totalNeto = total;
                            comprobante.TipoComprobante = FETipoComprobante.FACTURAS_B;
                            //comprobante.ImpTotal = total;
                            //comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(total, 2), TipoIva = FETipoIva.Iva0 });
                        }

                        #endregion

                        comprobante.ImpTotConc = 0;
                        //comprobante.ImpNeto = totalNeto;
                        comprobante.ImpOpEx = 0;
                        comprobante.Observaciones = "Periodo facturado " + fc.PeriodoDesde.ToString("dd/MM/yyyy") + " al " + fc.PeriodoHasta.ToString("dd/MM/yyyy");

                        comprobante.DetalleIva = new List<FERegistroIVA>();
                        comprobante.Tributos = new List<FERegistroTributo>();

                        double importeDetalle = 0;
                        foreach (var detalle in fc.FacturasDetalle)
                        {
                            importeDetalle = double.Parse(detalle.PrecioUnitario.ToString());
                            if (fc.Tipo != "RI")
                            {// && condicionIva != "EX")
                                if (detalle.Iva > 0)
                                    importeDetalle += (importeDetalle * 0.21);
                            }
                            //importeDetalle += (importeDetalle * 0.21);
                            importeDetalle = Math.Round(importeDetalle, 2);

                            //double importeDetalle = double.Parse(detalle.PrecioUnitario.ToString());
                            //double importeIva = importeDetalle * 0.21;
                            //double totalDetalle = importeDetalle + importeIva;

                            //if (fc.Tipo != "RI")
                            //importeDetalle += (importeDetalle * 0.21);
                            comprobante.ItemsDetalle.Add(new FEItemDetalle()
                            {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                Precio = importeDetalle,
                                Codigo = detalle.IDFacturaDetalle.ToString(),
                            });

                            if (fc.Tipo == "RI")
                            {

                                switch (detalle.Iva.ToString())
                                {
                                    case "21,00":
                                        AgregarDetalleIvaTotalizado(ref comprobante, detalle.PrecioUnitario, FETipoIva.Iva21);
                                        break;
                                    case "0,00":
                                        AgregarDetalleIvaTotalizado(ref comprobante, detalle.PrecioUnitario, FETipoIva.Iva0);
                                        break;
                                }
                            }
                            else
                            {
                                /*comprobante.ItemsDetalle.Add(new FEItemDetalle()
                                {
                                    Cantidad = detalle.Cantidad,
                                    Descripcion = detalle.Concepto,
                                    Precio = Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), 2),
                                    Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                    Bonificacion = detalle.Bonificacion
                                });*/

                                if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0))
                                {
                                    comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp += Math.Round(importeDetalle, 2);
                                }
                                else
                                {
                                    //if (tipo != "FCC" && tipo != "NDC" && tipo != "NCC" && tipo != "RCC")
                                    comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(importeDetalle, 2), TipoIva = FETipoIva.Iva0 });
                                }
                            }
                        }

                        #endregion

                        fc.FechaProceso = DateTime.Now;
                        try
                        {
                            fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["wsaa"], pathCertificado, pathTemplateFc);
                            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                            var pathPdf = HttpContext.Current.Server.MapPath("/files/facturas/") + numeroComprobante + ".pdf";
                            fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc);

                            fc.FechaCAE = comprobante.FechaVencCAE;
                            fc.CAE = comprobante.CAE;
                            fc.UsuarioProceso = usu.Usuario;
                            fc.Numero = numeroComprobante;
                            fc.Error = null;
                            fc.FechaError = null;

                            dbContext.SaveChanges();
                            cantOK++;
                        }
                        catch (Exception ex)
                        {
                            fc.Error = ex.Message;
                            fc.FechaError = DateTime.Now;
                            dbContext.SaveChanges();
                            cantError++;
                        }
                    }

                    if (cantError == 0)
                        desc = "Todas las facturas se han generado correctamente";
                    else
                        desc = "Se han generado " + cantOK + "/" + cantTotal + " correctamente. Haga click <a href='javascript:verFcErrores();'>aquí</a> para ver los errores";
                }
                else
                    throw new Exception("No hay facturas a generar");
            }
        }
        else
            desc = "Error. Vuelva a iniciar sesión";

        return desc;
    }

    [WebMethod(true)]
    public static void procesarFC(int idFactura)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var fc = dbContext.Facturas.Include("Comercios").Include("FacturasDetalle")
                    .Where(x => !x.FechaCAE.HasValue && x.Modo == "R" && x.IDFactura == idFactura)
                    .FirstOrDefault();


                if (fc != null)
                {
                    var condicionIva = fc.Comercios.CondicionIva;
                    string cuitCliente = fc.Comercios.NroDocumento.ReplaceAll("-", "").Trim();

                    if (cuitCliente == string.Empty)
                    {
                        throw new Exception("El cliente " + fc.Comercios.NombreFantasia + " (" + condicionIva + ") no tiene un CUIT cargado");
                    }

                    FEFacturaElectronica fe = new FEFacturaElectronica();
                    FEComprobante comprobante = null;
                    var pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CertificadoAFIP"]);
                    //var pathCertificado = ConfigurationManager.AppSettings["CertificadoAFIP"];
                    var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Template"]);
                    int puntoVenta = int.Parse(ConfigurationManager.AppSettings["FE.PtoVta"]);
                    long cuit = long.Parse(ConfigurationManager.AppSettings["FE.CUIT"]);
                    double total = 0;
                    double totalSinIva = 0;
                    //double totalNeto = 0;
                    //double iva = 0;


                    #region Seteo de datos

                    //EL DETALLE TIENE CONCEPTS EN IVA 21 Y EN IVA 0
                    //LO QUE DICE IVA 0, TIENE QUE IR A NO GRAVADO.

                    //EL QUE EMITE ES SIEMPRE RI.


                    //total = Math.Round(double.Parse(fc.ImporteTotal.ToString()), 2);
                    //iva = Math.Round(total * 0.21, 2);
                    total = double.Parse(fc.ImporteTotal.ToString());
                    totalSinIva = Math.Round(total, 2);
                    total += (total * 0.21);
                    total = Math.Round(total, 2);
                    cuitCliente = fc.Comercios.NroDocumento;

                    comprobante = new FEComprobante();
                    comprobante.DetalleIva = new List<FERegistroIVA>();
                    comprobante.Tributos = new List<FERegistroTributo>();
                    comprobante.Concepto = FEConcepto.Servicio;
                    comprobante.Fecha = DateTime.Now;
                    comprobante.Cuit = cuit;
                    comprobante.PtoVta = puntoVenta;
                    comprobante.FchServDesde = fc.PeriodoDesde;// DateTime.Now.AddDays(-1);
                    comprobante.FchServHasta = fc.PeriodoHasta;//DateTime.Now;
                    comprobante.FchVtoPago = DateTime.Now.AddDays(5); //fc.PeriodoHasta.AddDays(120);//DateTime.Now.AddDays(5);
                    comprobante.CodigoMoneda = "PES";
                    comprobante.CotizacionMoneda = 1;
                    comprobante.CondicionVenta = "Contado";
                    if (fc.Tipo == "RI")
                        comprobante.ClienteCondiionIva = "Responsable Inscripto";
                    else
                        comprobante.ClienteCondiionIva = fc.Comercios.CondicionIva;

                    #region Seteo los datos en base a la condicion iva

                    if (fc.Tipo == "RI" || fc.Tipo == "EX" || fc.Tipo == "MONOTRIBUTISTA")
                    {
                        if (fc.Comercios.TipoDocumento == "CUIT")//TODO: MEJORAR ESTO.
                            comprobante.DocTipo = 80;// 96= DNI//80=CUIT
                        else
                            comprobante.DocTipo = 96;// 96= DNI//80=CUIT

                        comprobante.DocNro = long.Parse(cuitCliente);
                        comprobante.ClienteNombre = fc.Comercios.RazonSocial;
                        comprobante.ClienteDomicilio = fc.Comercios.DomiciliosFiscal.Domicilio + " " + fc.Comercios.DomiciliosFiscal.PisoDepto;
                        comprobante.ClienteLocalidad = fc.Comercios.DomiciliosFiscal.Ciudad.HasValue ? fc.Comercios.DomiciliosFiscal.Ciudades.Nombre : "";
                    }
                    /*else
                    {
                        comprobante.DocTipo = 99;//Sin identificar/venta global diaria
                        comprobante.DocNro = 0;
                        comprobante.ClienteNombre = "Cliente final";
                        comprobante.ClienteDomicilio = "No informado";
                        comprobante.ClienteLocalidad = "No informado";
                    }*/

                    if (fc.Tipo == "RI")
                    {
                        //totalNeto = totalSinIva;
                        comprobante.TipoComprobante = FETipoComprobante.FACTURAS_A;
                        //comprobante.ImpTotal = total;// Math.Round(total + iva, 2);
                        //comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(totalSinIva, 2), TipoIva = FETipoIva.Iva21 });
                    }
                    else
                    {
                        //total += (total * 0.21);
                        //totalNeto = total;
                        comprobante.TipoComprobante = FETipoComprobante.FACTURAS_B;
                        //comprobante.ImpTotal = total;
                        //comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(total, 2), TipoIva = FETipoIva.Iva0 });
                    }

                    #endregion

                    comprobante.ImpTotConc = 0;
                    //comprobante.ImpNeto = totalNeto;
                    comprobante.ImpOpEx = 0;
                    comprobante.Observaciones = "Periodo facturado " + fc.PeriodoDesde.ToString("dd/MM/yyyy") + " al " + fc.PeriodoHasta.ToString("dd/MM/yyyy");

                    comprobante.DetalleIva = new List<FERegistroIVA>();
                    comprobante.Tributos = new List<FERegistroTributo>();

                    double importeDetalle = 0;
                    foreach (var detalle in fc.FacturasDetalle)
                    {
                        importeDetalle = double.Parse(detalle.PrecioUnitario.ToString());
                        if (fc.Tipo != "RI")
                        {// && condicionIva != "EX")
                            if (detalle.Iva > 0)
                                importeDetalle += (importeDetalle * 0.21);
                        }
                        //importeDetalle += (importeDetalle * 0.21);
                        importeDetalle = Math.Round(importeDetalle, 2);

                        //double importeDetalle = double.Parse(detalle.PrecioUnitario.ToString());
                        //double importeIva = importeDetalle * 0.21;
                        //double totalDetalle = importeDetalle + importeIva;

                        //if (fc.Tipo != "RI")
                        //importeDetalle += (importeDetalle * 0.21);
                        comprobante.ItemsDetalle.Add(new FEItemDetalle()
                        {
                            Cantidad = detalle.Cantidad,
                            Descripcion = detalle.Concepto,
                            Precio = importeDetalle,
                            Codigo = detalle.IDFacturaDetalle.ToString(),
                        });

                        if (fc.Tipo == "RI")
                        {

                            switch (detalle.Iva.ToString())
                            {
                                case "21,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle.PrecioUnitario, FETipoIva.Iva21);
                                    break;
                                case "0,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle.PrecioUnitario, FETipoIva.Iva0);
                                    break;
                            }
                        }
                        else
                        {
                            /*comprobante.ItemsDetalle.Add(new FEItemDetalle()
                            {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                Precio = Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), 2),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = detalle.Bonificacion
                            });*/

                            if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0))
                            {
                                comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp += Math.Round(importeDetalle, 2);
                            }
                            else
                            {
                                //if (tipo != "FCC" && tipo != "NDC" && tipo != "NCC" && tipo != "RCC")
                                comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(importeDetalle, 2), TipoIva = FETipoIva.Iva0 });
                            }
                        }
                    }

                    #endregion

                    fc.FechaProceso = DateTime.Now;
                    try
                    {
                        fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["wsaa"], pathCertificado, pathTemplateFc);
                        string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                        var pathPdf = HttpContext.Current.Server.MapPath("/files/facturas/") + numeroComprobante + ".pdf";
                        fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc);

                        fc.FechaCAE = comprobante.FechaVencCAE;
                        fc.CAE = comprobante.CAE;
                        fc.UsuarioProceso = usu.Usuario;
                        fc.Numero = numeroComprobante;
                        fc.Error = null;
                        fc.FechaError = null;

                        dbContext.SaveChanges();

                        //desc = "Todas las facturas se han generado correctamente";
                    }
                    catch (Exception ex)
                    {
                        fc.Error = ex.Message;
                        fc.FechaError = DateTime.Now;
                        dbContext.SaveChanges();

                        throw new Exception(ex.Message);
                    }
                }
                else
                    throw new Exception("No hay facturas a generar");
            }
        }
        //else
        //    desc = "Error. Vuelva a iniciar sesión";

        //return desc;
    }

    public static void AgregarDetalleIvaTotalizado(ref FEComprobante comprobante, decimal precio, FETipoIva tipoiva)
    {
        if (comprobante.DetalleIva.Any(x => x.TipoIva == tipoiva))
            comprobante.DetalleIva.Where(x => x.TipoIva == tipoiva).FirstOrDefault().BaseImp += Math.Round(double.Parse(precio.ToString()), 2);
        else
            comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = Math.Round(double.Parse(precio.ToString()), 2), TipoIva = tipoiva });
    }

    [WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string comercio, string cuit, string fc, string cbu)
    {
        string fileName = "FacturasManuales";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.Facturas.Include("FacturasDetalle").Include("Comercios")
                        .Where(x => x.Modo == "R" && x.IDComercio.HasValue).AsEnumerable();
                    if (comercio != "")
                    {
                        var idComercio = int.Parse(comercio);
                        info = info.Where(x => x.IDComercio == idComercio);
                    }
                    if (cuit != "")
                    {
                        info = info.Where(x => x.NroDocumento.ToLower().Contains(cuit.ToLower()));
                    }
                    if (fc != "")
                    {
                        info = info.Where(x => x.Numero != null && x.Numero.ToLower().Contains(fc.ToLower()));
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.PeriodoDesde >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.PeriodoHasta <= dtHasta);
                    }
                    if (cbu != string.Empty)
                    {
                        info = info.Where(x => x.Comercios.FormaPago_CBU.Contains(cbu));
                    }

                    info = info.ToList();

                    dt = info.Select(x => new
                    {
                        Tipo = x.Comercios.IDMarca.HasValue ? x.Comercios.Marcas.TipoTarjeta : "",
                        Franquicia = x.Comercios.IDFranquicia.HasValue ? x.Comercios.Franquicias.NombreFantasia : "",
                        CUIT = x.Comercios.NroDocumento,
                        NombreFantasia = x.Comercios.NombreFantasia,
                        RazonSocial = x.Comercios.RazonSocial,
                        Domicilio = x.Comercios.DomiciliosFiscal != null ? x.Comercios.DomiciliosFiscal.Domicilio + " " + x.Comercios.DomiciliosFiscal.PisoDepto : "",
                        Localidad = x.Comercios.DomiciliosFiscal != null && x.Comercios.DomiciliosFiscal.Ciudad.HasValue ? x.Comercios.DomiciliosFiscal.Ciudades.Nombre : "",
                        Provincia = x.Comercios.DomiciliosFiscal != null ? x.Comercios.DomiciliosFiscal.Provincias.Nombre : "",
                        CP = x.Comercios.DomiciliosFiscal != null ? x.Comercios.DomiciliosFiscal.CodigoPostal : "",
                        CondicionIva = x.Tipo,

                        //Arancel = x.Comercios.POSArancel ?? 0,
                        //Puntos = x.Comercios.POSPuntos ?? 0,
                        //CostoTransaccion = x.Comercios.CobrarUsoRed ? "SI" : "NO",
                        //ImporteArancel = x.FacturasDetalle.Where(y => y.Concepto.ToLower().Contains("arancel")).First().PrecioUnitario,
                        //ImportePuntos = x.FacturasDetalle.Where(y => y.Concepto.ToLower().Contains("puntos")).First().PrecioUnitario,
                        //ImporteTrVisa = x.FacturasDetalle.Where(y => y.Concepto.ToLower().Contains("red pos")).First().PrecioUnitario,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        Email = x.Comercios.Email,
                        EmailsEnvioFc = x.Comercios.EmailsEnvioFc,
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Numero = x.Numero,//.Trim(),
                        CAE = x.CAE,
                        CBU = x.Comercios.FormaPago_CBU

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [WebMethod(true)]
    public static void pasarAContabilium()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                dbContext.Database.Connection.ConnectionString = ConfigurationManager.AppSettings["FidelyPlusConnection"];

                var pasaje = dbContext.Database.ExecuteSqlCommand("exec IntegracionFidelyPlusFacturas", new object[] { });

                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "integracion", pasaje.ToString());

            }

        }

    }

    [WebMethod(true)]
    public static string obtenerRetenciones(int idFactura)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (idFactura > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.FacturasRetenciones.Where(x => x.IDFactura == idFactura).ToList();

                    html += "<tr>";
                    html += "<td>IIBB</td>";

                    var iibb = list.Where(x => x.Tipo == "IIBB").FirstOrDefault();
                    if (iibb != null)
                    {
                        html += "<td><input  type='text' id='txtNroRetIIBB' maxlength='50' class='form-control' value='" + iibb.NroRetencion + "'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtIIBB' maxlength='10' class='form-control number' value='" + iibb.Importe.ToString().Replace(",", ".") + "'/></td>";
                    }
                    else
                    {
                        html += "<td><input type='text' id='txtNroRetIIBB' maxlength='50' class='form-control'/></td>";
                        html += "<td><input  type='number' min='0.01' step='0.01'  id='txtIIBB' maxlength='10' class='form-control number' value='0.00'/></td>";
                        html += "</tr>";
                    }

                    html += "<tr>";
                    html += "<td>IVA</td>";
                    var iva = list.Where(x => x.Tipo == "IVA").FirstOrDefault();
                    if (iva != null)
                    {
                        html += "<td><input type='text' id='txtNroRetIVA' maxlength='50' class='form-control' value='" + iva.NroRetencion + "'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtIVA' maxlength='10' class='form-control number' value='" + iva.Importe.ToString().Replace(",", ".") + "'/></td>";
                    }
                    else
                    {
                        html += "<td><input type='text' id='txtNroRetIVA' maxlength='50' class='form-control'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'   id='txtIVA' maxlength='10' class='form-control number' value='0.00'/></td>";
                        html += "</tr>";
                    }
                    html += "<tr>";
                    html += "<td>GANANCIAS</td>";
                    var ganancias = list.Where(x => x.Tipo == "GANANCIAS").FirstOrDefault();
                    if (ganancias != null)
                    {
                        html += "<td><input type='text' id='txtNroRetGAN' maxlength='50' class='form-control' value='" + ganancias.NroRetencion + "'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtGANANCIAS' maxlength='10' class='form-control number' value='" + ganancias.Importe.ToString().Replace(",", ".") + "'/></td>";
                    }
                    else
                    {
                        html += "<td><input type='text' id='txtNroRetGAN' maxlength='50' class='form-control'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtGANANCIAS' maxlength='10' class='form-control number' value='0.00'/></td>";
                        html += "</tr>";
                    }
                    html += "<tr>";
                    html += "<td>OTROS</td>";
                    var otros = list.Where(x => x.Tipo == "OTROS").FirstOrDefault();
                    if (otros != null)
                    {
                        html += "<td><input type='text' id='txtNroRetOTR' maxlength='50' class='form-control' value='" + otros.NroRetencion + "'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtOTROS' maxlength='10' class='form-control number' value='" + otros.Importe.ToString().Replace(",", ".") + "'/></td>";
                    }
                    else
                    {
                        html += "<td><input type='text' id='txtNroRetOTR' maxlength='50' class='form-control'/></td>";
                        html += "<td><input type='number' min='0.01' step='0.01'  id='txtOTROS' maxlength='10' class='form-control number' value='0.00'/></td>";
                        html += "</tr>";
                    }
                }
            }
        }
        return html;
    }

    [WebMethod(true)]
    public static void guardarRetencion(int idFactura, string importeIIBB, string nroIIBB, string importeIVA, string nroIVA, string importeGanancias, string nroGanancias, string importeOtros, string nroOtros)
    {
        using (var dbContext = new ACHEEntities())
        {
            FacturasRetenciones retencionIIBB = new FacturasRetenciones();
            FacturasRetenciones retencionIVA = new FacturasRetenciones();
            FacturasRetenciones retencionGAN = new FacturasRetenciones();
            FacturasRetenciones retencionOTR = new FacturasRetenciones();
            decimal totRetenciones = 0;
            if (idFactura > 0)
            {
                var factura = dbContext.Facturas.Where(X => X.IDFactura == idFactura).FirstOrDefault();
                if (factura.FacturasRetenciones.Sum(x => x.Importe) > 0)
                    factura.ImporteADebitar = factura.ImporteADebitar + factura.FacturasRetenciones.Sum(x => x.Importe);

                var list = dbContext.FacturasRetenciones.Where(x => x.IDFactura == idFactura).ToList();

                if (list.Any(x => x.Tipo.ToLower() == "iibb"))
                {
                    retencionIIBB = list.Where(x => x.Tipo.ToLower() == "iibb").FirstOrDefault();
                    retencionIIBB.NroRetencion = nroIIBB;
                    retencionIIBB.Importe = importeIIBB != "" ? Convert.ToDecimal(importeIIBB.Replace(".", ",")) : 0;
                    totRetenciones += retencionIIBB.Importe;
                }
                else if (importeIIBB != "" && Convert.ToDecimal(importeIIBB) > 0)
                {
                    retencionIIBB.IDFactura = idFactura;
                    retencionIIBB.NroRetencion = nroIIBB;
                    retencionIIBB.Importe = Convert.ToDecimal(importeIIBB.Replace(".", ","));
                    totRetenciones += retencionIIBB.Importe;
                    retencionIIBB.Tipo = "IIBB";
                    dbContext.FacturasRetenciones.Add(retencionIIBB);
                }
                //else if (nroGanancias == "" && Convert.ToDecimal(importeIIBB) > 0)
                //    throw new Exception("Si elige un Importe, el campo de Nro Retencion respecto al importe no puede estar vacio");

                if (list.Any(x => x.Tipo.ToLower() == "iva"))
                {
                    retencionIVA = list.Where(x => x.Tipo.ToLower() == "iva").FirstOrDefault();
                    retencionIVA.NroRetencion = nroIVA;
                    retencionIVA.Importe = importeIVA != "" ? Convert.ToDecimal(importeIVA.Replace(".", ",")) : 0;
                    totRetenciones += retencionIVA.Importe;
                }
                else if (importeIVA != "" && Convert.ToDecimal(importeIVA) > 0)
                {
                    retencionIVA.IDFactura = idFactura;
                    retencionIVA.NroRetencion = nroIVA;
                    retencionIVA.Importe = Convert.ToDecimal(importeIVA.Replace(".", ","));
                    totRetenciones += retencionIVA.Importe;

                    retencionIVA.Tipo = "IVA";
                    dbContext.FacturasRetenciones.Add(retencionIVA);
                }
                //else if (nroGanancias == "" && Convert.ToDecimal(importeIVA) > 0)
                //    throw new Exception("Si elige un Importe, el campo de Nro Retencion respecto al importe no puede estar vacio");

                if (list.Any(x => x.Tipo.ToLower() == "ganancias"))
                {
                    retencionGAN = list.Where(x => x.Tipo.ToLower() == "ganancias").FirstOrDefault();
                    retencionGAN.NroRetencion = nroGanancias;
                    retencionGAN.Importe = importeGanancias != "" ? Convert.ToDecimal(importeGanancias.Replace(".", ",")) : 0;
                    totRetenciones += retencionGAN.Importe;

                }
                else if (importeGanancias != "" && Convert.ToDecimal(importeGanancias) > 0)
                {
                    retencionGAN.IDFactura = idFactura;
                    retencionGAN.NroRetencion = nroGanancias;
                    retencionGAN.Importe = Convert.ToDecimal(importeGanancias.Replace(".", ","));
                    totRetenciones += retencionGAN.Importe;

                    retencionGAN.Tipo = "GANANCIAS";
                    dbContext.FacturasRetenciones.Add(retencionGAN);
                }

                if (list.Any(x => x.Tipo.ToLower() == "otros"))
                {
                    retencionOTR = list.Where(x => x.Tipo.ToLower() == "otros").FirstOrDefault();
                    retencionOTR.NroRetencion = nroOtros;
                    retencionOTR.Importe = importeOtros != "" ? Convert.ToDecimal(importeOtros.Replace(".", ",")) : 0;
                    totRetenciones += retencionOTR.Importe;

                }
                else if (importeOtros != "" && Convert.ToDecimal(importeOtros) > 0)
                {
                    retencionOTR.IDFactura = idFactura;
                    retencionOTR.NroRetencion = nroOtros;
                    retencionOTR.Importe = Convert.ToDecimal(importeOtros.Replace(".", ","));
                    totRetenciones += retencionOTR.Importe;

                    retencionOTR.Tipo = "OTROS";
                    dbContext.FacturasRetenciones.Add(retencionOTR);
                }


                //resto retenciones al importe a debitar 

                if (totRetenciones > 0)
                {

                    factura.ImporteADebitar = factura.ImporteADebitar - totRetenciones;
                }

                //else if (nroGanancias == "" && Convert.ToDecimal(importeGanancias) > 0)
                //    throw new Exception("Si elige un Importe, el campo de Nro Retencion respecto al importe no puede estar vacio");

                dbContext.SaveChanges();
            }
            else
                throw new Exception("No existe una factura asocida a la ninguna retencion");
        }
    }
}