﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_movimientos : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Movimientos.Include("Comercios")
                    .Where(x => x.IDComercio.HasValue)
                    .OrderBy(x => x.Fecha)
                    .Select(x => new MovimientosViewModel()
                    {
                        ID = x.IDMovimiento,
                        IDEntidad = x.IDComercio.Value,
                        Nombre = x.Comercios.NombreFantasia,
                        Fecha = x.Fecha,
                        Importe = x.Precio,
                        Concepto = x.Concepto,
                        CUIT = x.Comercios.NroDocumento,
                        Facturada = x.IDFactura.HasValue ? "Si" : "No"
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.Fecha <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Movimientos.Where(x => x.IDMovimiento == id).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.IDFactura.HasValue)
                            throw new Exception("Esta venta ya fue facturada. No se puede eliminar");
                        else
                        {
                            dbContext.Movimientos.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}