﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_facturas_prov : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Facturas.Include("Proveedores").Where(x => x.Modo == "P" && x.IDProveedor.HasValue)
                    .OrderByDescending(x => x.PeriodoDesde)
                    .Select(x => new FacturasViewModel()
                    {
                        ID = x.IDFactura,
                        IDEntidad = x.IDProveedor.Value,
                        Numero = x.Numero.Trim(),
                        Nombre = x.Proveedores.NombreFantasia,
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        CAE = x.CAE,
                        CUIT = x.Comercios.NroDocumento,
                        Enviada = x.FechaEnvio.HasValue ? "Si" : "No",
                        Descargada = x.FechaRecibida.HasValue ? "Si" : "No"
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaDesde >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaHasta <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Facturas.Where(x => x.IDFactura == id).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.FechaCAE.HasValue)
                            throw new Exception("No se puede eliminar una factura con CAE");
                        else
                        {
                            int aux = dbContext.EliminarFactura(id);
                            //dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string obtenerDetalle(int idFactura)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (idFactura > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.FacturasDetalle.Where(x => x.IDFactura == idFactura).ToList();
                    if (list.Any())
                    {
                        foreach (var detalle in list)
                        {
                            html += "<tr>";
                            html += "<td>" + detalle.Concepto + "</td>";
                            html += "<td>" + detalle.PrecioUnitario.ToString("N2") + "</td>";
                            html += "<td>" + detalle.Iva.ToString("N2") + "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
                }
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Facturas.Include("Proveedores").Where(x => x.FechaError.HasValue && !x.FechaCAE.HasValue && x.Modo == "P" && x.IDProveedor.HasValue).ToList();
                if (list.Any())
                {
                    foreach (var fc in list)
                    {
                        html += "<tr>";
                        html += "<td>" + fc.Proveedores.NombreFantasia + "</td>";
                        html += "<td>" + fc.PeriodoDesde.ToShortDateString() + "-" + fc.PeriodoHasta.ToShortDateString() + "</td>";
                        if (fc.FechaError.HasValue)
                            html += "<td>" + fc.FechaError.Value.ToShortDateString() + "</td>";
                        else
                            html += "<td></td>";
                        html += "<td>" + fc.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    [WebMethod(true)]
    public static string ObtenerCAE(int importe)
    {
        var desc = "";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
            
            using (var dbContext = new ACHEEntities())
            {
                var facturas = dbContext.Facturas.Include("Proveedores").Include("FacturasDetalle")
                    .Where(x => !x.FechaCAE.HasValue && x.Modo == "P" && x.IDProveedor.HasValue && x.ImporteTotal > importe)
                    .OrderBy(x => x.PeriodoDesde)
                    .ToList();

                int cantError = 0;
                int cantTotal = facturas.Count();
                int cantOK = 0;

                if (cantTotal > 0)
                {
                    //Pre validacion
                    string cuitCliente = string.Empty;
                    foreach (var fc in facturas)
                    {
                        var condicionIva = fc.Proveedores.CondicionIva;
                        cuitCliente = fc.Proveedores.NroDocumento.ReplaceAll("-", "").Trim();

                        if (cuitCliente == string.Empty)
                        {
                            throw new Exception("El cliente " + fc.Proveedores.NombreFantasia + " (" + condicionIva + ") no tiene un CUIT cargado");
                        }
                    }
                    
                    FEFacturaElectronica fe = new FEFacturaElectronica();
                    FEComprobante comprobante = null;
                    var pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CertificadoAFIP"]);
                    var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Template"]);
                    int puntoVenta = int.Parse(ConfigurationManager.AppSettings["FE.PtoVta"]);
                    long cuit = long.Parse(ConfigurationManager.AppSettings["FE.CUIT"]);
                    double total = 0;
                    double totalSinIva = 0;
                    double totalNeto = 0;
                    //double iva = 0;

                    foreach (var fc in facturas)
                    {
                        #region Seteo de datos

                        total = double.Parse(fc.ImporteTotal.ToString());
                        totalSinIva = Math.Round(total, 2);
                        total += (total * 0.21);
                        total = Math.Round(total, 2);
                        cuitCliente = fc.Proveedores.NroDocumento;

                        comprobante = new FEComprobante();
                        comprobante.DetalleIva = new List<FERegistroIVA>();
                        comprobante.Tributos = new List<FERegistroTributo>();
                        comprobante.Concepto = FEConcepto.Servicio;
                        comprobante.Fecha = DateTime.Now;
                        comprobante.Cuit = cuit;
                        comprobante.PtoVta = puntoVenta;
                        comprobante.FchServDesde = fc.PeriodoDesde;// DateTime.Now.AddDays(-1);
                        comprobante.FchServHasta = fc.PeriodoHasta;//DateTime.Now;
                        comprobante.FchVtoPago = DateTime.Now.AddDays(5); //fc.PeriodoHasta.AddDays(120);//DateTime.Now.AddDays(5);
                        comprobante.CodigoMoneda = "PES";
                        comprobante.CotizacionMoneda = 1;
                        comprobante.CondicionVenta = "Contado";
                        if (fc.Tipo == "RI")
                            comprobante.ClienteCondiionIva = "Responsable Inscripto";
                        else
                            comprobante.ClienteCondiionIva = fc.Proveedores.CondicionIva;

                        #region Seteo los datos en base a la condicion iva

                        if (fc.Tipo == "RI" || fc.Tipo == "EX" || fc.Tipo == "MONOTRIBUTISTA")
                        {
                            if (fc.Proveedores.TipoDocumento == "CUIT")//TODO: MEJORAR ESTO.
                                comprobante.DocTipo = 80;// 96= DNI//80=CUIT
                            else
                                comprobante.DocTipo = 96;// 96= DNI//80=CUIT
                            
                            comprobante.DocNro = long.Parse(cuitCliente);
                            comprobante.ClienteNombre = fc.Proveedores.RazonSocial;
                            comprobante.ClienteDomicilio = fc.Proveedores.Domicilios.Domicilio;
                            //comprobante.ClienteLocalidad = fc.Proveedores.Domicilios.Ciudad;
                            comprobante.ClienteLocalidad = fc.Proveedores.Domicilios.Ciudad.HasValue ? fc.Proveedores.Domicilios.Ciudades.Nombre : "";
                        }
                        /*else
                        {
                            comprobante.DocTipo = 99;//Sin identificar/venta global diaria
                            comprobante.DocNro = 0;
                            comprobante.ClienteNombre = "Cliente final";
                            comprobante.ClienteDomicilio = "No informado";
                            comprobante.ClienteLocalidad = "No informado";
                        }*/


                        if (fc.Tipo == "RI")
                        {
                            totalNeto = totalSinIva;
                            comprobante.TipoComprobante = FETipoComprobante.FACTURAS_A;
                            //comprobante.ImpTotal = total;// Math.Round(total + iva, 2);
                            comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = totalSinIva, TipoIva = FETipoIva.Iva21 });
                        }
                        else
                        {
                            //total += (total * 0.21);
                            totalNeto = total;
                            comprobante.TipoComprobante = FETipoComprobante.FACTURAS_B;
                            //comprobante.ImpTotal = total;
                            comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = total, TipoIva = FETipoIva.Iva0 });
                        }

                        #endregion

                        comprobante.ImpTotConc = 0;
                        //comprobante.ImpNeto = totalNeto;
                        comprobante.ImpOpEx = 0;
                        comprobante.Observaciones = "";

                        double importeDetalle = 0;
                        foreach (var detalle in fc.FacturasDetalle)
                        {
                            importeDetalle = double.Parse(detalle.PrecioUnitario.ToString());
                            if (fc.Tipo != "RI")// && condicionIva != "EX")
                                importeDetalle += (importeDetalle * 0.21);
                            importeDetalle = Math.Round(importeDetalle, 2);


                            comprobante.ItemsDetalle.Add(new FEItemDetalle()
                            {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                Precio = importeDetalle,
                                Codigo = detalle.IDFacturaDetalle.ToString(),
                            });
                        }

                        #endregion

                        fc.FechaProceso = DateTime.Now;
                        try
                        {
                            fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["wsaa"], pathCertificado, pathTemplateFc);
                            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                            var pathPdf = HttpContext.Current.Server.MapPath("/files/facturas/") + numeroComprobante + ".pdf";
                            fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc);

                            fc.FechaCAE = comprobante.FechaVencCAE;
                            fc.CAE = comprobante.CAE;
                            fc.UsuarioProceso = usu.Usuario;
                            fc.Numero = numeroComprobante;
                            fc.Error = null;
                            fc.FechaError = null;

                            dbContext.SaveChanges();
                            cantOK++;
                        }
                        catch (Exception ex)
                        {
                            fc.Error = ex.Message;
                            fc.FechaError = DateTime.Now;
                            dbContext.SaveChanges();
                            cantError++;
                        }
                    }

                    if (cantError == 0)
                        desc = "Todas las facturas se han generado correctamente";
                    else
                        desc = "Se han generado " + cantOK + "/" + cantTotal + " correctamente. Haga click <a href='javascript:verFcErrores();'>aquí</a> para ver los errores";
                }
                else
                    throw new Exception("No hay facturas a generar");
            }
        }
        else
            desc = "Error. Vuelva a iniciar sesión";

        return desc;
    }

    [WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string proveedor, string cuit, string fc)
    {
        string fileName = "FacturasProv";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.Facturas.Include("FacturasDetalle").Include("Proveedores")
                        .Where(x => x.Modo == "P" && x.IDProveedor.HasValue).AsEnumerable();
                    if (proveedor != "")
                    {
                        var idProv = int.Parse(proveedor);
                        info = info.Where(x => x.IDProveedor == idProv);
                    }
                    if (cuit != "")
                        info = info.Where(x => x.NroDocumento.ToLower().Contains(cuit.ToLower()));
                    if (fc != "")
                        info = info.Where(x => x.Numero != null && x.Numero.ToLower().Contains(fc.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.PeriodoDesde >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.PeriodoHasta <= dtHasta);
                    }

                    dt = info.ToList().Select(x => new
                    {
                        CUIT = x.Proveedores.NroDocumento,
                        NombreFantasia = x.Proveedores.NombreFantasia,
                        RazonSocial = x.Proveedores.RazonSocial,
                        Domicilio = x.Proveedores.Domicilios != null ? x.Proveedores.Domicilios.Domicilio + " " + x.Proveedores.Domicilios.PisoDepto : "",
                        Localidad = x.Proveedores.Domicilios != null && x.Proveedores.Domicilios.Ciudad.HasValue ? x.Proveedores.Domicilios.Ciudades.Nombre : "",
                        Provincia = x.Proveedores.Domicilios != null ? x.Proveedores.Domicilios.Provincias.Nombre : "",
                        CP = x.Proveedores.Domicilios != null ? x.Proveedores.Domicilios.CodigoPostal : "",
                        CondicionIva = x.Tipo,
                        Subtotal = x.ImporteTotal - x.TotalIva,
                        Iva = x.TotalIva,
                        Total = x.ImporteTotal,
                        Email = x.Proveedores.Email,
                        EmailsEnvioFc = x.Proveedores.EmailsEnvioFc,
                        FechaDesde = x.PeriodoDesde,
                        FechaHasta = x.PeriodoHasta,
                        Numero = x.Numero.Trim(),
                        CAE = x.CAE
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}