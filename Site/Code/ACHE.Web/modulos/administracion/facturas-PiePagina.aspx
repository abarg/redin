﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="facturas-PiePagina.aspx.cs" Inherits="modulos_administracion_facturas_PiePagina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li class="last">Facturas Pie Página</li>
        </ul>
    </div>    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de facturas pie de página</h3>
            <div class="alert alert-danger alert-dismissable" runat="server"  id="divError"></div>
            <div class="alert alert-success alert-dismissable" runat="server" id="divOK" style="display: none">Se ha asociado la imagene a las facturas seleccionadas</div>
		    <form>
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
                            <button class="btn btn" type="button" id="Button1" onclick="nuevo();">Nuevo</button>
				    </div>
                </div>
            </form>                        
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br /><br />
        </div>
    </div>
    <div class="modal fade" id="modalGenerarPiePag">
		<div class="modal-dialog">
			<div class="modal-content">
                 <form id="Form1" runat="server"> 
                     <asp:HiddenField runat="server" ID="hdnIDFacturaPiePagina" Value="0" />
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					    <h3 class="modal-title">Generar nuevo pie de página para facturas</h3>
				    </div>

				    <div class="modal-body" runat="server" id="modalObtenerCAEbody">
               			<div id="divErrorModal" class="alert alert-danger alert-dismissable" style="display: none"></div>

			            <div class="formSep col-sm-12 col-md-12">
				            <div class="row">					    
                                <div class="col-sm-3 col-md-3">
						            <label><span class="f_req">*</span>Factura desde</label>
                                    <input id="txtFactDesde" runat="server" value="" maxlength="10" class="form-control"/>
						            <span class="help-block"></span>
					            </div>
                                <div class="col-sm-3 col-md-3">
						            <label><span class="f_req">*</span>Factura hasta</label>
                                    <input id="txtFactHasta" runat="server" value="" maxlength="10" class="form-control"/>
						            <span class="help-block"></span>
					            </div>
                               <div class="col-sm-6 col-md-6"><span class="f_req">*</span>Cargar archivo:                                
                                    <asp:FileUpload runat="server" ID="flpArch" TabIndex="1" />
                                   <span class="help-block">Extensiones: jpg/png/pdf. Tamaño:575x80 px</span>     
                                   <div id="divDescarga" style="display: none"></div>
                                                                
                                </div>                                                                 
				            </div>
                        </div>        
				    </div>
				    <div class="modal-footer">
					    <%--<button type="button" class="btn btn-succes" onclick="generar();">Generar</button>--%>
                        <asp:Button runat="server" id="btnGenerar" class="btn btn-success" type="button" OnClick="Generar" OnClientClick="return validarForm();" Text="Generar"></asp:Button>
                        <button type="button" class="btn btn-default" onclick="$('#modalGenerarPiePag').modal('hide');" >Cerrar</button>
				    </div>
                 </form>
			</div>
		</div>
	</div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">        
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/facturas-PiePagina.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>