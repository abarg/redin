﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_notase : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarCombos();

            hdnTipo.Value = Request.QueryString["Tipo"];

            //if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            //{
            //    hdnID.Value = Request.QueryString["ID"];

            //    if (hdnID.Value != "0")
            //        CargarInfo(int.Parse(hdnID.Value));

            //}

            if (hdnTipo.Value == "P")
                divComercio.Attributes.Add("style", "display:none");
            else
                divProveedor.Attributes.Add("style", "display:none");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string fecha, string concepto, int idEntidad, string precio, string tipo, string tipoMov)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    NotasCreditoDebito entity;
                    if (id > 0)
                        entity = dbContext.NotasCreditoDebito.Where(x => x.IDNota == id).FirstOrDefault();
                    else
                    {
                        entity = new NotasCreditoDebito();
                        entity.FechaAlta = DateTime.Now;
                    }

                    if (tipoMov == "P")
                    {
                        entity.IDProveedor = idEntidad;
                        entity.IDComercio = null;

                        Proveedores proveedor = dbContext.Proveedores.Where(x => x.IDProveedor == idEntidad).First();
                        entity.CondicionIva = proveedor.CondicionIva;
                        entity.NroDocumento = proveedor.NroDocumento;
                    }
                    else
                    {
                        entity.IDComercio = idEntidad;
                        entity.IDProveedor = null;

                        Comercios comercio = dbContext.Comercios.Where(x => x.IDComercio == idEntidad).First();
                        entity.CondicionIva = comercio.CondicionIva;
                        entity.NroDocumento = comercio.NroDocumento;
                    }
                    entity.Fecha = DateTime.Parse(fecha);
                    entity.ImporteTotal = decimal.Parse(precio);
                    entity.Enviada = false;
                    entity.Tipo = tipo;
                    entity.Concepto = concepto;
                    

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.NotasCreditoDebito.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    /*private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Movimientos.Where(x => x.IDMovimiento == id).FirstOrDefault();
            if (entity != null)
            {
                txtFecha.Text = entity.Fecha.ToShortDateString();
                ddlEmpresa.SelectedValue = entity.IDEmpresa.ToString();
                if (entity.IDServicio.HasValue)
                    ddlServicio.SelectedValue = entity.IDServicio.Value.ToString();
                txtConcepto.Text = entity.Concepto;
                chkFacturada.Checked = entity.IDFactura.HasValue;
                txtImporte.Text = entity.Precio.ToString();
                if (entity.IDFactura.HasValue)
                    txtFecha.Enabled = ddlEmpresa.Enabled = txtConcepto.Enabled = txtImporte.Enabled = ddlServicio.Enabled = false;
            }
        }
    }*/

    private void CargarCombos()
    {
        ddlComercio.DataSource = Common.LoadCasasMatrices();
        ddlComercio.DataBind();

        ddlProveedor.DataSource = Common.LoadProveedores();
        ddlProveedor.DataBind();
    }
}
