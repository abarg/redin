﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="seccion-manual-ayuda.aspx.cs" Inherits="modulos_administracion_seccion_manual_ayuda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Manual ayuda</a></li>
        </ul>
    </div> 
     <a class="btn btn-success"  href="seccion-manual-ayudae.aspx">Nuevo</a><br/><br/>

    <div class="row">
        <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
        <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Se ha eliminado correctamente.</div>
        <div class="col-sm-12 col-md-12" id="tblSecciones">
            <table class="table table-striped table-bordered mediaTable" >
				<thead>
					<tr>
                        <th class="essential" style="width:50px">Editar</th>
                        <th class="essential" style="width:50px">Eliminar</th>
                        <th class="essential">Titulo</th>
                        <th class="essential">Orden</th>
                        <th class="essential">Perfil</th>
					</tr>
				</thead>
				<tbody id="bodySecciones">
                    <tr><td colspan="5">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
    </div>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/seccion-manual-ayuda.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

