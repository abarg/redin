﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Collections.Specialized;

public partial class modulos_administracion_facturasenvio_prov : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
            //DateTime dsd=DateTime.Now.AddDays(-1);
            //DateTime hst=DateTime.Now.AddDays(1);
            txtFechaDesde.Text = "";//DateTime.Now.AddMonths(-1).AddDays(-1).ToString("dd/MM/yyyy");
            txtFechaHasta.Text = "";//DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            CargarFacturas(txtFechaDesde.Text, txtFechaHasta.Text);
        }
    }

    private void CargarFacturas(string fechaDesde, string fechaHasta)
    {
        using (var dbContext = new ACHEEntities())
        {
            List<ComboViewModel> facturasCombo = new List<ComboViewModel>();

           var facturas = dbContext.Facturas.Include("Proveedores")
                .Where(x => x.FechaCAE.HasValue && !x.Enviada && x.IDProveedor.HasValue).ToList()
                .Select(x => new FacturasProveedoresViewModel{ 
                    FechaDesde = x.PeriodoDesde,
                    FechaHasta = x.PeriodoHasta,
                    IDProveedor = (int)x.IDProveedor,
                    IDFactura = x.IDFactura,
                    Numero = x.Numero,
                    NombreFantasia = x.Proveedores.NombreFantasia
                });


           if (fechaDesde != null && fechaDesde != "") {
               DateTime dsd = Convert.ToDateTime(fechaDesde).AddDays(-1);
               facturas = facturas.Where(x => x.FechaDesde > dsd);
           }
           if (fechaHasta != null && fechaHasta != "") {
               DateTime hst = Convert.ToDateTime(fechaHasta).AddDays(1);
               facturas = facturas.Where(x => x.FechaHasta < hst);
           }

                foreach (var x in facturas) {
                    ComboViewModel factu = new ComboViewModel();                
                factu.ID =  x.IDProveedor.ToString() + "_" + x.IDFactura.ToString() + "_" + x.Numero;
                factu.Nombre =  x.NombreFantasia + " - " + x.Numero;
                facturasCombo.Add(factu);
                }
                
            searchable.DataSource = facturasCombo.OrderBy(x => x.Nombre).ToArray();;
            searchable.DataBind();
        }
    }

    [WebMethod(true)]
    public static EnvioMailsViewModel enviar(string ids, string items, string asunto, string mensaje)
    {
        using (var dbContext = new ACHEEntities())
        {
            string[] aux = ids.Split(',');
            string[] item;
            MailAddressCollection listaMail;
            int idProveedor = 0;
            int cantErrores = 0;
            int cantOK = 0;
            string detalleErrores = string.Empty;
            string titulo = string.Empty;

            if (aux.Length > 0)
            {
                #region EnvioMasivo

                foreach (var id in aux)
                {
                    if (id != "")
                    {
                        item = id.Split('_');
                        if (item.Length >= 2)
                        {
                            idProveedor = int.Parse(item[0]);
                            var entity = dbContext.Proveedores.Where(x => x.IDProveedor == idProveedor).FirstOrDefault();
                            if (entity != null)
                            {
                                string mails = entity.EmailsEnvioFc;
                                if (!string.IsNullOrEmpty(mails))
                                {
                                    listaMail = new MailAddressCollection();
                                    foreach (string mail in mails.Trim().Split(";"))
                                    {
                                        if (mail.Trim() != string.Empty)
                                        {
                                            if (mail.Trim().IsValidEmailAddress())
                                            {
                                                //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), mail,"");
                                                listaMail.Add(new MailAddress(mail.Trim()));
                                            }
                                            else
                                                detalleErrores += "La direccion " + mail.Trim() + " del proveedor " + entity.NombreFantasia + " no es válida<br/>";
                                        }
                                    }

                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), listaMail.Count.ToString(), "");
                                    ListDictionary replacements = new ListDictionary();
                                    replacements.Add("<CLIENTE>", entity.NombreFantasia);
                                    replacements.Add("<MENSAJE>", mensaje);

                                    List<string> attachments = new List<string>();
                                    if (item[2] != string.Empty)
                                        attachments.Add(HttpContext.Current.Server.MapPath("~/files/facturas/" + item[2] + ".pdf"));

                                    bool send = EmailHelper.SendMessage(EmailTemplate.EnvioFactura, replacements, ConfigurationManager.AppSettings["Email.Facturacion"], listaMail, "RedIN :: " + asunto, attachments);
                                    if (!send)
                                    {
                                        cantErrores++;
                                        if (item.Length == 3)
                                            detalleErrores += "No se pudo enviar la Fc Nro " + item[2] + " del proveedor " + entity.NombreFantasia + ".<br/>";
                                        else
                                            detalleErrores += "No se pudo enviar la Fc ID " + item[1] + " del proveedor " + entity.NombreFantasia + ".<br/>";
                                    }
                                    else
                                    {
                                        cantOK++;
                                        dbContext.Database.ExecuteSqlCommand(@"UPDATE FACTURAS SET Enviada=1, FechaEnvio=GETDATE() WHERE IDFactura=" + item[1]);
                                    }
                                    //else
                                    //    ActualizarFechaTarea(int.Parse(item.Value), idsVencimientos);
                                }

                                else
                                {
                                    cantErrores++;
                                    detalleErrores += "El proveedor " + entity.NombreFantasia + " no tiene cargados mails.<br/>";
                                }
                            }
                        }
                    }
                }

                if (cantOK < aux.Length)
                    titulo = "Se han enviado " + cantOK + "/" + aux.Length + " correctamente. Hubo " + cantErrores + " errores. Haga click <a href=\"javascript:verEnvioErrores('" + detalleErrores + "');\">aquí</a> para ver los errores";

                #endregion
            }

            return new EnvioMailsViewModel() { CantErrors = cantErrores, CantEnvios = cantOK, Mensaje = titulo };
        }
    }

    protected void Buscar(Object sender, EventArgs e) {
        CargarFacturas(txtFechaDesde.Text, txtFechaHasta.Text);
    }

}