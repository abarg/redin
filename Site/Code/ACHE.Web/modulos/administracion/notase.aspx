﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="notase.aspx.cs" Inherits="modulos_administracion_notase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/notase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li><a id="lnkTitulo" href="<%= ResolveUrl("~/modulos/administracion/notas.aspx") %>">NC/ND</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Edición de NC/ND</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                 <asp:HiddenField runat="server" ID="hdnTipo" Value="" />

                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Tipo</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control required">
                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Nota de Débito" Value="ND"></asp:ListItem>
                            <asp:ListItem Text="Nota de Crédito" Value="NC"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" id="divComercio" runat="server">
                    <label for="u_Precio" class="col-lg-3 control-label"><span class="f_req">*</span> Comercio</label>
                    <div class="col-lg-6">
                         <asp:DropDownList runat="server" ID="ddlComercio" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un comercio"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" id="divProveedor" runat="server">
                    <label for="u_Precio" class="col-lg-3 control-label"><span class="f_req">*</span> Proveedor</label>
                    <div class="col-lg-6">
                         <asp:DropDownList runat="server" ID="ddlProveedor" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un proveedor"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Fecha</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Concepto</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtConcepto" CssClass="form-control required" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Importe neto</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtImporte" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a id="lnkCancelar" href="notas.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</asp:Content>

