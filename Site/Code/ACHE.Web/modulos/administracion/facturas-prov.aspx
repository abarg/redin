﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="facturas-prov.aspx.cs" Inherits="modulos_administracion_facturas_prov" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/facturas-prov.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li class="last">Facturas proveedores</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de facturas de proveedores</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>
		    <form>
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>
						    <span class="help-block"></span>
					    </div>
                       
                        <div class="col-sm-3 col-md-3">
						    <label>Proveedor</label>
                            <select ID="ddlProveedor" class="chzn_b form-control" data-placeholder="Seleccione un proveedor">
                                <option value=""></option>
                            </select>
					    </div>
                        <div class="col-sm-2">
                            <label>CUIT</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <label>Número Fc</label>
                            <input id="txtNumero"  value="" maxlength="50" class="form-control"/>
                        </div>
                        
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filtrar();">Buscar</button>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="nuevo();">Nueva</button>
                            <button class="btn btn-success" type="button" id="btnFE" onclick="confirmarCAE();">Obtener CAE</button>--%>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Facturas" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br /><br />
        </div>
    </div>

    <div class="modal fade" id="modalObtenerCAE">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title">Obtener CAE</h3>
				</div>
				<div class="modal-body" id="modalObtenerCAEbody">
					<div class="alert alert-info">Al darle aceptar, ustedes informará a la AFIP todas las facturas generadas. ¿Desea continuar?.</div>
					Informar facturas mayores a <input type="text" id="txtImporteCAE" maxlength="4" class="form-control number" value="0" style="width:100px; display:inline"/>

                    <label id="divErrorCAE" style="display: none" class="error">Por favor, ingrese el importe</label>
                    
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-succes" onclick="generarCAE();">Obtener</button>
                    <button type="button" class="btn btn-default" onclick="$('#modalObtenerCAE').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

</asp:Content>

