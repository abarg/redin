﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="proveedorese.aspx.cs" Inherits="modulos_administracion_proveedorese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/proveedorese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <style type="text/css">
        #map-canvas {
          height:280px;background-color:transparent;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/modulos/administracion/Proveedores.aspx") %>">Proveedores</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Proveedores</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabDomicilioF" data-toggle="tab">Domicilio Fiscal</a></li>
                </ul>
                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de Alta</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFechaAlta" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Razón Social</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control required" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre de Fantasia</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombreFantasia" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Tipo y Nro Doc.</label>
                                <div class="col-sm-2 col-md-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTipoDoc">
                                        <asp:ListItem Value="CUIT" Text="CUIT" />
                                        <asp:ListItem Value="DNI" Text="DNI" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNroDocumento" CssClass="form-control required number" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Condición Iva</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlIVA">
                                        <asp:ListItem Value="RI" Text="Resp. Inscripto" />
                                        <asp:ListItem Value="MONOTRIBUTISTA" Text="Monotributista" />
                                        <asp:ListItem Value="EX" Text="Exento/No Resp." />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Email para envíos de fc</label>
                                <div class="col-lg-6">
                                    <asp:TextBox runat="server" ID="txtEnvioFc" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    <span class="help-block">Puede ingresar más de una dirección separados mediante ";"</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtCelular" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Web</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtWeb" CssClass="form-control" MaxLength="225"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtObservaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabDomicilioF">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais">
                                        <asp:ListItem Value="Argentina" Text="Argentina"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia" onchange="LoadCiudades(this.value,'ddlCiudad');">
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                        data-placeholder="Seleccione una ciudad">
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fax</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFax" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Geolocalizar</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLatitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Latitud</span>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtLongitud" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    <span class="help-block">Longitud</span>
                                </div>
                                 <a href="javascript:showMap();" id="btnMap" class="btn">Ver mapa</a>
                            </div>
                        </div>
                      
                        <div class="form-group" id="formButtons">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="proveedores.aspx" class="btn btn-link">Cancelar</a>

                                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myMapModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="modalTitle">Mapa</h4>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">   
                 <div id="map-canvas"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <script>
        var drag = false;
        var infowindow;

        /*$('#btnMap').click(function () {
            showMap();
        });*/

        function computepos(point) {
            $("#txtLatitud").val(point.lat().toFixed(6));
            $("#txtLongitud").val(point.lng().toFixed(6));
        }

        function drawMap(latitude, longitude) {
            var point = new google.maps.LatLng(latitude, longitude);

            var mapOptions = {
                zoom: 14,
                center: point,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: point
            })
            marker.setMap(map);
            map.setCenter(point);

            google.maps.event.addListener(map, 'click', function (event) {
                if (drag) { return; }
                if (map.getZoom() < 10) { map.setZoom(10); }
                map.panTo(event.latLng);
                computepos(event.latLng);
            });

            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "</p></div>";

                infowindow = new google.maps.InfoWindow({ content: html });
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(marker, 'dragstart', function () { if (infowindow) { infowindow.close(); } });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                //if (map.getZoom() < 10) { map.setZoom(10); }
                map.setCenter(event.latLng);
                computepos(event.latLng);
                drag = true;
                setTimeout(function () { drag = false; }, 250);
            });

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(point);
            });

            $("#myMapModal").modal("show");
        }

        function showMap() {
            var embAddr = $("#txtDomicilio").val() + ',' + $("#ddlCiudad option:selected").text() + ',' + $("#ddlProvincia option:selected").text();
            $("#modalTitle").html(embAddr);

            if ($("#txtLatitud").val() == "" && $("#txtLongitud").val() == "") {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': embAddr }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        drawMap(latitude, longitude);

                        $("#txtLatitud").val(latitude);
                        $("#txtLongitud").val(longitude);
                    }
                    else {
                        $("#map-canvas").html('Could not find this location from the address given.<p>' + embAddr + '</p>');
                    }
                });
            }
            else {
                var latitude = $("#txtLatitud").val();
                var longitude = $("#txtLongitud").val();

                drawMap(latitude, longitude);
            }
        };
    </script>
</asp:Content>

