﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pagos-prov.aspx.cs" Inherits="modulos_administracion_pagos_prov" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/pagos-prov.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li class="last">Pagos recibidos de proveedores</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de pagos recibidos de proveedores</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form>
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>
						    <span class="help-block"></span>
					    </div>
                       
                        <div class="col-sm-3 col-md-3">
						    <label>Proveedor</label>
                            <select ID="ddlProveedor" class="chzn_b form-control" data-placeholder="Seleccione un proveedor">
                                <option value=""></option>
                            </select>
					    </div>
                        <div class="col-sm-2">
                            <label>CUIT</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-3 col-md-3"></div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filtrar();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="nuevo();">Nuevo</button>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br /><br />
        </div>
    </div>
</asp:Content>

