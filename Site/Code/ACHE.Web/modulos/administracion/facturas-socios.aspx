﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="facturas-socios.aspx.cs" Inherits="modulos_administracion_facturas_socios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/facturas-socios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li class="last">Notas de Venta y Liq. Producto - Socios</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de Notas de Venta y Liq. Producto de Socios</h3>
           <asp:Literal runat="server" ID="litErrorFechas" Visible="false"> <div class="alert alert-danger alert-dismissable"> "Las fechas no pueden estar vacias" </div></asp:Literal>
            <asp:Literal runat="server" ID="litError" Visible="false"> <div class="alert alert-danger alert-dismissable"> "Las Notas de Venta y Liq. Producto no se han podido crear" </div></asp:Literal>
             <asp:Literal runat="server" ID="litOk" Visible="false"> <div class="alert alert-success alert-dismissable"> "Las Notas de Venta y Liq. Producto se han generado correctamente"</div></asp:Literal>
             <asp:Literal runat="server" ID="litErrorTipo" Visible="false"> <div class="alert alert-danger alert-dismissable"> "El formato del adjunto no es valido"</div></asp:Literal>
            <%--<div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>--%>
            <%--<div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>--%>
		    <form>
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2">
                            <label>Nro Documento</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <label>Número Fc</label>
                            <input id="txtNumero"  value="" maxlength="50" class="form-control"/>
                        </div>
                        
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filtrar();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnFE" onclick="generar();">Generar</button>
                            <div class="btn-group" id="btnMasAccciones" runat="server">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a onclick="exportar();">Exportar a Excel</a></li>
                                    <li><a href="facturas-PiePagina.aspx">Subir imagen Imprenta</a></li>
						        </ul>
					        </div>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Facturas" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br /><br />
        </div>
    </div>
    <form runat="server">
        <div class="modal fade" id="modalGenerar">
		    <div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					    <h3 class="modal-title">Generar Notas de Venta y Liq. Producto</h3>
				    </div>

				    <div class="modal-body" id="modalGenerarbody">
					    <div class="alert alert-info" >
                            En caso que desee regenerar un mes, tenga en cuenta que deberá regenerar los meses siguientes también.
                        </div>
                        <br />        
                        
                        Fecha desde: <asp:TextBox runat="server" id="txtFechaDesdeGenerar" maxlength="10" class="form-control validDate" style="width:100px; display:inline"></asp:TextBox>
                        Fecha hasta: <asp:TextBox runat="server"  type="text" id="txtFechaHastaGenerar" maxlength="10" class="form-control validDate" style="width:100px; display:inline"></asp:TextBox>
<%--                        <div class="row" style="margin-left:0px;">
                        <div  style="display:inline">Fecha Desde:  <asp:TextBox runat="server" MaxLength ="10" CssClass="form-control validDate" ID="dtDsd" Width="100"></asp:TextBox>
                         <div style="display:inline">Fecha Hasta: <asp:TextBox ID="dtHst" runat="server" MaxLength ="10" CssClass="form-control validDate" Width="100"></asp:TextBox></div>
                        </div>--%>
<%--                        <asp:Literal runat="server" ID="litErrorFechas" Text="Las fechas no pueden estar vacias" Visible="false"></asp:Literal>--%>
                        <%--<label id="divErrorGenerar" style="display: none" class="error">Por favor, ingrese las fechas</label>--%>
                        <%--<div><br>Cargar archivo:
                        <asp:FileUpload runat="server" ID="flpArch" TabIndex="1" />
                        <span class="help-block">Extensions: jpg/png/gif.</span>
                        </div>--%>
				    </div>
				    <div class="modal-footer">
                        <%--<asp:Button runat="server" CssClass="btn btn-success" OnClick="generarFacturas2" ID="generarFacturas" Text="Generar" />--%>
					    <button type="button" class="btn btn-succes" onclick="generarFacturas();">Generar</button>
                        <button type="button" class="btn btn-default" onclick="$('#modalGenerar').modal('hide');">Cerrar</button>
				    </div>
			    </div>
		    </div>
	    </div>
    </form>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

</asp:Content>

