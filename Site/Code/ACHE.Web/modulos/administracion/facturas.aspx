﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="facturas.aspx.cs" Inherits="modulos_administracion_facturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Administración</a></li>
            <li class="last">Facturas Recurrentes</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <%--<button runat="server" id="btnprueba" class="btn btn-success" type="button" onclick="prueba();" >prueba</button>--%>
			<h3 class="heading">Administración de facturas recurrentes</h3>
            <div class="alert alert-info alert-dismissable">La sincronización con Fidely Plus sólo tomará las facturas emitidas electrónicamente.</div>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>
		    <form>
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha desde</label>
                            <input id="txtFechaDesde" value="" maxlength="10" class="form-control validDate greaterThan"/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-2 col-md-2">
						    <label>Fecha hasta</label>
                            <input id="txtFechaHasta"  value="" maxlength="10" class="form-control validDate greaterThan "/>
						    <span class="help-block"></span>
					    </div>
                        <div class="col-sm-3 col-md-3">
						    <label>Comercio</label>
                            <select ID="ddlComercio" class="chzn_b form-control" data-placeholder="Seleccione un comercio">
                                <option value=""></option>
                            </select>
					    </div>
                        <div class="col-sm-3 col-md-3">
                            <label>Número Fc</label>
                            <input id="txtNumero"  value="" maxlength="50" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">        
                        <div class="col-sm-2">
                            <label>CUIT</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">
                            <label>CBU</label>
                            <input type="text" id="txtCBU" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-sm-2">
                            <label>Tipo Tarjeta</label>
                            <select ID="ddlTipoTarjeta" class="form-control">
                                <option Value="">Todas</option>
                                <option Value="REDIN">Redin</option>
                                <option Value="FIDELIZACION">Fidelizacion</option>
                                <option Value="GIFT">Giftcards</option>
                            </select>
                            
                        </div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filtrar();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="nuevoRecurrente();">Nueva</button>
                            <button class="btn btn-success" type="button" id="btnFE" onclick="confirmarCAE();">Obtener CAE</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <button class="btn btn-primary" type="button" id="btnContabilium" onclick="pasarAContabilium();">Pasar a contabilium</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Facturas" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br /><br />
        </div>
    </div>

    <div class="modal fade" id="modalObtenerCAE">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title">Obtener CAE</h3>
				</div>
				<div class="modal-body" id="modalObtenerCAEbody">
					<div class="alert alert-info">Al darle aceptar, ustedes informará a la AFIP todas las facturas generadas. ¿Desea continuar?.</div>
					<p id="modalPeriodo"></p>
                    <p id="modalTarjeta"></p>
                    
                    Informar facturas mayores a <input type="text" id="txtImporteCAE" maxlength="4" class="form-control number" value="0" style="width:100px; display:inline"/>

                    <label id="divErrorCAE" style="display: none" class="error">Por favor, ingrese el importe</label>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-succes" onclick="generarCAE();">Obtener</button>
                    <button type="button" class="btn btn-default" onclick="$('#modalObtenerCAE').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle"></h3>
				</div>
				<div class="modal-body">
                       
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">


						<thead id="headDetalle">
							
						</thead>
						<tbody id="bodyDetalle">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>

       <div class="modal fade" id="modalRetenciones">
           <form runat="server">
              <asp:HiddenField runat="server" ID="hdnIDFacturas" Value="0" />
           </form>
           
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titRetenciones"></h3>
				</div>
				<div class="modal-body">
					<!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headRetenciones">
                        <div class="alert alert-danger alert-dismissable" id="divErrorRet" style="display: none"></div>
                        <div class="alert alert-success alert-dismissable" id="divOkRet" style="display: none">Los datos se han actualizado correctamente.</div>
							
						</thead>
						<tbody id="bodyRetenciones">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
                    <button class="btn btn-success" type="button" onclick="guardarRetencion();filtrar();">Aceptar</button>
					<button class="btn btn-default" type="button"  onclick="$('#modalRetenciones').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/administracion/facturas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>