﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_notas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.NotasCreditoDebito.Include("Comercios")
                    .Where(x => x.IDComercio.HasValue)
                    .OrderByDescending(x => x.Fecha)
                    .Select(x => new FacturasViewModel()
                    {
                        ID = x.IDNota,
                        Tipo = x.Tipo,
                        IDEntidad = x.IDComercio.Value,
                        Numero = x.Numero.Trim(),
                        Nombre = x.Comercios.NombreFantasia,
                        CUIT = x.Comercios.NroDocumento,
                        FechaDesde = x.Fecha,
                        Total = x.ImporteTotal,
                        CAE = x.CAE,
                        Enviada = x.FechaEnvio.HasValue ? "Si" : "No"
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaDesde >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.FechaHasta <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.NotasCreditoDebito.Where(x => x.IDNota == id).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.FechaCAE.HasValue)
                            throw new Exception("No se puede eliminar una nc/nd con CAE");
                        else
                        {
                            dbContext.NotasCreditoDebito.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerDetalle(int id)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (id > 0)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.NotasCreditoDebito.Where(x => x.IDNota == id).ToList();
                    if (list.Any())
                    {
                        foreach (var detalle in list)
                        {
                            html += "<tr>";
                            html += "<td>" + detalle.Concepto + "</td>";
                            html += "<td>" + detalle.ImporteTotal.ToString("#0.00") + "</td>";
                            html += "</tr>";
                        }
                    }
                    else
                        html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";
                }
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerErrores()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.NotasCreditoDebito.Include("Comercios").Where(x => x.FechaError.HasValue && x.IDComercio.HasValue).ToList();
                if (list.Any())
                {
                    foreach (var fc in list)
                    {
                        html += "<tr>";
                        html += "<td>" + fc.Comercios.NombreFantasia + "</td>";
                        html += "<td>" + fc.Fecha.ToShortDateString() + "</td>";
                        if (fc.FechaError.HasValue)
                            html += "<td>" + fc.FechaError.Value.ToShortDateString() + "</td>";
                        else
                            html += "<td></td>";
                        html += "<td>" + fc.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }

    [WebMethod(true)]
    public static string ObtenerCAE()
    {
        var desc = "";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var facturas = dbContext.NotasCreditoDebito.Include("Comercios")
                    .Where(x => !x.FechaCAE.HasValue && x.IDComercio.HasValue)
                    .OrderBy(x => x.Fecha)
                    .ToList();

                int cantError = 0;
                int cantTotal = facturas.Count();
                int cantOK = 0;

                if (cantTotal > 0)
                {
                    //Pre validacion
                    string cuitCliente = string.Empty;
                    foreach (var fc in facturas)
                    {
                        var condicionIva = fc.Comercios.CondicionIva;
                        cuitCliente = fc.Comercios.NroDocumento.ReplaceAll("-", "").Trim();
                        if (cuitCliente == string.Empty)
                        {
                            throw new Exception("El cliente " + fc.Comercios.NombreFantasia + " (" + condicionIva + ") no tiene un CUIT cargado");
                        }
                    }

                    FEFacturaElectronica fe = new FEFacturaElectronica();
                    FEComprobante comprobante = null;
                    var pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CertificadoAFIP"]);
                    //var pathCertificado = ConfigurationManager.AppSettings["CertificadoAFIP"];
                    var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Template"]);
                    int puntoVenta = int.Parse(ConfigurationManager.AppSettings["FE.PtoVta"]);
                    long cuit = long.Parse(ConfigurationManager.AppSettings["FE.CUIT"]);
                    double total = 0;
                    double totalSinIva = 0;
                    double totalNeto = 0;

                    foreach (var fc in facturas)
                    {
                        #region Seteo de datos

                        total = double.Parse(fc.ImporteTotal.ToString());
                        totalSinIva = Math.Round(total, 2);
                        total += (total * 0.21);
                        total = Math.Round(total, 2);
                        cuitCliente = fc.Comercios.NroDocumento;

                        comprobante = new FEComprobante();
                        comprobante.DetalleIva = new List<FERegistroIVA>();
                        comprobante.Tributos = new List<FERegistroTributo>();
                        comprobante.Concepto = FEConcepto.Servicio;
                        comprobante.Fecha = DateTime.Now;
                        comprobante.Cuit = cuit;
                        comprobante.PtoVta = puntoVenta;
                        comprobante.FchServDesde = fc.Fecha;// DateTime.Now.AddDays(-1);
                        comprobante.FchServHasta = fc.Fecha;//DateTime.Now;
                        comprobante.FchVtoPago = DateTime.Now.AddDays(5);
                        comprobante.CodigoMoneda = "PES";
                        comprobante.CotizacionMoneda = 1;
                        comprobante.CondicionVenta = "Contado";

                        var condicionIva = fc.Comercios.CondicionIva;

                        if (condicionIva == "RI")
                            comprobante.ClienteCondiionIva = "Responsable Inscripto";
                        else
                            comprobante.ClienteCondiionIva = fc.Comercios.CondicionIva;

                        #region Seteo los datos en base a la condicion iva

                        if (condicionIva == "RI" || condicionIva == "EX" || condicionIva == "MONOTRIBUTISTA")
                        {
                            if (fc.Comercios.TipoDocumento == "CUIT")//TODO: MEJORAR ESTO.
                                comprobante.DocTipo = 80;// 96= DNI//80=CUIT
                            else
                                comprobante.DocTipo = 96;// 96= DNI//80=CUIT

                            comprobante.DocNro = long.Parse(cuitCliente);
                            comprobante.ClienteNombre = fc.Comercios.RazonSocial;
                            comprobante.ClienteDomicilio = fc.Comercios.DomiciliosFiscal.Domicilio + " " + fc.Comercios.DomiciliosFiscal.PisoDepto;
                            //comprobante.ClienteLocalidad = fc.Comercios.DomiciliosFiscal.Ciudad;
                            comprobante.ClienteLocalidad = fc.Comercios.DomiciliosFiscal.Ciudad.HasValue ? fc.Comercios.DomiciliosFiscal.Ciudades.Nombre : "";
                        }
                        /*else
                        {
                            comprobante.DocTipo = 99;//Sin identificar/venta global diaria
                            comprobante.DocNro = 0;
                            comprobante.ClienteNombre = "Cliente final";
                            comprobante.ClienteDomicilio = "No informado";
                            comprobante.ClienteLocalidad = "No informado";
                        }*/

                        if (condicionIva == "RI")
                        {
                            if (fc.Tipo == "NC")
                                comprobante.TipoComprobante = FETipoComprobante.NOTAS_CREDITO_A;
                            else
                                comprobante.TipoComprobante = FETipoComprobante.NOTAS_DEBITO_A;
                            totalNeto = totalSinIva;
                            //comprobante.ImpTotal = total;
                            comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = totalSinIva, TipoIva = FETipoIva.Iva21 });
                        }
                        //else if (condicionIva == "EX")
                        //{
                        //    if (fc.Tipo == "NC")
                        //        comprobante.TipoComprobante = FETipoComprobante.NOTAS_CREDITO_A;
                        //    else
                        //        comprobante.TipoComprobante = FETipoComprobante.NOTAS_DEBITO_A;

                        //    totalNeto = totalSinIva;
                        //    comprobante.ImpTotal = totalSinIva;
                        //    comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = totalSinIva, TipoIva = FETipoIva.Iva0 });
                        //}
                        else
                        {
                            if (fc.Tipo == "NC")
                                comprobante.TipoComprobante = FETipoComprobante.NOTAS_CREDITO_B;
                            else
                                comprobante.TipoComprobante = FETipoComprobante.NOTAS_DEBITO_B;
                            totalNeto = total;
                            //comprobante.ImpTotal = total;
                            comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = total, TipoIva = FETipoIva.Iva0 });
                        }

                        #endregion

                        comprobante.ImpTotConc = 0;
                        //comprobante.ImpNeto = totalNeto;
                        comprobante.ImpOpEx = 0;
                        comprobante.Observaciones = "";

                        double importeDetalle = double.Parse(fc.ImporteTotal.ToString());
                        if (condicionIva != "RI")// && condicionIva != "EX")
                            importeDetalle += (importeDetalle * 0.21);
                        importeDetalle = Math.Round(importeDetalle, 2);

                        comprobante.ItemsDetalle.Add(new FEItemDetalle()
                        {
                            Cantidad = 1,
                            Descripcion = fc.Concepto,
                            Precio = importeDetalle,
                            Codigo = fc.IDNota.ToString()
                        });

                        #endregion

                        fc.FechaProceso = DateTime.Now;
                        try
                        {
                            fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["wsaa"], pathCertificado, pathTemplateFc);
                            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');

                            var pathPdf = HttpContext.Current.Server.MapPath("/files/" + fc.Tipo.ToLower() + "/") + numeroComprobante + ".pdf";
                            fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc);

                            fc.FechaCAE = comprobante.FechaVencCAE;
                            fc.CAE = comprobante.CAE;
                            fc.Numero = numeroComprobante;
                            fc.Error = null;
                            fc.FechaError = null;

                            dbContext.SaveChanges();
                            cantOK++;
                        }
                        catch (Exception ex)
                        {
                            fc.Error = ex.Message;
                            fc.FechaError = DateTime.Now;
                            dbContext.SaveChanges();
                            cantError++;
                        }
                    }

                    if (cantError == 0)
                        desc = "Todas las nc/nd se han generado correctamente";
                    else
                        desc = "Se han generado " + cantOK + "/" + cantTotal + " correctamente. Haga click <a href='javascript:verFcErrores();'>aquí</a> para ver los errores";
                }
                else
                    throw new Exception("No hay facturas a generar");
            }
        }
        else
            desc = "Error. Vuelva a iniciar sesión";

        return desc;
    }
}