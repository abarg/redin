﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;

public partial class modulos_administracion_pagos_prov : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Pagos.Include("Proveedores")
                    .Where(x => x.IDProveedor.HasValue)
                    .OrderBy(x => x.Fecha)
                    .Select(x => new PagosViewModel()
                    {
                        ID = x.IDPago,
                        IDEntidad = x.IDProveedor.Value,
                        Nombre = x.Proveedores.NombreFantasia,
                        Fecha = x.Fecha,
                        Importe = x.Importe,
                        FormaDePago = x.FormaDePago,
                        NroComprobante = x.NroComprobante,
                        CUIT = x.Proveedores.NroDocumento,
                        Observaciones = x.Observaciones
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.Fecha <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Eliminar(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Pagos.Where(x => x.IDPago == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Pagos.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}