﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Model;


public partial class modulos_administracion_seccion_manual_ayudae : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["IDSeccion"]))
            {
                this.limpiarControles();
                this.hfIDSeccion.Value = Request.QueryString["IDSeccion"];

                if (!this.hfIDSeccion.Value.Equals("0"))
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDSeccion"]));
            }
        }
    }
    private void cargarDatos(int IDSeccion)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var seccion = dbContext.ManualAyuda.Where(x => x.IDManualAyuda == IDSeccion).FirstOrDefault();
                if (seccion != null)
                {
                    this.txtTitulo.Text = seccion.Titulo;
                    this.txtOrden.Text = seccion.Orden.ToString();
                    this.ddlPerfil.SelectedValue = seccion.Perfil;
                  
                }
                else Response.Redirect("~/error.aspx");
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void limpiarControles()
    {
        this.txtTitulo.Text = "";
        this.txtOrden.Text = "";
        this.ddlPerfil.SelectedValue = "";
      
    }
    [System.Web.Services.WebMethod(true)]
    public static void eliminar(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var articulo = dbContext.ArticulosManualAyuda.Where(x => x.IDArticulosManualAyuda == id).FirstOrDefault();
            dbContext.ArticulosManualAyuda.Remove(articulo);
            dbContext.SaveChanges();
        }
    }
    [System.Web.Services.WebMethod(true)]
    public static string generarTabla(int id)
    {
        var html = string.Empty;
        using (var dbContext = new ACHEEntities())
        {
            var list = dbContext.ArticulosManualAyuda.Where(x => x.IDManualAyuda==id).ToList();
            if (list.Any())
            {
                int n = 1;
                foreach (var item in list)
                {

                    html += "<tr class=\"selectVerificaciones\"  id=\"%\">";
                    html += "<td style='max-width:100px'><div align='center'><a href='articulo-manual-ayudae.aspx?IDArticulo=" + item.IDArticulosManualAyuda + "'><img src='../../img/grid/gridEdit.gif' style='cursor:pointer' title='Ver' class='editColumn'/><a/></div></td> ";
                    html += "<td style='max-width:100px'><div align='center'><a onclick='eliminar(" + item.IDArticulosManualAyuda + ");'><img src='../../img/grid/gridDelete.gif' style='cursor:pointer' title='Eliminar' class='gridDelete'/><a/></div></td> ";
                    html += "<td>" + item.Fecha.ToString("dd/MM/yyyy") + "</td> ";
                    html += "<td>" + item.Usuarios.Nombre + "</td> ";
                    html += "<td>" + item.Titulo + "</td> ";
                    html += "</tr>";
                    n++;
                }

            }
            else
            {
                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

        }

        return html;
    }
    [WebMethod(true)]
    public static void grabar(int idSeccion, string titulo, string orden, string perfil)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                ManualAyuda entity;


                if (idSeccion > 0)
                    entity = dbContext.ManualAyuda.FirstOrDefault(x => x.IDManualAyuda == idSeccion);
                else
                {
                    entity = new ManualAyuda();
                }
              
                entity.Titulo = titulo;
                entity.Orden = int.Parse(orden);
               
              
                entity.Perfil = perfil;
                if (idSeccion > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.ManualAyuda.Add(entity);
                    dbContext.SaveChanges();
                }
            }

        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}