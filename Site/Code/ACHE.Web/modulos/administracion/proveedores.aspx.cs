﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_administracion_proveedores : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Proveedores
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new ProveedoresViewModel()
                    {
                        ID = x.IDProveedor,
                        NombreFantasia = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        CondicionIva = x.CondicionIva,
                        Web = x.Web,
                        Email = x.Email
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Proveedores.Where(x => x.IDProveedor == id).FirstOrDefault();
                if (entity != null)
                {
                    dbContext.Proveedores.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string RazonSocial, string NroDocumento, string Nombre)
    {
        string fileName = "Proveedores";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.Proveedores.Include("Domicilios").OrderBy(x => x.NombreFantasia).AsEnumerable();
                    if (RazonSocial != "")
                        info = info.Where(x => x.RazonSocial.ToLower().Contains(RazonSocial.ToLower()));
                    if (NroDocumento != "")
                        info = info.Where(x => x.NroDocumento != null && x.NroDocumento.ToLower().Contains(NroDocumento.ToLower()));
                    if (Nombre != "")
                        info = info.Where(x => x.NombreFantasia.ToString().ToLower().Contains(Nombre.ToLower()));
                    

                    dt = info.ToList().Select(x => new
                    {
                        NombreFantasia = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Telefono = x.Telefono,
                        Celular = x.Celular,
                        CondicionIva = x.CondicionIva,
                        Web = x.Web,
                        Email = x.Email,
                        FechaAlta = x.FechaAlta.ToShortDateString(),
                        Observaciones = x.Observaciones,
                        Pais = x.Domicilios != null ? x.Domicilios.Pais : "",
                        Provincia = x.Domicilios != null ? x.Domicilios.Provincias.Nombre : "",
                        Ciudad = x.Domicilios != null && x.Domicilios.Ciudad.HasValue ? x.Domicilios.Ciudades.Nombre : "",
                        Domicilio = x.Domicilios != null ? x.Domicilios.Domicilio : "",
                        PisoDepto = x.Domicilios != null ? x.Domicilios.PisoDepto : "",
                        CodigoPostal = x.Domicilios != null ? x.Domicilios.CodigoPostal : "",
                        TelefonoDom = x.Domicilios != null ? x.Domicilios.Telefono : "",
                        Fax = x.Domicilios != null ? x.Domicilios.Fax : ""
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}