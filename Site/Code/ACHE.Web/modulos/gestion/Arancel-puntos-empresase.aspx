﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Arancel-puntos-empresase.aspx.cs" Inherits="modulos_gestion_Arancel_puntos_empresase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/arancel-puntos-empresase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Actualización masiva de arancel, puntos y descuentos por empresas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <br />
                    <asp:Panel runat="server" class="row" ID="pnlpaso1">
                        <%--<div class="form-group">
                            <div class="col-sm-3">
                                <label>Marca</label>
                                <asp:DropDownList runat="server" class="form-control" onchange="guardarMarca();" ID="ddlMarcas" />
                                
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Seleccione una opción</label>
                            <div class="col-lg-3">
                                <asp:DropDownList runat="server" class="form-control chosen" onchange="cargarComercios();" ID="ddlEmpresas"  ClientIDMode="Static"  />
                                <asp:HiddenField runat="server" ID="hdnIDEmpresa" ClientIDMode="Static" Value="0" />
                                <br />
                                <asp:Button runat="server" ID="btnBuscar" CssClass="btn btn-success" OnClientClick="return validar();" OnClick="btnCargarComercios_Click" Text="Obtener Comercios" />


                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlpaso2">
                        <div class="form-group" id="divComercios" runat="server">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Comercios</label>
                            <div class="col-lg-9">
                                <asp:DropDownList runat="server" ID="searchable" multiple="multiple" ClientIDMode="Static" DataTextField="Nombre" DataValueField="ID" />
                                <br />
                                <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span>Arancel %</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel2" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel3" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel4" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel5" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel6" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtArancel7" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span>Puntos %</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos2" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos3" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos4" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos5" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos6" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntos7" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Mult Puntos</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos1" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntos7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Descuento</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento2" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento3" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento4" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento5" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento6" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuento7" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Desc. Vip</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip2" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip3" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip4" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip5" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip6" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtDescuentoVip7" CssClass="form-control" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Puntos vip%</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip1" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip2" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip3" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip4" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip5" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip6" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtPuntosVip7" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Mult Puntos Vip</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Lunes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip2" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Martes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip3" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Miércoles</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip4" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Jueves</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip5" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Viernes</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip6" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Sábado</span>
                            </div>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip7" CssClass="form-control  numeric" MaxLength="2"></asp:TextBox>
                                <span class="help-block">Domingo</span>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-lg-2 control-label">Costo Fijo</label>
                            <div class="col-lg-1">
                                <asp:TextBox runat="server" ID="txtCostoFijo" CssClass="form-control numeric"  MaxLength="6"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Cobrar uso de Red</label>
                            <div class="checkbox-inline" style="margin-left: 15px">
                                <asp:CheckBox runat="server" ID="chkCobrarUsoRed" Text="Si" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

