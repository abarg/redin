﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="mapa.aspx.cs" Inherits="modulos_gestion_mapa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        html, body, #maincontainer, #contentwrapper, .main_content {
            height: 100%;
        }

        #contentwrapper, .main_content {
            min-height: 100%;
        }

        .main_content {
            padding-left: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
        }

        #map-optionbar-r {
            position: absolute;
            top: 125px;
            right: 35px;
            width: 230px;
            background-color: #fff;
            padding: 5px;
            font-size: 14px;
            line-height: 16px;
            border: 1px solid #232020;
            line-height: 16px;
        }

        .chk {
            vertical-align: top;
        }

        #mapSearch {
            right: 50px;
            height: 0px;
            position: relative;
            /* right: 0; */
            text-align: center;
            top: 20px;
            -webkit-transition: left 250ms cubic-bezier(0,0,0.2,1);
            transition: left 250ms cubic-bezier(0,0,0.2,1);
            z-index: 501;
            float: right;
        }

        .map-search-view {
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background-color: rgba(255,255,255,0.9);
            border-radius: 3px;
            box-shadow: 0 2px 5px 1px rgba(25,24,28,0.3);
            margin: 0 auto;
            padding: 0;
            -webkit-transition: width 250ms cubic-bezier(0,0,0.2,1);
            transition: width 250ms cubic-bezier(0,0,0.2,1);
            width: 300px;
        }

            .map-search-view .search-field {
                border: 1px solid transparent;
                padding: 3px;
                text-align: left;
            }
            /*.map-search-view .search-text {
                -webkit-transition: width 500ms;
                transition: width 500ms;
                background-color: transparent;
                border: 0;
                box-shadow: none;
                color: #333;
                font-size: 14px;
                margin: 0 0 0 32px;
                width: 330px;
            }*/
        .contentImg{
            float: left;
            width: 150px;
            margin-right: 10px;
            text-align: center;
        }
        .contentImg a{
            color: #369;
            font-size: 12px;
        }
        .contentTxt{
            float: left;
            width: 300px;
        }
        .clear{ clear: both; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="mapSearch">
        <div id="search" class="map-search-view">
            <div class="search-field">
                <form runat="server" id="frmBuscar">
                    Tipo de búsqueda: <input type="radio" id="rdbComercios" name="Tipo" checked="checked" onclick="toggleTipo();" />Comercios&nbsp; <input type="radio" id="rdbSocios" name="Tipo" onclick="toggleTipo();" />Socios
                    <br /><br />
                    Franquicia: <asp:DropDownList runat="server" ID="ddlFranquicias" class="form-control"></asp:DropDownList>&nbsp;
                    Marca<asp:DropDownList runat="server" ID="ddlMarcas" class="form-control"></asp:DropDownList>&nbsp;
                    <div id="divComercios">
<%--                          Rubro<asp:DropDownList runat="server" ID="ddlRubro"  onchange="cargarSubRubros();" ClientIDMode="Static" class="form-control"></asp:DropDownList>&nbsp;
                          Sub Rubro<asp:DropDownList runat="server" ID="ddlSubRubro"  class="form-control"></asp:DropDownList>&nbsp;--%>
                   </div>
                    Rubro <asp:DropDownList runat="server" ID="ddlRubro" class="form-control chosen" data-placeholder="Seleccione un Rubro" ClientIDMode="Static" onchange="cargarSubRubros();">
                            </asp:DropDownList>
                    SubRubro<asp:DropDownList runat="server" ID="ddlSubRubro" class="form-control chosen"> 
                            </asp:DropDownList>
                    <br /><br />
                    <div id="divSocios" style="display:none">
                        Sexo: <br />
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkMasculino" onclick="filtroClick(this, 'masculino');" value="masculino" />
                        <img src="/img/markers/masculino.png" style="width: 24px" />
                        Socios masculinos
                        <br>
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkFemenino" onclick="filtroClick(this, 'femenino');" value="femenino" />
                        <img src="/img/markers/femenino.png" style="width: 24px" />
                        Socios femeninos
        
                        <br />
                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkIndefinido" onclick="filtroClick(this, 'indefinido');" value="indefinido" />
                        <img src="/img/markers/indefinido.png" style="width: 24px" />
                        Socios indefinidos

                        <br /><br />
                        Edad: 
                        <asp:DropDownList runat="server" ID="ddlEdad" class="form-control" Width="100" style="display:inline">
                            <asp:ListItem Text="0-18" Value="0-18"></asp:ListItem>
                            <asp:ListItem Text="19-25" Value="19-25"></asp:ListItem>
                            <asp:ListItem Text="26-30" Value="26-30"></asp:ListItem>
                            <asp:ListItem Text="31-40" Value="31-40"></asp:ListItem>
                            <asp:ListItem Text="41-50" Value="41-50"></asp:ListItem>
                            <asp:ListItem Text="51-60" Value="51-60"></asp:ListItem>
                            <asp:ListItem Text="61+" Value="61-200"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <br />
                    <span id="litResultados"></span>
                    <br /><br />
                    <a href="javascript:buscar();" class="btn btn-primary" id="btnBuscar">Buscar</a>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                </form>
            </div>
            <div class="autocomplete dropdown-menu" style="display: none;"></div>
        </div>
    </div>


    <div id="map-canvas" style="height: 100%; min-height: 100%; width: 100%; margin-top: -20px"></div>

    <div id="map-optionbar-r" style="display:none">
        Mostrar/Ocultar<br>
        <br>

        
        
        <br />
        <br />
        <b>Comercios</b>
        <br />

        
        
        <!--
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox1" onclick="filtroClick(this, 'compras');">
        <img src="/img/markers/compras.png" style="width: 24px" />
        Rubro compras
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox2" onclick="filtroClick(this, 'deporte');">
        <img src="/img/markers/deporte.png" style="width: 24px" />
        Rubro deportes
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox4" onclick="filtroClick(this, 'entretenimiento');">
        <img src="/img/markers/entretenimiento.png" style="width: 24px" />
        Rubro entretenimiento
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox5" onclick="filtroClick(this, 'gastronomia');">
        <img src="/img/markers/gastronomia.png" style="width: 24px" />
        Rubro gastronomia
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox6" onclick="filtroClick(this, 'hogaryconstruccion');">
        <img src="/img/markers/hogaryconstruccion.png" style="width: 24px" />
        Rubro hogar y construcción
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox7" onclick="filtroClick(this, 'moda');">
        <img src="/img/markers/moda.png" style="width: 24px" />
        Rubro moda
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox8" onclick="filtroClick(this, 'saludybelleza');">
        <img src="/img/markers/saludybelleza.png" style="width: 24px" />
        Rubro salud y belleza
        <br />
        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox9" onclick="filtroClick(this, 'viajesyturismo');">
        <img src="/img/markers/viajesyturismo.png" style="width: 24px" />
        Rubro viajes y turismo-->
    </div>
    <%--<nav>

        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="<%= ResolveUrl("~/marcas/mapa.aspx") %>">Mapa</a></li>
                <li>Mapa de socios y comercios</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Mapa de socios y comercios</h3>

            <br />
            <div class="col-sm-2 col-md-2">
                <input type="checkbox" id="chckFem" checked="checked" />&nbsp;Socios femeninos
                <br />
                <input type="checkbox" id="chkMasc" checked="checked" />&nbsp;Socios masculinos
                <br />
                <input type="checkbox" id="chkComercios" checked="checked" />&nbsp;Comercios
            </div>
            <div class="col-sm-10 col-md-10">
                <div id="map-canvas" style="height: 500px; width: 100%"></div>
            </div>
        </div>
    </div>--%>

   

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
     <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIrIBfQShg1eG3yx4QHUSBK449lLVRT_g&signed_in=true&callback=initialize">
        </script>

        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/mapa.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>