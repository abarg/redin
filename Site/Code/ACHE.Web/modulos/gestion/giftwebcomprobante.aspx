﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="giftwebcomprobante.aspx.cs" Inherits="modulos_gestion_giftwebcomprobante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
     <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnVolver").focus();
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/modulos/gestion/giftweb.aspx") %>">Gift Web</a></li>
            <li class="last">Comprobante</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <%--<h1 class="invoice_heading"></h1>

            <br />--%>
            <br />
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4 col-md-4">
                    <div class="w-box" id="w_sort01">
                        <div class="w-box-header">
                            Comprobante #<%= Request.QueryString["Id"]  %>
                        </div>
                        <div class="w-box-content cnt_a">
                            <div class="text-center">
                                <h4>
                                    <asp:Literal runat="server" ID="litTitulo"></asp:Literal></h4>
                                <h4>
                                    <asp:Literal runat="server" ID="litOperacion"></asp:Literal></h4>
                            </div>
                            <br />

                            <div class="text-center">
                                <asp:Literal runat="server" ID="litComercio"></asp:Literal><br />
                                <asp:Literal runat="server" ID="litDomicilio"></asp:Literal>
                                <br />
                                <asp:Literal runat="server" ID="litCuit"></asp:Literal>
                            </div>

                            <br />
                            <br />
                            Nro Comprobante:
                            <asp:Literal runat="server" ID="litNroComprobante"></asp:Literal><br />
                            Comprobante:
                            <asp:Literal runat="server" ID="litTipoComprobante"></asp:Literal><br />
                            Punto Venta:
                            <asp:Literal runat="server" ID="litPtoVenta"></asp:Literal>

                            <br />
                            <br />
                            <h3>Importe total: $ <asp:Literal runat="server" ID="litImporteTotal"></asp:Literal></h3>
                            <br />
                            <br />

                            <div>
                                <div id="divDescuento1" runat="server" visible="false">
                                    Costo uso RED IN: $ <asp:Literal runat="server" ID="litUsoRed"></asp:Literal>
                                </div>
                                Saldo acumulado:
                                <asp:Literal runat="server" ID="litSaldo"></asp:Literal><br />
                            </div>
                        </div>
                        <%--<div class="w-box-footer">
                            <div class="text-center">
                                
                            </div>
                        </div>--%>
                        <input type="hidden" id="hdnID" value="<%= Request.QueryString["Id"]  %>" />
                    </div>
                </div>
                <div class="col-sm-4 col-md-4" style="margin-top: 175px;">
                    <button class="btn btn" type="button" id="btnVolver" onclick="window.location.href='giftweb.aspx';">Volver</button>
                    <%--<button class="btn btn" type="button" id="btnImprimir">Imprimir</button>
                    <button class="btn btn" type="button" id="btnAnular" onclick="anularOperacion();">Anular</button>--%>
                </div>
                <div class="col-sm-4"></div>
            </div>
        </div>
    </div>
</asp:Content>

