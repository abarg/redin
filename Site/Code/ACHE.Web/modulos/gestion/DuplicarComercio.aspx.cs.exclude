﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;
using ACHE.Model.ViewModels;

public partial class modulos_gestion_DuplicarComercio : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cargarCombos();
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var comercios = dbContext.Comercios.OrderBy(x => x.NombreFantasia).Select(x => new { IDComercio=x.IDComercio, NombreFantasia =x.NombreFantasia + " - " + x.Domicilios.Domicilio }).ToList();

            var todas = new { IDComercio = 0, NombreFantasia = "Elija un comercio" };
           // todas.NombreFantasia = "Elija un comercio";
           // todas.IDComercio = 0;
           comercios.Insert(0, todas);

            ddlComercios.DataSource = comercios;
            ddlComercios.DataValueField = "IDComercio";
            ddlComercios.DataTextField = "NombreFantasia";
            ddlComercios.DataBind();
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static ComerciosFrontViewModel buscarDatosComercio(string idComer) {
        using (var dbContext = new ACHEEntities()) {
            int id = int.Parse(idComer);
            ComerciosFrontViewModel comer = dbContext.Comercios.Include("Franquicias").Include("Marcas").Include("Rubros").Where(x => x.IDComercio == id)
                .Select(x => new ComerciosFrontViewModel {
                IDComercio = x.IDComercio,
                SDS = x.SDS,
                NombreFantasia = x.NombreFantasia,
                RazonSocial = x.RazonSocial,
                TipoDocumento = x.TipoDocumento,
                NroDocumento = x.NroDocumento,
                Telefono = x.Telefono,
                Franquicia = x.IDFranquicia != null? x.Franquicias.NombreFantasia: "",
                Marca = x.IDMarca != null? x.Marca.Nombre : "",
                Rubro = x.Rubros.Nombre != null? x.Rubros.Nombre : "",
                SubRubro = x.IDSubRubro != null? dbContext.Rubros.Where(y=>x.IDSubRubro == y.IDRubro).FirstOrDefault().Nombre:"",
                NombreEstablecimiento = x.NombreEst
                //NroEstablecimiento=x.NroNumEst
                }).FirstOrDefault();

            if (comer != null) 
                return comer;            
            else
                return null;        
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void duplicarComercio(string idComer, string nombreFantasia, string sds, string pos) {
        using (var dbContext = new ACHEEntities()) {
            int id = int.Parse(idComer);
            Comercios com = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
            if (com != null)
            {
                if (nombreFantasia == null || nombreFantasia == "")
                    throw new Exception("El campo Nombre Fantasia no puede estar vacio");
                else
                    com.NombreFantasia = nombreFantasia;
                if (sds != null && sds != ""){
                    if (!dbContext.Comercios.Any(s => s.SDS == sds))
                        com.SDS = sds;
                    else throw new Exception("Ya existe un Comercio con el Número de SDS ingresado");
                }
                else throw new Exception("El campo SDS no puede estar vacio");

                //if (pos != null && pos != ""){
                //    if( !dbContext.Comercios.Any(s => s.POSTerminal == pos))
                //        com.POSTerminal = pos;
                //    else throw new Exception("Ya existe un Comercio con el Número de terminal ingresado");
                //}
                //else throw new Exception("El campo Número de terminal no puede estar vacio");
                int idcomercioviejo = com.IDComercio;
                com = duplicarDatosForaneos(com);
                dbContext.Comercios.Add(com);
                dbContext.SaveChanges();
                duplicarPuntoDeVenta(idcomercioviejo, com.IDComercio);
                duplicarAlertas(idcomercioviejo, com.IDComercio);
            }
            else throw new Exception("El comercio no existe");
        }
    }

    public static Comercios duplicarDatosForaneos(Comercios comer) {
      
        comer=duplicarDomicilio(comer);
        comer=duplicarDomicilioFiscal(comer);
        return comer;
    }
   
    public static void duplicarPuntoDeVenta(int idViejo, int idNuevo)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usuComer = dbContext.PuntosDeVenta.Where(x => x.IDComercio == idViejo).ToList();
            foreach (var usu in usuComer)
            {
                usu.IDComercio = idNuevo;
                dbContext.PuntosDeVenta.Add(usu);
            }
            dbContext.SaveChanges();
        }
    }
    public static void duplicarAlertas(int idViejo, int idNuevo)
    {
        using (var dbContext = new ACHEEntities())
        {
            var usuComer = dbContext.Alertas.Where(x => x.IDComercio == idViejo).ToList();
            foreach (var usu in usuComer)
            {
                usu.IDComercio = idNuevo;
                dbContext.Alertas.Add(usu);
            }
            dbContext.SaveChanges();
        }
    }
    public static Comercios duplicarDomicilio(Comercios comer) {
        using (var dbContext = new ACHEEntities()) {
            var domicilio = dbContext.Domicilios.Where(x => x.IDDomicilio == comer.IDDomicilio).FirstOrDefault();
            if (domicilio != null) {
                dbContext.Domicilios.Add(domicilio);
                dbContext.SaveChanges();
                comer.IDDomicilio = domicilio.IDDomicilio;
            }
            return comer;
        }
    }
    public static Comercios duplicarDomicilioFiscal(Comercios comer) {
        using (var dbContext = new ACHEEntities()) {
            var domicilioFiscal = dbContext.Domicilios.Where(x => x.IDDomicilio == comer.IDDomicilioFiscal).FirstOrDefault();
            if (domicilioFiscal != null) {
                dbContext.Domicilios.Add(domicilioFiscal);
                dbContext.SaveChanges();
                comer.IDDomicilioFiscal = domicilioFiscal.IDDomicilio;
            }
            return comer;
        }
    }
}
