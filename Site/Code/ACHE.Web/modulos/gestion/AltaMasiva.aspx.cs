﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_gestion_AltaMasiva : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            this.cargarCombos();
    }

    private void cargarCombos() {
        try {
            //bMarca bMarca = new bMarca();
            //List<Marcas> listMarcas = bMarca.getMarcas();
            //this.ddlMarcas.DataSource = listMarcas;
            //this.ddlMarcas.DataValueField = "IDMarca";
            //this.ddlMarcas.DataTextField = "Nombre";
            //this.ddlMarcas.DataBind();

            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));

              using (var dbContext = new ACHEEntities()) {
                  this.ddlBIN.DataSource = dbContext.BIN.ToList();
                  this.ddlBIN.DataValueField = "Numero";
                  this.ddlBIN.DataTextField = "Nombre";
                  this.ddlBIN.DataBind();

            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod]
    public static List<ComboViewModel> GetMarcas(int idFranquicia) {
        List<ComboViewModel> result = new List<ComboViewModel>();
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                bMarca bMarca = new bMarca();
                List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(idFranquicia);
                if (listMarcas != null && listMarcas.Count() > 0)
                    result = listMarcas.Select(x => new ComboViewModel() { ID = x.IDMarca.ToString(), Nombre = x.Nombre }).ToList();
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return result;
    }

    
    [System.Web.Services.WebMethod]
    public static string GetDatosMarca(int idMarca)
    {
        string result =  "";
        string affinity="";
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities())
                {
                    var tarjeta = dbContext.Tarjetas.Where(x => x.IDMarca == idMarca).OrderByDescending(x =>x.IDTarjeta).FirstOrDefault();
                    if (tarjeta != null){
                        var numero=int.Parse( tarjeta.Numero.Substring( tarjeta.Numero.Length - 6,5));
                        result = (numero+1).ToString();
                        affinity ="7"+ tarjeta.Marcas.Affinity;
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return result+"#"+affinity;
    }
    

    [System.Web.Services.WebMethod]
    public static void generar(string PanDesde, string PanHasta, string Desde, string Hasta, int Marca, int Franquicia, string tipo) {
        try {
            bTarjeta bTarjeta = new bTarjeta();
            List<Tarjetas> listTarjetas = new List<Tarjetas>();

            int i = Convert.ToInt32(Desde);
            int x = Convert.ToInt32(Hasta);
            while (i <= x) {
                Tarjetas oTarjeta = new Tarjetas();
                string aux = "";
                if (i < 10) {
                    aux = "0000" + i.ToString();
                }
                else {
                    if (i < 100) {
                        aux = "000" + i.ToString();
                    }
                    else {
                        if (i < 1000) {
                            aux = "00" + i.ToString();
                        }
                        else {
                            if (i < 10000)
                                aux = "0" + i.ToString();
                            else
                                aux = i.ToString();
                        }
                    }
                }

                bTarjeta.CalcularPAN(PanDesde + PanHasta + aux);
                bTarjeta.Numero = aux;
                bTarjeta.PanAux = bTarjeta.ObtenerPan();
                bTarjeta.Track1 = ("%B" + bTarjeta.PanAux + "^                    ^491210100000?");
                bTarjeta.Track2 = (";" + bTarjeta.PanAux + "=491210100000?");

                DateTime fechaActual = DateTime.Now;
                oTarjeta.FechaAlta = fechaActual;
                oTarjeta.FechaEmision = fechaActual;
                oTarjeta.FechaVencimiento = Convert.ToDateTime("31/12/2049");
                oTarjeta.Estado = "A";
                oTarjeta.Numero = bTarjeta.PanAux;//PanDesde + PanHasta + aux;// 
                oTarjeta.IDMarca = Marca;
                oTarjeta.TipoTarjeta = tipo;
                if (Franquicia > 0)
                    oTarjeta.IDFranquicia = Franquicia;
                oTarjeta.IDSocio = null;
                oTarjeta.Credito = 0;
                oTarjeta.Giftcard = 0;
                oTarjeta.PuntosTotales = 0;

                listTarjetas.Add(oTarjeta);
                i++;
            }

            //Tarjetas
            bTarjeta.add(listTarjetas);
            }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}