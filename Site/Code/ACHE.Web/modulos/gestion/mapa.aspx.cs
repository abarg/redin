﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;

public partial class modulos_gestion_mapa : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubRubros(int IDRubroPadre)
    {

        using (var dbContext = new ACHEEntities())
        {
            List<ComboViewModel> result = new List<ComboViewModel>();

            var subRubros = dbContext.Rubros.Where(x => x.IDRubroPadre == IDRubroPadre).OrderBy(x => x.IDRubro).Select(x => new ComboViewModel()
            {
                ID = x.IDRubro.ToString(),
                Nombre = x.Nombre
            }).ToList();

            if (subRubros != null && subRubros.Count() > 0)
                result = subRubros;

            result.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });

            return result;
        }
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            bRubros bRubros = new bRubros();
            this.ddlRubro.DataSource = bRubros.getRubros();
            this.ddlRubro.DataValueField = "IDRubro";
            this.ddlRubro.DataTextField = "Nombre";
            this.ddlRubro.DataBind();
            this.ddlRubro.Items.Insert(0, new ListItem("", ""));

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));



        }
    }

    [WebMethod(true)]
    public static List<MarkersViewModel> GetMarkers(string tipo, int franquicia, int marca, int rubro, int subRubro, string edad)
    {
        var list = new List<MarkersViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (tipo == "C")
                    {
                        var preCom = dbContext.Comercios.Include("Domicilios").Include("Rubros")
                            .Where(x => x.Domicilios != null
                                && x.Domicilios.Latitud != string.Empty && x.Domicilios.Longitud != string.Empty
                                && x.Domicilios.Latitud != null
                                && x.Domicilios.Longitud != null).AsQueryable();
                        if (franquicia > 0)
                            preCom = preCom.Where(x => x.IDFranquicia == franquicia);
                        if (marca > 0)
                            preCom = preCom.Where(x => x.IDMarca == marca);
                        if (rubro > 0)
                            preCom = preCom.Where(x => x.Rubro == rubro);
                        if (subRubro > 0)
                            preCom = preCom.Where(x => x.IDSubRubro == subRubro);

                        var comercios = preCom.ToList();
                        if (comercios.Any())
                        {
                            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                            string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                            string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
                            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                            var trMensual = string.Empty;
                            var trAnual = string.Empty;
                            var promMensual = string.Empty;
                            var promAnual = string.Empty;

                            foreach (var com in comercios)
                            {
                                var marker = new MarkersViewModel();
                                marker.Nombre = com.NombreFantasia;
                                marker.Direccion = com.Domicilios.Domicilio;
                                marker.Lat = com.Domicilios.Latitud;
                                marker.Lng = com.Domicilios.Longitud;
                                marker.Categoria = com.Rubro.HasValue ? com.Rubro.Value.ToString() : "0";
                                //marker.NombreRubro = marker.IDRubro == 0 ? "" : com.Rubros.Nombre.RemoverAcentos().Replace(" ", "");
                                marker.Icono = com.Rubro.HasValue ? com.Rubros.Icono : "";

                                //Cant TR
                                var totalMensual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + com.IDComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (totalMensual.Any())
                                    trMensual = totalMensual[0].data.ToString();
                                else
                                    trMensual = "0";
                                /*
                                var totalAnual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + com.IDComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (totalAnual.Any())
                                    trAnual = totalAnual[0].data.ToString();
                                else
                                    trAnual = "0";*/

                                //Promedio ticket
                                var promedioMensual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + com.IDComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (promedioMensual.Any())
                                    promMensual = Math.Abs(promedioMensual[0].data).ToString("N2");
                                else
                                    promMensual = "0";
                                /*
                                var promedioAnual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + com.IDComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (promedioAnual.Any())
                                    promAnual = Math.Abs(promedioAnual[0].data).ToString("N2");
                                else
                                    promAnual = "0";
                                */
                                marker.Contenido = "<p><b>Cant TR mes actual:</b> " + trMensual + "<br>";
                                //marker.Contenido += "<b>Cant TR anual:</b> " + trAnual + "<br>";
                                marker.Contenido += "<b>Prom ticket mes actual:</b> $ " + promMensual + "<br>";
                                //marker.Contenido += "<b>Prom ticket anual:</b> $ " + promAnual + "</p>";
                                marker.Ficha = "/modulos/gestion/comerciosedicion.aspx?IDComercio=" + com.IDComercio;

                                if (!string.IsNullOrEmpty(com.Logo))
                                    marker.Foto = "/files/logos/" + com.Logo;
                                else
                                    marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                                list.Add(marker);
                            }
                        }
                    }
                    else
                    {
                        int edadDesde = int.Parse(edad.Split("-")[0]);
                        int edadHasta = int.Parse(edad.Split("-")[1]);

                        var preTar = dbContext.Tarjetas.Include("Socios")
                            .Where(x => x.PuntosTotales > 1
                                && x.Socios.Edad >= edadDesde && x.Socios.Edad <= edadHasta
                                && x.Socios.Domicilios != null && x.Socios.Domicilios.Latitud != string.Empty
                                && x.Socios.Domicilios.Longitud != string.Empty
                                && x.Socios.Domicilios.Latitud != null
                                && x.Socios.Domicilios.Longitud != null
                                ).AsQueryable();
                        if (franquicia > 0)
                            preTar = preTar.Where(x => x.IDFranquicia == franquicia);
                        if (marca > 0)
                            preTar = preTar.Where(x => x.IDMarca == marca);
                       

                        var tarjetas = preTar.ToList();
                        if (tarjetas.Any())
                        {
                            foreach (var soc in tarjetas)
                            {
                                var marker = new MarkersViewModel();
                                marker.Nombre = soc.Socios.Apellido + ", " + soc.Socios.Nombre;
                                marker.Direccion = soc.Socios.Domicilios.Domicilio;
                                marker.Lat = soc.Socios.Domicilios.Latitud;
                                marker.Lng = soc.Socios.Domicilios.Longitud;
                                marker.Contenido = "<p><b>DNI:</b> " + soc.Socios.NroDocumento + "<br><b>Email:</b> " + soc.Socios.Email + "<br><b>Credito:</b> $ " + soc.Credito.ToString("N2") + "<br><b>Giftcard:</b> $ " + soc.Giftcard.ToString("N2") + "<br><b>POS:</b> " + Math.Round(soc.Credito + soc.Giftcard) + "</p>";
                                marker.Ficha = "/common/Sociose.aspx?IDSocio=" + soc.IDSocio;
                                if (soc.Socios.AuthTipo == "FB")
                                    marker.Foto = string.Format("https://graph.facebook.com/{0}/picture?type=normal", soc.Socios.AuthID);
                                else if (!string.IsNullOrEmpty(soc.Socios.Foto))
                                    marker.Foto = "/files/socios/" + soc.Socios.Foto;
                                else
                                    marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                                if (soc.Socios.Sexo == "F")
                                {
                                    marker.Icono = "femenino.png";
                                    marker.Categoria = "femenino";
                                }
                                else if (soc.Socios.Sexo == "M")
                                {
                                    marker.Icono = "masculino.png";
                                    marker.Categoria = "masculino";
                                }
                                else
                                {
                                    marker.Icono = "indefinido.png";
                                    marker.Categoria = "indefinido";
                                }
                                list.Add(marker);
                            }
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return list;
    }
}