﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_gestion_ZonasEdicion : PaginaBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            cargarProvincias();
            if (!String.IsNullOrEmpty(Request.QueryString["IDZona"])) {
                try {
                    int idZona = int.Parse(Request.QueryString["IDZona"]);
                    if (idZona > 0) {
                        this.hdnIDZona.Value = idZona.ToString();
                        cargarDatosZona(idZona);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("Zonas.aspx");
                }
            }
        }
    }

    private void cargarProvincias() {
        using (var dbContext = new ACHEEntities()) {
            var provincias = dbContext.Provincias.OrderBy(x => x.Nombre).ToList();
            if (provincias != null) {
                cmbProvincia.DataSource = provincias;
                cmbProvincia.DataTextField = "Nombre";
                cmbProvincia.DataValueField = "IDProvincia";
                cmbProvincia.DataBind();
                cmbProvincia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarCiudades(int idProvincia)
    {
        using (var dbContext = new ACHEEntities())
        {
            var ciudades = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).OrderBy(x => x.Nombre).ToList();
            if (ciudades != null)
            {
                cmbCiudad.DataSource = ciudades;
                cmbCiudad.DataTextField = "Nombre";
                cmbCiudad.DataValueField = "IDCiudad";
                cmbCiudad.DataBind();
                cmbCiudad.Items.Insert(0, new ListItem("", ""));
            }
        }
    }



    private void cargarDatosZona(int idZona) {
        using (var dbContext = new ACHEEntities()) {
            var zona = dbContext.Zonas.Where(x => x.IDZona == idZona).FirstOrDefault();
            if (zona != null) {
                this.txtNombre.Text = zona.Nombre;
                cargarCiudades(zona.IDProvincia);
                this.cmbCiudad.SelectedValue = zona.IDCiudad.ToString();
                this.cmbProvincia.SelectedValue = zona.IDProvincia.ToString();
            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDZona, string Nombre, int IDCiudad,int IDProvincia) {
        try {
            //Zona
            Zonas entity;
            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Zonas.Where(x => x.IDCiudad == IDCiudad && x.Nombre == Nombre && x.IDZona != IDZona).FirstOrDefault();
                    if ( aux != null)
                        throw new Exception("Ya existe una zona perteneciente a la provincia " + aux.Provincias.Nombre + " llamada " + aux.Nombre);

                if (IDZona > 0)
                    entity = dbContext.Zonas.FirstOrDefault(s => s.IDZona == IDZona);
                else
                    entity = new Zonas();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";
                entity.IDProvincia = IDProvincia;
                entity.IDCiudad = IDCiudad;

                if (IDZona > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.Zonas.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDZona.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}

