﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Business;

public partial class modulos_gestion_ReasignacionTarjetas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
        }
    }

    private void cargarCombos()
    {

        bFranquicia bFranquicia = new bFranquicia();
        List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
        this.ddlFranquicias.DataSource = listFranquicias;
        this.ddlFranquicias.DataValueField = "IDFranquicia";
        this.ddlFranquicias.DataTextField = "NombreFantasia";
        this.ddlFranquicias.DataBind();

        this.ddlFranquicias.Items.Insert(0, new ListItem("", "0"));

    }

    [System.Web.Services.WebMethod]
    public static void ReasignarTarjetas(string numeroDesde, string numeroHasta, int idFranquicia, int idMarca, string tipo)
    {
        using (var dbContext = new ACHEEntities())
        {
            try
            {
                string sql = "exec ReasignarTarjetas '" + numeroDesde + "', '" + numeroHasta + "', " + idFranquicia + ", " + idMarca + ", '" + tipo + "'";
                var total = dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> CargarMarcas(int idFranquicia)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Marcas.Where(x => x.IDFranquicia == idFranquicia)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDMarca,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }

        return entities;
    }
}