﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Transacciones.aspx.cs" Inherits="modulos_gestion_Transacciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--<link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/transacciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li class="last">Transacciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Transacciones</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. Tarjeta</label>
                            <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>Numero Est</label>
                            <asp:TextBox runat="server" ID="txtNumeroEst" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2">
                            <label>Nro. Doc. Socio</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                        <div class="col-md-2">
                            <label>Terminal</label>
						    <input type="text" id="txtTerminal" value="" maxlength="20" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Origen</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlOrigen">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Visa" Value="Visa"></asp:ListItem>
                                <asp:ListItem Text="Web" Value="Web"></asp:ListItem>
                                <asp:ListItem Text="POS" Value="POS"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Operación</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlOperacion">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Venta" Value="Venta"></asp:ListItem>
                                <asp:ListItem Text="Anulacion" Value="Anulacion"></asp:ListItem>
                                <asp:ListItem Text="Canje" Value="Canje"></asp:ListItem>
                                <asp:ListItem Text="Carga" Value="Carga"></asp:ListItem>
                                <asp:ListItem Text="Descarga" Value="Descarga"></asp:ListItem>
                                <asp:ListItem Text="CuponIn" Value="CuponIn"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label>Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="100" />
                        </div>
                        <div class="col-sm-2">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" />
                        </div>
                        <div class="col-sm-2">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="nueva();">Nuevo</button>
                            <div class="btn-group" id="btnMasAccciones" runat="server">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a href="<%= ResolveUrl("~/modulos/gestion/ArancelPuntosMarcase.aspx") %>">Actualizacion de arancel, puntos y descuentos por marcas</a></li>
                                    <li><a href="<%= ResolveUrl("~/modulos/gestion/Arancel-puntos-empresase.aspx") %>">Actualizacion de arancel, puntos y descuentos por empresas</a></li>
                                    <li><a href="<%= ResolveUrl("~/modulos/seguridad/Importacion.aspx") %>">Importar manualmente archivo VISA</a></li>
						        </ul>
					        </div>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Transacciones" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>

</asp:Content>

