﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="msjPushe.aspx.cs" Inherits="modulos_gestion_msjPushe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/notificacionesPush.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/frontend/bundle.js") %>"></script>    
	
    <%--multiselect--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />

    <style type="text/css">
        .thumb {
            width: inherit;
            margin: 5px;
            border: solid 1px #ccc;
        }

        .sexo {
            height: 100px;
        }

        input.radio.todos {
            margin-left: 46px;
        }

        input.radio.femenino {
            margin-left: 20px;
        }

        input.radio.masculino {
            margin-left: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Enviar notificaciones push</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Notificacion push</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Las notificaciones estan en camino.</div>
            <label id="lblError" style="color: red; display: none;"></label>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">


              <div id="divMarcas">
                    <div class="form-group"  runat="server">
                        <label class="col-lg-2 control-label">Marcas</label>
                        <div class="col-lg-9"  style="margin-bottom: 10px;">
                            <asp:DropDownList class="form-control" runat="server" ID="ddlMarcas" multiple="multiple" DataTextField="Nombre" DataValueField="ID" />
                                                    
                        </div>
                    </div>
                </div>
           

                <div id="divSedes">
                    <div class="form-group"  runat="server">
                        <label class="col-lg-2 control-label">Terminales</label>
                        <div class="col-lg-9"  style="margin-bottom: 10px;">
                            <asp:DropDownList class="form-control" runat="server" ID="searchable" multiple="multiple" DataTextField="Nombre" DataValueField="ID" />
                        
                            <br />

                            <a href='#' id='select-all'>seleccionar todos del listado</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a> | <strong> <a href="#"> Seleccionar todas las terminales M-POS y NEXGO</a></strong>

                            
                        </div>
                    </div>
                </div>

               <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Titulo</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtTitulo" textMode="multiline" Columns="50" Rows="1" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mensaje</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" textMode="multiline" Columns="50" Rows="5" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>


              <div id="divSedes">
                    <div class="form-group"  runat="server">
                        <label class="col-lg-2 control-label">Accion (Redirigir al apretar notificacion)</label>
                        <div class="col-lg-9"  style="margin-bottom: 10px;">
                            <asp:DropDownList class="form-control" runat="server" ID="txtAccion" multiple="multiple" DataTextField="Nombre" DataValueField="ID" />
                                  
                        </div>
                    </div>
                </div>
             
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button runat="server" id="btnEnviar" class="btn btn-success" type="button">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

