﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Tarjetas.aspx.cs" Inherits="modulos_gestion_Tarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/tarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li class="last">Tarjetas</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Tarjetas</h3>
             <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTarjeta" runat="server">
                <div class="formSep col-sm-8 col-md-8">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <label>Nro. Tarjeta</label>
                            <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" ClientIDMode="Static" />
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" ClientIDMode="Static" />
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label>Tipo</label>
                            <asp:DropDownList runat="server" class="form-control" ID="cmbTipo">
                           <asp:ListItem Text="Todos" Value=""></asp:ListItem>

                            <asp:ListItem Text="Beneficio" Value="Beneficio"></asp:ListItem>
                            <asp:ListItem Text="Giftcard" Value="Giftcard"></asp:ListItem>
                            <asp:ListItem Text="CuponIN" Value="CuponIN"></asp:ListItem>
                        </asp:DropDownList>           

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-sm-md-12">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnVerTodos" onclick="verTodos();">Ver Todos</button>
                            
                            <div class="btn-group">
						        <button class="btn btn-info">Otras acciones</button>
						        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
						        <ul class="dropdown-menu">
							        <li><a href="javascript:exportar();">Exportar</a></li>
                                    <li><a href="TarjetasSustitucion.aspx">Anular/Sustituir</a></li>
                                    <li><a href="AltaMasiva.aspx">Alta masiva</a></li>
							        <li><a href="BajaMasiva.aspx">Baja masiva</a></li>
							        <li><a href="javascript:exportarCalcupan();">Exportar calcupan</a></li>
							        <li><a href="ReactivarTarjetas.aspx">Reactivacion masiva</a></li>
                                    <li><a href="ReasignacionTarjetas.aspx">Reasignacion masiva</a></li>
                                    <li><a href="AsociarTarjetas.aspx">Asociar tarjetas</a></li>
                                    <li><a href="bins.aspx">Modificar BIN</a></li>
						        </ul>
					        </div>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="tarjetas" style="display:none">Descargar</a>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingCalcupan" style="display:none" />
                            <a href="" id="lnkDownloadCalcupan" download="tarjetasCalcupan" style="display:none">Descargar</a>
                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>

    <div class="modal fade" id="modalDetalleTr">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalleTr"></h3>
				</div>
				<div class="modal-body">
					<table class="table table-condensed table-striped" id="tableDetalleTr">
						<thead id="headDetalleTr">
							<tr>
                                <th>Fecha</th> 
                                <th>Hora</th> 
                                <th>Operacion</th> 
                                <th>SDS</th> 
                                <th>Comercio</th> 
                                <th>Establecimiento</th> 
                                <th>$ Original</th> 
                                <th>$ Ahorro</th> 
                                <th>Puntos</th> 
                            </tr>
						</thead>
						<tbody id="bodyDetalleTr">
							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="$('#modalDetalleTr').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>



        <div class="modal fade" id="modalTipoTarjetas">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

					<h3 class="modal-title" id="titEditarTipo"></h3>
				</div>
				<div class="modal-body">
					 <div class="modal-body">
                            <div class="container">
                                   <form id="formEdicion" >
                                                               <input type="hidden"  id="hdnIDTarjeta"/>

                                   <div class="row">                                
                                       <div class="col-sm-6">
                                            <label class="col-lg-4 control-label"><span class="f_req">*</span> Tipo tarjeta</label>
                                            <div class="col-lg-6">
                                                  <select class="form-control" id="ddlTipo">
                                                      <option  value="Beneficio">Beneficio</option>
                                                      <option  value="Giftcard">Giftcard</option>
                                                      <option  value="CuponIN">CuponIN</option>
                                                  </select>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
				</div>
				<div class="modal-footer">
                                            <button class="btn btn-success" id="btnGuardar" onclick="guardarTipo();">Guardar</button>

					<button type="button" class="btn btn-default" onclick="$('#modalTipoTarjetas').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>





























</asp:Content>

