﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReasignacionTarjetas.aspx.cs" Inherits="modulos_gestion_ReasignacionTarjetas" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/reasignacionTarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/gestion/tarjetas.aspx") %>">Tarjetas</a></li>
                <li>Reasignación de tarjetas</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Reasignación de tarjetas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos han sido actualizados correctamente</div>
            <form runat="server" id="formAltaMasiva" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="txtNumeroDesde" class="col-lg-2 control-label"><span class="f_req">*</span> Número Desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtNumeroDesde" ClientIDMode="Static" CssClass="form-control numeric"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtNumeroHasta" class="col-lg-2 control-label"><span class="f_req">*</span> Número Hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtNumeroHasta" ClientIDMode="Static" CssClass="form-control numeric"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ddlFranquicias" class="col-lg-2 control-label"><span class="f_req">*</span> Franquicia</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Marca</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="ddlTipo" class="col-lg-2 control-label"><span class="f_req">*</span> Tipo</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTipo" ClientIDMode="Static">
                            <asp:ListItem Text="Beneficio" Value="B"></asp:ListItem>
                            <asp:ListItem Text="Giftcard" Value="G"></asp:ListItem>
                            <asp:ListItem Text="CuponIN" Value="C"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="validar();">Reasignar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display: none" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

