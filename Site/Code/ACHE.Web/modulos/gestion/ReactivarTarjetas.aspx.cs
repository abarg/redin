﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using ACHE.Model;
using System.Web.Services;

public partial class modulos_gestion_ReactivarTarejtas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarCombos();

    }
    private void cargarCombos() {
        try {
            bMarca bMarca = new bMarca();
            this.ddlMarcas.DataSource = bMarca.getMarcas();
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            //this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }
    protected void Importar(object sender, EventArgs e) {
        string path = string.Empty;
        pnlError.Visible = pnlOK.Visible = false;
        //lblResultados.Text = "";

        if (flpArchivo.HasFile) {
            //if (flpArchivo.FileName.Split('.')[flpArchivo.FileName.Split('.').Length - 1].ToUpper() != "CSV")
            //    ShowError("Formato incorrecto. El archivo debe ser CSV.");
            //else {

                try {
                    path = Server.MapPath("~/files//importaciones//") + flpArchivo.FileName;
                    flpArchivo.SaveAs(path);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("NumeroTarjeta");
                    dt.Columns.Add("IDMarca");


                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    int errores = 0;
                    int total = 0;

                    #region Armo Datatable

                    for (int i = 0; i < rows.Length; i++) {
                        //Split the tab separated fields (comma separated split commented)
                        if (i > 0) {
                            string[] dr = rows[i].Split(new char[] { ';' });
                           
                            if (dr.Length > 0) {
                                #region Armo DataRow
                                if (dr[0].Trim() != "") {
                                    total++;
                                    try {

                                        DataRow drNew = dt.NewRow();
                                        drNew[0] = dr[0].Trim();
                                        drNew[1] = int.Parse(ddlMarcas.SelectedValue);

                                        dt.Rows.Add(drNew);
                                    }
                                    catch (Exception ex) {
                                        var msg = ex.Message;
                                        errores++;
                                    }
                                }

                                #endregion
                            }
                        }
                    }

                    #endregion

                    bool ok = true;
                    #region Importar datatable

                    if (dt.Rows.Count > 0) {
                        EliminarTmp();
                        try {
                            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString)) {
                                sqlConnection.Open();

                                // Run the Bulk Insert
                                using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString)) {
                                    //bulkCopy.BulkCopyTimeout = 60 * 6;//3 min
                                    bulkCopy.DestinationTableName = " [dbo].[TarjetasTmp]";

                                    foreach (DataColumn col in dt.Columns)
                                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                                    bulkCopy.WriteToServer(dt);
                                }
                            }
                            ProcesarTmp();
                        }
                        catch (Exception ex) {
                            ok = false;
                            ShowError("Se ha producido un error inesperado. Contacte al adminstrador. Detalle: " + ex.Message);
                        }
                    }

                    File.Delete(path);

                    if (ok) {

                        int totalOk = dt.Rows.Count - errores;

                        if (errores > 0) {
                            ShowError("Se encontraron " + errores + " errores en la importación a la tabla temporal.");
                            //lblResultados.ForeColor = Color.Red;
                            //lblResultados.Visible = true;
                        }

                        int cantErrores = 0;

                        using (var dbContext = new ACHEEntities()) {
                            cantErrores = dbContext.TarjetasTmp.Where(x => x.Error.Length > 0).Count();
                        }

                        if (cantErrores > 0) {
                            totalOk = totalOk - cantErrores;
                            var msg = ". Se encontraron <a href='javascript:verErrores();'>" + cantErrores + " errores</a> en la importación.";

                            ShowError("Registros importados: " + totalOk + " de " + (total).ToString() + msg);

                            //lblResultados.ForeColor = Color.Red;
                            //lblResultados.Visible = true;
                        }
                        else
                            ShowOk("Registros importados: " + totalOk + " de " + (total).ToString());
                    }
                    #endregion
                }
                catch (Exception ex) {
                    ShowError(ex.Message.ToString());
                }
            //}
        }
        else
            ShowError("Por favor, ingrese un archivo.");
    }
    [WebMethod(true)]
    public static string obtenerErrores() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.TarjetasTmp.Where(x => x.Error.Length > 0).ToList();
                if (list.Any()) {
                    foreach (var detalle in list) {
                        html += "<tr>";
                        html += "<td>" + detalle.NumeroTarjeta + "</td>";
                        html += "<td>" + detalle.Marcas.Nombre + "</td>";
                        html += "<td>" + detalle.Error + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    private void EliminarTmp() {
        using (var dbContext = new ACHEEntities()) {
            dbContext.Database.ExecuteSqlCommand("DELETE TarjetasTmp", new object[] { });
        }
    }

    private void ProcesarTmp() {
        using (var dbContext = new ACHEEntities()) {
            dbContext.Database.CommandTimeout = 360;
            dbContext.Database.ExecuteSqlCommand("exec ProcesarTarjetasTmp " + int.Parse(ddlMarcas.SelectedValue), new object[] { });
        }
    }

    private void ShowError(string msg) {
        pnlOK.Visible = false;

        litError.Text = msg;
        pnlError.Visible = true;
    }

    private void ShowOk(string msg) {
        pnlError.Visible = false;

        litOk.Text = msg;
        pnlOK.Visible = true;
    }
}