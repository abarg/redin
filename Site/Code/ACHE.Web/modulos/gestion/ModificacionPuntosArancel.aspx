﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModificacionPuntosArancel.aspx.cs" Inherits="modulos_gestion_ModificacionPuntosArancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li>Modificación de Puntos y Arancel</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Modificación de Puntos y Arancel - HISTÓRICO</h3>
            
            <div class="alert alert-info" >
                La actualización de puntos y arancel se realiza en base al numero de establecimiento de los comercios.
                Si selecciona una Marca, dicha modificación solo aplicará a las tarjetas de la marca seleccionada.
            </div>
            
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-lg-2 control-label"> Comercio</label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" ID="ddlComercio" CssClass="chzn_b form-control"
                            data-placeholder="Seleccione un comercio"
                            DataTextField="Nombre" DataValueField="NumEst">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Marca Tarjeta</label>
                    <div class="col-lg-8">
                        
                        <asp:DropDownList runat="server" ID="ddlMarcas" CssClass="chzn_b form-control required"
                            data-placeholder="Seleccione una marca"
                            DataTextField="Nombre" DataValueField="IDMarca">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Operación</label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlOperacion">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="Venta" Value="Venta"></asp:ListItem>
                            <asp:ListItem Text="Anulacion" Value="Anulacion"></asp:ListItem>
                            <asp:ListItem Text="Canje" Value="Canje"></asp:ListItem>
                            <asp:ListItem Text="Carga" Value="Carga"></asp:ListItem>
                            <asp:ListItem Text="Descarga" Value="Descarga"></asp:ListItem>
                            <asp:ListItem Text="CuponIN" Value="CuponIN"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Origen</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlOrigen">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="Visa" Value="Visa"></asp:ListItem>
                            <asp:ListItem Text="Web" Value="Web"></asp:ListItem>
                            <asp:ListItem Text="POS" Value="POS"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Fecha Desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtDesde" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Fecha Hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtHasta" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                    </div>
                </div>
               <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Arancel</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtArancel" ClientIDMode="Static" CssClass="form-control required numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Puntos</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtPuntos" ClientIDMode="Static" CssClass="form-control required numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Mult Puntos</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtMulPuntos" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                            </div>
                </div>
                  <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Puntos vip</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtPuntosVip" ClientIDMode="Static" CssClass="form-control required numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Mult Puntos vip</label>
                            <div class="col-lg-3">
                                <asp:TextBox runat="server" ID="txtMulPuntosVip" CssClass="form-control required" MaxLength="2"></asp:TextBox>
                            </div>
                </div>
                     <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Arancel vip</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtArancelVip" ClientIDMode="Static" CssClass="form-control required numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Costo Red</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtCostoRed" ClientIDMode="Static" CssClass="form-control required numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="actualizar();">Actualizar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display: none" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/modificacionPuntosArancel.js?v=1") %>"></script>
</asp:Content>