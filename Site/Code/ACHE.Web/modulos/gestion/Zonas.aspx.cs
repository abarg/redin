﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;

public partial class modulos_gestion_Zonas : PaginaBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarProvincias();
    }

    private void cargarProvincias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var provincias = dbContext.Provincias.OrderBy(x => x.Nombre).ToList();
            if (provincias != null)
            {
                cmbProvincia.DataSource = provincias;
                cmbProvincia.DataTextField = "Nombre";
                cmbProvincia.DataValueField = "IDProvincia";
                cmbProvincia.DataBind();
                cmbProvincia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                return dbContext.Zonas.Include("Ciudades")
                        .OrderBy(x => x.Nombre)
                        .Select(x => new ZonasViewModel()
                        {
                            IDZona = x.IDZona,
                            IDProvincia = x.IDProvincia,
                            IDCiudad = x.IDCiudad,
                            Ciudad = x.Ciudades.Nombre,
                            Provincia = x.Provincias.Nombre,

                            Nombre = x.Nombre
                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var zona = dbContext.Zonas.Where(x => x.IDZona == id).FirstOrDefault();
                    if (zona != null)
                    {
                        if (dbContext.Comercios.Any(x => x.IDZona.HasValue && x.IDZona.Value == id))
                            throw new Exception("La zona se encuentra asignada a 1 o más comercios. No se puede eliminar");
                        dbContext.Zonas.Remove(zona);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string idProvincia, string nombre)
    {
        string fileName = "Zonas";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.Zonas.Include("Provincias").Include("Ciudades").OrderBy(x => x.Nombre).AsEnumerable();
                    if (!string.IsNullOrEmpty(idProvincia))
                        if (int.Parse(idProvincia) > 0)
                            info = info.Where(x => x.IDProvincia == int.Parse(idProvincia));
                    if (!string.IsNullOrEmpty(nombre))
                        info = info.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));

                   var aux = info.Select(x => new
                    {
                        IDZona = x.IDZona,
                        IDProvincia = x.IDProvincia,
                        Provincia = x.Provincias.Nombre,
                        Ciudad = x.Ciudades.Nombre,
                        Nombre = x.Nombre,
                    }).ToList();
                   dt = aux.ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}