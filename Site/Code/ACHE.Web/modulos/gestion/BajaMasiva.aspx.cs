﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_gestion_BajaMasiva : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            this.LoadInfo();
    }

    private void LoadInfo() {
        try {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
            
            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod]
    public static List<ComboViewModel> GetMarcas(int idFranquicia) {
        List<ComboViewModel> result = new List<ComboViewModel>();
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                bMarca bMarca = new bMarca();
                List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(idFranquicia);
                if (listMarcas != null && listMarcas.Count() > 0)
                    result = listMarcas.Select(x => new ComboViewModel() { ID = x.IDMarca.ToString(), Nombre = x.Nombre }).ToList();
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return result;
    }

    [System.Web.Services.WebMethod]
    public static List<int> EjecutarBaja(int desde, int hasta, int idMarca, int idFranquicia, string tipo) {
        List<int> result = new List<int>();
        try {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var user = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities()) {
                    string prefijo = dbContext.Marcas.Where(x => x.IDMarca == idMarca).FirstOrDefault().Prefijo;
                    if (!string.IsNullOrEmpty(prefijo)) {
                        string fechaHoy = DateTime.Now.ToString(formato);
                        string sqlCant = "select count(*) from Tarjetas where TipoTarjeta = '" + tipo + "' and Numero like '%" + prefijo + "%' and IDFranquicia = " + idFranquicia + " and IDMarca = " + idMarca + " and substring(Numero, 11,5) >= " + desde + " and substring(Numero, 11,5) <= " + hasta + " and PuntosTotales = 0 and Estado = 'A'";
                        int cantBajas = dbContext.Database.SqlQuery<Int32>(sqlCant, new object[] { }).First();

                        var sqlBaja = sqlCant.Replace("select count(*) from Tarjetas", "update Tarjetas set Estado = 'B', FechaBaja = '" + fechaHoy + "', MotivoBaja = 'Masiva', UsuarioBaja = '" + user.Usuario + "'");
                        dbContext.Database.ExecuteSqlCommand(sqlBaja);

                        string sqlCantPuntos = "select count(*) from Tarjetas where TipoTarjeta = '" + tipo + "' and Numero like '%" + prefijo + "%' and IDFranquicia = " + idFranquicia + " and IDMarca = " + idMarca + " and substring(Numero, 11,5) >= " + desde + " and substring(Numero, 11,5) <= " + hasta + " and PuntosTotales > 0 and Estado = 'A'";
                        int cantPuntos = dbContext.Database.SqlQuery<Int32>(sqlCantPuntos, new object[] { }).First();

                        result.Insert(0, cantPuntos);
                        result.Insert(1, cantBajas);
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return result;
    }

    [System.Web.Services.WebMethod]
    public static int EjecutarBajaPuntos(int desde, int hasta, int idMarca, int idFranquicia, string tipo) {
        int result = 0;
        try {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var user = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities()) {
                    string prefijo = dbContext.Marcas.Where(x => x.IDMarca == idMarca).FirstOrDefault().Prefijo;
                    if (!string.IsNullOrEmpty(prefijo)) {
                        string sqlPuntos = "select count(*) from tarjetas where TipoTarjeta = '" + tipo + "' and Numero like '%" + prefijo + "%' and IDFranquicia = " + idFranquicia + " and IDMarca = " + idMarca + " and substring(Numero, 11,5) >= " + desde + " and substring(Numero, 11,5) <= " + hasta + " and PuntosTotales > 0 and Estado = 'A'";
                        result = dbContext.Database.SqlQuery<Int32>(sqlPuntos, new object[] { }).First();
                        
                        string fechaHoy = DateTime.Now.ToString(formato);
                        string sqlBaja = "update Tarjetas set Estado = 'B', FechaBaja = '" + fechaHoy + "', MotivoBaja = 'Masiva', UsuarioBaja = '" + user.Usuario + "' where TipoTarjeta = '" + tipo + "' and Numero like '%" + prefijo + "%' and IDFranquicia = " + idFranquicia + " and IDMarca = " + idMarca + " and substring(Numero, 11,5) >= " + desde + " and substring(Numero, 11,5) <= " + hasta + " and PuntosTotales > 0 and Estado = 'A'";
                        dbContext.Database.ExecuteSqlCommand(sqlBaja);
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return result;
    }
}