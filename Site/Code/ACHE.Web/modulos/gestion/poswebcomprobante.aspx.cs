﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class modulos_gestion_poswebcomprobante : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var id = Request.QueryString["Id"];
            if (id != string.Empty)
            {
                int idTr = int.Parse(id);

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Transacciones.Where(x => x.IDTransaccion == idTr && x.Origen == "Web").FirstOrDefault();
                    if (entity != null)
                    {
                        litTitulo.Text = entity.FechaTransaccion.ToString("dd/MM/yyyy") + " - RED IN - " + entity.FechaTransaccion.ToString("HH:mm");
                        litOperacion.Text = entity.Operacion;

                        var comercio = dbContext.Comercios.Where(x => x.IDComercio == entity.IDComercio).FirstOrDefault();

                        litComercio.Text = comercio.RazonSocial;
                        litDomicilio.Text = comercio.Domicilios.Domicilio;
                        litCuit.Text = comercio.NroDocumento;

                        litNroComprobante.Text = entity.NroComprobante;
                        litTipoComprobante.Text = entity.TipoComprobante;
                        litPtoVenta.Text = entity.PuntoDeVenta;

                        decimal importe = entity.Importe.HasValue ? entity.Importe.Value : 0;
                        decimal ahorro = entity.ImporteAhorro.HasValue ? entity.ImporteAhorro.Value : 0;
                        decimal usoRed = entity.UsoRed + (entity.UsoRed * 0.21M);

                        bTarjeta bTarjeta = new bTarjeta();
                        Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(entity.NumTarjetaCliente, false);

                        litImporteTotal.Text = importe.ToString("N2");
                        litUsoRed.Text = usoRed.ToString("N2");
                        litDescuento.Text = ahorro.ToString("N2");
                        litPuntos.Text = entity.PuntosAContabilizar.HasValue ? entity.PuntosAContabilizar.Value.ToString() : "";
                        litPuntosTotales.Text = oTarjeta.PuntosTotales.ToString();

                        decimal importeTotal = (importe - ahorro) + (usoRed + (usoRed * 0.21M));

                        litImporteConDescuento.Text = importeTotal.ToString("N2");

                        if (ahorro <= 0)
                            divDescuento1.Visible = divDescuento2.Visible = false;
                    }
                    else
                        Response.Redirect("home.aspx");
                }
            }
            else
                Response.Redirect("home.aspx");
        }
    }
}