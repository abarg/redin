﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using System.IO;
//using System.Transactions;

public partial class modulos_gestion_TarjetasSustitucion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.cargarMarcas();
            //this.getSocios();
            if (Request.QueryString["IDSocio"] != null)
            {
                //this.ddlSocio.SelectedValue = Request.QueryString["IDSocio"];
                //this.ddlSocio.Enabled = false;
                this.hfIDSocio.Value = Request.QueryString["IDSocio"];
            }
            if (Request.QueryString["IDTarjeta"] != null)
            {
                this.hfIDTarjeta.Value = Request.QueryString["IDTarjeta"];
            }
        }
    }

    private void cargarMarcas()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            this.ddlMarcas.Enabled = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    void Page_PreInit(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
            MasterPageFile = "~/MasterPageMarcas.master";

    }

    [WebMethod(true)]
    public static void grabar(string Tipo, string Numero, int Marca, string Motivo, string NumeroNuevo, int IDSocio)
    {
        try
        {
            //Tarjetas
            bool actualizarTr = false;
            //using (TransactionScope ts = new TransactionScope())
            //{
            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(Numero, false);
            Tarjetas oTarjetaNueva = null;

            if (oTarjeta == null) throw new Exception("El Número de Tarjeta no existe");

            if (Tipo.Equals("D")) //Desvinculación
            {
                if (!oTarjeta.IDSocio.HasValue)
                    throw new Exception("La Tarjeta no tiene un socio asignado");

                oTarjeta.IDSocio = null;
                bTarjeta.add(oTarjeta);
            }
            else if (Tipo.Equals("A")) //Anulación
            {
                oTarjeta.Estado = "B";
                oTarjeta.FechaBaja = DateTime.Now;
                oTarjeta.MotivoBaja = Motivo;

                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    var Usuarios = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }
                else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var Usuarios = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }

                bTarjeta.add(oTarjeta);
            }
            else if (Tipo.Equals("S")) //Sustitución
            {
                if (oTarjeta.IDSocio != IDSocio)
                    throw new Exception("La Tarjeta no corresponde al Socio ingresado");
                //int PuntosDisponibles = oTarjeta.PuntosDisponibles.HasValue ? oTarjeta.PuntosDisponibles.Value : 0;
                int PuntosTotales = oTarjeta.PuntosTotales;

                oTarjetaNueva = bTarjeta.getTarjetaPorNumero(NumeroNuevo, false);
                if (oTarjetaNueva == null)
                    throw new Exception("El Nuevo Número de Tarjeta no existe");
                if (oTarjetaNueva.IDSocio.HasValue && oTarjetaNueva.IDSocio != oTarjeta.IDSocio)
                    throw new Exception("La Nueva Tarjeta tiene un Socio vinculado");

                cMarca cMarca = new cMarca();
                var marcaTarjetaNueva = cMarca.getMarcas().Where(x => x.IDMarca == Marca);
                int idFranquicia = 0;
                if (marcaTarjetaNueva != null) {
                    idFranquicia = marcaTarjetaNueva.FirstOrDefault().IDFranquicia ?? 0;
                    if (idFranquicia == 0) {
                        throw new Exception("La Franquicia a asignar no existe");
                    }
                }
                else
                    throw new Exception("La Marca a asignar no existe");

                oTarjeta.Estado = "B";
                oTarjeta.FechaBaja = DateTime.Now;
                oTarjeta.MotivoBaja = Motivo;
                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    var Usuarios = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }
                else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                {
                    var Usuarios = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                    oTarjeta.UsuarioBaja = Usuarios.Usuario;
                }
                oTarjeta.PuntosTotales = 0;

                decimal credito = oTarjeta.Credito;
                decimal giftcard = oTarjeta.Giftcard;

                oTarjeta.Credito = 0;
                oTarjeta.Giftcard = 0;

                bTarjeta.add(oTarjeta);

                oTarjetaNueva.Estado = "A";
                oTarjetaNueva.FechaAsignacion = DateTime.Now;
                oTarjetaNueva.IDSocio = oTarjeta.IDSocio;
                
                oTarjetaNueva.IDMarca = Marca;

                oTarjetaNueva.IDFranquicia = idFranquicia;//oTarjeta.IDFranquicia;
                //oTarjetaNueva.PuntosDisponibles = PuntosDisponibles;
                oTarjetaNueva.PuntosTotales += PuntosTotales;
                oTarjetaNueva.Credito += credito;
                oTarjetaNueva.Giftcard += giftcard;

                bTarjeta.add(oTarjetaNueva);

            }

            //ts.Complete();

            if (Tipo.Equals("S"))
                actualizarTr = true;
            //}

            if (actualizarTr)
            {
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.ActualizarTrDeTarjeta(Numero, NumeroNuevo);
                    dbContext.ActualizarPuntosPorTarjeta(NumeroNuevo);
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void grabarMasiva(string Tipo, string Numeros, string Motivo)
    {
        try
        {

            string[] values = Numeros.Split(',');

            List<Tarjetas> list = new List<Tarjetas>();

            foreach(string Numero in values) { 

                bTarjeta bTarjeta = new bTarjeta();
                Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(Numero, false);

                if (oTarjeta == null) throw new Exception("El Número de Tarjeta no existe");

                if (Tipo.Equals("D")) //Desvinculación
                {
                    if (!oTarjeta.IDSocio.HasValue)
                        throw new Exception("La Tarjeta no tiene un socio asignado");

                    oTarjeta.IDSocio = null;
                    bTarjeta.add(oTarjeta);
                }
                else if (Tipo.Equals("A")) //Anulación
                {
                    oTarjeta.Estado = "B";
                    oTarjeta.FechaBaja = DateTime.Now;
                    oTarjeta.MotivoBaja = Motivo;

                    if (HttpContext.Current.Session["CurrentUser"] != null)
                    {
                        var Usuarios = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                        oTarjeta.UsuarioBaja = Usuarios.Usuario;
                    }
                    else if (HttpContext.Current.Session["CurrentMarcasUser"] != null)
                    {
                        var Usuarios = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                        oTarjeta.UsuarioBaja = Usuarios.Usuario;
                    }


                    bTarjeta.add(oTarjeta);
                }


            }


        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();

                var marca = HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.SociosView
                                .Where(x => (x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower())))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();

                            break;
                        case "DNI":
                            list = dbContext.SociosView
                                .Where(x => x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas
                                .Where(x => x.Numero.Contains(valor))
                                .OrderBy(x => x.Numero)
                                .Select(x => new Combo3ViewModel()
                                {
                                    ID = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                                    Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                                    Importe = x.Credito.ToString()

                                }).Distinct().OrderBy(x => x.Nombre).Take(10).ToList();
                            break;
                    }
                }
            

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string buscarSociosMasivo(string tipo, string valores)
    {
        try
        {
            List<TarjetaSocioViewModel> list = new List<TarjetaSocioViewModel>();

                var marca = HttpContext.Current.Session["CurrentUser"];

                string[] values = valores.Split(',');

                using (var dbContext = new ACHEEntities())
                {


                    list = dbContext.Tarjetas
                        .Where(x => values.Contains(x.Numero))
                        .OrderBy(x => x.Numero)
                        .Select(x => new TarjetaSocioViewModel()
                        {
                            IDSocio = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                            Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                            DNI = x.Socios.NroDocumento,
                            NumeroTarjeta = x.Numero,
                            Importe = x.Credito.ToString()

                        }).Distinct().OrderBy(x => x.Nombre).ToList();
                    
                }
            

            return exportarListado(list);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public class TarjetaSocioViewModel
    {
        public int IDSocio { get; set; }
        public string Nombre { get; set; }
        public string DNI { get; set; }
        public string Importe { get; set; }
        public string NumeroTarjeta { get; set; }
    }

    public static string exportarListado(List<TarjetaSocioViewModel> list)
    {

        string fileName = "tarjetas";
        string path = "/tmp/";

        try
        {
            DataTable dt = new DataTable();

            dt = list.ToDataTable();

            if (dt.Rows.Count > 0)
            {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
            {
                throw new Exception("No se encuentran datos para los valores seleccionados");
            }
            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }

    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new ClosedXML.Excel.XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio && !x.FechaBaja.HasValue)
                     .OrderBy(x => x.Numero)
                     .Select(x => new Combo3ViewModel() { ID = x.IDMarca, Nombre = x.Numero, Importe = x.Credito.ToString() })
                     .Distinct().OrderBy(x => x.Nombre).AsEnumerable().ToList();

                
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}

