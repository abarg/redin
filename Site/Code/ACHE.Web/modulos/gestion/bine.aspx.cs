﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;


public partial class modulos_gestion_bine : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            Session["CurrentLogo"] = null;

            if (!String.IsNullOrEmpty(Request.QueryString["IDBIN"])) {
                this.hdnIDBIN.Value = Request.QueryString["IDBIN"];

                if (!this.hdnIDBIN.Value.Equals("0")) {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDBIN"]));
                }
            }
        }
    }

    private void cargarDatos(int id) {
        try {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.BIN.Where(x => x.IDBIN == id).FirstOrDefault();
                if (entity != null) {
                    this.txtNombre.Text = entity.Nombre;
                    this.txtNumero.Text = entity.Numero.ToString();
                }
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int id, string nombre,int numero) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    BIN entity;
                    if (id > 0)
                        entity = dbContext.BIN.Where(x => x.IDBIN == id).FirstOrDefault();
                    else {
                        entity = new BIN();
                    }
                    entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper() : "";
                    entity.Numero = numero;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.BIN.Add(entity);
                        dbContext.SaveChanges();
                    }

                    return entity.IDBIN.ToString();
                }

            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";

    }

}