﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AltaMasiva.aspx.cs" Inherits="modulos_gestion_AltaMasiva" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/altaMasiva.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/gestion/tarjetas.aspx") %>">Tarjetas</a></li>
                <li>Alta Masiva</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Alta Masiva</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            
            <form runat="server" id="formAltaMasiva" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Franquicia</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control required" ID="ddlFranquicias" ClientIDMode="Static" onchange="getMarcas();" />
                    </div>
                </div>       
                <div class="form-group">
                    <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Marca</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control required" ID="ddlMarcas" onchange="getDatosMarca();" ClientIDMode="Static" />
                    </div>
                </div>   
                <div class="form-group">
                    <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Tipo</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTipo">
                            <asp:ListItem Text="Beneficio" Value="B"></asp:ListItem>
                            <asp:ListItem Text="Giftcard" Value="G"></asp:ListItem>
                            <asp:ListItem Text="CuponIN" Value="C"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>   
                <div class="form-group">
                    <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> PAN - Desde</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlBIN">
                        </asp:DropDownList>
                    </div>
                </div> 
<%--                <div class="form-group">
                    <label for="txtPanDesde" class="col-lg-2 control-label"><span class="f_req">*</span> PAN - Desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtPanDesde" CssClass="form-control required number numeric" MaxLength="5" Text="63711"></asp:TextBox>
                    </div>
                </div>--%>
                <div class="form-group">
                    <label for="txtPanHasta" class="col-lg-2 control-label"><span class="f_req">*</span> PAN - Hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtPanHasta" CssClass="form-control required number numeric" MaxLength="5" Text="70100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtDesde" class="col-lg-2 control-label"><span class="f_req">*</span> Desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtDesde" CssClass="form-control required number numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtHasta" class="col-lg-2 control-label"><span class="f_req">*</span> Hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtHasta" CssClass="form-control required number numeric" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

