﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ZonasEdicion.aspx.cs" Inherits="modulos_gestion_ZonasEdicion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/zonasEdicion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <style type="text/css">
        #map-canvas {
            height: 280px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/gestion/Zonas.aspx") %>">Zonas</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Zona</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formZona" class="form-horizontal" role="form">
                <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
                <br />
                <div class="form-group">
                    <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbProvincia" ClientIDMode="Static" CssClass="form-control required chosen" data-placeholder="Seleccione una provincia" onchange="LoadCiudades2(this.value,'cmbCiudad');" />
                    </div>
                </div>

              <div class="form-group">
                    <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Ciudad</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbCiudad" ClientIDMode="Static" CssClass="form-control required chosen" data-placeholder="Seleccione una ciudad" />
                    </div>
                </div>


                <div class="form-group">
                    <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required small" MaxLength="100" />
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIDZona" Value="0" />
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a href="Zonas.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

</asp:Content>

