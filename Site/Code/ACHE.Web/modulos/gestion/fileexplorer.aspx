﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="fileexplorer.aspx.cs" Inherits="modulos_gestion_fileexplorer" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li>File Explorer</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">File Explorer</h3>

            <form id="form1" runat="server">
                <br />
                <telerik:RadScriptManager ID="RadScriptManager1" runat="Server">
                </telerik:RadScriptManager>

                <telerik:RadFileExplorer runat="server" ID="FileExplorer1"
                    EnableOpenFile="false" DisplayUpFolderItem="true" EnableCreateNewFolder="true"
                    Width="100%" Height="800px" Skin="Default"
                    OnClientFileOpen="OnClientFileOpen">
                    <Configuration ViewPaths="~/files/explorer" UploadPaths="~/files/explorer" DeletePaths="~/files/explorer"
                        MaxUploadFileSize="50000000" />
                    
                </telerik:RadFileExplorer>

            </form>
        </div>
    </div>

    <script type="text/javascript">
        function OnClientFileOpen(oExplorer, args) {
            var item = args.get_item();
            var fileExtension = item.get_extension();
            //alert(fileExtension);
            //var fileDownloadMode = document.getElementById("chkbxDownoaldFile").checked;
            if (fileExtension != null && fileExtension != "") {// Download the file
                // File is a image document, do not open a new window
                args.set_cancel(true);

                // Tell browser to open file directly
                var requestImage = "Handler.ashx?path=" + item.get_url();
                document.location = requestImage;
            }
        }
    </script>

</asp:Content>

