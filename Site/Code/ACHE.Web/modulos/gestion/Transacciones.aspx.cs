﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using System.Data;

public partial class modulos_gestion_Transacciones : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
        }
    }

    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            var listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("", ""));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta, string NumTerminal)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesView
                    .OrderByDescending(x => x.FechaTransaccion)
                    .Select(x => new TransaccionesViewModel()
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        PuntosTotales = x.PuntosTotales,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        IDMarca = x.IDMarca,
                        Marca = x.Marca,
                        IDFranquicia = x.IDFranquicia,
                        NumTerminal = x.POSTerminal
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }
                if (NumTerminal != string.Empty)
                {
                    
                    result = result.Where(x => x.NumTerminal == NumTerminal);
                }
                var cant=result.ToList().Count();
                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {


                    var tr = dbContext.Transacciones.Where(x => x.IDTransaccion == id).FirstOrDefault();
                    if (tr != null)
                    {
                        var tarjeta = tr.NumTarjetaCliente;

                        dbContext.Transacciones.Remove(tr);
                        dbContext.SaveChanges();

                        dbContext.ActualizarPuntosPorTarjeta(tarjeta);
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string NumEst, string documento, string comercio,
        int idMarca, int idFranquicia, string origen, string operacion, string NumTerminal)
    {
        string fileName = "Transacciones";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.CommandTimeout = 360;
                    
                    var info = dbContext.TransaccionesView.OrderByDescending(x => x.FechaTransaccion).AsQueryable();

                    if (tarjeta != "")
                        info = info.Where(x => x.Numero.ToLower().Contains(tarjeta.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumentoSocio.ToLower().Contains(documento.ToLower()));
                    if (NumEst != "")
                        info = info.Where(x => x.NroEstablecimiento != null && x.NroEstablecimiento.ToLower().Contains(NumEst.ToLower()));
                    if (comercio != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(comercio.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }
                    if (idMarca > 0)
                        info = info.Where(x => x.IDMarca == idMarca);
                    if (idFranquicia > 0)
                        info = info.Where(x => x.IDFranquicia == idFranquicia);
                    if (origen != "")
                        info = info.Where(x => x.Origen == origen);
                    if (operacion != "")
                        info = info.Where(x => x.Operacion == operacion);
                    if (NumTerminal != "")
                        info = info.Where(x => x.POSTerminal == NumTerminal);

                    dt = info.ToList().Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Origen = x.Origen,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        NumTerminal = x.POSTerminal,
                        Marca = x.Marca,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                        //PuntosTotales = .PuntosTotales,//TODO: PORQUE NO FUNCIONA?
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Descripcion = x.Descripcion
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        return GetTrBySocio(id, null);

    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocioMax(int id, int max)
    {
        return GetTrBySocio(id, max);
    }

    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesView.Where(x => x.IDSocio.HasValue && x.IDSocio == id)
                    .OrderByDescending(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel()
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                    });

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjeta(string tarjeta)
    {
        return GetTrByTarjeta(tarjeta, null);
    }

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetByTarjetaMax(string tarjeta, int max)
    {
        return GetTrByTarjeta(tarjeta, max);
    }

    private static List<TransaccionesViewModel> GetTrByTarjeta(string tarjeta, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesView.Where(x => x.Numero == tarjeta && x.ImporteOriginal > 1)
                     .OrderByDescending(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel
                     {
                         Fecha = x.Fecha,
                         FechaTransaccion = x.FechaTransaccion,
                         Hora = x.Hora,
                         Origen = x.Origen,
                         Operacion = x.Operacion,
                         SDS = x.SDS,
                         Comercio = x.NombreFantasia,
                         NroEstablecimiento = x.NroEstablecimiento,
                         Tarjeta = x.Numero,
                         Socio = x.Apellido + ", " + x.Nombre,
                         ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                         ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                         Puntos = x.PuntosAContabilizar ?? 0,
                     }).ToList();

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo2ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.Socios
                                .Where(x => x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo2ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Take(10).ToList();
                            break;
                        case "DNI":
                            list = dbContext.Socios
                                .Where(x => x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo2ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Take(10).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas.Include("Socios").Where(x => x.IDSocio.HasValue && x.Numero.Contains(valor))
                                 .OrderBy(x => x.Numero)
                                 .Select(x => new Combo2ViewModel() { ID = x.IDSocio.Value, Nombre = x.Socios.Apellido + ", " + x.Socios.Nombre })
                                .Take(10).ToList();
                            break;
                    }

                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    //[System.Web.Services.WebMethod(true)]
    //public static List<Combo2ViewModel> buscarComercios(string tipo, string valor)
    //{
    //    try
    //    {
    //        List<Combo2ViewModel> list = new List<Combo2ViewModel>();
    //        if (HttpContext.Current.Session["CurrentUser"] != null)
    //        {
    //            using (var dbContext = new ACHEEntities())
    //            {
    //                switch (tipo)
    //                {
    //                    case "Nombre":
    //                        list = dbContext.Comercios
    //                            .Where(x => x.NombreFantasia.ToLower().Contains(valor.ToLower()) || x.RazonSocial.ToLower().Contains(valor.ToLower()))
    //                            .OrderBy(x => x.NombreFantasia)
    //                            .Select(x => new Combo2ViewModel() { ID = x.IDComercio, Nombre = x.NombreFantasia })
    //                            .Take(10).ToList();
    //                        break;
    //                    case "CUIT":
    //                        list = dbContext.Comercios
    //                            .Where(x => x.NroDocumento.Contains(valor.ToLower()))
    //                            .OrderBy(x => x.NombreFantasia)
    //                            .Select(x => new Combo2ViewModel() { ID = x.IDComercio, Nombre = x.NombreFantasia })
    //                            .Take(10).ToList();
    //                        break;
    //                    case "SDS":
    //                        list = dbContext.Comercios
    //                            .Where(x => x.SDS.Contains(valor.ToLower()))
    //                            .OrderBy(x => x.NombreFantasia)
    //                            .Select(x => new Combo2ViewModel() { ID = x.IDComercio, Nombre = x.NombreFantasia })
    //                            .Take(10).ToList();
    //                        break;
    //                    case "Terminal":
    //                        list = dbContext.Comercios
    //                            .Where(x => x.POSTerminal.Contains(valor.ToLower()))
    //                            .OrderBy(x => x.NombreFantasia)
    //                            .Select(x => new Combo2ViewModel() { ID = x.IDComercio, Nombre = x.NombreFantasia })
    //                            .Take(10).ToList();
    //                        break;
    //                }

    //            }
    //        }

    //        return list;
    //    }
    //    catch (Exception e)
    //    {
    //        var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
    //        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
    //        throw e;
    //    }
    //}

    [System.Web.Services.WebMethod(true)]
    public static List<ComboViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<ComboViewModel> list = new List<ComboViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio)
                     .OrderBy(x => x.Numero)
                     .Select(x => new ComboViewModel() { ID = x.Numero, Nombre = x.Numero })
                     .ToList();
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}