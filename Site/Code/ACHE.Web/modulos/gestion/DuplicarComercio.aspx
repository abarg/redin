﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DuplicarComercio.aspx.cs" Inherits="modulos_gestion_DuplicarComercio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
           <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
        <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <%--    <div class="col-sm-12 col-md-12">--%>
			<h3 class="heading">Duplicar Comercios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-danger alert-dismissable" id="divErrorEmptySearch" style="display: none">El campo del Nombre de comercio no puede estar vacio</div>    
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">El comercio se ha duplicado correctamente</div>
		    <form id="form" runat="server">
            <asp:HiddenField runat="server" ID="hdnIDComercio" Value="0" />
            <div class="formSep col-sm-12 col-md-12" style="margin: 0 0 0 -17px;">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-3">
                        <label><span class="f_req">*</span> Comercios</label>                    
                        <asp:DropDownList runat="server" ClientIDMode="Static" CssClass="form-control required chosen" ID="ddlComercios" PlaceHolder="Elija un comercio" />
                        </div>
                        <div class="col-sm-3">
                            <br />
                        <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>
                    
                        </div>
                    </div>
                </div>            
			  
           </div>
            <div class="row">
                <div class="form-group">
<%--                <div class="col-lg-12">--%>
                    <div  id="datosComercio"  style="display:none">
                        <div class="vcard">
                            <ul  style="margin: 0 0 0 -30px;">
                                <li class="v-heading">
                                Datos del comercio
						        </li>
                                 <li>
                                    <span class="item-key">SDS</span>
					                <div class="vcard-item" id="lblSDS"></div>
                                </li>
                                 <li>
                                    <span class="item-key">Razón Social</span>
					                <div class="vcard-item" id="lblRazonSocial"></div>
                                </li>
                                 <li>
                                    <span class="item-key">Nombre Fantasia</span>
					                <div class="vcard-item" id="lblFantasia"></div>
                                </li> 
                                <li>
                                    <span class="item-key">Nombre Est.</span>
					                <div class="vcard-item" id="lblNomEst"></div>
                                </li>
<%--                                  <li>
                                    <span class="item-key">Nro Est.</span>
					                <div class="vcard-item" id="lblNroEst"></div>
                                </li>--%>
                                <li>
                                    <span class="item-key">Franquicia</span>
					                <div class="vcard-item" id="lblFranq"></div>
                                </li>
                                <li>
                                    <span class="item-key">Marca</span>
					                <div class="vcard-item" id="lblMarca"></div>
                                </li>
                                <li>
                                    <span class="item-key">Tipo Doc.</span>
					                <div class="vcard-item" id="lblTipoDoc"></div>
                                </li>
                                <li>
                                    <span class="item-key">Nro. Doc.</span>
					                <div class="vcard-item" id="lblNroDoc"></div>
                                </li>
                                <li>
                                    <span class="item-key">Rubro</span>
					                <div class="vcard-item" id="lblRubro"></div>
                                </li>
                                <li>
                                    <span class="item-key">Sub Rubro</span>
					                <div class="vcard-item" id="lblSubRubro"></div>
                                </li>
                                <li>
                                    <span class="item-key">Tel.</span>
					                <div class="vcard-item" id="lblTel"></div>
                                </li>                                   
                                          
                                </ul>
                            </div>
                        </div>
                    <%--</div>--%>
                </div>
            </div>
             <div class="row" style="display:none" id="nombreFantasia">
				    <div class="col-sm-3">
					    <label><span class="f_req">* </span>Nombre Fantasía</label>
					    <input type="text" id="txtNombreFantasia" value="" maxlength="150" class="form-control number" />
				    </div>
			    </div>

             <div class="row" style="display:none" id="sds">
				    <div class="col-sm-3">
					    <label><span class="f_req"> </span>SDS</label>
					    <input type="text" id="txtSDS" value="" maxlength="20" class="form-control number" />
				    </div>
			    </div>
        
			<%--    <div class="row" style="display:none" id="pos">
				    <div class="col-sm-3">
					    <label><span class="f_req">* </span>Terminal/Licencia</label>
					    <%--<input type="text" id="txtPOS" value="" maxlength="20"  class="form-control number" />
                        <asp:TextBox runat="server" ID="txtPOS" CssClass="form-control"  MaxLength="10" MinLength="8"></asp:TextBox>
                    </div>     
                </div>   --%>               
                    <div class="row col-sm-3" style="display:none"  id="btnDupliComer">
                        <br />
                    <button class="btn btn-success" type="button" onclick="duplicarComercio();">Duplicar Comercio</button>
                    </div>				    
			   
            </form>            
	<%--	</div>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/duplicarComercios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
