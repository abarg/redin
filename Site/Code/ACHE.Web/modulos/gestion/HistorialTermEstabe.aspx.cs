﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class modulos_gestion_HistorialTermEstabe : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                this.hfID.Value = Request.QueryString["ID"];

                if (!this.hfID.Value.Equals("0"))
                    this.cargarDatosUsuario(Convert.ToInt32(Request.QueryString["ID"]));
            }
        }
    }

   
    private void  cargarCombos()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.Terminales.Include("Comercios").Include("Domicilios")
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDTerminal,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.Comercios.NombreFantasia + " - " + x.Domicilios.Domicilio + " - " + x.NumEst  + " - " + x.POSTerminal
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        ddlComercio.DataSource =entities;
        ddlComercio.DataBind();
    }

    [WebMethod(true)]
    public static void grabar(int id, int idTerminal, string tipo, string anterior, string nuevo, string observaciones)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {

                HistorialTerminales entity;
                if (id > 0)
                    entity = dbContext.HistorialTerminales.Where(x => x.IDHistorial == id).FirstOrDefault();
                else
                {
                    entity = new HistorialTerminales();
                    entity.FechaBaja = DateTime.Now;
                }
                entity.Tipo = tipo;
                entity.Anterior = anterior;
                entity.Nuevo = nuevo;
                entity.POSTerminal = "";
                entity.Observaciones = observaciones;
                entity.IDTerminal = idTerminal;

                if (id > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.HistorialTerminales.Add(entity);
                    dbContext.SaveChanges();
                }
            }

        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static int generarCambio(string tipo, int idTerminal, string anterior, string nuevo)
    {
        try
        {
            int result = 0;
            using (var dbContext = new ACHEEntities())
            {
                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.IDTerminal == idTerminal).First();
                
                var sql = "UPDATE Transacciones SET ";                

                if (tipo == "T")//cambio de terminal
                {
                    sql += "NumTerminal = '" + nuevo + "' WHERE NumTerminal = '" + anterior + "' and NumEst = '" + terminal.NumEst + "'";
                    result = dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
                }
                else//cambio de establecimiento
                {
                    sql += "NumEst = '" + nuevo + "' WHERE NumEst = '" + anterior + "' and NumTerminal = '" + terminal.POSTerminal + "'";
                    result = dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
                }
            }

            return result;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void cargarDatosUsuario(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.HistorialTerminales.Where(x => x.IDHistorial == id).FirstOrDefault();
                if (entity != null)
                {
                    this.ddlComercio.SelectedValue = entity.IDTerminal.ToString();
                    this.ddlTipo.SelectedValue = entity.Tipo;
                    this.txtAnterior.Text = entity.Anterior;
                    this.txtNuevo.Text = entity.Nuevo;
                    this.txtObservaciones.Text = entity.Observaciones;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}