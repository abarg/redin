﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Liquidacion-Franquicias.aspx.cs" Inherits="modulos_gestion_Liquidacion_Franquicias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/liquidacion-franquicias.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li>Liquidación de franquicias</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Liquidación de franquicias</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
                        <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            <form runat="server" id="formLiquidacionFranq" class="form-horizontal" role="form">
                <div class="form-group"> 
                     <label class="col-lg-2 control-label"><span class="f_req">*</span> FechaDesde</label>
                          <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                          </div>    
                 </div> 
                <div class="form-group"> 
                     <label class="col-lg-2 control-label"><span class="f_req">*</span> FechaHasta</label>
                          <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                          </div>    
                 </div>     
                 <div class="form-group">  
                     <label  class="col-lg-2 control-label"><span class="f_req">*</span> Franquicia</label>
                     <div class="col-lg-4">
                        <asp:DropDownList runat="server" CssClass="form-control chzn_b required" ID="ddlFranquicias" />
                     </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar</button>
                    </div>
                </div>

                <asp:HiddenField runat="server"  ID="hfArchivo"/>
            </form>
        </div>
    </div>
</asp:Content>

