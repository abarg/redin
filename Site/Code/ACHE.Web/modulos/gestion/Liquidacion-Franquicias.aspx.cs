﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using System.Data;
public partial class modulos_gestion_Liquidacion_Franquicias : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarCombos();
    }

    [System.Web.Services.WebMethod]
    public static void GenerarLiquidacion(int idFranquicia, string fechaDesde, string fechaHasta)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                string sql = "exec Generar_Liquidacion '" + DateTime.Now.ToString(formato) + "','" + DateTime.Parse(fechaDesde).ToString(formato) + "','" + DateTime.Parse(fechaHasta).ToString(formato) + "'," + idFranquicia ;
                dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void cargarCombos()
    {
        try
        {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}