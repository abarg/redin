﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Beneficiose.aspx.cs" Inherits="modulos_gestion_Beneficiose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/beneficiose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <style type="text/css">
        .thumb {
            width: inherit;
            margin: 5px;
            border: solid 1px #ccc;
        }

        .sexo {
            height: 100px;
        }

        input.radio.todos {
            margin-left: 46px;
        }

        input.radio.femenino {
            margin-left: 20px;
        }

        input.radio.masculino {
            margin-left: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Beneficios</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Beneficios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            <label id="lblError" style="color: red; display: none;"></label>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <div class="form-group">
                      <label class="col-sm-4 col-md-4 col-lg-3 control-label"></label>
                    <div class="col-sm-8 col-md-8 col-lg-9">

                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton runat="server" ID="chkEmpresa" ClientIDMode="Static" onclick="changeCheck();" Checked="true" GroupName="Tipo" />&nbsp;Empresa
                                    <asp:RadioButton runat="server" ID="chkComercio" ClientIDMode="Static" onclick="changeCheck();" GroupName="Tipo" />&nbsp;Comercio
                                     <asp:RadioButton runat="server" ID="chkMarca" ClientIDMode="Static" onclick="changeCheck();" GroupName="Tipo" />&nbsp;Marca
                                    <br /><br />
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlMarcas"  ClientIDMode="Static" ></asp:DropDownList>
                                    <asp:DropDownList runat="server" ID="ddlComercios"  CssClass="form-control required" ClientIDMode="Static" ></asp:DropDownList>
                                    <asp:DropDownList runat="server" ID="ddlEmpresas"  CssClass="form-control required"  ClientIDMode="Static" ></asp:DropDownList>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Fecha desde</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control required validDate greaterThan" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                  <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Fecha Hasta</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control required validDate greaterThan" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Sexo</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="imgs">
                            <table>
                                <tr>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/todos.png" id="img4" />
                                            <asp:RadioButton runat="server" Checked="true" ID="rdbTodos" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/femenino.png" id="img5" />
                                            <asp:RadioButton runat="server" ID="rdbFemenino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/masculino.png" id="img6" />
                                            <asp:RadioButton runat="server" ID="rdbMasculino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Edad</label>
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton runat="server" ID="chk0" ClientIDMode="Static" onclick="changeCheck();" Checked="true" GroupName="Edad" />&nbsp;Todas
                                    <asp:RadioButton runat="server" ID="chkSegmentado" ClientIDMode="Static" onclick="changeCheck();" GroupName="Edad" />&nbsp;Segmentado por rango
                                    <br />
                                    <div id="divTodos" style="display: none;margin-top: 20px;">
                                        <asp:CheckBox runat="server" ID="chk1" ClientIDMode="Static" />&nbsp;0-18
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk2" ClientIDMode="Static" />&nbsp;19-25
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk3" ClientIDMode="Static" />&nbsp;26-30
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk4" ClientIDMode="Static" />&nbsp;31-40
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk5" ClientIDMode="Static" />&nbsp;41-50
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk6" ClientIDMode="Static" />&nbsp;51-60
                                        <br />
                                        <asp:CheckBox runat="server" ID="chk7" ClientIDMode="Static" />&nbsp;61+
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mult. Puntos</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMultPuntos" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
             
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();">Guardar</button>
                        <a href="Beneficios.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

