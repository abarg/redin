﻿using System;
using ACHE.Extensions;
using System.Configuration;
using System.Web.Services;
using ACHE.Model;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

public partial class modulos_gestion_msjPushe : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
          cargarCombos();

        }
    }

    [WebMethod(true)]
    public static string obtenerCredencialesFB()
    {
        return ConfigurationManager.AppSettings["FireBase.PrivateKey"];
    }
 
   private void cargarCombos()
   {
        using(var dbContext = new ACHEEntities())
        {

            string sqlMarcas = @"select M.IDMarca as ID, M.Nombre from Marcas as M
                                join Comercios as C on C.IDMarca = M.IDMarca
                                join Terminales as T on T.IDComercio = C.IDComercio
                                where T.POSSistema in ('NEXGO', 'MPOS')
                                group by M.IDMarca, M.Nombre";

            var marcas = dbContext.Database.SqlQuery<Combo>(sqlMarcas).ToList();

            if (marcas != null)
            {
                ddlMarcas.DataSource = marcas;
                ddlMarcas.DataTextField = "Nombre";
                ddlMarcas.DataValueField = "ID";
                ddlMarcas.DataBind();
            }

        }
   }

    [WebMethod(true)]
    public static string persistirNotificaciones(string terminales, string mensaje, string titulo)
    {
        try { 
            if(terminales != null)
            {
                var splitted = terminales.Split(",");

                using (var dbContext = new ACHEEntities())
                {
                    foreach(string terminal in splitted)
                    {
                        int terminalID = int.Parse(terminal);

                        var entity = new NotificacionesFirebase();
                        entity.Mensaje = mensaje;
                        entity.IDTerminal = terminalID;
                        entity.Leido = false;
                        entity.action = "default";
                        entity.title = titulo;
                        entity.FechaAlta = DateTime.Now;

                        dbContext.NotificacionesFirebase.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch(Exception e)
        {
            return "error";
        }

        return "ok";
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> getTerminalesPorMarcas(string marcas)
    {
        List<Combo2ViewModel> listTerminales = new List<Combo2ViewModel>();

        if (marcas != string.Empty)
        {
            var splitted = marcas.Split(",");

            using (var dbContext = new ACHEEntities())
            {

                var sql = @"select CONCAT(T.POSSistema COLLATE Modern_Spanish_CI_AS, ' - ', C.NombreFantasia, ' - ',  T.POSTerminal) as Nombre, T.IDTerminal as ID from Terminales as T
                            join Comercios as C on C.IDComercio = T.IDComercio
                            where T.POSSistema in ('NEXGO', 'MPOS')
                            and C.IDMarca IN ( ";

                for (int i = 0; i < splitted.Length; i++)
                {
                    if (i != splitted.Length - 1)
                        sql += "'" + splitted[i] + "',";
                    else
                        sql += "'" + splitted[i] + "'";

                }



                sql += ");";



                listTerminales = dbContext.Database.SqlQuery<Combo2ViewModel>(sql).ToList();


            }

        }
        else
        {
            listTerminales = null;
        }

        return listTerminales;
    }

    public class Combo
    {
        public string Nombre { get; set; }
        public int ID { get; set; }
    }

}