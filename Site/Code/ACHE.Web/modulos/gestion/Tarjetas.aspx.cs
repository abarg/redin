﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using FileHelpers;
using ClosedXML.Excel;


public partial class modulos_gestion_Tarjetas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarCombos();
    }

    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));

            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    [System.Web.Services.WebMethod]
    public static void CambiarTipo(int id, string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var tarj = dbContext.Tarjetas.Where(x => x.IDTarjeta == id).FirstOrDefault();
                if (tarj != null)
                {
                    var tipoTarjeta="";
                    if (tipo == "Beneficio")
                        tipoTarjeta = "B";
                    else if (tipo == "Giftcard")
                        tipoTarjeta = "G";
                    else
                        tipoTarjeta = "C";
                    tarj.TipoTarjeta=tipoTarjeta;
                    dbContext.SaveChanges();
                }
            }
        }
    }
    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Tarjetas.Include("Franquicias").Include("Marcas")
                    .OrderBy(x => x.IDTarjeta)
                    .Select(x => new TarjetasViewModel()
                    {
                        IDTarjeta = x.IDTarjeta,
                        IDFranquicia = x.IDFranquicia.HasValue ? x.IDFranquicia : 0,
                        IDMarca = x.IDMarca,
                        Marca = x.Marcas.Nombre != null && x.Marcas.Nombre != "" ? x.Marcas.Nombre : "",
                        Franquicia = x.IDFranquicia.HasValue ? x.Franquicias.NombreFantasia : "",
                        Numero = x.Numero,
                        FechaAlta = x.FechaAlta,
                        FechaEmision = x.FechaEmision ?? DateTime.MinValue,
                        FechaVencimiento = x.FechaVencimiento,
                        FechaBaja = x.FechaBaja,
                        Puntos = x.PuntosTotales,
                        Credito = x.Credito,
                        Giftcard = x.Giftcard,
                        POS = x.Credito + x.Giftcard,
                        Tipo=(x.TipoTarjeta=="B")?"Beneficio":(x.TipoTarjeta=="G")?"Giftcard":"CuponIN",
                        Total = x.Credito + x.Giftcard
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                bTarjeta bTarjeta = new bTarjeta();
                bTarjeta.deleteTarjeta(id);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static void Activar(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var tar = dbContext.Tarjetas.Where(x => x.IDTarjeta == id).FirstOrDefault();
                    if (tar != null)
                    {
                        tar.FechaBaja = null;
                        tar.Estado = "A";
                        dbContext.SaveChanges();
                    }


                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string numero, int idMarca, int idFranquicia,string tipo)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {

                var tarjetas = dbContext.Tarjetas.Include("Marcas").Include("Franquicias")
                    .OrderBy(x => x.IDTarjeta)
                    .Select(x => new
                    {
                        IDTarjeta = x.IDTarjeta,

                        IDMarca = x.IDMarca,
                        IDFranquicia = x.IDFranquicia ?? 0,
                        Marca = x.Marcas.Nombre,
                        Franquicia = x.IDFranquicia.HasValue ? x.Franquicias.NombreFantasia : "",
                        Numero = x.Numero,
                        FechaAlta = x.FechaAlta,
                        FechaEmision = x.FechaEmision,
                        FechaVencimiento = x.FechaVencimiento,
                        FechaBaja = x.FechaBaja,
                        Puntos = x.PuntosTotales,
                        Credito = x.Credito,
                        Giftcard = x.Giftcard,
                        POS = x.Credito + x.Giftcard,
                        Tipo = (x.TipoTarjeta == "B") ? "Beneficio" : (x.TipoTarjeta == "G") ? "Giftcard" : "CuponIN",

                        Total = x.Credito + x.Giftcard
                    }).AsEnumerable();

                if (!string.IsNullOrEmpty(numero))
                    tarjetas = tarjetas.Where(x => x.Numero.Contains(numero));
                if (idMarca > 0)
                    tarjetas = tarjetas.Where(x => x.IDMarca == idMarca);
                if (idFranquicia > 0)
                    tarjetas = tarjetas.Where(x => x.IDFranquicia == idFranquicia);

                if (!string.IsNullOrEmpty(tipo))
                {
                    /*
                    var tipoTarjeta = "";
                    if (tipo == "Beneficio")
                        tipoTarjeta = "B";
                    else if (tipo == "Giftcard")
                        tipoTarjeta = "G";
                    else
                        tipoTarjeta = "C";*/
                    tarjetas = tarjetas.Where(x => x.Tipo == tipo);

                }
                #region header y footer
                string header = "IDTarjeta;Franquicia;Marca;Numero;FechaAlta;FechaEmision;FechaVencimiento;FechaBaja;Puntos;Credito;Giftcard;POS;Total;Tipo";
                //string fechaHoy = DateTime.Now.ToString("yyyyMMdd");
                #endregion


                #region detalle
                List<TarjetaFHExport> listaTarjetas = new List<TarjetaFHExport>();

                foreach (var tarj in tarjetas)
                {
                    TarjetaFHExport tarjeta = new TarjetaFHExport();
                    tarjeta.IDTarjeta = tarj.IDTarjeta;
                    //tarjeta.IDFranquicia = tarj.IDFranquicia;
                    //tarjeta.IDMarca = tarj.IDMarca;
                    tarjeta.Marca = tarj.Marca;
                    tarjeta.Franquicia = tarj.Franquicia;
                    tarjeta.Numero = "=\"" + tarj.Numero + "\"";
                    tarjeta.FechaAlta = tarj.FechaAlta.ToString("dd/MM/yyyy");
                    tarjeta.FechaEmision = tarj.FechaEmision.HasValue ? tarj.FechaEmision.Value.ToString("dd/MM/yyyy") : "";
                    tarjeta.FechaVencimiento = tarj.FechaVencimiento.ToString("dd/MM/yyyy");
                    tarjeta.FechaBaja = tarj.FechaBaja.HasValue ? tarj.FechaBaja.Value.ToString("dd/MM/yyyy") : "";
                    tarjeta.Puntos = tarj.Puntos;
                    tarjeta.Credito = tarj.Credito;
                    tarjeta.Giftcard = tarj.Giftcard;
                    tarjeta.POS = tarj.POS;
                    tarjeta.Total = tarj.Total;
                    tarjeta.Tipo = tarj.Tipo;

                    listaTarjetas.Add(tarjeta);
                }
                #endregion

                string path = HttpContext.Current.Server.MapPath("/files/tarjetas/tarjetas.csv");
                var engine = new DelimitedFileEngine(typeof(TarjetaFHExport))
                {
                    HeaderText = header
                };
                engine.Options.Delimiter = ";";
                engine.WriteFile(path, listaTarjetas);
                return path;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    [System.Web.Services.WebMethod]
    public static string ExportarCalcupan(string numero, int idMarca, int idFranquicia,string tipo)
    {
        try
        {
            string fileName = "Calcupan";
            string path = "/tmp/";

            using (var dbContext = new ACHEEntities())
            {

                int nroTarjeta = 0;
                var tarjetas = dbContext.Tarjetas.Include("Marcas").Include("Franquicias")
                    .OrderBy(x => x.IDTarjeta)
                    .Select(x => new
                    {
                        PAN = x.Numero,
                        Trak1 = "B" + x.Numero + "                   491210100000",
                        Trak2 = "" + x.Numero + "=491210100000",
                        IDMarca = x.IDMarca,
                        IDFranquicia = x.IDFranquicia ?? 0,
                        Tipo=x.TipoTarjeta
                    }).AsEnumerable();

                if (!string.IsNullOrEmpty(numero))
                    tarjetas = tarjetas.Where(x => x.PAN.Contains(numero));
                if (idMarca > 0)
                    tarjetas = tarjetas.Where(x => x.IDMarca == idMarca);
                if (idFranquicia > 0)
                    tarjetas = tarjetas.Where(x => x.IDFranquicia == idFranquicia);

                if (!string.IsNullOrEmpty(tipo))
                {
                    var tipoTarjeta = "";
                    if (tipo == "Beneficio")
                        tipoTarjeta = "B";
                    else if (tipo == "Giftcard")
                        tipoTarjeta = "G";
                    else
                        tipoTarjeta = "C";
                    tarjetas = tarjetas.Where(x => x.Tipo == tipoTarjeta);

                }


                #region detalle
                List<TarjetaCalcupan> listaTarjetas = new List<TarjetaCalcupan>();

                foreach (var tarj in tarjetas)
                {
                    TarjetaCalcupan tarjeta = new TarjetaCalcupan();
                    tarjeta.Numero = ' ' + (++nroTarjeta).ToString("#00000");
                    tarjeta.PAN = ' ' + tarj.PAN;
                    tarjeta.PANFrente = tarj.PAN.Substring(0, 4) + " " + tarj.PAN.Substring(4, 4) + " " + tarj.PAN.Substring(8, 4) + " " + tarj.PAN.Substring(12, 4);
                    tarjeta.Trak1 = tarj.Trak1;
                    tarjeta.Trak2 = tarj.Trak2;
                    listaTarjetas.Add(tarjeta);
                }
                #endregion

                DataTable dt = new DataTable();
                dt = listaTarjetas.Select(x => new
                {
                    Numero = x.Numero,
                    PAN = x.PAN,
                    PANFrente = x.PANFrente,
                    Trak1 = x.Trak1,
                    Trak2 = x.Trak2
                }).ToList().ToDataTable();

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}



