﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class modulos_gestion_posweb : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarSocios(string tipo, string valor)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.SociosView
                                .Where(x => x.Nombre.ToLower().Contains(valor.ToLower()) || x.Apellido.ToLower().Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "DNI":
                            list = dbContext.SociosView
                                .Where(x => x.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Apellido)
                                .Select(x => new Combo3ViewModel() { ID = x.IDSocio, Nombre = x.Apellido + ", " + x.Nombre })
                                .Distinct().Take(10).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "Tarjeta":
                            list = dbContext.Tarjetas
                                .Where(x => x.Numero.Contains(valor) && x.TipoTarjeta != "G")
                                .OrderBy(x => x.Numero)
                                .Select(x => new Combo3ViewModel()
                                {
                                    ID = x.IDSocio.HasValue ? x.IDSocio.Value : 0,
                                    Nombre = x.IDSocio.HasValue ? x.Socios.Apellido + ", " + x.Socios.Nombre : x.Numero,
                                    Importe = x.Credito.ToString()

                                }).Distinct().Take(10).OrderBy(x => x.Nombre).ToList();
                            break;
                    }
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo2ViewModel> buscarComercios(string tipo, string valor)
    {
        try
        {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    switch (tipo)
                    {
                        case "Nombre":
                            list = dbContext.Terminales.Include("Comercios")
                                .Where(x => x.Activo && x.Comercios.NombreFantasia.ToLower().Contains(valor.ToLower()) || x.Comercios.RazonSocial.ToLower().Contains(valor.ToLower()))
                                .OrderBy(x => x.Comercios.NombreFantasia)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTerminal, Nombre = x.POSTerminal + " - " + x.Comercios.NombreFantasia })
                                .Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "CUIT":
                            list = dbContext.Terminales.Include("Comercios")
                                .Where(x => x.Activo && x.Comercios.NroDocumento.Contains(valor.ToLower()))
                                .OrderBy(x => x.Comercios.NombreFantasia)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTerminal, Nombre = x.POSTerminal + " - " + x.Comercios.NombreFantasia })
                                .Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "SDS":
                            list = dbContext.Terminales.Include("Comercios")
                                .Where(x => x.Activo && x.Comercios.SDS.Contains(valor.ToLower()))
                                .OrderBy(x => x.Comercios.NombreFantasia)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTerminal, Nombre = x.POSTerminal + " - " + x.Comercios.NombreFantasia })
                                .Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "Terminal":
                            list = dbContext.Terminales.Include("Comercios")
                                .Where(x => x.Activo && x.POSTerminal.Contains(valor.ToLower()))
                                .OrderBy(x => x.Comercios.NombreFantasia)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTerminal, Nombre = x.POSTerminal + " - " + x.Comercios.NombreFantasia })
                                .Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                        case "ID":
                            list = dbContext.Terminales.Include("Comercios")
                                .Where(x => x.Activo && x.NroNumEst.ToLower() == valor.ToLower())
                                .OrderBy(x => x.Comercios.NombreFantasia)
                                .Select(x => new Combo2ViewModel() { ID = x.IDTerminal, Nombre = x.POSTerminal + " - " + x.Comercios.NombreFantasia })
                                .Take(20).OrderBy(x => x.Nombre).ToList();
                            break;
                    }

                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static List<Combo3ViewModel> buscarTarjetas(int socio)
    {
        try
        {
            List<Combo3ViewModel> list = new List<Combo3ViewModel>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var dtHoy = DateTime.Now.Date;
                
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Tarjetas.Where(x => x.IDSocio == socio && x.TipoTarjeta != "G"
                        && (x.FechaBaja == null || (x.FechaBaja.HasValue && x.FechaBaja.Value > dtHoy)))
                     .OrderBy(x => x.Numero)
                     .Select(x => new Combo3ViewModel() { ID = (int)(x.Credito * 100), Nombre = x.Numero, Importe = x.Credito.ToString() })
                     .Distinct().OrderBy(x => x.Nombre).ToList();
                }
            }

            return list;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string grabar(string tipo, string tarjeta, int idTerminal, string importe, string descripcion,
        string puntoVenta, string tipoComprobante, string nroComprobante)//string idPremio, 
    {

        string idTransaccion = "";
        decimal currentImporte = decimal.Parse(importe);

        if (currentImporte == 0)
            throw new Exception("El importe debe ser mayor a $0");

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];
                    idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, idTerminal, "Web", "", descripcion, currentImporte, "", "", "", "", tarjeta, "", tipo, "000000000000", "1100", puntoVenta, nroComprobante, tipoComprobante, "A-" + usu.Nombre);
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        return idTransaccion;
    }

    [System.Web.Services.WebMethod(true)]
    public static ComboViewModel obtenerInfoSocio(int id)
    {
        try
        {
            ComboViewModel info = new ComboViewModel();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Socios.Where(x => x.IDSocio == id).FirstOrDefault();
                    if (aux != null)
                    {
                        info.ID = aux.NroDocumento == "00" ? "No disponible" : aux.NroDocumento;
                        info.Nombre = aux.Foto ?? "";
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string anularOperacion(int id)
    {
        string idTransaccion = "";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {

                using (var dbContext = new ACHEEntities())
                {

                    var aux = dbContext.Transacciones.Where(x => x.IDTransaccion == id && (x.Operacion == "Venta" || x.Operacion == "Canje")).FirstOrDefault();
                    if (aux == null)
                        throw new Exception("El ID ingresado es inexistente o no corresponde a una venta/canje.");

                    var usu = (Usuarios)HttpContext.Current.Session["CurrentUser"];


                    int IDTerminal = dbContext.Terminales.Where(x => x.POSTerminal == aux.Terminales.POSTerminal).FirstOrDefault().IDTerminal;

                    idTransaccion = ACHE.Business.Common.CrearTransaccion(dbContext, aux.FechaTransaccion, IDTerminal, "Web", "", "Anulacion via web de ID " + id, aux.Importe,
                  "", "", "", aux.IDTransaccion.ToString(), aux.NumTarjetaCliente, "", "Anulacion", aux.PuntosDisponibles, "1100", aux.PuntoDeVenta, "", "", "A-" + usu.Usuario);

                }

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        return idTransaccion;
    }

    [System.Web.Services.WebMethod(true)]
    public static TransaccionesViewModel buscarTrParaAnular(int id)
    {
        try
        {
            TransaccionesViewModel info = new TransaccionesViewModel();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.TransaccionesView.Where(x => x.IDTransaccion == id && (x.Operacion == "Venta" || x.Operacion == "Canje")).FirstOrDefault();
                    if (aux == null)
                        throw new Exception("El ID ingresado es inexistente o no corresponde a una venta/canje.");
                    else
                    {
                        info.Puntos = aux.PuntosAContabilizar ?? 0;
                        info.Tarjeta = aux.Numero;
                        info.ImporteOriginal = aux.Ticket ?? 0;
                        info.Fecha = aux.FechaTransaccion.ToString("dd/MM/yyyy");
                        info.Hora = aux.FechaTransaccion.ToString("HH:mm:ss");
                        info.Socio = aux.Nombre + ", " + aux.Apellido;
                        info.Comercio = aux.NombreFantasia;
                        info.Origen = aux.Origen;
                        info.Operacion = aux.Operacion;
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            //var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
    [System.Web.Services.WebMethod(true)]
    public static string getInfoMarcaPorComercio(int id)
    {
        try
        {
            string info = "";
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var comercio = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                    if (comercio != null && comercio.IDMarca != null)
                    {
                        var aux = dbContext.Marcas.Where(x => x.IDMarca == comercio.IDMarca).FirstOrDefault();
                        if (aux != null)
                        {
                            info = aux.PuntoDeVenta + "#" + aux.NroComprobante + "#" + aux.TipoComprobante;
                        }
                    }
                }
            }

            return info;
        }
        catch (Exception e)
        {
            //var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}