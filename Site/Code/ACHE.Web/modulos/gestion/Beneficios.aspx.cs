﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Model;


public partial class modulos_gestion_Beneficios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Beneficios.OrderBy(x => x.FechaDesde).ToList();


                if (fechaDesde != string.Empty)
                {
                    var dtDesde = DateTime.Parse(fechaDesde);
                    list = list.Where(x => x.FechaDesde >= dtDesde).ToList();
                }

                if (fechaHasta != string.Empty)
                {
                    var dtHasta = DateTime.Parse(fechaHasta);
                    list = list.Where(x => x.FechaHasta <= dtHasta).ToList();
                }



                return list.Select(x => new BeneficiosViewModel()
                {
                    IDBeneficio = x.IDBeneficio,
                    fechaDesde = x.FechaDesde,
                    fechaHasta = x.FechaHasta,
                    Sexo = x.Sexo,
                    Edad = x.Edad,
                    Nombre =(x.IDEmpresa.HasValue)?"Empresa: "+ x.Empresas.Nombre:((x.IDComercio.HasValue) ?"Comercio: "+x.Comercios.NombreFantasia:"Marca: "+ x.Marcas.Nombre),
                 
                    mulPuntos = x.MultiplicaPuntos
               
                }).AsQueryable().ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var b = dbContext.Beneficios.Where(x => x.IDBeneficio == id).FirstOrDefault();
                    if (b != null)
                    {
                        dbContext.Beneficios.Remove(b);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }
}