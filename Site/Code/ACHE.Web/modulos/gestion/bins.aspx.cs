﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Configuration;

public partial class modulos_gestion_bins : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.BIN
                    .OrderBy(x => x.Nombre)
                    .Select(x => new BINViewModel(){
                        IDBIN = x.IDBIN,
                        Nombre = x.Nombre,
                        Numero = x.Numero
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.BIN.Where(x => x.IDBIN == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.BIN.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}