﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.Configuration;
using System.Data;
using System.IO;


public partial class modulos_gestion_HistorialTermEstab : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.HistorialTerminales.Include("Terminales")
                    .OrderByDescending(x => x.FechaBaja)
                    .Select(x => new HistorialTerminalesViewModel()
                    {
                        ID = x.IDHistorial,
                        IDComercio = x.Terminales.IDComercio,
                        NombreFantasia = x.Terminales.Comercios.NombreFantasia,
                        Tipo = x.Tipo == "T" ? "Terminal" : "Establecimiento",
                        Anterior = x.Anterior,
                        Nuevo = x.Nuevo,
                        RazonSocial = x.Terminales.Comercios.RazonSocial,
                        Observaciones = x.Observaciones,
                        SDS = x.Terminales.Comercios.SDS,
                        FechaBaja = x.FechaBaja
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                bHistorialTerminales bHistorialTerminales = new bHistorialTerminales();
                bHistorialTerminales.deleteHistorial(id);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string SDS, string Nombre, string Terminal) {

        string fileName = "HistorialTerminal";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {


                    var info = dbContext.HistorialTerminales.Include("Terminales")
                            .OrderByDescending(x => x.FechaBaja)
                            .Select(x => new HistorialTerminalesViewModel() {
                                ID = x.IDHistorial,
                                IDComercio = x.Terminales.IDComercio,
                                NombreFantasia = x.Terminales.Comercios.NombreFantasia,
                                Tipo = x.Tipo == "T" ? "Terminal" : "Establecimiento",
                                Anterior = x.Anterior,
                                Nuevo = x.Nuevo,
                                RazonSocial = x.Terminales.Comercios.RazonSocial,
                                Observaciones = x.Observaciones,
                                SDS = x.Terminales.Comercios.SDS,
                                FechaBaja = x.FechaBaja
                            });

                    if (SDS != "")
                        info = info.Where(x => x.SDS.ToLower().Contains(SDS.ToLower()));
                    if (Nombre != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(Nombre.ToLower()));
                    if (Terminal != "")
                        info = info.Where(x => x.Anterior.ToLower().Contains(Terminal.ToLower()));


                    dt = info.ToList().Select(x => new {
                        NombreFantasia = x.NombreFantasia,
                        Tipo = x.Tipo,
                        Anterior = x.Anterior,
                        Nuevo = x.Nuevo,
                        RazonSocial = x.RazonSocial,
                        Observaciones = x.Observaciones,
                        SDS = x.SDS,
                        FechaBaja = x.FechaBaja
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}