﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="bine.aspx.cs" Inherits="modulos_gestion_bine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <style type="text/css">
        .bar {
            height: 18px;
            background: green;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/gestion/bins.aspx") %>">BIN</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-10 col-md-10">
            <h3 class="heading" id="litTitulo">Edición de BIN</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <br />
                    <asp:HiddenField runat="server" ID="hdnIDBIN" Value="0" />
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                        <div class="col-lg-3">
                            <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="30" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><span class="f_req">*</span> Numero</label>
                        <div class="col-lg-2">
                            <asp:TextBox runat="server" ID="txtNumero" CssClass="form-control numeric required" MaxLength="6" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                            <a href="bins.aspx" class="btn btn-link">Cancelar</a>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/bine.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>