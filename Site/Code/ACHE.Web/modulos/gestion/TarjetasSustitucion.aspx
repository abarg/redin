﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TarjetasSustitucion.aspx.cs" Inherits="modulos_gestion_TarjetasSustitucion" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/tarjetasSustitucion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Gestión</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/gestion/tarjetas.aspx") %>">Tarjetas</a></li>
                <li>Anular/Sustituir Tarjetas</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Anular/Sustituir Tarjetas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formSusTarjeta" class="form-horizontal" role="form">
               
               <div class="form-group">
                    <label for="rdbAnulacion" class="col-lg-2 control-label">Método</label>
                    <div class="col-lg-4">
                        <select id="metodo" class="form-control">
                            <option value="individual">Individual</option>
                            <option value="masiva">Masiva</option>
                        </select>
                    </div>
                </div> 
                
                
                <div class="form-group">
                    <label for="rdbAnulacion" class="col-lg-2 control-label">Tipo</label>
                    <div class="col-lg-10">
                        <label class="radio-inline">
                            <asp:RadioButton runat="server" ID="rdbAnulacion" GroupName="grpTipo" Checked="true" Text="Anulación" Width="80px" />
                        </label>
                        <label class="radio-inline">
                            <asp:RadioButton runat="server" ID="rdbSustitucion" GroupName="grpTipo" Checked="false" Text="Sustitución" Width="80px" />
                        </label>
                        <label class="radio-inline">
                            <asp:RadioButton runat="server" ID="rdbDesvinculacion" GroupName="grpTipo" Checked="false" Text="Desvinculación" Width="80px" />
                        </label>
                    </div>
                </div>

                <div >

                        <div class="form-group" id="buscarPor">                            
                            <label class="col-lg-2 control-label"><span class="f_req ">*</span> Buscar por</label>
                             <div class="col-lg-4">
                                <select runat="server" id="ddlTrBuscarSocio" class="form-control required">
                                    <option value="Tarjeta">Nro Tarjeta</option>
                                    <option value="Nombre">Nombre y apellido</option>
                                    <option value="DNI">DNI</option>
                                </select>                                         
                            </div>
                        </div>

                     <div class="form-group" id="buscarPorTarjeta" hidden>                            
                            <label class="col-lg-2 control-label"><span class="f_req ">*</span> Buscar por</label>
                             <div class="col-lg-4">
                                <select runat="server" id="Select1" class="form-control required">
                                    <option value="Tarjeta">Nro Tarjeta</option>
                                </select> 
                                 Solo se permite busqueda por tarjeta si se utiliza la opcion masiva
                            </div>
                        </div>
                    

                        <div class="form-group" id="inputIndividual"> 
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Valor a buscar</label>
                            <div class="col-lg-4">
                                <input runat="server" type="text" id="txtValorSocio" class="form-control required" MaxLength="20"/>
                            </div>
                        </div>

                       <div class="form-group" id="inputMasivo" hidden> 
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Valores a buscar</label>
                            <div class="col-lg-4">
                                <textarea id="txtMasiva" cols="40" rows="5"></textarea>
                                Valores separados por coma ej: 6371171999151316,6371171999018838,
                                6371171999172122
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnBuscarSocio" class="btn" type="button" onclick="buscarSocios();">Buscar</button>
                                <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />  
                            </div>
                        </div>

                        
                        <div class="form-group datosMasiva" hidden>
                            <div class="col-sm-8 col-sm-offset-2" >

                                 <a href="" id="lnkDownload" download="Socios" >Descargar</a> listado de tarjetas encontradas a anular/desvincular, una vez verificado, eliga un motivo y luego presione el boton "Grabar"
                            
                            </div>
                        </div>

                        <div class="form-group datosMasiva" style="display:none">
                            <label for="ddlMotivo" class="col-lg-2 control-label"><span class="f_req">*</span> Motivo</label>
                            <div class="col-lg-4">
                                <asp:DropDownList runat="server" ID="ddlMotivoMasiva" CssClass="form-control required">
                                    <asp:ListItem Value="" Text="" />
                                    <asp:ListItem Value="1" Text="Robo" />
                                    <asp:ListItem Value="2" Text="Extravio" />
                                    <asp:ListItem Value="3" Text="Anulacion" />
                                    <asp:ListItem Value="5" Text="Error interno" />
                                    <asp:ListItem Value="6" Text="Carga Fraudulenta" />
                                    <asp:ListItem Value="7" Text="Faltante de stock" />
                                    <asp:ListItem Value="8" Text="Error de solicitud" />
                                    <asp:ListItem Value="9" Text="Cancela el cliente" />
                                    <asp:ListItem Value="4" Text="Otro" />
                                </asp:DropDownList>
                            </div>  
                        </div>

                       <div class="form-group datosMasiva" style="display:none">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="Button1" class="btn btn-success" type="button" onclick="grabarMasiva();">Grabar</button>
                                <a class="btn btn-link" href="tarjetas.aspx">Cancelar</a>
                            </div>
                        </div>


                        <div class="form-group datos2" style="display:none">  
                                                    
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Socio</label>
                            <div class="col-lg-4">
                                <select runat="server" id="ddlTrSocio" class="form-control required" onchange="buscarTarjetas();">
                                </select>                                         
                            </div>
                        </div>

                        <div class="form-group datos2" style="display:none">                            
                            <label class="col-lg-2 control-label"><span class="f_req">*</span> Tarjeta</label>.
                            <div class="col-lg-4">
                                <select runat="server" id="ddlTrTarjeta" class="form-control required" onchange="buscarMarca();" >                                            
                                </select>                                         
                            </div>
                        </div>                      
                   </div>

                <div class="form-group datos2" style="display:none">
                    <label for="ddlMotivo" class="col-lg-2 control-label"><span class="f_req">*</span> Motivo</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="ddlMotivo" CssClass="form-control required">
                            <asp:ListItem Value="" Text="" />
                            <asp:ListItem Value="1" Text="Robo" />
                            <asp:ListItem Value="2" Text="Extravio" />
                            <asp:ListItem Value="3" Text="Anulacion" />
                            <asp:ListItem Value="5" Text="Error interno" />
                            <asp:ListItem Value="6" Text="Carga Fraudulenta" />
                            <asp:ListItem Value="7" Text="Faltante de stock" />
                            <asp:ListItem Value="8" Text="Error de solicitud" />
                            <asp:ListItem Value="9" Text="Cancela el cliente" />
                            <asp:ListItem Value="4" Text="Otro" />
                        </asp:DropDownList>
                    </div>  
                </div>

                <div id="divSustitucion" style="display: none;">
                    <div class="form-group">
                        <label for="ddlMarcas" class="col-lg-2 control-label"><span class="f_req">*</span> Marca</label>
                        <div class="col-lg-4">
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtNumeroNuevo" class="col-lg-2 control-label"><span class="f_req">*</span> Nuevo Número</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtNumeroNuevo" CssClass="form-control required number" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="form-group datos2" style="display:none">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a class="btn btn-link" href="tarjetas.aspx">Cancelar</a>
                    </div>
                </div>

                <asp:HiddenField runat="server" ID="hfIDSocio" />
                <asp:HiddenField runat="server" ID="hfIDTarjeta" />
            </form>
        </div>
    </div>
</asp:Content>

