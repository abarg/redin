﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistorialTermEstabe.aspx.cs" Inherits="modulos_gestion_HistorialTermEstabe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />   
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/historialTermEstabe.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/gestion/HistorialTermEstab.aspx") %>">Historial de terminales/establecimientos</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Edición de historial de terminales/establecimientos</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Comercio</label>
                    <div class="col-lg-8">
                         <asp:DropDownList runat="server" ID="ddlComercio" CssClass="chzn_b form-control required" 
                             data-placeholder="Seleccione un comercio"
                             DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Tipo de cambio</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Value="T" Text="Terminal"></asp:ListItem>
                            <asp:ListItem Value="E" Text="Establecimiento"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Anterior</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtAnterior" CssClass="form-control required" MaxLength="20" MinLength="8"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*</span> Nuevo</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtNuevo" CssClass="form-control required" MaxLength="20" MinLength="8"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Observaciones</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtObservaciones" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Aceptar</button>
                        <a href="HistorialTermEstab.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

                <asp:HiddenField runat="server" ID="hfID" Value="0" />
            </form>
        </div>
    </div>
</asp:Content>

