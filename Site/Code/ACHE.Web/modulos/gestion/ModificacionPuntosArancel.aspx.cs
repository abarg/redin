﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class modulos_gestion_ModificacionPuntosArancel : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            cargarCombos();
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {

            bMarca bMarca = new bMarca();
            this.ddlMarcas.DataSource = bMarca.getMarcas();
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));

        }
    }

    [WebMethod(true)]
    public static int actualizarPuntosArancel(string idComercio, string fechaDesde, string fechaHasta, string puntos,
        string arancel, int idMarca, string operacion, string costoRed, string origen,string puntosVip,int multPuntos,int multPuntosVip,string arancelVip)
    {
        try
        {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            DateTime fechaHoy = DateTime.Now;
            DateTime fDesde = DateTime.Parse(fechaDesde);
            DateTime fHasta = DateTime.Parse(fechaHasta);

            if (fDesde.Date > DateTime.Now.Date)
                throw new Exception("La Fecha Desde debe ser menor o igual a hoy");

            if(idComercio=="" && idMarca==0)
                throw new Exception("Debe ingresar una marca o un comercio");
            //var dif = (fechaHoy - fDesde).TotalDays;
            //if (dif > 60 || dif < 0)
            //    throw new Exception("La Fecha Desde debe estar dentro de los ultimos 45 dias");
            int result = 0;
           
            using (var dbContext = new ACHEEntities())
            {
                string numEst = "";
                string numTerminal = "";
                decimal pts = decimal.Parse(puntos.Replace(".", ",")) * multPuntos;
                decimal ptsVip = decimal.Parse(puntosVip.Replace(".", ",")) * multPuntosVip;

                if (idComercio != "")
                {
                    int id = int.Parse(idComercio);
                    //var com = dbContext.Comercios.Where(x => x.IDComercio == id).FirstOrDefault();
                    var terminales = dbContext.Terminales.Where(x => x.IDComercio == id).ToList();
                    //numEst = com.NumEst.ToString();
                    //numTerminal = com.POSTerminal.ToString();

                   
                    foreach (var terminal in terminales)
                    {
                        numEst = terminal.NumEst;
                        numTerminal = terminal.POSTerminal.ToString();
                        var sql = "exec ModificacionPuntosArancel '" + fDesde.ToString(formato) + "','" + fHasta.ToString(formato) + "'," + idMarca + ",'" + numEst + "','" + numTerminal + "','" + operacion + "','" + origen + "'," + pts.ToString().Replace(",", ".") + "," + ptsVip.ToString().Replace(",", ".") + "," + arancel + "," + arancelVip + "," + costoRed.Replace(",", ".") + "," + multPuntos + "," + multPuntosVip;
                        result = dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
                    }
                }
                else 
                {
                    var sql = "exec ModificacionPuntosArancel '" + fDesde.ToString(formato) + "','" + fHasta.ToString(formato) + "'," + idMarca + ",'" + numEst + "','" + numTerminal + "','" + operacion + "','" + origen + "'," + pts.ToString().Replace(",", ".") + "," + ptsVip.ToString().Replace(",", ".") + "," + arancel + "," + arancelVip + "," + costoRed.Replace(",", ".") + "," + multPuntos + "," + multPuntosVip;
                    result = dbContext.Database.ExecuteSqlCommand(sql, new object[] { });
                }
            }
            return result;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}