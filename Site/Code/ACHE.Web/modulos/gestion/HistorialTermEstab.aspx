﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistorialTermEstab.aspx.cs" Inherits="modulos_gestion_HistorialTermEstab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/gestion/historialTermEstab.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Gestión</a></li>
            <li class="last">Historial de terminales/establecimientos</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de historial de terminales/establecimientos</h3>
            <form id="formTarjeta" runat="server">
                <div class="formSep col-sm-8 col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <label>SDS</label>
                            <asp:TextBox runat="server" ID="txtSDS" CssClass="form-control" MaxLength="10" />
                        </div>
                        <div class="col-md-4">
                            <label>Nombre</label>
                            <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-4">
                            <label>Terminal/Establecimiento anterior</label>
                            <asp:TextBox runat="server" ID="txtTerminal" CssClass="form-control" MaxLength="50" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Tickets" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

