﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Business;



public partial class modulos_gestion_Beneficiose : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                 if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }




            rdbTodos.InputAttributes["class"] = "radio todos";
            rdbFemenino.InputAttributes["class"] = "radio femenino";
            rdbMasculino.InputAttributes["class"] = "radio masculino";

            chk0.InputAttributes.Add("value", "0");
            chk1.InputAttributes.Add("value", "1");
            chk2.InputAttributes.Add("value", "2");
            chk3.InputAttributes.Add("value", "3");
            chk4.InputAttributes.Add("value", "4");
            chk5.InputAttributes.Add("value", "5");
            chk6.InputAttributes.Add("value", "6");
            chk7.InputAttributes.Add("value", "7");
        }
    }


    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Beneficios.Where(x => x.IDBeneficio == id).FirstOrDefault();
            if (entity != null)
            {

                txtMultPuntos.Text = entity.MultiplicaPuntos.ToString();
                txtFechaHasta.Text = entity.FechaHasta.Value.ToString("dd/MM/yyyy");
                txtFechaDesde.Text = entity.FechaDesde.Value.ToString("dd/MM/yyyy");
                if (entity.IDComercio.HasValue)
                {
                    ddlComercios.SelectedValue = entity.IDComercio.ToString();
                    chkComercio.Checked = true;
                }else if (entity.IDEmpresa.HasValue)
                {
                    ddlEmpresas.SelectedValue = entity.IDEmpresa.ToString();
                    chkEmpresa.Checked = true;
                }
                else
                {
                    ddlMarcas.SelectedValue = entity.IDMarca.ToString();
                    chkMarca.Checked = true;
                }
                #region sexo
                rdbTodos.Checked = false;
                rdbFemenino.Checked = false;
                rdbMasculino.Checked = false;

                switch (entity.Sexo)
                {
                    case "T":
                        rdbTodos.Checked = true;
                        break;
                    case "F":
                        rdbFemenino.Checked = true;
                        break;
                    case "M":
                        rdbMasculino.Checked = true;
                        break;
                }
                #endregion

                #region edad

                if (entity.Edad == "Todas")
                {
                    chk0.Checked = true;
                    chkSegmentado.Checked = false;
                }
                else
                {
                    chk0.Checked = false;
                    chkSegmentado.Checked = true;

                    if (entity.Edad.Contains("0-18"))
                        chk1.Checked = true;
                    if (entity.Edad.Contains("19-25"))
                        chk2.Checked = true;
                    if (entity.Edad.Contains("26-30"))
                        chk3.Checked = true;
                    if (entity.Edad.Contains("31-40"))
                        chk4.Checked = true;
                    if (entity.Edad.Contains("41-50"))
                        chk5.Checked = true;
                    if (entity.Edad.Contains("51-60"))
                        chk6.Checked = true;
                    if (entity.Edad.Contains("61+"))
                        chk7.Checked = true;
                }

                #endregion

               
            }
        }
    }

    private void cargarCombos()
    {
        try
        {

            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas  = bMarca.getMarcas();

            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));
            using (var dbContext = new ACHEEntities())
            {
                var empresas = dbContext.Empresas.OrderBy(x=>x.Nombre).ToList();
                ddlEmpresas.DataSource = empresas;
                ddlEmpresas.DataValueField = "IDEmpresa";
                ddlEmpresas.DataTextField = "Nombre";
                ddlEmpresas.DataBind();
                  this.ddlEmpresas.Items.Insert(0, new ListItem("", ""));

                  var comercios = dbContext.Comercios.OrderBy(x => x.NombreFantasia).ToList();

                ddlComercios.DataSource = comercios;
                ddlComercios.DataValueField = "IDComercio";
                ddlComercios.DataTextField = "NombreFantasia";
                ddlComercios.DataBind();
                this.ddlComercios.Items.Insert(0, new ListItem("", ""));
            }
          
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod(true)]
    public static int guardar(int id, string tipo, int idTipo, string fechaDesde, string fechaHasta, string sexo, string edad, int multPuntos)
    {
        int idGenerado = 0;
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {

                using (var dbContext = new ACHEEntities())
                {
                    var fdesde= DateTime.Parse(fechaDesde);
                    var fhasta = DateTime.Parse(fechaHasta);
                   
                    Beneficios entity;
                    if (id > 0)
                        entity = dbContext.Beneficios.Where(x => x.IDBeneficio == id).FirstOrDefault();
                    else
                    {
                        var valid = dbContext.Beneficios.Any(x => (x.FechaDesde <= fdesde || x.FechaHasta >= fdesde || x.FechaDesde <= fhasta || x.FechaHasta >= fhasta) && (x.IDComercio == idTipo || x.IDEmpresa == idTipo || x.IDMarca == idTipo) && ((x.Sexo == "T" || x.Sexo == sexo) && (( x.Edad.Contains(edad))) || x.Edad == "Todas"));
                        if (valid)
                            throw new Exception("Ya existe un beneficio con esas condiciones para esa fecha");
                        entity = new Beneficios();
                    }
                    if (tipo == "empresa")
                    {
                        entity.IDEmpresa = idTipo;
                        entity.IDComercio = null;
                        entity.IDMarca = null;
                    }
                    else if (tipo == "comercio")
                    {
                        entity.IDComercio = idTipo;
                        entity.IDMarca = null;
                        entity.IDEmpresa = null;
                    }
                    else
                    {
                        entity.IDEmpresa = null;
                        entity.IDComercio = null;
                        entity.IDMarca = idTipo;
                    }
                    entity.FechaDesde = DateTime.Parse(fechaDesde);
                    entity.FechaHasta = DateTime.Parse(fechaHasta);
                    entity.Sexo = sexo;
                    entity.Edad = edad;
                    entity.MultiplicaPuntos = multPuntos;
                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Beneficios.Add(entity);
                        dbContext.SaveChanges();
                    }

                    idGenerado = entity.IDBeneficio;
                }
            }

            return idGenerado;
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }


}