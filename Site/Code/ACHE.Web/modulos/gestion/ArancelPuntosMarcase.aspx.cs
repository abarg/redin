﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Collections.Specialized;


public partial class modulos_gestion_ArancelPuntosMarcase : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        pnlpaso2.Visible = false;
        cargarCombos();
        //ddlMarcas.SelectedValue = IDMarca.ToString();
    }
    
    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = null;
            listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("Todos los comercios", "0"));
            
   
            int idMarca = int.Parse(hdnIDMarca.Value);
            using (var dbContext = new ACHEEntities())
            {
                var comercios = dbContext.Comercios.Where(x => (idMarca == 0 || x.IDMarca == idMarca) && x.Activo == true)
                    .Select(x => new ComboViewModel { ID = x.IDComercio.ToString(), Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio }).OrderBy(x => x.Nombre).ToList();
                if (comercios != null)
                {
                    searchable.DataSource = comercios;
                    searchable.DataBind();
                }
            }
      
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
              
    }
            
    [System.Web.Services.WebMethod(true)]
    public static List<int> getComercios(int idmarca)
    {
        List<int> ids = new List<int>();
        using (var dbContext = new ACHEEntities())
        {
            var comercios = dbContext.Comercios.Where(x => (idmarca == 0 || x.IDMarca == idmarca) && x.Activo == true).Distinct().OrderBy(x => x.NombreFantasia).ToList();
            foreach (var comercio in comercios)
            {
                ids.Add(comercio.IDComercio);
            }
        }

        return ids;
    }
           

    protected void btnCargarComercios_Click(object sender, EventArgs e)
    {
        searchable.Items.Clear();
        int idMarca = int.Parse(hdnIDMarca.Value);
        using (var dbContext = new ACHEEntities())
        {
            var comercios = dbContext.Comercios.Where(x =>(idMarca==0|| x.IDMarca == idMarca))// && x.Activo == true)
                .Select(x => new ComboViewModel { ID = x.IDComercio.ToString(), Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio }).Distinct().OrderBy(x => x.Nombre).ToList();

            if (comercios != null)
            {
                searchable.DataSource = comercios;
                searchable.DataBind();
            }
        }
        cargarCombos();
        ddlMarcas.SelectedValue = idMarca.ToString() ;
        pnlpaso2.Visible = true;
    }

    [System.Web.Services.WebMethod(true)]
    public static void grabar(string comercios, string POSArancel, string Arancel2, string Arancel3, string Arancel4, string Arancel5, string Arancel6, string Arancel7, string POSPuntos, string Puntos2
        , string Puntos3, string Puntos4, string Puntos5, string Puntos6, string Puntos7, string MultPuntos1, string MultPuntos2, string MultPuntos3, string MultPuntos4, string MultPuntos5
        , string MultPuntos6, string MultPuntos7, string Descuento, string Descuento2, string Descuento3, string Descuento4, string Descuento5, string Descuento6, string Descuento7
        , string DescuentoVip, string DescuentoVip2, string DescuentoVip3, string DescuentoVip4, string DescuentoVip5, string DescuentoVip6, string DescuentoVip7
        , string PuntosVip, string PuntosVip2, string PuntosVip3, string PuntosVip4, string PuntosVip5, string PuntosVip6, string PuntosVip7, string MulPuntosVip, string MulPuntosVip2, string MulPuntosVip3
        , string MulPuntosVip4, string MulPuntosVip5, string MulPuntosVip6, string MulPuntosVip7,string CostoFijo,bool UsoRed)
    {
        try
        {
            string[] ids;
            ids = comercios.Split(",");
            if (ids.Count() > 0 && ids[0] != "null")
            {
                using (var dbContext = new ACHEEntities())
                {
                    foreach (var idComercio in ids)
                    {
                        int idCom = int.Parse(idComercio);
                        var terminales = dbContext.Terminales.Include("Comercios").Where(x => x.IDComercio == idCom).ToList();

                        var comercio = dbContext.Comercios.Where(x => x.IDComercio == idCom).FirstOrDefault();
                        if (CostoFijo != string.Empty)
                            comercio.CostoFijo = decimal.Parse(CostoFijo); 

                        foreach (var termi in terminales) {
                            if (termi != null) {
                                termi.POSArancel = decimal.Parse(POSArancel);
                                termi.Arancel2 = decimal.Parse(Arancel2);
                                termi.Arancel3 = decimal.Parse(Arancel3);
                                termi.Arancel4 = decimal.Parse(Arancel4);
                                termi.Arancel5 = decimal.Parse(Arancel5);
                                termi.Arancel6 = decimal.Parse(Arancel6);
                                termi.Arancel7 = decimal.Parse(Arancel7);

                                termi.POSPuntos = decimal.Parse(POSPuntos);
                                termi.Puntos2 = decimal.Parse(Puntos2);
                                termi.Puntos3 = decimal.Parse(Puntos3);
                                termi.Puntos4 = decimal.Parse(Puntos4);
                                termi.Puntos5 = decimal.Parse(Puntos5);
                                termi.Puntos6 = decimal.Parse(Puntos6);
                                termi.Puntos7 = decimal.Parse(Puntos7);

                                termi.MultiplicaPuntos1 = int.Parse(MultPuntos1);
                                termi.MultiplicaPuntos2 = int.Parse(MultPuntos2);
                                termi.MultiplicaPuntos3 = int.Parse(MultPuntos3);
                                termi.MultiplicaPuntos4 = int.Parse(MultPuntos4);
                                termi.MultiplicaPuntos5 = int.Parse(MultPuntos5);
                                termi.MultiplicaPuntos6 = int.Parse(MultPuntos6);
                                termi.MultiplicaPuntos7 = int.Parse(MultPuntos7);

                                termi.Descuento = int.Parse(Descuento);
                                termi.Descuento2 = int.Parse(Descuento2);
                                termi.Descuento3 = int.Parse(Descuento3);
                                termi.Descuento4 = int.Parse(Descuento4);
                                termi.Descuento5 = int.Parse(Descuento5);
                                termi.Descuento6 = int.Parse(Descuento6);
                                termi.Descuento7 = int.Parse(Descuento7);

                                if (DescuentoVip != string.Empty)
                                    termi.DescuentoVip = int.Parse(DescuentoVip);
                                else
                                    termi.DescuentoVip = null;
                                if (DescuentoVip2 != string.Empty)
                                    termi.DescuentoVip2 = int.Parse(DescuentoVip2);
                                else
                                    termi.DescuentoVip2 = null;
                                if (DescuentoVip3 != string.Empty)
                                    termi.DescuentoVip3 = int.Parse(DescuentoVip3);
                                else
                                    termi.DescuentoVip3 = null;
                                if (DescuentoVip4 != string.Empty)
                                    termi.DescuentoVip4 = int.Parse(DescuentoVip4);
                                else
                                    termi.DescuentoVip4 = null;
                                if (DescuentoVip5 != string.Empty)
                                    termi.DescuentoVip5 = int.Parse(DescuentoVip5);
                                else
                                    termi.DescuentoVip5 = null;
                                if (DescuentoVip6 != string.Empty)
                                    termi.DescuentoVip6 = int.Parse(DescuentoVip6);
                                else
                                    termi.DescuentoVip6 = null;
                                if (DescuentoVip7 != string.Empty)
                                    termi.DescuentoVip7 = int.Parse(DescuentoVip7);
                                else
                                    termi.DescuentoVip7 = null;

                                if (PuntosVip != string.Empty)
                                    termi.PuntosVip1 = decimal.Parse(PuntosVip);
                                else
                                    termi.PuntosVip1 = null;
                                if (PuntosVip2 != string.Empty)
                                    termi.PuntosVip2 = decimal.Parse(PuntosVip2);
                                else
                                    termi.PuntosVip2 = null;
                                if (PuntosVip3 != string.Empty)
                                    termi.PuntosVip3 = decimal.Parse(PuntosVip3);
                                else
                                    termi.PuntosVip3 = null;
                                if (PuntosVip4 != string.Empty)
                                    termi.PuntosVip4 = decimal.Parse(PuntosVip4);
                                else
                                    termi.PuntosVip4 = null;
                                if (PuntosVip5 != string.Empty)
                                    termi.PuntosVip5 = decimal.Parse(PuntosVip5);
                                else
                                    termi.PuntosVip5 = null;
                                if (PuntosVip6 != string.Empty)
                                    termi.PuntosVip6 = decimal.Parse(PuntosVip6);
                                else
                                    termi.PuntosVip6 = null;
                                if (PuntosVip7 != string.Empty)
                                    termi.PuntosVip7 = decimal.Parse(PuntosVip7);
                                else
                                    termi.PuntosVip7 = null;

                                if (MulPuntosVip != string.Empty)
                                    termi.MultiplicaPuntosVip1 = int.Parse(MulPuntosVip);
                                else
                                    termi.MultiplicaPuntosVip1 = null;

                                if (MulPuntosVip2 != string.Empty)
                                    termi.MultiplicaPuntosVip2 = int.Parse(MulPuntosVip2);
                                else
                                    termi.MultiplicaPuntosVip2 = null;

                                if (MulPuntosVip3 != string.Empty)
                                    termi.MultiplicaPuntosVip3 = int.Parse(MulPuntosVip3);
                                else
                                    termi.MultiplicaPuntosVip3 = null;

                                if (MulPuntosVip4 != string.Empty)
                                    termi.MultiplicaPuntosVip4 = int.Parse(MulPuntosVip4);
                                else
                                    termi.MultiplicaPuntosVip4 = null;

                                if (MulPuntosVip5 != string.Empty)
                                    termi.MultiplicaPuntosVip5 = int.Parse(MulPuntosVip5);
                                else
                                    termi.MultiplicaPuntosVip5 = null;

                                if (MulPuntosVip6 != string.Empty)
                                    termi.MultiplicaPuntosVip6 = int.Parse(MulPuntosVip6);
                                else
                                    termi.MultiplicaPuntosVip6 = null;

                                if (MulPuntosVip7 != string.Empty)
                                    termi.MultiplicaPuntosVip7 = int.Parse(MulPuntosVip7);
                                else
                                    termi.MultiplicaPuntosVip7 = null;

                                if (UsoRed != null)
                                    termi.CobrarUsoRed = UsoRed;
                                else
                                    termi.CobrarUsoRed = false;
                            }
                        }

                    }

                    dbContext.SaveChanges();
                }
            }
            else throw new Exception("Debe seleccionar al menos un comercio");


        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}