﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

public partial class modulos_cuponin_keywordse : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");

            CargarCombos();

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));

            }
        }
    }

    private void CargarCombos()
    {
        ddlComercio.DataSource = Common.LoadComercios();
        ddlComercio.DataBind();

    }

    [WebMethod(true)]
    public static void guardar(int id, string codigo, int idComercio, string fechaDesde, string fechaHasta, string respuesta, bool activo,
        string respuestaSinStock, string respuestaVencida, string tarjetasDesde, string tarjetasHasta)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Keywords.FirstOrDefault(s => s.Codigo == codigo);
                    if (aux != null && aux.IDKeyword != id)
                        throw new Exception("Ya existe una keyword con el código ingresado");

                    Keywords entity;
                    if (id > 0)
                        entity = dbContext.Keywords.Where(x => x.IDKeyword == id).FirstOrDefault();
                    else
                    {
                        entity = new Keywords();
                    }

                    if (tarjetasDesde != string.Empty)
                    {
                        if (!dbContext.Tarjetas.Any(x => x.Numero == tarjetasDesde && x.TipoTarjeta == "C"))
                            throw new Exception("No existen tarjetas destinadas a CuponIN  en el rango " + tarjetasDesde);
                    }
                    if (tarjetasHasta != string.Empty)
                    {
                        if (!dbContext.Tarjetas.Any(x => x.Numero == tarjetasHasta && x.TipoTarjeta == "C"))
                            throw new Exception("No existen tarjetas destinadas a CuponIN en el rango " + tarjetasHasta);
                    }


                    entity.FechaInicio = DateTime.Parse(fechaDesde);
                    if (fechaHasta != "")
                        entity.FechaFin = DateTime.Parse(fechaHasta);
                    else
                        entity.FechaFin = null;
                    entity.Codigo = codigo;
                    //entity.Stock = stock;
                    entity.Activo = activo;
                    entity.IDComercio = idComercio;
                    entity.Respuesta = respuesta;
                    entity.MensajeSinStock = respuestaSinStock;
                    entity.MensajeVencida = respuestaVencida;
                    entity.NumeroDesde = tarjetasDesde;
                    entity.NumeroHasta = tarjetasHasta;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Keywords.Add(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Keywords.Where(x => x.IDKeyword == id).FirstOrDefault();
            if (entity != null)
            {
                txtFechaDesde.Text = entity.FechaInicio.ToShortDateString();
                if (entity.FechaFin.HasValue)
                    txtFechaHasta.Text = entity.FechaFin.Value.ToShortDateString();
                txtCodigo.Text = entity.Codigo;
                //txtStock.Text = entity.Stock.ToString();

                //if (!dbContext.Tarjetas.Any(x => x.Numero >= tarjetasDesde && x.TipoTarjeta == "C"))
                if (!string.IsNullOrEmpty(entity.NumeroDesde) && !string.IsNullOrEmpty(entity.NumeroHasta))
                {
                    string sql = "select count(IDTarjeta) from tarjetas where numero>=" + entity.NumeroDesde + " and numero<=" + entity.NumeroHasta + " and tipotarjeta='C'";
                    var total = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();
                    sql = "select count(IDTarjeta) from tarjetas where numero>=" + entity.NumeroDesde + " and numero<=" + entity.NumeroHasta + " and fechabaja is null and tipotarjeta='C'";
                    var usadas = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();

                    txtStock.Text = usadas.ToString() + "/" + total.ToString();
                }

                chkActivo.Checked = entity.Activo;
                ddlComercio.SelectedValue = entity.IDComercio.ToString();
                txtRespuesta.Text = entity.Respuesta;
                txtRespuestaSinStock.Text = entity.MensajeSinStock;
                txtRespuestaVencido.Text = entity.MensajeVencida;
                txtTarjetasDesde.Text = entity.NumeroDesde;
                txtTarjetasHasta.Text = entity.NumeroHasta;
            }
        }
    }
}