﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_cuponin_consumos : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CargarCombo();
    }

    private void CargarCombo()
    {
        using (var dbContext = new ACHEEntities())
        {
            var keywords = dbContext.Keywords.Select(x => new { ID = x.IDKeyword, Nombre = x.Codigo }).OrderBy(x => x.Nombre).ToList();

            ddlKeyword.DataValueField = "ID";
            ddlKeyword.DataTextField = "Nombre";
            ddlKeyword.DataSource = keywords;
            ddlKeyword.DataBind();

            ddlKeyword.Items.Insert(0, new ListItem("Sin Keyword", "-1"));
            ddlKeyword.Items.Insert(0, new ListItem("Todas las keyword", "0"));
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string keyword, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.KeywordsConsumos.Include("Keywords")
                    .OrderByDescending(x => x.Fecha)
                    .Select(x => new KeywordsConsumosViewModel()
                    {
                        IDConsumo = x.IDConsumo,
                        IDKeyword = x.IDKeyword,
                        Keyword = x.IDKeyword.HasValue ? x.Keywords.Codigo : "",
                        PhoneNumber = x.PhoneNumber,
                        Fecha = x.Fecha,
                        Valido = x.Valido ? "Si" : "No",
                        Observaciones = x.Observations,
                        TarjetaUsada = x.TarjetaUsada
                    });

                if (keyword == "-1")
                    result = result.Where(x => !x.IDKeyword.HasValue);
                else if (keyword == "0")
                    result = result.Where(x => x.IDKeyword.HasValue);
                else
                {
                    int idAux = int.Parse(keyword);
                    result = result.Where(x => x.IDKeyword == idAux);
                }

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.Fecha <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string Exportar(string keyword, string fechaDesde, string fechaHasta)
    {
        string fileName = "Consumos";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    var info = dbContext.KeywordsConsumos.Include("Keywords")
                    .OrderByDescending(x => x.Fecha)
                    .Select(x => new
                    {
                        IDConsumo = x.IDConsumo,
                        IDKeyword = x.IDKeyword,
                        Keyword = x.IDKeyword.HasValue ? x.Keywords.Codigo : "",
                        PhoneNumber = x.PhoneNumber,
                        Fecha = x.Fecha,
                        Valido = x.Valido ? "Si" : "No",
                        Observaciones = x.Observations,
                        TarjetaUsada = x.TarjetaUsada
                    });

                    if (keyword == "-1")
                        info = info.Where(x => !x.IDKeyword.HasValue);
                    else if (keyword == "0")
                        info = info.Where(x => x.IDKeyword.HasValue);
                    else
                    {
                        int idAux = int.Parse(keyword);
                        info = info.Where(x => x.IDKeyword == idAux);
                    }

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        info = info.Where(x => x.Fecha <= dtHasta);
                    }


                    dt = info.Select(x => new
                    {
                        Keyword = x.Keyword,
                        PhoneNumber = x.PhoneNumber,
                        Fecha = x.Fecha,
                        Valido = x.Valido,
                        Observaciones = x.Observaciones,
                        TarjetaUsada = x.TarjetaUsada
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}