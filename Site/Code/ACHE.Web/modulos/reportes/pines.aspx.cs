﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_pines : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            LoadInfo();
    }

    private void LoadInfo() {
        using (var dbContext = new ACHEEntities()) {
            #region empresas canje
            var canjes = dbContext.EmpresasCanje.Where(x => x.Activo && x.UsaPines).OrderBy(x => x.Nombre).ToList();
            if (canjes != null) {
                cmbEmpresa.DataSource = canjes;
                cmbEmpresa.DataValueField = "IDEmpresaCanje";
                cmbEmpresa.DataTextField = "Nombre";
                cmbEmpresa.DataBind();

                cmbEmpresa.Items.Insert(0, new ListItem("Todas", ""));
            }
            #endregion
        }
    }

    private static DataTable getResults(int idEmpresa) {
        DataTable dt = new DataTable();
        using (var dbContext = new ACHEEntities()) {
            string sql = "select e.Nombre as Empresa, p.CostoInterno, count(p.idpin) as CantidadTotal, (select count(p1.IDPin) from Pines p1 where p1.IDEmpresaCanje=e.IDEmpresaCanje and p1.CostoInterno=p.CostoInterno and p1.IDSocio is null) as CantidadDisponibles from EmpresasCanje e inner join Pines p on e.IDEmpresaCanje=p.IDEmpresaCanje where activo = 1 and UsaPines = 1";
            if (idEmpresa > 0)
                sql += "and p.IDEmpresaCanje=" + idEmpresa;
            sql += "group by e.IDEmpresaCanje, e.Nombre,p.CostoInterno order by Empresa, CantidadTotal";
            var list = dbContext.Database.SqlQuery<EmpresasCanjeViewModel>(sql, new object[] { }).ToList();
            var aux = list.Select(x => new EmpresasCanjeViewModel() {
                Empresa = x.Empresa,
                CantidadTotal = x.CantidadTotal,
                CantidadDisponibles = x.CantidadDisponibles,
                CantidadNoDisponibles = x.CantidadTotal - x.CantidadDisponibles,
                CostoInterno = x.CostoInterno,
            }).ToList();

            dt = aux.Select(x => new {
                //Empresa = x.Empresa,
                Disponibles = x.CantidadDisponibles,
                Usados = x.CantidadNoDisponibles,
                Total = x.CantidadTotal,
                CostoInterno = "$" + x.CostoInterno,
            }).ToList().ToDataTable();
        }

        return dt;
    }

    [WebMethod(true)]
    public static string Buscar(int idEmpresa) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = getResults(idEmpresa);
                if (dt.Rows.Count > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table  id='grid'><colgroup>");

                    foreach (System.Data.DataColumn dc in dt.Columns) {
                        sb.Append("<col style='width:100px' ></col>");
                    }

                    sb.Append("</colgroup><thead><tr>");

                    foreach (System.Data.DataColumn dc in dt.Columns) {
                        sb.Append("<th>" + dc.ColumnName.Replace("_", " ") + "</th>");
                    }
                    sb.Append("</thead></tr><tbody>");

                    //write table data
                    foreach (System.Data.DataRow dr in dt.Rows) {
                        sb.Append("<tr>");
                        foreach (System.Data.DataColumn dc in dt.Columns) {
                            if (dc.ColumnName != "Estado")
                                sb.Append("<td>" + dr[dc].ToString() + "</td>");
                            else
                                sb.Append("<td><span class='label label-" + dr[dc].ToString().ToLower() + "' style='font-size: 12px;'>" + dr[dc].ToString() + "</label></td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");

                    return sb.ToString();
                }
                else
                    throw new Exception("No se encontraron registros");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    [WebMethod(true)]
    public static string Exportar(int idEmpresa) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            string fileName = "Pines";
            string path = "/tmp/";
            try {
                DataTable dt = getResults(idEmpresa);
                if (dt.Rows.Count > 0)
                    ExportarAExcel(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encontraron registros");

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}