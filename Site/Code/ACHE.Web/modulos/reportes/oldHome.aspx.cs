﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_oldHome : PaginaBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {

                /*ESTABLECIMIENTOS INEXISTENTES*/
                #region ESTABLECIMIENTOS INEXISTENTES
                int est = dbContext.Database.SqlQuery<int>("select count(NumEst) from Transacciones where NumEst not in (select ter.NumEst from Terminales ter where NumEst =  ANY (select ter.Numest)) and Importe > 1 ", new object[] { }).First();
                if (est > 0)
                {
                    litEst.Text = "<strong class='act-danger'>" + est + "</strong>";
                    pnlNumEstError.Visible = true;
                }
                else
                {
                    litEst.Text = "0";
                    pnlNumEstOk.Visible = true;
                }
                #endregion


                /*TERMINALES INEXISTENTES*/
                #region TERMINALES INEXISTENTES
                int terminales = dbContext.Database.SqlQuery<int>("select count(*) from Transacciones where NumTerminal not in (select POSTerminal from Terminales) and Importe>1", new object[] { }).First();
                if (terminales > 0)
                {
                    litTer.Text = "<strong class='act-danger'>" + terminales + "</strong>";
                    pnlTerminalesError.Visible = true;
                }
                else
                {
                    litTer.Text = "0";
                    pnlTerminalesOk.Visible = true;
                }
                #endregion


                /*TR INVALIDAS*/
                #region TR INVALIDAS
                int tr = dbContext.Database.SqlQuery<int>(@"select count(*)
                                                            from Transacciones
                                                            join terminales on Transacciones.IDTerminal = terminales.IDTerminal
                                                            where Transacciones.NumEst not in (select NumEst from Terminales where Terminales.POSTerminal =POSTerminal)
                                                            and Importe > 1", new object[] { }).First();
                if (tr > 0)
                {
                    litTr.Text = "<strong class='act-danger'>" + tr + "</strong>";
                    pnlTrError.Visible = true;
                }
                else
                {
                    litTr.Text = "0";
                    pnlTrOk.Visible = true;
                }
                #endregion


                /*Arancel/Puntos inválidas*/
                #region Arancel/Puntos inválidas
                int arancel = dbContext.Database.SqlQuery<int>("select count(*) from Transacciones where (Arancel is null or Puntos is null) and Importe>1", new object[] { }).First();
                if (arancel > 0)
                {
                    spnArancel.InnerHtml = "<strong class='act-danger'>" + arancel + "</strong>";
                    pnlArancelError.Visible = true;
                }
                else
                {
                    spnArancel.InnerText = "0";
                    pnlArancelOk.Visible = true;
                }
                #endregion


                /*TARJETAS NEGATIVAS*/
                #region TARJETAS NEGATIVAS
                int tarNegativas = dbContext.Database.SqlQuery<int>("select count(distinct IDTarjeta) from tarjetas where fechabaja is null and Credito<0 and FechaBaja is null", new object[] { }).First();
                if (tarNegativas > 0)
                {
                    litTar.Text = "<strong class='act-danger'>" + tarNegativas + "</strong>";
                    pnlTarError.Visible = true;
                }
                else
                {
                    litTar.Text = "0";
                    pnlTarOk.Visible = true;
                }
                #endregion



                //*********ALERTAS DEL SISTEMA RELACIONADAS A COMERCIOS*********//


                /*COMERCIOS INCOMPLETOS*/
                #region COMERCIOS INFO INCOMPLETA
                int comercio = dbContext.Database.SqlQuery<int>("select count(comIncompletos.IDComercio) from   (select distinct c.IDComercio, c.NombreFantasia as 'NombreComercio', f.NombreFantasia as 'NombreFranquicia', cid2.Nombre as Ciudad, d.Domicilio, c.IDDomicilioFiscal, isnull(cid.Nombre,'') as 'CiudadFiscal', d2.Domicilio as 'DomicilioFiscal', isnull(c.FormaPago_CBU,'') as FormaPago_CBU, ct.Email, ct.Telefono, isnull(c.FichaAlta,'') as FichaAlta from Comercios c inner join Domicilios d on c.IDDomicilio = d.IDDomicilio inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio left join Ciudades cid on cid.IDCiudad=d2.Ciudad left join Ciudades cid2 on d.Ciudad=cid2.IDCiudad  left join Franquicias f on c.IDFranquicia = f.IDFranquicia left join Contactos ct on c.IDContacto = ct.IDContacto where (d.Ciudad = '' or d.Domicilio = '' or IDDomicilioFiscal is null or d2.Ciudad = '' or d2.Domicilio = '' or c.FormaPago_CBU = '' or ct.Email = '' or ct.Telefono = '' or c.FichaAlta = '') and (c.Activo = 1) ) as comIncompletos", new object[] { }).First();
                if (comercio > 0)
                {
                    litComercio.Text = "<strong class='act-danger'>" + comercio + "</strong>";
                    pnlComercioError.Visible = true;
                }
                else
                {
                    litComercio.Text = "0";
                    pnlComercioOk.Visible = true;
                }
                #endregion

                /*TERMINALES SIN REPROGRAMAR*/
                #region TERMINALES SIN REPROGRAMAR
                int repro = dbContext.Database.SqlQuery<int>("select count(distinct IDTerminal) from Terminales where activo=1 and POSFechaReprogramacion is null and POSReprogramado=0 and POSfechaactivacion is null", new object[] { }).First();
                if (repro > 0)
                {
                    litReprog.Text = "<strong class='act-danger'>" + repro + "</strong>";
                    pnlReprogError.Visible = true;
                }
                else
                {
                    litReprog.Text = "0";
                    pnlReprogOk.Visible = true;
                }
                #endregion


                /*TERMINALES SIN ACTIVARSE*/
                #region TERMINALES SIN ACTIVARSE
                int sinActivarse = dbContext.Database.SqlQuery<int>("select count(terInactivas.IDComercio) from  (select distinct ter.IDComercio,ter.POSTipo,ter.POSTerminal,ter.NumEst from Terminales ter join Comercios c on c.IDComercio = ter.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio where ter.activo=1 and ter.POSFechaReprogramacion is not null and ter.POSReprogramado=1 and ter.POSfechaactivacion is null) as terInactivas", new object[] { }).First();
                if (sinActivarse > 0)
                {
                    litSinActivarse.Text = "<strong class='act-danger'>" + sinActivarse + "</strong>";
                    pnlSinActivarseError.Visible = true;
                }
                else
                {
                    litSinActivarse.Text = "0";
                    pnlSinActivarseOk.Visible = true;
                }
                #endregion



                /*COMERCIOS SIN TRANSACCIONES*/
                #region COMERCIOS SIN TRANSACCIONES
                //string sql = "select count(comSinTran.IDComercio) from(	  select c.IDComercio,ter.POSTerminal ,d.Domicilio,ter.NumEst   from Terminales ter   join Comercios c on c.IDComercio = ter.IDComercio	  join Franquicias f on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio 	  full join Transacciones tr on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst	  where ter.Estado <> 'DE' 	  group by c.IDComercio,ter.POSTerminal,d.Domicilio,ter.NumEst 	  having count(tr.IDTransaccion) < 1  ) comSinTran";               int sinTransacciones = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();
                int sinTransacciones = dbContext.Database.SqlQuery<int>("select count( distinct comSinTran.IDComercio) from(	 select distinct c.IDComercio,c.NombreFantasia as NombreComercio, F.NombreFantasia as NombreFranquicia, d.Domicilio  	 from Transacciones tr 	 right join Comercios c on c.idcomercio = tr.idcomercio	  join Terminales ter on ter.IDComercio = c.IDComercio	 join Franquicias f on f.IDFranquicia = c.IDFranquicia 	 join Domicilios d on d.IDDomicilio = c.IDDomicilio	 where tr.IDTransaccion is null and ter.Estado <>  ALL (SELECT 'DE') 	 ) comSinTran", new object[] { }).First();
                if (sinTransacciones > 0)
                {
                    litSinTransacciones.Text = "<strong class='act-danger'>" + sinTransacciones + "</strong>";
                    pnlSinTransacciones.Visible = true;
                }
                else
                {
                    litSinTransacciones.Text = "0";
                    pnlSinTransaccionesOk.Visible = true;
                }
                #endregion


                /*Comercios con mas de 1 transaccion en los últimos 3 dias*/
                #region Comercios con mas de 1 transaccion en los últimos 3 dias
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                //int trAyer = dbContext.Database.SqlQuery<int>("select count(*) from (select c.IDComercio from comercios c full join Transacciones tr on c.IDComercio = tr.IDComercio where FechaTransaccion>='" + DateTime.Now.AddDays(-3).ToString(formato) + "'   group by c.IDComercio Having count(c.IDComercio )>1) as cantComer ", new object[] { }).First();
                int trAyer = dbContext.Database.SqlQuery<int>("select count(*) from (select c.IDComercio from comercios c full join Transacciones tr on c.IDComercio = tr.IDComercio where FechaTransaccion>='" + DateTime.Now.AddDays(-3).ToString(formato) + "'   group by c.IDComercio Having count(c.IDComercio )>1) as cantComer ", new object[] { }).First();
                if (trAyer > 0)
                {
                    litTransAyer.Text = "<strong class='act-danger'>" + trAyer + "</strong>";
                    pnlTransAyer.Visible = true;
                }
                else
                {
                    litTransAyer.Text = "0";
                    pnlTransAyerOK.Visible = true;
                }
                #endregion
            }
        }
    }

    [WebMethod(true)]
    public static int solucionarArancelYPuntos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                dbContext.Database.SqlQuery<Chart>("exec SolucionarAlertasArancelYPuntos", new object[] { }).ToList();
                int arancel = dbContext.Database.SqlQuery<int>("select count(*) from Transacciones where (Arancel is null or Puntos is null) and Importe>1", new object[] { }).First();
                return arancel;
            }
        }
        return 0;
    }


    [WebMethod(true)]
    public static string obtenerDetalle(string tipo)
    {
        var html = string.Empty;
        var camposPendientes = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (tipo != "")
            {
                using (var dbContext = new ACHEEntities())
                {
                    string sql = "";

                    /* ESTABLECIMIENTOS INEXISTENTES*/
                    if (tipo == "establecimientos")
                        sql = "select distinct FechaTransaccion,Origen,Operacion, NumTarjetaCliente as Tarjeta, Importe, NumEst,NumTerminal from Transacciones where NumEst not in  (select ter.NumEst from Terminales ter where NumEst =  ANY (select ter.Numest ) group by ter.NumEst) and Importe>1";

                    /* TERMINALES INEXISTENTES*/
                    else if (tipo == "terminales")
                        sql = " select isnull((select top 1 c.NombreFantasia from comercios c join Terminales ter  on c.IDComercio = ter.IDTerminal where ter.NumEst = tr.numEst),'')as 'NombreComercio',isnull((select top 1 ter.POSTipo from Terminales ter   where ter.NumEst = tr.numEst),'')as 'POSTipo'  ,tr.FechaTransaccion,tr.Origen,tr.Operacion,tr.NumTarjetaCliente as 'Tarjeta',tr.Importe, tr.NumEst,tr.NumTerminal from Transacciones  tr where NumTerminal not in (select POSTerminal from Terminales) and Importe>1";

                    else if (tipo == "tr")
                        sql = @"select FechaTransaccion,Origen,Operacion, NumTarjetaCliente as Tarjeta, Importe, Transacciones.NumEst,NumTerminal
                                from Transacciones
                                join terminales on Transacciones.IDTerminal = terminales.IDTerminal
                                where Transacciones.NumEst not in (select NumEst from Terminales where Terminales.POSTerminal = POSTerminal)
                                and Importe > 1";

                    /*Arancel/Puntos inválidas*/
                    else if (tipo == "arancel")
                        sql = "select c.IDComercio,c.NombreFantasia  as NombreComercio, t.POSTipo ,FechaTransaccion,Origen,Operacion, NumTarjetaCliente as Tarjeta, Importe, t.NumEst,tr.NumTerminal from Transacciones tr join Terminales t on tr.IDTerminal = t.IDTerminal  join Comercios c on c.IDComercio = tr.IDComercio  where (tr.Arancel is null or tr.Puntos is null) and Importe>1";

                    /*TERMINALES SIN REPROGRAMAR*/
                    else if (tipo == "repro")
                        sql = "select distinct c.IDComercio,t.POSTipo, c.NombreFantasia as 'NombreComercio', t.POSTerminal, t.NumEst, c.NombreFantasia as 'NombreFranquicia', ci.Nombre, d.Domicilio, c.FechaAlta from Comercios c inner join Terminales t on t.IDComercio = c.IDComercio inner join Domicilios d on c.IDDomicilio = d.IDDomicilio left join Ciudades ci on ci.IDCiudad = d.Ciudad left join Franquicias f on c.IDFranquicia = f.IDFranquicia where t.activo=1 and t.POSFechaReprogramacion is null and t.POSReprogramado=0 and t.POSfechaactivacion is null ";

                    /*TARJETAS PUNTOS NEGATIVOS*/
                    else if (tipo == "tar")
                        sql = " select (select top 1 FechaTransaccion from Transacciones where NumTarjetaCliente= Tarjetas.Numero order by FechaTransaccion desc) as FechaTransaccion, ISNULL((select top 1 c.IDComercio from Comercios c where c.IDMarca = Marcas.IDMarca ),0) as IDComercio,ISNULL((select top 1 c.NombreFantasia from Comercios c where c.IDMarca = Marcas.IDMarca ),'') as NombreComercio,Numero as Tarjeta, PuntosTotales as PuntosTotales, Marcas.Nombre as Marca,ISNULL(Socios.Apellido+', '+Socios.Nombre,'') as Socio from Tarjetas inner join Marcas on Marcas.IDMarca= Tarjetas.IDMarca left join Socios on Socios.IDSocio= Tarjetas.IDSocio where Credito<0 and FechaBaja is null";


                    try
                    {
                        var list = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();

                        if (list.Any())
                        {

                            foreach (var detalle in list)
                            {
                                /* ESTABLECIMIENTOS INEXISTENTES*/
                                #region ESTABLECIMIENTOS INEXISTENTES
                                if (tipo == "tr" || tipo == "establecimientos")
                                {
                                    html += "<tr>";
                                    html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                    html += "<td>" + detalle.Origen + "</td>";
                                    html += "<td>" + detalle.Operacion + "</td>";
                                    html += "<td>" + detalle.Tarjeta + "</td>";
                                    html += "<td>" + detalle.Importe.ToString("N2") + "</td>";
                                    html += "<td>" + detalle.NumEst + "</td>";
                                    html += "<td>" + detalle.NumTerminal + "</td>";
                                    html += "</tr>";
                                }
                                #endregion

                                /* Arancel/Puntos inválidas*/
                                #region Arancel/Puntos inválidas
                                else if (tipo == "arancel")
                                {
                                    html += "<tr>";
                                    html += "<td><a href='/common/Comerciose.aspx?IDComercio=" + detalle.IDComercio + "'>" + detalle.NombreComercio + "</a></td>";
                                    html += "<td>" + detalle.POSTipo + "</td>";
                                    html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                    html += "<td>" + detalle.Origen + "</td>";
                                    html += "<td>" + detalle.Operacion + "</td>";
                                    html += "<td>" + detalle.Tarjeta + "</td>";
                                    html += "<td>" + detalle.Importe.ToString("N2") + "</td>";
                                    html += "<td>" + detalle.NumEst + "</td>";
                                    html += "<td>" + detalle.NumTerminal + "</td>";
                                    html += "</tr>";
                                }
                                #endregion

                                /* TRANSACCIONES CON TERMINALES INEXISTENTES*/
                                #region TRANSACCIONES CON TERMINALES INEXISTENTES
                                else if (tipo == "terminales")
                                {
                                    html += "<tr>";
                                    html += "<td>" + detalle.NombreComercio + "</td>";
                                    html += "<td>" + detalle.POSTipo + "</td>";
                                    html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                    html += "<td>" + detalle.Origen + "</td>";
                                    html += "<td>" + detalle.Operacion + "</td>";
                                    html += "<td>" + detalle.Tarjeta + "</td>";
                                    html += "<td>" + detalle.Importe.ToString("N2") + "</td>";
                                    html += "<td>" + detalle.NumEst + "</td>";
                                    html += "<td>" + detalle.NumTerminal + "</td>";
                                    html += "</tr>";
                                }
                                #endregion

                                #region otros
                                else if (tipo != "repro" && tipo != "tar" && tipo != "sinTrans")
                                {
                                    html += "<tr>";
                                    html += "<td><a href='/common/Comerciose.aspx?IDComercio=" + (detalle.IDComercio != null ? detalle.IDComercio : 0) + "'>" + detalle.NombreComercio + "</a></td>";
                                    html += "<td>" + detalle.POSTipo + "</td>";
                                    html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                    html += "<td>" + detalle.Origen + "</td>";
                                    html += "<td>" + detalle.Operacion + "</td>";
                                    html += "<td>" + detalle.Tarjeta + "</td>";
                                    html += "<td>" + detalle.Importe.ToString("N2") + "</td>";
                                    html += "<td>" + detalle.NumEst + "</td>";
                                    html += "<td>" + detalle.NumTerminal + "</td>";
                                    html += "</tr>";
                                }
                                #endregion

                                /*TERMINALES SIN REPROGRAMAR y SIN TRANSACCIONES*/
                                #region TERMINALES SIN REPROGRAMAR y SIN TRANSACCIONES
                                else if (tipo == "repro" || tipo == "sinTrans")
                                {
                                    html += "<tr>";
                                    html += "<td><a href='/common/Comerciose.aspx?IDComercio=" + detalle.IDComercio + "'>" + detalle.NombreComercio + "</a></td>";
                                    html += "<td>" + detalle.POSTipo + "</td>";
                                    html += "<td>" + detalle.POSTerminal + "</td>";
                                    html += "<td>" + detalle.NumEst + "</td>";
                                    html += "<td>" + detalle.NombreFranquicia + "</td>";
                                    html += "<td>" + detalle.Domicilio + "</td>";
                                    html += "</tr>";
                                }
                                #endregion

                                /*TARJETAS PUNTOS NEGATIVOS*/
                                #region TARJETAS PUNTOS NEGATIVOS
                                else if (tipo == "tar")
                                {
                                    html += "<tr>";
                                    html += "<td><a href='/common/Comerciose.aspx?IDComercio=" + detalle.IDComercio + "'>" + detalle.NombreComercio + "</a></td>";
                                    html += "<td>" + detalle.Tarjeta + "</td>";
                                    html += "<td>" + detalle.Marca + "</td>";
                                    html += "<td>" + detalle.Socio + "</td>";
                                    html += "<td>" + detalle.PuntosTotales + "</td>";
                                    html += "<td>" + detalle.FechaTransaccion.ToString("dd/MM/yyyy") + "</td>";
                                    html += "</tr>";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (tipo != "repro")
                                html += "<tr><td colspan='7'>No hay un detalle disponible</td></tr>";
                            else if (tipo == "repro")
                                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                            else
                                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
                        }
                    }

                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static List<DetalleAlerta> obtenerDetalleComercio(string tipo)
    {
        var html = string.Empty;
        var camposPendientes = string.Empty;
        List<DetalleAlerta> list = new List<DetalleAlerta>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                string sql = "";

                /*COMERCIOS INCOMPLETOS*/
                if (tipo == "info")
                    sql = "select distinct c.IDComercio, c.NombreFantasia as 'NombreComercio', f.NombreFantasia as 'NombreFranquicia', cid2.Nombre as Ciudad, d.Domicilio, c.IDDomicilioFiscal, isnull(cid.Nombre,'') as 'CiudadFiscal', d2.Domicilio as 'DomicilioFiscal', isnull(c.FormaPago_CBU,'') as FormaPago_CBU, ct.Email, ct.Telefono,isnull(c.FichaAlta,'') as FichaAlta from Comercios c inner join Terminales ter on ter.idcomercio = c.idcomercio inner join Domicilios d on c.IDDomicilio = d.IDDomicilio inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio left join Ciudades cid on cid.IDCiudad=d2.Ciudad left join Ciudades cid2 on d.Ciudad=cid2.IDCiudad  left join Franquicias f on c.IDFranquicia = f.IDFranquicia left join Contactos ct on c.IDContacto = ct.IDContacto where (d.Ciudad = '' or d.Domicilio = '' or IDDomicilioFiscal is null or d2.Ciudad = '' or d2.Domicilio = '' or c.FormaPago_CBU = '' or ct.Email = '' or ct.Telefono = '' or c.FichaAlta = '') and (c.Activo = 1) order by c.NombreFantasia";

                /*COMERCIOS SIN TRANSACCIONES*/
                else if (tipo == "sinTrans")
                    sql = "	select distinct c.IDComercio,c.NombreFantasia as NombreComercio, F.NombreFantasia as NombreFranquicia, d.Domicilio 	 from Transacciones tr  right join Comercios c on c.idcomercio = tr.idcomercio  join Terminales ter on ter.IDComercio = c.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia  join Domicilios d on d.IDDomicilio = c.IDDomicilio where tr.IDTransaccion is null and ter.Estado <>  ALL (SELECT 'DE') ";

                /*COMERCIOS CON MAS DE 1 TRANSACCION EN LOS ULTIMOS 3 DIAS*/
                else if (tipo == "masDe1Trans")
                {
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    sql = " select * FROM (select count (tr.IDTransaccion)as CantTR , c.IDComercio,c.NombreFantasia as NombreComercio, F.NombreFantasia as NombreFranquicia, d.Domicilio  from comercios c  full join Transacciones tr on c.IDComercio = tr.IDComercio join Franquicias f  on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio   where FechaTransaccion>='" + DateTime.Now.AddDays(-3).ToString(formato) + "'  group by   c.IDComercio,c.NombreFantasia, F.NombreFantasia , d.Domicilio) as t where CantTR>1";
                }

                /*TERMINALES SIN ACTIVARSE*/
                else if (tipo == "inactivos")
                    sql = "select distinct ter.IDComercio,c.NombreFantasia as 'NombreComercio',ter.POSTipo,ter.POSTerminal,ter.NumEst,f.NombreFantasia as 'NombreFranquicia',d.Domicilio from Terminales ter join Comercios c on c.IDComercio = ter.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio where ter.activo=1 and ter.POSFechaReprogramacion is not null and ter.POSReprogramado=1 and ter.POSfechaactivacion is null";

                try
                {
                    list = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                }

                catch (Exception ex)
                {
                    throw ex;
                }

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        if (tipo == "info")
                        {
                            if (string.IsNullOrEmpty(detalle.Ciudad))
                                camposPendientes += "Ciudad";
                            if (string.IsNullOrEmpty(detalle.Domicilio))
                                camposPendientes += ", Domicilio";
                            if (string.IsNullOrEmpty(detalle.CiudadFiscal))
                                camposPendientes += ", Ciudad Fiscal";
                            if (string.IsNullOrEmpty(detalle.DomicilioFiscal))
                                camposPendientes += ", Domicilio Fiscal";
                            if (string.IsNullOrEmpty(detalle.FormaPago_CBU))
                                camposPendientes += ", Forma de Pago(CBU)";
                            if (string.IsNullOrEmpty(detalle.Email))
                                camposPendientes += ", Email";
                            if (string.IsNullOrEmpty(detalle.Telefono))
                                camposPendientes += ", Telefono";
                            if (string.IsNullOrEmpty(detalle.POSTerminal) && detalle.POSTipo != "Web")
                                camposPendientes += ", Terminal POS";
                            if (string.IsNullOrEmpty(detalle.FichaAlta))
                                camposPendientes += ", Ficha Alta";

                            if (camposPendientes.Length > 2 && camposPendientes.Substring(0, 2) == ", ")
                                camposPendientes = camposPendientes.Substring(2, camposPendientes.Length - 2);
                            detalle.Pendientes = camposPendientes;
                        }

                        detalle.NombreComercio = "<a href='/common/Comerciose.aspx?IDComercio=" + detalle.IDComercio + "'>" + detalle.NombreComercio + "</a>";

                        camposPendientes = string.Empty;
                    }

                }
                //else
                //    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }

        return list;
    }

    [WebMethod(true)]
    public static string Exportar(string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                string fileName = string.Empty;
                string path = "/tmp/";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    string sql = string.Empty;

                    switch (tipo)
                    {
                        /*ESTABLECIMIENTOS INEXISTENTES*/
                        case "establecimientos":
                            fileName = "Establecimientos";
                            sql = "select distinct FechaTransaccion,Origen,Operacion, NumTarjetaCliente as Tarjeta, Importe, NumEst,NumTerminal from Transacciones where NumEst not in  (select ter.NumEst from Terminales ter where NumEst =  ANY (select ter.Numest ) group by ter.NumEst) and Importe>1";
                            break;

                        /*TERMINALES INEXISTENTES*/
                        case "terminales":
                            fileName = "Terminales";
                            sql = " select isnull((select top 1 c.NombreFantasia from comercios c join Terminales ter  on c.IDComercio = ter.IDTerminal where ter.NumEst = tr.numEst),'')as 'NombreComercio',isnull((select top 1 ter.POSTipo from Terminales ter   where ter.NumEst = tr.numEst),'')as 'POSTipo'  ,tr.FechaTransaccion,tr.Origen,tr.Operacion,tr.NumTarjetaCliente as 'Tarjeta',tr.Importe, tr.NumEst,tr.NumTerminal from Transacciones  tr where NumTerminal not in (select POSTerminal from Terminales) and Importe>1";
                            break;
                        /*TR INVALIDAS*/
                        case "tr":
                            fileName = "Transacciones";
                            sql = "select FechaTransaccion,Origen,Operacion,NumEst,NumTerminal from Transacciones where NumEst not in (select NumEst from Terminales where Transacciones.NumTerminal=POSTerminal) and Importe>1";
                            break;

                        /*ARANCELES Y PUNTOS INVALIDOS*/
                        case "arancel":
                            fileName = "Aranceles";
                            sql = "select (select top 1  c.nombreFantasia from comercios c where c.NumEst = t.NumEst and c.POSTerminal = t.NumTerminal)  as NombreComercio,(select top 1  c.POSTipo from comercios c where c.NumEst = t.NumEst and c.POSTerminal = t.NumTerminal) as POSTipo ,FechaTransaccion,Origen,Operacion, NumTarjetaCliente as Tarjeta, Importe, NumEst,NumTerminal from Transacciones t where (Arancel is null or Puntos is null) and Importe>1";
                            break;

                        /*COMERCIOS INFO INCOMPLETOS*/
                        case "info":
                            fileName = "Comercios";
                            sql = "select distinct c.IDComercio, c.NombreFantasia as 'NombreComercio', f.NombreFantasia as 'NombreFranquicia', cid2.Nombre as Ciudad, d.Domicilio, c.IDDomicilioFiscal, isnull(cid.Nombre,'') as 'CiudadFiscal', d2.Domicilio as 'DomicilioFiscal', isnull(c.FormaPago_CBU,'') as FormaPago_CBU, ct.Email, ct.Telefono,isnull(c.FichaAlta,'') as FichaAlta from Comercios c inner join Terminales ter on ter.idcomercio = c.idcomercio inner join Domicilios d on c.IDDomicilio = d.IDDomicilio inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio left join Ciudades cid on cid.IDCiudad=d2.Ciudad left join Ciudades cid2 on d.Ciudad=cid2.IDCiudad  left join Franquicias f on c.IDFranquicia = f.IDFranquicia left join Contactos ct on c.IDContacto = ct.IDContacto where (d.Ciudad = '' or d.Domicilio = '' or IDDomicilioFiscal is null or d2.Ciudad = '' or d2.Domicilio = '' or c.FormaPago_CBU = '' or ct.Email = '' or ct.Telefono = '' or c.FichaAlta = '') and (c.Activo = 1) order by c.NombreFantasia";
                            break;

                        /*TERMINALES SIN ACTIVARSE*/
                        case "inactivos":
                            fileName = "Comercios";
                            sql = "select distinct ter.IDComercio,c.NombreFantasia as 'NombreComercio',ter.POSTipo,ter.POSTerminal,ter.NumEst,f.NombreFantasia as 'NombreFranquicia',d.Domicilio from Terminales ter join Comercios c on c.IDComercio = ter.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio where ter.activo=1 and ter.POSFechaReprogramacion is not null and ter.POSReprogramado=1 and ter.POSfechaactivacion is null"; break;

                        /*TERMINALES SIN REPROGRAMAR*/
                        case "repro":
                            fileName = "Comercios";
                            sql = "select distinct c.IDComercio,t.POSTipo, c.NombreFantasia as 'NombreComercio', t.POSTerminal, t.NumEst, c.NombreFantasia as 'NombreFranquicia', ci.Nombre, d.Domicilio, c.FechaAlta from Comercios c inner join Terminales t on t.IDComercio = c.IDComercio inner join Domicilios d on c.IDDomicilio = d.IDDomicilio left join Ciudades ci on ci.IDCiudad = d.Ciudad left join Franquicias f on c.IDFranquicia = f.IDFranquicia where t.activo=1 and t.POSFechaReprogramacion is null and t.POSReprogramado=0 and t.POSfechaactivacion is null ";
                            break;

                        /*TARJETAS PUNTOS NEGATIVOS*/
                        case "tar":
                            fileName = "Tarjetas";
                            sql = "select ISNULL((select top 1 c.IDComercio from Comercios c where c.IDMarca = Marcas.IDMarca ),0) as IDComercio,ISNULL((select top 1 c.NombreFantasia from Comercios c where c.IDMarca = Marcas.IDMarca ),'') as NombreComercio,Numero as Tarjeta, PuntosTotales as PuntosTotales, Marcas.Nombre as Marca,ISNULL(Socios.Apellido+', '+Socios.Nombre,'') as Socio from Tarjetas inner join Marcas on Marcas.IDMarca= Tarjetas.IDMarca left join Socios on Socios.IDSocio= Tarjetas.IDSocio where Credito<0 and FechaBaja is null";
                            break;

                        /*COMERCIOS SIN TRANSACCIONES*/
                        case "sinTrans":
                            fileName = "Sin Transacciones";
                            sql = "	select distinct c.IDComercio,c.NombreFantasia as NombreComercio, F.NombreFantasia as NombreFranquicia, d.Domicilio 	 from Transacciones tr  right join Comercios c on c.idcomercio = tr.idcomercio  join Terminales ter on ter.IDComercio = c.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia  join Domicilios d on d.IDDomicilio = c.IDDomicilio where tr.IDTransaccion is null and ter.Estado <>  ALL (SELECT 'DE') ";
                            break;

                        /*COMERCIOS CON MAS DE 1 TRANSACCION EN LOS ULTIMOS 3 DIAS*/
                        case "masDe1Trans":
                            fileName = "Mas de una Transaccion";
                            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                            sql = " select * FROM (select count (tr.IDTransaccion)as CantTR , c.IDComercio,c.NombreFantasia as NombreComercio, F.NombreFantasia as NombreFranquicia, d.Domicilio  from comercios c  full join Transacciones tr on c.IDComercio = tr.IDComercio join Franquicias f  on f.IDFranquicia = c.IDFranquicia join Domicilios d on d.IDDomicilio = c.IDDomicilio   where FechaTransaccion>='" + DateTime.Now.AddDays(-3).ToString(formato) + "'  group by   c.IDComercio,c.NombreFantasia, F.NombreFantasia , d.Domicilio) as t where CantTR>1";
                            break;

                    }

                    var info = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                    if (info.Any())
                    {
                        /*COMERCIOS INCOMPLETOS*/
                        if (tipo == "info")
                        {
                            var camposPendientes = string.Empty;
                            foreach (var detalle in info)
                            {
                                if (string.IsNullOrEmpty(detalle.Ciudad))
                                    camposPendientes += "Ciudad";
                                if (string.IsNullOrEmpty(detalle.Domicilio))
                                    camposPendientes += ", Domicilio";
                                if (string.IsNullOrEmpty(detalle.CiudadFiscal))
                                    camposPendientes += ", Ciudad Fiscal";
                                if (string.IsNullOrEmpty(detalle.DomicilioFiscal))
                                    camposPendientes += ", Domicilio Fiscal";
                                if (string.IsNullOrEmpty(detalle.FormaPago_CBU))
                                    camposPendientes += ", Forma de Pago(CBU)";
                                if (string.IsNullOrEmpty(detalle.Email))
                                    camposPendientes += ", Email";
                                if (string.IsNullOrEmpty(detalle.Telefono))
                                    camposPendientes += ", Telefono";
                                if (string.IsNullOrEmpty(detalle.POSTerminal) && detalle.POSTipo != "Web")
                                    camposPendientes += ", Terminal POS";
                                if (string.IsNullOrEmpty(detalle.FichaAlta))
                                    camposPendientes += ", Ficha Alta";

                                if (camposPendientes.Length > 2 && camposPendientes.Substring(0, 2) == ", ")
                                    camposPendientes = camposPendientes.Substring(2, camposPendientes.Length - 2);
                                detalle.Pendientes = camposPendientes;

                                camposPendientes = string.Empty;
                            }
                        }
                    }

                    switch (tipo)
                    {
                        /*ESTABLECIMIENTOS INEXISTENTES*/
                        case "establecimientos":
                        /*TR INVALIDAS*/
                        case "tr":
                            dt = info.Select(x => new
                            {
                                FechaTransaccion = x.FechaTransaccion.ToShortDateString(),
                                Origen = x.Origen,
                                Operacion = x.Operacion,
                                NumEst = x.NumEst,
                                NumTerminal = x.NumTerminal,
                            }).ToList().ToDataTable();
                            break;
                        /*TERMINALES INEXSITENTES*/
                        case "terminales":
                        /*ARANCEL Y PUNTOS NEGATIVOS*/
                        case "arancel":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                TipoPOS = x.POSTipo,
                                FechaTransaccion = x.FechaTransaccion.ToShortDateString(),
                                Origen = x.Origen,
                                Operacion = x.Operacion,
                                NumEst = x.NumEst,
                                NumTerminal = x.NumTerminal,
                            }).ToList().ToDataTable();
                            break;
                        /*COMERCIOS INCOMPLETOS*/
                        case "info":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                NombreFranquicia = x.NombreFranquicia,
                                Pendientes = x.Pendientes
                            }).ToList().ToDataTable();
                            break;
                        /*TERMINALES SIN ACTIVARSE*/
                        case "inactivos":

                        /*TERMINALES SIN REPROGRAMARSE*/
                        case "repro":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                TipoPOS = x.POSTipo,
                                NumTerminal = x.POSTerminal,
                                NumEst = x.NumEst,
                                Franquicia = x.NombreFranquicia,
                                Domicilio = x.Domicilio
                            }).ToList().ToDataTable();
                            break;

                        /*COMERCIOS SIN TRANSACCIONES*/
                        case "sinTrans":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                Franquicia = x.NombreFranquicia,
                                Domicilio = x.Domicilio
                            }).ToList().ToDataTable();
                            break;
                        /*TARJETAS NEGATIVAS*/
                        case "tar":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                Tarjeta = x.Tarjeta,
                                Marca = x.Marca,
                                Socio = x.Socio,
                                Puntos = x.IDComercio,
                                Fecha = x.FechaTransaccion
                            }).ToList().ToDataTable();
                            break;
                        /*COMERCIOS CON MAS DE 1 TRANSACCION EN LOS ULTIMOS 3 DIAS*/
                        case "masDe1Trans":
                            dt = info.Select(x => new
                            {
                                NombreComercio = x.NombreComercio,
                                Franquicia = x.NombreFranquicia,
                                Domicilio = x.Domicilio
                            }).ToList().ToDataTable();
                            break;
                    }
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }


    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    public class DetalleAlerta
    {
        public DateTime FechaTransaccion { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
        public string NumEst { get; set; }
        public string Tarjeta { get; set; }
        public decimal Importe { get; set; }
        public string NumTerminal { get; set; }
        public int IDComercio { get; set; }
        public string NombreComercio { get; set; }
        public string NombreFranquicia { get; set; }
        public string Ciudad { get; set; }
        public string Domicilio { get; set; }
        public int IDDomicilioFiscal { get; set; }
        public string CiudadFiscal { get; set; }
        public string DomicilioFiscal { get; set; }
        public string FormaPago_CBU { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string POSTerminal { get; set; }
        public string POSTipo { get; set; }
        public string FichaAlta { get; set; }
        public string Pendientes { get; set; }
        public string Marca { get; set; }
        public string Socio { get; set; }
        public int PuntosTotales { get; set; }
    }
}