﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_ReporteComercios : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToShortDateString();
            this.txtFechaHasta.Text = DateTime.Now.ToShortDateString();
            //this.cargarCombos();
        }
    }

    //private void cargarCombos()
    //{
    //    try
    //    {
    //        bComercio bComercio = new bComercio();
    //        List<eComercio> listComercios = bComercio.getComercios();

    //        this.ddlNombre.DataSource = listComercios;
    //        this.ddlNombre.DataValueField = "NombreEst";
    //        this.ddlNombre.DataTextField = "NombreEst";
    //        this.ddlNombre.DataBind();
    //        this.ddlNombre.Items.Insert(0, new ListItem("", ""));
    //    }
    //    catch(Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    [WebMethod(true)]
    public static string buscar(string Tipo, string FechaDesde, string FechaHasta, string SDS, string Nombre, string CantOper)
    {
        try
        {
            bComercio bComercio = new bComercio();
            DataTable dt = new DataTable();

            if (FechaDesde == "") FechaDesde = null;
            if (FechaHasta == "") FechaHasta = null;
            if (SDS == "") SDS = null;
            if (Nombre == "") Nombre = null;
            if (CantOper == "") CantOper = null;

            SqlParameter p_Tipo = new SqlParameter("@Tipo", Tipo);
            SqlParameter p_FechaDesde = new SqlParameter("@FechaDesdeTrans", DateTime.Parse(FechaDesde).ToString("yyyy/MM/dd"));
            SqlParameter p_FechaHasta = new SqlParameter("@FechaHastaTrans", DateTime.Parse(FechaHasta).ToString("yyyy/MM/dd"));
            SqlParameter p_SDS = new SqlParameter("@SDS", SDS);
            SqlParameter p_Nombre = new SqlParameter("@NombreEst", Nombre);
            SqlParameter p_CantOper = new SqlParameter("@CantOperaciones", CantOper);

            SqlParameter[] parametros = new SqlParameter[6];
            parametros[0] = p_Tipo;
            parametros[1] = p_FechaDesde;
            parametros[2] = p_FechaHasta;
            parametros[3] = p_SDS;
            parametros[4] = p_Nombre;
            parametros[5] = p_CantOper;

            SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "GetReporteComercios", parametros);
            dt.Load(reader);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table  id='grid'><colgroup>");
                
                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    sb.Append("<col style='width:100px' ></col>");
                }

                sb.Append("</colgroup><thead><tr>");
                
                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    sb.Append("<th>" + dc.ColumnName + "</th>");
                }
                sb.Append("</thead></tr><tbody>");

                //write table data
                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    sb.Append("<tr>");
                    foreach (System.Data.DataColumn dc in dt.Columns)
                    {
                        sb.Append("<td>" + dr[dc].ToString() + "</td>");
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");

                return sb.ToString();
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            //return "/ArchivosTemp/Reportes" + nombreArchivo + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string exportar(string Tipo, string FechaDesde, string FechaHasta, string SDS, string Nombre, string CantOper)
    {
        string nombreArchivo = "";
        string rutaReportes = "ArchivosTemp/Reportes";
        try
        {
            bComercio bComercio = new bComercio();
            DataTable dt = new DataTable();

            if (FechaDesde == "") FechaDesde = null;
            if (FechaHasta == "") FechaHasta = null;
            if (SDS == "") SDS = null;
            if (Nombre == "") Nombre = null;
            if (CantOper == "") CantOper = null;

            SqlParameter p_Tipo = new SqlParameter("@Tipo", Tipo);
            SqlParameter p_FechaDesde = new SqlParameter("@FechaDesdeTrans", DateTime.Parse(FechaDesde).ToString("yyyy/MM/dd"));
            SqlParameter p_FechaHasta = new SqlParameter("@FechaHastaTrans", DateTime.Parse(FechaHasta).ToString("yyyy/MM/dd"));
            SqlParameter p_SDS = new SqlParameter("@SDS", SDS);
            SqlParameter p_Nombre = new SqlParameter("@NombreEst", Nombre);
            SqlParameter p_CantOper = new SqlParameter("@CantOperaciones", CantOper);

            SqlParameter[] parametros = new SqlParameter[6];
            parametros[0] = p_Tipo;
            parametros[1] = p_FechaDesde;
            parametros[2] = p_FechaHasta;
            parametros[3] = p_SDS;
            parametros[4] = p_Nombre;
            parametros[5] = p_CantOper;

            SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "GetReporteComercios", parametros);
            dt.Load(reader);
            if (dt.Rows.Count > 0)
            {
                if (Tipo.Equals("A")) nombreArchivo = "Comercios_Agrupado";
                else if (Tipo.Equals("D")) nombreArchivo = "Comercios_Detallado";
                ExportarAExcel(dt, HttpContext.Current.Server.MapPath(rutaReportes) + Path.GetFileName(nombreArchivo), nombreArchivo);
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            return "/ArchivosTemp/Reportes" + nombreArchivo + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}