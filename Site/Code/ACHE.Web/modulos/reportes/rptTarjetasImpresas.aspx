﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="rptTarjetasImpresas.aspx.cs" Inherits="modulos_reportes_rptTarjetasImpresas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <h3 class="heading">Tarjetas impresas por marca</h3>
            <%--<div id="fl_tarjetas_impresas" style="height: 1000px; width: 90%; margin: 15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>--%>
            <div class="row">
                <div class="col-md-2">
                    <label>Numero tarjeta desde</label>
                    <input id="txtDesdeTarjeta" type="text" maxlength="20" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label>Numero tarjeta hasta</label>
                    <input id="txtHastaTarjeta" type="text" maxlength="20" class="form-control" />
                </div>

            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-md-8">
                    <button class="btn" type="button" id="Button1" onclick="ObtenerTarjetasImpresas2();">Buscar</button>
                    <button class="btn btn-success" type="button" id="btnExportarEmitidas" onclick="ExportarTarjetasImpresas();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingEmitidas" style="display: none" />
                    <a href="" id="lnkDownloadEmitidas" download="Emitidas" style="display: none">Descargar</a>
                </div>
            </div>
        </div>
        <br />
        <table class="table table-condensed table-striped" data-provides="rowlink" id="tablaImpresas">

            <thead id="headImpresas">
                <tr>
                    <th style="min-width: 200px">Marca</th>
                    <th>Tarjetas Activas</th>
                    <th>Tarjetas Inactivas</th>
                    <th>Tarjetas Totales</th>
                    <th>Desde</th>
                    <th>Hasta</th>
                </tr>
            </thead>
            <tbody id="bodyImpresas">
            </tbody>
        </table>
    </div>
    </div>
    

        
    <div class="modal modal-wide fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>SDS</th>
                                <th>Nombre</th>
                                <th>Franquicia</th>
                                <th>Marca</th>
                                <th>Domicilio</th>
                                <th>Fecha Carga</th>
                                <th>Fecha Alta</th>
                                <th>Terminal</th>
                                <th>Establecimiento</th>
                                <th>Fecha Activ</th>
                                <th>Fecha Reprog</th>
                                <th>Reprogramado</th>
                                <th>Invalido</th>
                                <th>Activo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <%--<button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>--%>
                </div>
            </div>
        </div>
    </div>
    <%--    <div class="modal fade" id="modalCargarFechaVencimiento">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Edicion FechaVencimiento</h4>
                        <input type="hidden" id="hdnID" value="0" />
                          <input type="hidden" id="hdnNumero" value="0" />
                        
                    </div>
                    <div class="modal-body">
                            <div class="container">
                                   <form id="formEdicion">
                                   <div class="row">                                
                                       <div class="col-sm-6">
                                            <label class="col-lg-4 control-label"><span class="f_req">*</span> Fecha vencimiento</label>
                                            <div class="col-lg-6">
                                                <input type="text" id="txtFechaVencimiento" class="form-control required validDate" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnGuardar" onclick="guardarFechaVencimiento();">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
    </div>--%>


    <!-- charts functions -->

    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/rptTarjetasImpresas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <!-- tablas -->
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

