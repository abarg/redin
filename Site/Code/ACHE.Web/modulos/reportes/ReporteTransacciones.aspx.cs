﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Data.Entity.Infrastructure;



public partial class modulos_reportes_ReporteTransacciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");

            this.cargarMarcas();
            this.cargarFranquicias();
            //this.cargarComercios();
        }

    }


    private void cargarFranquicias()
    {
        try
        {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("Todas las franquicias", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void cargarMarcas()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("Todas las marcas", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string PeriodoDesde, string PeriodoHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                int mesDesde = DateTime.Parse(PeriodoDesde).Month;
                int añoDesde = DateTime.Parse(PeriodoDesde).Year;

                int mesHasta = DateTime.Parse(PeriodoHasta).Month;
                int añoHasta = DateTime.Parse(PeriodoHasta).Year;
                string mes = "";
                var result = dbContext.TransaccionesComerciosView
                    .Where(x => x.mes >= mesDesde && x.anio >= añoDesde && x.mes <= mesHasta && x.anio <= añoHasta)
                        .OrderBy(x => x.mes)
                        .OrderBy(x => x.anio)
                        .Select(x => new
                        {
                            Mes = x.mes,
                            NombreMes = x.nombreMes,
                            Año = x.anio,
                            CantTrans = x.CantTrans,
                            Comercio = x.Comercio,
                            IDComercio = x.IDComercio,
                            IDFranquicia = x.IDFranquicia,
                            Franquicia = x.Franquicia,
                            Marca = x.Marca,
                            IDMarca = x.IDMarca,
                            DomicilioComercio = x.DomicilioComercio
                            //PESOS = x.PESOS ?? 0
                            //IDDomicilio = x.IDDomicilio
                        }).ToList();

                return result.AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList
            }
        }
        else
            return null;
    }

    public static string convertirNumeroAMes(int numeroMes)
    {
        string mes;
        switch (numeroMes)
        {
            case 1: mes = "Enero";
                return mes;
            case 2: mes = "Febrero";
                return mes;
            case 3: mes = "Marzo";
                return mes;
            case 4: mes = "Abril";
                return mes;
            case 5: mes = "Mayo";
                return mes;
            case 6: mes = "Junio";
                return mes;
            case 7: mes = "Julio";
                return mes;
            case 8: mes = "Agosto";
                return mes;
            case 9: mes = "Septiembre";
                return mes;
            case 10: mes = "Octubre";
                return mes;
            case 11: mes = "Noviembre";
                return mes;
            case 12: mes = "Diciembre";
                return mes;
        }
        return "";
    }

    [WebMethod(true)]
    public static string exportar(string PeriodoDesde, string PeriodoHasta, int idFranquicia, int idMarca, string comercio)
    {
        string fileName = "TransaccionesComercios";
        string path = "/tmp/";
        try
        {

            DataTable dt = new DataTable();

            int mesDesde = DateTime.Parse(PeriodoDesde).Month;
            int añoDesde = DateTime.Parse(PeriodoDesde).Year;

            int mesHasta = DateTime.Parse(PeriodoHasta).Month;
            int añoHasta = DateTime.Parse(PeriodoHasta).Year;
            using (var dbContext = new ACHEEntities())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 300;

                var info = dbContext.TransaccionesComerciosView
                    .Where(x => x.mes >= mesDesde && x.anio >= añoDesde && x.mes <= mesHasta && x.anio <= añoHasta)
                        .OrderBy(x => x.mes)
                        .OrderBy(x => x.anio)
                        .Select(x => new
                        {
                            Mes = x.mes ?? 0,
                            NombreMes = x.nombreMes,
                            Año = x.anio ?? 0,
                            CantTrans = x.CantTrans ?? 0,
                            Comercio = x.Comercio,
                            IDComercio = x.IDComercio,
                            IDFranquicia = x.IDFranquicia,
                            Franquicia = x.Franquicia,
                            Marca = x.Marca,
                            IDMarca = x.IDMarca,
                            DomicilioComercio = x.DomicilioComercio
                           // Pesos = x.PESOS??0

                            //IDDomicilio = x.IDDomicilio
                        });
                if (comercio != "")
                    info = info.Where(x => x.Comercio.ToLower().Contains(comercio.ToLower()));
                if (idMarca > 0)
                    info = info.Where(x => x.IDMarca == idMarca);
                if (idFranquicia > 0)
                    info = info.Where(x => x.IDFranquicia == idFranquicia);

                var aux = info.ToList();

                dt = info
                    .OrderBy(x => x.Año).ThenBy(x => x.Mes)
                    .Select(x => new
                    {
                        Año = x.Año,
                        NombreMes = x.NombreMes,
                        Franquicia = x.Franquicia,
                        Marca = x.Marca,
                        Comercio = x.Comercio,
                        DomicilioComercio = x.DomicilioComercio,
                        CantTrans = x.CantTrans
                        //PESOS=x.Pesos
                    }).ToList().ToDataTable();
            }

            if (dt.Rows.Count > 0)
            {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}


