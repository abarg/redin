﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Data.Entity.Infrastructure;

public partial class modulos_reportes_facturacion_detalle : PaginaBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            this.cargarMarcas();
            this.cargarFranquicias();
        }
    }


    private void cargarFranquicias() {
        try {
            bFranquicia bFranquicia = new bFranquicia();
            List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
            this.ddlFranquicias.DataSource = listFranquicias;
            this.ddlFranquicias.DataValueField = "IDFranquicia";
            this.ddlFranquicias.DataTextField = "NombreFantasia";
            this.ddlFranquicias.DataBind();

            this.ddlFranquicias.Items.Insert(0, new ListItem("Todas las franquicias", "0"));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    private void cargarMarcas() {
        try {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("Todas las marcas", "0"));
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 300;
                
                var result = dbContext.Rpt_FacturacionDetalle(DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta))
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new {
                        Fecha = x.FechaTransaccion,
                        Origen = x.Origen,
                        POSTipo = x.POSTipo,
                        POSSistema = x.POSSistema,
                        POSTerminal = x.POSTerminal,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia.ToUpper(),
                        Tarjeta = x.Tarjeta,
                        NroDocumento = x.NroDocumento,
                        NumEst = x.NumEst,
                        FranqComercio = x.FranqComercio,
                        Marca = x.Marca,
                        IDMarca = x.IDMarca,
                        Socio = x.Socio,
                        ImporteOriginal = x.ImporteOriginal,
                        ImporteAhorro = x.ImporteAhorro,
                        ImportePagado = x.ImportePagado,
                        CostoRedIn = x.CostoRedIn,
                        Ticket = x.Ticket,
                        Arancel = x.Arancel,
                        Puntos = x.Puntos,
                        NetoGrabado = x.NetoGrabado,
                        IVA = x.IVA,
                        TotalFactura = x.TotalFactura,
                        IIBB = x.IIBB,
                        Tish = x.Tish,
                        CostoPuntos = x.CostoPuntos,
                        CostoPos = x.CostoPos,
                        NetoTax = x.NetoTax,
                        Franquicia = x.Franquicia,
                        IDFranquicia = x.IDFranquicia
                   
                        //CantOperaciones = x.CantOperaciones
                    });

                return result.ToList().AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string exportar(string fechaDesde, string fechaHasta, string sds, string nombre, int idMarca, string cant, string operacion, int idFranquicia) {
        string fileName = "FacturacionDetalle";
        string path = "/tmp/";
        try {

            DataTable dt = new DataTable();
            using (var dbContext = new ACHEEntities()) {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 300;

                var info = dbContext.Rpt_FacturacionDetalle(DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta))
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new {
                        Fecha = x.FechaTransaccion,
                        Origen = x.Origen,
                        POSTipo = x.POSTipo,
                        POSSistema = x.POSSistema,
                        POSTerminal = x.POSTerminal,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia.ToUpper(),
                        Tarjeta = x.Tarjeta,
                        NroDocumento = x.NroDocumento,
                        NumEst = x.NumEst,
                        FranqComercio = x.FranqComercio,
                        Marca = x.Marca,
                        IDMarca = x.IDMarca,
                        Socio = x.Socio,
                        ImporteOriginal = x.ImporteOriginal,
                        ImporteAhorro = x.ImporteAhorro,
                        ImportePagado = x.ImportePagado,
                        CostoRedIn = x.CostoRedIn,
                        Ticket = x.Ticket,
                        Arancel = x.Arancel,
                        Puntos = x.Puntos,
                        NetoGrabado = x.NetoGrabado,
                        IVA = x.IVA,
                        TotalFactura = x.TotalFactura,
                        IIBB = x.IIBB,
                        Tish = x.Tish,
                        CostoPuntos = x.CostoPuntos,
                        CostoPos = x.CostoPos,
                        NetoTax = x.NetoTax,
                        Franquicia = x.Franquicia,
                        IDFranquicia = x.IDFranquicia
                        //CantOperaciones = x.CantOperaciones
                    }).ToList().AsQueryable();


                if (sds != "")
                    info = info.Where(x => x.SDS.ToLower().Contains(sds.ToLower()));
                if (nombre != "")
                    info = info.Where(x => x.NombreFantasia.ToLower().Contains(nombre.ToLower()));
                if (idMarca > 0)
                    info = info.Where(x => x.IDMarca == idMarca);               
                if( idFranquicia  > 0)
                    info = info.Where(x => x.IDFranquicia == idFranquicia);
                if (operacion != "")
                    info = info.Where(x => x.Operacion.ToLower() == operacion.ToLower());
                /*if (cant != "")
                    info = info.Where(x => x.CantOperaciones >= int.Parse(cant));*/

                dt = info.Select(x => new {
                    Fecha = x.Fecha.ToShortDateString(),
                    Horario = x.Fecha.ToString("HH:MM:ss"),
                    Origen = x.Origen,
                    POSTipo = x.POSTipo,
                    POSSistema = x.POSSistema,
                    POSTerminal = x.POSTerminal,
                    Operacion = x.Operacion,
                    SDS = x.SDS,
                    Franquicia = x.FranqComercio,
                    Marca = x.Marca,
                    NombreFantasia = x.NombreFantasia.ToUpper(),
                    //FranqComercio = x.FranqComercio,
                    Tarjeta = x.Tarjeta,
                    NroDocumento = x.NroDocumento,
                    NumEst = x.NumEst,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal??0,
                    ImporteAhorro = x.ImporteAhorro ?? 0,
                    ImportePagado = x.ImportePagado ?? 0,
                    CostoRedIn = x.CostoRedIn ?? 0,
                    Ticket = x.Ticket ?? 0,
                    Arancel = x.Arancel ?? 0,
                    Puntos = x.Puntos ?? 0,
                    NetoGrabado = x.NetoGrabado ?? 0,
                    IVA = x.IVA ?? 0,
                    TotalFactura = x.TotalFactura ?? 0,
                    IIBB = x.IIBB ?? 0,
                    Tish = x.Tish ?? 0,
                    CostoPuntos = x.CostoPuntos,
                    CostoPos = x.CostoPos,
                    NetoTax = x.NetoTax ?? 0
                    //CantOperaciones = x.CantOperaciones.HasValue ? x.CantOperaciones.Value : 0
                }).ToList().ToDataTable();

            }

            if (dt.Rows.Count > 0) {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}