﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dashboard-fact.aspx.cs" Inherits="modulos_reportes_dashboard_fact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <!--[if lt IE 9]>
		<script src="/lib/flot/excanvas.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
		<div class="col-sm-12 tac">
            <h3 class="heading">Estadísticas generales</h3>
            <ul class="ov_boxes">
                <li>
					<div class="p_bar_down p_canvas">
                        <img src="../../img/dashboard/sex.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0; height:70px">
						<strong id="resultado_sexo" style="font-size: 13px;padding-top: 9px;">Calculando...</strong><%--F: 100 - M: 300--%>
						Total Socios: <span id="totalSexo"></span>
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/email.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_mails">Calculando...</strong>
						Cantidad de <br />mails
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/MetroUI_Phone.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_cel">Calculando...</strong>
						Cantidad de <br />celulares
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/credit_card_yellow.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_tarjetas_activas" style="font-size: 13px">Calculando...</strong>
						Cantidad de <br />Tarjetas activas
					</div>
				</li>
                 <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/money2.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_dinero_credito" style="font-size: 13px">Calculando...</strong>
						Dinero disponible en tarjetas
					</div>
				</li>
                
            </ul>
        </div>
    </div>
    <div class="row">
		<div class="col-sm-12 tac">
			<h3 class="heading"><asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
            <ul class="ov_boxes">
				<li>
					<div class="p_bar_up p_canvas">
                        <img src="../../img/dashboard/money-graph.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_ticketpromedio_mensual">Calculando...</strong>
						Promedio ticket <br />mensual
					</div>
				</li>
				<li>
					<div class="p_line_up p_canvas">
                         <img src="../../img/dashboard/Tracking_3D.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_tasauso_mensual">Calculando...</strong>
                        Tasa de <br />uso
					</div>
				</li>
				<li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/transaction.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_tr_mensual">Calculando...</strong>
						Cantidad de <br />trans. mensuales
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/billete.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_arancel_mensual">Calculando...</strong>
						Total arancel <br />mensual
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/coins.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_puntos_mensual">Calculando...</strong>
						Total puntos <br />mensuales
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/graph.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_promedio_puntos">Calculando...</strong>
						Promedio <br />puntos
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/graph.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_promedio_arancel">Calculando...</strong>
						Promedio <br />arancel
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/graph.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_promedio_descuento">Calculando...</strong>
						Promedio <br />descuento
					</div>
				</li>
            </ul>
            <ul class="ov_boxes">
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/bill.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_facturacion_mensual">Calculando...</strong>
						Total <br />facturado 
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/exchange.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_canjes_realizados">Calculando...</strong>
						Canjes <br />realizados
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/emblem-money.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_facturacion">Calculando...</strong>
						Total <br />facturacion
					</div>
				</li>
                
			</ul>
            <ul class="ov_boxes">
                
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/credit_card.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_cantidad_tarjetas">Calculando...</strong>
						Cantidad <br />tarjetas
					</div>
				</li>
                 <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/1396123817_banker.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_facturacion_comercios">Calculando...</strong>
						Facturacion <br />comercios
					</div>
				</li>
            
              
            </ul>

		</div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Puntos otorgados <small>últimos 4 meses</small></h3>
            <div id="fl_puntos_otorgados" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Facturación & Ahorro <small>últimos 4 meses</small></h3>
            <div id="fl_facturacion_ahorro" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
			<h3 class="heading">Facturación & Ahorro <small>últimos 30 días</small></h3>
            <div id="fl_facturacion_ahorro_detalle" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
			<h3 class="heading">Arancel, Puntos & Total Facturado  <small>últimos 30 días</small></h3>
            <div id="fl_arancel_puntos_fac_detalle" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>




    <%--<div class="row"></div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<div class="heading clearfix">
				<h3 class="pull-left">TOP 10 Mejores socios</h3>
			</div>
			<table class="table table-striped table-bordered mediaTable">
				<thead>
					<tr>
						
						<th class="essential persist">Nombre y Apellido</th>
						<th class="optional">Tarjeta</th>
						<th class="optional">Marca</th>
						<th class="essential">Importe</th>
					</tr>
				</thead>
				<tbody id="bodySocios">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
        <div class="col-sm-6 col-lg-6">
			<div class="heading clearfix">
				<h3 class="pull-left">TOP 10 Mejores comercios</h3>
			</div>
			<table class="table table-striped table-bordered mediaTable">
				<thead>
					<tr>
						
						<th class="essential persist">Comercio</th>
						<th class="optional">Domicilio</th>
						<!--th class="optional">Nro. Estab.</!--th-->
						<th class="essential">Importe</th>
					</tr>
				</thead>
				<tbody id="bodyComercios">
                    <tr><td colspan="3">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Tarjetas asignadas <small>últimos 4 meses</small></h3>
            <div id="fl_tarjetas_asignadas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Tarjetas activas & inactivas <small>últimos 4 meses</small></h3>
            <div id="fl_tarjetas_activas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading" id="lblTerminalesPorPOS">Terminales por pos </h3>
            <div id="fl_terminales_activas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading" id="lblEstadoTerminales">Estado de las terminales</h3>
            <div id="fl_estado_terminales" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading">Tarjetas asignadas por marca</h3>
            <div id="fl_tarjetas_marca" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <h3 class="heading">&nbsp;</h3>
            <div id="fl_tarjetas_marca_legend"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
			<h3 class="heading">Tarjetas impresas por marca</h3>
            <div id="fl_tarjetas_impresas" style="height:500px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>--%>

	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <%--<script src="/lib/flot/jquery.flot.pyramid.js"></script>--%>


    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/dashboard-fact.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

