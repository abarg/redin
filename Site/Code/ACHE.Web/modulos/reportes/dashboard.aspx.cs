﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;

public partial class modulos_reportes_dashboard : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litFechaTit.Text = "Desde " + DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy") + " hasta " + DateTime.Now.ToString("dd/MM/yyyy");
    }

    #region Graficos

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTerminalesPorPOS()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TerminalesActivos").ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasEmitidas()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasEmitidas").ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasEmitidasActivas()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasEmitidasActivas").ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasAsignadas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasAsignadas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasAsignadas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasAsignadas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasAsignadas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ChartDecimal> obtenerPuntosOtorgados()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<ChartDecimal> list = new List<ChartDecimal>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PuntosOtorgados '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PuntosOtorgados '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PuntosOtorgados '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PuntosOtorgados '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new ChartDecimal() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
             

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorro()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteAhorro '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteAhorro '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteAhorro '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteAhorro '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagado()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImportePagado'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImportePagado '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImportePagado '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImportePagado '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginal()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteOriginal '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteOriginal '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteOriginal '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ImporteOriginal '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasPorMarca()
    {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasPorMarca").ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasActivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasActivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasActivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasActivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasActivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasInactivas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasInactivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasInactivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasInactivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TarjetasInactivas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Socios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_TopSocios", new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Comercios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_TopComercios", new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }
     [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10SociosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_TopSociosMensual '" + fechaDesde + "','" + fechaHasta + "'",new object[] { }).ToList();

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
         }
    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10ComerciosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_TopComerciosMensual '" + fechaDesde + "','" + fechaHasta + "'",new object[] { }).ToList();

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }


    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteAhorroDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_ImporteAhorroPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImporteOriginalDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);//"01-20-2014";// 
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_ImporteOriginalPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerImportePagadoDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_ImportePagadoPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }


    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerArancelDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_ArancelPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerPuntosDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_PuntosPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();

                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> ObtenerFacturadoDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_FacturadoPorDias '" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerEstadoTerminales()
    {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + "" + "','" + "" + "','" + "" + "'," + 0 + "," + 0, new object[] { }).ToList();
/*
                var aux = dbContext.GetReporteTerminales("", "", "", 0, 0)
                    .Select(x => new TerminalesViewModel()
                    {
                        Estado = "",
                        Fecha_Activ = x.Fecha_Activacion,
                        Fecha_Reprog = x.Fecha_Reprogramacion,
                        Reprogramado = x.Reprogramado ? "Si" : "No",
                        Invalido = x.Comercio_Inválido ? "Si" : "No",
                        Activo = x.Activo ? "Si" : "No",
                        CantTR = x.CantTR,
                        UltimoImporte = x.UltimoImporte,
                        Tipo = x.Tipo
                    }).ToList();
                */
                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }

                //var prevList = aux.ToList();
                int negro = aux.Count(x => x.Estado == "Negro");
                int naranja = aux.Count(x => x.Estado == "Naranja");
                int verde = aux.Count(x => x.Estado == "Verde");
                int rojo = aux.Count(x => x.Estado == "Rojo");
                int amarillo = aux.Count(x => x.Estado == "Amarillo");
                int azul = aux.Count(x => x.Estado == "Azul");
                int desconocido = aux.Count(x => x.Estado == "Desconocido");
                int noprobado = aux.Count(x => x.Estado == "Marron");

                list.Add(new Chart() { data = negro, label = "<a href=\"javascript:verDetalleTerminales('Negro');\">Baja (" + negro + ")</a>" });
                list.Add(new Chart() { data = naranja, label = "<a href=\"javascript:verDetalleTerminales('Naranja');\">Inválido (" + naranja + ")</a>" });
                list.Add(new Chart() { data = verde, label = "<a href=\"javascript:verDetalleTerminales('Verde');\">Activo Mayor a $1 (" + verde + ")</a>" });
                list.Add(new Chart() { data = rojo, label = "<a href=\"javascript:verDetalleTerminales('Rojo');\">No Reprogramado (" + rojo + ")</a>" });
                list.Add(new Chart() { data = amarillo, label = "<a href=\"javascript:verDetalleTerminales('Amarillo');\">Activo Menor a $1 (" + amarillo + ")</a>" });
                list.Add(new Chart() { data = azul, label = "<a href=\"javascript:verDetalleTerminales('Azul');\">Web (" + azul + ")</a>" });
                list.Add(new Chart() { data = desconocido, label = "<a href=\"javascript:verDetalleTerminales('Desconocido');\">Desconocido (" + desconocido + ")</a>" });
                list.Add(new Chart() { data = noprobado, label = "<a href=\"javascript:verDetalleTerminales('Marron');\">No Probado (" + noprobado + ")</a>" });
            }
        }

        return list;
    }

    #endregion

    #region Estadisticas Superiores

    //Generales
    [WebMethod(true)]
    public static string obtenerTotalSociosPorSexo()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalSociosPorSexo", new object[] { }).ToList();
                if (list.Any())
                {
                    html = "F: " + int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "") + " - M: " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "") + " <br/> I: " + int.Parse(list[2].data.ToString()).ToString("N").Replace(",00", "");
                    decimal total = list[0].data + list[1].data + list[2].data;
                    html += "," + total.ToString("N").Replace(",00", "");
                }
                else
                    html = "Error";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalEmails()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalEmails", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalCelulares()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalCelulares", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalTarjetasActivas()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalTarjetasActivas", new object[] { }).ToList();
                if (list.Any())
                    html = int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "") + " / " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    //Mensuales    
    [WebMethod(true)]
    public static string obtenerTotalPuntosMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            /*SqlParameter p_FechaDesde = new SqlParameter("@FechaDesde", fechaDesde);
            SqlParameter p_FechaHasta = new SqlParameter("@FechaHasta", fechaHasta);
            SqlParameter[] parametros = new SqlParameter[2];
            parametros[0] = p_FechaDesde;
            parametros[1] = p_FechaHasta;

            var sql = "Dashboard_TotalPuntosMensual";// +fechaDesde + "','" + fechaHasta + "'";
            SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, sql, parametros);
            if(reader.HasRows)
            {
                while (reader.Read())
                    html= reader.GetValue(0).ToString();
            }
            else
                html = "";*/
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalPuntosMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalArancelMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalArancelMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalTRMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalTRMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalTasaUsoMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalTasaUsoMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("#0.00");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerPromedioTicketMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PromedioTicketMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalFacturadoMensual()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalFacturadoMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    #endregion

    #region Otros

    [WebMethod(true)]
    public static string obtenerDetalleTerminales(string estado)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + estado + "','" + "" + "','" + "" + "'," + 0 + "," + 0, new object[] { }).ToList();

                /*
                var list = dbContext.GetReporteTerminales(estado, "", "", 0, 0).ToList();
                var aux = list.Select(x => new TerminalesViewModel()
                        {
                            SDS = x.SDS,
                            Nombre = x.Nombre,
                            Estado = "",
                            Domicilio = x.Domicilio,
                            Localidad = x.Localidad,
                            Fecha_Carga = x.Fecha_Carga,
                            Fecha_Alta = x.Fecha_Alta,
                            Dealer = x.Dealer,
                            Tipo = x.Tipo + " " + x.Tipo_Terminal,
                            Terminal = x.Terminal,
                            Establecimiento = x.Establecimiento,
                            Fecha_Activ = x.Fecha_Activacion,
                            Fecha_Reprog = x.Fecha_Reprogramacion,
                            Reprogramado = x.Reprogramado ? "Si" : "No",
                            Invalido = x.Comercio_Inválido ? "Si" : "No",
                            Activo = x.Activo ? "Si" : "No",
                            Observaciones = x.Observaciones,
                            CantTR = x.CantTR,
                            UltimoImporte = x.UltimoImporte,
                            Marca = x.NombreMarca,
                            Franquicia = x.NombreFranquicia
                        }).ToList(); //.ToDataTable();
                */
                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }
                aux = aux.Where(x => x.Estado.ToLower() == estado.ToLower()).ToList();

                if (aux.Any())
                {
                    foreach (var detalle in aux)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.SDS + "</td>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.NombreFranquicia + "</td>";
                        html += "<td>" + detalle.NombreMarca + "</td>";
                        html += "<td>" + detalle.Domicilio + "</td>";
                        html += "<td>" + detalle.FechaCarga + "</td>";
                        html += "<td>" + detalle.FechaAlta + "</td>";
                        html += "<td>" + detalle.Terminal + "</td>";
                        html += "<td>" + detalle.Establecimiento + "</td>";
                        html += "<td>" + detalle.FechaActivacion + "</td>";
                        html += "<td>" + detalle.FechaReprogramacion.ToString("dd/MM/yyyy") + "</td>";
                        html += "<td>" + (detalle.Reprogramado ? "Si":"No") + "</td>";
                        html += "<td>" + (detalle.ComercioInvalido ? "Si":"No") + "</td>";
                        html += "<td>" + (detalle.Activo ? "Si":"No") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='14'>No hay un detalle disponible</td></tr>";

            }
        }

        return html;
    }

    #endregion
}