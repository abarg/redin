﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reprogramaciones.aspx.cs" Inherits="modulos_reportes_reprogramaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/reprogramaciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Reportes</a></li>
            <li class="last">Reprogramaciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Reprogramaciones</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los comercios se han actualizado correctamente</div>
            <form runat="server" id="formReporteComercios" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <button class="btn btn-success" type="button" id="btnDescargar" onclick="descargar();">Generar reportes</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                        <br />
                        <br />
                        <a href="" id="lnkDownload" class="download" download="Reprogramaciones AMEX" style="display:none">Descargar reporte AMEX</a>
                        <br />
                        <a href="" id="lnkDownload2" class="download" download="Reprogramaciones LAPOS" style="display:none">Descargar reporte LAPOS</a>
                        <br />
                        <a href="" id="lnkDownload3" class="download" download="Reprogramaciones POSNET" style="display:none">Descargar reporte POSNET</a>
                        <br /><br />
                        <button class="btn btn-default" type="button" id="btnActualizarFecha" style="display:none" onclick="actualizarFecha();">Guardar reprogramaciones</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</asp:Content>

