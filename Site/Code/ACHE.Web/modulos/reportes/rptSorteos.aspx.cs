﻿using ACHE.Extensions;
using ACHE.Model;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_reportes_rptSorteos : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            cargarCombos();

        }
    }

    private void cargarCombos()
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var sorteos = dbContext.Sorteos.OrderBy(x => x.Titulo).ToList();
                this.ddlSorteos.DataSource = sorteos;
                this.ddlSorteos.DataValueField = "IDSorteo";
                this.ddlSorteos.DataTextField = "Titulo";
                this.ddlSorteos.DataBind();
                this.ddlSorteos.Items.Insert(0, new ListItem("", ""));

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesSorteos.Include("Sorteos").Include("Tarjetas").AsQueryable();
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    aux = aux.Where(x => x.Fecha >= dtDesde).AsQueryable();
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    aux = aux.Where(x => x.Fecha <= dtHasta).AsQueryable();
                }

                var result = aux
                      .OrderBy(x => x.Fecha)
                      .Select(x => new
                      {
                          IDSorteo = x.IDSorteo,
                          Sorteo = x.Sorteos.Titulo,
                          Socio = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + "," + x.Tarjetas.Socios.Nombre : "",
                          IDSocio = x.Tarjetas.IDSocio,
                          Tarjeta = x.Tarjetas.Numero,
                          DNI = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                          Email = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Email : "",
                          Celular = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Celular : "",
                          Fecha = x.Fecha,
                          Latitud = x.Tarjetas.Socios.Domicilios.Latitud,//(-34.5976949).ToString(),
                          Longitud = x.Tarjetas.Socios.Domicilios.Longitud,//(-58.44220330000002).ToString(),//
                          Domicilio = x.Tarjetas.Socios.Domicilios.Domicilio + ", " + x.Tarjetas.Socios.Domicilios.Ciudades.Nombre + ", " + x.Tarjetas.Socios.Domicilios.Provincias.Nombre
                      }).ToList();

                return result.ToList().AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList

            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string exportar(string idSorteo, string socio, string tarjeta, string fechaDesde, string fechaHasta)
    {
        string fileName = "Sorteos";
        string path = "/tmp/";
        try
        {

            DataTable dt = new DataTable();
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesSorteos.Include("Sorteos").Include("Tarjetas").AsQueryable();
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.Fecha >= dtDesde).AsQueryable();
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);
                    result = result.Where(x => x.Fecha <= dtHasta).AsQueryable();
                }
                if (idSorteo != string.Empty)
                {
                    int idsorteo = int.Parse(idSorteo);
                    result = result.Where(x => x.IDSorteo == idsorteo).AsQueryable();
                }
                if (socio != string.Empty)
                    result = result.Where(x => x.Tarjetas.Socios.Nombre.ToLower().Contains(socio.ToLower()) || x.Tarjetas.Socios.Apellido.ToLower().Contains(socio.ToLower())).AsQueryable();

                if (tarjeta != string.Empty)
                    result = result.Where(x => x.Tarjetas.Numero.Contains(tarjeta)).AsQueryable();

                dt = result.ToList()
                      .OrderBy(x => x.Fecha)
                      .Select(x => new
                      {
                          Fecha = (x.Fecha.HasValue) ? x.Fecha.Value.ToString("dd/MM/yyyy") : "",
                          Sorteo = x.Sorteos.Titulo,
                          Socio = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Apellido + "," + x.Tarjetas.Socios.Nombre : "",
                          Tarjeta = x.Tarjetas.Numero,
                          DNI = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.NroDocumento : "",
                          Email = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Email : "",
                          Celular = x.IDTarjeta.HasValue && x.Tarjetas.IDSocio.HasValue ? x.Tarjetas.Socios.Celular : ""

                      }).ToList().ToDataTable();


            }

            if (dt.Rows.Count > 0)
            {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}