﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_reportes_cc_general : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (CurrentUser.Tipo != "A")
            //    Response.Redirect("/default.aspx");
        }
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter, string idComercio, string estado)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            int idAux = 0;
            if (idComercio != string.Empty)
                idAux = int.Parse(idComercio);

            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.ObtenerCCEmpresaGeneral(idAux).ToList();
                if (estado != "")
                {
                    if (estado == "Al dia")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value >= -1 && x.Saldo.Value <= 1).ToList();
                    else if (estado == "A favor")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value < -1).ToList();
                    else if (estado == "Deudor")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value > 1).ToList();
                }

                var aux = list.AsQueryable().Select(x => new CCEmpresaGeneralViewModel()
                    {
                        ID = x.IDComercio,
                        Comercio = x.NombreFantasia,
                        Importe = x.Importe.HasValue ? x.Importe.Value : 0,
                        Abonado = x.Abonado,
                        Saldo = x.Saldo.HasValue ? x.Saldo.Value : 0,
                        FechaUltimoPago = x.UltimoPago
                    }).ToDataSourceResult(take, skip, sort, filter);

                return aux;
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string Exportar(string idComercio, string estado)
    {
        string fileName = "CuentaCorrienteGeneral";
        string path = "/tmp/";
        try
        {

            int idAux = 0;
            if (idComercio != string.Empty)
                idAux = int.Parse(idComercio);

            DataTable dt = new DataTable();
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.ObtenerCCEmpresaGeneral(idAux).ToList();
                if (estado != "")
                {
                    if (estado == "Al dia")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value >= -1 && x.Saldo.Value <= 1).ToList();
                    else if (estado == "A favor")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value < -1).ToList();
                    else if (estado == "Deudor")
                        list = list.Where(x => x.Saldo.HasValue && x.Saldo.Value > 1).ToList();
                }

                dt = list.Select(x => new CCEmpresaGeneralViewModel()
                {
                    ID = x.IDComercio,
                    Comercio = x.NombreFantasia,
                    Importe = x.Importe.HasValue ? x.Importe.Value : 0,
                    Abonado = x.Abonado,
                    Saldo = x.Saldo.HasValue ? x.Saldo.Value : 0,
                    FechaUltimoPago = x.UltimoPago
                }).ToList().ToDataTable();
            }

            if (dt.Rows.Count > 0)
            {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }
            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [WebMethod(true)]
    public static string obtenerDetalle(int idComercio)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                DateTime dtDesde = DateTime.Parse("01/01/1900");
                DateTime dtHasta = DateTime.Parse("01/01/2100");
                var list = dbContext.ObtenerCCEmpresa();// idComercio, dtDesde, dtHasta).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Detalle + "</td>";
                        if (detalle.Detalle != "Saldo")
                            html += "<td>" + detalle.Fecha + "</td>";
                        else
                            html += "<td></td>";
                        html += "<td>" + (detalle.Importe.HasValue ? detalle.Importe.Value.ToString("N2") : "") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

}