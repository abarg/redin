﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_reprogramaciones : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {

    }

    private static DataTable getResults(string tipo) {
        DataTable dt = new DataTable();
        using (var dbContext = new ACHEEntities()) {
            List<ReporteReprogramacionesViewModel> list = new List<ReporteReprogramacionesViewModel>();
            switch (tipo.ToUpper()) {
                case "AMEX":
                    list = dbContext.ReporteReprogramacionesAMEX
                        .OrderBy(x => x.NombreFantasia)
                        .Select(x => new ReporteReprogramacionesViewModel() {
                            NombreFantasia = x.NombreFantasia,
                            RazonSocial = x.RazonSocial,
                            CUIT = x.CUIT,
                            Direccion = x.Dirección,
                            Localidad = x.Localidad,
                            CodigoPostal = x.CP,
                            Contacto = x.Contacto,
                            NroTerminal = x.NroTerminal,
                            NumEst = x.NumEst,
                            Observacion = x.Observaciones
                        }).ToList();

                    dt = list.Select(x => new {
                        Comercio = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        CUIT = x.CUIT,
                        Direccion = x.Direccion,
                        Localidad = x.Localidad,
                        CP = x.CodigoPostal,
                        Contacto = x.Contacto,
                        NroTerminal = x.NroTerminal,
                        NroEstablecimiento = x.NumEst,
                        Observacion = x.Observacion
                    }).ToList().ToDataTable();
                    break;
                case "LAPOS":
                    list = dbContext.ReporteReprogramacionesLAPOS
                        .OrderBy(x => x.NombreFantasia)
                        .Select(x => new ReporteReprogramacionesViewModel() {
                            NroTerminal = x.NroTerminal,
                            MarcaVisa = x.MarcaVisa,
                            Moneda = x.Moneda,
                            NumEst = x.NumEst,
                            CodigoSolicitante = x.CodigoSolicitante,
                            RazonSocial = x.RazonSocial,           
                            NombreFantasia = x.NombreFantasia,
                            CUIT = x.CUIT,
                            Observacion = x.Observacion
                        }).ToList();

                    dt = list.Select(x => new {
                        NroTerminal = x.NroTerminal,
                        MarcaVisa = x.MarcaVisa,
                        Moneda = x.Moneda,
                        Establecimiento = x.NumEst,
                        CodigoSolicitante = x.CodigoSolicitante,
                        RazonSocial = x.RazonSocial,
                        NombreFantasia = x.NombreFantasia,
                        CUIT = x.CUIT,
                        Observacion = x.Observacion,
                    }).ToList().ToDataTable();
                    break;
                case "POSNET":
                    list = dbContext.ReporteReprogramacionesPOSNET
                        .OrderBy(x => x.NombreFantasia)
                        .Select(x => new ReporteReprogramacionesViewModel() {
                            NroTerminal = x.NroTerminal,
                            Marca = x.Marca,
                            NumEst = x.NumEst,
                            RazonSocial = x.RazonSocial,
                            NombreFantasia = x.NombreFantasia,
                            CUIT = x.CUIT,
                            Observacion = x.Observacion
                        }).ToList();

                    dt = list.Select(x => new {
                        NroTerminal = x.NroTerminal,
                        Marca = x.Marca,
                        Establecimiento = x.NumEst,
                        RazonSocial = x.RazonSocial,
                        NombreFantasia = x.NombreFantasia,
                        CUIT = x.CUIT,
                        Observacion = x.Observacion,
                    }).ToList().ToDataTable();
                    break;
            }
        }

        return dt;
    }

    [WebMethod(true)]
    public static string Exportar(string tipo) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            string fileName = "Reprogramaciones_" + tipo.ToUpper();
            string path = "/tmp/";
            try {
                DataTable dt = getResults(tipo.ToUpper());
                if (dt.Rows.Count > 0)
                    ExportarAExcel(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encontraron registros");

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [WebMethod(true)]
    public static void actualizarComercios()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.ExecuteSqlCommand("UPDATE TERMINALES SET POSFechaReprogramacion=GETDATE() WHERE POSSistema IN ('AMEX','POSNET','LAPOS') and Estado != 'DE' and Activo=1");
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }
}