﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="modulos_reportes_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <!--[if lt IE 9]>
		<script src="/lib/flot/excanvas.min.js"></script>
    <![endif]-->
    <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }

        .modal.modal-wide .modal-dialog {
          width: 90%;
        }
        .modal-wide .modal-body {
          overflow-y: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row hide">
		<div class="col-sm-12 tac">
            <h3 class="heading">Estadísticas generales</h3>
            <ul class="ov_boxes">
                <li>
					<div class="p_bar_down p_canvas">
                        <img src="../../img/dashboard/sex.png" style="width:48px" />
					</div>
					<div class="ov_text" style="padding:0; height:70px">
						<strong id="resultado_sexo" style="font-size: 13px;padding-top: 9px;">Calculando...</strong><%--F: 100 - M: 300--%>
						Total Socios: <span id="totalSexo"></span>
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/email.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_mails">Calculando...</strong>
						Cantidad de <br />mails
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/MetroUI_Phone.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_cel">Calculando...</strong>
						Cantidad de <br />celulares
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/credit_card_yellow.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_total_tarjetas_activas" style="font-size: 13px">Calculando...</strong>
						Cantidad de <br />Tarjetas activas
					</div>
				</li>
            </ul>
        </div>
    </div>
    <div class="row hide">
		<div class="col-sm-12 tac">
			<h3 class="heading"><asp:Literal runat="server" ID="litFechaTit"></asp:Literal></h3>
            <ul class="ov_boxes">
				<li>
					<div class="p_bar_up p_canvas">
                        <img src="../../img/dashboard/money-graph.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_ticketpromedio_mensual">Calculando...</strong>
						Promedio ticket <br />mensual
					</div>
				</li>
				<li>
					<div class="p_line_up p_canvas">
                         <img src="../../img/dashboard/Tracking_3D.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_tasauso_mensual">Calculando...</strong>
                        Tasa de <br />uso
					</div>
				</li>
				<li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/transaction.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_tr_mensual">Calculando...</strong>
						Cantidad de <br />trans. mensuales
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/billete.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_arancel_mensual">Calculando...</strong>
						Total arancel <br />mensual
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/coins.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_puntos_mensual">Calculando...</strong>
						Total puntos <br />mensuales
					</div>
				</li>
                <li>
					<div class="p_line_down p_canvas">
                        <img src="../../img/dashboard/invoice.png" style="width:48px" />
					</div>
					<div class="ov_text">
						<strong id="resultado_facturacion_mensual">Calculando...</strong>
						Total <br />facturado 
					</div>
				</li>
			</ul>
		</div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-lg-6"  >
            <div class="heading clearfix">
                <h3 class="pull-left">TOP 10 Mejores socios</h3>
            </div>
            <a onclick="mostrarSociosMensual();" href="#">Vista mensual&nbsp;|</a> 
            <a onclick="mostrarSociosGeneral();" href="#" >Histórico</a>
    
            <table id="tblSocios"class="table table-striped table-bordered mediaTable" style="display: none" >
				<thead>
					<tr>
						<th class="essential persist">Nombre y Apellido</th>
						<th class="optional">Tarjeta</th>
						<th class="optional">Marca</th>
						<th class="essential">Importe</th>
					</tr>
				</thead>
				<tbody id="bodySocios">
                    <tr><td colspan="4">Calculando...</td></tr>
				</tbody>
			</table>

            <table id="tblSociosMensual"class="table table-striped table-bordered mediaTable">
                <thead>
                    <tr>
                        <th class="essential persist">Nombre y Apellido</th>
                        <th class="optional">Tarjeta</th>
                        <th class="optional">Marca</th>
                        <th class="essential">Importe</th>
                    </tr>
                </thead>
                <tbody id="bodySociosMes">
                    <tr>
                        <td colspan="4">Calculando...</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-sm-6 col-lg-6">
            <div class="heading clearfix">
                <h3 class="pull-left">TOP 10 Mejores comercios</h3>
            </div>
            <a onclick="mostrarComerciosMensual();" href="#">Vista mensual&nbsp;|</a>
            <a onclick="mostrarComerciosGeneral();" href="#">Histórico</a>
            <br/>
            <table id="tblComerciosMensual" class="table table-striped table-bordered mediaTable">
                <thead>
                    <tr>
                        <th class="essential persist">Nombre y Apellido</th>
                        <th class="optional">Tarjeta</th>
                        <th class="essential">Importe</th>
                    </tr>
                </thead>
                <tbody id="bodyComerciosMes">
                    <tr>
                        <td colspan="3">Calculando...</td>
                    </tr>
                </tbody>
            </table>
            <table id="tblComercios" class="table table-striped table-bordered mediaTable" style="display: none" >
				<thead>
					<tr>
						<th class="essential persist">Comercio</th>
						<th class="optional">Domicilio</th>
						<!--th class="optional">Nro. Estab.</!--th-->
						<th class="essential">Importe</th>
					</tr>
				</thead>
				<tbody id="bodyComercios">
                    <tr><td colspan="3">Calculando...</td></tr>
				</tbody>
			</table>
        </div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading" id="lblTerminalesPorPOS">Terminales por pos </h3>
            <div id="fl_terminales_activas" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
			<h3 class="heading" id="lblEstadoTerminales">Estado de las terminales</h3>
            <div id="fl_estado_terminales" style="height:270px;width:90%;margin:15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    
    <div class="modal modal-wide fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>SDS</th> 
                                <th>Nombre</th> 
                                <th>Franquicia</th> 
                                <th>Marca</th> 
                                <th>Domicilio</th> 
                                <th>Fecha Carga</th> 
                                <th>Fecha Alta</th> 
                                <th>Terminal</th>
                                <th>Establecimiento</th>
                                <th>Fecha Activ</th>
                                <th>Fecha Reprog</th>
                                <th>Reprogramado</th>
                                <th>Invalido</th>
                                <th>Activo</th>
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


	<!-- charts -->
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js") %>"></script>
    <%--<script src="/lib/flot/jquery.flot.pyramid.js"></script>--%>


    <!-- charts functions -->
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/dashboard.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

