﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.IO;
using ClosedXML.Excel;


public partial class modulos_reportes_rptTarjetasImpresas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region Tablas HTML


    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<TarjetasImpresas> ObtenerTarjetasEmitidas(int desde, int hasta)
    {
        List<TarjetasImpresas> result = null;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var sql = ("exec Dashboard_TarjetasEmitidas_Listado " + desde + "," + hasta);
                result = dbContext.Database.SqlQuery<TarjetasImpresas>(sql, new object[] { }).ToList();
                result = result.Select(x => new TarjetasImpresas()
                {
                    Marca = x.Marca,
                    Activas = x.Activas,
                    Inactivas = x.Total - x.Activas,
                    Total = x.Total,
                    Desde = x.Desde,
                    Hasta=x.Hasta
                    
                }).ToList();
            }
        }
        return result;
    }


    [WebMethod(true)]
    public static string ExportarTarjetasEmitidas(int desde, int hasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                string fileName = "emitidas";
                string path = "/tmp/";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var sql=("exec Dashboard_TarjetasEmitidas_Listado " + desde + "," + hasta);
                    var info = dbContext.Database.SqlQuery<TarjetasImpresas>(sql, new object[] { }).ToList();
                    info = info.Select(x => new TarjetasImpresas()
                    {
                        Marca = x.Marca,
                        Activas = x.Activas,
                        Inactivas = x.Total - x.Activas,
                        Total = x.Total,
                        Desde = x.Desde,
                        Hasta=x.Hasta
                        
                        
                    }).ToList();
                    if (info.Any())
                    {
                        dt = info.Select(x => new
                        {
                            Marca = x.Marca,
                            Activas = x.Activas,
                            Inactivas = x.Inactivas,
                            Total = x.Total,
                            Desde = x.Desde,
                            Hasta = x.Hasta
                        }).ToList().ToDataTable();

                        if (dt.Rows.Count > 0)
                            generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                        else
                            throw new Exception("No se encuentran datos para los filtros seleccionados");
                        return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
    #endregion
}