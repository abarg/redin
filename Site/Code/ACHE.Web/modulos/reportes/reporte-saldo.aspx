﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="reporte-saldo.aspx.cs" Inherits="modulos_reportes_reporte_saldo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/reporte-saldo.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Reportes</a></li>
            <li class="last">Reporte Saldo de Facturación</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Reporte Saldos de Facturación</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none"></div>
            <form>
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display: none" />
                            <a href="" id="lnkDownload" download="Facturas" style="display: none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid">
            </div>
            <br />
            <br />
        </div>
    </div>
    <div class="bs-example-modal-lg">
    <div class="modal fade bs-example-modal-lg" id="modalDetalle">
        <div class="modal-dialog bs-example-modal-lg" >
            <div class="modal-content bs-example-modal-lg">
                <div class="modal-header bs-example-modal-lg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body"style="max-height: 800px;">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink">
                        <thead id="headDetalle">
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>