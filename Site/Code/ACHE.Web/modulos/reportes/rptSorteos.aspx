﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="rptSorteos.aspx.cs" Inherits="modulos_reportes_rptSorteos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <%--  <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>--%>
    <script src="//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAEg7-84AYcNlPK0FZQm_zUAfFcYmOFleQ" async="" defer="defer" type="text/javascript"></script>
        <style type="text/css">
        #map-canvas {
            height: 280px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Reportes</a></li>
            <li class="last">Sorteos</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Sorteos</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formReporte" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                         <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Sorteo</label>
                            <asp:DropDownList runat="server" ID="ddlSorteos" CssClass="form-control"></asp:DropDownList>
                        </div>
                           <div class="col-sm-3">
						    <label>Socio</label>
                            <input type="text" id="txtSocio" value="" maxlength="100" class="form-control"/>
					    </div>
                         <div class="col-sm-3">
						    <label>Nro. de Tarjeta</label>
						    <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control number" />
                        </div>
                      
                    </div>
                   
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnVerTodos" onclick="verTodos();">Ver todos</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Terminales" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modalTitle">Mapa</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/rptSorteo.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

