﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;


public partial class modulos_reportes_reporteSMS : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToShortDateString();
            this.txtFechaHasta.Text = DateTime.Now.ToShortDateString();
            cargarDatos();
        }
    }    
    
    private void cargarDatos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
            Marcas todas = new Marcas();
            todas.Nombre = "Todas las marcas";
            todas.IDMarca = 0;
            marcas.Insert(0, todas);

            ddlMarcas.DataSource = marcas;
            ddlMarcas.DataValueField = "IDMarca";
            ddlMarcas.DataTextField = "Nombre";
            ddlMarcas.DataBind();
        }
    }
    
    private static DatosSMS obtenerInfoSMS(DateTime fechaDesde, DateTime fechaHasta, int IDMarca)
    {
        DatosSMS cantEnviados = new DatosSMS();
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

        using (var dbContext = new ACHEEntities())
        {
            var total = dbContext.Database.SqlQuery<DatosSMS>("exec CantidadSMS '" + fechaDesde.ToString(formato) + "','" + fechaHasta.ToString(formato) + "', " + IDMarca, new object[] { }).ToList();

            if (total.Any())
            {
                return total[0];

            }

            else
            {
                return null;
            }

            //   return total;
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta, int IDMarca, string mensaje)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                var result = dbContext.Rpt_CantidadSMS(DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta), IDMarca, mensaje)
                      .OrderBy(x => x.cant)
                      .Select(x => new
                   {
                       IDMarca = x.IDMarca != null && x.IDMarca != 0 ? x.IDMarca : 0,
                       Nombre = (x.Nombre == null) ? "Envio Automático" : x.Nombre,
                       cantEnviados = x.cantEnviados,
                       cantNoenviados = x.cantNoenviados,
                       cant = x.cant,
                       Costo = x.Costo,
                       Tipo = x.Tipo,
                       PeriodoDesde = fechaDesde,
                       PeriodoHasta = fechaHasta
                   }).ToList();

                return result.ToList().AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string obtenerDetalle(string desde, string hasta, int idMarca, string mensaje, string costo)
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.SMSEnvios.Where(x => x.Enviado == true).ToList();
                if (idMarca > 0)
                    list = list.Where(x => x.IDMarca == idMarca).ToList();

                if (desde != "" && desde != null)
                {
                    DateTime dtDesde = DateTime.Parse(desde);
                    list = list.Where(x => x.FechaEnvio >= dtDesde).ToList();
                }
                if (hasta != "" && hasta != null)
                {
                    DateTime dtHasta = DateTime.Parse(hasta);
                    list = list.Where(x => x.FechaEnvio <= dtHasta).ToList();
                }

                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.FechaEnvio + "</td>";
                        html += "<td>" + detalle.PhoneNumber + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }


    public static string convertirFecha(string fecha)
    {
        var input = fecha.Substring(4, 11);//formato MMM dd YYYY
        string[] fechaSplit = input.Split(' ');

        string numeroMes = convertirANumeroMes(fechaSplit[0]);

        //var format = "MM/dd/YYYY";

        input = fechaSplit[1] + "/" + numeroMes + "/" + fechaSplit[2];
        return input;
        //var date = DateTime.Parse(input);
        //return date.ToString("dd/MM/yyyy");
    }

    public static string convertirANumeroMes(string nombreMes)
    {

        switch (nombreMes)
        {
            case "Jan": return "01";
            case "Feb": return "02";
            case "Mar": return "03";
            case "Apr": return "04";
            case "May": return "05";
            case "Jun": return "06";
            case "Jul": return "07";
            case "Aug": return "08";
            case "Sep": return "09";
            case "Oct": return "10";
            case "Nov": return "11";
            case "Dec": return "12";
        }
        return "";

    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string desde, string hasta, int idMarca, string mensaje, string costo)
    {

        string fileName = "SMS";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.SMSEnvios.Where(x => x.Enviado == true).AsEnumerable();

                    if (idMarca > 0)
                        list = dbContext.SMSEnvios.Where(x => x.IDMarca == idMarca && x.Enviado == true).ToList();


                    if (desde != "" && desde != null)
                    {
                        DateTime dtDesde = DateTime.Parse(desde);
                        list = list.Where(x => x.FechaEnvio >= dtDesde).ToList();
                    }
                    if (hasta != "" && hasta != null)
                    {
                        DateTime dtHasta = DateTime.Parse(hasta);
                        list = list.Where(x => x.FechaEnvio <= dtHasta).ToList();
                    }

                    if (decimal.Parse(costo) >= 0)
                    {
                        decimal costoAux = Convert.ToDecimal(costo.Replace(".", ","));
                        list = list.Where(x => x.Costo == costoAux).ToList();
                    }

                    dt = list.ToList().Select(x => new
                    {
                        fecha = x.FechaEnvio,
                        telefono = x.PhoneNumber
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}
