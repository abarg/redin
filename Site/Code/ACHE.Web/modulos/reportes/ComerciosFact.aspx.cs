﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_ComerciosFact : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToShortDateString();
            this.txtFechaHasta.Text = DateTime.Now.ToShortDateString();
            this.cargarMarcas();
        }
    }

    //private void cargarCombos()
    //{
    //    try
    //    {
    //        bComercio bComercio = new bComercio();
    //        List<eComercio> listComercios = bComercio.getComercios();

    //        this.ddlNombre.DataSource = listComercios;
    //        this.ddlNombre.DataValueField = "NombreEst";
    //        this.ddlNombre.DataTextField = "NombreEst";
    //        this.ddlNombre.DataBind();
    //        this.ddlNombre.Items.Insert(0, new ListItem("", ""));
    //    }
    //    catch(Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    private void cargarMarcas()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcas();
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("Todas las marcas", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string buscar(string Tipo, string FechaDesde, string FechaHasta, string SDS, string Nombre, string CantOper, int IDMarca)
    {
        try
        {
            DateTime dtDesde = DateTime.Parse(Convert.ToDateTime(FechaDesde).ToString("dd/MM/yyyy"));
            DateTime dtHasta = DateTime.Parse(Convert.ToDateTime(FechaHasta).ToString("dd/MM/yyyy"));

            if (dtHasta.GetDaysDiff(dtDesde) > 31)
                throw new Exception("El período de fechas no puede superar los 31 días");

            bComercio bComercio = new bComercio();
            DataTable dt = new DataTable();

            if (FechaDesde == "") FechaDesde = null;
            if (FechaHasta == "") FechaHasta = null;
            if (SDS == "") SDS = null;
            if (Nombre == "") Nombre = null;
            if (CantOper == "") CantOper = null;

            SqlParameter p_Tipo = new SqlParameter("@Tipo", Tipo);
            SqlParameter p_FechaDesde = new SqlParameter("@FechaDesdeTrans", dtDesde.ToString("yyyy/MM/dd"));
            SqlParameter p_FechaHasta = new SqlParameter("@FechaHastaTrans", dtHasta.ToString("yyyy/MM/dd"));
            SqlParameter p_SDS = new SqlParameter("@SDS", SDS);
            SqlParameter p_Nombre = new SqlParameter("@NombreEst", Nombre);
            SqlParameter p_CantOper = new SqlParameter("@CantOperaciones", CantOper);
            SqlParameter p_IDMarca = new SqlParameter("@IDMarca", IDMarca);

            SqlParameter[] parametros = new SqlParameter[7];
            parametros[0] = p_Tipo;
            parametros[1] = p_FechaDesde;
            parametros[2] = p_FechaHasta;
            parametros[3] = p_SDS;
            parametros[4] = p_Nombre;
            parametros[5] = p_CantOper;
            parametros[6] = p_IDMarca;

            SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "GetReporteComercios", parametros);
            dt.Load(reader);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table  id='grid'><colgroup>");

                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    sb.Append("<col style='width:100px' ></col>");
                }

                sb.Append("</colgroup><thead><tr>");

                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    sb.Append("<th>" + dc.ColumnName + "</th>");
                }
                sb.Append("</thead></tr><tbody>");

                //write table data
                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    sb.Append("<tr>");
                    foreach (System.Data.DataColumn dc in dt.Columns)
                    {
                        sb.Append("<td>" + dr[dc].ToString() + "</td>");
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");

                return sb.ToString();
            }
            else
                throw new Exception("No se encuentran datos para los filtros seleccionados");

        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string exportar(string Tipo, string FechaDesde, string FechaHasta, string SDS, string Nombre, string CantOper, int IDMarca)
    {
        string fileName = "ComerciosFact";
        string path = "/tmp/";
        try
        {
            DateTime dtDesde = DateTime.Parse(Convert.ToDateTime(FechaDesde).ToString("dd/MM/yyyy"));
            DateTime dtHasta = DateTime.Parse(Convert.ToDateTime(FechaHasta).ToString("dd/MM/yyyy"));

            if (dtHasta.GetDaysDiff(dtDesde) > 31)
                throw new Exception("El período de fechas no puede superar los 31 días");

            bComercio bComercio = new bComercio();
            DataTable dt = new DataTable();

            if (FechaDesde == "") FechaDesde = null;
            if (FechaHasta == "") FechaHasta = null;
            if (SDS == "") SDS = null;
            if (Nombre == "") Nombre = null;
            if (CantOper == "") CantOper = null;

            SqlParameter p_Tipo = new SqlParameter("@Tipo", Tipo);
            SqlParameter p_FechaDesde = new SqlParameter("@FechaDesdeTrans", dtDesde.ToString("yyyy/MM/dd"));
            SqlParameter p_FechaHasta = new SqlParameter("@FechaHastaTrans", dtHasta.ToString("yyyy/MM/dd"));
            SqlParameter p_SDS = new SqlParameter("@SDS", SDS);
            SqlParameter p_Nombre = new SqlParameter("@NombreEst", Nombre);
            SqlParameter p_CantOper = new SqlParameter("@CantOperaciones", CantOper);
            SqlParameter p_IDMarca = new SqlParameter("@IDMarca", IDMarca);

            SqlParameter[] parametros = new SqlParameter[7];
            parametros[0] = p_Tipo;
            parametros[1] = p_FechaDesde;
            parametros[2] = p_FechaHasta;
            parametros[3] = p_SDS;
            parametros[4] = p_Nombre;
            parametros[5] = p_CantOper;
            parametros[6] = p_IDMarca;

            SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "GetReporteComercios", parametros);
            dt.Load(reader);
            if (dt.Rows.Count > 0)
            {
                if (Tipo.Equals("A")) fileName += "_Agrupado";
                else if (Tipo.Equals("D")) fileName += "_Detallado";
                ExportarAExcel(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
                throw new Exception("No se encuentran datos para los filtros seleccionados");

            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}