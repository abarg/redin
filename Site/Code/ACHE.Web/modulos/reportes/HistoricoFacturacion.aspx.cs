﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_HistoricoFacturacion : PaginaBase
{
    private static int idFranquicia;
    protected void Page_Load(object sender, EventArgs e)
    {
      
        cargarFranquicias();
        if (!String.IsNullOrEmpty(Request.QueryString["IDFranquicia"]))
        {
            var idFranq = Convert.ToInt32(Request.QueryString["IDFranquicia"]); ;
            generarTablaFranquicias(idFranq);
            ddlFranquicias.SelectedValue = idFranq.ToString();
            pnlAdmin.Visible = false;
            idFranquicia = idFranq;
        }
        else
        {
            generarTabla();
            pnlFranq.Visible = false;
            idFranquicia = 0;
        }
    }
    #region franquicia
    public int obtenerTotalSociosPorSexo(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalSociosPorSexoMensual '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data + total[1].data + total[2].data);
        else
            result = 0;

        return result;
    }

    public int obtenerTotalEmails(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalEmailsMensuales '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data);

        else
            result = 0;


        return result;
    }

    public int obtenerTotalCelulares(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalCelularesMensuales '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();

        if (total.Any())
            result = Math.Abs(total[0].data);

        else
            result = 0;

        return result;
    }

    public int obtenerTotalTarjetasActivas(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;


        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data);

        else
            result = 0;


        return result;
    }

    public decimal obtenerCantidadTarjetas(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_CantidadTarjetasUnicas '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data);
        else
            result = 0;

        return result;
    }

    public decimal obtenerFacturacionComercios(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_FacturacionComercios '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data);
        else
            result = 0;



        return result;
    }

    public decimal obtenerPromedioDescuento(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;
        var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromedioDescuentos '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(Convert.ToDecimal(total[0].data));
        else
            result = 0;


        return result;
    }

    public decimal obtenerPromedioPuntos(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;


        var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromPuntosMensual '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(Convert.ToDecimal(total[0].data));
        else
            result = 0;



        return result;
    }

    public decimal obtenerPromedioArancel(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;


        var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromedioArancel '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(Convert.ToDecimal(total[0].data));
        else
            result = 0;


        return result;
    }

    public int obtenerTotalTRMensual(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;




        var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
            result = Math.Abs(total[0].data);
        else
            result = 0;


        return result;
    }

    public decimal obtenerTotalTasaUsoMensual(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;



        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_TotalTasaUsoMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
            result = Convert.ToDecimal((total[0].data).ToString("0.00"));
        else
            result = 0;


        return result;
    }

    public decimal obtenerPromedioTicketMensual(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;


        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_PromedioTicketMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
            result = Convert.ToDecimal(Math.Abs(total[0].data).ToString("0.00"));
        else
            result = 0;



        return result;
    }

    public decimal obtenerTarjPropiasComerciosPropios(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
        {
            result = Math.Abs(total[0].data);
        }
        else
            result = 0;

        return result;
    }

    public decimal obtenerTarjPropiasOtrosComercios(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;



        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCt','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
        {
            result = Math.Abs(total[0].data);
        }
        else
            result = 0;



        return result;
    }

    public decimal obtenerOtrasTarjComerciosPropios(int idFranquicia, string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;


        var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TtCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
        if (total.Any())
        {
            result = Math.Abs(total[0].data);
        }
        else
            result = 0;


        return result;
    }



    private void  generarMesesFranq()
    {
        using (var dbContext = new ACHEEntities())
        {
            var ultimo = dbContext.HistoricoFacturacionFranquicias.Where(x => x.IDFranquicia ==idFranquicia).OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).FirstOrDefault();

            if (ultimo != null)
            {
                DateTime lastDay = new DateTime(ultimo.Anio ?? 0, ultimo.Mes ?? 0, 1);
                int monthNow = DateTime.Now.Month;
                int yearNow = DateTime.Now.Year;


                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                string fechaDesde;
                string fechaHasta;
                int cantMeses = ((yearNow - ultimo.Anio ?? 0) * 12) + monthNow - ultimo.Mes ?? 0;
                if (cantMeses > 1)//el mes anterior y el actual no estan generados asi que no se acualizan 
                {
                    for (int i = 1; i <= cantMeses; i++)
                    {
                        HistoricoFacturacionFranquicias entity = new HistoricoFacturacionFranquicias();
                        entity.IDFranquicia = idFranquicia;

                        entity.Anio = lastDay.AddMonths(i).Year;
                        entity.Mes = lastDay.AddMonths(i).Month;

                        fechaDesde = lastDay.AddMonths(i).ToString(formato);
                        fechaHasta = lastDay.AddMonths(i + 1).AddDays(-1).ToString(formato);
                        entity.PromedioTickets = obtenerPromedioTicketMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTRMensual = obtenerTotalTRMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TasaDeUso = obtenerTotalTasaUsoMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasComerciosPropios = obtenerTarjPropiasComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.OtrasTarjetasComerciosPropios = obtenerOtrasTarjComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasOtrosComercios = obtenerTarjPropiasOtrosComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosSocios = obtenerTotalSociosPorSexo(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.PromedioDescuento = obtenerPromedioDescuento(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.FacturacionComercios = obtenerFacturacionComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.PromedioArancel = obtenerPromedioArancel(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosCelulares = obtenerTotalCelulares(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosEmails = obtenerTotalEmails(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalConIVA = (entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios) * 1.21M;
                        entity.TotalSinIVA = entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios;
                        dbContext.HistoricoFacturacionFranquicias.Add(entity);

                    }

                }
                else if (cantMeses == 1)// actualizo ante ultimo y agrego ultimo
                {
                    for (int i = 1; i <= 2; i++)
                    {


                        HistoricoFacturacionFranquicias entity;
                        if (i == 1)//actualizo 
                        {

                            entity = dbContext.HistoricoFacturacionFranquicias.Where(x => x.Anio == lastDay.Year && x.Mes == lastDay.Month).FirstOrDefault();
                            fechaDesde = lastDay.ToString(formato);
                            fechaHasta = lastDay.AddMonths(1).AddDays(-1).ToString(formato);

                        }
                        else
                        {
                            entity = new HistoricoFacturacionFranquicias();
                            fechaDesde = lastDay.AddMonths(1).ToString(formato);
                            fechaHasta = lastDay.AddMonths(2).AddDays(-1).ToString(formato);
                            entity.Anio = lastDay.AddMonths(1).Year;
                            entity.Mes = lastDay.AddMonths(1).Month;

                        }
                        entity.IDFranquicia = idFranquicia;

                        entity.PromedioTickets = obtenerPromedioTicketMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTRMensual = obtenerTotalTRMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TasaDeUso = obtenerTotalTasaUsoMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasComerciosPropios = obtenerTarjPropiasComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.OtrasTarjetasComerciosPropios = obtenerOtrasTarjComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasOtrosComercios = obtenerTarjPropiasOtrosComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosSocios = obtenerTotalSociosPorSexo(idFranquicia, fechaDesde, fechaHasta, dbContext);

                        entity.PromedioDescuento = obtenerPromedioDescuento(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.FacturacionComercios = obtenerFacturacionComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.PromedioArancel = obtenerPromedioArancel(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosCelulares = obtenerTotalCelulares(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosEmails = obtenerTotalEmails(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalConIVA = (entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios) * 1.21M;
                        entity.TotalSinIVA = entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios;
                        if (i != 1)
                            dbContext.HistoricoFacturacionFranquicias.Add(entity);

                    }
                }
                else // actualizo ultimos dos
                {
                    for (int i = 1; i <= 2; i++)
                    {

                        fechaDesde = lastDay.ToString(formato);
                        fechaHasta = lastDay.AddMonths(1).AddDays(-1).ToString(formato);
                        int anio = lastDay.Year;
                        int mes = lastDay.Month;

                        if (i > 1)
                        {
                            fechaDesde = lastDay.AddMonths(-1).ToString(formato);
                            fechaHasta = lastDay.AddDays(-1).ToString(formato);

                            anio = lastDay.AddMonths(-1).Year;
                            mes = lastDay.AddMonths(-1).Month;

                        }

                        HistoricoFacturacionFranquicias entity = dbContext.HistoricoFacturacionFranquicias.Where(x => x.Anio == anio && x.Mes == mes).FirstOrDefault();
                        entity.IDFranquicia = idFranquicia;



                        entity.PromedioTickets = obtenerPromedioTicketMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTRMensual = obtenerTotalTRMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TasaDeUso = obtenerTotalTasaUsoMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasComerciosPropios = obtenerTarjPropiasComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.OtrasTarjetasComerciosPropios = obtenerOtrasTarjComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TarjetasPropiasOtrosComercios = obtenerTarjPropiasOtrosComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalConIVA = (entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios) * 1.21M;
                        entity.TotalSinIVA = entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios;
                        entity.PromedioDescuento = obtenerPromedioDescuento(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.FacturacionComercios = obtenerFacturacionComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.PromedioArancel = obtenerPromedioArancel(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosCelulares = obtenerTotalCelulares(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosEmails = obtenerTotalEmails(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(idFranquicia, fechaDesde, fechaHasta, dbContext);
                        entity.NuevosSocios = obtenerTotalSociosPorSexo(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    }
                }

            }
            else
            {
                for (int i = 1; i <= 12; i++)
                {
                    HistoricoFacturacionFranquicias entity = new HistoricoFacturacionFranquicias();
                    var hoy = DateTime.Now;
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    string fechaDesde;
                    string fechaHasta;
                    entity.Anio = hoy.AddMonths((-12 + i)).Year;
                    entity.Mes = hoy.AddMonths((-12 + i)).Month;

                    fechaDesde = hoy.AddMonths((-12 + i)).ToString(formato);
                    fechaHasta = hoy.AddMonths((-12 + i) + 1).AddDays(-1).ToString(formato);
                    entity.IDFranquicia = idFranquicia;



                    entity.PromedioTickets = obtenerPromedioTicketMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TotalTRMensual = obtenerTotalTRMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TasaDeUso = obtenerTotalTasaUsoMensual(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TarjetasPropiasComerciosPropios = obtenerTarjPropiasComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.OtrasTarjetasComerciosPropios = obtenerOtrasTarjComerciosPropios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TarjetasPropiasOtrosComercios = obtenerTarjPropiasOtrosComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TotalConIVA = (entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios) * 1.21M;
                    entity.TotalSinIVA = entity.TarjetasPropiasComerciosPropios + entity.OtrasTarjetasComerciosPropios + entity.TarjetasPropiasOtrosComercios;
                    entity.PromedioDescuento = obtenerPromedioDescuento(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.FacturacionComercios = obtenerFacturacionComercios(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.PromedioArancel = obtenerPromedioArancel(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.NuevosCelulares = obtenerTotalCelulares(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.NuevosEmails = obtenerTotalEmails(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    entity.NuevosSocios = obtenerTotalSociosPorSexo(idFranquicia, fechaDesde, fechaHasta, dbContext);
                    dbContext.HistoricoFacturacionFranquicias.Add(entity);

                }
            }

            dbContext.SaveChanges();
        }
    }



    public void generarTablaFranquicias(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var table = dbContext.HistoricoFacturacionFranquicias.Where(x => x.IDFranquicia == id);
            var meses = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Mes = x.Mes + "-" + x.Anio, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptMesesFranq.DataSource = meses.OrderBy(x => x.Orden);
            rptMesesFranq.DataBind();

            var promTickets = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioTickets ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptPromTicketsFranq.DataSource = promTickets.OrderBy(x => x.Orden);
            rptPromTicketsFranq.DataBind();


            var tarjPropEnComPropios = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TarjetasPropiasComerciosPropios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTarjPropiasComPropiosFranq.DataSource = tarjPropEnComPropios.OrderBy(x => x.Orden);
            rptTarjPropiasComPropiosFranq.DataBind();

            var tarjPropOtrocCom = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TarjetasPropiasOtrosComercios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTarjPropOtrosComFranq.DataSource = tarjPropOtrocCom.OrderBy(x => x.Orden);
            rptTarjPropOtrosComFranq.DataBind();

            var otrasTarj = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.OtrasTarjetasComerciosPropios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptOtrasTatjComPropiosFranq.DataSource = otrasTarj.OrderBy(x => x.Orden);
            rptOtrasTatjComPropiosFranq.DataBind();

            var totalTRMensual = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalTRMensual ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTotalTRMensualFranq.DataSource = totalTRMensual.OrderBy(x => x.Orden);
            rptTotalTRMensualFranq.DataBind();

            var tasaDeUso = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TasaDeUso ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTasaDeUsoFranq.DataSource = tasaDeUso.OrderBy(x => x.Orden);
            rptTasaDeUsoFranq.DataBind();

            var totalSinIVA = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalSinIVA ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTotalSinIVAFranq.DataSource = totalSinIVA.OrderBy(x => x.Orden);
            rptTotalSinIVAFranq.DataBind();

            var totalConIVA = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalConIVA ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTotalConIVAFranq.DataSource = totalConIVA.OrderBy(x => x.Orden);
            rptTotalConIVAFranq.DataBind();

            var promDescuento = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioDescuento ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptPromedioDescuentoFranq.DataSource = promDescuento.OrderBy(x => x.Orden);
            rptPromedioDescuentoFranq.DataBind();

            var facturacionComercios = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.FacturacionComercios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptFacturacionComerciosFranq.DataSource = facturacionComercios.OrderBy(x => x.Orden);
            rptFacturacionComerciosFranq.DataBind();

            var promArancel = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioArancel ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptPromedioArancelFranq.DataSource = promArancel.OrderBy(x => x.Orden);
            rptPromedioArancelFranq.DataBind();



            var nuevosCelulares = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosCelulares ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptNuevosCenlularesFranq.DataSource = nuevosCelulares.OrderBy(x => x.Orden);
            rptNuevosCenlularesFranq.DataBind();

            var nuevosEmails = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosEmails ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptNuevosEmailsFranq.DataSource = nuevosEmails.OrderBy(x => x.Orden);
            rptNuevosEmailsFranq.DataBind();

            var totalTarjASctivas = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalTajetasActivas ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptTotalTarjetasActivasFranq.DataSource = totalTarjASctivas.OrderBy(x => x.Orden);
            rptTotalTarjetasActivasFranq.DataBind();

            var nuevosSocios = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosSocios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
            rptNuevosSociosFranq.DataSource = nuevosSocios.OrderBy(x => x.Orden);
            rptNuevosSociosFranq.DataBind();


        }
    }

    #endregion 

    #region admin
    private void cargarFranquicias()
    {
        bFranquicia bFranquicia = new bFranquicia();
        List<Franquicias> listFranquicias = bFranquicia.getFranquicias();
        this.ddlFranquicias.DataSource = listFranquicias;// bFranquicia.getFranquicias();
        this.ddlFranquicias.DataValueField = "IDFranquicia";
        this.ddlFranquicias.DataTextField = "NombreFantasia";
        this.ddlFranquicias.DataBind();
        this.ddlFranquicias.Items.Insert(0, new ListItem("", "0"));
    }
    public decimal obtenerPromedioTicketMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PromedioTicketMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();

            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data)).ToString("N2"));
            else
                result = 0;


        return result;
    }


    public decimal obtenerTotalPuntosMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalPuntosMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data)).ToString("N2"));
            else
                result = 0;


        return result;
    }


    public decimal obtenerTotalArancelMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalArancelMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data)).ToString("N2"));
            else
                result = 0;

        return result;
    }


    public int obtenerTotalTRMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalTRMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = (Math.Abs(total[0].data));
            else
                result = 0;

        return result;
    }


    public decimal obtenerTotalTasaUsoMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalTasaUsoMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal(Math.Abs(total[0].data).ToString("N2"));
            else
                result = 0;

        return result;
    }


    public decimal obtenerTotalFacturadoMensual(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {

        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalFacturadoMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data).ToString("N2")));
            else
                result = 0;


        return result;
    }

    public int obtenerTotalCanjes(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {

        int result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalCanjes '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Math.Abs(Convert.ToInt32(total[0].data));
            else
                result = 0;

        return result;
    }


    public decimal obtenerPromedioDescuento(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;
            var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_PromDescuento '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Math.Abs(Convert.ToDecimal(total[0].data));
            else
                result = 0;

        return result;
    }

    public decimal obtenerPromedioPuntos(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;
            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_PromPuntosMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();//Dashboard_PromPuntos
            if (total.Any())
                result = Math.Abs(Convert.ToDecimal(total[0].data));
            else
                result = 0;
        

        return result;
    }


    public decimal obtenerPromedioArancel(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_PromArancel '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Math.Abs(Convert.ToDecimal(total[0].data));
            else
                result = 0;


        return result;
    }

    public decimal obtenerTotalFacturacion(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;


            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_FactTotal '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data).ToString("N2")));
            else
                result = 0;

        return result;
    }


    public decimal obtenerCantidadTarjetas(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;
            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_cantidadTarjetas '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data)).ToString("N2"));
            else
                result = 0;


        return result;
    }

    public decimal obtenerFacturacionComercios(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        decimal result;

            var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Facturacion_Comercios '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Convert.ToDecimal((Math.Abs(total[0].data).ToString("N2")));

            else
                result = 0;

        return result;
    }



    #region Generales

    public int obtenerTotalSociosPorSexo(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalSociosPorSexoMensual '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Math.Abs(total[0].data + total[1].data + total[2].data);
            else
                result = 0;

        return result;
    }

    public int obtenerTotalEmails(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalEmailsMensuales '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = Math.Abs(total[0].data);
            else
                result = 0;


        return result;
    }

    public int obtenerTotalCelulares(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalCelularesMensuales '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();

            if (total.Any())
                result = Math.Abs(total[0].data);
            else
                result = 0;
        

        return result;
    }

    public int obtenerTotalTarjetasActivas(string fechaDesde, string fechaHasta, ACHEEntities dbContext)
    {
        int result;

            var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_TotalTarjetasActivasMensuales '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
            if (total.Any())
                result = (Math.Abs(total[0].data));
            else
                result = 0;

        return result;
    }

    #endregion


    protected void btngenerarMesesFaltantes_Click(object sender, EventArgs e)
    {

        using (var dbContext = new ACHEEntities())
        {
            if (idFranquicia == 0)
            {
                var ultimo = dbContext.HistoricoFacturacion.OrderByDescending(x => x.IDHistoricoFacturacion).FirstOrDefault();
                if (ultimo != null)
                {
                    DateTime lastDay = new DateTime(ultimo.Anio ?? 0, ultimo.Mes ?? 0, 1);
                    int monthNow = DateTime.Now.Month;
                    int yearNow = DateTime.Now.Year;


                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    string fechaDesde;
                    string fechaHasta;
                    int cantMeses = ((yearNow - ultimo.Anio ?? 0) * 12) + monthNow - ultimo.Mes ?? 0;
                    if (cantMeses > 1)//el mes anterior y el actual no estan generados asi que no se acualizan 
                    {
                        for (int i = 1; i <= cantMeses; i++)
                        {
                            HistoricoFacturacion entity = new HistoricoFacturacion();

                            entity.Anio = lastDay.AddMonths(i).Year;
                            entity.Mes = lastDay.AddMonths(i).Month;

                            fechaDesde = lastDay.AddMonths(i).ToString(formato);
                            fechaHasta = lastDay.AddMonths(i + 1).AddDays(-1).ToString(formato);
                            entity.PromedioTickets = obtenerPromedioTicketMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalPuntos = obtenerTotalPuntosMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalArancel = obtenerTotalArancelMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalTRMensual = obtenerTotalTRMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TasaDeUso = obtenerTotalTasaUsoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioPuntos = obtenerPromedioPuntos(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioDescuento = obtenerPromedioDescuento(fechaDesde, fechaHasta, dbContext);
                            entity.FacturacionComercios = obtenerFacturacionComercios(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioArancel = obtenerPromedioArancel(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturacion = obtenerTotalFacturacion(fechaDesde, fechaHasta, dbContext);
                            entity.TotalCanjes = obtenerTotalCanjes(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturado = obtenerTotalFacturadoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosCelulares = obtenerTotalCelulares(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosEmails = obtenerTotalEmails(fechaDesde, fechaHasta, dbContext);
                            entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosSocios = obtenerTotalSociosPorSexo(fechaDesde, fechaHasta, dbContext);
                            dbContext.HistoricoFacturacion.Add(entity);

                        }

                    }
                    else if (cantMeses == 1)// actualizo ante ultimo y agrego ultimo
                    {
                        for (int i = 1; i <= 2; i++)
                        {


                            HistoricoFacturacion entity;
                            if (i == 1)//actualizo 
                            {

                                entity = dbContext.HistoricoFacturacion.Where(x => x.Anio == lastDay.Year && x.Mes == lastDay.Month).FirstOrDefault();
                                fechaDesde = lastDay.ToString(formato);
                                fechaHasta = lastDay.AddMonths(1).AddDays(-1).ToString(formato);

                            }
                            else
                            {
                                entity = new HistoricoFacturacion();
                                fechaDesde = lastDay.AddMonths(1).ToString(formato);
                                fechaHasta = lastDay.AddMonths(2).AddDays(-1).ToString(formato);
                                entity.Anio = lastDay.AddMonths(1).Year;
                                entity.Mes = lastDay.AddMonths(1).Month;

                            }
                            entity.PromedioTickets = obtenerPromedioTicketMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalPuntos = obtenerTotalPuntosMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalArancel = obtenerTotalArancelMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalTRMensual = obtenerTotalTRMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TasaDeUso = obtenerTotalTasaUsoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioPuntos = obtenerPromedioPuntos(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioDescuento = obtenerPromedioDescuento(fechaDesde, fechaHasta, dbContext);
                            entity.FacturacionComercios = obtenerFacturacionComercios(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioArancel = obtenerPromedioArancel(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturacion = obtenerTotalFacturacion(fechaDesde, fechaHasta, dbContext);
                            entity.TotalCanjes = obtenerTotalCanjes(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturado = obtenerTotalFacturadoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosCelulares = obtenerTotalCelulares(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosEmails = obtenerTotalEmails(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosSocios = obtenerTotalSociosPorSexo(fechaDesde, fechaHasta, dbContext);

                            entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(fechaDesde, fechaHasta, dbContext);
                            if (i != 1)
                                dbContext.HistoricoFacturacion.Add(entity);

                        }
                    }
                    else // actualizo ultimos dos
                    {
                        for (int i = 1; i <= 2; i++)
                        {

                            fechaDesde = lastDay.ToString(formato);
                            fechaHasta = lastDay.AddMonths(1).AddDays(-1).ToString(formato);
                            int anio = lastDay.Year;
                            int mes = lastDay.Month;

                            if (i > 1)
                            {
                                fechaDesde = lastDay.AddMonths(-1).ToString(formato);
                                fechaHasta = lastDay.AddDays(-1).ToString(formato);

                                anio = lastDay.AddMonths(-1).Year;
                                mes = lastDay.AddMonths(-1).Month;

                            }

                            HistoricoFacturacion entity = dbContext.HistoricoFacturacion.Where(x => x.Anio == anio && x.Mes == mes).FirstOrDefault();




                            entity.PromedioTickets = obtenerPromedioTicketMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalPuntos = obtenerTotalPuntosMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalArancel = obtenerTotalArancelMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TotalTRMensual = obtenerTotalTRMensual(fechaDesde, fechaHasta, dbContext);
                            entity.TasaDeUso = obtenerTotalTasaUsoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioPuntos = obtenerPromedioPuntos(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioDescuento = obtenerPromedioDescuento(fechaDesde, fechaHasta, dbContext);
                            entity.FacturacionComercios = obtenerFacturacionComercios(fechaDesde, fechaHasta, dbContext);
                            entity.PromedioArancel = obtenerPromedioArancel(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturacion = obtenerTotalFacturacion(fechaDesde, fechaHasta, dbContext);
                            entity.TotalCanjes = obtenerTotalCanjes(fechaDesde, fechaHasta, dbContext);
                            entity.TotalFacturado = obtenerTotalFacturadoMensual(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosCelulares = obtenerTotalCelulares(fechaDesde, fechaHasta, dbContext);
                            entity.NuevosSocios = obtenerTotalSociosPorSexo(fechaDesde, fechaHasta, dbContext);

                            entity.NuevosEmails = obtenerTotalEmails(fechaDesde, fechaHasta, dbContext);
                            entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(fechaDesde, fechaHasta, dbContext);

                        }
                    }

                }
                else
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        HistoricoFacturacion entity = new HistoricoFacturacion();
                        var hoy = DateTime.Now;
                        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                        string fechaDesde;
                        string fechaHasta;
                        entity.Anio = hoy.AddMonths((-12 + i)).Year;
                        entity.Mes = hoy.AddMonths((-12 + i)).Month;

                        fechaDesde = hoy.AddMonths((-12 + i)).ToString(formato);
                        fechaHasta = hoy.AddMonths((-12 + i) + 1).AddDays(-1).ToString(formato);
                        entity.PromedioTickets = obtenerPromedioTicketMensual(fechaDesde, fechaHasta, dbContext);
                        entity.TotalPuntos = obtenerTotalPuntosMensual(fechaDesde, fechaHasta, dbContext);
                        entity.TotalArancel = obtenerTotalArancelMensual(fechaDesde, fechaHasta, dbContext);
                        entity.TotalTRMensual = obtenerTotalTRMensual(fechaDesde, fechaHasta, dbContext);
                        entity.TasaDeUso = obtenerTotalTasaUsoMensual(fechaDesde, fechaHasta, dbContext);
                        entity.PromedioPuntos = obtenerPromedioPuntos(fechaDesde, fechaHasta, dbContext);
                        entity.PromedioDescuento = obtenerPromedioDescuento(fechaDesde, fechaHasta, dbContext);
                        entity.FacturacionComercios = obtenerFacturacionComercios(fechaDesde, fechaHasta, dbContext);
                        entity.PromedioArancel = obtenerPromedioArancel(fechaDesde, fechaHasta, dbContext);
                        entity.TotalFacturacion = obtenerTotalFacturacion(fechaDesde, fechaHasta, dbContext);
                        entity.TotalCanjes = obtenerTotalCanjes(fechaDesde, fechaHasta, dbContext);
                        entity.TotalFacturado = obtenerTotalFacturadoMensual(fechaDesde, fechaHasta, dbContext);
                        entity.NuevosCelulares = obtenerTotalCelulares(fechaDesde, fechaHasta, dbContext);
                        entity.NuevosEmails = obtenerTotalEmails(fechaDesde, fechaHasta, dbContext);
                        entity.NuevosSocios = obtenerTotalSociosPorSexo(fechaDesde, fechaHasta, dbContext);

                        entity.TotalTajetasActivas = obtenerTotalTarjetasActivas(fechaDesde, fechaHasta, dbContext);
                        dbContext.HistoricoFacturacion.Add(entity);

                    }
                }

                dbContext.SaveChanges();
                Response.Redirect("/modulos/reportes/HistoricoFacturacion.aspx");
            }
            else
            {
                generarMesesFranq();
                Response.Redirect("/modulos/reportes/HistoricoFacturacion.aspx?IDFranquicia="+ idFranquicia);

            }
        }
    }



    public void generarTabla()
    {
        using (var dbContext = new ACHEEntities())
        {
          
            var table = dbContext.HistoricoFacturacion;
            var meses = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Mes =x.Mes+ "-" + x.Anio, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptMeses.DataSource = meses.OrderBy(x => x.Orden);
            rptMeses.DataBind();

            var promTickets = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioTickets ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptPromTickets.DataSource = promTickets.OrderBy(x => x.Orden);
            rptPromTickets.DataBind();


            var totalPuntos = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalPuntos ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalPuntos.DataSource = totalPuntos.OrderBy(x => x.Orden);
            rptTotalPuntos.DataBind();

            var totalArancel = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalArancel ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalArancel.DataSource = totalArancel.OrderBy(x => x.Orden);
            rptTotalArancel.DataBind();

            var totalTRMensual = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalTRMensual ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalTRMensual.DataSource = totalTRMensual.OrderBy(x => x.Orden);
            rptTotalTRMensual.DataBind();

            var tasaDeUso = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TasaDeUso ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTasaDeUso.DataSource = tasaDeUso.OrderBy(x => x.Orden);
            rptTasaDeUso.DataBind();

            var promPuntos = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioPuntos ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptPromedioPuntos.DataSource = promPuntos.OrderBy(x => x.Orden);
            rptPromedioPuntos.DataBind();

            var promDescuento = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioDescuento ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptPromedioDescuento.DataSource = promDescuento.OrderBy(x => x.Orden);
            rptPromedioDescuento.DataBind();

            var facturacionComercios = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.FacturacionComercios ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptFacturacionComercios.DataSource = facturacionComercios.OrderBy(x => x.Orden);
            rptFacturacionComercios.DataBind();

            var promArancel = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioArancel ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptPromedioArancel.DataSource = promArancel.OrderBy(x => x.Orden);
            rptPromedioArancel.DataBind();

            var totFacturacion = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalFacturacion ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalFacturacion.DataSource = totFacturacion.OrderBy(x => x.Orden);
            rptTotalFacturacion.DataBind();

            var totalCanjes = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalCanjes ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalCanjes.DataSource = totalCanjes.OrderBy(x => x.Orden);
            rptTotalCanjes.DataBind();

            var totFacturado = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalFacturado ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalFacturado.DataSource = totFacturado.OrderBy(x => x.Orden);
            rptTotalFacturado.DataBind();

            var nuevosCelulares = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosCelulares ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptNuevosCenlulares.DataSource = nuevosCelulares.OrderBy(x => x.Orden);
            rptNuevosCenlulares.DataBind();

            var nuevosEmails = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosEmails ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptNuevosEmails.DataSource = nuevosEmails.OrderBy(x => x.Orden);
            rptNuevosEmails.DataBind();

            var totalTarjASctivas = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalTajetasActivas ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptTotalTarjetasActivas.DataSource = totalTarjASctivas.OrderBy(x => x.Orden);
            rptTotalTarjetasActivas.DataBind();

            var nuevosSocios = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosSocios ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
            rptNuevosSocios.DataSource = nuevosSocios.OrderBy(x => x.Orden);
            rptNuevosSocios.DataBind();


        }
    }
    #endregion




    [WebMethod(true)]
    public static string Exportar()
    {

        string fileName = "HistoricoFacturacion";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();



                using (var dbContext = new ACHEEntities())
                {
                    if (idFranquicia ==0)
                    {
                        var table = dbContext.HistoricoFacturacion;
                        var meses = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Mes = x.Mes + "-" + x.Anio, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
                        var N = meses.Count();
                        dt.Columns.Add("Concepto", typeof(string));
                        foreach (var item in meses.OrderBy(x => x.Orden))
                        {
                            dt.Columns.Add(item.Mes, typeof(string));
                        }
                        DataRow dr = dt.NewRow();
                        dr[0] = "Promedio de ticket";
                        var promTickets = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioTickets ?? 0, Orden = x.IDHistoricoFacturacion }).Take(12).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promTickets.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr[0] = "Total puntos";
                        var totPuntos = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalPuntos ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totPuntos.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total arancel";
                        var totArancel = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalArancel ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totArancel.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total tr. mensual";
                        var tottrMensual = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalTRMensual ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = tottrMensual.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Tasa de uso";
                        var totTasaDeUso = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TasaDeUso ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totTasaDeUso.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Promedio puntos";
                        var promPuntos = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioPuntos ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promPuntos.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Promedio descuento";
                        var promdesc = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioDescuento ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promdesc.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Facturacion comercio";
                        var factComercios = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.FacturacionComercios ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = factComercios.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Promedio arancel";
                        var promAra = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.PromedioArancel ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promAra.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total facturacion";
                        var totfact = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalFacturacion ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totfact.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);


                        dr = dt.NewRow();
                        dr[0] = "Total canjes";
                        var totCanjes = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalCanjes ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totCanjes.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total facturado";
                        var totFact = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalFacturado ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totFact.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Nuevos celulares";
                        var nuevoscels = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosCelulares ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = nuevoscels.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Nuevos emails";
                        var nuevosemails = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosEmails ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = nuevosemails.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total tarjetas activas";
                        var totActivas = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.TotalTajetasActivas ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totActivas.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Nuevos socios";
                        var Nuevossocios = table.OrderByDescending(x => x.IDHistoricoFacturacion).Select(x => new { Valor = x.NuevosSocios ?? 0, Orden = x.IDHistoricoFacturacion }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = Nuevossocios.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);
                    }
                    else
                    {

                        var table = dbContext.HistoricoFacturacionFranquicias.Where(x => x.IDFranquicia == idFranquicia);
                        var meses = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Mes = x.Mes + "-" + x.Anio, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
                        dt.Columns.Add("Concepto", typeof(string));
                        int N = meses.Count();
                        foreach (var item in meses.OrderBy(x => x.Orden))
                        {
                            dt.Columns.Add(item.Mes, typeof(string));
                        }
                        DataRow dr = dt.NewRow();
                        dr[0] = "Tarj propias en comercios propios";
                        var tarjpropcomprop = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TarjetasPropiasComerciosPropios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(12).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = tarjpropcomprop.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Tarj propias en otros comercios";
                        var totPropiosEnOtrosCom = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TarjetasPropiasOtrosComercios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totPropiosEnOtrosCom.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Otras tarj propias en comercios propios";
                        var otrastarj = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.OtrasTarjetasComerciosPropios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = otrastarj.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total sin IVA";
                        var TotalsinIVA = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalSinIVA ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = TotalsinIVA.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total con IVA";
                        var TotalconIVA = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalConIVA ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = TotalconIVA.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();

                        dr[0] = "Promedio de ticket";
                        var promTickets = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioTickets ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promTickets.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total tr. mensual";
                        var tottrMensual = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalTRMensual ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = tottrMensual.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Tasa de uso";
                        var totTasaDeUso = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TasaDeUso ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totTasaDeUso.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);


                        dr = dt.NewRow();
                        dr[0] = "Promedio descuento";
                        var promdesc = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioDescuento ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promdesc.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Facturacion comercio";
                        var factComercios = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.FacturacionComercios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = factComercios.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Promedio arancel";
                        var promAra = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.PromedioArancel ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = promAra.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);


                        dr = dt.NewRow();
                        dr[0] = "Nuevos celulares";
                        var nuevoscels = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosCelulares ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = nuevoscels.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Nuevos emails";
                        var nuevosemails = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosEmails ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = nuevosemails.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Total tarjetas activas";
                        var totActivas = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.TotalTajetasActivas ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = totActivas.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr[0] = "Nuevos socios";
                        var Nuevossocios = table.OrderByDescending(x => x.IDHistoricoFacturacionFranquicia).Select(x => new { Valor = x.NuevosSocios ?? 0, Orden = x.IDHistoricoFacturacionFranquicia }).Take(N).ToList();
                        for (int i = 1; i <= N; i++)
                            dr[i] = Nuevossocios.OrderBy(x => x.Orden).ElementAt((i - 1)).Valor.ToString();
                        dt.Rows.Add(dr);
                    }

                }
                ExportarAExcel(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }



    public static void ExportarAExcel(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);

        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}

