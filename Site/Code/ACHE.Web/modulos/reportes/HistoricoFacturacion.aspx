﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistoricoFacturacion.aspx.cs" Inherits="modulos_reportes_HistoricoFacturacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/historico-facturacion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Estadísticas de los últimos 12 meses</h3>
            <form id="Form1" runat="server">
               <div class="formSep col-sm-12 col-md-12">
                <div class="row">
                 <div class="col-sm-3" >
                    <label>Franquicia</label>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlFranquicias"  onchange="mostrarReporte(this.value);" />
                      </div>
                 </div>
                <div class="row" >
                    <div class="col-sm-8 col-sm-md-8">
                        <asp:Button ID="Button1" runat="server"  CssClass="btn btn-success"  Text="Generar meses faltantes"  OnClientClick="showimg();"  OnClick="btngenerarMesesFaltantes_Click"/>
                          <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                        <a href="" id="lnkDownload" download="HistoricoFact" style="display:none">Descargar</a>
                    </div>
                </div>
            </form>
                        </div>
            <br/>
                 <asp:Panel ID="pnlAdmin" runat="server">

            <table  class="table table-condensed table-striped" data-provides="rowlink">
                 <thead>
                     <tr>       
                        <th style="text-align:left">Concepto</th>
                           <asp:Repeater runat="server" ID="rptMeses"  ClientIDMode="Static">
                                <ItemTemplate>  
                                      <th style="text-align:right"> <%# Eval("Mes").ToString() %> </th>
                                </ItemTemplate>
                          </asp:Repeater>
                    <tr/>
                </thead>
      
             
                      <tr>
                          <td style="text-align:left">Promedio de ticket</td>
                          <asp:Repeater runat="server" ID="rptPromTickets"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor") %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
                      <tr>
                          <td style="text-align:left">Total puntos</td>
                          <asp:Repeater runat="server" ID="rptTotalPuntos"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total arancel</td>
                          <asp:Repeater runat="server" ID="rptTotalArancel"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total tr. mensual</td>
                          <asp:Repeater runat="server" ID="rptTotalTRMensual"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Tasa de uso</td>
                          <asp:Repeater runat="server" ID="rptTasaDeUso"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Promedio puntos</td>
                          <asp:Repeater runat="server" ID="rptPromedioPuntos"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Promedio descuento</td>
                          <asp:Repeater runat="server" ID="rptPromedioDescuento"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Facturacion comercios</td>
                          <asp:Repeater runat="server" ID="rptFacturacionComercios"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Promedio arancel</td>
                          <asp:Repeater runat="server" ID="rptPromedioArancel"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total facturacion</td>
                          <asp:Repeater runat="server" ID="rptTotalFacturacion"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total canjes</td>
                          <asp:Repeater runat="server" ID="rptTotalCanjes"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total facturado</td>
                          <asp:Repeater runat="server" ID="rptTotalFacturado"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Nuevos celulares</td>
                          <asp:Repeater runat="server" ID="rptNuevosCenlulares"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Nuevos emails</td>
                          <asp:Repeater runat="server" ID="rptNuevosEmails"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total tarjetas activas</td>
                          <asp:Repeater runat="server" ID="rptTotalTarjetasActivas"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               

                      <tr>
                          <td style="text-align:left">Nuevos socios</td>
                          <asp:Repeater runat="server" ID="rptNuevosSocios"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
               
                   
           </table>
         </asp:Panel>
                       <asp:Panel ID="pnlFranq" runat="server">
                       <table  class="table table-condensed table-striped" data-provides="rowlink">
                     <thead>
                         <tr>       
                            <th style="text-align:left">Concepto</th>
                               <asp:Repeater runat="server" ID="rptMesesFranq"  ClientIDMode="Static">
                                    <ItemTemplate>  
                                          <th style="text-align:right"> <%# Eval("Mes").ToString() %> </th>
                                    </ItemTemplate>
                              </asp:Repeater>
                        <tr/>
                    </thead>
      
             
                      <tr>
                          <td style="text-align:left">Tarj propias en comercios propios</td>
                          <asp:Repeater runat="server" ID="rptTarjPropiasComPropiosFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor") %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
                      <tr>
                          <td style="text-align:left">Tarj propias en otros comercios</td>
                          <asp:Repeater runat="server" ID="rptTarjPropOtrosComFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Otras tarj en comercios propios</td>
                          <asp:Repeater runat="server" ID="rptOtrasTatjComPropiosFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               

                      <tr>
                          <td style="text-align:left">Total sin IVA</td>
                          <asp:Repeater runat="server" ID="rptTotalSinIVAFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
                      <tr>
                          <td style="text-align:left">Total con IVA</td>
                          <asp:Repeater runat="server" ID="rptTotalConIVAFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
                      <tr>
                          <td style="text-align:left">Promedio de ticket mensual</td>
                          <asp:Repeater runat="server" ID="rptPromTicketsFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor") %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
                      <tr>
                          <td style="text-align:left">Total tr. mensual</td>
                          <asp:Repeater runat="server" ID="rptTotalTRMensualFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                     
               
                      <tr>
                          <td style="text-align:left">Tasa de uso</td>
                          <asp:Repeater runat="server" ID="rptTasaDeUsoFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>

                      <tr>
                          <td style="text-align:left">Promedio descuento</td>
                          <asp:Repeater runat="server" ID="rptPromedioDescuentoFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Facturacion comercios</td>
                          <asp:Repeater runat="server" ID="rptFacturacionComerciosFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Promedio arancel</td>
                          <asp:Repeater runat="server" ID="rptPromedioArancelFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Nuevos celulares</td>
                          <asp:Repeater runat="server" ID="rptNuevosCenlularesFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Nuevos emails</td>
                          <asp:Repeater runat="server" ID="rptNuevosEmailsFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
                      <tr>
                          <td style="text-align:left">Total tarjetas activas</td>
                          <asp:Repeater runat="server" ID="rptTotalTarjetasActivasFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               

                      <tr>
                          <td style="text-align:left">Nuevos socios</td>
                          <asp:Repeater runat="server" ID="rptNuevosSociosFranq"  ClientIDMode="Static">
                              <ItemTemplate>  
                                        <td style="text-align:right"><%# Eval("Valor").ToString() %></td>
                              </ItemTemplate>  
                          </asp:Repeater>
                      <tr/>
               
               
                   
           </table>
                           </asp:Panel>

    </div>
  </div>

</asp:Content>

