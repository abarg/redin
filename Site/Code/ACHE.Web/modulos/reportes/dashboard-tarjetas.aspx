﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dashboard-tarjetas.aspx.cs" Inherits="modulos_reportes_dashboard_tarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <!--[if lt IE 9]>
		<script src="/lib/flot/excanvas.min.js"></script>
    <![endif]-->
       <style type="text/css">
        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #FFF;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }
        .legend table, .legend > div {
            height: 56px !important;
            opacity: 1 !important;
            top: 8px;
            right: 10px;
            width: 110px !important;
            background-color: transparent !important;
        }
 
        .legend table {
            border-spacing: 5px;
            /*border: 1px solid #555;*/
            padding: 2px;
        }
        .ov_boxes .ov_text {
            width:125px !important
        }

        .modal.modal-wide .modal-dialog {
          width: 90%;
        }
        .modal-wide .modal-body {
          overflow-y: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-6 col-lg-6">
            <h3 class="heading">Tarjetas asignadas <small>últimos 4 meses</small></h3>
            <div id="fl_tarjetas_asignadas" style="height: 270px; width: 90%; margin: 15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <h3 class="heading">Tarjetas activas & inactivas <small>últimos 4 meses</small></h3>
            <div id="fl_tarjetas_activas" style="height: 270px; width: 90%; margin: 15px auto 0">
                <img src="/img/dashboard/gif-load.gif" />
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalCargarFechaVencimiento">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="H1">Edicion FechaVencimiento</h4>
                        <input type="hidden" id="hdnID" value="0" />
                          <input type="hidden" id="hdnNumero" value="0" />
                        
                    </div>
                    <div class="modal-body">
                            <div class="container">
                                   <form id="formEdicion">
                                   <div class="row">                                
                                       <div class="col-sm-6">
                                            <label class="col-lg-4 control-label"><span class="f_req">*</span> Fecha vencimiento</label>
                                            <div class="col-lg-6">
                                                <input type="text" id="txtFechaVencimiento" class="form-control required validDate" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnGuardar" onclick="guardarFechaVencimiento();">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
    </div>

    <script type="text/javascript" src="<%= ResolveUrl("~/lib/date/date.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.resize.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.pie.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.axislabels.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.curvedLines.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.orderBars.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.time.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.categories.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/flot/jquery.flot.multihighlight.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <%--<script src="/lib/flot/jquery.flot.pyramid.js"></script>--%>

    <!-- charts functions -->

    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/dashboard-tarjetas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>


</asp:Content>

