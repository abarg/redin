﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_reportes_reporte_saldo : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [WebMethod(true)]
    public static DataSourceResult Buscar(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.ReporteSaldosFacturacion
                     .Select(x => new {
                         IDComercio = x.IDComercio,
                         Comercio = x.Comercio,
                         PeriodoDesde = x.UltimoPeriodoDesde,
                         PeriodoHasta = x.UltimoPeriodoHasta,
                         Saldo = x.Saldo,
                         CantFacturas =x.CantFacturas,
                     }).OrderBy(x => x.Comercio).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string obtenerFacturas(int idComercio) {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            if (idComercio > 0) {
                using (var dbContext = new ACHEEntities()) {
                    var list = dbContext.Facturas.Where(x => x.IDComercio == idComercio && x.Modo.ToUpper() == "R" && x.FechaCAE == null).ToList();
                    if (list.Any()) {
                        foreach (var factura in list) {
                            html += "<tr>";
                            html += "<td>" + factura.PeriodoDesde.ToShortDateString() + "</td>";
                            html += "<td>" + factura.PeriodoHasta.ToShortDateString() + "</td>";
                            html += "<td>$" + factura.ImporteTotal.ToString("N2") + "</td>";
                            html += "</tr>";
                        }
                    }
                    else html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
                }
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string Exportar() {
        string fileName = "Facturas";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.ReporteSaldosFacturacion
                     .Select(x => new {
                         IDComercio = x.IDComercio,
                         Comercio = x.Comercio,
                         PeriodoDesde = x.UltimoPeriodoDesde,
                         PeriodoHasta = x.UltimoPeriodoHasta,
                         Saldo = (decimal) x.Saldo,
                         CantFacturas = x.CantFacturas,
                     }).OrderBy(x => x.Comercio);

                    dt = info.Select(x => new {
                        Comercio = x.Comercio,
                        CantFacturas = (int)x.CantFacturas,
                        PeriodoDesde = (DateTime) x.PeriodoDesde,
                        PeriodoHasta = (DateTime) x.PeriodoHasta,
                        Saldo = "$" + x.Saldo,
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }
}