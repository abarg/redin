﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="oldHome.aspx.cs" Inherits="modulos_reportes_oldHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <div class="row">
        <input type="hidden" id="hdnTipo" />
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Alertas del sistema relacionadas a información inconsistente</h3>
            <div class="row">
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Establecimientos inexistentes</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlNumEstError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litEst"></asp:Literal>
                                                transacciones con Números de Establecimientos son inexistentes.
                                                <br />
                                                <a href="javascript:verDetalle('establecimientos');">Ver detalle</a>       
                                                <a href="modulos/gestion/historialTermEstab.aspx">Solucionar</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlNumEstOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Terminales inexistentes</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTerminalesError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litTer"></asp:Literal>
                                                transacciones cuyos Terminales son inexistentes.
                                                <br />
                                                <a href="javascript:verDetalle('terminales');">Ver detalle</a>
                                                <a href="modulos/gestion/historialTermEstab.aspx">Solucionar</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlTerminalesOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">TR inválidas</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTrError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litTr"></asp:Literal>
                                                transacciones con Números de Establecimientos no corresponden a su Terminal.
                                                <br />
                                                <a href="javascript:verDetalle('tr');">Ver detalle</a>
                                                <a href="modulos/gestion/historialTermEstab.aspx">Solucionar</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlTrOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Arancel/Puntos inválidas</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlArancelError" Visible="false">
                                                Se han detectado
                                                <span runat="server"  ID="spnArancel"></span>
                                                transacciones con Arancel/Puntos inválidos.
                                                <br />
                                                <a href="javascript:verDetalle('arancel');">Ver detalle</a>
                                                <a href="javascript:solucionarArancelYPuntos();">Solucionar</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlArancelOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header" style="background: #b94a48; color: #fff;">Tarjetas con puntos negativos</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTarError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litTar"></asp:Literal>
                                                tarjetas con puntos negativos.
                                                <br />
                                                <a href="javascript:verDetalle('tar');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlTarOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        
                    </div>
                </div>
            </div>

            <br />
            <h3 class="heading">Alertas del sistema relacionadas a comercios</h3>

            <div class="row">
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Comercios incompletos</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlComercioError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litComercio"></asp:Literal>
                                                comercios con informacion incompleta.
                                                <br />
                                                    <a href="javascript:verDetalleComercios('info');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlComercioOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Terminales sin reprogramar</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlReprogError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litReprog"></asp:Literal>
                                                terminales que no se han enviado a reprogramar.
                                                <br />
                                                <a href="javascript:verDetalle('repro');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlReprogOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Terminales sin activarse</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlSinActivarseError" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litSinActivarse"></asp:Literal>
                                                terminales sin activarse (con TR < $1).
                                                <br />
                                                <a href="javascript:verDetalleComercios('inactivos');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlSinActivarseOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Comercios sin transacciones</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlSinTransacciones" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litSinTransacciones"></asp:Literal>
                                                comercios sin transacciones.
                                                <br />
                                                <a href="javascript:verDetalleComercios('sinTrans');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlSinTransaccionesOk" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="row"> 
                <div class="col-sm-6 col-md-6 dd_column">
                    <div class="w-box">
                        <div class="w-box-header">Comercios con mas de 1 transaccion en los últimos 3 dias</div>
                        <div class="w-box-content">
                            <table class="table table-striped table_vam no-th">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="pnlTransAyer" Visible="false">
                                                Se han detectado
                                                <asp:Literal runat="server" ID="litTransAyer"></asp:Literal>
                                                comercios con mas de 1 transaccion en los últimos 3 dias
                                                <br />
                                                <a href="javascript:verDetalleComercios('masDe1Trans');">Ver detalle</a>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnlTransAyerOK" Visible="false">
                                                No se han detectado alertas<br />
                                                <br />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>Comercio</th>
                                <th>Franquicia</th> 
                                <th>Pendientes</th> 
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                    <%--<a href="/modulos/reportes/facturacion.aspx">Facturación</a>--%>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle2">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle2"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="table1">
                        <thead id="headDetalle2">
                            <tr>
                                <th>Comercio</th>
                                <th>Tipo POS</th> 
                                <th>Establecimiento</th> 
                                <th>Franquicia</th> 
                                <th>Pendientes</th> 
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle2">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnExportar2" onclick="exportar2();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading2" style="display:none" />
                    <a href="" id="lnkDownload2" download="Comercios" style="display:none">Descargar</a>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle2').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="modalSolucion">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titSolucionar"></h3>
                </div>
                <div  style="text-align:center" class="modal-body">
                    <img alt="" src="/img/dashboard/gif-load.gif" id="imgLoading3" style="display:none;" />
                     <span id="spnCantTr"></span>    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="$('#modalSolucion').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">    
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/home.js?v=1") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>