﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pines.aspx.cs" Inherits="modulos_reportes_pines" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/reportes/pines.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Reportes</a></li>
            <li class="last">Pines</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Pines</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formReportePines" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Empresa Canje</label>
                            <asp:DropDownList runat="server" ID="cmbEmpresa" CssClass="form-control" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Pines" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="divGrid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

