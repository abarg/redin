﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class modulos_reportes_rnk_socios : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cargarDatos();
    }

    private void cargarDatos()
    {
        using (var dbContext = new ACHEEntities())
        {
            #region marcas
            var marcas = dbContext.Marcas.OrderBy(x => x.Nombre).ToList();
            Marcas todas = new Marcas();
            todas.Nombre = "Todas las marcas";
            todas.IDMarca = 0;
            marcas.Insert(0, todas);

            ddlMarcas.DataSource = marcas;
            ddlMarcas.DataValueField = "IDMarca";
            ddlMarcas.DataTextField = "Nombre";
            ddlMarcas.DataBind();
            #endregion

            #region franquicias
            var franquicias = dbContext.Franquicias.OrderBy(x => x.NombreFantasia).ToList();
            Franquicias todasF = new Franquicias();
            todasF.NombreFantasia = "Todas las franquicias";
            todasF.IDFranquicia = 0;
            franquicias.Insert(0, todasF);

            ddlFranquicias.DataSource = franquicias;
            ddlFranquicias.DataValueField = "IDFranquicia";
            ddlFranquicias.DataTextField = "NombreFantasia";
            ddlFranquicias.DataBind();
            #endregion
        }
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                var info = dbContext.RankingSocios
                        .OrderByDescending(x => x.Importe)
                        .Select(x => new RptRankingSociosViewModel() {
                            IDFranquicia = x.IDFranquicia ?? 0,
                            CantTr = x.CantTr ?? 0,
                            Franquicia = x.Franquicia,
                            Marca = x.Marca,
                            IDMarca = x.IDMarca,
                            Socio = x.Socio,
                            NroDocumento = x.NroDoc,
                            Importe = x.Importe,
                            ImporteAhorro = x.Ahorro,
                            Tarjeta = x.Tarjeta
                        }).Take(50).ToDataSourceResult(take, skip, sort, filter);

                 return info;
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string Exportar(int franquicia, int marca)
    {
        string fileName = "RankingSocios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.RankingSocios.AsEnumerable();
                    info=info.ToList();
                    if (franquicia > 0)
                        info = info.Where(x => x.IDFranquicia == franquicia);
                    if (marca > 0)
                        info = info.Where(x => x.IDMarca == marca);

                    dt = info.Select(x => new RptRankingSociosViewModel()
                        {
                            IDFranquicia = x.IDFranquicia ?? 0,
                            CantTr = x.CantTr ?? 0,
                            Franquicia = x.Franquicia,
                            Marca = x.Marca,
                            IDMarca = x.IDMarca,
                            Socio = x.Socio,
                            NroDocumento = x.NroDoc,
                            Importe = x.Importe,
                            ImporteAhorro = x.Ahorro,
                            Tarjeta = x.Tarjeta
                        }).OrderByDescending(x => x.Importe).Take(50).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}