﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_common : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadComercios()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.Comercios.Include("Domicilios")
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDComercio,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia + " - " + x.Domicilios.Domicilio
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }

    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadCiudades(int idProvincia)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDCiudad,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }

        return entities;
    }
    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadProvincias(int idPais)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Provincias.Where(x => x.IDPais == idPais)
                .Select(x => new Combo2ViewModel()
                {
                    ID = x.IDProvincia,
                    Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }

        return entities;
    }
    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadZonas(int idCiudad)
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();

        using (var dbContext = new ACHEEntities())
        {
            entities = dbContext.Zonas.Where(x => x.IDCiudad == idCiudad)
                .Select(x => new Combo2ViewModel()
                {
                      ID = x.IDZona,
                      Nombre = x.Nombre
                }).OrderBy(x => x.Nombre).ToList();
        }

        return entities;
    }
    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadProveedores()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.Proveedores
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDProveedor,// POSTerminal + "_" + x.POSEstablecimiento,
                        Nombre = x.NombreFantasia
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }

    [System.Web.Services.WebMethod]
    public static List<Combo2ViewModel> LoadCasasMatrices()
    {
        List<Combo2ViewModel> entities = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                entities = dbContext.ObtenerComerciosParaFC()
                    .Select(x => new Combo2ViewModel()
                     {
                         ID = x.IDComercio.Value,
                         Nombre = x.NroDocumento + " - " + x.NombreFantasia
                     }).OrderBy(x => x.Nombre).ToList();
            }
        }
        return entities;
    }
}