﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GeneracionDat.aspx.cs" Inherits="modulos_seguridad_GeneracionDat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/seguridad/generacionDat.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li>Generación de .dat</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Generación de .dat</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>

            <form runat="server" id="formGeneracionDat" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tipo de Archivo</label>
                    <div class="col-lg-10">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTipoArchivo" Width="200px" onchange="showDias(this.value);">
                            <asp:ListItem Value="1" Text="Beneficios - Comercios (COMCLUBIN)" />
                            <asp:ListItem Value="2" Text="Beneficios - Tarjetas (TARJCLUBIN)" />
                            <%--<asp:ListItem Value="3" Text="Puntos - Comercios" />--%>
                            <asp:ListItem Value="4" Text="Puntos - Tarjetas (TARJPTCLUBIN)" />
                            <asp:ListItem Value="5" Text="Puntos - Cuentas (CUENCLUBIN)" />
                            <asp:ListItem Value="6" Text="Puntos - Premios (PREMCLUBIN)" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" id="divDias">
                    <label class="col-lg-2 control-label">Descuentos para el día</label>
                    <div class="col-lg-10">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlDia" Width="200px">
                            <asp:ListItem Value="1" Text="Lunes" />
                            <asp:ListItem Value="2" Text="Martes" />
                            <asp:ListItem Value="3" Text="Miercoles" />
                            <asp:ListItem Value="4" Text="Jueves" />
                            <asp:ListItem Value="5" Text="Viernes" />
                            <asp:ListItem Value="6" Text="Sabado" />
                            <asp:ListItem Value="7" Text="Domingo" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtNroProceso" class="col-lg-2 control-label"><span class="f_req"> *</span> Nro. de Proceso</label>
                    <div class="col-lg-10">
                        <asp:TextBox runat="server" ID="txtNroProceso" CssClass="form-control required proceso" MaxLength="10" Width="200px" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                        <a href="" id="lnkDownload" download="DAT" style="display:none">Descargar .dat</a>

                        <button runat="server" id="btnExportar" class="btn" type="button" onclick="exportar();">Visualizar</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading2" style="display:none" />
                        <a href="" id="lnkDownloadCSV" accept=".csv" download="CSV" style="display:none">Descargar </a>

                    </div>
                </div>

                <asp:HiddenField runat="server"  ID="hfArchivo"/>
            </form>
        </div>
    </div>
</asp:Content>

