﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using ACHE.Model;
using ACHE.Business;
using ACHE.Extensions;
using System.Data;
using System.Text;

public partial class modulos_gestion_Importacion : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Tipo"] == "P")
            {
                ddlTipoArchivo.SelectedValue = "5";
                lnkPremiosPrueba.Visible = true;
            }
            this.limpiarControles();
        }
    }

    private void limpiarControles()
    {
        this.fu.ClearFileFromPersistedStore();
    }

    protected void subirArchivoComercios(object sender, EventArgs e)
    {
        try
        {
            if (Path.GetExtension(this.fu.FileName).Equals(".xls")
                || Path.GetExtension(this.fu.FileName).Equals(".xlsx")
                || Path.GetExtension(this.fu.FileName).Equals(".txt")
                || Path.GetExtension(this.fu.FileName).Equals(".csv"))
            {
                string nombreArchivoRuta = Server.MapPath("ArchivosTemp/") + Path.GetFileName(this.fu.FileName);
                this.fu.SaveAs(nombreArchivoRuta);

                if (this.ddlTipoArchivo.SelectedValue.Equals("3"))
                    this.importarArchivoTransaccionesVisa(nombreArchivoRuta, this.fu.FileName.Split(".txt")[0]);
                else if (this.ddlTipoArchivo.SelectedValue.Equals("5"))
                    this.importarArchivoPremios(nombreArchivoRuta, false);
                else if (this.ddlTipoArchivo.SelectedValue.Equals("6"))
                    this.importarArchivoPremios(nombreArchivoRuta, true);

                File.Delete(nombreArchivoRuta);
            }
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw ex;
        }
    }

    private void importarArchivoTransaccionesVisa(string nombreArchivoRuta, string nombreArchivo)
    {
        bTransaccion bTransaccion;
        DataTable dt;
        StreamReader arch = null;
        string linea;
        string numEst = "";
        int longNumEst = 15;
        string numTer = "";
        int longNumTer = 8;
        string TipoMensaje = "";
        int longTipoMensaje = 4;
        string TipoTrans = "";
        int longTipoTrans = 6;
        string fecTrans = "";
        int longFecTrans = 8;
        string HorTrans = "";
        int longHorTrans = 6;
        string numCupon = "";
        int longNumCupon = 6;
        string numRef = "";
        int longNumRef = 12;
        string numRefOrg = "";
        int longNumRefOrg = 12;
        string numTarCli = "";
        int longNumTarCli = 16;
        string codPrem = "";
        int longCodPrem = 11;
        string puntIng = "";
        int longPuntIng = 12;
        string puntDis = "";
        int longPuntDis = 12;
        try
        {
            arch = new StreamReader(nombreArchivoRuta);

            //Estructura tabla
            dt = new DataTable();
            dt.Columns.Add("NumEst", typeof(string));
            dt.Columns.Add("NumTerminal");
            dt.Columns.Add("TipoMensaje");
            dt.Columns.Add("TipoTransaccion");
            dt.Columns.Add("FechaTransaccion", typeof(DateTime));
            dt.Columns.Add("NumCupon");
            dt.Columns.Add("NumReferencia");
            dt.Columns.Add("NumRefOriginal");
            dt.Columns.Add("NumTarjetaCliente");
            dt.Columns.Add("CodigoPremio");
            dt.Columns.Add("PuntosIngresados");
            dt.Columns.Add("PuntosDisponibles");
            dt.Columns.Add("NombreArchivo");
            dt.Columns.Add("IDUsuario", typeof(int));
            dt.Columns.Add("Origen");
            dt.Columns.Add("Operacion");
            dt.Columns.Add("Importe");
            dt.Columns.Add("ImporteAhorro");
            dt.Columns.Add("Descuento", typeof(int));
            dt.Columns.Add("PuntosAContabilizar", typeof(int));
            //dt.Columns.Add("IDMarca", typeof(int));

            Usuarios oUsu = (Usuarios)Session["CurrentUser"];

            string[] vlineas;
            while ((linea = arch.ReadLine()) != null)
            {
                vlineas = linea.Split(';');

                numEst = vlineas[0];
                numEst = numEst.PadLeft(longNumEst, ' ');

                numTer = vlineas[1];
                numTer = numTer.PadLeft(longNumTer, ' ');

                TipoMensaje = vlineas[2];
                TipoMensaje = TipoMensaje.PadLeft(longTipoMensaje, ' ');

                TipoTrans = vlineas[3];
                TipoTrans = TipoTrans.PadLeft(longTipoTrans, ' ');

                fecTrans = vlineas[4];
                fecTrans = fecTrans.PadLeft(longFecTrans, ' ');

                HorTrans = vlineas[5];
                HorTrans = HorTrans.PadLeft(longHorTrans, ' ');

                numCupon = vlineas[6];
                numCupon = numCupon.PadLeft(longNumCupon, ' ');

                numRef = vlineas[7];
                numRef = numRef.PadLeft(longNumRef, ' ');

                numRefOrg = vlineas[8];
                numRefOrg = numRefOrg.PadLeft(longNumRefOrg, ' ');

                numTarCli = vlineas[9];
                numTarCli = numTarCli.PadLeft(longNumTarCli, ' ');

                codPrem = vlineas[10];
                codPrem = codPrem.PadLeft(longCodPrem, ' ');

                puntIng = vlineas[11];
                puntIng = puntIng.PadLeft(longPuntIng, ' ');

                puntDis = vlineas[12];
                puntDis = puntDis.PadLeft(longPuntDis, ' ');

                DataRow dr = dt.NewRow();
                dr["NumEst"] = numEst;
                dr["NumTerminal"] = numTer;
                dr["TipoMensaje"] = TipoMensaje;
                dr["TipoTransaccion"] = TipoTrans;

                DateTime dFec = DateTime.ParseExact(fecTrans, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                dFec = dFec.AddHours(Convert.ToInt32(HorTrans.Substring(0, 2)));
                dFec = dFec.AddMinutes(Convert.ToInt32(HorTrans.Substring(2, 2)));
                dFec = dFec.AddSeconds(Convert.ToInt32(HorTrans.Substring(4, 2)));
                dr["FechaTransaccion"] = dFec;

                dr["NumCupon"] = numCupon;
                dr["NumReferencia"] = numRef;
                dr["NumRefOriginal"] = numRefOrg;
                dr["NumTarjetaCliente"] = numTarCli;
                dr["CodigoPremio"] = codPrem;
                dr["PuntosIngresados"] = puntIng;
                dr["PuntosDisponibles"] = puntDis;
                dr["NombreArchivo"] = nombreArchivo;
                dr["IDUsuario"] = oUsu.IDUsuario;
                dr["Origen"] = "Visa";
                dr["Operacion"] = "Carga";
                dr["Descuento"] = 0;
                dr["PuntosAContabilizar"] = 0;
                //dr["IDMarca"] = 0;
                dt.Rows.Add(dr);
            }

            if (dt.Rows.Count > 0)
            {
                bTransaccion = new bTransaccion();
                bTransaccion.limpiarTransaccionesTmp();
                bTransaccion.insertarTransaccionesTmp(dt);
                bTransaccion.actualizarTransacciones(nombreArchivo, oUsu.IDUsuario);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            arch.Close();
        }
    }

    /*
    private void importarArchivoTransaccionesFidely(string nombreArchivoRuta, string nombreArchivo)
    {
        bTransaccion bTransaccion;
        DataTable dt;
        StreamReader arch = null;
        string linea;
        int i = 0;
        string fechaOperacion = "";
        string tarjeta = "";
        string operacion = "";
        string descripcion = "";
        string valorOperacion = "";
        try
        {
            arch = new StreamReader(nombreArchivoRuta);

            //Estructura tabla
            dt = new DataTable();
            dt.Columns.Add("FechaTransaccion", typeof(DateTime));
            dt.Columns.Add("Importe", typeof(Decimal));
            dt.Columns.Add("NumTarjetaCliente");
            dt.Columns.Add("Descripcion");
            dt.Columns.Add("Origen");
            dt.Columns.Add("Operacion");
            dt.Columns.Add("NombreArchivo");
            dt.Columns.Add("IDUsuario", typeof(int));

            Usuarios oUsu = (Usuarios)Session["CurrentUser"];

            string[] vlineas;
            while ((linea = arch.ReadLine()) != null)
            {
                if (i != 0)
                {
                    vlineas = linea.Split(",");

                    if (vlineas[4] != "") // Operación
                    {
                        if (vlineas[4].Contains("Carga") || vlineas[4].Contains("Descarga"))
                        {
                            if (vlineas[0] != "") fechaOperacion = CsvToTabDelimited(vlineas[0]);
                            if (vlineas[3] != "") tarjeta = CsvToTabDelimited(vlineas[3]);
                            if (vlineas[5] != "") descripcion = CsvToTabDelimited(vlineas[5]);
                            if (vlineas[6] != "") valorOperacion = CsvToTabDelimited(vlineas[6]);

                            DataRow dr = dt.NewRow();

                            if (fechaOperacion != "") dr["FechaTransaccion"] = Convert.ToDateTime(fechaOperacion);
                            Double _valorOperacion;
                            if (valorOperacion != "")
                            {
                                valorOperacion = valorOperacion.Replace(".", ",");
                                if (Double.TryParse(valorOperacion, out _valorOperacion))
                                    dr["Importe"] = Convert.ToDouble(_valorOperacion);
                            }
                            if (vlineas[4].Contains("Carga")) operacion = "Carga"; else operacion = "Descarga";

                            dr["NumTarjetaCliente"] = tarjeta;
                            dr["Descripcion"] = descripcion;
                            dr["Origen"] = "Fidely";
                            dr["Operacion"] = operacion;
                            dr["NombreArchivo"] = nombreArchivo;
                            dr["IDUsuario"] = oUsu.IDUsuario;

                            dt.Rows.Add(dr);
                        }
                    }
                }

                i++;
            }

            if (dt.Rows.Count > 0)
            {
                bTransaccion = new bTransaccion();
                bTransaccion.limpiarTransaccionesTmp();
                bTransaccion.insertarTransaccionesTmp(dt);
                bTransaccion.actualizarTransacciones(nombreArchivo, oUsu.IDUsuario);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            arch.Close();
        }
    }
    */
    private void importarArchivoPremios(string nombreArchivoRuta, bool esPrivado)
    {
        bPremio bPremio;
        try
        {
            DataTable dt = csvToDataTable(nombreArchivoRuta);
            if (dt.Rows.Count > 0)
            {
                bPremio = new bPremio();
                bPremio.limpiarPremiosTmp();
                bPremio.insertarPremiosTmp(dt, nombreArchivoRuta);
                bPremio.actualizarPremios(esPrivado);
            }
            else
                throw new Exception("No hay datos en el archivo. Revise el formato");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    private static DataTable csvToDataTable(string path)
    {

        DataTable dt = new DataTable();

        dt.Columns.Add("TipoMov");
        dt.Columns.Add("Codigo");
        dt.Columns.Add("ValorPuntos");
        dt.Columns.Add("FechaVigenciaDesde");
        dt.Columns.Add("FechaVigenciaHasta");
        dt.Columns.Add("Descripcion");
        dt.Columns.Add("Foto");
        dt.Columns.Add("StockActual");
        dt.Columns.Add("ValorPesos");
        dt.Columns.Add("IDMarca");
        dt.Columns.Add("IDRubro");
        dt.Columns.Add("StockMinimo");

        string csvContentStr = File.ReadAllText(path);
        string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
        int errores = 0;
        int total = 0;

        #region Armo Datatable

        for (int i = 0; i < rows.Length; i++)
        {
            //Split the tab separated fields (comma separated split commented)
            if (i > 0)
            {
                string[] dr = rows[i].Split(new char[] { ';' });

                if (dr.Length > 0)
                {
                    #region Armo DataRow
                    if (dr[0].Trim() != "")
                    {
                        total++;
                        try
                        {
                            DataRow drNew = dt.NewRow();
                            drNew[0] = dr[0].Trim() == "" ? null : dr[0].Trim();
                            drNew[1] = dr[1].Trim();
                            drNew[2] = dr[2].Trim();
                            drNew[3] = dr[3].Trim() == "" ? null : dr[3].Trim();
                            drNew[4] = dr[4].Trim() == "" ? null : dr[4].Trim();
                            drNew[5] = dr[5].Trim() == "" ? null : dr[5].Trim();
                            drNew[6] = dr[6].Trim() == "" ? null : dr[6].Trim();

                            drNew[7] = dr[7].Trim();
                            //drNew[8] = dr[8].Trim();
                            drNew[8] = dr[8].Trim() == "" ? 0 : (int)decimal.Parse(dr[8].Trim().ToString().Replace(".", ","));
                            //drNew[9] = dr[9].Trim() == "" ? 0 : (int)decimal.Parse(dr[9].Trim().ToString().Replace(".", ","));
                            drNew[9] = dr[9].Trim() == "" ? null : dr[9].Trim();
                            drNew[10] = dr[10].Trim() == "" ? null : dr[10].Trim();
                            drNew[11] = dr[11].Trim() == "" ? null : dr[11].Trim(); ;

                            dt.Rows.Add(drNew);
                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            errores++;
                        }
                    }

                    #endregion
                }
            }
        }
        return dt;
        #endregion
    }

    public static string CsvToTabDelimited(string line)
    {
        var ret = new StringBuilder(line.Length);
        bool inQuotes = false;
        for (int idx = 0; idx < line.Length; idx++)
        {
            if (line[idx] == '"')
            {
                inQuotes = !inQuotes;
            }
            else
            {
                if (line[idx] == ',')
                {
                    ret.Append(inQuotes ? ',' : '\t');
                }
                else
                {
                    ret.Append(line[idx]);
                }
            }
        }
        return ret.ToString();
    }

    public static System.Boolean IsNumeric(System.Object Expression)
    {
        if (Expression == null || Expression is DateTime)
            return false;

        if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
            return true;

        try
        {
            if (Expression is string)
                Double.Parse(Expression as string);
            else
                Double.Parse(Expression.ToString());
            return true;
        }
        catch { }
        return false;
    }
}