﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class WEB_Usuarios : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        using (var dbContext = new ACHEEntities())
        {
            return dbContext.Usuarios
                .OrderBy(x => x.IDUsuario)
                .Select(x => new UsuariosViewModel()
                {
                    IDUsuario = x.IDUsuario,
                    Nombre = x.Nombre,
                    Apellido = x.Apellido,
                    Pwd=x.Pwd,
                    Usuario = x.Usuario,
                    Email = x.Email,
                    Activo = x.Activo ? "Si" : "No"
                }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
        }
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        try
        {
            bUsuario bUsuario = new bUsuario();
            bUsuario.deleteUsuario(id);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}