﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.IO;
using System.Data.Entity.Infrastructure;
using ACHE.Model.ViewModels;
using System.Data;
using System.Text;

public partial class modulos_seguridad_GeneracionDat : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.limpiarControles();
            try
            {
                ddlDia.SelectedValue = (((int)DateTime.Now.DayOfWeek) + 1).ToString();
            }
            catch (Exception ex) { }
        }
    }

    [WebMethod(true)]
    public static string generar(string TipoArchivo, string NroProceso, string diaDescuento)
    {
        string nombreArchivo = "";
        try
        {
            //Beneficios - Comercios
            if (TipoArchivo.Equals("1"))
                nombreArchivo = ACHE.Services.GeneracionDat.generarComerciosBeneficios(NroProceso, diaDescuento, HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/"));
            //Beneficios - Tarjetas
            else if (TipoArchivo.Equals("2"))
                nombreArchivo = ACHE.Services.GeneracionDat.generarTarjetasBeneficios(NroProceso, HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/"));
            //Puntos - Comercios
            //else if (TipoArchivo.Equals("3"))
            //    nombreArchivo = generarComerciosPuntos(NroProceso);
            //Puntos - Tarjetas
            else if (TipoArchivo.Equals("4"))
                nombreArchivo = ACHE.Services.GeneracionDat.generarTarjetasPuntos(NroProceso, HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/"));
            //Puntos - Cuentas
            else if (TipoArchivo.Equals("5"))
                nombreArchivo = ACHE.Services.GeneracionDat.generarCuentasPuntos(NroProceso, HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/"));// generarCuentasPuntos(NroProceso);
            //Puntos - Premios
            else if (TipoArchivo.Equals("6"))
                nombreArchivo = ACHE.Services.GeneracionDat.generarPremiosPuntos(NroProceso, HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/"));// generarPremiosPuntos(NroProceso);

            return nombreArchivo;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #region Puntos

   

    
   

    /*private static string generarComerciosPuntos(string NroProceso)
    {
        string rutaArchivo = "";
        string _nroProceso = NroProceso;
        //Datos del nombre --
        string nombreCompletoArchivo = "";
        string nombreArchivo = "COMCLUBIN_";
        string extensionArchivo = ".DAT";
        int longNroProcesoArchivo = 10;
        //Contenido: Cabecera --
        string cabeceraCompleta = "";
        string caracterCabecera = "C";
        int longNroProcesoCabecera = 5;
        string horaCompleta = "";
        int longCabecera = 123;
        //Contenido: Pie --
        string pieCompleto = "";
        string caracterPie = "T";
        string cantReg = "";
        int longNroProcesoPie = 5;
        int longPie = 123;
        int longRegPie = 6;
        //Contenido --
        string contenidoCompleto = "";
        string caracterContenido = "D";
        int longContenido = 123;
        string tipoMov = "";
        string numEst = "";
        int longNumEst = 15;
        string marca = "1";
        string idBanco = "90";
        string nroNumEst = "";
        int longNroNumEst = 12;
        string nombreEst = "";
        int longNombreEst = 40;
        string localidad = "";
        int longLocalidad = 15;
        string pais = "AR";
        string rubroNac = "0000";
        string rubroInt = "0000";
        string estado = "";
        int longEstado = 2;
        string banco = "935";
        bComercio bComercio;
        List<eComercio> listComercios;
        try
        {
            //Si hay datos se crea el archivo
            bComercio = new bComercio();
            listComercios = bComercio.getComercios();
            if (listComercios.Count > 0)
            {
                cantReg = listComercios.Count.ToString();

                //Fecha
                string anio = DateTime.Now.Year.ToString();
                string mes = DateTime.Now.Month.ToString();
                string dia = DateTime.Now.Day.ToString();
                string hora = DateTime.Now.Hour.ToString();
                string minutos = DateTime.Now.Minute.ToString();
                string segundos = DateTime.Now.Second.ToString();

                if (mes.Length.Equals(1))
                    mes = mes.PadLeft(2, '0');
                if (dia.Length.Equals(1))
                    dia = dia.PadLeft(2, '0');
                if (hora.Length.Equals(1))
                    hora = hora.PadLeft(2, '0');
                if (minutos.Length.Equals(1))
                    minutos = minutos.PadLeft(2, '0');
                if (segundos.Length.Equals(1))
                    segundos = segundos.PadLeft(2, '0');

                //Nombre del archivo
                NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
                nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
                rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Puntos/") + Path.GetFileName(nombreCompletoArchivo);

                //Cabecera
                NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
                horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
                cabeceraCompleta = caracterCabecera + NroProceso + anio + mes + dia + horaCompleta;
                cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

                //Pie
                NroProceso = _nroProceso.PadLeft(longNroProcesoPie, '0');
                cantReg = cantReg.PadLeft(longRegPie, '0');
                pieCompleto = caracterPie + NroProceso + anio + mes + dia + cantReg;
                pieCompleto = pieCompleto.PadRight(longPie, ' ');

                using (StreamWriter file = new StreamWriter(rutaArchivo))
                {
                    //Se agrega la cabecera
                    cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
                    file.WriteLine(cabeceraCompleta);

                    //Contenido   
                    foreach (eComercio oCom in listComercios)
                    {
                        if (oCom.TipoMov == null) oCom.TipoMov = "";
                        tipoMov = oCom.TipoMov.Trim();

                        if (oCom.NumEst == null) oCom.NumEst = "";
                        numEst = oCom.NumEst.Trim();
                        numEst = numEst.PadLeft(longNumEst, '0');

                        if (oCom.NroNumEst == null) oCom.NroNumEst = "";
                        nroNumEst = oCom.NroNumEst.Trim();
                        nroNumEst = nroNumEst.PadLeft(longNroNumEst, '0');

                        if (oCom.NombreEst == null) oCom.NombreEst = "";
                        nombreEst = oCom.NombreEst.Trim();//TODO: Remover y long
                        nombreEst = nombreEst.PadRight(longNombreEst, ' ');

                        if (oCom.oDomicilio.Ciudad == null) oCom.oDomicilio.Ciudad = "";
                        localidad = oCom.oDomicilio.Ciudad.Trim();//TODO: Remover y long
                        localidad = localidad.PadRight(longLocalidad, ' ');

                        if (oCom.Estado == null) oCom.Estado = "";
                        estado = oCom.Estado.Trim();//TODO: Ver esto
                        estado = estado.PadLeft(longEstado, '0');

                        contenidoCompleto = caracterContenido + tipoMov + numEst + marca + idBanco + nroNumEst + nombreEst + localidad + pais + rubroNac + rubroInt + estado + banco;
                        contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
                        //Se agrega el Contenido
                        contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
                        file.WriteLine(contenidoCompleto);
                    }

                    //Se agrega el pie
                    pieCompleto = StringExtensions.RemoverCaracteresEspeciales(pieCompleto);
                    file.WriteLine(pieCompleto);
                }

                return "/ArchivosTemp/Puntos/" + nombreCompletoArchivo;
            }
            else
            {
                throw new Exception("No hay datos para generar el archivo.");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }*/

    #endregion

    #region Beneficios

    //private static string generarComerciosBeneficios(string NroProceso, string diaDescuentos)
    //{
    //    string rutaArchivo = "";
    //    string _nroProceso = NroProceso;
    //    //Datos del nombre --
    //    string nombreCompletoArchivo = "";
    //    string nombreArchivo = "COMCLUBIN_";
    //    string extensionArchivo = ".DAT";
    //    int longNroProcesoArchivo = 10;
    //    //Contenido: Cabecera --
    //    string cabeceraCompleta = "";
    //    string caracterCabecera = "HDR";
    //    int longNroProcesoCabecera = 10;
    //    string horaCompleta = "";
    //    int longCabecera = 123;
    //    //Contenido --
    //    string contenidoCompleto = "";
    //    //string caracterContenido = "D";
    //    int longContenido = 123;
    //    string CodAct = "";
    //    //int longCodAct = 2;
    //    string NumEst = "";
    //    //int longNumEst = 15;
    //    string NombreEstab = "";
    //    int longNombreEstab = 40;
    //    string localidad = "";
    //    int longLocalidad = 15;
    //    string pais = "AR";
    //    string rubroNac = "0000";
    //    string rubroInter = "0000";
    //    string moneda = "032";
    //    string estado = "";
    //    //int longEstado = 2;
    //    string provincia = "00";
    //    string banco = "935";
    //    string sucursal = "935";
    //    string descuento = "";
    //    int longDescuento = 2;
    //    string relleno1 = "0";
    //    string descuentoVip = "";
    //    int longDescuentoVip = 2;
    //    string affinity = "";
    //    int longAffinity = 4;
    //    string maxCuotas = "01";
    //    string tipoPlan = "0";
    //    string relleno2 = "0000";
    //    string grupoCerr = "0";
    //    bComercio bComercio;
    //    try
    //    {
    //        //Si hay datos se crea el archivo
    //        bComercio = new bComercio();
    //        var listComercios = bComercio.getComerciosForVisa();
    //        if (listComercios.Count > 0)
    //        {
    //            //Fecha
    //            string anio = DateTime.Now.Year.ToString();
    //            string mes = DateTime.Now.Month.ToString();
    //            string dia = DateTime.Now.Day.ToString();
    //            string hora = DateTime.Now.Hour.ToString();
    //            string minutos = DateTime.Now.Minute.ToString();
    //            string segundos = DateTime.Now.Second.ToString();

    //            if (mes.Length.Equals(1))
    //                mes = mes.PadLeft(2, '0');
    //            if (dia.Length.Equals(1))
    //                dia = dia.PadLeft(2, '0');
    //            if (hora.Length.Equals(1))
    //                hora = hora.PadLeft(2, '0');
    //            if (minutos.Length.Equals(1))
    //                minutos = minutos.PadLeft(2, '0');
    //            if (segundos.Length.Equals(1))
    //                segundos = segundos.PadLeft(2, '0');

    //            //Nombre del archivo
    //            NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
    //            nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
    //            rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

    //            //Cabecera
    //            NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
    //            horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
    //            cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
    //            cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

    //            using (StreamWriter file = new StreamWriter(rutaArchivo))
    //            {
    //                //Se agrega la cabecera
    //                cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
    //                file.WriteLine(cabeceraCompleta);

    //                //Contenido   
    //                foreach (Terminales oCom in listComercios)
    //                {
    //                    if (oCom.POSSistema != "SOFT" && oCom.POSSistema != "POS")
    //                    {
    //                        try
    //                        {
    //                            //if (oCom.Estado == null) oCom.CodAct = "";
    //                            CodAct = oCom.Estado + "135";
    //                            //if (oCom.NumEstBenef == null) oCom.NumEstBenef = "";
    //                            NumEst = int.Parse(oCom.NumEst).ToString();
    //                            //if (NumEst.Contains("e")) NumEst = Double.Parse(NumEst, System.Globalization.NumberStyles.Float).ToString().Replace('.', ' ');
    //                            NumEst = NumEst.PadLeft(12, '0');

    //                            NombreEstab = bComercio.getComercio(oCom.IDComercio).NombreEst;
    //                            if (NombreEstab == null)
    //                                NombreEstab = "";

    //                            if (NombreEstab.Length > longNombreEstab)
    //                                NombreEstab = NombreEstab.Substring(0, longNombreEstab);
    //                            NombreEstab = NombreEstab.PadRight(longNombreEstab, ' ');

    //                            if (oCom.Domicilios != null)
    //                            {
    //                                if (!oCom.Domicilios.Ciudad.HasValue)
    //                                    localidad = "";
    //                                else
    //                                    localidad = oCom.Domicilios.Ciudades.Nombre;
    //                                if (localidad.Length > longLocalidad)
    //                                    localidad = localidad.Substring(0, longLocalidad);
    //                                localidad = localidad.PadRight(longLocalidad, ' ');
    //                            }
    //                            else
    //                            {
    //                                localidad = "";
    //                                localidad = localidad.PadRight(longLocalidad, ' ');
    //                            }

    //                            //if (oCom.EstadoBenef == null) oCom.EstadoBenef = "";
    //                            //estado = oCom.EstadoBenef;
    //                            //estado = estado.PadLeft(longEstado, '0');
    //                            if (oCom.Estado == "DE")
    //                                estado = "39";
    //                            else
    //                                estado = "30";

    //                            descuentoVip = "";
    //                            descuento = "";
    //                            //if (oCom.Descuento == null) oCom.Descuento = "";
    //                            if (diaDescuentos == "1")
    //                            {
    //                                descuento = oCom.Descuento.ToString();
    //                                if (oCom.DescuentoVip.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "2")
    //                            {
    //                                descuento = oCom.Descuento2.ToString();
    //                                if (oCom.DescuentoVip2.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip2.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "3")
    //                            {
    //                                descuento = oCom.Descuento3.ToString();
    //                                if (oCom.DescuentoVip3.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip3.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "4")
    //                            {
    //                                descuento = oCom.Descuento4.ToString();
    //                                if (oCom.DescuentoVip4.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip4.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "5")
    //                            {
    //                                descuento = oCom.Descuento5.ToString();
    //                                if (oCom.DescuentoVip5.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip5.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "6")
    //                            {
    //                                descuento = oCom.Descuento6.ToString();
    //                                if (oCom.DescuentoVip6.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip6.Value.ToString();
    //                            }
    //                            else if (diaDescuentos == "7")
    //                            {
    //                                descuento = oCom.Descuento7.ToString();
    //                                if (oCom.DescuentoVip7.HasValue)
    //                                    descuentoVip = oCom.DescuentoVip7.Value.ToString();
    //                            }

    //                            descuento = descuento.PadLeft(longDescuento, '0');
    //                            descuentoVip = descuentoVip.PadLeft(longDescuentoVip, '0');

    //                            if (oCom.AffinityBenef == null) oCom.AffinityBenef = "";
    //                            affinity = oCom.AffinityBenef;
    //                            affinity = affinity.PadLeft(longAffinity, '0');

    //                            contenidoCompleto = CodAct + NumEst + NombreEstab + localidad + pais + rubroNac + rubroInter + moneda
    //                            + estado + provincia + banco + sucursal + descuento + relleno1 + descuentoVip + affinity + maxCuotas + tipoPlan + relleno2 + grupoCerr;
    //                            contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
    //                            //Se agrega el Contenido
    //                            contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
    //                            file.WriteLine(contenidoCompleto);
    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            var msg = ex.Message;
    //                        }
    //                    }
    //                }
    //            }

    //            return "/ArchivosTemp/Beneficios/" + nombreCompletoArchivo;
    //        }
    //        else
    //        {
    //            throw new Exception("No hay datos para generar el archivo.");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //private static string generarTarjetasBeneficios(string NroProceso)
    //{
    //    string rutaArchivo = "";
    //    string _nroProceso = NroProceso;
    //    //Datos del nombre --
    //    string nombreCompletoArchivo = "";
    //    string nombreArchivo = "TARJCLUBIN_";
    //    string extensionArchivo = ".DAT";
    //    int longNroProcesoArchivo = 10;
    //    //Contenido: Cabecera --
    //    string cabeceraCompleta = "";
    //    string caracterCabecera = "HDR";
    //    int longNroProcesoCabecera = 10;
    //    string horaCompleta = "";
    //    int longCabecera = 80;
    //    //Contenido --
    //    string contenidoCompleto = "";
    //    int longContenido = 90;
    //    string numTar = "";
    //    int longNumTar = 19;
    //    string longLongNumTar = "";
    //    int _longLongNumTar = 2;
    //    string estado = "";
    //    int longEstado = 2;
    //    string limCompra = "";
    //    int longLimCompra = 9;
    //    string limAdelanto = "";
    //    int longLimAdelanto = 9;
    //    string limCuotas = "";
    //    int longLimCuotas = 9;
    //    string fecha = "";
    //    int longFecha = 9;
    //    string cuentaAsoc = "";
    //    int longCuentaAsoc = 15;
    //    string limProp = "";
    //    string AammVto = "";
    //    int longAammVto = 4;
    //    string affinity = "";
    //    int longAffinity = 4;

    //    try
    //    {
    //        using (var dbContext = new ACHEEntities())
    //        {
    //            var listTarjetas = dbContext.Tarjetas.Include("Marcas")//.Where(x => x.IDSocio.HasValue)
    //                .Select(x => new
    //                {
    //                    IDTarjeta = x.IDTarjeta,
    //                    Numero = x.Numero,
    //                    IDMarca = x.IDMarca,
    //                    Affinity = x.Marcas.Affinity,
    //                    //PuntosDisponibles = x.PuntosTotales,
    //                    Vencimiento = x.FechaVencimiento,
    //                    Estado = x.Estado
    //                }).ToList();

    //            if (listTarjetas.Count > 0)
    //            {
    //                //Fecha
    //                string anio = DateTime.Now.Year.ToString();
    //                string mes = DateTime.Now.Month.ToString();
    //                string dia = DateTime.Now.Day.ToString();
    //                string hora = DateTime.Now.Hour.ToString();
    //                string minutos = DateTime.Now.Minute.ToString();
    //                string segundos = DateTime.Now.Second.ToString();

    //                if (mes.Length.Equals(1))
    //                    mes = mes.PadLeft(2, '0');
    //                if (dia.Length.Equals(1))
    //                    dia = dia.PadLeft(2, '0');
    //                if (hora.Length.Equals(1))
    //                    hora = hora.PadLeft(2, '0');
    //                if (minutos.Length.Equals(1))
    //                    minutos = minutos.PadLeft(2, '0');
    //                if (segundos.Length.Equals(1))
    //                    segundos = segundos.PadLeft(2, '0');

    //                //Nombre del archivo
    //                NroProceso = _nroProceso.PadLeft(longNroProcesoArchivo, '0');
    //                nombreCompletoArchivo = nombreArchivo + NroProceso + extensionArchivo;
    //                rutaArchivo = HttpContext.Current.Server.MapPath("ArchivosTemp/Beneficios/") + Path.GetFileName(nombreCompletoArchivo);

    //                //Cabecera
    //                NroProceso = _nroProceso.PadLeft(longNroProcesoCabecera, '0');
    //                horaCompleta = hora.ToString() + minutos.ToString() + segundos.ToString();
    //                cabeceraCompleta = caracterCabecera + anio + mes + dia + horaCompleta + NroProceso;
    //                cabeceraCompleta = cabeceraCompleta.PadRight(longCabecera, ' ');

    //                using (StreamWriter file = new StreamWriter(rutaArchivo))
    //                {
    //                    //Se agrega la cabecera
    //                    cabeceraCompleta = StringExtensions.RemoverCaracteresEspeciales(cabeceraCompleta);
    //                    file.WriteLine(cabeceraCompleta);

    //                    //Contenido   
    //                    foreach (var oTar in listTarjetas)
    //                    {
    //                        //if (oTar.IDMarca == 2)
    //                        //{
    //                        //if (oSoc.NumTar == null) oSoc.NumTar = "";
    //                        numTar = oTar.Numero;
    //                        numTar = numTar.PadRight(longNumTar, ' ');

    //                        //if (oSoc.LongNumTar == null) oSoc.LongNumTar = "";
    //                        longLongNumTar = "16";
    //                        longLongNumTar = longLongNumTar.PadRight(_longLongNumTar, '0');

    //                        //if (oSoc.Estado == null) oSoc.Estado = "";
    //                        estado = "20";//oSoc.Estado;
    //                        if (oTar.Estado == "B" || oTar.Vencimiento.Date < DateTime.Now.Date)
    //                            estado = "29";//oSoc.Estado;
    //                        estado = estado.PadRight(longEstado, '0');

    //                        //if (oSoc.LimCompra == null) oSoc.LimCompra = "";
    //                        if (oTar.IDMarca == 2)//CuponIN
    //                            limCompra = "000000001";
    //                        else
    //                            limCompra = "000000000";
    //                        limCompra = limCompra.PadRight(longLimCompra, '0');

    //                        //if (oSoc.LimAdelanto == null) oSoc.LimAdelanto = "";
    //                        limAdelanto = "000000000";// oSoc.LimAdelanto;
    //                        limAdelanto = limAdelanto.PadRight(longLimAdelanto, '0');

    //                        //if (oSoc.LimCuotas == null) oSoc.LimCuotas = "";
    //                        limCuotas = "000000000";// oSoc.LimCuotas;
    //                        limCuotas = limCuotas.PadRight(longLimCuotas, '0');

    //                        //if (oSoc.Fecha == null) oSoc.Fecha = DateTime.Now;
    //                        //if (oSoc.Fecha.ToString().Contains("0001")) oSoc.Fecha = DateTime.Now;
    //                        fecha = formatoJuliano(DateTime.Now);
    //                        fecha = fecha + hora + minutos;
    //                        fecha = fecha.PadLeft(longFecha, '0');

    //                        //if (oSoc.CuentaAsoc == null) oSoc.CuentaAsoc = "";
    //                        cuentaAsoc = "000000000";//oSoc.CuentaAsoc;
    //                        cuentaAsoc = cuentaAsoc.PadLeft(longCuentaAsoc, '0');

    //                        //if (oSoc.LimProp == null) oSoc.LimProp = "";
    //                        limProp = "T";// oSoc.LimProp;

    //                        //if (oSoc.AammVto == null) oSoc.AammVto = "4912";
    //                        AammVto = "4912"; //oSoc.AammVto;
    //                        AammVto = AammVto.PadLeft(longAammVto, '0');

    //                        //if (oSoc.AffinityBenef == null) oSoc.AffinityBenef = "";
    //                        affinity = oTar.Affinity;// "0000";// oSoc.AffinityBenef;
    //                        affinity = affinity.PadLeft(longAffinity, '0');

    //                        contenidoCompleto = numTar + longNumTar + estado + limCompra + limAdelanto + limCuotas + fecha + cuentaAsoc + limProp + AammVto + affinity;
    //                        contenidoCompleto = contenidoCompleto.PadRight(longContenido, ' ');
    //                        //Se agrega el Contenido
    //                        contenidoCompleto = StringExtensions.RemoverCaracteresEspeciales(contenidoCompleto);
    //                        file.WriteLine(contenidoCompleto);
    //                        //}
    //                    }
    //                }

    //                return "/ArchivosTemp/Beneficios/" + nombreCompletoArchivo;
    //            }
    //            else
    //                throw new Exception("No hay datos para generar el archivo.");
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    #endregion

   

    private void limpiarControles()
    {
        this.txtNroProceso.Text = "";
    }



    #region exportar
    [WebMethod(true)]
    public static string exportar(string TipoArchivo, string NroProceso, string diaDescuento)
    {
        string nombreArchivo = "";
        try
        {
            if (TipoArchivo.Equals("1"))
            {
                nombreArchivo = ExportarComerciosBeneficios(NroProceso, diaDescuento);
            }
            else if (TipoArchivo.Equals("2"))
            {
                nombreArchivo = ExportarTarjetasBeneficios(NroProceso);
            }
            else if (TipoArchivo.Equals("4"))
            {
                nombreArchivo = ExportarTarjetasPuntos(NroProceso);
            }
            else if (TipoArchivo.Equals("5"))
            {
                nombreArchivo = ExportarCuentasPuntos(NroProceso);
            }
            else if (TipoArchivo.Equals("6"))
            {
                nombreArchivo = ExportarPremiosPuntos(NroProceso);
            }
            return nombreArchivo;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
    public static string ExportarPremiosPuntos(string NroProceso)
    {
        List<PremiosPuntosViewModel> listaPremiosPuntos = new List<PremiosPuntosViewModel>();
        List<Premios> listPremios;

        cPremio cPremio = new cPremio();

        listPremios = cPremio.getPremios();
        if (listPremios.Count > 0)
        {

            foreach (Premios oPrem in listPremios)
            {
                PremiosPuntosViewModel item = new PremiosPuntosViewModel();
                item.codProd = oPrem.IDMarca.HasValue ? oPrem.Marcas.Codigo + oPrem.Codigo : oPrem.Codigo;
                item.valorPuntos = oPrem.ValorPuntos.ToString().Trim();
                listaPremiosPuntos.Add(item);

            }
        }


        string fileName = "PremiosPuntos" + "_" + NroProceso;
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    dt = listaPremiosPuntos.Select(x => new
                    {
                        Codigo_Producto=x.codProd,
                        Valor_Puntos=x.valorPuntos
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }
    public static string ExportarTarjetasPuntos(string NroProceso)
    {
        List<TarjetasPuntosViewModel> listaTarjetasPuntos = new List<TarjetasPuntosViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            var listTarjetas = dbContext.Tarjetas
                .Select(x => new
                {
                    IDTarjeta = x.IDTarjeta,
                    Numero = x.Numero,
                    Estado = x.FechaBaja.HasValue ? "29" : "20",
                    Cuenta = x.IDTarjeta
                }).ToList();

            if (listTarjetas.Count > 0)
            {
                foreach (var oTar in listTarjetas)
                {
                    TarjetasPuntosViewModel item = new TarjetasPuntosViewModel();


                    item.numTar = oTar.Numero.Trim(); ;
                    item.EstadoTarjeta = oTar.Estado;
                    listaTarjetasPuntos.Add(item);
                }
            }
        }

        string fileName = "TarjetasPuntos_" + NroProceso;
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    dt = listaTarjetasPuntos.Select(x => new {
                        Numero_Tarjeta=x.numTar,
                        Estado=x.EstadoTarjeta
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static string ExportarCuentasPuntos(string NroProceso)
    {
        List<CuentasPuntosViewModel> listaCuentasPuntos = new List<CuentasPuntosViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            dbContext.Database.ExecuteSqlCommand("exec tmpActualizarArchivosVisa", new object[] { });

            var listTarjetas = dbContext.InformeTarjetasVisa//.Where(x => x.IDTarjeta == 34050 || x.IDTarjeta == 41859 || x.IDTarjeta == 587)
                .Select(x => new
                {
                    Cuenta = x.IDTarjeta,
                    PuntosDisponibles = x.Credito + x.Giftcard,
                    Estado = x.FechaBaja.HasValue ? "19" : "10"
                }).OrderByDescending(x => x.PuntosDisponibles).ToList();

            if (listTarjetas.Count > 0)
            {
                //Contenido   
                foreach (var oTar in listTarjetas)
                {
                    CuentasPuntosViewModel item = new CuentasPuntosViewModel();
                    int puntos = 0;
                    if (oTar.Estado == "10")
                    {
                        puntos = (int)(oTar.PuntosDisponibles ?? 0);
                        if (puntos < 0)
                            puntos = 0;
                    }
                    item.numCuenta = oTar.Cuenta.ToString();
                    item.puntosDisponibles = puntos.ToString();
                    listaCuentasPuntos.Add(item);
                }
            }
        }
        string fileName = "CuentasPuntos_" + NroProceso;
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    dt = listaCuentasPuntos.Select(x => new
                    {
                        Numero_Cuenta=x.numCuenta,
                        Puntos=x.puntosDisponibles
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }


    public static string ExportarComerciosBeneficios(string NroProceso, string diaDescuentos)
    {

        List<ComerciosBeneficiosViewModel> listaComerciosBeneficios = new List<ComerciosBeneficiosViewModel>();
        bComercio bComercio = new bComercio();
        var listComercios = bComercio.getComerciosForVisa();
        string descuento = "";
        string descuentoVip="";
        if (listComercios.Count > 0)
        {
            foreach (Terminales oCom in listComercios)
            {
                ComerciosBeneficiosViewModel item = new ComerciosBeneficiosViewModel();
                if (oCom.POSSistema != "SOFT")
                {

                    item.NumEst = int.Parse(oCom.NumEst).ToString();

                    var NombreEstab = bComercio.getComercio(oCom.IDComercio).NombreEst;
                    if (NombreEstab == null)
                        NombreEstab = "";

                    if (diaDescuentos == "1")
                    {
                        descuento = oCom.Descuento.ToString();
                        if (oCom.DescuentoVip.HasValue)
                            descuentoVip = oCom.DescuentoVip.Value.ToString();
                    }
                    else if (diaDescuentos == "2")
                    {
                        descuento = oCom.Descuento2.ToString();
                        if (oCom.DescuentoVip2.HasValue)
                            descuentoVip = oCom.DescuentoVip2.Value.ToString();
                    }
                    else if (diaDescuentos == "3")
                    {
                        descuento = oCom.Descuento3.ToString();
                        if (oCom.DescuentoVip3.HasValue)
                            descuentoVip = oCom.DescuentoVip3.Value.ToString();
                    }
                    else if (diaDescuentos == "4")
                    {
                        descuento = oCom.Descuento4.ToString();
                        if (oCom.DescuentoVip4.HasValue)
                            descuentoVip = oCom.DescuentoVip4.Value.ToString();
                    }
                    else if (diaDescuentos == "5")
                    {
                        descuento = oCom.Descuento5.ToString();
                        if (oCom.DescuentoVip5.HasValue)
                            descuentoVip = oCom.DescuentoVip5.Value.ToString();
                    }
                    else if (diaDescuentos == "6")
                    {
                        descuento = oCom.Descuento6.ToString();
                        if (oCom.DescuentoVip6.HasValue)
                            descuentoVip = oCom.DescuentoVip6.Value.ToString();
                    }
                    else if (diaDescuentos == "7")
                    {
                        descuento = oCom.Descuento7.ToString();
                        if (oCom.DescuentoVip7.HasValue)
                            descuentoVip = oCom.DescuentoVip7.Value.ToString();
                    }
                    item.descuentoVip = descuentoVip;
                    item.descuento = descuento;
                    listaComerciosBeneficios.Add(item);
                }
            }
        }

        string fileName = "ComerciosBeneficios_" + NroProceso;
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                   
                    dt = listaComerciosBeneficios.Select(x => new
                    {
                        Numero_Establecimiento=x.NumEst,
                        Nombre_Establecimiento=x.NombreEstab,
                        Descuento=x.descuento,
                        Descuento_Vip=x.descuentoVip

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static string ExportarTarjetasBeneficios(string NroProceso)
    {
        List<TarjetasBeneficiosViewModel> listaTarjetasBeneficios = new List<TarjetasBeneficiosViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            var listTarjetas = dbContext.Tarjetas.Include("Marcas")//.Where(x => x.IDSocio.HasValue)
                .Select(x => new
                {
                    IDTarjeta = x.IDTarjeta,
                    Numero = x.Numero,
                    IDMarca = x.IDMarca,
                    Affinity = x.Marcas.Affinity,
                    //PuntosDisponibles = x.PuntosTotales,
                    Estado = x.Estado
                }).ToList();

            if (listTarjetas.Count > 0)
            {
                //Contenido   
                foreach (var oTar in listTarjetas)
                {
                    TarjetasBeneficiosViewModel item = new TarjetasBeneficiosViewModel();
                    item.numTar = oTar.Numero;

                    item.affinity = oTar.Affinity;

                    listaTarjetasBeneficios.Add(item);
                }
            }
        }
        string fileName = "TarjetasBeneficios_" + NroProceso;
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    dt = listaTarjetasBeneficios.Select(x=>new
                    {
                        Numero_Tarjeta=x.numTar,
                        Affinity=x.affinity

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }




    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        StringBuilder fileContent = new StringBuilder();
        foreach (var col in dt.Columns)
        {
            fileContent.Append(col.ToString() + ";");
        }

        fileContent.Replace(";", System.Environment.NewLine, fileContent.Length - 1, 1);



        foreach (DataRow dr in dt.Rows)
        {
            foreach (var column in dr.ItemArray)
            {
                fileContent.Append("\"" + column.ToString() + "\";");
            }

            fileContent.Replace(";", System.Environment.NewLine, fileContent.Length - 1, 1);
        }

        System.IO.File.WriteAllText(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv", fileContent.ToString());

    }
    #endregion
}