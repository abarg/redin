﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.IO;
using FileHelpers;
using System.Collections;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;



public partial class modulos_seguridad_Galicia : PaginaBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["ListaFacturas"] = null;

            txtVencimiento1.Text = (DateTime.Now.AddWorkdays(2)).ToString("dd-MM-yyyy");
            txtVencimiento2.Text = (DateTime.Now.AddWorkdays(4)).ToString("dd-MM-yyyy");
            txtVencimiento3.Text = (DateTime.Now.AddWorkdays(6)).ToString("dd-MM-yyyy");

        }
    }

    private void CargarFacturas(DateTime fechaDesde, DateTime fechaHasta, string filtro, string tipoTarjeta)
    {
        divNoFacturas.Visible = false;
        divPaso2.Visible = false;
        using (var dbContext = new ACHEEntities())
        {
            var facturas = dbContext.Facturas
                .Include("Comercios").Include("FacturasRetenciones").Include("OrdenesDepago").Include("comercios.Marcas")
                .Where(x => x.Modo == "R" && x.Comercios.FormaPago == "D"
                    && !string.IsNullOrEmpty(x.Comercios.FormaPago_CBU) && x.PeriodoDesde >= fechaDesde && x.PeriodoDesde <= fechaHasta)//x.FechaCAE != null
                .Select(x => new FacturasGaliciaViewModel
                {
                    IDFactura = x.IDFactura,
                    Nombre = x.Comercios.RazonSocial + " - " + x.Numero + " - " + x.Comercios.NombreFantasia + " - " + x.NroDocumento,
                    IDComercio = x.IDComercio ?? 0,
                    FechaAlta = x.FechaAlta,
                    Tipo = x.Tipo,
                    Numero = x.Numero,
                    Documento = x.NroDocumento,
                    ImporteTotal = x.ImporteTotal,
                    FormaPago = x.Comercios.FormaPago,
                    FormaPagoCBU = x.Comercios.FormaPago_CBU,
                    FormaPagoTipoCuenta = (x.Comercios.FormaPago_TipoCuenta == "1") ? "CC" : "CA",
                    TipoDocumento = x.Comercios.TipoDocumento,
                    Retenciones = x.FacturasRetenciones.Any() ? x.FacturasRetenciones.Sum(y => y.Importe) : 0,
                    Canjes = x.OrdenesDePago.Any() ? x.OrdenesDePago.Sum(y => y.Importe) : 0,
                    ImporteADebitar = x.ImporteADebitar,
                    TipoTarjeta = x.Comercios.IDMarca.HasValue ? x.Comercios.Marcas.TipoTarjeta : ""
                    //0// x.FacturasDetalle.Any(y => y.Concepto.Contains("CANJE")) ? x.FacturasDetalle.Where(y => y.Concepto.Contains("CANJE")).Sum(y => y.PrecioUnitario) : 0,
                }).ToList();

            if (tipoTarjeta != string.Empty)
                facturas = facturas.Where(x => x.TipoTarjeta == tipoTarjeta).ToList();

            if (filtro.ToLower() == "con")
                facturas = facturas.Where(x => x.Retenciones > 0).ToList();
            else if (filtro.ToLower() == "sin")
                facturas = facturas.Where(x => x.Retenciones == 0).ToList();


            Session["ListaFacturas"] = facturas;

            if (facturas != null && facturas.Count() > 0)
            {
                searchable.DataSource = facturas.Select(x => new { IDFactura = x.IDFactura, Nombre = x.Nombre }).ToList();
                searchable.DataBind();
                divPaso2.Visible = true;
            }
            else
            {
                divNoFacturas.Visible = false;
                divNoFacturas.Visible = true;
            }
        }
    }

    private static string formatoCBU(string cbuOld, string tipoCuenta)
    {
        if (!string.IsNullOrEmpty(cbuOld))
        {
            if (cbuOld.Length == 22)
            {
                string nuevoCBU = string.Empty;
                string tipoCuentaString = string.Empty;

                string primeros8 = cbuOld.Substring(0, 8);
                string ultimos14 = cbuOld.Substring(7, 15);

                //nuevoCBU = "0" + primeros8 + "000" + ultimos14;
                nuevoCBU = primeros8 + "000" + ultimos14;
                return nuevoCBU;
            }
        }
        return (string.Empty).PadLeft(26, '0');
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        DateTime desde = DateTime.Parse(txtDesde.Text).Date;
        DateTime hasta = DateTime.Parse(txtHasta.Text).AddDays(1).AddTicks(-1);
        CargarFacturas(desde, hasta, ddlRetenciones.SelectedValue, ddlTipoTarjeta.SelectedValue);
    }

    [System.Web.Services.WebMethod]
    public static string EscribirArchivo(string venc1, string venc2, string venc3, string facturas)
    {
        //try {
        DateTime dtVenc1 = DateTime.Parse(venc1);
        DateTime dtVenc2 = DateTime.Parse(venc2);
        DateTime dtVenc3 = DateTime.Parse(venc3);

        using (var dbContext = new ACHEEntities())
        {
            List<FacturasGaliciaViewModel> detalles = new List<FacturasGaliciaViewModel>();
            string[] arr = facturas.Split(',');
            var listaFacturas = (List<FacturasGaliciaViewModel>)HttpContext.Current.Session["ListaFacturas"];
            if (listaFacturas != null)
            {
                foreach (var item in arr)
                {
                    int id = int.Parse(item);
                    var aux = listaFacturas.Where(x => x.IDFactura == id).FirstOrDefault();
                    if (aux != null)
                        detalles.Add(aux);
                }
            }
            else
            {
                foreach (var item in arr)
                {
                    int id = int.Parse(item);
                    var aux = dbContext.Facturas
                    .Include("Comercios")//.Include("FacturasRetenciones")//.Include("FacturasDetalle")
                    .Where(x => x.IDFactura == id)
                    .Select(x => new FacturasGaliciaViewModel
                    {
                        IDComercio = x.IDComercio ?? 0,
                        FechaAlta = x.FechaAlta,
                        Tipo = x.Tipo,
                        Numero = x.Numero,
                        Documento = x.NroDocumento,
                        ImporteTotal = x.ImporteTotal,
                        //Retenciones = x.FacturasRetenciones.Any() ? x.FacturasRetenciones.Sum(y => y.Importe) : 0,
                        //Canjes = 0,//x.FacturasDetalle.Any(y => y.Concepto.Contains("CANJE")) ? x.FacturasDetalle.Where(y => y.Concepto.Contains("CANJE")).Sum(y => y.PrecioUnitario) : 0,
                        //Canjes = x.OrdenesDePago.Any() ? x.OrdenesDePago.Sum(y => y.Importe) : 0,
                        FormaPago = x.Comercios.FormaPago,
                        FormaPagoCBU = x.Comercios.FormaPago_CBU,
                        FormaPagoTipoCuenta = (x.Comercios.FormaPago_TipoCuenta == "1") ? "CC" : "CA",
                        TipoDocumento = x.Comercios.TipoDocumento,
                        ImporteADebitar = x.ImporteADebitar
                    }).FirstOrDefault();
                    if (aux != null)
                        detalles.Add(aux);
                }
            }


            #region header y footer
            string header = string.Empty;
            string footer = string.Empty;
            string fechaHoy = DateTime.Now.ToString("yyyyMMdd");
            string importe = detalles.Sum(x => x.ImporteADebitar).ToString().Replace(",", "").PadLeft(14, '0');  //(detalles.Sum(x => x.ImporteTotal - x.Retenciones - x.Canjes)).ToString().Replace(",", "").PadLeft(14, '0');
            string cantRegistros = (detalles.Count()).ToString().PadLeft(7, '0');
            string espacios = (string.Empty).PadRight(304);

            header += "00015270C" + fechaHoy + "1EMPRESA" + importe.ToString() + cantRegistros + espacios;
            footer += "99915270C" + fechaHoy + "1EMPRESA" + importe.ToString() + cantRegistros + espacios;
            #endregion

            #region detalle
            List<DetalleFH> listaDetalle = new List<DetalleFH>();

            foreach (var det in detalles)
            {
                //var importeTotal = det.Tipo == "RI" ? det.ImporteTotal * 1.21M : det.ImporteTotal;
                var importeTotal = det.ImporteADebitar;// No hace falta mas ya que ya viene con iIVA * 1.21M;

                DetalleFH detalle = new DetalleFH();
                detalle.TipoRegistro = "370".PadLeft(4, '0');
                detalle.IdCliente = det.Documento.PadRight(22, ' '); //("30710708998").PadRight(22, ' ');
                detalle.Bloque1 = "0" + det.FormaPagoCBU.Substring(0, 7);   //"00070166";
                detalle.Digito1 = det.FormaPagoCBU.Substring(7, 1);
                detalle.Bloque2 = "000" + det.FormaPagoCBU.Substring(8, 13);
                detalle.Digito2 = det.FormaPagoCBU.Substring(det.FormaPagoCBU.Length - 1, 1);
                detalle.ReferenciaUnivoca = det.IDFactura.ToString().PadLeft(15, '0');
                detalle.FechaVto1 = dtVenc1.ToString("yyyyMMdd");
                detalle.ImporteVto1 = (Math.Round(importeTotal, 2).ToString().Replace(",", "")).PadLeft(14, '0');
                detalle.FechaVto2 = dtVenc2.ToString("yyyyMMdd");
                detalle.ImporteVto2 = (Math.Round(importeTotal, 2).ToString().Replace(",", "")).PadLeft(14, '0');
                detalle.FechaVto3 = dtVenc3.ToString("yyyyMMdd");
                detalle.ImporteVto3 = (Math.Round(importeTotal, 2).ToString().Replace(",", "")).PadLeft(14, '0');
                detalle.MonedaFactura = 0;
                detalle.MotivoRechazo = (string.Empty).PadLeft(3);
                detalle.TipoDocumento = "0080";
                detalle.NroDocumento = det.Documento;
                detalle.NuevoID = (string.Empty).PadRight(22, '0');
                detalle.NuevaCBU = (string.Empty).PadLeft(26, '0'); //formatoCBU(det.FormaPagoCBU, det.FormaPagoTipoCuenta);
                detalle.ImporteMinimo = (string.Empty).PadRight(14, '0');
                detalle.FechaProxVto = (string.Empty).PadLeft(8);
                detalle.IdCuentaAnterior = (string.Empty).PadRight(22, '0');
                detalle.MensajeATM = (string.Empty).PadLeft(40);
                detalle.ConceptoFactura = (string.Empty).PadLeft(10);
                detalle.FechaCobro = (string.Empty).PadLeft(8);
                detalle.ImporteCobrado = (string.Empty).PadRight(14, '0');
                detalle.FechaAcreditamiento = (string.Empty).PadLeft(8);
                detalle.Libre = (string.Empty).PadLeft(26);
                listaDetalle.Add(detalle);
            }
            #endregion

            string path = HttpContext.Current.Server.MapPath("/files/Galicia/texto.txt");
            var engine = new FileHelperEngine<DetalleFH>
            {
                HeaderText = header,
                FooterText = footer
            };
            engine.WriteFile(path, listaDetalle);
            return path;
        }
    }


    [System.Web.Services.WebMethod]
    public static string Exportar(string venc1, string venc2, string venc3, string facturas)
    {

        string fileName = "Galicia";
        string path = "/tmp/";

        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                DateTime dtVenc1 = DateTime.Parse(venc1);
                DateTime dtVenc2 = DateTime.Parse(venc2);
                DateTime dtVenc3 = DateTime.Parse(venc3);

                using (var dbContext = new ACHEEntities())
                {


                    List<FacturasGaliciaViewModel> detalles = new List<FacturasGaliciaViewModel>();
                    string[] arr = facturas.Split(',');
                    var listaFacturas = (List<FacturasGaliciaViewModel>)HttpContext.Current.Session["ListaFacturas"];
                    if (listaFacturas != null)
                    {
                        foreach (var item in arr)
                        {
                            int id = int.Parse(item);
                            var aux = listaFacturas.Where(x => x.IDFactura == id).FirstOrDefault();
                            if (aux != null)
                                detalles.Add(aux);

                            dt = detalles.Select(x => new
                            {
                                Nombre = x.Nombre,
                                Fecha = x.FechaAlta.ToString(),
                                ImporteTotal = x.ImporteTotal,
                                CBU = x.FormaPagoCBU

                            }).ToList().ToDataTable();
                        }
                    }
                    else
                    {
                        foreach (var item in arr)
                        {
                            int id = int.Parse(item);
                            var aux = dbContext.Facturas
                            .Include("Comercios")//.Include("FacturasRetenciones")//.Include("FacturasDetalle")
                            .Where(x => x.IDFactura == id)
                            .Select(x => new FacturasGaliciaViewModel
                            {
                                FechaAlta = DateTime.Parse(x.FechaAlta.ToString("dd/mm/YYYY")),
                                ImporteTotal = x.ImporteTotal,
                                FormaPago = x.Comercios.FormaPago,
                                FormaPagoCBU = x.Comercios.FormaPago_CBU,

                            }).FirstOrDefault();
                            if (aux != null)
                                detalles.Add(aux);

                            dt = detalles.Select(x => new
                            {
                                Nombre = x.Nombre,
                                Importe = x.ImporteTotal,
                                Fecha = x.FechaAlta,
                                FormaPagoCBU = x.FormaPagoCBU,
                            }).ToList().ToDataTable();
                        }
                    }
                    //dt = detalles.ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [System.Web.Services.WebMethod]
    public static string Previsualizar(string venc1, string venc2, string venc3, string facturas)
    {
        string html = "";
        if (HttpContext.Current.Session["CurrentUser"] != null || HttpContext.Current.Session["CurrentMarcasUser"] != null)
        {
            DateTime dtVenc1 = DateTime.Parse(venc1);
            DateTime dtVenc2 = DateTime.Parse(venc2);
            DateTime dtVenc3 = DateTime.Parse(venc3);

            using (var dbContext = new ACHEEntities())
            {

                List<FacturasGaliciaViewModel> detalles = new List<FacturasGaliciaViewModel>();
                string[] arr = facturas.Split(',');
                var listaFacturas = (List<FacturasGaliciaViewModel>)HttpContext.Current.Session["ListaFacturas"];
                try
                {
                    if (listaFacturas != null)
                    {
                        foreach (var item in arr)
                        {
                            int id = int.Parse(item);
                            var factura = listaFacturas.Where(x => x.IDFactura == id).FirstOrDefault();
                            html += "<tr>";
                            html += "<td>" + factura.Nombre + "</td>";
                            html += "<td>" + factura.FechaAlta.ToShortDateString() + "</td>";
                            html += "<td>" + factura.ImporteTotal + "</td>";
                            html += "<td>" + factura.ImporteADebitar + "</td>";

                            html += "<td>" + factura.FormaPagoCBU + "</td>";
                            html += "</tr>";
                        }
                    }
                }
                catch
                {
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
                }
            }
        }
        return html;
    }

    [System.Web.Services.WebMethod]
    public static bool ValidarRetenciones(string facturas)
    {
        using (var dbContext = new ACHEEntities())
        {

            List<int> facturasSinRetenciones = new List<int>();
            string[] arr = facturas.Split(',');
            var listaFacturas = (List<FacturasGaliciaViewModel>)HttpContext.Current.Session["ListaFacturas"];
            if (listaFacturas != null)
            {
                foreach (var item in arr)
                {
                    int id = int.Parse(item);
                    //var aux = dbContext.FacturasRetenciones.Where(x => x.IDFactura == id).Select(x => new {IDFactura =x.IDFactura}).FirstOrDefault();
                    if (!dbContext.FacturasRetenciones.Any(x => x.IDFactura == id))
                        facturasSinRetenciones.Add(id);
                }
                if (facturasSinRetenciones != null)
                {
                    var comerciosConRetenciones = dbContext.Facturas.Include("Comercios").Where(x => x.Comercios.ConRetenciones == true).ToList();
                    if (comerciosConRetenciones != null)
                    {
                        var aux = comerciosConRetenciones.Any(x => facturasSinRetenciones.Contains(x.IDFactura));
                        return aux;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }

}


