﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Importacion.aspx.cs" Inherits="modulos_gestion_Importacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/importacion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li>Importación</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Importación</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <form class="form-horizontal" id="form_misDatos" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <div class="form-group">
                    <label for="ddlTipoArchivo" class="col-lg-2 control-label">Tipo de Archivo</label>

                    <div class="col-lg-10">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlTipoArchivo" Width="200px">
                            <%--<asp:ListItem Value="1" Text="Comercios" />--%>
                            <%--<asp:ListItem Value="2" Text="Socios" />--%>
                            <asp:ListItem Value="3" Text="Transacciones Visa" />
                            <%--<asp:ListItem Value="4" Text="Transacciones Fidely" />--%>
                            <asp:ListItem Value="6" Text="Catalogo privado" />
                            <asp:ListItem Value="5" Text="Premios" />
                            
                        </asp:DropDownList>
                    </div>
                </div>
                <div data-provides="fileupload" class="fileupload fileupload-new">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <label for="fu" class="control-label col-sm-2">Archivo</label>
                            <ajaxToolkit:AsyncFileUpload runat="server" ID="fu" CssClass="form-control" PersistFile="true"
                                ThrobberID="throbber" OnClientUploadComplete="UploadCompleted" OnClientUploadError="UploadError"
                                OnUploadedComplete="subirArchivoComercios" />
                            
                            <asp:Label runat="server" ID="throbber" Style="display: none;">
                                <img alt="" src="../../img/ajax_loader.gif" />
                            </asp:Label>
                        </div>
                    </div>                
                </div>                        
                <div class="col-lg-10" runat="server" id="lnkPremiosPrueba" style="display: none;padding-left: 135px;">                                                                     
                    Descargar Ejemplo CSV <a href="../../files/premios/prueba premios.csv" download>Descargar </a>  
                </div>
            </form>
        </div>
    </div>
</asp:Content>

