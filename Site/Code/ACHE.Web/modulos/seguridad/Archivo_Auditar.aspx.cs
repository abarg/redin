﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.IO;
using FileHelpers;
using System.Collections;

public partial class modulos_seguridad_Archivo_Auditar : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
    }

    [System.Web.Services.WebMethod]
    public static List<string> EscribirArchivos(string desde, string hasta) {
        List<string> result = new List<string>();
        try {
            DateTime fechaDesde = DateTime.Parse(desde + " 00:00:01 am");
            DateTime fechaHasta = DateTime.Parse(hasta + " 23:29:29 pm");

            using (var dbContext = new ACHEEntities()) {
                #region archivo transacciones
                var transacciones = dbContext.TransaccionesAuditar.Where(x => x.FECHA >= fechaDesde && x.FECHA <= fechaHasta).ToList();

                #region detalle
                List<TransaccionFH> listaTransacciones = new List<TransaccionFH>();

                foreach (var item in transacciones) {
                    TransaccionFH transaccion = new TransaccionFH();
                    transaccion.EMPRESA = "02120";//.PadRight(5);
                    transaccion.FPRES = DateTime.Now.ToString("ddMMyy");
                    transaccion.TIPOREG = "01";
                    transaccion.NUMCOM = "22939862".PadLeft(10, '0');
                    transaccion.NUMEST = "22939862".PadLeft(10, '0');//item.NUMEST.PadLeft(10, '0');
                    transaccion.CODOP = "1505";
                    transaccion.TIPOAPLIC = "+";
                    transaccion.LOTE = "1".PadLeft(4, '0');
                    transaccion.CODBCO = "".PadRight(3);
                    transaccion.CODCASA = "".PadRight(3);
                    transaccion.BCOEST = "".PadRight(3);
                    transaccion.CASAEST = "".PadRight(3);
                    transaccion.NUMTAR = item.NUMTAR.PadRight(16, '0');
                    transaccion.FORIG_COMPRA = item.FECHA.ToString("ddMMyy");
                    transaccion.FPAG = item.FPAG.ToString("ddMMyy");
                    transaccion.NUMCOMP = item.NUMCOMP.Trim().PadLeft(8, '0');
                    transaccion.IMPORTE = item.IMPORTE.ToString().Replace(",", "").PadLeft(9, '0');
                    transaccion.SIGNO1 = "+";
                    transaccion.NUMAUT = "".PadLeft(5);
                    transaccion.NUMCUOT = "1".PadLeft(2, '0');
                    transaccion.PLANCUOT = "0".PadLeft(2, '0');
                    transaccion.REC_ACEP = "0";
                    transaccion.RECH_PRINC = "".PadLeft(2);
                    transaccion.RECH_SECUN = "".PadLeft(2);
                    transaccion.IMP_PLAN = item.IMPORTE.ToString().Replace(",", "").PadLeft(9, '0');
                    transaccion.SIGNO2 = "+";
                    transaccion.MCA_PEX = "E";
                    transaccion.NROLIQ = "".PadLeft(8);
                    transaccion.CCO_ORIGEN = "".PadLeft(1);
                    transaccion.CCO_MOTIVO = "".PadLeft(2);
                    transaccion.MONEDA = "A";
                    transaccion.ASTER = "*";
                    transaccion.PROMO_BONIF_USU = item.PROMO_BONIF_USU.ToString().PadLeft(3, '0');
                    transaccion.PROMO_BONIF_EST = "0".PadLeft(3, '0');
                    transaccion.ID_PROMO = "".PadLeft(1);
                    transaccion.DTO_PROMO = "0".PadLeft(7, '0');
                    transaccion.SIGNO_DTO_PROMO = "+";
                    transaccion.ID_IVA_CF = "i";
                    transaccion.DEALER = item.NUMEST.PadLeft(10, '0');
                    transaccion.CUIT_EST = item.CUIT_EST.PadLeft(11, '0');
                    transaccion.FPAGO_AJU_LQE = item.FPAGO_AJU_LQE.ToString("ddMMyy");
                    transaccion.COD_MOTIVO_AJU_LQE = "".PadLeft(4);
                    transaccion.RESERVADO1 = "".PadLeft(9);
                    transaccion.RESERVADO2 = "".PadLeft(12);
                    transaccion.PORCDTO_ARANCEL = "0".PadLeft(4, '0');
                    transaccion.ARANCEL = "0".PadRight(7, '0');
                    transaccion.SIGNO_ARANCEL = "+";
                    transaccion.TNA = "0".PadLeft(5, '0');
                    transaccion.COSTO_FIN = "0".PadLeft(7, '0');
                    transaccion.SIGNO_CF = "+";
                    transaccion.RESERVADO3 = "".PadLeft(6);
                    transaccion.RESERVADO4 = "".PadLeft(1);
                    transaccion.TIPODEPLAN = "A";
                    transaccion.LIBRE = "".PadLeft(8);
                    transaccion.ID_FIN_REGISTRO = "*";

                    listaTransacciones.Add(transaccion);
                }
                #endregion

                string path1 = HttpContext.Current.Server.MapPath("/files/auditar/transacciones.txt");
                var engine = new FileHelperEngine<TransaccionFH> { };
                engine.WriteFile(path1, listaTransacciones);
                result.Add(path1);
                #endregion

                #region archivo total transacciones
                decimal transaccionesTotal = dbContext.Transacciones.Where(x => x.FechaTransaccion >= fechaDesde && x.FechaTransaccion <= fechaHasta && x.Operacion.ToLower() == "canje").Sum(x => x.Importe) ?? 0;

                #region detalle
                List<TransaccionTotalFH> listaTotal = new List<TransaccionTotalFH>();
                TransaccionTotalFH total = new TransaccionTotalFH();
                total.EMPRESA = "02120";//.PadRight(5);
                total.FPRES = DateTime.Now.ToString("ddMMyy");
                total.TIPOREG = "05";
                total.NUMCOM = "22939862".PadLeft(10, '0');
                total.NUMEST = "22939862".PadLeft(10, '0');
                total.CODOP = "1505";
                total.TIPOAPLIC = "+";
                total.LIBRE1 = "".PadLeft(38);
                total.FPAGO = DateTime.Now.ToString("ddMMyy");
                total.LIBRE2 = "".PadLeft(8);
                total.IMPORTE = transaccionesTotal.ToString().Replace(",", "").PadLeft(10);
                total.SIGNO = "+";
                total.LIBRE2 = "".PadLeft(25);
                total.MCA_PEX = "E";
                total.LIBRE4 = "".PadLeft(12);
                total.ASTER = "*";

                listaTotal.Add(total);
                #endregion

                string path2 = HttpContext.Current.Server.MapPath("/files/auditar/transacciones_total.txt");
                var engine2 = new FileHelperEngine<TransaccionTotalFH> { };
                engine2.WriteFile(path2, listaTotal);
                result.Add(path2);
                #endregion
            }
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        return result;
    }
}