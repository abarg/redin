﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UsuariosEdicion.aspx.cs" Inherits="modulos_seguridad_UsuariosEdicion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/seguridad/usuariosEdicion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Configuración</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/seguridad/Usuarios.aspx") %>">Usuarios</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Edición de Usuario</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="u_Nombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="u_Apellido" class="col-lg-2 control-label"><span class="f_req">*</span> Apellido</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="u_Usuario" class="col-lg-2 control-label"><span class="f_req">*</span> Usuario</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="u_Email" class="col-lg-2 control-label"><span class="f_req">*</span> Email</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control required email" MaxLength="128"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="u_Password" class="col-lg-2 control-label"><span class="f_req">*</span> Contraseña</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control required" TextMode="Password" MaxLength="20" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="u_Password" class="col-lg-2 control-label">Activo</label>
                    <div class="col-lg-8">
                        <div class="checkbox">
                            &nbsp;&nbsp;&nbsp;
                            <asp:CheckBox runat="server" ID="chkActivo" Checked="true"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();" >Aceptar</button>
                       <a href="Usuarios.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

                <asp:HiddenField runat="server" value="0" ID="hfIDUsuario" />
            </form>
        </div>
    </div>
</asp:Content>

