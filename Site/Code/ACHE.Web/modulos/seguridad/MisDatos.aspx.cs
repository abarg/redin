﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class MisDatos : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            this.cargarDatosUsuario();
    }

    [WebMethod(true)]
    public static void grabar(string nombre, string apellido, string email, string pwd)
    {
        bUsuario bUSuario = new bUsuario();
        Usuarios oUsuario = (Usuarios)HttpContext.Current.Session["CurrentUser"];

        if (!email.IsValidEmailAddress())
            throw new Exception("Email Incorrecto.");

        oUsuario.Nombre = nombre;
        oUsuario.Apellido = apellido;
        oUsuario.Email = email;
        oUsuario.Pwd = pwd;

        bUSuario.add(oUsuario);
        HttpContext.Current.Session["CurrentUser"] = oUsuario;
    }

    private void cargarDatosUsuario()
    {
        Usuarios oUsuario = CurrentUser;
        this.txtNombre.Text = oUsuario.Nombre;
        this.txtApellido.Text = oUsuario.Apellido;
        this.litUsuario.Text = oUsuario.Usuario;
        this.txtEmail.Text = oUsuario.Email;
        this.txtPassword.Attributes["value"] = oUsuario.Pwd;
    }
}