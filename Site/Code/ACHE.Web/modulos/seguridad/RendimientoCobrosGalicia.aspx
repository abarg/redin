﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RendimientoCobrosGalicia.aspx.cs" Inherits="modulos_seguridad_RendimientoCobrosGalicia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <%--<script type="text/javascript" src="<%= ResolveUrl("~/js/views/importacion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li>Rendimiento Cobro Galicia</li>
            </ul>
        </div>
    </nav>
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Galicia: Rendimiento de cobros</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" runat="server" visible="false"><asp:Literal runat="server" ID="litError"></asp:Literal></div>
            <div class="alert alert-success alert-dismissable" id="divOK" runat="server" visible="false"><asp:Literal runat="server" ID="litOk"></asp:Literal></div>

            <form class="form-horizontal" id="form_misDatos" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <div data-provides="fileupload" class="fileupload fileupload-new">
                    <div class="form-group">
                        <label for="fu" class="control-label col-sm-3">Archivo TXT separado por ";"</label>
                        <div class="col-sm-8">
                            <asp:FileUpload runat="server" ID="flpArchivo" CssClass="form-control" />
                            
                            <%--<ajaxToolkit:AsyncFileUpload runat="server" ID="fu" CssClass="form-control" PersistFile="true"
                                ThrobberID="throbber" OnClientUploadComplete="UploadCompletedTxt" OnClientUploadError="UploadError"
                                OnUploadedComplete="subirArchivoGalicia" />
                            
                            <asp:Label runat="server" ID="throbber" Style="display: none;">
                                <img alt="" src="../../img/ajax_loader.gif" />
                            </asp:Label>--%>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <asp:Button runat="server" ID="btnGenerar" CssClass="btn btn-success" Text="Procesar" OnClick="subirArchivoGalicia" />
                            <%--<button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Procesar</button>--%>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</asp:Content>

