﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class modulos_seguridad_UsuariosEdicion : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["IDUsuario"]))
            {
                this.limpiarControles();
                this.hfIDUsuario.Value = Request.QueryString["IDUsuario"];

                if (!this.hfIDUsuario.Value.Equals("0"))
                    this.cargarDatosUsuario(Convert.ToInt32(Request.QueryString["IDUsuario"]));
            }
        }
    }

    [WebMethod(true)]
    public static void grabar(string IDUsuario, string nombre, string apellido, string usuario, string email, string pwd, bool activo)
    {
        try
        {
            if (!email.IsValidEmailAddress())
            {
                throw new Exception("Email Incorrecto.");
            }

            bUsuario bUSuario = new bUsuario();
            Usuarios oUsuario = null;
            if (string.IsNullOrEmpty(IDUsuario) || IDUsuario.Equals("0"))
            {
                oUsuario = new Usuarios();
                oUsuario.FechaAlta = DateTime.Now;
            }
            else
                oUsuario = bUSuario.getUsuario(Convert.ToInt32(IDUsuario));

            oUsuario.Nombre = nombre;
            oUsuario.Apellido = apellido;
            oUsuario.Usuario = usuario;
            oUsuario.Email = email;
            oUsuario.Pwd = pwd;
            oUsuario.Activo = activo;
            
            bUSuario.add(oUsuario);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    /*
    [WebMethod(true)]
    public static void eliminar(string IDUsuario)
    {
        try
        {
            bUsuario bUsuario = new bUsuario();
            bUsuario.deleteUsuario(Convert.ToInt32(IDUsuario));
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }*/

    private void cargarDatosUsuario(int IDUSuario)
    {
        try
        {
            bUsuario bUSuario = new bUsuario();
            Usuarios oUsuario = bUSuario.getUsuario(IDUSuario);
            if (oUsuario != null)
            {
                this.txtNombre.Text = oUsuario.Nombre;
                this.txtApellido.Text = oUsuario.Apellido;
                this.txtUsuario.Text = oUsuario.Usuario;
                this.txtEmail.Text = oUsuario.Email;
                this.txtPassword.Attributes["value"] = oUsuario.Pwd;
                this.chkActivo.Checked = oUsuario.Activo;
            }
            else
                Response.Redirect("~/error.aspx");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void limpiarControles()
    {
        this.txtNombre.Text = "";
        this.txtApellido.Text = "";
        this.txtUsuario.Text = "";
        this.txtEmail.Text = "";
        this.txtPassword.Text = "";
        this.chkActivo.Checked = true;
    }
}