﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using ACHE.Model;
using ACHE.Business;
using ACHE.Extensions;
using System.Data;
using System.Text;

public partial class modulos_seguridad_RendimientoCobrosGalicia : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void limpiarControles()
    {
        //this.fu.ClearFileFromPersistedStore();
    }

    protected void subirArchivoGalicia(object sender, EventArgs e)
    {
        try
        {
            if (flpArchivo.HasFile && flpArchivo.FileName.ToLower().Contains(".txt"))
            {
                string nombreArchivoRuta = Server.MapPath("ArchivosTemp/") + Path.GetFileName(flpArchivo.FileName);
                flpArchivo.SaveAs(nombreArchivoRuta);

                this.importarArchivo(nombreArchivoRuta, flpArchivo.FileName.Split(".txt")[0]);

                File.Delete(nombreArchivoRuta);
            }
            else
            {
                divOK.Visible = false;
                divError.Visible = true;
                litError.Text = "Debe ingresar un archivo válido";
            }
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw ex;
        }
    }

    private void importarArchivo(string nombreArchivoRuta, string nombreArchivo)
    {
        decimal total = 0;
        int cant = 0;
        int cantAnterior = 0;
        int rechazados = 0;
        StreamReader arch = null;
        string linea;
        try
        {
            arch = new StreamReader(nombreArchivoRuta);
            Usuarios oUsu = (Usuarios)Session["CurrentUser"];

            string[] vlineas;
            int index = 0;

            using (var dbContext = new ACHEEntities())
            {
                while ((linea = arch.ReadLine()) != null)
                {
                    if (index > 0)
                    {
                        try
                        {
                            vlineas = linea.Split(';');
                            string transaccion = vlineas[1].ToString();
                            if (vlineas.Count() == 15)
                            {
                                string importeRechazado = vlineas[10].ToString();
                                if (transaccion.Trim() == "RED BENEFI")
                                {
                                    if ("" == importeRechazado.Trim())
                                    {
                                        try
                                        {
                                            bool existe = false;
                                            Pagos pago = new Pagos();
                                            DateTime dFec = DateTime.ParseExact(vlineas[4], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                            dFec = dFec.AddHours(Convert.ToInt32(vlineas[4].Substring(0, 2)));
                                            dFec = dFec.AddMinutes(Convert.ToInt32(vlineas[4].Substring(2, 2)));
                                            dFec = dFec.AddSeconds(Convert.ToInt32(vlineas[4].Substring(4, 2)));
                                            pago.NroComprobante = "";
                                            pago.SUSS = 0;
                                            pago.RetGanancias = 0;
                                            pago.Redondeo = 0;
                                            pago.Otros = 0;
                                            pago.IIBB = 0;
                                            pago.FormaDePago = "Debito Bancario";
                                            pago.Fecha = dFec;

                                            string aux = vlineas[9].Substring(0, vlineas[9].Length - 2) + "," + vlineas[9].Substring(vlineas[9].Length - 2, 2);

                                            pago.Importe = Convert.ToDecimal(aux);
                                            if (vlineas[6] != "")
                                            {
                                                try
                                                {
                                                    int idFact = Convert.ToInt32(vlineas[6]);
                                                    var fact = dbContext.Facturas.Where(x => x.IDFactura == idFact).FirstOrDefault();
                                                    if (fact != null)
                                                    {
                                                        pago.NroFactura = fact.Numero;
                                                        pago.IDComercio = fact.IDComercio;
                                                        pago.IDProveedor = fact.IDProveedor;

                                                        if (dbContext.Pagos.Any(x => x.NroFactura == fact.Numero && x.IDComercio == fact.IDComercio
                                                            && x.IDProveedor == fact.IDProveedor && x.FormaDePago == "Debito Bancario"))
                                                            existe = true;
                                                    }
                                                }
                                                catch (Exception ex) {
                                                    if (dbContext.Pagos.Any(x => x.Fecha == pago.Fecha && x.Importe == pago.Importe))
                                                        existe = true;
                                                
                                                }
                                            }
                                            else
                                            {
                                                if (dbContext.Pagos.Any(x => x.Fecha == pago.Fecha && x.Importe == pago.Importe))
                                                    existe = true;
                                            }

                                            pago.Observaciones = "Galicia:" + vlineas[11].ToString();
                                            if (!existe)//Sólo lo agrego nuevamente para evitar duplicados
                                            {
                                                dbContext.Pagos.Add(pago);
                                                cant++;
                                                total += pago.Importe;
                                            }
                                            else
                                                cantAnterior++;
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = ex.Message;
                                        }
                                    }
                                    else
                                        rechazados++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                        }
                        //else
                        //    throw new Exception("Formato de archivo incorrecto: La cant de columnas no coincide");
                    }
                    index++;
                }
                dbContext.SaveChanges();
            }

            if (cant == 0 && cantAnterior > 0)
            {
                litError.Text = "Los pagos registrados en este archivo ya se encuentran en el sistema. Por favor, verifiquelos manualmente para evitar inconvenientes.";
                divOK.Visible = false;
                divError.Visible = true;
            }
            else
            {
                litOk.Text = "Los datos se han actualizado correctamente. Importe total: $" + total.ToString("N2") + " - Operaciones: " + cant + " - <b>Rechazados por galicia: " + rechazados + "</b>";
                divOK.Visible = true;
                divError.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            arch.Close();
        }
    }
}