﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Galicia.aspx.cs" Inherits="modulos_seguridad_Galicia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li>Gestión de archivos Galicia</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h1 class="heading" style="border: none;">Gestión de archivos Galicia</h1>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formGeneracionDat" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hfArchivo" ClientIDMode="Static" />
                <div class="formSep" style="border-bottom: 0px">
                    <div id="divPaso1">
                        <h3 class="heading" style="color: #428bca">1. Ingrese fecha de generación</h3>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Fecha Generación Desde</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtDesde" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Fecha Generación Hasta</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtHasta" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tipo Tarjeta</label>
                                <div class="col-lg-3">
                                    <asp:DropDownList runat="server" ID="ddlTipoTarjeta" CssClass="form-control">
                                        <asp:ListItem Text="Todas" Value="" Selected="True" />
                                        <asp:ListItem Text="Redin" Value="REDIN"  />
                                        <asp:ListItem Text="Fidelizacion" Value="FIDELIZACION" />
                                        <asp:ListItem Text="Giftcards" Value="GIFT" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Mostrar retenciones</label>
                                <div class="col-lg-3">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlRetenciones">
                                        <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Con Retencion" Value="Con"></asp:ListItem>
                                        <asp:ListItem Text="Sin Retencion" Value="Sin"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <asp:Button runat="server" ID="btnBuscar" CssClass="btn" OnClick="btnBuscar_Click" Text="Buscar" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div runat="server" id="divNoFacturas"  visible="false">
                        <h3 class="heading" style="color: #fe0101; border: none;">No se han encontrado facturas entre las fechas ingresadas</h3>
                    </div>
                    <div id="divPaso2" runat="server" visible="false">
                        <h3 class="heading" style="color: #428bca">2. Seleccione las facturas</h3>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Facturas</label>
                                <div class="col-lg-9">
                                    <asp:DropDownList runat="server" ID="searchable" multiple="multiple" ClientIDMode="Static"
                                        DataTextField="Nombre" DataValueField="IDFactura">
                                    </asp:DropDownList>

                                    <br />
                                    <a href='#' id='select-all'>seleccionar todos</a>&nbsp;|&nbsp;<a href='#' id='deselect-all'>deseleccionar todos</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button id="btnVencimientos" class="btn" onclick="$('#divPaso3, #divGenerar').show(); return false">Siguiente</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divPaso3" style="display: none;">
                        <h3 class="heading" style="color: #428bca">3. Ingrese fechas de vencimiento</h3>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>1° Vencimiento</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtVencimiento1" ClientIDMode="Static" CssClass="form-control required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>2° Vencimiento</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtVencimiento2" ClientIDMode="Static" CssClass="form-control required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>3° Vencimiento</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtVencimiento3" ClientIDMode="Static" CssClass="form-control required"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div id="divGenerar" style="display: none;">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar</button>
                                    <button runat="server" id="btnVisualizar" class="btn" type="button" onclick="Previsualizar();">Previsualizar</button>
                                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display: none" />
                                    <a href="" id="lnkDownload" download="DAT" style="display: none;">Descargar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="modalDetalle">
		<div class="modal-dialog" <%--style="width: 1400px;right: 200px;"--%>>
			<div class="modal-content" style="width: 800px">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="titDetalle">Previsualización de importes a debitar</h3>
				</div>
				<div class="modal-body">
					<table class="table table-condensed table-striped" data-provides="rowlink">
						<thead id="headDetalle">
							<tr>                              
                                <th>Nombre</th>
                                <th>Fecha</th>
                                <th style="min-width: 100px;">Importe</th>
                                <th>Importe a debitar</th>
                                <th>CBU</th>
							</tr>
						</thead>
						<tbody id="bodyDetalle">							
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoadingExl" style="display:none" />
                    <a href="" id="lnkDownloadExl" download="Detalle" style="display:none">Descargar Excel</a>
					<button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/seguridad/galicia.js?v=1") %>"></script>
    <!-- multiselect -->
	<script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>