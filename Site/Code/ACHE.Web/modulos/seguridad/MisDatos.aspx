﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MisDatos.aspx.cs" Inherits="MisDatos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/misDatos.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Mis Datos</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Mis Datos</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

			<form class="form-horizontal" id="form_misDatos" runat="server">
				<fieldset>
					<div class="form-group">
						<label class="control-label col-sm-2">Usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<strong><asp:Literal runat="server" ID="litUsuario"></asp:Literal></strong>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label for="u_fname" class="control-label col-sm-2"><span class="f_req">*</span> Nombre</label>
						<div class="col-sm-8">
                            
							<asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="100"></asp:TextBox>
						</div>
					</div>
                    <div class="form-group">
						<label for="u_fname" class="control-label col-sm-2"><span class="f_req">*</span> Apellido</label>
						<div class="col-sm-8">
                            <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control required" MaxLength="100"></asp:TextBox>
						</div>
					</div>
					<div class="form-group">
						<label for="u_email" class="control-label col-sm-2"><span class="f_req">*</span> Email</label>
						<div class="col-sm-8">
							<asp:TextBox runat="server" ID="txtEmail" CssClass="form-control required email" MaxLength="128"></asp:TextBox>
						</div>
					</div>
					<div class="form-group">
						<label for="u_password" class="control-label col-sm-2"><span class="f_req">*</span> Contraseña</label>
						<div class="col-sm-8">
							<asp:TextBox runat="server" ID="txtPassword" CssClass="form-control required" TextMode="Password" MaxLength="10" MinLength="3" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-2">
							<button id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
						</div>
					</div>
				</fieldset>
			</form>
        </div>
    </div>      
    
            
</asp:Content>

