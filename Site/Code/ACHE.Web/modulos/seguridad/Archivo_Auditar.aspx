﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Archivo_Auditar.aspx.cs" Inherits="modulos_seguridad_Archivo_Auditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/seguridad/archivo_auditar.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li>Generación de archivos Auditar</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Generación de archivos Auditar</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formGeneracionArchivos" clientidmode="static" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Fecha Transacciones Desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtDesde" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Fecha Transacciones Hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtHasta" ClientIDMode="Static" CssClass="form-control validDate greaterThan required"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button runat="server" id="btnGenerar" class="btn btn-success" type="button" onclick="generar();">Generar archivos</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                        <br />
                        <br />
                        <a id="lnkDownload" download="transacciones_1" style="display: none;">Descargar archivo transacciones</a>
                        <br />
                        <a id="lnkDownload2" download="transacciones_2" style="display: none;">Descargar archivo transacciones total</a>
                    </div>
                </div>
                <asp:HiddenField runat="server"  ID="hfArchivo"/>
            </form>
        </div>
    </div>
</asp:Content>

