﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_empresase : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //this.txtArancel.Text = "3";
            Session["CurrentLogo"] = null;
            //CargarCombos();
            if (!String.IsNullOrEmpty(Request.QueryString["IDEmpresa"]))
            {
                this.hdnID.Value = Request.QueryString["IDEmpresa"];

                if (!this.hdnID.Value.Equals("0"))
                {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDEmpresa"]));
                }
            }
        }
    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Empresas.Where(x => x.IDEmpresa == id).FirstOrDefault();
                if (entity != null)
                {

                    this.txtNombre.Text = entity.Nombre;
                    this.ddlColor.SelectedValue = entity.Color;
                    this.rdbActivo.SelectedValue = entity.Activo == true ? "Si" : "No";

                    if (!string.IsNullOrEmpty(entity.Logo))
                    {
                        lnkLogo.NavigateUrl = "/fileHandler.ashx?type=logos&module=admin&file=" + entity.Logo;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.Logo + "', 'divLogo')");
                        divLogo.Visible = true;

                        imgLogo.ImageUrl = "/files/logos/" + entity.Logo;
                    }
                    else
                        imgLogo.ImageUrl = "http://www.placehold.it/180x120/EFEFEF/AAAAAA";
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int id, string nombre, string color, bool activo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    Empresas entity;
                    if (id > 0)
                        entity = dbContext.Empresas.Where(x => x.IDEmpresa == id).FirstOrDefault();
                    else
                    {
                        entity = new Empresas();
                        entity.FechaAlta = DateTime.Now;
                    }
                    entity.Nombre = nombre != null && nombre != ""? nombre.ToUpper() : "" ;
                    entity.Color = color;
                    entity.Activo = activo;

                    if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                        entity.Logo = HttpContext.Current.Session["CurrentLogo"].ToString();

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Empresas.Add(entity);
                        dbContext.SaveChanges();
                    }

                    return entity.IDEmpresa.ToString();
                }

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";

    }

    [WebMethod(true)]
    public static void eliminarLogo(int id, string archivo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var file = "//files//logos//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Empresas.Where(x => x.IDEmpresa == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Logo = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadLogo(object sender, EventArgs e)
    {
        try
        {
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "empresa_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/logos/"), uniqueName);

                flpLogo.SaveAs(path);

                Session["CurrentLogo"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentLogo"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idEmpresa)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.UsuariosEmpresas
                    .Where(x => x.IDEmpresa == idEmpresa)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null)
                    {

                        dbContext.UsuariosEmpresas.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDEmpresa, int IDUsuario, string usuario, string email, string pwd, string tipo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.UsuariosEmpresas.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosEmpresas.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosEmpresas entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosEmpresas();
                    else
                        entity = dbContext.UsuariosEmpresas.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDEmpresa = IDEmpresa;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosEmpresas.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    #region Comercios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrillaComercios(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idEmpresa)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.EmpresasComercios.Include("Comercios").Include("Domicilios")
                    .Where(x => x.IDEmpresa == idEmpresa)
                    .OrderBy(x => x.Comercios.NombreFantasia)
                    .Select(x => new
                    {
                        ID = x.IDEmpresaComercio,
                        Nombre = x.Comercios.NombreFantasia,
                        NroDocumento = x.Comercios.NroDocumento,
                        Domicilio = x.Comercios.Domicilios.Domicilio
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void DeleteComercio(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.EmpresasComercios.Where(x => x.IDEmpresaComercio == id).FirstOrDefault();
                    if (entity != null)
                    {

                        dbContext.EmpresasComercios.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void agregarComercio(int IDEmpresa, int IDComercio)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.EmpresasComercios.Any(x => x.IDComercio == IDComercio && x.IDEmpresa == IDEmpresa))
                        throw new Exception("Ya se encuentra asignado el comercio seleccionado");

                    EmpresasComercios entity = new EmpresasComercios();
                    entity.IDEmpresa = IDEmpresa;
                    entity.IDComercio = IDComercio;

                    dbContext.EmpresasComercios.Add(entity);
                    dbContext.SaveChanges();

                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion
}