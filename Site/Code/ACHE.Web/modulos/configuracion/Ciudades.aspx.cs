﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;


public partial class modulos_configuracion_Ciudades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            cargarProvincias();
            cargarPaises();
        }

    }

    private void cargarPaises() {
        using (var dbContext = new ACHEEntities()) {
            var paises = dbContext.Paises.OrderBy(x => x.Nombre).Select(x => new { IDPais = x.IDPais, Nombre = x.Nombre.ToUpper() }).ToList();
            if (paises != null) {
                cmbPaises.DataSource = paises;
                cmbPaises.DataTextField = "Nombre";
                cmbPaises.DataValueField = "IDPais";
                cmbPaises.DataBind();
                cmbPaises.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarProvincias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var provincias = dbContext.Provincias.OrderBy(x => x.Nombre).ToList();
            if (provincias != null)
            {
                cmbProvincia.DataSource = provincias;
                cmbProvincia.DataTextField = "Nombre";
                cmbProvincia.DataValueField = "IDProvincia";
                cmbProvincia.DataBind();
                cmbProvincia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Ciudades.Include("Provincias").Include("Paises")
                        .OrderBy(x => x.Nombre)
                        .Select(x => new CiudadesViewModel()
                        {
                            IDCiudad = x.IDCiudad,
                            IDProvincia = x.IDProvincia,
                            Provincia = x.Provincias.Nombre,
                            Pais = x.Provincias.Paises.Nombre.ToUpper(),
                            IDPais = x.Provincias.Paises.IDPais,
                            CP = x.CP,
                            Nombre = x.Nombre

                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var ciudad = dbContext.Ciudades.Where(x => x.IDCiudad == id).FirstOrDefault();
                    if (ciudad != null)
                    {
                        if (dbContext.Domicilios.Any(x => x.Ciudad.HasValue && x.Ciudad.Value == id))
                            throw new Exception("No se puede eliminar ya que la ciudad pertenece a un comercio y/o socio");
                        dbContext.Ciudades.Remove(ciudad);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }


    [WebMethod(true)]
    public static string Exportar(string idProvincia, string nombre, string idPais)
    {
        string fileName = "Ciudades";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.Ciudades.Include("Provincias").OrderBy(x => x.Nombre)
                        .Select(x => new {
                    IDCiudad = x.IDCiudad,
                    Provincia = x.Provincias.Nombre.ToUpper(),
                    IDProvincia = x.IDProvincia,
                    Pais = x.Provincias.Paises.Nombre,
                    IDPais = x.Provincias.IDPais,
                    Nombre = x.Nombre,
                    CP = x.CP
                    }).AsEnumerable();

                    if (!string.IsNullOrEmpty(idPais))
                        if (int.Parse(idPais) > 0)
                            info = info.Where(x => x.IDPais == int.Parse(idPais));
                    
                    if (!string.IsNullOrEmpty(idProvincia))
                        if (int.Parse(idProvincia) > 0)
                            info = info.Where(x => x.IDProvincia == int.Parse(idProvincia));

                    if (!string.IsNullOrEmpty(nombre))
                        info = info.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));

                    dt = info.Select(x => new
                    {
                        IDCiudad = x.IDCiudad,
                        Pais = x.Pais,
                        Provincia = x.Provincia,
                        Nombre = x.Nombre,
                        CP = x.CP

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }


    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais) {
        List<ComboViewModel> listCiudades = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {

            listCiudades = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listCiudades.Insert(0, todas);

        }
        return listCiudades;
    }

}