﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Rubrose.aspx.cs" Inherits="modulos_configuracion_Rubrose" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/rubrose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

    <style type="text/css">
        .bar {
            height: 18px;
            background: green;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/rubros.aspx") %>">Rubros</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-10 col-md-10">
            <h3 class="heading" id="litTitulo">Edición de Empresa</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li id="liSubrubros"><a href="#tabSubrubros" class="hide" data-toggle="tab">Subrubros</a></li>
                </ul>

                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />

                    <div class="tab-content">
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                            <asp:HiddenField runat="server" ID="hdnIDRubro" Value="0" />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="50" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group" id="divUploadLogo" style="display: none">
                                <label class="col-lg-2 control-label">Icono</label>
                                <div class="col-lg-4">
                                    <asp:Image runat="server" ID="imgLogo" Style="width: 48px; height: 48px;"></asp:Image>
                                    <br />
                                    <br />
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                                        ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                                        ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                        OnUploadedComplete="uploadLogo" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                    <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                        <img alt="" src="../../img/ajax_loader.gif" />
                                    </asp:Label>
                                    <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                                </div>
                                <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                                    Actual:
                                    <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="rubros.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabSubrubros">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorSubrubro" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtNombreSubrubro" CssClass="form-control" MaxLength="100" ClientIDMode="Static"></asp:TextBox>
                                        <span class="help-block">Nombre</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarSubrubro" class="btn" type="button" onclick="agregarSubrubro();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div id="grid"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hdnIDSubrubro" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</asp:Content>

