﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
public partial class modulos_configuracion_Dealere : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["IDDealer"]))
            {
                this.limpiarControles();
                this.hfIDDealer.Value = Request.QueryString["IDDealer"];

                if (!this.hfIDDealer.Value.Equals("0"))
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDDealer"]));
            }
        }
    }

    private void cargarDatos(int IDDealer)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var dealer = dbContext.Dealer.Where(x => x.IDDealer == IDDealer).FirstOrDefault();
                if (dealer != null)
                {
                    this.txtNombre.Text = dealer.Nombre;
                    this.txtApellido.Text = dealer.Apellido;
                    this.txtCodigo.Text = dealer.Codigo;
                    this.txtEmail.Text = dealer.Email;
                    this.txtPwd.Attributes["value"]  = dealer.Pwd;
                    this.txtComisionAlta.Text = dealer.ComisionAlta.ToString();
                    this.txtComisionGiftCard.Text = dealer.ComisionGiftCard.ToString();
                    this.txtComisionTarjetas.Text = dealer.ComisionTarjetas.ToString();
                    this.chkActivo.Checked = dealer.Activo;
                }
                else Response.Redirect("~/error.aspx");
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void limpiarControles()
    {
        this.txtNombre.Text = "";
        this.txtApellido.Text = "";
        this.txtCodigo.Text = "";
        this.txtEmail.Text = "";
        this.txtPwd.Text = "";
        this.txtComisionAlta.Text ="";
        this.txtComisionAlta.Text = "";
        this.txtComisionTarjetas.Text = "";
        this.chkActivo.Checked = true;
    }


    [WebMethod(true)]
    public static void grabar(int idDealer, string nombre, string apellido, string pwd, string email, string codigo,string comisionAlta, string comisionTarjetas,
        string comisionGiftCard, bool activo)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                Dealer entity;
                if (email != string.Empty)
                {
                    var aux = dbContext.Dealer.FirstOrDefault(x => x.Email == email && x.IDDealer != idDealer);
                    if (aux != null )
                        throw new Exception("Ya existe un Dealer con el email ingresado");
                }
                if (codigo != string.Empty)
                {
                    var aux = dbContext.Dealer.FirstOrDefault(x => x.Codigo == codigo && x.IDDealer != idDealer);
                    if (aux != null)
                        throw new Exception("Ya existe un Dealer con el codigo ingresado");
                }

                if (idDealer > 0)
                    entity = dbContext.Dealer.FirstOrDefault(x => x.IDDealer == idDealer);
                else
                {
                    entity = new Dealer();
                    entity.FechaAlta = DateTime.Now;
                }
                entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper(): "";
                entity.Apellido = apellido != null && apellido != "" ? apellido.ToUpper(): "";
                entity.Pwd = pwd;
                entity.Email = email;
                entity.Codigo = codigo != null && codigo != "" ? codigo.ToUpper() : ""; ;
                if (comisionAlta != "")
                    entity.ComisionAlta = decimal.Parse(comisionAlta);
                else
                    entity.ComisionAlta = null;

                if (comisionTarjetas != "")
                    entity.ComisionTarjetas = decimal.Parse(comisionTarjetas);
                else
                    entity.ComisionTarjetas = null;

                if (comisionGiftCard != "")
                    entity.ComisionGiftCard = decimal.Parse(comisionGiftCard);
                else
                    entity.ComisionGiftCard = null;

                entity.Activo = activo;
                if (idDealer > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Dealer.Add(entity);
                    dbContext.SaveChanges();
                }
            }

        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
        }
    }


}