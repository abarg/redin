﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using ClosedXML.Excel;
using System.Data;
using System.Web.Services;
using System.IO;



public partial class modulos_configuracion_marcas : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cargarCombos();
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var franquicias = dbContext.Franquicias.ToList();

            Franquicias todas = new Franquicias();
            todas.NombreFantasia = "Todas";
            todas.IDFranquicia = 0;
            franquicias.Insert(0, todas);

            ddlFranquicias.DataSource = franquicias;
            ddlFranquicias.DataValueField = "IDFranquicia";
            ddlFranquicias.DataTextField = "NombreFantasia";
            ddlFranquicias.DataBind();

        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Marcas.Include("Franquicias")
                    .OrderBy(x => x.Nombre)
                    .Select(x => new MarcasViewModel()
                    {
                        IDMarca = x.IDMarca,
                        Nombre = x.Nombre,
                        Affinity = x.Affinity,
                        Prefijo = x.Prefijo,
                        FechaAlta = x.FechaAlta,
                        Arancel = x.POSArancel,
                        MostrarSoloPOSPropios = x.MostrarSoloPOSPropios ? "Si" : "No",
                        MostrarSoloTarjetasPropias = x.MostrarSoloTarjetasPropias ? "Si" : "No",
                        IDFranquicia = x.IDFranquicia,
                        Franquicia = x.Franquicias.NombreFantasia,
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.Tarjetas.Any(x => x.IDMarca == id))
                        throw new Exception("Esta Marca tiene tarjetas creadas. No se puede eliminar");
                    else if (dbContext.Comercios.Any(x => x.IDMarca == id))
                        throw new Exception("Esta Marca tiene comercios asignados. No se puede eliminar");
                    else
                    {
                        var entity = dbContext.Marcas.Where(x => x.IDMarca == id).FirstOrDefault();
                        if (entity != null)
                        {

                            dbContext.Marcas.Remove(entity);
                            dbContext.SaveChanges();

                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    [WebMethod(true)]
    public static string Exportar(string idFranquicia, string nombre)
    {
        string fileName = "Marcas";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.Marcas.Include("Franquicias").Include("Domicilios").OrderBy(x => x.Nombre).AsEnumerable();
                    if (!string.IsNullOrEmpty(idFranquicia))
                        if (int.Parse(idFranquicia) > 0)
                            info = info.Where(x => x.IDFranquicia == int.Parse(idFranquicia));
                    if (!string.IsNullOrEmpty(nombre))
                        info = info.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));

            
                    dt = info.Select(x => new
                    {
                      
                        Franquicia = (x.IDFranquicia.HasValue)? x.Franquicias.NombreFantasia:"",
                        IDMarca = x.IDMarca, 
                        Nombre = x.Nombre,
                        Prefijo = x.Prefijo,  
                        Affinity = x.Affinity,
                        MostrarSoloTarjetasPropias = x.MostrarSoloTarjetasPropias ? "Si" : "No",
                        MostrarSoloPOSPropios = x.MostrarSoloPOSPropios ? "Si" : "No",
                        Arancel = x.POSArancel,
                        FechaAlta = x.FechaAlta,
                        Color=x.Color,
                        POSWeb = x.HabilitarPOSWeb ? "Si" : "No",
                        Catalogo=x.TipoCatalogo,
                        CodigoPremios= x.Codigo,
                        GiftWeb = x.HabilitarGiftcard ? "Si" : "No",
                        CuponInWeb = x.HabilitarCuponIN ? "Si" : "No",
                        AdministrarProductos = x.MostrarProductos ? "Si" : "No",
                        SMSWeb = x.HabilitarSMS ? "Si" : "No",
                     
                        CostoSMS=x.CostoSMS,
                        EmailAlertas=x.EmailAlertas,
                        CelularAltertas=x.CelularAlertas,
                        CelularEmpresa=x.EmpresaCelularAlertas,
                        RazonSocial=x.RazonSocial,
                        CondicionIVA=x.CondicionIva,
                        TipoDoc=x.TipoDocumento,
                        NroDocumento=x.NroDocumento,

                    //    Provincia = x.Domicilios != null ? x.Domicilios.Provincias.Nombre : "",
                      
                //     Ciudad = (x.IDDomicilio != null && x.Domicilios.Ciudad.HasValue) ? x.Domicilios.Ciudades.Nombre : "",
                        Domicilio = x.IDDomicilio != null ? x.Domicilios.Domicilio : "",
                        Piso = x.IDDomicilio != null ? x.Domicilios.PisoDepto : "",
                        CodigoPostal = x.IDDomicilio != null ? x.Domicilios.CodigoPostal : "",
                        Telefono = x.IDDomicilio != null ? x.Domicilios.Telefono : "",
                
                        CostoTransaccional = x.CostoTransaccional ?? 0,
                        CostoSeguro = x.CostoSeguro??0,
                        CostoPLUSIN = x.CostoPlusin ?? 0,
                        CostoSMS2 = x.CostoSMS2 ?? 0,
                
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
           return "";
    }


    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}