﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_franquiciase : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarPaises();

            this.txtFechaAlta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            if (!String.IsNullOrEmpty(Request.QueryString["IDFranquicia"]))
            {
                this.hdnID.Value = Request.QueryString["IDFranquicia"];

                if (!this.hdnID.Value.Equals("0"))
                {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDFranquicia"]));
                }
            }
        }
    }

    private void cargarPaises()
    {
        
        ddlPais.DataSource = Common.LoadPaises();
        ddlPais.DataTextField = "Nombre";
        ddlPais.DataValueField = "ID";
        ddlPais.DataBind();
        ddlPais.Items.Insert(0, new ListItem("", "0"));


    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Franquicias.Where(x => x.IDFranquicia == id).FirstOrDefault();
                if (entity != null)
                {
                    //Datos Principales
                    this.txtNombreFantasia.Text = entity.NombreFantasia;
                    this.txtRazonSocial.Text = entity.RazonSocial;
                    this.txtFechaAlta.Text = entity.FechaAlta.ToString("dd/MM/yyyy");
                    this.ddlTipoDoc.SelectedValue = entity.TipoDocumento;
                    this.txtNroDocumento.Text = entity.NroDocumento;
                    this.txtWeb.Text = entity.Web;
                    this.txtEmail.Text = entity.Email;
                    this.txtObservaciones.Text = entity.Observaciones;
                    this.ddlIVA.SelectedValue = entity.CondicionIva;

                    //Arancel & Comision
                    this.txtArancel.Text = entity.Arancel.ToString();
                    this.txtComisionTpCp.Text = entity.ComisionTpCp.ToString();
                    this.txtComisionTpCt.Text = entity.ComisionTpCt.ToString();
                    this.txtComisionTtCp.Text = entity.ComisionTtCp.ToString();
                    this.txtPubLocal.Text = entity.PublicidadLocal.ToString();
                    this.txtPubNacional.Text = entity.PublicidadNacional.ToString();

                    //Formas Pago
                    if (entity.FormaPago != null)
                    {
                        if (entity.FormaPago.Equals("D"))
                        {
                            this.rdbFormaPago_Debito.Checked = true;
                            this.rdbFormaPago_Tarjeta.Checked = false;

                            this.txtFormaPago_Banco.Text = entity.FormaPago_Banco;
                            if (entity.FormaPago_TipoCuenta != null)
                            {
                                if (entity.FormaPago_TipoCuenta.Equals("1")) this.ddlFormaPago_TipoCuenta.SelectedValue = "1";
                                else if (entity.FormaPago_TipoCuenta.Equals("2")) this.ddlFormaPago_TipoCuenta.SelectedValue = "2";
                            }

                            this.txtFormaPago_NroCuenta.Text = entity.FormaPago_NroCuenta;
                            this.txtFormaPago_CBU.Text = entity.FormaPago_CBU;
                            this.txtFormaPago_CBU_Rep.Text = entity.FormaPago_CBU;
                        }
                        else if (entity.FormaPago.Equals("T"))
                        {
                            this.rdbFormaPago_Debito.Checked = false;
                            this.rdbFormaPago_Tarjeta.Checked = true;

                            this.txtFormaPago_Tarjeta.Text = entity.FormaPago_Tarjeta;
                            this.txtFormaPago_BancoEmisor.Text = entity.FormaPago_BancoEmisor;
                            this.txtFormaPago_NroTarjeta.Text = entity.FormaPago_NroTarjeta;
                            this.txtFormaPago_FechaVto.Text = entity.FormaPago_FechaVto;
                            this.txtFormaPago_CodigoSeg.Text = entity.FormaPago_CodigoSeg;
                        }
                    }

                    //Contacto
                    if (entity.IDContacto != 0)
                    {
                        Contactos oContacto = entity.Contactos;

                        this.txtContacto_Nombre.Text = oContacto.Nombre;
                        this.txtContacto_Apellido.Text = oContacto.Apellido;
                        this.txtContacto_Cargo.Text = oContacto.Cargo;
                        this.txtContacto_Telefono.Text = oContacto.Telefono;
                        this.txtContacto_Celular.Text = oContacto.Celular;
                        this.txtContacto_Email.Text = oContacto.Email;
                        this.txtContacto_Observaciones.Text = oContacto.Observaciones;
                        this.txtContacto_Documento.Text = oContacto.NroDocumento;
                    }

                    //Domicilio Fiscal
                    if (entity.IDDomicilio != 0)
                    {
                        Domicilios oDomicilioF = entity.Domicilios;

                        this.ddlPais.SelectedValue = "1";
                        this.ddlProvincia.SelectedValue = oDomicilioF.Provincia.ToString();
                        ddlCiudad.DataSource = Common.LoadCiudades(oDomicilioF.Provincia);
                        ddlCiudad.DataTextField = "Nombre";
                        ddlCiudad.DataValueField = "ID";
                        ddlCiudad.DataBind();
                        ddlCiudad.Items.Insert(0, new ListItem("", ""));

                        if (oDomicilioF.Ciudad.HasValue)
                            ddlCiudad.SelectedValue = oDomicilioF.Ciudad.Value.ToString();
                        this.txtDomicilio.Text = oDomicilioF.Domicilio;
                        this.txtCodigoPostal.Text = oDomicilioF.CodigoPostal;
                        this.txtTelefonoDom.Text = oDomicilioF.Telefono;
                        this.txtFax.Text = oDomicilioF.Fax;
                        this.txtPisoDepto.Text = oDomicilioF.PisoDepto;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static void grabar(int IDFranquicia, string NombreFantasia, string RazonSocial, string TipoDoc, string NroDocumento
        , string IVA, string Web, string Email
        , string Arancel, string ComisionTpCp, string ComisionTtCp, string ComisionTpCt, string PubLocal, string PubNacional
        , string Observaciones, string FechaAlta
        , string Pais, string Provincia, string Ciudad, string Domicilio, string CodigoPostal, string TelefonoDom, string Fax, string PisoDepto
        , string FormaPago, string FormaPago_Banco, string FormaPago_TipoCuenta, string FormaPago_NroCuenta, string FormaPago_CBU, string FormaPago_Tarjeta, string FormaPago_BancoEmisor, string FormaPago_NroTarjeta
        , string FormaPago_FechaVto, string FormaPago_CodigoSeg
        , string Contacto_Nombre, string Contacto_Apellido, string Contacto_Cargo, string Contacto_Telefono, string Contacto_Celular, string Contacto_Email, string Contacto_Observaciones, string Contacto_Documento)
    {
        try
        {
            Franquicias entity;
            using (var dbContext = new ACHEEntities())
            {
                if (IDFranquicia > 0)
                    entity = dbContext.Franquicias.FirstOrDefault(s => s.IDFranquicia == IDFranquicia);
                else
                {
                    entity = new Franquicias();
                    entity.FechaAlta = DateTime.Now;
                    entity.Estado = "A";
                }

                entity.NombreFantasia = NombreFantasia != null && NombreFantasia != "" ? NombreFantasia.ToUpper() : "";
                entity.RazonSocial = RazonSocial != null && NombreFantasia != "" ? NombreFantasia.ToUpper() : ""; 
                entity.TipoDocumento = TipoDoc;
                entity.NroDocumento = NroDocumento;
                entity.Web = Web;
                entity.Email = Email;
                entity.Observaciones = Observaciones;
                entity.CondicionIva = IVA;

                //Arancel & Comision
                entity.Arancel = decimal.Parse(Arancel);
                entity.ComisionTpCp = decimal.Parse(ComisionTpCp);
                entity.ComisionTpCt = decimal.Parse(ComisionTpCt);
                entity.ComisionTtCp = decimal.Parse(ComisionTtCp);
                entity.PublicidadLocal = decimal.Parse(PubLocal);
                entity.PublicidadNacional = decimal.Parse(PubNacional);

                //Formas de Pago
                if (FormaPago != "")
                {
                    if (FormaPago.Equals("D"))
                    {
                        entity.FormaPago_Banco = FormaPago_Banco != null && FormaPago_Banco != ""? FormaPago_Banco.ToUpper() : "";
                        entity.FormaPago_TipoCuenta = FormaPago_TipoCuenta != null && FormaPago_TipoCuenta != "" ? FormaPago_TipoCuenta.ToUpper() : "";
                        entity.FormaPago_NroCuenta = FormaPago_NroCuenta.ToString();
                        entity.FormaPago_CBU = FormaPago_CBU.ToString();
                    }
                    else if (FormaPago.Equals("T"))
                    {
                        entity.FormaPago_Tarjeta = FormaPago_Tarjeta != null && FormaPago_Tarjeta != "" ? FormaPago_Tarjeta.ToUpper() : "";
                        entity.FormaPago_BancoEmisor = FormaPago_BancoEmisor != null && FormaPago_BancoEmisor != "" ? FormaPago_BancoEmisor.ToUpper() : ""; 
                        entity.FormaPago_NroTarjeta = FormaPago_NroTarjeta.ToString();
                        entity.FormaPago_FechaVto = FormaPago_FechaVto.ToString();
                        entity.FormaPago_CodigoSeg = FormaPago_CodigoSeg.ToString();
                    }
                    entity.FormaPago = FormaPago;
                }

                //Contacto
                if (!entity.IDContacto.HasValue)
                {
                    entity.Contactos = new Contactos();
                    entity.Contactos.FechaAlta = DateTime.Now;
                    entity.Contactos.Estado = "A";
                    entity.Contactos.TipoContacto = "F";
                }
                entity.Contactos.Nombre = Contacto_Nombre != null && Contacto_Nombre != "" ? Contacto_Nombre.ToUpper() : "";
                entity.Contactos.Apellido = Contacto_Apellido != null && Contacto_Apellido != "" ? Contacto_Apellido.ToUpper() : "";
                entity.Contactos.Cargo = Contacto_Cargo != null && Contacto_Cargo != "" ? Contacto_Cargo.ToUpper() : ""; 
                entity.Contactos.Telefono = Contacto_Telefono;
                entity.Contactos.Celular = Contacto_Celular;
                entity.Contactos.Email = Contacto_Email;
                entity.Contactos.Observaciones = Contacto_Observaciones;
                entity.Contactos.NroDocumento = Contacto_Documento;

                //Domicilio
                if (entity.IDDomicilio == 0)
                {
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    entity.Domicilios.Estado = "A";
                    entity.Domicilios.TipoDomicilio = "F";
                }
                entity.Domicilios.Entidad = "F";
                entity.Domicilios.Pais = Pais != null && Pais != "" ? Pais.ToUpper() : "";
                entity.Domicilios.Provincia = int.Parse(Provincia);
                if (Ciudad != string.Empty)
                    entity.Domicilios.Ciudad = int.Parse(Ciudad);
                else
                    entity.Domicilios.Ciudad = null;
                entity.Domicilios.Domicilio = Domicilio != null && Domicilio != "" ? Domicilio.ToUpper() : "";
                entity.Domicilios.CodigoPostal = CodigoPostal;
                entity.Domicilios.Telefono = TelefonoDom;
                entity.Domicilios.Fax = Fax;
                entity.Domicilios.PisoDepto = PisoDepto;

                if (IDFranquicia > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.Franquicias.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #region Usuarios

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idFranquicia)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.UsuariosFranquicias
                    .Where(x => x.IDFranquicia == idFranquicia)
                    .OrderBy(x => x.Usuario)
                    .Select(x => new UsuariosViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        Usuario = x.Usuario,
                        Pwd = x.Pwd,
                        Email = x.Email,
                        Tipo = x.Tipo == "A" ? "Admin" : "Backoffice",
                        Activo = x.Activo ? "Si" : "No"
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.UsuariosFranquicias.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarUsuario(int IDFranquicia, int IDUsuario, string usuario, string email, string pwd, string tipo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.UsuariosFranquicias.Any(x => x.Email.ToLower() == email.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Email ingresado");
                    if (dbContext.UsuariosFranquicias.Any(x => x.Usuario.ToLower() == usuario.ToLower() && x.IDUsuario != IDUsuario))
                        throw new Exception("Ya existe un usuario con el Nombre de Usuario  ingresado");

                    UsuariosFranquicias entity;
                    if (IDUsuario == 0)
                        entity = new UsuariosFranquicias();
                    else
                        entity = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == IDUsuario).FirstOrDefault();

                    entity.IDFranquicia = IDFranquicia;
                    entity.Usuario = usuario;
                    entity.Pwd = pwd;
                    entity.Email = email;
                    entity.Activo = true;
                    entity.Tipo = tipo;

                    if (IDUsuario == 0)
                        dbContext.UsuariosFranquicias.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais)
    {
        List<ComboViewModel> listProvincias = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listProvincias = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
        }
        return listProvincias;
    }
}