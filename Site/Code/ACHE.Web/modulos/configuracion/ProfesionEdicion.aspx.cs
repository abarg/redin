﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_ProfesionEdicion : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
           
            if (!String.IsNullOrEmpty(Request.QueryString["IDProfesion"])) {
                try {
                    int idProfesion = int.Parse(Request.QueryString["IDProfesion"]);
                    if (idProfesion > 0)
                    {
                        this.hdnIDProfesion.Value = idProfesion.ToString();
                        cargarDatosProfesion(idProfesion);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("Profesion.aspx");
                }
            }
        }
    }




    private void cargarDatosProfesion(int idProfesion)
    {
        using (var dbContext = new ACHEEntities()) {
            var profesion = dbContext.Profesiones.Where(x => x.IDProfesion == idProfesion).FirstOrDefault();
            if (profesion != null)
            {
                this.txtNombre.Text = profesion.Nombre;

            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDProfesion, string Nombre)
    {
        try {
            
            Profesiones entity;
            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Profesiones.Where(x => x.Nombre == Nombre && x.IDProfesion != IDProfesion).FirstOrDefault();
                    if ( aux != null)
                        throw new Exception("Ya existe una profesión con el nombre: " + aux.Nombre);

                    if (IDProfesion > 0)
                        entity = dbContext.Profesiones.FirstOrDefault(s => s.IDProfesion == IDProfesion);
                else
                    entity = new Profesiones();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";


                if (IDProfesion > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.Profesiones.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDProfesion.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}

