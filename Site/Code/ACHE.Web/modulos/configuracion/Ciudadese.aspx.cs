﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_Ciudadese : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        //    cargarProvincias();
            cargarPaises();

            if (!String.IsNullOrEmpty(Request.QueryString["IDCiudad"]))
            {
                try
                {
                    int idCiudad = int.Parse(Request.QueryString["IDCiudad"]);
                    if (idCiudad > 0)
                    {
                        this.hdnIDCiudad.Value = idCiudad.ToString();
                        cargarDatosCiudad(idCiudad);
                        this.cmbPaises.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("Ciudades.aspx");
                }
            }
        }

    }

    private void cargarDatosCiudad(int idCiudad)
    {
        using (var dbContext = new ACHEEntities())
        {
            var ciudad = dbContext.Ciudades.Include("Provincias").Where(x => x.IDCiudad == idCiudad).FirstOrDefault();
            if (ciudad != null)
            {
                this.txtNombreCiudad.Text = ciudad.Nombre;
          //      this.cmbProvincia.SelectedValue = ciudad.IDProvincia.ToString();
                this.txtCP.Text = ciudad.CP.ToString();
                this.cmbPaises.SelectedValue = ciudad.Provincias.Paises.IDPais.ToString();
                this.hdnIDProvincia.Value = ciudad.IDProvincia.ToString();
            }
        }
    }

    private void cargarPaises() {
        using (var dbContext = new ACHEEntities()) {
            var paises = dbContext.Paises.Include("Provincias").Where(x => x.Provincias.FirstOrDefault() != null).OrderBy(x => x.Nombre).Select(x => new { IDPais = x.IDPais, Nombre = x.Nombre.ToUpper() }).ToList();
            if (paises != null) {
                cmbPaises.DataSource = paises;
                cmbPaises.DataTextField = "Nombre";
                cmbPaises.DataValueField = "IDPais";
                cmbPaises.DataBind();
                cmbPaises.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void cargarProvincias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var provincias = dbContext.Provincias.OrderBy(x => x.Nombre).ToList();
            if (provincias != null)
            {
                cmbProvincia.DataSource = provincias;
                cmbProvincia.DataTextField = "Nombre";
                cmbProvincia.DataValueField = "IDProvincia";
                cmbProvincia.DataBind();
                cmbProvincia.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int idCiudad, string Nombre, int idProvincia, int CP)
    {
        try
        {
            //Ciudad
            Ciudades entity;
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia && x.Nombre == Nombre && x.IDCiudad != idCiudad && x.CP != CP).FirstOrDefault();
                if (aux != null)
                    throw new Exception("Ya existe una ciudad perteneciente a la provincia " + aux.Provincias.Nombre + " llamada " + aux.Nombre);

                if (idCiudad > 0)
                    entity = dbContext.Ciudades.FirstOrDefault(s => s.IDCiudad == idCiudad);
                else
                    entity = new Ciudades();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";
                entity.CP = CP;
                entity.IDProvincia = idProvincia;

                if (idCiudad > 0)
                    dbContext.SaveChanges();
                else
                {
                    var newId = dbContext.Ciudades.Max(x => x.IDCiudad) + 1;

                    entity.IDCiudad = newId;

                    dbContext.Ciudades.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDCiudad.ToString();
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> provinciasByPaises(int idPais) {
        List<ComboViewModel> listCiudades = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities()) {

            listCiudades = dbContext.Provincias.Where(x => x.IDPais == idPais).Select(x => new ComboViewModel { ID = x.IDProvincia.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listCiudades.Insert(0, todas);

        }
        return listCiudades;
    }
}