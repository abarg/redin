﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model.ViewModels;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ACHE.Model;

public partial class modulos_configuracion_Dealer : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                return dbContext.Dealer.OrderBy(x => x.FechaAlta)
                        .Select(x => new DealerViewModel()
                        {
                            IDDealer = x.IDDealer,
                            FechaAlta = x.FechaAlta,
                            Nombre = x.Nombre,
                            Apellido = x.Apellido,
                            Pwd = x.Pwd,
                            Codigo = x.Codigo,
                            Email = x.Email,
                            ComisionAlta = x.ComisionAlta ?? 0,
                            ComisionGiftCard = x.ComisionGiftCard ?? 0,
                            ComisionTarjetas = x.ComisionTarjetas ?? 0,
                            Activo = x.Activo ? "Si" : "No"
                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var dealer = dbContext.Dealer.Where(x => x.IDDealer == id).FirstOrDefault();

                 
                    if (dealer != null)
                    {
                        if (dbContext.Comercios.Any(x => x.IDDealer == id))
                            throw new Exception("No se puede eliminar el dealer. Esta asignado a un comercio");
                        dbContext.Dealer.Remove(dealer);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }
}