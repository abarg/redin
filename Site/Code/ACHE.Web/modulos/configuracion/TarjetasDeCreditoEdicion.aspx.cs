﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_TarjetasDeCreditoEdicion : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

            if (!String.IsNullOrEmpty(Request.QueryString["IDTarjetasDeCredito"]))
            {
                try {
                    int idTarjetaDeCredito = int.Parse(Request.QueryString["IDTarjetasDeCredito"]);
                    if (idTarjetaDeCredito > 0)
                    {
                        this.hdnIDTarjetaDeCredito.Value = idTarjetaDeCredito.ToString();
                        cargarDatosTarjetaDeCredito(idTarjetaDeCredito);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("TarjetasDeCredito.aspx");
                }
            }
        }
    }




    private void cargarDatosTarjetaDeCredito(int idTarjetaDeCredito)
    {
        using (var dbContext = new ACHEEntities()) {
            var tarjetasDeCredito = dbContext.TarjetasDeCredito.Where(x => x.IDTarjetasDeCredito == idTarjetaDeCredito).FirstOrDefault();
            if (tarjetasDeCredito != null)
            {
                this.txtNombre.Text = tarjetasDeCredito.Nombre;

            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDTarjetasDeCredito, string Nombre)
    {
        try {
            
            TarjetasDeCredito entity;
            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.TarjetasDeCredito.Where(x => x.Nombre == Nombre && x.IDTarjetasDeCredito != IDTarjetasDeCredito).FirstOrDefault();
                    if ( aux != null)
                        throw new Exception("Ya existe una tarjeta de crédito con el nombre: " + aux.Nombre);

                    if (IDTarjetasDeCredito > 0)
                        entity = dbContext.TarjetasDeCredito.FirstOrDefault(s => s.IDTarjetasDeCredito == IDTarjetasDeCredito);
                else
                        entity = new TarjetasDeCredito();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";


                if (IDTarjetasDeCredito > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.TarjetasDeCredito.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDTarjetasDeCredito.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}

