﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Empresase.aspx.cs" Inherits="modulos_configuracion_empresase" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/empresase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.iframe-transport.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.fileupload.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.ui.widget.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <style type="text/css">
        .bar {
            height: 18px;
            background: green;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/empresas.aspx") %>">Empresas</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <!--<div id="dialog" style="display:none;">
        <iframe id="frameTarjetasSus" src="" width="100%" height="100%"></iframe>
     </div>-->

    <div class="row">
        <div class="col-sm-10 col-md-10">
            <h3 class="heading" id="litTitulo">Edición de Empresa</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabUsuarios" class="hide" data-toggle="tab">Usuarios</a></li>
                    <li><a href="#tabComercios" class="hide" data-toggle="tab">Comercios</a></li>
                </ul>

                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />

                    <div class="tab-content">
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                            <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Color</label>
                                <div class="col-lg-3">
                                    <asp:DropDownList runat="server" ID="ddlColor" CssClass="form-control">
                                        <asp:ListItem Text="Azul" Value="blue" Selected="True" />
                                        <asp:ListItem Text="Marron" Value="brown" />
                                        <asp:ListItem Text="Negro" Value="dark" />
                                        <asp:ListItem Text="Rojo" Value="tamarillo" />
                                        <asp:ListItem Text="Verde" Value="green" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Color</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtColor" CssClass="form-control required" MaxLength="10" />
                                </div>
                            </div>--%>
                            <%--<div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Logo</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtAffinity" CssClass="form-control required" MaxLength="4" />
                                </div>
                            </div>--%>
                            <div class="form-group" id="divUploadLogo" style="display: none">
                                <label class="col-lg-2 control-label">Logo</label>
                                <div class="col-lg-4">
                                    <asp:Image runat="server" ID="imgLogo" Style="width: 180px; height: 120px;"></asp:Image>
                                    <br />
                                    <br />
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                                        ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                                        ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                        OnUploadedComplete="uploadLogo" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                    <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                        <img alt="" src="../../img/ajax_loader.gif" />
                                    </asp:Label>
                                    <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                                </div>
                                <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                                    Actual:
                                    <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group" style="display:none">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Activo</label>
                                <div class="col-lg-2">
                                    <asp:RadioButtonList runat="server" ID="rdbActivo">
                                        <asp:ListItem Value="Si" Text="Si" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="empresas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabUsuarios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorUsuario" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Usuario</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Email</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MinLength="3" MaxLength="10"></asp:TextBox>
                                        <span class="help-block">Contraseña</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:DropDownList runat="server" ID="ddlTipoUsuario" CssClass="form-control">
                                            <asp:ListItem Text="Admin" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Backoffice" Value="B"></asp:ListItem>
                                            <%--<asp:ListItem Text="Dataentry" Value="D"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                        <span class="help-block">Tipo</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarUsuario" class="btn" type="button" onclick="agregarUsuario();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div id="grid"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hfIDUsuario" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabComercios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorComercio" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-9">
                                        <asp:DropDownList runat="server" ID="ddlComercio" CssClass="form-control chzn_b" data-placeholder="Seleccione un comercio" Style="width: 100% important!">
                                        </asp:DropDownList>
                                        <span class="help-block">Comercio</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarComercio" class="btn" type="button" onclick="agregarComercio();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div id="gridComercios"></div>
                                    <br />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</asp:Content>

