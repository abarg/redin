﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Marcase.aspx.cs" Inherits="modulos_configuracion_marcase" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.jqEasyCharCounter.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/marcase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/redactor.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/js/redactor.es.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/css/redactor.css") %>" />
    <%--<script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.iframe-transport.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.fileupload.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/fileupload/jquery.ui.widget.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
    <style type="text/css">
        .bar {
            height: 18px;
            background: green;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/marcas.aspx") %>">Marcas</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <!--<div id="dialog" style="display:none;">
        <iframe id="frameTarjetasSus" src="" width="100%" height="100%"></iframe>
     </div>-->

    <div class="row">
        <div class="col-sm-10 col-md-10">
            <h3 class="heading" id="litTitulo">Edición de Marca</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabUsuarios" class="hide" data-toggle="tab">Usuarios</a></li>
                    <li><a href="#tabComercios" class="hide" data-toggle="tab">Comercios</a></li>
                    <li><a href="#tabSMS" class="active" data-toggle="tab">SMS</a></li>
                    <li><a href="#tabEmail" class="active" data-toggle="tab">Email</a></li>
                    <li><a href="#tabAlertas" data-toggle="tab">Alertas</a></li>
                    <li><a href="#tabFacturacion" data-toggle="tab">Datos Facturación</a></li>
                    <li><a href="#tabCostos" data-toggle="tab">Costos</a></li>
                </ul>

                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />

                    <div class="tab-content">
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                            <asp:HiddenField runat="server" ID="hdnID" Value="0" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hdnIDComercioSMS" ClientIDMode="Static" Value="0" />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Prefijo</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPrefijo" CssClass="form-control required" MaxLength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Affinity</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtAffinity" CssClass="form-control required" MaxLength="4" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Arancel %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtArancel" CssClass="form-control required number" MaxLength="1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Franquicia</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control required" ID="ddlFranquicias">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Mostrar</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkMostrarSoloTarjetasPropias" Text="Sólo tarjetas propias" />
                                    <asp:CheckBox runat="server" ID="chkMostrarSoloPOSPropios" Text="Sólo POS propios" />
                                </div>
                            </div>
                            <div class="form-group" id="divUploadLogo" style="display: none">
                                <label class="col-lg-2 control-label">Logo</label>
                                <div class="col-lg-4">
                                    <asp:Image runat="server" ID="imgLogo" Style="width: 180px; height: 120px;"></asp:Image>
                                    <br />
                                    <br />
                                    <ajaxToolkit:AsyncFileUpload runat="server" ID="flpLogo" CssClass="form-control" PersistFile="true"
                                        ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                                        ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                                        OnUploadedComplete="uploadLogo" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                                    <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                                        <img alt="" src="../../img/ajax_loader.gif" />
                                    </asp:Label>
                                    <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                                </div>
                                <div class="col-sm-4" runat="server" id="divLogo" visible="false">
                                    Actual:
                                    <asp:HyperLink runat="server" ID="lnkLogo" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkLogoDelete" Target="_blank">Eliminar</asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Color</label>
                                <div class="col-lg-3">
                                    <asp:DropDownList runat="server" ID="ddlColor" CssClass="form-control">
                                        <asp:ListItem Text="Azul" Value="blue" Selected="True" />
                                        <asp:ListItem Text="Marron" Value="brown" />
                                        <asp:ListItem Text="Negro" Value="dark" />
                                        <asp:ListItem Text="Rojo" Value="tamarillo" />
                                        <asp:ListItem Text="Verde" Value="green" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">POS Web</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkPosWeb" Text="Habilitado" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Catálogo</label>
                                <div class="col-lg-3">
                                    <asp:DropDownList runat="server" ID="ddlCatalogo" CssClass="form-control">
                                        <asp:ListItem Text="Publico" Value="A" Selected="True" />
                                        <asp:ListItem Text="Privado" Value="C" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Código premios</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control required" MinLength="3" MaxLength="3" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Giftcard Web</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkGiftcardWeb" Text="Habilitado" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">CuponIN Web</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkCuponIN" Text="Habilitado" />
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label">Administrar productos</label>
                                <div class="checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkProductos" Text="Habilitado" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Fecha Tope Canje</label>
                                <div class="col-lg-2" >
                                <asp:TextBox runat="server" ID="txtFechaTopeCanje" CssClass="form-control validDate greaterThan" MaxLength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Fecha Caducidad</label>
                                <div class="col-lg-2">
                                <asp:TextBox runat="server" ID="txtFechaCaducidad" CssClass="form-control validDate greaterThan" MaxLength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Cuit Emisor</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCuitEmisor" CssClass="form-control required" MaxLength="20" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="marcas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabUsuarios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorUsuario" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Usuario</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Email</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MinLength="3" MaxLength="10"></asp:TextBox>
                                        <span class="help-block">Contraseña</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:DropDownList runat="server" ID="ddlTipoUsuario" CssClass="form-control">
                                            <asp:ListItem Text="Admin" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Backoffice" Value="B"></asp:ListItem>
                                            <asp:ListItem Text="Dataentry" Value="D"></asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block">Tipo</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarUsuario" class="btn" type="button" onclick="agregarUsuario();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="grid"></div>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:HiddenField runat="server" ID="hfIDUsuario" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabComercios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorComercio" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-9">
                                        <asp:DropDownList runat="server" ID="ddlComercio" CssClass="form-control chzn_b" data-placeholder="Seleccione un comercio" Style="width: 100% !important">
                                        </asp:DropDownList>
                                        <span class="help-block">Comercio</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarComercio" class="btn" type="button" onclick="agregarComercio();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="gridComercios"></div>
                                    <br />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabSMS">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">SMS Web</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkSMS" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensaje('S');" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCostoSMS" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                    <span class="help-block">Costo</span>
                                </div>
                            </div>
                            <div class="form-group sms">
                                <label class="col-lg-2 control-label">SMS Bienvenida</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkSMSBienvenida" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensaje('B');" />
                                </div>
                            </div>
                            <div class="form-group" id="divBienvenida" style="display: none;">
                                <label class="col-lg-2 control-label">Mensaje Bienvenida</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtSMSBienvenida" ClientIDMode="Static" CssClass="form-control alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group sms">
                                <label class="col-lg-2 control-label">SMS Cumpleaños</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkSMSCumpleanios" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensaje('C');" />
                                </div>
                            </div>
                            <div class="form-group" id="divCumpleanios" style="display: none;">
                                <label class="col-lg-2 control-label">Mensaje Cumpleaños</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtSMSCumpleanios" ClientIDMode="Static" CssClass="form-control alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                    <br />Palabras clave: XNOMBREX, XAPELLIDOX
                                </div>
                            </div>
                            <div class="form-group sms">
                                <label class="col-lg-2 control-label">Comercio facturante</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" ID="cmbComercioSMS" ClientIDMode="Static" CssClass="form-control chzn_b" data-placeholder="Seleccione un comercio" Style="width: 100% !important" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="btnGrabar2" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="marcas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabEmail">
                            <br />
                           <div class="form-group">
                                <label class="col-lg-2 control-label">Email Bienvenida a socio</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkEmailRegistroASocio" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensajeEmail('BS');" />
                                </div>
                            </div>
                            <div class="form-group" id="divBienvenidaASocio" style="display: none;">
                                <label class="col-lg-2 control-label">Mensaje Bienvenida</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmailBienvenidaASocio" ClientIDMode="Static" CssClass="form-control htmlEditor" TextMode="MultiLine" Rows="10"></asp:TextBox>
                                    <br />Palabras clave: XNOMBREX, XAPELLIDOX
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email Cumpleaños</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkEmailCumpleanios" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensajeEmail('C');" />

                                </div>
                            </div>
                            <div class="form-group" id="divEmailCumpleanios" style="display: none;">
                                <label class="col-lg-2 control-label">Mensaje Cumpleaños</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmailCumpleanios" ClientIDMode="Static" CssClass="form-control htmlEditor" TextMode="MultiLine" Rows="10"></asp:TextBox>
                                    <br />Palabras clave: XNOMBREX, XAPELLIDOX
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email Bienvenida a comercio</label>
                                <div class="col-lg-1 checkbox-inline" style="margin-left: 15px">
                                    <asp:CheckBox runat="server" ID="chkEmailAComercio" ClientIDMode="Static" Text="Habilitado" onclick="mostrarMensajeEmail('BC');" />
                                </div>
                            </div>
                            <div class="form-group" id="divBienvenidaComercio" style="display: none;">
                                <label class="col-lg-2 control-label">Mensaje Bienvenida</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmailAComercio" ClientIDMode="Static" CssClass="form-control htmlEditor" TextMode="MultiLine" Rows="10"></asp:TextBox>
                                    <br />Palabras clave: XNOMBREX
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="Button1" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="marcas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabAlertas">
                            <br />
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Email </label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:TextBox runat="server" ID="txtEmailAlerta" CssClass="form-control email" ></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Celular </label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:TextBox runat="server" ID="txtCelularAlerta" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Celular empresa </label>
                                <div class="col-lg-2">
                                    <asp:DropDownList runat="server" ID="txtCelularEmpresaAlerta" CssClass="form-control">
                                        <asp:ListItem Text="" Value="" />
                                        <asp:ListItem Text="Claro" Value="Claro" />
                                        <asp:ListItem Text="Movistar" Value="Movistar" />
                                        <asp:ListItem Text="Nextel" Value="Nextel" />
                                        <asp:ListItem Text="Personal" Value="Personal" />
                                        <asp:ListItem Text="Otro" Value="Otro" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="Button2" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="marcas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                      </div>

                        <div class="tab-pane" id="tabFacturacion">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Razón Social</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ddlIVA" class="col-lg-2 control-label">Condición IVA</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlIVA">
                                        <asp:ListItem Value="" Text="" />
                                        <asp:ListItem Value="RI" Text="Resp. Inscripto" />
                                        <asp:ListItem Value="MONOTRIBUTISTA" Text="Monotributista" />
                                        <asp:ListItem Value="EX" Text="Exento/No Resp." />
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="ddlTipoDoc" class="col-lg-2 control-label">Tipo y Nro Doc.</label>
                                <div class="col-sm-2 col-md-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTipoDoc">
                                        <asp:ListItem Value="" Text="" />
                                        <asp:ListItem Value="CUIT" Text="CUIT" />
                                        <asp:ListItem Value="DNI" Text="DNI" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNroDoc" CssClass="form-control number" MaxLength="20"></asp:TextBox>
                                    <span class="help-block">Si no existe, poner 00</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlProvincia" onchange="LoadCiudades(this.value,'ddlCiudad');">
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                        data-placeholder="Seleccione una ciudad">
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button runat="server" id="Button3" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="marcas.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tabCostos">
                            <br />
                            <div class="form-group">
                                <label for="txtCostoTransaccional" class="col-lg-2 control-label">Costo transaccional</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCostoTransaccional" CssClass="form-control" MaxLength="100"></asp:TextBox>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtCostoSeguro" class="col-lg-2 control-label">Costo de seguro</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCostoSeguro" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtCostoPlusin" class="col-lg-2 control-label">Costo de PLUSIN</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCostoPlusin" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="txtCostoSMS2" class="col-lg-2 control-label">Costo de SMS</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtCostoSMS2" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                           <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                                    <button runat="server" id="Button4" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                    <a href="Socios.aspx" class="btn btn-link">Cancelar</a>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</asp:Content>

