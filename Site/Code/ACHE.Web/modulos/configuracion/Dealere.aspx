﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dealere.aspx.cs" Inherits="modulos_configuracion_Dealere" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/dealere.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Dealer</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

      <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición Dealer</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                 <div class="form-group">
                    <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Apellido</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtPwd" class="col-lg-2 control-label"><span class="f_req">*</span> Contraseña</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control required small" TextMode="Password" MaxLength="20" />
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtEmail" class="col-lg-2 control-label"><span class="f_req">*</span> Email</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control required email small" MaxLength="128"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="txtCodigo" class="col-lg-2 control-label"><span class="f_req">*</span> Código</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                        <label for="txtComisionAlta" class="col-lg-2 control-label">Comisión por alta</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtComisionAlta" CssClass="form-control" MaxLength="100" Value="0"></asp:TextBox>

                        </div>
                    </div>
                 <div class="form-group">
                        <label for="txtComisionTarjetas" class="col-lg-2 control-label">Comisión tarjetas</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtComisionTarjetas" CssClass="form-control" MaxLength="100"  Value="0"></asp:TextBox>
                        </div>
                </div>
                        
                 <div class="form-group">
                        <label for="txtComisionGiftCard" class="col-lg-2 control-label">Comisión gift card</label>
                        <div class="col-lg-4">
                            <asp:TextBox runat="server" ID="txtComisionGiftCard" CssClass="form-control" MaxLength="100"  Value="0"></asp:TextBox>
                        </div>
                    </div>


                <div class="form-group">
                    <label for="chkActivo" class="col-lg-2 control-label">Activo</label>
                    <div class="col-lg-4">
                        <asp:CheckBox runat="server" ID="chkActivo" Checked="true"/>
                    </div>
                </div>

                 <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();" >Aceptar</button>
                       <a href="Dealer.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

                <asp:HiddenField runat="server" value="0" ID="hfIDDealer" />
            </form>
        </div>
    </div>
  
</asp:Content>

