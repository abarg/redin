﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Ciudadese.aspx.cs" Inherits="modulos_configuracion_Ciudadese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/ciudadese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <style type="text/css">
        #map-canvas {
            height: 280px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Ciudades.aspx") %>">Ciudades</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Ciudades</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formCiudad" class="form-horizontal" role="form">
                <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
                <br />
                <div class="form-group">
                    <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbPaises" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtApellido" class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbProvincia" ClientIDMode="Static" CssClass="form-control required"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNombreCiudad" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtCP" class="col-lg-2 control-label"><span class="f_req">*</span> CP</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtCP" CssClass="form-control required small" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdnIDProvincia" Value="0" />

                <asp:HiddenField runat="server" ID="hdnIDCiudad" Value="0" />
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a href="Ciudades.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

