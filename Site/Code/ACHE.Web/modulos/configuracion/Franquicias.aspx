﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Franquicias.aspx.cs" Inherits="modulos_configuracion_franquicias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/franquicias.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Configuración</a></li>
            <li class="last">Franquicias</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de franquicias</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form>
			    <div class="formSep col-sm-8 col-md-8">
				    <div class="row">
                        <div class="col-sm-4">
                            <label>Nombre Fantasía</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-sm-4">
                            <label>Razón social</label>
                            <input type="text" id="txtRazonSocial" value="" maxlength="128" class="form-control" />
                        </div>
                        <div class="col-sm-3">
                            <label>Nro. de Documento</label>
                            <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>

