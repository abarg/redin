﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Configuration;

public partial class modulos_configuracion_rubros : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Rubros
                    .Where(x => x.IDRubroPadre == null) //Que no sea subrubro
                    .OrderBy(x => x.Nombre)
                    .Select(x => new RubrosViewModel()
                    {
                        IDRubro = x.IDRubro,
                        Nombre = x.Nombre,
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Rubros.Where(x => x.IDRubro == id).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Rubros.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}