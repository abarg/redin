﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;


public partial class modulos_configuracion_Provincias : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            cargarPaises();
    }


    private void cargarPaises() {
        using (var dbContext = new ACHEEntities()) {
            var paises = dbContext.Paises.OrderBy(x => x.Nombre).Select(x => new { IDPais = x.IDPais, Nombre = x.Nombre.ToUpper() }).ToList();
            if (paises != null) {
                cmbPaises.DataSource = paises;
                cmbPaises.DataTextField = "Nombre";
                cmbPaises.DataValueField = "IDPais";
                cmbPaises.DataBind();
                cmbPaises.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.Provincias.Include("Paises")
                        .OrderBy(x => x.Paises.Nombre)
                        .Select(x => new  {
                            IDProvincia = x.IDProvincia,
                            Provincia = x.Nombre,
                            Pais = x.Paises.Nombre.ToUpper(),
                            IDPais = x.IDPais
                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                try {
                    var provincia = dbContext.Provincias.Where(x => x.IDProvincia == id).FirstOrDefault();
                    if (provincia != null) {
                        if (dbContext.Ciudades.Any(x => x.IDProvincia == id))
                            throw new Exception("No se puede eliminar ya que la provincia tiene una ciudad asignada");
                        dbContext.Provincias.Remove(provincia);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e) {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string idPais, string provincia) {
        string fileName = "Provincias";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Provincias.Include("Paises").OrderBy(x => x.Nombre).AsEnumerable();
                    if (!string.IsNullOrEmpty(idPais))
                        if (int.Parse(idPais) > 0)
                            info = info.Where(x => x.IDPais == int.Parse(idPais));
                    if (!string.IsNullOrEmpty(provincia))
                        info = info.Where(x => x.Nombre.ToLower().Contains(provincia.ToLower()));

                    dt = info.Select(x => new {
                        Pais = x.Paises.Nombre,
                        Provincia = x.Nombre

                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }




    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}