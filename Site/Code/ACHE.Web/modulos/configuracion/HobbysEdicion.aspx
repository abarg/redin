﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HobbysEdicion.aspx.cs" Inherits="modulos_configuracion_HobbysEdicion" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/Hobbys.aspx") %>">Hobbies</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Hobbies</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" style="display: none">Los datos se han actualizado correctamente.</div>

            <form runat="server" id="formHobby" class="form-horizontal" role="form">
                <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
                <br />
               

                <div class="form-group">
                    <label for="txtNombre" class="col-lg-2 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required small" MaxLength="100" />
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIDHobby" Value="0" />
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2" id="divBotones">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                        <a href="Hobbys.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FooterContent" runat="Server">
  
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/hobbysEdicion.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
</asp:Content>