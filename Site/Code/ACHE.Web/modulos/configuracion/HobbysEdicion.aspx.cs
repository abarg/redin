﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_HobbysEdicion : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

            if (!String.IsNullOrEmpty(Request.QueryString["IDHobbie"]))
            {
                try {
                    int idHobby = int.Parse(Request.QueryString["IDHobbie"]);
                    if (idHobby > 0)
                    {
                        this.hdnIDHobby.Value = idHobby.ToString();
                        cargarDatosHobby(idHobby);
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("Hobbys.aspx");
                }
            }
        }
    }




    private void cargarDatosHobby(int idHobby)
    {
        using (var dbContext = new ACHEEntities()) {
            var hobby = dbContext.Hobbies.Where(x => x.IDHobbie == idHobby).FirstOrDefault();
            if (hobby != null)
            {
                this.txtNombre.Text = hobby.Nombre;

            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int IDHobbie, string Nombre)
    {
        try {
            
            Hobbies entity;
            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Hobbies.Where(x => x.Nombre == Nombre && x.IDHobbie != IDHobbie).FirstOrDefault();
                    if ( aux != null)
                        throw new Exception("Ya existe un hobbie con el nombre: " + aux.Nombre);

                    if (IDHobbie > 0)
                        entity = dbContext.Hobbies.FirstOrDefault(s => s.IDHobbie == IDHobbie);
                else
                    entity = new Hobbies();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";


                if (IDHobbie > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.Hobbies.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDHobbie.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}

