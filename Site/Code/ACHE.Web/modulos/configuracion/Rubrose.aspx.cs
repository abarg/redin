﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_Rubrose : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CurrentLogo"] = null;

            if (!String.IsNullOrEmpty(Request.QueryString["IDRubro"]))
            {
                this.hdnIDRubro.Value = Request.QueryString["IDRubro"];

                if (!this.hdnIDRubro.Value.Equals("0"))
                {
                    this.cargarDatos(Convert.ToInt32(Request.QueryString["IDRubro"]));
                }
            }
        }
    }

    private void cargarDatos(int id)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Rubros.Where(x => x.IDRubro == id).FirstOrDefault();
                if (entity != null)
                {
                    this.txtNombre.Text = entity.Nombre;

                    if (!string.IsNullOrEmpty(entity.Icono))
                    {
                        lnkLogo.NavigateUrl = "/fileHandler.ashx?type=rubros&module=admin&file=" + entity.Icono;
                        lnkLogoDelete.NavigateUrl = "javascript: void(0)";
                        lnkLogoDelete.Attributes.Add("onclick", "return deleteLogo('" + entity.Icono + "', 'divLogo')");
                        divLogo.Visible = true;

                        imgLogo.ImageUrl = "/files/rubros/" + entity.Icono;
                    }
                    else
                        imgLogo.ImageUrl = "http://www.placehold.it/48x48/EFEFEF/AAAAAA";
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static string grabar(int id, string nombre)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    Rubros entity;
                    if (id > 0)
                        entity = dbContext.Rubros.Where(x => x.IDRubro == id).FirstOrDefault();
                    else
                    {
                        entity = new Rubros();
                    }
                    entity.Nombre = nombre != null && nombre != "" ? nombre.ToUpper() : "";

                    if (HttpContext.Current.Session["CurrentLogo"] != null && HttpContext.Current.Session["CurrentLogo"] != "")
                        entity.Icono = HttpContext.Current.Session["CurrentLogo"].ToString();

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Rubros.Add(entity);
                        dbContext.SaveChanges();
                    }

                    return entity.IDRubro.ToString();
                }

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";

    }

    [WebMethod(true)]
    public static void eliminarLogo(int id, string archivo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var file = "//files//rubros//" + archivo;
            if (!string.IsNullOrEmpty(file) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(file)))
            {
                System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                if (id > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Rubros.Where(x => x.IDRubro == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Icono = null;
                            HttpContext.Current.Session["CurrentLogo"] = null;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

    protected void uploadLogo(object sender, EventArgs e)
    {
        try
        {
            var extension = flpLogo.FileName.Split('.')[1].ToLower();
            if (extension == "jpg" || extension == "png" || extension == "gif")
            {

                string ext = System.IO.Path.GetExtension(flpLogo.FileName);
                string uniqueName = "rubro_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ext;
                string path = System.IO.Path.Combine(Server.MapPath("~/files/rubros/"), uniqueName);

                flpLogo.SaveAs(path);

                Session["CurrentLogo"] = uniqueName;

                //string script = "SetFile('" + uniqueName + "');";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileOk", script, true);
            }
            else
                throw new Exception("Extension inválida");
        }
        catch (Exception ex)
        {
            Session["CurrentLogo"] = null;
            // I didn't test it in visual studio, it means we will call the method js after postback
            string script = "ShowUploadError('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "FileError", script, true);
            //throw ex;
        }
    }

    #region Subrubros

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, int idRubro)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Rubros
                    .Where(x => x.IDRubroPadre == idRubro)
                    .OrderBy(x => x.Nombre)
                    .Select(x => new SubrubrosViewModel()
                    {
                        IDSubrubro = x.IDRubro,
                        IDRubroPadre = x.IDRubroPadre,
                        Nombre = x.Nombre
                    }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Rubros.Where(x => x.IDRubro == id).FirstOrDefault();
                    if (entity != null)
                    {

                        dbContext.Rubros.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void procesarSubrubro(int IDSubrubro, int IDRubroPadre, string nombre)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.Rubros.Any(x => x.Nombre.ToLower() == nombre.ToLower() && x.IDRubroPadre == IDRubroPadre))
                        throw new Exception("Ya existe un subrubro con el nombre ingresado");

                    Rubros entity;
                    if (IDSubrubro == 0)
                        entity = new Rubros();
                    else
                        entity = dbContext.Rubros.Where(x => x.IDRubro == IDSubrubro).FirstOrDefault();

                    entity.IDRubroPadre = IDRubroPadre;
                    entity.Nombre= nombre != null && nombre != "" ? nombre.ToUpper() : "";

                    if (IDSubrubro == 0)
                        dbContext.Rubros.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    #endregion
}