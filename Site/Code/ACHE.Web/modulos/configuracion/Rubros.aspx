﻿<%@ Page Title="" MasterPageFile="~/MasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="Rubros.aspx.cs" Inherits="modulos_configuracion_rubros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/rubros.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Configuración</a></li>
            <li class="last">Rubros</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de rubros</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form>
			    <div class="formSep col-sm-8 col-md-8">
				    <div class="row">
					    <div class="col-sm-4 col-md-4">
						    <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="50" class="form-control"/>
						    <span class="help-block"></span>
					    </div>
					    
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>