﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dealer.aspx.cs" Inherits="modulos_configuracion_Dealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/dealer.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de Dealer</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formSocio" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                     <div class="row"> 
                     <div class="col-sm-2">
                            <label>Nombre</label>
                            <asp:TextBox runat="server" class="form-control" ID="txtNombre" />
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                             <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        </div>
                       
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

