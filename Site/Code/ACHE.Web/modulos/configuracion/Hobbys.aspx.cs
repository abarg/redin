﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using ACHE.Business;
using ACHE.Model.ViewModels;

public partial class modulos_configuracion_Hobbys : PaginaBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {

                return dbContext.Hobbies
                        .OrderBy(x => x.Nombre)
                        .Select(x => new HobbiesViewModel()
                        {
                            IDHobbies = x.IDHobbie,
                            Nombre = x.Nombre
                        }).ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var hobbys = dbContext.Hobbies.Where(x => x.IDHobbie == id).FirstOrDefault();
                    if (hobbys != null)
                    {

                        dbContext.Hobbies.Remove(hobbys);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw new Exception("No se puede eliminar ésta profesión porque ya se encuentre asociada");

                }
            }
        }
    }

    [WebMethod(true)]
    public static string Exportar(string nombre)
    {
        string fileName = "hobbies";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {

                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.Hobbies.OrderBy(x => x.Nombre).AsEnumerable();
                    if (!string.IsNullOrEmpty(nombre))
                        info = info.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));
                    dt = info.Select(x => new
                    {
                        IDHobbie = x.IDHobbie,
                        Nombre = x.Nombre
                    }).ToList().ToDataTable();

                    if (dt.Rows.Count > 0)
                        generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    else
                        throw new Exception("No se encuentran datos para los filtros seleccionados");
                    return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}