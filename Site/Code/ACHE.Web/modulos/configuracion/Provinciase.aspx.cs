﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;

public partial class modulos_configuracion_Provinciase : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            cargarPaises();
            if (!String.IsNullOrEmpty(Request.QueryString["IDProvincia"])) {
                try {
                    int idProvincia = int.Parse(Request.QueryString["IDProvincia"]);
                    if (idProvincia > 0) {
                        this.hdnIDProvincia.Value = idProvincia.ToString();
                        cargarDatosProvincia(idProvincia);
                        cmbPaises.Enabled = false;
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("Ciudades.aspx");
                }
            }
        }

    }


    private void cargarDatosProvincia(int idProvincia) {
        using (var dbContext = new ACHEEntities()) {
            var provincia = dbContext.Provincias.Where(x => x.IDProvincia == idProvincia).FirstOrDefault();
            if (provincia != null) {
                this.txtNombreProvincia.Text = provincia.Nombre;
                this.cmbPaises.SelectedValue = provincia.Paises.IDPais.ToString();
            }
        }
    }


    private void cargarPaises() {
        using (var dbContext = new ACHEEntities()) {
            var paises = dbContext.Paises.OrderBy(x => x.Nombre).Select(x => new { IDPais = x.IDPais, Nombre = x.Nombre.ToUpper() }).ToList();
            if (paises != null) {
                cmbPaises.DataSource = paises;
                cmbPaises.DataTextField = "Nombre";
                cmbPaises.DataValueField = "IDPais";
                cmbPaises.DataBind();
                cmbPaises.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [WebMethod(true)]
    public static string grabar(int idProvincia, string Nombre, int idPais) {
        try {
            //Ciudad
            Provincias entity;
            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Provincias.Include("Paises").Where(x => x.IDProvincia != idProvincia && x.Nombre.ToLower() == Nombre.ToLower() && x.Paises.IDPais == idPais).FirstOrDefault();
                if (aux != null)
                    throw new Exception("Ya existe una provincia perteneciente al país " + aux.Paises.Nombre + " llamada " + aux.Nombre);

                if (idProvincia > 0)
                    entity = dbContext.Provincias.FirstOrDefault(s => s.IDProvincia == idProvincia);                    
                else
                    entity = new Provincias();

                entity.Nombre = Nombre != null && Nombre != "" ? Nombre.Trim().ToUpper() : "";
                entity.IDPais = idPais;

                if (idProvincia > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.Provincias.Add(entity);
                    dbContext.SaveChanges();
                }
            }

            return entity.IDProvincia.ToString();
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}