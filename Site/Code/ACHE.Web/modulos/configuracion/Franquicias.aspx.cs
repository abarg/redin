﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;

public partial class modulos_configuracion_franquicias : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Franquicias
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new FranquiciasViewModel()
                    {
                        IDFranquicia = x.IDFranquicia,
                        Nombre = x.NombreFantasia,
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        Email = x.Email,
                        Arancel = x.Arancel,
                        ComisionTpCp = x.ComisionTpCp,
                        ComisionTtCp = x.ComisionTtCp,
                        ComisionTpCt = x.ComisionTpCt,
                        PubLocal = x.PublicidadLocal,
                        PubNacional = x.PublicidadNacional
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    //if (dbContext.Tarjetas.Any(x => x.IDMarca == id))
                    //    throw new Exception("Esta Marca tiene tarjetas creadas. No se puede eliminar");
                    //else
                    //{
                        var entity = dbContext.Franquicias.Where(x => x.IDFranquicia == id).FirstOrDefault();
                        if (entity != null)
                        {
                            entity.Estado = "B";
                            //dbContext.Franquicias.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    //}
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

}