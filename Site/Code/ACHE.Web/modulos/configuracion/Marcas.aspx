﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Marcas.aspx.cs" Inherits="modulos_configuracion_marcas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/marcas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Configuración</a></li>
            <li class="last">Marcas</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Administración de marcas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server">
                <div class="formSep col-sm-8 col-md-8">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <label>Franquicia</label>
                            <asp:DropDownList runat="server" ID="ddlFranquicias" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                            <span class="help-block"></span>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="50" class="form-control" />
                            <span class="help-block"></span>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Ciudades" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

