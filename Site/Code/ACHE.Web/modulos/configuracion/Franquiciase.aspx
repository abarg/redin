﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Franquiciase.aspx.cs" Inherits="modulos_configuracion_franquiciase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/configuracion/franquiciase.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Configuración</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/configuracion/franquicias.aspx") %>">Franquicias</a></li>
                <li>Edición</li>
            </ul>
        </div>
    </nav>

     <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading" id="litTitulo">Edición de Franquicias</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

            <div class="tabbable" id="Tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tabDatosPrincipales" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tabComision" data-toggle="tab">Comisión & Publicidad</a></li>
                    <li><a href="#tabFormasPago" data-toggle="tab">Datos Bancarios</a></li>
                    <li><a href="#tabContacto" data-toggle="tab">Contacto</a></li>
                    <li><a href="#tabDomicilio" data-toggle="tab">Domicilio</a></li>
                    <li><a href="#tabUsuarios" class="hide" data-toggle="tab">Usuarios</a></li>
                </ul>

                <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                    <div class="tab-content">                       
                        <div class="tab-pane active" id="tabDatosPrincipales">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fecha de Carga</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtFechaAlta" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Razón Social</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control required" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Nombre de Fantasia</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtNombreFantasia" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Tipo y Nro Doc.</label>
                                <div class="col-sm-2 col-md-2">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTipoDoc">
                                        <asp:ListItem Value="CUIT" Text="CUIT" />
                                        <asp:ListItem Value="DNI" Text="DNI" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtNroDocumento" CssClass="form-control required number" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Condición Iva</label>
                                <div class="col-sm-4 col-md-4">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlIVA">
                                        <asp:ListItem Value="RI" Text="Resp. Inscripto" />
                                        <asp:ListItem Value="MONOTRIBUTISTA" Text="Monotributista" />
                                        <asp:ListItem Value="EX" Text="Exento/No Resp." />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Web</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtWeb" CssClass="form-control" MaxLength="225"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtObservaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="tab-pane" id="tabComision">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Comisión %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtArancel" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> T propias en C propias %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtComisionTpCp" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> T terceros en C propios %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtComisionTtCp" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> T propias en C terceros %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtComisionTpCt" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pub. Local %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPubLocal" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pub. Nacional %</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPubNacional" CssClass="form-control required" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tabFormasPago">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tipo</label>
                                <div class="col-lg-4">
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdbFormaPago_Debito" GroupName="grpFormaPago" Checked="false" Text="Débito Bancario" Width="100px" />
                                    </label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdbFormaPago_Tarjeta" GroupName="grpFormaPago" Checked="false" Text="Tarjeta: Débito/Crédito" Width="150px" />
                                    </label>
                                </div>
                            </div>

                            <div runat="server" id="divFormaPago_Debito">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Banco</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_Banco" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tipo Cuenta</label>
                                    <div class="col-lg-4">
                                        <asp:DropDownList runat="server" class="form-control" ID="ddlFormaPago_TipoCuenta">
                                            <asp:ListItem Value="1" Text="Caja de Ahorro" />
                                            <asp:ListItem Value="2" Text="Cuenta Corriente" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nro. de Cuenta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_NroCuenta" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">CBU</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CBU" CssClass="form-control range" MaxLength="22" Minlength="22"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Repetir CBU</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CBU_Rep" CssClass="form-control range" equalTo="#txtFormaPago_CBU" MaxLength="22" Minlength="22"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div runat="server" id="divFormaPago_Tarjeta" style="display: none;">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tarjeta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_Tarjeta" CssClass="form-control" MaxLength="23"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Banco emisor</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_BancoEmisor" CssClass="form-control" MaxLength="13"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nro. Tarjeta</label>
                                    <div class="col-lg-4">
                                        <asp:TextBox runat="server" ID="txtFormaPago_NroTarjeta" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Fecha Vto. (mm/aaaa)</label>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFormaPago_FechaVto" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Cod. de Seguridad</label>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtFormaPago_CodigoSeg" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div class="tab-pane" id="tabContacto">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Nombre" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Apellido</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Apellido" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">DNI</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtContacto_Documento" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cargo</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Cargo" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtContacto_Telefono" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Celular</label>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtContacto_Celular" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Email" CssClass="form-control email" MaxLength="128"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Observaciones</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtContacto_Observaciones" Rows="5" CssClass="form-control" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabDomicilio">
                            <br />
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Pais</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlPais">
                                    
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"><span class="f_req">*</span> Provincia</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control required" ID="ddlProvincia" onchange="LoadCiudades(this.value,'ddlCiudad');">
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Ciudad</label>
                                <div class="col-lg-4">
                                    <asp:DropDownList runat="server" CssClass="form-control chzn_b" ID="ddlCiudad"
                                        data-placeholder="Seleccione una ciudad">
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Domicilio</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Piso/Depto</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Código Postal</label>
                                <div class="col-lg-2">
                                    <asp:TextBox runat="server" ID="txtCodigoPostal" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtTelefonoDom" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Fax</label>
                                <div class="col-lg-4">
                                    <asp:TextBox runat="server" ID="txtFax" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabUsuarios">
                            <br />
                            <div class="alert alert-danger alert-dismissable" id="divErrorUsuario" style="display: none"></div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Usuario</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:TextBox runat="server" ID="txtEmailUsuario" CssClass="form-control email" MaxLength="100"></asp:TextBox>
                                        <span class="help-block">Email</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control" MinLength="3" MaxLength="10"></asp:TextBox>
                                        <span class="help-block">Contraseña</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:DropDownList runat="server" ID="ddlTipoUsuario" CssClass="form-control">
                                            <asp:ListItem Text="Admin" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Backoffice" Value="B"></asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block">Tipo</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button runat="server" id="btnAgregarUsuario" class="btn" type="button" onclick="agregarUsuario();">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                   
                                    <div id="grid"></div>
                                    <br /><br /><br />
                                    <asp:HiddenField runat="server" ID="hfIDUsuario" Value="0" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="formButtons">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
                                <a href="franquicias.aspx" class="btn btn-link">Cancelar</a>

                                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</asp:Content>

