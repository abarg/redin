﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pinese.aspx.cs" Inherits="modulos_catalogo_pinese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>" ></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/catalogo/pinese.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>" ></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskMoney.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Catálogo</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/catalogo/pines.aspx") %>">Pines</a></li>
            <li>
                <span id="spnModo" />
            </li>
        </ul>
    </div>

     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Pin</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span> Control</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtCodigoControl" CssClass="form-control required" MaxLength="6" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span> Parte1</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtCodigoParte1" CssClass="form-control required" MaxLength="10" />
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span> Parte2</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtCodigoParte2" CssClass="form-control required" MaxLength="10" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span> Empresa Canje</label>
                    <div class="col-lg-4">
                        <asp:DropDownList runat="server" ID="cmbEmpresa" CssClass="form-control required" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"> Descripcion</label>
                    <div class="col-lg-4">
                        <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control" TextMode="MultiLine" Rows="5" Columns="10" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span>Costo Interno</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtCostoInterno" ClientIDMode="Static" CssClass="form-control required" MaxLength="10" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*&nbsp;</span>Costo Puntos</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtCostoPuntos" ClientIDMode="Static" CssClass="form-control required" MaxLength="10" />
                    </div>
                </div>
                <div class="form-group" runat="server" id="divSocio" visible="false">
                    <label class="col-lg-3 control-label">Socio</label>
                    <div class="col-lg-3">
                        <asp:Literal runat="server" ID="litSocio" />
                    </div>
                </div>
                <div class="form-group" runat="server" id="divFechaCompra" visible="false">
                    <label class="col-lg-3 control-label">Fecha Compra</label>
                    <div class="col-lg-3">
                        <asp:Literal runat="server" ID="litFechaCompra" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a href="pines.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

