﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="altaPines.aspx.cs" Inherits="modulos_gestion_altaPines" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/catalogo/altaMasiva.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <nav>
        <div id="jCrumbs" class="breadCrumb module">
            <ul>
                <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#">Catálogo</a></li>
                <li><a href="<%= ResolveUrl("~/modulos/catalogo/pines.aspx") %>">Pines</a></li>
                <li>Alta Masiva</li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Alta Masiva de Pines</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" runat="server" visible="false"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" runat="server" visible="false" >Los datos se han actualizado correctamente.</div>
            <form runat="server" id="formAltaMasiva" class="form-horizontal" role="form" >
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Empresa Canje</label>
                    <div class="col-lg-3">
                        <asp:DropDownList runat="server" ID="cmbEmpresaCanje" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ID="rqvEmpresa" ControlToValidate="cmbEmpresaCanje"
                            ErrorMessage="Este campo es obligatorio." CssClass="errorRequired" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><span class="f_req">*&nbsp;</span>Archivo CSV</label>
                    <div class="col-lg-3">
                        <asp:FileUpload ID="flpPines" runat="server" Style="float: left" />
                        <asp:RequiredFieldValidator runat="server" ID="rqvArchivo" ControlToValidate="flpPines"
                            ErrorMessage="Este campo es obligatorio." CssClass="errorRequired" />
                        <br />
                        <br />
                        <div style="clear: both"></div>
                        <a href="/files/importaciones/formato.csv" target="_blank">Ver formato</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <asp:Button runat="server" ID="btnImportar" CssClass="btn btn-success" OnClick="btnImportar_Click" Text="Importar" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

