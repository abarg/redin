﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;

public partial class modulos_catalogo_pinese : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            try {
                CargarCombos();
                if (!string.IsNullOrEmpty(Request.QueryString["ID"])) {
                    hdnID.Value = Request.QueryString["ID"];
                    if (hdnID.Value != "0")
                        CargarInfo(int.Parse(hdnID.Value));
                }
            }
            catch(Exception ex) {
                Response.Redirect("pines.aspx");
            }
        }
    }

    private void CargarCombos() {
        using (var dbContext = new ACHEEntities()) {
            var canjes = dbContext.EmpresasCanje.Where(x => x.Activo && x.UsaPines).ToList();
            if (canjes != null) {
                this.cmbEmpresa.DataSource = canjes;
                this.cmbEmpresa.DataValueField = "IDEmpresaCanje";
                this.cmbEmpresa.DataTextField = "Nombre";
                this.cmbEmpresa.DataBind();
                this.cmbEmpresa.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    private void CargarInfo(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Pines.Where(x => x.IDPin == id).FirstOrDefault();
            if (entity != null) {
                cmbEmpresa.SelectedValue = entity.IDEmpresaCanje.ToString();
                txtCodigoControl.Text = entity.Control;
                txtCodigoParte1.Text = entity.Parte1;
                txtCodigoParte2.Text = entity.Parte2;
                txtDescripcion.Text = entity.Descripcion;
                txtCostoInterno.Text = entity.CostoInterno.ToString();
                txtCostoPuntos.Text = entity.CostoPuntos.ToString();
                if (entity.IDSocio.HasValue) {
                    divSocio.Visible = divFechaCompra.Visible = true;
                    litFechaCompra.Text = DateTime.Parse(entity.FechaCompra.ToString()).ToShortDateString();
                    litSocio.Text = entity.Socios.Apellido + ", " + entity.Socios.Nombre;
                }
            }
            else throw new Exception("Entidad Inexistente");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string control, string parte1,string parte2, int idEmpresaCanje, string descripcion, decimal costoInterno, int costoPuntos) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var existe = dbContext.Pines.Any(x => x.Control == control &&x.Parte1==parte1&&x.Parte2==parte2 && x.IDPin != id);
                    if (existe)
                        throw new Exception("Ya existe un pin con el código ingresado");

                    Pines entity;
                    if (id > 0)
                        entity = dbContext.Pines.Where(x => x.IDPin == id).FirstOrDefault();
                    else
                        entity = new Pines();

                    entity.Control = control;
                    entity.Parte1 = parte1;
                    entity.Parte2 = parte2;
                    entity.IDEmpresaCanje = idEmpresaCanje;
                    entity.Descripcion = descripcion;
                    entity.CostoInterno = costoInterno;
                    entity.CostoPuntos = costoPuntos;

                    if (id > 0)
                        dbContext.SaveChanges();
                    else {
                        dbContext.Pines.Add(entity);
                        dbContext.SaveChanges();

                        dbContext.ActualizarStockPines(idEmpresaCanje);
                    }
                }
            }
        }
        catch (Exception ex) {
            var msg = ex.Message;
            throw ex;
        }
    }
}