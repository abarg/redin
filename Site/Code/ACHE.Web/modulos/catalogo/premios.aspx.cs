﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_catalogo_premios : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CargarCombos();
    }


    private void CargarCombos()
    {
        bRubros bRubros = new bRubros();
        this.ddlRubro.DataSource = bRubros.getRubros();
        this.ddlRubro.DataValueField = "IDRubro";
        this.ddlRubro.DataTextField = "Nombre";
        this.ddlRubro.DataBind();
        this.ddlRubro.Items.Insert(0, new ListItem("", "0"));
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string stockDesde, string stockHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Premios
                    .Where(x => x.TipoMov != "B" && !x.IDMarca.HasValue)
                    .OrderBy(x => x.Descripcion)
                    .Select(x => new PremiosViewModel()
                    {
                        IDPremio = x.IDPremio,
                        IDRubro = x.IDRubro,
                        Descripcion = x.Descripcion,
                        Codigo = x.Codigo,
                        StockActual = x.StockActual,
                        ValorPuntos = x.ValorPuntos,
                        ValorPesos = x.ValorPesos,
                        FechaVigenciaDesde = x.FechaVigenciaDesde,
                        FechaVigenciaHasta = x.FechaVigenciaHasta
                    });

                if (stockDesde != string.Empty)
                {
                    int dtDesde = int.Parse(stockDesde);
                    result = result.Where(x => x.StockActual >= dtDesde);
                }
                if (stockHasta != string.Empty)
                {
                    int dtHasta = int.Parse(stockHasta);
                    result = result.Where(x => x.StockActual <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Premios.Where(x => x.IDPremio == id).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.TipoMov = "B";
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string Exportar(string descripcion, string codigo, string stockDesde, string stockHasta, int idRubro)
    {
        string fileName = "Premios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var info = dbContext.Premios.Where(x => x.TipoMov != "B" && !x.IDMarca.HasValue).AsEnumerable();
                    if (descripcion != "")
                        info = info.Where(x => x.Descripcion.ToLower().Contains(descripcion.ToLower()));
                    if (codigo != "")
                        info = info.Where(x => x.Codigo.ToLower().Contains(codigo.ToLower()));
                    if (stockDesde != string.Empty)
                    {
                        int dtDesde = int.Parse(stockDesde);
                        info = info.Where(x => x.StockActual >= dtDesde);
                    }
                    if (stockHasta != string.Empty)
                    {
                        int dtHasta = int.Parse(stockHasta);
                        info = info.Where(x => x.StockActual <= dtHasta);
                    }
                    if (idRubro > 0)
                        info = info.Where(x => x.IDRubro == idRubro);

                    dt = info.Select(x => new
                    {
                        Descripcion = x.Descripcion,
                        Codigo = x.Codigo,
                        StockActual = x.StockActual,
                        ValorPuntos = x.ValorPuntos,
                        ValorPesos = x.ValorPesos,
                        FechaVigenciaDesde = x.FechaVigenciaDesde.HasValue ? x.FechaVigenciaDesde.Value.ToString("dd/MM/yyyy") : "",
                        FechaVigenciaHasta = x.FechaVigenciaHasta.HasValue ? x.FechaVigenciaHasta.Value.ToString("dd/MM/yyyy") : ""
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}