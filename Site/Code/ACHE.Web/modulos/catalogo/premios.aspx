﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="premios.aspx.cs" Inherits="modulos_catalogo_premios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/catalogo/premios.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Catálogo</a></li>
            <li class="last">Premios</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de premios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form runat="server">
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
					    <div class="col-md-2">
                            <label>Rubro</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlRubro">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
						    <label>Descripción</label>
                            <input type="text" id="txtDescripcion" value="" maxlength="20" class="form-control"/>
						    <span class="help-block"></span>
					    </div>
					    <div class="col-md-2">
						    <label>Código</label>
                            <input type="text" id="txtCodigo" value="" maxlength="6" class="form-control"/>
						    <span class="help-block"></span>
					    </div>
                         <div class="col-md-2">
                            <label>Stock desde</label>
                            <input type="text" id="txtStockDesde" value="" maxlength="10" class="form-control"/>
                        </div>
                        <div class="col-md-2">
                            <label>Stock hasta</label>
                            <input type="text" id="txtStockHasta" value="" maxlength="10" class="form-control"/>
                        </div>
                        <div class="col-md-2"></div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            <a class="btn btn-info" id="btnImportar" href="/modulos/seguridad/Importacion.aspx">Importar</a>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Premios" style="display:none">Descargar</a>
                            
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>

