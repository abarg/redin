﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using System.IO;
using System.Data.SqlClient;

public partial class modulos_gestion_altaPines : PaginaBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            LoadInfo();
        }
    }

    private void LoadInfo() {
        using (var dbContext = new ACHEEntities()) {
            var empresas = dbContext.EmpresasCanje.Where(x => x.Activo && x.UsaPines).ToList();
            if (empresas != null) {
                cmbEmpresaCanje.DataSource = empresas;
                cmbEmpresaCanje.DataValueField = "IDEmpresaCanje";
                cmbEmpresaCanje.DataTextField = "Nombre";
                cmbEmpresaCanje.DataBind();

                cmbEmpresaCanje.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    #region Importación
    protected void btnImportar_Click(object sender, EventArgs e) {
        try {
            if (flpPines.HasFile) {
                int idEmpresa = int.Parse(cmbEmpresaCanje.SelectedValue);
                string extension = this.flpPines.PostedFile.FileName;
                extension = extension.Substring(extension.LastIndexOf(".") + 1);
                if (extension.Trim() == "csv") {
                    string path = Server.MapPath("/files/importaciones/") + flpPines.PostedFile.FileName;
                    flpPines.PostedFile.SaveAs(path);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("IDEmpresaCanje", typeof(int));//0
                    dt.Columns.Add("Control", typeof(string));//1
                    dt.Columns.Add("Parte1", typeof(string));//2
                    dt.Columns.Add("Parte2", typeof(string));//3
                    dt.Columns.Add("Descripcion", typeof(string));//4
                    dt.Columns.Add("CostoInterno", typeof(decimal));//5
                    dt.Columns.Add("CostoPuntos", typeof(int));//6

                    string csvContentStr = File.ReadAllText(path);
                    string[] rows = csvContentStr.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < rows.Length; i++) {
                        //Split the tab separated fields (comma separated split commented)
                        if (i > 0) {
                            string[] dr = rows[i].Split(new char[] { ';' });
                            if (dr.Length == 6) {
                                #region Armo DataRow
                                try {
                                    if (!string.IsNullOrEmpty(dr[0])) {
                                        DataRow drNew = dt.NewRow();
                                        drNew[0] = idEmpresa;//idempresacanje
                                        drNew[1] = dr[0];//control
                                        drNew[2] = dr[1];//parte1
                                        drNew[3] = dr[2];//parte2
                                        drNew[4] = dr[3];//descripción
                                        drNew[5] = dr[4];//costointerno
                                        drNew[6] = dr[5];//costopuntos

                                        dt.Rows.Add(drNew);
                                    }
                                }
                                catch (Exception ex) {
                                    var msg = ex.Message;
                                }
                                #endregion
                            }
                        }
                    }

                    if (dt.Rows.Count > 0) {
                        EliminarTmp();
                        using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString)) {
                            sqlConnection.Open();
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection)) {
                                bulkCopy.DestinationTableName = "PinesTemp";
                                foreach (DataColumn col in dt.Columns)
                                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                                bulkCopy.WriteToServer(dt);
                            }
                        }
                        ProcesarTmp(idEmpresa);
                        File.Delete(path);
                        ShowOk("Archivo importado correctamente");
                        
                    }
                    else
                        ShowError("No hay datos para importar. Verifique que el formato sea el correcto");
                }
                else
                    ShowError("Extension de archivo invalida");
            }
            else
                ShowError("Debe seleccionar un archivo");
        }
        catch (Exception ee) {
            var msg = ee.InnerException != null ? ee.InnerException.Message : ee.Message;
            ShowError(ee.Message);
        }
    }

    private void EliminarTmp() {
        try {
            using (var dbContext = new ACHEEntities()) {
                dbContext.Database.ExecuteSqlCommand("delete PinesTemp");
            }
        }
        catch (Exception ex) {
            ShowError("Error al eliminar los datos de la tabla temporal");
        }
    }

    private void ProcesarTmp(int idEmpresa) {
        try {
            using (var dbContext = new ACHEEntities()) {
                dbContext.Database.ExecuteSqlCommand("exec ProcesarPines");
                dbContext.ActualizarStockPines(idEmpresa);
            }
        }
        catch (Exception ex) {
            ShowError("Error al eliminar los datos de la tabla temporal");
        }
    }

    private void ShowError(string msg) {
        divOK.Visible = false;
        divOK.InnerText = string.Empty;
        divError.Visible = true;
        divError.InnerText = msg;
    }

    private void ShowOk(string msg) {
        divError.Visible = false;
        divError.InnerText = string.Empty;
        divOK.Visible = true;
        divOK.InnerText = msg;
    }
    #endregion
}