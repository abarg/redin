﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="premiose.aspx.cs" Inherits="modulos_catalogo_premiose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
  <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/catalogo/premiose.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Catálogo</a></li>
            <li><a href="<%= ResolveUrl("~/modulos/catalogo/premios.aspx") %>">Premios</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Premio</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />

                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Código</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control required" MinLength="6" MaxLength="7"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Descripción</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control required" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"> Rubro</label>
                    <div class="col-sm-4 col-md-4">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlRubro">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Valor Puntos</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtValorPuntos" CssClass="form-control required" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Valor Pesos</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtValorPesos" CssClass="form-control required" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Vigencia desde</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Vigencia hasta</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Stock</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtStock" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Stock mínimo</label>
                    <div class="col-lg-3">
                        <asp:TextBox runat="server" ID="txtStockMinimo" CssClass="form-control required" ></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divUploadFoto" style="display:none">
                    <label class="col-lg-3 control-label">Foto</label>
                    <div class="col-lg-4">
                        <asp:Image runat="server" ID="imgFoto" style="width: 180px; height: 120px;"></asp:Image>
                        <br /><br />
                        <ajaxToolkit:AsyncFileUpload runat="server" ID="flpFoto" CssClass="form-control" PersistFile="true"
                            ThrobberID="throbberFoto" OnClientUploadComplete="UploadCompleted" Width="200px"
                            ErrorBackColor="Red" CompleteBackColor="White" UploadingBackColor="White"
                            OnUploadedComplete="uploadFoto" OnClientUploadStarted="UploadStarted" OnClientUploadError="UploadError" />
                        <asp:Label runat="server" ID="throbberFoto" Style="display: none;">
                            <img alt="" src="../../img/ajax_loader.gif" />
                        </asp:Label>
                        <span class="help-block">Extensions: jpg/png/gif. Max size: 1mb</span>
                    </div>
                    <div class="col-sm-4" runat="server" id="divFoto" visible="false">
                        Actual: <asp:HyperLink runat="server" ID="lnkFoto" Target="_blank">Descargar</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink runat="server" ID="lnkFotoDelete" Target="_blank">Eliminar</asp:HyperLink>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                       <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();" >Guardar</button>
                       <a href="premios.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</asp:Content>

