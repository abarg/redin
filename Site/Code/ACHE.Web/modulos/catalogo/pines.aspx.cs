﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class modulos_catalogo_pines : PaginaBase {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            CargarCombos();
    }

    private void CargarCombos() {
        using (var dbContext = new ACHEEntities()) {
            var canjes = dbContext.EmpresasCanje.Where(x => x.Activo && x.UsaPines).ToList();
            if (canjes != null) {
                this.cmbEmpresa.DataSource = canjes;
                this.cmbEmpresa.DataValueField = "IDEmpresaCanje";
                this.cmbEmpresa.DataTextField = "Nombre";
                this.cmbEmpresa.DataBind();
                this.cmbEmpresa.Items.Insert(0, new ListItem("", ""));
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            using (var dbContext = new ACHEEntities()) {
                var result=  dbContext.Pines
                    .Include("EmpresasCanje")
                    .OrderBy(x => x.Control)
                    .Select(x => new PinesViewModel() {
                        IDPin = x.IDPin,
                        IDEmpresaCanje = x.IDEmpresaCanje,
                        EmpresaCanje = x.EmpresasCanje.Nombre,
                        Control = x.Control,
                        Parte1=x.Parte1,
                        Parte2=x.Parte2,
                        CostoInterno = x.CostoInterno,
                        CostoPuntos = x.CostoPuntos,
                        Socio = x.IDSocio != null ? x.Socios.Apellido  + ", " + x.Socios.Nombre : string.Empty,
                        FechaCompra = x.FechaCompra != null ? x.FechaCompra : null,
                    }).ToDataSourceResult(take, skip, sort, filter);

                //foreach (var item in result) { 
                
                //}

                return result;
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static void Delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Pines.Where(x => x.IDPin == id).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Pines.Remove(entity);
                        dbContext.SaveChanges();

                        dbContext.ActualizarStockPines(entity.IDEmpresaCanje);
                    }
                }
            }
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string Exportar(string control, string parte1, string parte2, int idEmpresa) {
        string fileName = "Pines";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Pines.Include("EmpresasCanje").Include("Socios").AsEnumerable();
                    if (control != "")
                        info = info.Where(x => x.Control.ToLower() == control.ToLower());
                    if (parte1 != "")
                        info = info.Where(x => x.Parte1.ToLower() == parte1.ToLower());
                    if (parte2 != "")
                        info = info.Where(x => x.Parte2.ToLower() == parte2.ToLower());
                    if (idEmpresa > 0)
                        info = info.Where(x => x.IDEmpresaCanje == idEmpresa);

                    dt = info.Select(x => new {
                        EmpresaCanje = x.EmpresasCanje.Nombre,
                        Control=x.Control,
                        Parte1=x.Parte1,
                        Parte2=x.Parte2,
                        CostoInterno = x.CostoInterno,
                        CostoPuntos = x.CostoPuntos,
                        Socio = x.IDSocio != null ? x.Socios.Apellido + ", " + x.Socios.Nombre : string.Empty,
                        FechaCompra = x.IDSocio != null ? x.FechaCompra.ToString() : string.Empty
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

}