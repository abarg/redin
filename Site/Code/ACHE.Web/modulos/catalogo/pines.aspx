﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="pines.aspx.cs" Inherits="modulos_catalogo_pines" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/catalogo/pines.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="#">Catálogo</a></li>
            <li class="last">Pines</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
			<h3 class="heading">Administración de premios</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
		    <form runat="server">
			    <div class="formSep col-sm-12 col-md-12">
				    <div class="row">
                        <div class="col-md-2">
						    <label>Control</label>
                            <input type="text" id="txtCodigoControl" value="" maxlength="50" class="form-control" />
						    <span class="help-block"></span>
					    </div>
                         <div class="col-md-2">
						    <label>Parte1</label>
                            <input type="text" id="txtCodigoParte1" value="" maxlength="50" class="form-control" />
						    <span class="help-block"></span>
					    </div>
                         <div class="col-md-2">
						    <label>Parte2</label>
                            <input type="text" id="txtCodigoParte2" value="" maxlength="50" class="form-control" />
						    <span class="help-block"></span>
					    </div>
					    <div class="col-md-2">
                            <label>Empresa</label>
                            <asp:DropDownList runat="server" class="form-control" ID="cmbEmpresa" ClientIDMode="Static" />
                        </div>
                        <div class="col-md-2"></div>
				    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnVerTodos" onclick="verTodos();">Ver Todos</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                            &nbsp;&nbsp;
                            <a class="btn btn-success" href="altaPines.aspx" id="btnAltaMasiva">Alta Masiva</a>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Pines" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>            
		</div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br /><br />
        </div>
    </div>
</asp:Content>

