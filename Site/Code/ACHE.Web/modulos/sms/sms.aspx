﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sms.aspx.cs" Inherits="modulos_sms_sms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
     <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Campañas SMS</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Campañas SMS</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Nombre</label>
                            <input type="text" id="txtNombre" value="" maxlength="100" class="form-control" />
                        </div>
                        <div class="col-md-10">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn" type="button" id="btnNuevo" onclick="Nuevo('I');">Nueva campaña interna</button>
                            <button class="btn" type="button" id="Button1" onclick="Nuevo('E');">Nueva campaña externa</button>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>--%>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/sms/sms.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>