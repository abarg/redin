﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="smse.aspx.cs" Inherits="modulos_sms_smse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.jqEasyCharCounter.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/sms/smse.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />   
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Default.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/modulos/sms/sms.aspx") %>">Campañas SMS</a></li>
            <li>Edición</li>
        </ul>
    </div>
    
     <div class="row">
        <div class="col-sm-10 col-md-10">
            <h3 class="heading" id="litTitulo">Gestión de Campaña SMS</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" ClientIDMode="Static" Value="I" />

                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Fecha Alta</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaAlta" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Fecha Envío Solicitada</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaEnvio" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Usuario Alta</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtUsuarioAlta" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Marca</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtMarca" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Nombre</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtNombre" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Sexo</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:DropDownList runat="server" class="form-control" ID="ddlSexo" Enabled="false">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="Femenino" Value="F"></asp:ListItem>
                            <asp:ListItem Text="Masculino" Value="M"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Edad</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <%--<asp:DropDownList runat="server" ID="ddlEdad" class="form-control" Enabled="false">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="0-18" Value="0-18"></asp:ListItem>
                            <asp:ListItem Text="19-25" Value="19-25"></asp:ListItem>
                            <asp:ListItem Text="26-30" Value="26-30"></asp:ListItem>
                            <asp:ListItem Text="31-40" Value="31-40"></asp:ListItem>
                            <asp:ListItem Text="41-50" Value="41-50"></asp:ListItem>
                            <asp:ListItem Text="51-60" Value="51-60"></asp:ListItem>
                            <asp:ListItem Text="61+" Value="61-200"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtEdades" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mensaje</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" CssClass="form-control required alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        <small>En caso de que lo considere, puede modificar el mensaje original</small>
                    </div>
                </div>
                <div class="form-group externa">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Teléfonos</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtTelefonos" Enabled="false" CssClass="form-control required telefonos" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Envios estimados:</label>
                    <div class="col-sm-4 col-md-4 col-lg-3" style="margin-top: 5px;">
                        <span class="text-warning" style="font-weight: bold;font-size: 18px;"><asp:Literal runat="server" ID="txtEnviosEstimados" /></span>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Costo envio:</label>
                    <div class="col-sm-2" style="margin-top: 5px;">
                        <span class="text-info" style="font-weight: bold;font-size: 18px;">$ <asp:Literal runat="server" ID="txtCosto" /></span>
                    </div>
                    <label class="col-sm-2 col-md-2 col-lg-2 control-label" style="text-align:left;max-width: 120px;">Total estimado:</label>
                    <div class="col-sm-2" style="margin-top: 5px;">
                        <span class="text-success" style="font-weight: bold;font-size: 18px;">$ <asp:Literal runat="server" ID="txtTotal" /></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button runat="server" id="btnAprobar" class="btn btn-success" type="button" onclick="cambiarEstado('A');" >Aprobar</button>
                        <button runat="server" id="btnRechazar" class="btn btn-danger" type="button" onclick="cambiarEstado('R');" >Rechazar</button>
                        <a href="sms.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

