﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;
using System.Collections.Specialized;

public partial class modulos_sms_smse : PaginaBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }
        }
    }

    [WebMethod(true)]
    public static void cambiarEstado(int id, string estado, string mensaje)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                //var usu = (WebMarcasUser)HttpContext.Current.Session["CurrentMarcasUser"];
                //int idMarca = usu.IDMarca;

                using (var dbContext = new ACHEEntities())
                {
                    SMS entity = dbContext.SMS.Where(x => x.IDSMS == id).FirstOrDefault();

                    entity.Mensaje = mensaje;
                    entity.Estado = estado;
                    entity.FechaUltEstado = DateTime.Now;

                    dbContext.SaveChanges();

                    var estadoDesc = (estado == "A" ? "aprobada" : "rechazada");

                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<USUARIO>", entity.UsuarioAlta);
                    replacements.Add("<MENSAJE>", "Su campaña " + entity.Nombre + " ha sido " + estadoDesc);

                    var usuMarca = dbContext.UsuariosMarcas.FirstOrDefault(x => x.IDMarca == entity.IDMarca && x.Usuario == entity.UsuarioAlta);
                    if (usuMarca != null)
                    {
                        bool send = EmailHelper.SendMessage(EmailTemplate.Notificacion, replacements, usuMarca.Email, "RedIN: Campaña SMS " + estadoDesc);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.SMS.Include("Marcas").Where(x => x.IDSMS == id).FirstOrDefault();
            if (entity != null)
            {
                if (entity.Estado != "P")
                    Response.Redirect("sms.aspx");

                hdnTipo.Value = entity.Tipo;
                txtNombre.Text = entity.Nombre;
                txtFechaAlta.Text = entity.FechaAlta.ToString("dd/MM/yyyy");
                txtFechaEnvio.Text = entity.FechaEnvioSolicitada.ToString("dd/MM/yyyy");
                txtUsuarioAlta.Text = entity.UsuarioAlta;
                ddlSexo.Text = entity.Sexo;
                //ddlEdad.Text = entity.Edad;
                txtMensaje.Text = entity.Mensaje;
                txtNombre.Text = entity.Nombre;
                txtTelefonos.Text = entity.Telefonos;

                txtMarca.Text = entity.Marcas.Nombre;
                txtCosto.Text = entity.Marcas.CostoSMS.ToString("N2");

                if (entity.Tipo.ToUpper() == "I")
                {
                    string sql = "select count(*) from SociosView where Celular is not null and IDMarca = " + entity.IDMarca + " ";

                    #region edad
                    txtEdades.Text = entity.Edad.Replace(";", ", ").Replace("61-200", "61+");
                    if (entity.Edad.Trim() != "" && entity.Edad != "Todas")
                    {
                        sql += " and (";

                        string[] edades = entity.Edad.Split(";");
                        string[] min = new string[6];
                        string[] max = new string[6];
                        for (int i = 0; i < edades.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty((edades[i])))
                            {
                                min[i] = edades[i].Split("-")[0];
                                max[i] = edades[i].Split("-")[1];
                            }
                        }

                        int hasta = min.Count();
                        int hasta2 = hasta - 1;
                        for (int j = 0; j < hasta; j++)
                        {
                            if (!string.IsNullOrEmpty(min[j]) && !string.IsNullOrEmpty(max[j]))
                            {
                                if (j == 0)
                                    sql += "(Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                                else
                                    sql += " or (Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                            }
                        }

                        sql += " )";
                    }
                    #endregion

                    #region sexo
                    if (entity.Sexo.Trim() != "" && entity.Sexo != "T")
                    {
                        sql += " and Sexo = ";
                        if (entity.Sexo == "F")
                            sql += "'F'";
                        else
                            sql += "'M'";
                    }
                    #endregion

                    int cant = dbContext.Database.SqlQuery<Int32>(sql, new object[] { }).First();
                    txtTotal.Text = (cant * entity.Marcas.CostoSMS).ToString("N2");
                    txtEnviosEstimados.Text = cant.ToString();
                }
                else if (entity.Tipo.ToUpper() == "E")
                {
                    string telefonos = entity.Telefonos;
                    string[] array = telefonos.Split(';');
                    int cant = array.Length;
                    txtTotal.Text = (cant * entity.Marcas.CostoSMS).ToString("N2");
                    txtEnviosEstimados.Text = cant.ToString();
                }

                /*var sqlQuery = dbContext.SociosView.Where(x => x.IDMarca == entity.IDMarca && x.Celular != string.Empty).AsQueryable();
                if (entity.Sexo.Trim() != "")
                    if (entity.Sexo != "T")
                        sqlQuery = sqlQuery.Where(x => x.Sexo == entity.Sexo);
                if (entity.Edad != "")
                {
                    if (entity.Edad != "Todas")
                    {

                        string[] edades = entity.Edad.Split(";");
                        string[] min = new string[6];
                        string[] max = new string[6];
                        for (int i = 0; i < edades.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty((edades[i])))
                            {
                                min[i] = edades[i].Split("-")[0];
                                max[i] = edades[i].Split("-")[1];
                            }
                        }

                        int hasta = min.Count();
                        int hasta2 = hasta - 1;
                        for (int j = 0; j < hasta; j++)
                        {
                            if (!string.IsNullOrEmpty(min[j]) && !string.IsNullOrEmpty(max[j]))
                            {
                                int a = int.Parse(min[j]);
                                int b = int.Parse(max[j]);
                                sqlQuery = sqlQuery.Where(x => x.Edad >= a && x.Edad <= b);
                            }
                        }

                        //int edadDesde = int.Parse(entity.Edad.Split("-")[0]);
                        //int edadHasta = int.Parse(entity.Edad.Split("-")[1]);

                        //sqlQuery = sqlQuery.Where(x => x.Edad >= edadDesde && x.Edad <= edadHasta);
                    }
                }

                int cant = sqlQuery.Count();*/
            }
        }
    }
}