﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="smse-int-ext.aspx.cs" Inherits="modulos_sms_smse_int_ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/lib/smoke/themes/gebo.css") %>" />
    <style type="text/css">
        .thumb {
            width: inherit;
            margin: 5px;
            border: solid 1px #ccc;
        }

        .sexo {
            height: 100px;
        }

        input.radio.todos {
            margin-left: 46px;
        }

        input.radio.femenino {
            margin-left: 20px;
        }

        input.radio.masculino {
            margin-left: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
       <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/Home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/modulos/sms/sms.aspx") %>">Campañas SMS</a></li>
            <li class="last">Edicion</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading" id="litTitulo">Edición de Campañas SMS Internas</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>
            <label id="lblError" style="color: red; display: none;"></label>
            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" ClientIDMode="Static" Value="I" />
                <asp:HiddenField runat="server" ID="hdnPrecio" Value="" />
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Nombre</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MinLength="3" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label"><span class="f_req">*</span> Fecha de envío</label>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <asp:TextBox runat="server" ID="txtFechaEnvio" CssClass="form-control required validDate" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
                 <div class="form-group interna">
                     <label class="col-sm-4 col-md-4 col-lg-3 control-label">Marca</label>
                     <div>                     
                         <div class="col-sm-4 col-md-4 col-lg-4">                            
                                     <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas"/>
                         </div>                             
                     </div>
                </div>
                <div runat="server" id="divTransacciones" class="form-group interna">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label"><br />c/s Transacciones</label>
                <div>                     
                    <div  class="col-sm-4 col-md-4 col-lg-4">
                        <br /><asp:DropDownList runat="server" class="form-control" ID="ddlTransacciones">
                            <asp:ListItem Text="TODAS" Value=""></asp:ListItem>
                            <asp:ListItem Text="SIN TRANSACCIONES" Value="sin"></asp:ListItem>
                            <asp:ListItem Text="CON TRANSACCIONES" Value="con"></asp:ListItem>
                        </asp:DropDownList>
                    </div>  
                    <div runat="server" id="divFechaTrans" class="col-md-2" style="bottom:5px;display: none">
                        <label> <span class="f_req">* </span>Fecha desde</label>
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10"/>
                    </div>                   
                </div>
                </div>
                <div runat="server" id="divProvincias"  class="form-group interna">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Provincias</label>
                    <div>                     
                        <div class="col-sm-4 col-md-4 col-lg-4">                            
                            <asp:DropDownList runat="server" class="form-control" ID="ddlProvincias" />
                        </div>                             
                    </div>
                </div>
                <div runat="server" id="divCiudades"  class="form-group interna">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Ciudades</label>
                    <div>                             
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <asp:DropDownList runat="server" value="" class="form-control" ID="ddlCiudades" />
                        </div>                      
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Sexo</label>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="imgs">
                            <table>
                                <tr>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/todos.png" id="img4" />
                                            <asp:RadioButton runat="server" Checked="true" ID="rdbTodos" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/femenino.png" id="img5" />
                                            <asp:RadioButton runat="server" ID="rdbFemenino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="thumb">
                                            <img class="sexo" src="/img/sms/masculino.png" id="img6" />
                                            <asp:RadioButton runat="server" ID="rdbMasculino" ClientIDMode="Static" GroupName="sexo" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group interna">
                    <label class="col-sm-4 col-md-4 col-lg-3 control-label">Edad</label>
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <table>
                            <tr>
                                <td>
                                    <%--<input type="checkbox" value="0" id="chk0" onclick="changeCheck('T');" />&nbsp;Todas--%>
                                    <asp:RadioButton runat="server" ID="chk0" ClientIDMode="Static" onclick="changeCheck();" Checked="true" GroupName="Edad" />&nbsp;Todas
                                    <asp:RadioButton runat="server" ID="chkSegmentado" ClientIDMode="Static" onclick="changeCheck();" GroupName="Edad" />&nbsp;Segmentado por rango
                                    <br />
                                    <div id="divTodos" style="display: none;margin-top: 20px;">
                                        <%--<input type="checkbox" value="1" id="chk1"  />&nbsp;0-18--%>
                                        <asp:CheckBox runat="server" ID="chk1" ClientIDMode="Static" />&nbsp;0-18
                                        <br />
                                        <%--<input type="checkbox" value="2" id="chk2"  />&nbsp;19-25--%>
                                        <asp:CheckBox runat="server" ID="chk2" ClientIDMode="Static" />&nbsp;19-25
                                        <br />
                                        <%--<input type="checkbox" value="3" id="chk3"  />&nbsp;26-30--%>
                                        <asp:CheckBox runat="server" ID="chk3" ClientIDMode="Static" />&nbsp;26-30
                                        <br />
                                        <%--<input type="checkbox" value="4" id="chk4"  />&nbsp;31-40--%>
                                        <asp:CheckBox runat="server" ID="chk4" ClientIDMode="Static" />&nbsp;31-40
                                        <br />
                                        <%--<input type="checkbox" value="5" id="chk5"  />&nbsp;40-50--%>
                                        <asp:CheckBox runat="server" ID="chk5" ClientIDMode="Static" />&nbsp;41-50
                                        <br />
                                        <%--<input type="checkbox" value="6" id="chk6"  />&nbsp;51-60--%>
                                        <asp:CheckBox runat="server" ID="chk6" ClientIDMode="Static" />&nbsp;51-60
                                        <br />
                                        <%--<input type="checkbox" value="7" id="chk7"  />&nbsp;61+--%>
                                        <asp:CheckBox runat="server" ID="chk7" ClientIDMode="Static" />&nbsp;61+
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div runat="server" id="divProfesiones"  class="form-group interna">
                <label class="col-sm-4 col-md-4 col-lg-3 control-label">Profesión</label>
                    <div>                     
                        <div class="col-sm-4 col-md-4 col-lg-4">                            
                            <asp:DropDownList runat="server" class="form-control" ID="ddlProfesiones" />
                        </div>                             
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Mensaje</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" CssClass="form-control required alphanumeric rtaMax" MaxLength="160" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group externa">
                    <label class="col-lg-3 control-label"><span class="f_req">*</span> Teléfonos</label>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtTelefonos" CssClass="form-control required telefonos" ClientIDMode="Static" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        <br />
                        <p>Formato: <abbr style="color: #1975FF;">542236874456-movistar;542236859456-personal;542236874445-claro</abbr></p>
                    </div>
                </div>
                <div class="form-group">
                       <div class="col-lg-3 control-label interna"> 
                            Cant. a enviar: 
                         </div>
                        <div class="col-sm-2 col-lg-2 interna">
                            <button runat="server" id="btnCalcular" class="btn" type="button" onclick="calcular();">Calcular</button>
                        </div>
                        <div class="col-sm-2 col-lg-2 interna">
                            <span id="spnCantidad" class="text-info" />
                        </div>
                        <div class="col-lg-2 calculo">
                            <span class="text-success" style="font-weight: bold; font-size: 18px;" id="spnCosto"></span>
                        </div>
                   
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button runat="server" id="btnGrabar" class="btn btn-success" type="button" onclick="guardar();">Guardar</button>
                        <a href="sms.aspx" class="btn btn-link">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.jqEasyCharCounter.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/sms/smse-int-ext.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/smoke/smoke.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>