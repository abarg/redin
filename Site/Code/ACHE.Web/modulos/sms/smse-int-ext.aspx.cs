﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Web.Services;
using ACHE.Business;
using System.Collections.Specialized;

public partial class modulos_sms_smse_int_ext : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0")
                    CargarInfo(int.Parse(hdnID.Value));
            }

            if (!string.IsNullOrEmpty(Request.QueryString["Tipo"]))
            {
                hdnTipo.Value = Request.QueryString["Tipo"];
                if (hdnTipo.Value == "I")
                {
                    CargarProvincias();
                    CargarProfesiones();
                    CargarMarcas();
                }
                else
                {
                    // divCiudades.Visible = false;
                    // divProfesiones.Visible = false;
                    // divProvincias.Visible = false;
                    // divTransacciones.Visible = false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(hdnID.Value) || hdnID.Value == "0")
                    Response.Redirect("sms.aspx");
            }

            rdbTodos.InputAttributes["class"] = "radio todos";
            rdbFemenino.InputAttributes["class"] = "radio femenino";
            rdbMasculino.InputAttributes["class"] = "radio masculino";

            chk0.InputAttributes.Add("value", "0");
            chk1.InputAttributes.Add("value", "1");
            chk2.InputAttributes.Add("value", "2");
            chk3.InputAttributes.Add("value", "3");
            chk4.InputAttributes.Add("value", "4");
            chk5.InputAttributes.Add("value", "5");
            chk6.InputAttributes.Add("value", "6");
            chk7.InputAttributes.Add("value", "7");
        }
    }

    [WebMethod]
    public static List<string> Calcular(string edad, string sexo, string precioSMS, string prov, string ciudad, string tipoTrans, string fechaDesde, string profesion, int idMarca)
    {
        try
        {
            List<string> result = new List<string>();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                //     var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                //    int idMarca = usu.IDMarca;
                using (var dbContext = new ACHEEntities())
                {
                    #region calculo de estimados
                    string sql = "select count(*) from SociosView  join Domicilios d on d.IDDomicilio = SociosView.IDDomicilio join Ciudades c on d.Ciudad = c.IDCiudad join Provincias p on d.Provincia = p.IDProvincia where Celular is not null ";

                    #region Transacciones
                    if (tipoTrans != "")
                    {
                        if (fechaDesde != "")
                        {
                            string estadoTrans = "";

                            if (tipoTrans == "sin")
                                estadoTrans = " not in ";
                            else
                                estadoTrans = " in ";
                            sql += " and SociosView.idsocio " + estadoTrans + " (select distinct(tar.IDSocio) from Transacciones tr,Tarjetas tar where tr.NumTarjetaCliente = tar.Numero and  tr.FechaTransaccion >= '" + fechaDesde + "' )";
                        }
                        else
                            throw new Exception("Debe ingresar una fecha");

                    }

                    #endregion

                    #region Ubicacion
                    if (prov != "")
                    {
                        sql += " and ";
                        sql += prov + " = p.IDProvincia ";

                        if (ciudad != "")
                        {
                            sql += " and ";
                            sql += ciudad + " = c.IDCiudad ";
                        }
                    }
                    #endregion

                    #region edad
                    if (edad != "Todas")
                    {
                        sql += " and (";

                        string[] edades = edad.Split(";");
                        string[] min = new string[6];
                        string[] max = new string[6];
                        for (int i = 0; i < edades.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty((edades[i])))
                            {
                                min[i] = edades[i].Split("-")[0];
                                max[i] = edades[i].Split("-")[1];
                            }
                        }

                        int hasta = min.Count();
                        int hasta2 = hasta - 1;
                        for (int j = 0; j < hasta; j++)
                        {
                            if (!string.IsNullOrEmpty(min[j]) && !string.IsNullOrEmpty(max[j]))
                            {
                                if (j == 0)
                                    sql += "(Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                                else
                                    sql += " or (Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                            }
                        }

                        sql += " )";
                    }
                    //else
                    //{
                    //    sql += "(Edad >= 0 and Edad <= 200)";
                    //}
                    #endregion

                    #region sexo
                    if (sexo != "T")
                    {
                        sql += " and Sexo = ";
                        if (sexo == "F")
                            sql += "'F'";
                        else
                            sql += "'M'";
                    }
                    #endregion

                    #region profesion
                    if (profesion != "")
                    {
                        sql += " and IDSocio in (select s.IDSocio from Socios s left join Profesiones prof on s.IDProfesion = prof.IDProfesion where prof.IDProfesion = " + profesion + " )";
                    }
                    #endregion

                    #region profesion
                    if (idMarca >0)
                    {
                        sql += " and IDMarca = " + idMarca;
                    }
                    #endregion

                    sql += " ";

                    int cant = dbContext.Database.SqlQuery<Int32>(sql, new object[] { }).First();
                    var costoTotal = (cant * decimal.Parse(precioSMS)).ToString("N2");
                    result.Add(costoTotal);
                    result.Add(cant.ToString());
                    #endregion
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, string tipo, string nombre, string sexo, string edad, string mensaje, string fechaEnvio, string telefonos, int idProvincia, int idCiudad, string tipoTrans, string fechaDesdeTrans, int idProfesion, int idMarca)
    {
        int idGenerado = 0;
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {

                using (var dbContext = new ACHEEntities())
                {
                    SMS entity;
                    if (id > 0)
                        entity = dbContext.SMS.Where(x => x.IDSMS == id).FirstOrDefault();
                    else
                    {
                        entity = new SMS();
                        entity.UsuarioAlta = "";
                        entity.FechaAlta = DateTime.Now;
                        entity.Estado = "B";
                        entity.Tipo = tipo;
                    }
                    entity.Estado = "A";
                    entity.Nombre = nombre;
                    entity.Sexo = sexo;
                    entity.Edad = edad;
                    entity.Mensaje = mensaje;
                    entity.FechaEnvioSolicitada = DateTime.Parse(fechaEnvio);
                    entity.Telefonos = telefonos;

                    if (idProvincia == 0)
                        entity.IDProvincia = null;
                    else
                        entity.IDProvincia = idProvincia;

                    if (idCiudad == 0)
                        entity.IDCiudad = null;
                    else
                        entity.IDCiudad = idCiudad;

                    if (idProfesion == 0)
                        entity.IDProfesion = null;
                    else
                        entity.IDProfesion = idProfesion;

                    if (idMarca > 0)
                        entity.IDMarca = idMarca;
                    else
                        entity.IDMarca = null;


                    if (tipoTrans != "" && tipoTrans != "undefined")
                    {
                        entity.ConTransacciones = tipoTrans == "con" ? true : false;
                        if (fechaDesdeTrans != "")
                            entity.FechaDesdeTransacciones = Convert.ToDateTime(fechaDesdeTrans);
                    }
                    else
                    {
                        entity.FechaDesdeTransacciones = null;
                        entity.ConTransacciones = null;
                    }


                    if (entity.FechaEnvioSolicitada.Year == DateTime.Now.Year
                        && entity.FechaEnvioSolicitada.Month == DateTime.Now.Month
                        && entity.FechaEnvioSolicitada.Day == DateTime.Now.Day)
                    {
                        throw new Exception("La fecha de envio de la campaña no puede ser hoy");
                    }

                    if (id > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.SMS.Add(entity);
                        dbContext.SaveChanges();
                    }

                    idGenerado = entity.IDSMS;
                }
            }

            return idGenerado;
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            throw ex;
        }
    }

    private void CargarMarcas()
    {
        bMarca bMarca = new bMarca();

        this.ddlMarcas.DataSource = bMarca.getMarcas();
        this.ddlMarcas.DataValueField = "IDMarca";
        this.ddlMarcas.DataTextField = "Nombre";
        this.ddlMarcas.DataBind();
        this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));

    }

    private void CargarInfo(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            //Provincias
            var prov = dbContext.SMS.Where(x => x.IDSMS == id).FirstOrDefault();

            CargarProfesiones();
            CargarProvincias();

            var entity = dbContext.SMS.Where(x => x.IDSMS == id).FirstOrDefault();
            if (entity != null)
            {
                hdnTipo.Value = entity.Tipo;
                txtNombre.Text = entity.Nombre;
                #region sexo
                rdbTodos.Checked = false;
                rdbFemenino.Checked = false;
                rdbMasculino.Checked = false;

                switch (entity.Sexo)
                {
                    case "T":
                        rdbTodos.Checked = true;
                        break;
                    case "F":
                        rdbFemenino.Checked = true;
                        break;
                    case "M":
                        rdbMasculino.Checked = true;
                        break;
                }
                #endregion

                #region edad

                if (entity.Edad == "Todas")
                {
                    chk0.Checked = true;
                    chkSegmentado.Checked = false;
                }
                else
                {
                    chk0.Checked = false;
                    chkSegmentado.Checked = true;

                    if (entity.Edad.Contains("0-18"))
                        chk1.Checked = true;
                    if (entity.Edad.Contains("19-25"))
                        chk2.Checked = true;
                    if (entity.Edad.Contains("26-30"))
                        chk3.Checked = true;
                    if (entity.Edad.Contains("31-40"))
                        chk4.Checked = true;
                    if (entity.Edad.Contains("41-50"))
                        chk5.Checked = true;
                    if (entity.Edad.Contains("51-60"))
                        chk6.Checked = true;
                    if (entity.Edad.Contains("61+"))
                        chk7.Checked = true;
                }

                #endregion

                txtMensaje.Text = entity.Mensaje;
                txtNombre.Text = entity.Nombre;
                txtFechaEnvio.Text = entity.FechaEnvioSolicitada.ToString("dd/MM/yyyy");
                txtTelefonos.Text = entity.Telefonos;

                if (entity.ConTransacciones != null)
                    ddlTransacciones.SelectedValue = entity.ConTransacciones.Value ? "con" : "sin";

                if (entity.FechaDesdeTransacciones != null)
                {
                    txtFechaDesde.Text = entity.FechaDesdeTransacciones.Value.ToShortDateString();
                    divFechaTrans.Visible = true;
                }
                else
                {
                    entity.FechaDesdeTransacciones = null;
                    entity.ConTransacciones = null;
                }

                if (entity.IDProvincia != null)
                {
                    ddlProvincias.SelectedValue = entity.IDProvincia.ToString();
                    List<ComboViewModel> listaCiudades = new List<ComboViewModel>();
                    CargarCiudades(entity.IDProvincia.Value);
                }

                if (entity.IDCiudad != null)
                    ddlCiudades.SelectedValue = entity.IDCiudad.ToString();

                if (entity.IDProfesion != null)
                    ddlProfesiones.SelectedValue = entity.IDProfesion.ToString();
                if (entity.IDMarca != null)
                    ddlMarcas.SelectedValue = entity.IDMarca.ToString();

            }
        }
    }

    private void CargarProvincias()
    {
        using (var dbContext = new ACHEEntities())
        {
            //Provincias
            var prov = dbContext.Provincias.OrderBy(x => x.Nombre).ToList();
            this.ddlProvincias.DataSource = prov;
            this.ddlProvincias.DataValueField = "IDProvincia";
            this.ddlProvincias.DataTextField = "Nombre";
            this.ddlProvincias.DataBind();
            this.ddlProvincias.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void CargarProfesiones()
    {
        using (var dbContext = new ACHEEntities())
        {
            //Provincias
            var profesion = dbContext.Profesiones.OrderBy(x => x.Nombre).ToList();
            this.ddlProfesiones.DataSource = profesion;
            this.ddlProfesiones.DataValueField = "IDProfesion";
            this.ddlProfesiones.DataTextField = "Nombre";
            this.ddlProfesiones.DataBind();
            this.ddlProfesiones.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void CargarCiudades(int idProvincia)
    {
        using (var dbContext = new ACHEEntities())
        {
            //Provincias
            var ciudad = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).OrderBy(x => x.Nombre).ToList();
            this.ddlCiudades.DataSource = ciudad;
            this.ddlCiudades.DataValueField = "IDCiudad";
            this.ddlCiudades.DataTextField = "Nombre";
            this.ddlCiudades.DataBind();
            this.ddlCiudades.Items.Insert(0, new ListItem("", ""));
        }
    }

    [WebMethod(true)]
    public static List<ComboViewModel> ciudadesByProvincias(int idProvincia)
    {
        List<ComboViewModel> listaCiudades = new List<ComboViewModel>();
        using (var dbContext = new ACHEEntities())
        {
            listaCiudades = dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).Select(x => new ComboViewModel { ID = x.IDCiudad.ToString(), Nombre = x.Nombre }).OrderBy(x => x.Nombre).ToList();
            ComboViewModel todas = new ComboViewModel();
            todas.ID = "";
            todas.Nombre = "";
            listaCiudades.Insert(0, todas);
        }
        return listaCiudades;
    }

}