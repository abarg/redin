﻿using System;
using ACHE.Extensions;
using ACHE.Business;
using ACHE.Model;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Collections.Specialized;

public partial class modulos_sms_sms : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.SMS.Include("Marcas")
                    .Where(x => x.Estado != "B")
                    .OrderByDescending(x => x.FechaAlta)
                    .Select(x => new SMSAdminViewModel()
                    {
                        ID = x.IDSMS,
                        Marca = x.Marcas.Nombre,
                        Nombre = x.Nombre,
                        Mensaje = x.Mensaje,
                        Estado = x.Estado == "B" ? "Borrador" : (x.Estado == "P" ? "Pendiente" : (x.Estado == "R" ? "Rechazada" : (x.Estado == "E" ? "Enviada" : "Aprobada" ))),
                        FechaAlta = x.FechaAlta,
                        FechaEnvio = x.FechaEnvioSolicitada,
                        UsuarioAlta = x.UsuarioAlta,
                        Enviados = x.SMSEnvios.Count(),
                        Tipo = x.Tipo == "I" ? "Interna" : "Externa",
                    }).ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }
   [WebMethod(true)]
    public static void Delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
              
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.SMS.Where(x => x.IDSMS == id ).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.SMS.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    
    }
}