﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Data.Entity.Infrastructure;

public partial class franquicias_rpt_facturacion_detalle : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            this.cargarMarcas();
        }
    }

    private void cargarMarcas()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(CurrentFranquiciasUser.IDFranquicia);
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("Todas las marcas", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            using (var dbContext = new ACHEEntities())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 300;
                
                var result = dbContext.Rpt_Franq_FacturacionDetalle(DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta), idFranquicia)
                   
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new
                    {
                        Fecha = x.FechaTransaccion,
                        Origen = x.Origen,
                        POSTipo = x.POSTipo,
                        POSSistema = x.POSSistema,
                        POSTerminal = x.POSTerminal,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia.ToUpper(),
                        Tarjeta = x.Tarjeta,
                        NroDocumento = x.NroDocumento,
                        NumEst = x.NumEst,
                        Marca = x.Marca,
                        IDMarca = x.IDMarca,
                        Socio = x.Socio,
                        ImporteOriginal = x.ImporteOriginal,
                        ImporteAhorro = x.ImporteAhorro,
                        ImportePagado = x.ImportePagado,
                        CostoRedIn = x.CostoRedIn,
                        Ticket = x.Ticket,
                        ComisionTtCp = x.ComisionTtCp,
                        ComisionTpCp = x.ComisionTpCp,
                        ComisionTpCt = x.ComisionTpCt
                        //CantOperaciones = x.CantOperaciones
                    });


                return result.ToList().AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList
            }
        }
        else
            return null;
    }

    [WebMethod(true)]
    public static string exportar(string fechaDesde, string fechaHasta, string sds, string nombre, int idMarca, string cant)
    {
        string fileName = "FacturacionDetalle";
        string path = "/tmp/";
        try
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            DataTable dt = new DataTable();
            using (var dbContext = new ACHEEntities())
            {
                ((IObjectContextAdapter)dbContext).ObjectContext.CommandTimeout = 300;
                
                var info = dbContext.Rpt_Franq_FacturacionDetalle(DateTime.Parse(fechaDesde), DateTime.Parse(fechaHasta), idFranquicia)
                    .OrderBy(x => x.NombreFantasia)
                    .Select(x => new
                    {
                        Fecha = x.FechaTransaccion,

                        Origen = x.Origen,
                        POSTipo = x.POSTipo,
                        POSSistema = x.POSSistema,
                        POSTerminal = x.POSTerminal,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        NombreFantasia = x.NombreFantasia.ToUpper(),
                        Tarjeta = x.Tarjeta,
                        NroDocumento = x.NroDocumento,
                        NumEst = x.NumEst,
                        Marca = x.Marca,
                        IDMarca = x.IDMarca,
                        Socio = x.Socio,
                        ImporteOriginal = x.ImporteOriginal,
                        ImporteAhorro = x.ImporteAhorro,
                        ImportePagado = x.ImportePagado,
                        CostoRedIn = x.CostoRedIn,
                        Ticket = x.Ticket,
                        ComisionTtCp = x.ComisionTtCp,
                        ComisionTpCp = x.ComisionTpCp,
                        ComisionTpCt = x.ComisionTpCt
                        //CantOperaciones = x.CantOperaciones
                    });

                if (sds != "")
                    info = info.Where(x => x.SDS.ToLower().Contains(sds.ToLower()));
                if (nombre != "")
                    info = info.Where(x => x.NombreFantasia.ToLower().Contains(nombre.ToLower()));
                if (idMarca > 0)
                    info = info.Where(x => x.IDMarca == idMarca);
                /*if (cant != "")
                    info = info.Where(x => x.CantOperaciones >= int.Parse(cant));*/

                dt = info.Select(x => new
                {
                    Fecha = x.Fecha.ToShortDateString(),
                    Horario = x.Fecha.ToString("HH:MM:ss"),

                    Origen = x.Origen,
                    POSTipo = x.POSTipo,
                    POSSistema = x.POSSistema,
                    POSTerminal = x.POSTerminal,
                    Operacion = x.Operacion,
                    SDS = x.SDS,
                    NombreFantasia = x.NombreFantasia.ToUpper(),
                    Tarjeta = x.Tarjeta,
                    NroDocumento = x.NroDocumento,
                    NumEst = x.NumEst,
                    Marca = x.Marca,
                    Socio = x.Socio,
                    ImporteOriginal = x.ImporteOriginal,
                    ImporteAhorro = x.ImporteAhorro,
                    ImportePagado = x.ImportePagado,
                    CostoRedIn = x.CostoRedIn,
                    Ticket = x.Ticket,
                    ComisionTtCp = x.ComisionTtCp,
                    ComisionTpCp = x.ComisionTpCp,
                    ComisionTpCt = x.ComisionTpCt
                    //CantOperaciones = x.CantOperaciones.HasValue ? x.CantOperaciones.Value : 0
                }).ToList().ToDataTable();

            }

            if (dt.Rows.Count > 0)
            {
                generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            }
            else
            {
                throw new Exception("No se encuentran datos para los filtros seleccionados");
            }

            return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}