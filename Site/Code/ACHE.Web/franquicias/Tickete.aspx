﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="Tickete.aspx.cs" Inherits="franquicias_Tickete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
        <link href="../../lib/chosen/chosen.css" rel="stylesheet" />
    <script src="../../lib/chosen/chosen.jquery.js"></script><link href="../../lib/chosen/chosen.css" rel="stylesheet" />
    <script src="../../js/jquery.maskMoney.min.js"></script>
    <script src="../../js/views/tickets/ticketsenvio.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/franquicias/Ticket.aspx") %>">Listado Tickets</a></li>
            <li class="last">Tickets</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Ticket #<asp:Literal runat="server" ID="litID"></asp:Literal>
                - <asp:Literal runat="server" ID="LitAsunto"></asp:Literal>
                -  <asp:Literal runat="server" ID="litEstado"></asp:Literal>
                   <asp:Literal runat="server" ID="litFechaCierre"></asp:Literal>
            </h3>
            <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">Su mensaje ha sido enviado correctamente.</div></asp:Literal>
            <asp:Literal runat="server" id="litErrorMail" Visible="false"><div class="alert alert-danger alert-dismissable">El mail no se ha enviado correctamente</div></asp:Literal>
            <asp:Literal runat="server" id="litErrorDesc" Visible="false"><div class="alert alert-danger alert-dismissable">El mensaje de descripcion no puede estar vacío</div></asp:Literal>            
            <form runat="server" id="formTicket" class="form-horizontal" role="form">
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Area:
                            <asp:Literal runat="server" ID="litArea"></asp:Literal>                           
                        </h1>
                    </div>
                </div>
                <br>
                <br>
                <asp:ListView runat="server" ID="rptTickets">
                    <ItemTemplate>
                        <div class="col-md-12">
                            <p>De: <%# Eval("Usuario") %></p>

                            <p>Fecha: <%# Eval("Fecha") %></p>
                            <p>Mensaje: <%# Eval("Descripcion") %></p>
                            <br>
                            <%--<a href="/files/tickets/<%# Eval("Adjunto") %>" style="display: <%# Eval("MostrarAdjunto") %>" download>descargar adjunto</a>--%>
                            <%# Eval("Adjunto") %>
                        </div>

                        <br>
                    </ItemTemplate>
                </asp:ListView>

                <div>
                    <br>
                    <div class="col-lg-6">
                        <asp:TextBox runat="server" ID="txtMensaje" TextMode="MultiLine" Rows="3" TabIndex="1" CssClass="form-control"></asp:TextBox>
                        <br>
                        <%--<div>Cargar archivo:<asp:FileUpload runat="server" ID="flpArch" TabIndex="2" /></div>--%>
                        <asp:FileUpload ID="file_upload" runat="server" Text="Elegir Archivo" />
                        <asp:ListView runat="server" ID="rptImagenes">                                        
                                    <ItemTemplate>
                                        <label id='<%# Eval("FileName") %>'><%# Eval("FileName") %></label>
                            </ItemTemplate>
                        </asp:ListView>
                        <br>                       
                        <asp:Button ID="btnFileUpload" runat="server" Text="Cargar Archivo" OnClick="btnFileUpload_Click" />
                        <asp:Label ID="lblUploadStatus" runat="server"></asp:Label>
                        <br /><br />
                        <asp:Button ID="btnDelete" runat="server" Text="Borrar Todas" OnClick="btnFileDelete_Click" />
                        <br> <br>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><span class="f_req"></span> Poner en copia de ticket nuevo</label>
                            <div class="col-lg-9">
					            <div class="fieldwrap">
                                    <asp:DropDownList runat="server" ID="cmbEmails" ClientIDMode="Static" Width="100%"
                                    data-placeholder="Seleccione 1 o más mails" multiple="true" CssClass="chzn-select-deselect"></asp:DropDownList>
                                    <span style="color:red"><asp:Literal runat="server" ID="litDetalleErrores" Text="No se han podido enviar los mails correctamente" Visible="false"></asp:Literal></span>
                                    <asp:HiddenField runat="server" ID="searchable" ClientIDMode="Static" />
					            </div>                        
                            </div>
                        </div>
                        <asp:Button runat="server" ID="btnResponderTicket" class="btn btn-success" type="button" OnClick="ResponderTicket" Text="Responder"  OnClientClick="$('#searchable').val($('#cmbEmails').val());"></asp:Button>
                        &nbsp;<a href="Ticket.aspx">volver</a>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    </div>

</asp:Content>

