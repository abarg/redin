﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Business;
using System.Web.Script.Serialization;



public partial class franquicias_mapa : PaginaFranquiciasBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentFranquiciasUser.Tipo == "B")
                Response.Redirect("home.aspx");
            cargarCombos();
        }
    }
    [WebMethod(true)]
    public static List<ComboViewModel> cargarSubRubros(int IDRubroPadre)
    {

        using (var dbContext = new ACHEEntities())
        {
            List<ComboViewModel> result = new List<ComboViewModel>();

            var subRubros = dbContext.Rubros.Where(x => x.IDRubroPadre == IDRubroPadre).OrderBy(x => x.IDRubro).Select(x => new ComboViewModel()
            {
                ID = x.IDRubro.ToString(),
                Nombre = x.Nombre
            }).ToList();

            if (subRubros != null && subRubros.Count() > 0)
                result = subRubros;

            result.Insert(0, new ComboViewModel() { ID = "", Nombre = "" });

            return result;
        }
    }

    private void cargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {

            try
            {

                bRubros bRubros = new bRubros();
                this.ddlRubro.DataSource = bRubros.getRubros();
                this.ddlRubro.DataValueField = "IDRubro";
                this.ddlRubro.DataTextField = "Nombre";
                this.ddlRubro.DataBind();
                this.ddlRubro.Items.Insert(0, new ListItem("", ""));

                bMarca bMarca = new bMarca();
                List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(CurrentFranquiciasUser.IDFranquicia);
                this.ddlMarcas.DataSource = listMarcas;
                this.ddlMarcas.DataValueField = "IDMarca";
                this.ddlMarcas.DataTextField = "Nombre";
                this.ddlMarcas.DataBind();

                this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }


            try
            {

                var sql = "SELECT * FROM Polygons";


                var polygons = dbContext.Database.SqlQuery<PolygonsView>(sql, new object[] { }).ToList();
                if (polygons != null)
                {
                    this.ddlLocalidades.DataSource = polygons;
                    this.ddlLocalidades.DataBind();
                }


                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio as ID from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio";

                var sedesData = dbContext.Database.SqlQuery<Combo>(sqlSedes).ToList();
                if (sedesData != null)
                {
                    ddlSedes.DataSource = sedesData;
                    ddlSedes.DataTextField = "Nombre";
                    ddlSedes.DataValueField = "ID";
                    ddlSedes.DataBind();
                }


                var sqlAct = "SELECT * FROM ACTIVIDADES";

                var actividades = dbContext.Database.SqlQuery<ActividadesView>(sqlAct, new object[] { }).ToList();
                if (actividades != null)
                {
                    this.ddlActividades.DataSource = actividades;
                    this.ddlActividades.DataBind();
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }


        }
    }



    public class Combo
    {
        public string Nombre { get; set; }
        public int ID { get; set; }
    }

    [WebMethod(true)]
    public static List<PolygonsView> GetPolygons(string Polygons)
    {

        var list = new List<PolygonsView>();

        if (Polygons != "")
        {

            int[] ids;

            ids = Polygons.Split(',').Select(Int32.Parse).ToArray();

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    list = dbContext.Polygons.Where(p => ids.Contains((int)p.IDPolygon)).Select(x => new PolygonsView()
                    {

                        IDPolygon = x.IDPolygon,
                        Nombre = x.Nombre,
                        Paths = x.Paths,
                        StrokeColor = x.StrokeColor,
                        StrokeOpacity = x.StrokeOpacity,
                        StrokeWeight = x.StrokeWeight,
                        FillColor = x.FillColor,
                        FillOpacity = x.FillOpacity

                    }).ToList();
                }


            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        return list;
    }

    [WebMethod(true)]
    public static List<MarkersViewModel> GetMarkers(string tipo, int rubro, int subRubro, string edad, string actividades, string sedes)
    {
        var list = new List<MarkersViewModel>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (tipo == "M")
                    {
                        var preCom = dbContext.Comercios.Include("Domicilios").Include("Rubros")
                            .Where(x => x.IDFranquicia == idFranquicia && x.Domicilios != null
                                && x.Domicilios.Latitud != string.Empty && x.Domicilios.Longitud != string.Empty
                                && x.Domicilios.Latitud != null
                                && x.Domicilios.Longitud != null).AsQueryable();
                        if (rubro > 0)
                            preCom = preCom.Where(x => x.Rubro == rubro);
                        if (subRubro > 0)
                            preCom = preCom.Where(x => x.IDSubRubro == subRubro);

                        var comercios = preCom.ToList();
                        if (comercios.Any())
                        {
                            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                            string fechaDesdeMensual = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                            string fechaDesdeAnual = DateTime.Now.AddYears(-1).GetFirstDayOfMonth().ToString(formato);
                            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                            var trMensual = string.Empty;
                            var trAnual = string.Empty;
                            var promMensual = string.Empty;
                            var promAnual = string.Empty;

                            foreach (var com in comercios)
                            {
                                var marker = new MarkersViewModel();
                                marker.Nombre = com.NombreFantasia;
                                marker.Direccion = com.Domicilios.Domicilio;
                                marker.Lat = com.Domicilios.Latitud;
                                marker.Lng = com.Domicilios.Longitud;
                                marker.Categoria = com.Rubro.HasValue ? com.Rubro.Value.ToString() : "0";
                                //marker.NombreRubro = marker.IDRubro == 0 ? "" : com.Rubros.Nombre.RemoverAcentos().Replace(" ", "");
                                marker.Icono = com.Rubro.HasValue ? com.Rubros.Icono : "";

                                //Cant TR
                                var totalMensual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + com.IDComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (totalMensual.Any())
                                    trMensual = totalMensual[0].data.ToString();
                                else
                                    trMensual = "0";

                                var totalAnual = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Comercios_TotalTR " + com.IDComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (totalAnual.Any())
                                    trAnual = totalAnual[0].data.ToString();
                                else
                                    trAnual = "0";

                                //Promedio ticket
                                var promedioMensual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + com.IDComercio + ",'" + fechaDesdeMensual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (promedioMensual.Any())
                                    promMensual = Math.Abs(promedioMensual[0].data).ToString("N2");
                                else
                                    promMensual = "0";

                                var promedioAnual = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Comercios_PromedioTicket " + com.IDComercio + ",'" + fechaDesdeAnual + "','" + fechaHasta + "'", new object[] { }).ToList();
                                if (promedioAnual.Any())
                                    promAnual = Math.Abs(promedioAnual[0].data).ToString("N2");
                                else
                                    promAnual = "0";

                                marker.Contenido = "<p><b>Cant TR mes actual:</b> " + trMensual + "<br><b>Cant TR anual:</b> " + trAnual + "<br><b>Prom ticket mes actual:</b> $ " + promMensual + "<br><b>Prom ticket anual:</b> $ " + promAnual + "</p>";
                                marker.Ficha = "/common/Comerciose.aspx?IDComercio=" + com.IDComercio;

                                if (!string.IsNullOrEmpty(com.Logo))
                                    marker.Foto = "/files/logos/" + com.Logo;
                                else
                                    marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                                list.Add(marker);
                            }
                        }
                    }
                    else
                    {


                        int edadDesde = int.Parse(edad.Split("-")[0]);
                        int edadHasta = int.Parse(edad.Split("-")[1]);

                        if (actividades != "" || sedes != "")
                        {

                            //var tarjetasAct = dbContext.Tarjetas.Include("Socios").Where(x => actividadSocioNivel.Contains((int)x.Socios.IDSocio)
                            // && x.Socios.Edad >= edadDesde && x.Socios.Edad <= edadHasta
                            // && x.Socios.Domicilios != null
                            // //&& x.Socios.Domicilios.Latitud != string.Empty
                            // //&& x.Socios.Domicilios.Latitud != null
                            // && x.Socios.Domicilios.Longitud != null).ToList();

                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "before sqlSocios", DateTime.Now.ToString("hh.mm.ss.ffffff"));


                            string sqlSocios = @"SELECT S.IDSocio, S.Nombre, S.Apellido, S.NroDocumento, D.Latitud, D.Longitud, D.Domicilio, T.Credito, S.Email, S.AuthID, S.Sexo, Actividades = STUFF((SELECT N', ' + A.Actividad
                                                from ActividadSocioNivel as ASN
	                                            join Actividades as A on A.IDActividad = ASN.IDActividad
	                                            where ASN.IDSocio = S.IDSocio
                                                FOR XML PATH(N'')), 1, 2, N'')
                                                from Tarjetas as T
	                                            join Socios as S on T.IDSocio = S.IDSocio
	                                            join Domicilios as D on S.IDDomicilio = D.IDDomicilio
	                                            join ActividadSocioNivel as ASN on ASN.IDSocio = S.IDSocio
                                                join ActividadComercio as AC on AC.IDActividad = ASN.IDActividad
                                                join Comercios as C on AC.IDComercio = C.IDComercio
                                                where D.Latitud <> ''
	                                            and D.Longitud <> ''";


                            if (actividades != "") {


                                var actividadesIds = actividades.Split(',');

                                sqlSocios +=  " and ASN.IDActividad IN (";

                                                    for (int i = 0; i < actividadesIds.Length; i++)
                                                    {
                                                        if (i != actividadesIds.Length - 1)
                                                            sqlSocios += "'" + actividadesIds[i] + "',";
                                                        else
                                                            sqlSocios += "'" + actividadesIds[i] + "'";

                                                    }

                                sqlSocios += ")";

                            }

                            if (sedes != "") {


                                var sedesIds = sedes.Split(',');

                                sqlSocios += " and C.IDComercio IN (";

                                for (int i = 0; i < sedesIds.Length; i++)
                                {
                                    if (i != sedesIds.Length - 1)
                                        sqlSocios += "'" + sedesIds[i] + "',";
                                    else
                                        sqlSocios += "'" + sedesIds[i] + "'";

                                }

                                sqlSocios += ")";

                            }


                            sqlSocios += " group by S.IDSocio, S.Nombre, S.Apellido, S.NroDocumento, D.Latitud, D.Longitud, D.Domicilio, T.Credito, S.Email, S.AuthID, S.Sexo";



                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le sql is", sqlSocios);


                            var sociosAgrupados = dbContext.Database.SqlQuery<SociosViewModel>(sqlSocios, new object[] { }).ToList();



                            if (sociosAgrupados.Any())
                            {


                                foreach (var soc in sociosAgrupados)
                                {   

                                    var marker = new MarkersViewModel();
                                    marker.Nombre = soc.Apellido + ", " + soc.Nombre;
                                    marker.Direccion = soc.Domicilio;
                                    marker.Lat = soc.Latitud;
                                    marker.Lng = soc.Longitud;
                                    marker.Contenido = "<p><b>DNI:</b> " + soc.NroDocumento + "<br><b>Email:</b> " + soc.Email + "<br><b>Credito:</b> $ " + soc.Credito.ToString("N2") + "<br><b>Beneficios Asignados:</b> " + soc.Actividades + "</p>";
                                    marker.Ficha = "/common/Sociose.aspx?IDSocio=" + soc.IDSocio;

                                    marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                                    if (soc.Sexo == "F")
                                    {
                                        marker.Icono = "femenino.png";
                                        marker.Categoria = "femenino";
                                    }
                                    else if (soc.Sexo == "M")
                                    {
                                        marker.Icono = "masculino.png";
                                        marker.Categoria = "masculino";
                                    }
                                    else
                                    {
                                        marker.Icono = "indefinido.png";
                                        marker.Categoria = "indefinido";
                                    }

                                    list.Add(marker);


                                }
                            }


                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "after foreach is", DateTime.Now.ToString("hh.mm.ss.ffffff"));


                            return list;
                        }


                        var preTar = dbContext.Tarjetas.Include("Socios")
                           .Where(x => x.PuntosTotales > 1
                               && x.IDFranquicia == idFranquicia
                               && x.Socios.Edad >= edadDesde && x.Socios.Edad <= edadHasta
                               && x.Socios.Domicilios != null && x.Socios.Domicilios.Latitud != string.Empty
                               && x.Socios.Domicilios.Latitud != null
                               && x.Socios.Domicilios.Longitud != null
                               && x.Socios.Domicilios.Longitud != string.Empty).AsQueryable();


                        var tarjetas = preTar.ToList();
                        if (tarjetas.Any())
                        {
                            foreach (var soc in tarjetas)
                            {
                                var marker = new MarkersViewModel();
                                marker.Nombre = soc.Socios.Apellido + ", " + soc.Socios.Nombre;
                                marker.Direccion = soc.Socios.Domicilios.Domicilio;
                                marker.Lat = soc.Socios.Domicilios.Latitud;
                                marker.Lng = soc.Socios.Domicilios.Longitud;
                                marker.Contenido = "<p><b>DNI:</b> " + soc.Socios.NroDocumento + "<br><b>Email:</b> " + soc.Socios.Email + "<br><b>Credito:</b> $ " + soc.Credito.ToString("N2") + "<br><b>Giftcard:</b> $ " + soc.Giftcard.ToString("N2") + "<br><b>POS:</b> " + Math.Round(soc.Credito + soc.Giftcard) + "</p>";
                                marker.Ficha = "/common/Sociose.aspx?IDSocio=" + soc.IDSocio;
                                if (soc.Socios.AuthTipo == "FB")
                                    marker.Foto = string.Format("https://graph.facebook.com/{0}/picture?type=normal", soc.Socios.AuthID);
                                else if (!string.IsNullOrEmpty(soc.Socios.Foto))
                                    marker.Foto = "/files/socios/" + soc.Socios.Foto;
                                else
                                    marker.Foto = "http://www.placehold.it/150x150&text=sin foto";

                                if (soc.Socios.Sexo == "F")
                                {
                                    marker.Icono = "femenino.png";
                                    marker.Categoria = "femenino";
                                }
                                else if (soc.Socios.Sexo == "M")
                                {
                                    marker.Icono = "masculino.png";
                                    marker.Categoria = "masculino";
                                }
                                else
                                {
                                    marker.Icono = "indefinido.png";
                                    marker.Categoria = "indefinido";
                                }
                                list.Add(marker);


                                if (actividades != "")
                                {

                                }


                            }
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return list;
    }



    [WebMethod(true)]
    public static List<ComboViewModel> sedesPorLocalidades(string localidades)
    {

        List<ComboViewModel> listSedes = new List<ComboViewModel>();

        if (localidades != string.Empty)
        {


            var splitted = localidades.Split(",");


            using (var dbContext = new ACHEEntities())
            {



                var sql = @"select NombreFantasia as Nombre from comercios AS C
                            join Domicilios AS D on C.IDDomicilio = D.IDDomicilio
                            join Ciudades AS CI ON CI.IDCiudad = D.Ciudad
                            where IDFranquicia = 43 and CI.Nombre IN ( ";

                for (int i = 0; i < splitted.Length; i++)
                {
                    if (i != splitted.Length - 1)
                        sql += "'" + splitted[i] + "',";
                    else
                        sql += "'" + splitted[i] + "'";

                }



                sql += ");";



                listSedes = dbContext.Database.SqlQuery<ComboViewModel>(sql).ToList();


            }

        }
        else
        {
            using (var dbContext = new ACHEEntities())
            {

                string sqlSedes = @"select DISTINCT C.NombreFantasia as Nombre, C.IDComercio from ActividadSocioNivel ASN
                                    join ActividadComercio AC on AC.IDActividad = ASN.IDActividad
                                    join Comercios C on C.IDComercio = AC.IDComercio";




                listSedes = dbContext.Database.SqlQuery<ComboViewModel>(sqlSedes).ToList();


            }
        }

        return listSedes;
    }


    [WebMethod(true)]
    public static List<Combo2ViewModel> actividadesPorSedes(string sedes)
    {

        List<Combo2ViewModel> listActividades = new List<Combo2ViewModel>();

        if (sedes != string.Empty)
        {


            var splitted = sedes.Split(",");


            using (var dbContext = new ACHEEntities())
            {



                var sql = @"select distinct A.Actividad as Nombre, A.IDActividad as ID from ActividadComercio AS AC
                            join Comercios as C on C.IDComercio = AC.IDComercio
							join Actividades as A on A.IDActividad = AC.IDActividad
							where C.IDComercio IN ( ";

                for (int i = 0; i < splitted.Length; i++)
                {
                    if (i != splitted.Length - 1)
                        sql += "'" + splitted[i] + "',";
                    else
                        sql += "'" + splitted[i] + "'";

                }



                sql += ");";



                listActividades = dbContext.Database.SqlQuery<Combo2ViewModel>(sql).ToList();


            }

        }
        else
        {

            using (var dbContext = new ACHEEntities())
                listActividades = dbContext.Actividades.OrderBy(x => x.Actividad).Select(x => new Combo2ViewModel { ID = x.IDActividad, Nombre = x.Actividad.ToUpper() }).ToList();

        }

        return listActividades;
    }
}
