﻿<%@ WebHandler Language="C#" Class="Telerik.Web.Examples.FileExplorer.FilterAndDownloadFiles.Handler" %>

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Text;

namespace Telerik.Web.Examples.FileExplorer.FilterAndDownloadFiles {
	//[RadCompressionSettings(HttpCompression = CompressionType.None)] // Disable RadCompression for this page ;
	public class Handler : IHttpHandler {
		#region IHttpHandler Members

		private HttpContext _context;
		private HttpContext Context {
			get {
				return _context;
			}
			set {
				_context = value;
			}
		}


		public void ProcessRequest(HttpContext context) {
			Context = context;
			string filePath = context.Request.QueryString["path"];
			filePath = context.Server.MapPath(filePath);

			if (filePath == null) {
				return;
			}

			System.IO.StreamReader streamReader = new System.IO.StreamReader(filePath);
			System.IO.BinaryReader br = new System.IO.BinaryReader(streamReader.BaseStream);

			byte[] bytes = new byte[streamReader.BaseStream.Length];

			br.Read(bytes, 0, (int)streamReader.BaseStream.Length);

			if (bytes == null) {
				return;
			}

			streamReader.Close();
			br.Close();
			string extension = System.IO.Path.GetExtension(filePath);
			string fileName = System.IO.Path.GetFileName(filePath);
			//string fileName = System.IO.Path.GetFileName(filePath).con;

			//if (extension == ".jpg")
			//{ // Handle *.jpg and

			//}
			//else if (extension == ".gif")
			//{// Handle *.gif
			WriteFile(bytes, fileName, extension, context.Response);
			//}

		}

		/// <summary>
		/// Sends a byte array to the client
		/// </summary>
		/// <param name="content">binary file content</param>
		/// <param name="fileName">the filename to be sent to the client</param>
		/// <param name="contentType">the file content type</param>
		private void WriteFile(byte[] content, string fileName, string extension, HttpResponse response) {
			response.Buffer = true;
			response.Clear();
			response.ContentType = GetContentType(extension);

			//response.AddHeader("content-disposition", "inline; filename=" + fileName);
			response.AppendHeader("Content-disposition", "attachment; filename=" + fileName);

			response.BinaryWrite(content);
			response.Flush();
			response.End();
		}

		private string GetContentType(string extension) {
			string contentType = "";

			if (extension.ToLower() == ".docx" || extension.ToLower() == ".doc")
				contentType = "application/msword";
			else if (extension.ToLower() == ".xls")
				contentType = "application/vnd.ms-excel";
			else {

				Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(extension);

				if (registryKey != null && registryKey.GetValue("Content Type") != null)

					contentType = registryKey.GetValue("Content Type").ToString();
			}
			return contentType;
		}



		public bool IsReusable {
			get {
				return false;
			}
		}

		#endregion
	}
}	