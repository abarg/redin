﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using ClosedXML.Excel;
using ACHE.Business;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.IO;


public partial class franquicias_Ticket : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e) {
        eliminarArchTemp();
        if (!IsPostBack) {
            cargarCombos();
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            if (Request.QueryString["Envio"] == "false") {
                litErrorMail.Visible = true;
            }
            if (Request.QueryString["Envio"] == "true" || Request.QueryString["Envio"] == "false")
                litOk.Visible = true;
        }
    }


    [System.Web.Services.WebMethod]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta) {
        int idFranquicia = 0;
        string nombreFranquicia = "";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            idFranquicia = usu.IDFranquicia;
            nombreFranquicia = usu.Franquicia;

        }
        if (idFranquicia > 0 ) {            
            using (var dbContext = new ACHEEntities()) {
                var result = dbContext.Tickets
                    .Where(x => x.IDFranquicia != null && x.IDFranquicia == idFranquicia) 
                    .OrderByDescending(x => x.IDTicket)
                    .Select(x => new  {
                        IDTicket = x.IDTicket,
                        Franquicia = x.Franquicias.NombreFantasia,
                        FechaAlta = x.FechaAlta,
                        Area = x.TicketsArea.Nombre,
                        Estado = x.Estado,
                        Asunto = x.Asunto,
                        FechaCierre = x.FechaCierre != null ? x.FechaCierre.ToString(): "",
                        Usuario = x.UsuarioAlta,
                        Prioridad = x.Prioridad,
                        IDUsuarioAlta = x.IDUsuarioAlta,
                        IDFranquicia = idFranquicia,
                        IDAreaTicket = x.IDArea
                    });

                 if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaAlta >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaAlta <= dtHasta);
                }
                   return result.ToDataSourceResult(take, skip, sort, filter);
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod]
    public static string Exportar(string FechaDesde, string FechaHasta, string IDTicket, string Estado, string Prioridad, string Asunto, int IDAreaTicket, string Usuario) {

        string fileName = "Tickets";
        string path = "/tmp/";
        DateTime dtDsd = Convert.ToDateTime(FechaDesde);
        DateTime dtHst = Convert.ToDateTime(FechaHasta);
        dtHst = dtHst.AddDays(1);

        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usuarioFran = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    
                    var info = dbContext.Tickets
                        .Where(x => x.FechaAlta >= dtDsd && x.FechaAlta <= dtHst && x.IDFranquicia == usuarioFran.IDFranquicia)
                        .OrderByDescending(x => x.IDTicket).Select(x => new {
                        IDTicket = x.IDTicket,
                        Usuario = x.UsuarioAlta,
                        Asunto = x.Asunto,
                        Estado = x.Estado,
                        FechaAlta = x.FechaAlta,
                        FechaCierre = x.FechaCierre,
                        Area = x.TicketsArea.Nombre,
                        Prioridad = x.Prioridad,
                        IDAreaTicket = x.IDArea
                    });

                    if (IDTicket != "")
                        info = info.Where(x => x.IDTicket != null && x.IDTicket.ToString().ToLower().Equals(IDTicket.ToLower()));
                    if (Estado != "")
                        info = info.Where(x => x.Estado != null && x.Estado.ToLower().Equals(Estado.ToLower()));
                    if (Prioridad != "")
                        info = info.Where(x => x.Prioridad != null && x.Prioridad.ToLower().Equals(Prioridad.ToLower()));

                    if (Asunto != "")
                        info = info.Where(x => x.Asunto != null && x.Asunto.ToLower().Contains(Asunto.ToLower()));

                    if (IDAreaTicket > 0)
                        info = info.Where(x => x.IDAreaTicket == IDAreaTicket);
                    if (Usuario != "")
                        info = info.Where(x => x.Usuario != null && x.Usuario.ToLower().Contains(Usuario.ToLower()));
                   

                    dt = info.ToList().Select(x => new {
                        Ticket = x.IDTicket,
                        Asunto = x.Asunto,
                        Usuario = x.Usuario,
                        FechaAlta = x.FechaAlta.ToString("dd/MM/yyyy"),
                        FechaCierre = x.FechaCierre.HasValue ? x.FechaCierre.Value.ToString("dd/MM/yyyy") : "",
                        Estado = x.Estado,
                        Area = x.Area,
                        Prioridad = x.Prioridad
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0) {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void generarArchivo(DataTable dt, string ruta, string nombre) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    protected void eliminarArchTemp() {
        string pathString = "~/files/tickets/temp";
        string path = Server.MapPath(pathString);
        if (System.IO.Directory.Exists(path)) {
            System.IO.Directory.Delete(path, true);
        }
    }

    private void cargarCombos() {
        try {
            using (var dbContext = new ACHEEntities()) {
                List<TicketsArea> listaAreas = dbContext.TicketsArea.Where(x => x.Activa).ToList();
                this.cmbAreas.DataSource = listaAreas;
                this.cmbAreas.DataValueField = "IDAreaTicket";
                this.cmbAreas.DataTextField = "Nombre";
                this.cmbAreas.DataBind();
                this.cmbAreas.Items.Insert(0, new ListItem("Todas", ""));

            }

        }
        catch (Exception ex) {
            throw ex;
        }
    }

}