﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="franquicias_home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <input type="hidden" id="hdnTipo" />
        <div class="col-sm-12 col-md-12">
            <%--<h1 class="heading">Bienvenido</h1>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>--%>

		    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div align="center">
                    <br /><br />
                    <h1 class="heading" style="border-bottom: 0px">Bienvenido <asp:Literal runat="server" ID="litNombre"></asp:Literal></h1>
                    <br />
                    <%--<asp:Image runat="server" ID="imgLogo" style="max-width: 400px" />--%>
                    <br />
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">Comercios</div>
                <div class="w-box-content">
                    <table class="table table-striped table_vam no-th">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="pnlComercioError" Visible="false">
                                        Se han detectado
                                        <asp:Literal runat="server" ID="litComercio"></asp:Literal>
                                        comercios con informacion incompleta.
                                        <br />
                                        <a href="javascript:verDetalleComercios();">Ver detalle</a>
                                        <br />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlComercioOk" Visible="false">
                                        No se han detectado alertas<br />
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
        
        <div class="col-sm-6 col-md-6 dd_column">
            <div class="w-box">
                <div class="w-box-header">Comercios sin transacciones</div>
                <div class="w-box-content">
                    <table class="table table-striped table_vam no-th">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="pnlSinTransacciones" Visible="false">
                                        Se han detectado
                                        <asp:Literal runat="server" ID="litSinTransacciones"></asp:Literal>
                                        comercios sin transacciones.
                                        <br />
                                        <a href="javascript:verDetalleComercios('sinTrans');">Ver detalle</a>
                                        <br />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlSinTransaccionesOk" Visible="false">
                                        No se han detectado alertas<br />
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetalle">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="titDetalle"></h3>
                </div>
                <div class="modal-body">
                    <!--div class="alert alert-info">In this table jquery plugin turns a table row into a clickable link.</!--div-->
                    <table class="table table-condensed table-striped" data-provides="rowlink" id="tableDetalle">
                        <thead id="headDetalle">
                            <tr>
                                <th>Comercio</th> 
                               <!-- <th>Establecimiento</th> -->
                                <th>Marca</th> 
                                <th>Pendientes</th> 
                            </tr>
                        </thead>
                        <tbody id="bodyDetalle">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                    <a href="" id="lnkDownload" download="Comercios" style="display:none">Descargar</a>
                    <button type="button" class="btn btn-default" onclick="$('#modalDetalle').modal('hide');">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
	<script src="<%= ResolveUrl("~/lib/datatables/jquery.dataTables.bootstrap.min.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/home.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>