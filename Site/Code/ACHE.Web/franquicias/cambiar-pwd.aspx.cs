﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class franquicias_cambiar_pwd : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            litUsuario.Text = CurrentFranquiciasUser.Usuario;
    }

    [WebMethod(true)]
    public static void grabar(string pwdActual, string pwd)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var user = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];

            using (var dbContext = new ACHEEntities())
            {
                UsuariosFranquicias entity = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                if (entity.Pwd != pwdActual)
                    throw new Exception("La contraseña actual ingresada es inválida");

                entity.Pwd = pwd;
                dbContext.SaveChanges();
            }

            HttpContext.Current.Session["CurrentFranquiciasUser"] = user;
        }
    }
}