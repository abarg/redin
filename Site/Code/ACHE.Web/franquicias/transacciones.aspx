﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="transacciones.aspx.cs" Inherits="franquicias_transacciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   <%-- <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/transacciones.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Transacciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Listado de Transacciones</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. Tarjeta</label>
                            <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2">
                            <label>Nro. Doc. Socio</label>
						    <input type="text" id="txtDocumento" value="" maxlength="20" class="form-control number" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Origen</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlOrigen">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Visa" Value="Visa"></asp:ListItem>
                                <asp:ListItem Text="Web" Value="Web"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Operación</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlOperacion">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Venta" Value="Venta"></asp:ListItem>
                                <asp:ListItem Text="Anulacion" Value="Anulacion"></asp:ListItem>
                                <asp:ListItem Text="Canje" Value="Canje"></asp:ListItem>
                                <asp:ListItem Text="Carga" Value="Carga"></asp:ListItem>
                                <asp:ListItem Text="Descarga" Value="Descarga"></asp:ListItem>
                                <asp:ListItem Text="CuponIn" Value="CuponIn"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlMarcas" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Transacciones" style="display:none">Descargar</a>
                            <%--<button class="btn" type="button" id="btnNuevo" onclick="NuevaTr();">Nuevo</button>--%>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>

    <%--<div class="modal fade" id="modalTransacciones">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title">Alta de Transacción</h3>
				</div>
				<div class="modal-body">
                    <div class="alert alert-danger alert-dismissable" id="divErrorTr" style="display: none"></div>
					<div class="col-sm-12">
				        <form id="formTr" role="form" >
                            <div class="formSep" style="border-bottom:0px">
				                <div class="row">
					                 <div class="col-sm-4">
						                <label><span class="f_req">*</span> Tipo</label>
						                <select name="ddlTrTipo" id="ddlTrTipo" class="form-control required">
                                            <option value="Venta">Venta</option>
                                            <option value="Anulacion">Anulación</option>
                                        </select>                                         
					                </div>
                                    <div class="col-sm-4">
						                <label><span class="f_req">*</span> Tarjeta</label>
						                <input type="text" id="txtTrTarjeta" class="form-control required" maxlength="20" />
					                </div>
				                </div>
			                    <div class="row">
                                    <div class="col-sm-12">
						                <label><span class="f_req">*</span> Comercio</label>
                                        <select ID="ddlTrComercio" class="form-control chzn_b" data-placeholder="Seleccione un comercio" style="width:100% important!">
                                            <option value=""></option>
                                        </select>
					                </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label><span class="f_req">*</span> Importe</label>
						                <input type="text" id="txtTrImporte" class="form-control number required" />
					                </div>
                                    <div class="col-sm-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
						                <label>Descripción</label>
						                <input type="text" id="txtTrDescripcion" class="form-control" maxlength="100" />
					                </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div style="clear:both"></div>
				</div>
				<div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-success" onclick="SaveTr();">Aceptar</a>
                    <a href="javascript:void(0)" onclick="$('#modalTransacciones').modal('hide');" class="btn">Cancelar</a>
                </div>
			</div>
		</div>
	</div>--%>
</asp:Content>
