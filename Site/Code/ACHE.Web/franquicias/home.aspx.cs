﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model;
using System.Data;
using ClosedXML.Excel;
using System.IO;

public partial class franquicias_home : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litNombre.Text = CurrentFranquiciasUser.Franquicia;

        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                int comercio = dbContext.Database.SqlQuery<int>("select count(distinct c.IDComercio) from Comercios c  inner join Domicilios d on c.IDDomicilio = d.IDDomicilio  inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio  left join Contactos ct on c.IDContacto = ct.IDContacto where c.IDFranquicia=" + CurrentFranquiciasUser.IDFranquicia + " and (d.Ciudad = ''  or d.Domicilio = ''  or IDDomicilioFiscal is null  or d2.Ciudad = ''  or d2.Domicilio = ''  or c.FormaPago_CBU = ''  or ct.Email = ''  or ct.Telefono = ''  or c.FechaAlta = '') and  (c.Activo = 1)", new object[] { }).First();
                if (comercio > 0)
                {
                    litComercio.Text = "<strong class='act-danger'>" + comercio + "</strong>";
                    pnlComercioError.Visible = true;
                }
                else
                {
                    litComercio.Text = "0";
                    pnlComercioOk.Visible = true;
                }

                int sinTransacciones = dbContext.Database.SqlQuery<int>("select count(*) from (select c.IDComercio from comercios c join terminales ter on ter.IDComercio = c.IDComercio  full join Transacciones tr on tr.IDComercio=c.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia   join Marcas m on m.IDMarca = c.IDMarca where (ter.Estado <>  ALL( SELECT 'DE')) and f.IDFranquicia =" + CurrentFranquiciasUser.IDFranquicia + "  group by c.IDComercio, c.NombreFantasia, m.Nombre Having count(tr.IDTransaccion) < 1 ) as CantComSinTrans", new object[] { }).First();
                if (sinTransacciones > 0)
                {
                    litSinTransacciones.Text = "<strong class='act-danger'>" + sinTransacciones + "</strong>";
                    pnlSinTransacciones.Visible = true;
                }
                else
                {
                    litSinTransacciones.Text = "0";
                    pnlSinTransaccionesOk.Visible = true;
                }
            }
        }
    }

    [WebMethod(true)]
    public static List<DetalleAlerta> obtenerDetalleComercio(string tipo)
    {
        var html = string.Empty;
        var camposPendientes = string.Empty;
        List<DetalleAlerta> list = new List<DetalleAlerta>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities())
            {
                string sql;
                if (tipo == "sinTrans")
                    sql = "  select c.IDComercio,c.NombreFantasia as NombreComercio, isnull(m.Nombre,'') as Marca  from comercios c join terminales ter on ter.IDComercio = c.IDComercio  full join Transacciones tr on tr.IDComercio=c.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia join Marcas m on m.IDMarca = c.IDMarca where (ter.Estado <>  ALL( SELECT 'DE')) and f.IDFranquicia ='"+idFranquicia+"' group by c.IDComercio, c.NombreFantasia,m.Nombre Having count(tr.IDTransaccion) < 1  ";
                else
                    sql = "select distinct c.IDComercio, c.NombreFantasia as 'NombreComercio', isnull(m.Nombre,'') as 'Marca',    cid2.Nombre as Ciudad, d.Domicilio,  c.IDDomicilioFiscal, isnull(cid.Nombre,'') as 'CiudadFiscal', d2.Domicilio as 'DomicilioFiscal',    c.FormaPago_CBU, ct.Email, ct.Telefono,  c.FechaAlta from   Comercios c    left join Marcas m on c.IDMarca = m.IDMarca  inner join Domicilios d on c.IDDomicilio = d.IDDomicilio       inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio   	   left join Ciudades cid on cid.IDCiudad=d2.Ciudad left join Ciudades cid2 on d.Ciudad=cid2.IDCiudad left join Contactos ct on c.IDContacto = ct.IDContacto where c.IDFranquicia="+idFranquicia+" and (d.Ciudad = '' or d.Domicilio = '' or IDDomicilioFiscal is  null or d2.Ciudad = '' or d2.Domicilio = ''		  or c.FormaPago_CBU = '' or ct.Email = ''   or ct.Telefono = '' or c.FechaAlta = '') and (c.Activo = 1)   order by c.NombreFantasia";

                list = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                if (list.Any())
                {

                    foreach (var detalle in list)
                    {
                        if (tipo != "sinTrans")
                        {
                            if (string.IsNullOrEmpty(detalle.Ciudad))
                                camposPendientes += "Ciudad";
                            if (string.IsNullOrEmpty(detalle.Domicilio))
                                camposPendientes += ", Domicilio";
                            if (string.IsNullOrEmpty(detalle.CiudadFiscal))
                                camposPendientes += ", Ciudad Fiscal";
                            if (string.IsNullOrEmpty(detalle.DomicilioFiscal))
                                camposPendientes += ", Domicilio Fiscal";
                            if (string.IsNullOrEmpty(detalle.FormaPago_CBU))
                                camposPendientes += ", Forma de Pago(CBU)";
                            if (string.IsNullOrEmpty(detalle.Email))
                                camposPendientes += ", Email";
                            if (string.IsNullOrEmpty(detalle.Telefono))
                                camposPendientes += ", Telefono";
                            //if (string.IsNullOrEmpty(detalle.POSTerminal) && detalle.POSTipo != "Web")
                              //  camposPendientes += ", Terminal POS";
                            if (string.IsNullOrEmpty(detalle.FichaAlta))
                                camposPendientes += ", Ficha Alta";

                            if (camposPendientes.Length > 2 && camposPendientes.Substring(0, 2) == ", ")
                                camposPendientes = camposPendientes.Substring(2, camposPendientes.Length - 2);
                            detalle.Pendientes = camposPendientes;
                        }
                        detalle.NombreComercio = "<a href='/Common/Comerciose.aspx?IDComercio=" + detalle.IDComercio + "'>" + detalle.NombreComercio + "</a>";

                        camposPendientes = string.Empty;
                    }

                }
                //else
                //    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
        }

        return list;
    }

    [WebMethod(true)]
    public static string Exportar(string tipo)
    {
        string fileName = "InfoComercios";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    string sql;
                    if (tipo == "sinTrans")
                        sql = "  select c.IDComercio,c.NombreFantasia as NombreComercio, isnull(m.Nombre,'') as Marca  from comercios c join terminales ter on ter.IDComercio = c.IDComercio  full join Transacciones tr on tr.IDComercio=c.IDComercio join Franquicias f on f.IDFranquicia = c.IDFranquicia join Marcas m on m.IDMarca = c.IDMarca where (ter.Estado <>  ALL( SELECT 'DE')) and f.IDFranquicia ='" + idFranquicia + "' group by c.IDComercio, c.NombreFantasia,m.Nombre Having count(tr.IDTransaccion) < 1  ";
                    else
                        sql = "select distinct c.IDComercio, c.NombreFantasia as 'NombreComercio', isnull(m.Nombre,'') as 'Marca',    cid2.Nombre as Ciudad, d.Domicilio,  c.IDDomicilioFiscal, isnull(cid.Nombre,'') as 'CiudadFiscal', d2.Domicilio as 'DomicilioFiscal',    c.FormaPago_CBU, ct.Email, ct.Telefono,  c.FechaAlta from   Comercios c    left join Marcas m on c.IDMarca = m.IDMarca  inner join Domicilios d on c.IDDomicilio = d.IDDomicilio       inner join Domicilios d2 on c.IDDomicilioFiscal = d2.IDDomicilio   	   left join Ciudades cid on cid.IDCiudad=d2.Ciudad left join Ciudades cid2 on d.Ciudad=cid2.IDCiudad left join Contactos ct on c.IDContacto = ct.IDContacto where c.IDFranquicia=" + idFranquicia + " and (d.Ciudad = '' or d.Domicilio = '' or IDDomicilioFiscal is  null or d2.Ciudad = '' or d2.Domicilio = ''		  or c.FormaPago_CBU = '' or ct.Email = ''   or ct.Telefono = '' or c.FechaAlta = '') and (c.Activo = 1)   order by c.NombreFantasia";

                    var info = dbContext.Database.SqlQuery<DetalleAlerta>(sql, new object[] { }).ToList();
                    if (info.Any())
                    {
                        var camposPendientes = string.Empty;
                        foreach (var detalle in info)
                        {
                            if (string.IsNullOrEmpty(detalle.Ciudad))
                                camposPendientes += "Ciudad";
                            if (string.IsNullOrEmpty(detalle.Domicilio))
                                camposPendientes += ", Domicilio";
                            if (string.IsNullOrEmpty(detalle.CiudadFiscal))
                                camposPendientes += ", Ciudad Fiscal";
                            if (string.IsNullOrEmpty(detalle.DomicilioFiscal))
                                camposPendientes += ", Domicilio Fiscal";
                            if (string.IsNullOrEmpty(detalle.FormaPago_CBU))
                                camposPendientes += ", Forma de Pago(CBU)";
                            if (string.IsNullOrEmpty(detalle.Email))
                                camposPendientes += ", Email";
                            if (string.IsNullOrEmpty(detalle.Telefono))
                                camposPendientes += ", Telefono";
                            if (string.IsNullOrEmpty(detalle.POSTerminal) && detalle.POSTipo != "Web")
                                camposPendientes += ", Terminal POS";
                            if (string.IsNullOrEmpty(detalle.FichaAlta))
                                camposPendientes += ", Ficha Alta";

                            if (camposPendientes.Length > 2 && camposPendientes.Substring(0, 2) == ", ")
                                camposPendientes = camposPendientes.Substring(2, camposPendientes.Length - 2);
                            detalle.Pendientes = camposPendientes;

                            camposPendientes = string.Empty;
                        }

                    }

                    if (tipo != "sinTrans")
                    {
                        dt = info.Select(x => new
                        {

                            NombreComercio = x.NombreComercio,
                       //     NumEst = x.NumEst,
                            Marca = x.Marca,
                            Pendientes = x.Pendientes
                        }).ToList().ToDataTable();
                    }
                    else
                    {
                        dt = info.Select(x => new
                        {

                            NombreComercio = x.NombreComercio,
                    //        NumEst = x.NumEst,
                            Marca = x.Marca
                        }).ToList().ToDataTable();
                    }
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }


    public class DetalleAlerta
    {
        public DateTime FechaTransaccion { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
      //  public string NumEst { get; set; }
   //     public string NumTerminal { get; set; }
        public int IDComercio { get; set; }
        public string NombreComercio { get; set; }
        public string Marca { get; set; }
        public string Ciudad { get; set; }
        public string Domicilio { get; set; }
        public int IDDomicilioFiscal { get; set; }
        public string CiudadFiscal { get; set; }
        public string DomicilioFiscal { get; set; }
        public string FormaPago_CBU { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string POSTerminal { get; set; }
        public string POSTipo { get; set; }
        public string FichaAlta { get; set; }
        public string Pendientes { get; set; }
    }
}