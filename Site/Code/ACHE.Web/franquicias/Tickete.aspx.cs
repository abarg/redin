﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using System.Configuration;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Net.Mail;


using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections;
using System.Collections.Generic;

public partial class franquicias_Tickete : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            cargarTickets();
            CargarMailsYUsuarios();
            if (Request.QueryString["Envio"] == "false") {
                litErrorMail.Visible = true;
            }
            if (Request.QueryString["Envio"] == "true" || Request.QueryString["Envio"] == "false")
                litOk.Visible = true;
        }
    }

    protected void cargarTickets()
    {
        int id = int.Parse(Request.QueryString["IDTicket"]);
        using (var dbContext = new ACHEEntities())
        {
            List<TicketseViewModel> tickets = new List<TicketseViewModel>();
            var auxTickets = dbContext.TicketsDetalle.Include("Tickets").Where(x => x.IDTicket == id).ToList();

            foreach (var det in auxTickets)
            {
                var usuario = "";
                if (det.IDFranquicia.HasValue)
                    usuario = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == det.IDUsuario).First().Usuario;
                else
                    usuario = dbContext.Usuarios.Where(x => x.IDUsuario == det.IDUsuario).First().Usuario;

                var ticket = new TicketseViewModel();

                ticket.Fecha = det.Fecha.ToString();
                ticket.Descripcion = det.Descripcion;
                ticket.Usuario = usuario;
                //ticket.MostrarAdjunto = det.Adjunto != null ? "block" : "none";
                ticket.Area = det.Tickets.TicketsArea.Nombre;
                ticket.IDTicket = det.IDTicket;
                ticket.Estado = det.Tickets.Estado;
                ticket.FechaCierre = det.Tickets.FechaCierre.HasValue ? det.Tickets.FechaCierre.Value.ToString("dd/MM/yyyy") : "";

                #region Adjuntos
                var adjuntos = dbContext.TicketsArchivos.Where(x => x.IDDetalleTicket == det.IDDetalleTicket).ToList();
                if (adjuntos.Count() > 0) {
                    ticket.Adjunto = "Archivos: ";
                }
                int max = adjuntos.Count();
                int i = 0;
                foreach (var adj in adjuntos) {
                    ticket.MostrarAdjunto = "block";
                    i++;
                    if (max > 1 && i < max) {
                        ticket.Adjunto += "<a href=\"/files/tickets/" + adj.NombreArchivo + "\" download>" + adj.NombreArchivo + "  -  </a>";
                    }
                    else {
                        ticket.Adjunto += "<a href=\"/files/tickets/" + adj.NombreArchivo + "\" download>" + adj.NombreArchivo + "  </a>";
                    }

                }
                #endregion

                tickets.Add(ticket);
            }
            rptTickets.DataSource = tickets;
            rptTickets.DataBind();

            litArea.Text = tickets.First().Area;
            litEstado.Text = tickets.First().Estado;
            litID.Text = tickets.First().IDTicket.ToString();
            LitAsunto.Text = tickets.First().Asunto;
            litFechaCierre.Text = tickets.First().FechaCierre;
        }
    }

    protected void ResponderTicket(Object sender, EventArgs e) {
        int id = int.Parse(Request.QueryString["IDTicket"]);

        if (id != null && id != 0 && CurrentFranquiciasUser != null) {
            using (var dbContext = new ACHEEntities()) {
                TicketsDetalle det = new TicketsDetalle();
                det.IDTicket = id;
                det.Descripcion = txtMensaje.Text;
                det.IDUsuario = CurrentFranquiciasUser.IDUsuario;
                det.IDFranquicia = CurrentFranquiciasUser.IDFranquicia;
                det.Fecha = DateTime.Now;

                string pathStringOrigen = "~/files/tickets/temp";
                string carpetaOrigen = Server.MapPath(pathStringOrigen);
                string pathStringDestino = "~/files/tickets";
                string carpetaDestino = Server.MapPath(pathStringDestino);

                guardarArchivosDefinitivos(det, carpetaOrigen, carpetaDestino);

                var ticket = dbContext.Tickets.Where(x => x.IDTicket == id).FirstOrDefault();
                if (ticket != null) {
                    ticket.Estado = "Abierto";

                    dbContext.TicketsDetalle.Add(det);
                    dbContext.SaveChanges();

                    #region envioMail

                    string iDTicket = det.IDTicket.ToString();
                    string asunto = "Se respondio el ticket: '" + ticket.Asunto + "'";
                    string mensaje = txtMensaje.Text;

                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<USUARIO>", CurrentFranquiciasUser.Usuario);
                    replacements.Add("<IDTICKET>", iDTicket);
                    replacements.Add("<MENSAJE>", mensaje);
                    List<string> attachments = null;


                    List<TicketsViewModel> usuariosTicket = dbContext.TicketsDetalle
                        .Where(x => x.IDTicket == det.IDTicket).ToList()
                        .Select(x => new TicketsViewModel {
                            IDUsuarioAlta = x.IDUsuario,
                            IDFranquicia = x.IDFranquicia != null ? (int)x.IDFranquicia : 0
                        }).ToList();

                    List<string> emailsTicketTo = new List<string>();
                    if (usuariosTicket.Count() > 0) {
                        foreach (var usuTicket in usuariosTicket) {
                            string mail = "";
                            if (usuTicket.IDFranquicia != 0 && usuTicket.IDFranquicia != null) //si idFranquicia es != null es de franquicia, sino es admin
                            {
                                mail = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == usuTicket.IDUsuarioAlta && x.IDUsuario != CurrentFranquiciasUser.IDUsuario).FirstOrDefault() != null ?
                                    dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == usuTicket.IDUsuarioAlta && x.IDUsuario != CurrentFranquiciasUser.IDUsuario).FirstOrDefault().Email : ""; ;
                            }
                            else {
                                mail = dbContext.Usuarios.Where(x => x.IDUsuario == usuTicket.IDUsuarioAlta).FirstOrDefault() != null ?
                                    dbContext.Usuarios.Where(x => x.IDUsuario == usuTicket.IDUsuarioAlta).FirstOrDefault().Email : "";
                            }
                            if (mail != "")
                                emailsTicketTo.Add(mail);
                        }
                    }
                    //if (emailTo == null)
                    //    throw new Exception("El email de destino no existe");
                    string emailArea = "";
                    if (dbContext.TicketsArea.Any(x => x.IDAreaTicket == ticket.IDArea))
                        emailArea = dbContext.TicketsArea.Where(x => x.IDAreaTicket == ticket.IDArea).FirstOrDefault().Email;

                    if (emailArea == null)
                        throw new Exception("El email del área no puede estar vacio");

                    //try {
                        bool envioOk = false;
                        envioOk = enviar(searchable.Value, replacements, asunto, mensaje, attachments, emailArea, emailsTicketTo);

                        //enviarMail(det.IDTicket.ToString(), CurrentUser.Usuario, det.Descripcion, "", emailTo, asunto);
                        if (envioOk)
                            Response.Redirect("Tickete.aspx?IDTicket=" + det.IDTicket.ToString() + "&Envio=true");
                        else
                            Response.Redirect("Tickete.aspx?IDTicket=" + det.IDTicket.ToString() + "&Envio=false");
                    //}
                    //catch {
                        //throw new Exception("Algunos mails no se han podido enviar");
                    //}
                    #endregion

                }
                else throw new Exception("El ticket no existe");
            }
        }
    }

    private void CargarMailsYUsuarios() {
        using (var dbContext = new ACHEEntities()) {
            var listFranq = dbContext.UsuariosFranquicias
                .Where(x=> x.IDFranquicia == CurrentFranquiciasUser.IDFranquicia)
                .Select(x => new ComboViewModel {
                    ID = x.IDUsuario.ToString(),
                    Nombre = x.Usuario + " - " + x.Email
                }).OrderBy(x => x.Nombre).ToList();//
            cmbEmails.DataTextField = "Nombre";
            cmbEmails.DataValueField = "ID";
            cmbEmails.DataSource = listFranq.ToArray();
            cmbEmails.DataBind();
        }
    }

    protected bool enviar(string ids, ListDictionary replacements, string asunto, string mensaje, List<string> attachments, string emailArea, List<string> emailsTicketTo) {       
            using (var dbContext = new ACHEEntities()) {           
                string[] aux = ids.Split(',');
                string[] item;
                MailAddressCollection listaMail;
                int idUsuario = 0;
                string detalleErrores = string.Empty;
                string titulo = string.Empty;
                string mail = "";
                string nombreUsu = "";
                bool send = false;
                listaMail = new MailAddressCollection();
                if (aux.Length > 0) {
                    #region EnvioMasivo
                    foreach (var id in aux) {
                        if (id != "") {
                            item = id.Split('_');
                            if (item != null) {                              
                                idUsuario = int.Parse(item[0]);                                
                                UsuariosFranquicias usuFran = new UsuariosFranquicias();
                                usuFran = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                                nombreUsu = usuFran.Usuario;
                                mail = usuFran.Email.Trim();
                                
                                if (mail != "") {
                                    if (mail.Trim().IsValidEmailAddress()) {
                                        listaMail.Add(new MailAddress(mail.Trim()));
                                    }
                                    //else if (mail == "") {
                                        //cantErrores++;
                                        //litDetalleErrores.Text += "El usuario " + nombreUsu + " no tiene cargados mails.<br/>";
                                        //litDetalleErrores.Visible = true;
                                    //}
                                    //else {
                                    //    cantErrores++;
                                    //    litDetalleErrores.Text += "La direccion " + mail.Trim() + " del usuario " + nombreUsu + " no es válida<br/>";
                                    //    litDetalleErrores.Visible = true;
                                    //}
                                }
                            }
                        }
                    }                                                    
                }
                listaMail.Add(new MailAddress(emailArea));
            
                if (emailsTicketTo.Count() > 0)
                    foreach (string mailTo in emailsTicketTo) {
                        if (!listaMail.Any(x => x.ToString() == mailTo.Trim()))
                        listaMail.Add(new MailAddress(mailTo.Trim()));                        
                 }

                try {
                    send = EmailHelper.SendMessage(EmailTemplate.TicketArea, replacements, ConfigurationManager.AppSettings["Email.From"], listaMail, "RedIN :: " + asunto, attachments);
                }
                catch (Exception ex) {
                    send = false;
                }                     //if (!send)
                //    litDetalleErrores.Visible = true;

                //else
                    //litOk.Visible = true;                               
                    #endregion   
                return send;   
            }
        }

    protected void crearNuevoArchivoTemp() {
        string pathString = "~/files/tickets/temp";
        string path = Server.MapPath(pathString);
        System.IO.Directory.CreateDirectory(path);
    }

    protected void eliminarArchTemp() {
        string pathString = "~/files/tickets/temp";
        string path = Server.MapPath(pathString);
        if (System.IO.Directory.Exists(path)) {
            System.IO.Directory.Delete(path, true);
        }
    }

    protected void btnFileDelete_Click(object sender, EventArgs e) {
        eliminarArchTemp();
        Response.Redirect("Tickete.aspx?IDTicket=" + hdnID.Value);
    }

    protected void btnFileUpload_Click(object sender, EventArgs e) {

        litErrorDesc.Visible = false;
        string pathStringTmp = "~/files/tickets/temp";
        System.IO.Directory.CreateDirectory(Server.MapPath(pathStringTmp));
        string nombre_carpetaTmp = Server.MapPath(pathStringTmp);
        guardarArchivosTmp(nombre_carpetaTmp);
        DirectoryInfo di = new DirectoryInfo(nombre_carpetaTmp);
        int j = 0;
        int max = di.GetFiles().Count();

        if (max > 0) {
            var imagenes = di.GetFiles().Select(x => new {
                FileName = x.Name
            });
            rptImagenes.DataSource = imagenes;
            rptImagenes.DataBind();
        }
    }

    protected void guardarArchivosTmp(string carpetaDestino) {
        try {
            if (file_upload.HasFile) {
                foreach (var file in file_upload.PostedFiles) {
                    string ext = System.IO.Path.GetExtension(file.FileName);
                    string uniqueName = DateTime.Now.ToString("ddMMyyyyHHmmss") + "_" + file.FileName;
                    //string uniqueName = file.FileName;
                    string path = System.IO.Path.Combine(carpetaDestino, uniqueName);
                    file_upload.SaveAs(path);
                }
                lblUploadStatus.Text = "File(s) uploaded successfully.";
            }
            else {
                lblUploadStatus.Text = "Please upload proper file.";
            }
        }
        catch (Exception ex) {
            lblUploadStatus.Text = "Error in uploading file." + ex.Message;
        }
    }

    protected void guardarArchivosDefinitivos(TicketsDetalle ticketDetalle, string carpetaOrigen, string carpetaDestino) {

        DirectoryInfo di = new DirectoryInfo(carpetaOrigen);
        if (di.GetFiles().Count() > 0) {
            foreach (var file in di.GetFiles()) {
                string nombre_carpeta = System.IO.Path.Combine(carpetaDestino, file.Name);
                int i = 0;
                file.CopyTo(nombre_carpeta);
                var ticketAdjunto = new TicketsArchivos();
                ticketAdjunto.NombreArchivo = file.Name;
                ticketDetalle.TicketsArchivos.Add(ticketAdjunto);
            }
        }
        eliminarArchTemp();
    }           


}
