﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class franquicias_fileexplorer : PaginaFranquiciasBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //set properties according to configuration panel
        FileExplorer1.VisibleControls = GetVisibleControls();
        FileExplorer1.TreeView.EnableDragAndDrop = true;
        FileExplorer1.Grid.ClientSettings.AllowRowsDragDrop = true;

        var nombre = CurrentFranquiciasUser.Franquicia.ToUpper().Replace("RED IN ","");

        FileExplorer1.Configuration.ViewPaths = new string[] { "~/files/explorer/franquicias/" + CurrentFranquiciasUser.IDFranquicia + " - " + nombre, "~/files/explorer/publico/" };
        FileExplorer1.Configuration.UploadPaths = new string[] { "~/files/explorer/franquicias/" + CurrentFranquiciasUser.IDFranquicia + " - " + nombre, "~/files/explorer/publico/" };
        FileExplorer1.Configuration.DeletePaths = new string[] { "~/files/explorer/franquicias/" + CurrentFranquiciasUser.IDFranquicia + " - " + nombre, "~/files/explorer/publico/" };
    }

    protected Telerik.Web.UI.FileExplorer.FileExplorerControls GetVisibleControls()
    {
        Telerik.Web.UI.FileExplorer.FileExplorerControls explorerControls = 0;
        explorerControls |= Telerik.Web.UI.FileExplorer.FileExplorerControls.AddressBox;
        explorerControls |= Telerik.Web.UI.FileExplorer.FileExplorerControls.Grid;
        explorerControls |= Telerik.Web.UI.FileExplorer.FileExplorerControls.Toolbar;
        explorerControls |= Telerik.Web.UI.FileExplorer.FileExplorerControls.TreeView;

        return explorerControls;

    }
}