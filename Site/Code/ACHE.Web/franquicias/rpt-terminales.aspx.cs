﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Text;

public partial class franquicias_rpt_terminales : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cargarCombos();
    }

    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(CurrentFranquiciasUser.IDFranquicia);
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();
            this.ddlMarcas.Items.Insert(0, new ListItem("", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private static DataTable getResults(string estado, string sds, string nombre, int marca, int idFranquicia, string fechaDesde, string fechaHasta)
    {
        DataTable dt = new DataTable();
        using (var dbContext = new ACHEEntities())
        {
            var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + estado + "','" + sds + "','" + nombre + "'," + marca + "," + idFranquicia, new object[] { }).ToList();
            if (fechaDesde != string.Empty)
            {
                var dtDesde = DateTime.Parse(fechaDesde);
                aux = aux.Where(x => x.FechaReprogramacion >= dtDesde).ToList();
            }

            if (fechaHasta != string.Empty)
            {
                var dtHasta = DateTime.Parse(fechaHasta);
                aux = aux.Where(x => x.FechaReprogramacion <= dtHasta).ToList();
            }

            foreach (var row in aux)
            {
                if (row.Tipo.ToUpper() == "WEB")
                    row.Estado = "Azul";
                else if (!row.Activo)
                    row.Estado = "Negro";
                else if (row.ComercioInvalido && row.CantTR == 0)
                    row.Estado = "Naranja";
                else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                    row.Estado = "Verde";
                else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                    row.Estado = "Marron";
                else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                    row.Estado = "Rojo";
                else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                    row.Estado = "Amarillo";
                else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                    row.Estado = "Amarillo";
                else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                    row.Estado = "Verde";
                //else if (row.Activo == "Si" && row.CantTR == 0)
                //    row.Estado = "Amarillo";
                else
                    row.Estado = "Desconocido";
            }

            if (estado != string.Empty)
                aux = aux.Where(x => x.Estado == estado).ToList();
            dt = aux.Select(x => new
            {
                SDS = x.SDS,
                Nombre = x.Nombre,
                Estado = x.Estado,
                EsadoGift = x.EstadoGift,
                EstadoCompras = x.EstadoCompras,
                EstadoCanjes = x.EstadoCanjes,
                Franquicia = x.NombreFranquicia,
                Marca = x.NombreMarca,
                Domicilio = x.Domicilio,
                Localidad = x.Localidad,
                Fecha_Carga = x.FechaCarga,
                Fecha_Alta = x.FechaAlta,
                Dealer = x.Dealer,
                Tipo = x.Tipo,
                Terminal = x.Terminal,
                Establecimiento = x.Establecimiento,
                Fecha_Activ = x.FechaActivacion,
                Fecha_Reprog = x.FechaReprogramacion.ToString("dd/MM/yyyy"),
                Reprogramado = x.Reprogramado ? "Si" : "No",
                Invalido = x.ComercioInvalido ? "Si" : "No",
                Activo = x.Activo ? "Si" : "No",
                Observaciones = x.Observaciones,
            }).ToList().ToDataTable();
        }

        return dt;
    }

    [WebMethod(true)]
    public static string buscar(string estado, string sds, string nombre, int marca, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            try
            {
                DataTable dt = getResults(estado, sds, nombre, marca, idFranquicia, fechaDesde, fechaHasta);
                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table  id='grid'><colgroup>");

                    foreach (System.Data.DataColumn dc in dt.Columns)
                    {
                        sb.Append("<col style='width:100px' ></col>");
                    }

                    sb.Append("</colgroup><thead><tr>");

                    foreach (System.Data.DataColumn dc in dt.Columns)
                    {
                        sb.Append("<th>" + dc.ColumnName.Replace("_", " ") + "</th>");
                    }
                    sb.Append("</thead></tr><tbody>");

                    //write table data
                    foreach (System.Data.DataRow dr in dt.Rows)
                    {
                        sb.Append("<tr>");
                        foreach (System.Data.DataColumn dc in dt.Columns)
                        {
                            if (dc.ColumnName != "Estado")
                                sb.Append("<td>" + dr[dc].ToString() + "</td>");
                            else
                                sb.Append("<td><span class='label label-" + dr[dc].ToString().ToLower() + "' style='font-size: 12px;'>" + dr[dc].ToString() + "</label></td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");

                    return sb.ToString();
                }
                else
                    throw new Exception("No se encontraron registros");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    [WebMethod(true)]
    public static string exportar(string estado, string sds, string nombre, int marca, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            string fileName = "Terminales";
            string path = "/tmp/";

            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            try
            {
                DataTable dt = getResults(estado, sds, nombre, marca, idFranquicia, fechaDesde, fechaHasta);
                if (dt.Rows.Count > 0)
                    ExportarAExcel(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encontraron registros");

                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            return "";
    }

    public static void ExportarAExcel(DataTable dt, string ruta, string nombre)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, nombre);
        wb.SaveAs(ruta + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
}