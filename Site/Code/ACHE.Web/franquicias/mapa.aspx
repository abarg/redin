﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="mapa.aspx.cs" Inherits="franquicias_mapa" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        html, body, #maincontainer, #contentwrapper, .main_content {
            height: 100%;
        }

        #contentwrapper, .main_content {
            min-height: 100%;
        }

        .main_content {
            padding-left: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 0px !important;
            padding-top: 95px !important;
        }

        #map-optionbar-r {
            position: absolute;
            top: 125px;
            right: 35px;
            width: 230px;
            background-color: #fff;
            padding: 5px;
            font-size: 14px;
            line-height: 16px;
            border: 1px solid #232020;
            line-height: 16px;
        }

        .chk {
            vertical-align: top;
        }

        #mapSearch {
            right: 50px;
            height: 0px;
            position: relative;
            /* right: 0; */
            text-align: center;
            top: 20px;
            -webkit-transition: left 250ms cubic-bezier(0,0,0.2,1);
            transition: left 250ms cubic-bezier(0,0,0.2,1);
            z-index: 501;
            float: right;
        }

        .map-search-view {
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background-color: rgba(255,255,255,0.9);
            border-radius: 3px;
            box-shadow: 0 2px 5px 1px rgba(25,24,28,0.3);
            margin: 0 auto;
            padding: 0;
            -webkit-transition: width 250ms cubic-bezier(0,0,0.2,1);
            transition: width 250ms cubic-bezier(0,0,0.2,1);
            width: 600px;
            cursor: move !important;
        }

        .map-search-view .search-field {
            border: 1px solid transparent;
            padding: 3px;
            text-align: left;
        }

        .contentImg{
            float: left;
            width: 150px;
            margin-right: 10px;
            text-align: center;
        }
        .contentImg a{
            color: #369;
            font-size: 12px;
        }
        .contentTxt{
            float: left;
            width: 300px;
        }
        .clear{ clear: both; }

        .movable{
          background-color: rgba(255,255,255,1) !important;
        }

        .expandable{
            text-align: center;
            font-weight: bold;
            display: block;
        }
        .fullBtn{
            width: 75%;
            border-radius: 0;
        }

   
        #divSexo{
            display: none;
        }

        
        #divPolygons{
            display: none;
        }

        #divActividades{
            display: none;
        } 
        
        #divSedes{
            display: none;
        }



    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="mapSearch">
        <div id="search" class="map-search-view">
            <div class="search-field">
                <form runat="server" id="frmBuscar">
                    Tipo de búsqueda:            
                    <div class="tabbable" id="Tabs">             
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabDatosPrincipales" onclick="toggleTipo('comercios');" data-toggle="tab">Comercios</a></li>
                            <li><a href="#tabDomicilio"  class="asociado" data-toggle="tab" onclick="toggleTipo('socios');">Socios</a></li>
                        </ul>
                    </div>
                    <br /><br />
                    <!-- Marca<asp:DropDownList runat="server" ID="ddlMarcas" class="form-control"></asp:DropDownList> -->

                    <div id="divComercios">
                          Rubro<asp:DropDownList runat="server" ID="ddlRubro"  onchange="cargarSubRubros();" ClientIDMode="Static" class="form-control"></asp:DropDownList>&nbsp;
                          Sub Rubro<asp:DropDownList runat="server" ID="ddlSubRubro"  class="form-control"></asp:DropDownList>&nbsp;
                   </div>
                    <!--
                    <div id="divComercios">
                        Rubros: <br />
                        <asp:Repeater runat="server" ID="rptRubros">
                            <ItemTemplate>
                                <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="Checkbox<%# (Container.ItemIndex + 1) %>" value="<%# Eval("IDRubro") %>" onclick="<%# Eval("Funcion") %>">
                                <img src="<%# Eval("Icono") %>" style="width: 24px" />
                                Rubro <%# Eval("Nombre") %>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>-->
                    <div id="divSocios" style="display:none">

                        <div class="expandable"><a class="btn btn-primary fullBtn" onClick="display('sexo')">Edades, sexo</a></div>

                        <div id="divSexo">

                            <div class="form-group"  runat="server">
                                <label class="col-lg-2 control-label"> Sexo: </label>
                                <div class="col-lg-9" style="margin-bottom: 10px;">
                
                                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkMasculino" onclick="filtroClick(this, 'masculino');" value="masculino" />
                                        <img src="/img/markers/masculino.png" style="width: 24px" />
                                        Socios masculinos
                                        <br>
                                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkFemenino" onclick="filtroClick(this, 'femenino');" value="femenino" />
                                        <img src="/img/markers/femenino.png" style="width: 24px" />
                                        Socios femeninos
                                        
                                        <br />
                                        <input type="checkbox" checked="checked" name="chkFiltros" class="chk" id="chkIndefinido" onclick="filtroClick(this, 'indefinido');" value="indefinido" />
                                        <img src="/img/markers/indefinido.png" style="width: 24px" />
                                        Socios indefinidos

                                       </div>
                                  </div>

                          <div id="divEdades" >
                               <div class="form-group"  runat="server">
                                   <label class="col-lg-2 control-label">Edad:</label>
                                   <div class="col-lg-9" style="margin-bottom: 10px;">

                                      <asp:DropDownList runat="server" ID="ddlEdad" class="form-control">
                                          <asp:ListItem Text="0-18" Value="0-18"></asp:ListItem>
                                          <asp:ListItem Text="19-25" Value="19-25"></asp:ListItem>
                                          <asp:ListItem Text="26-30" Value="26-30"></asp:ListItem>
                                          <asp:ListItem Text="31-40" Value="31-40"></asp:ListItem>
                                          <asp:ListItem Text="41-50" Value="41-50"></asp:ListItem>
                                          <asp:ListItem Text="51-60" Value="51-60"></asp:ListItem>
                                          <asp:ListItem Text="61+" Value="61-200"></asp:ListItem>
                                      </asp:DropDownList>
                                    </div>
                                </div>
                           </div>

                        </div>

                        <br />  <br />


                  <div class="expandable"><a class="btn btn-primary fullBtn" onClick="display('capas')">Capas</a></div>


                     <div id="divPolygons">
                        <div class="form-group"  runat="server">
                            <label class="col-lg-2 control-label">Capas</label>
                            <div class="col-lg-9" style="margin-bottom: 10px;">
                                <asp:DropDownList class="form-control" runat="server" ID="ddlLocalidades" multiple="multiple" DataTextField="Nombre" DataValueField="IDPolygon" />
                            </div>
                        </div>
                    </div>

                       
                        <br /> <br />


                        
                  <div class="expandable"><a class="btn btn-primary fullBtn" onClick="display('Sedes')">Sedes</a></div>

                  <div id="divSedes">
                        <div class="form-group"  runat="server">
                            <label class="col-lg-2 control-label">Sedes</label>
                            <div class="col-lg-9"  style="margin-bottom: 10px;">
                                <asp:DropDownList class="form-control" runat="server" ID="ddlSedes" multiple="multiple" DataTextField="Nombre" DataValueField="ID" />
                            </div>
                        </div>
                    </div>



                        <br /> <br />

                  <div class="expandable"><a class="btn btn-primary fullBtn" onClick="display('actividades')">Actividades</a></div>

                  <div id="divActividades">
                        <div class="form-group"  runat="server">
                            <label class="col-lg-2 control-label">Actividades</label>
                            <div class="col-lg-9">
                           
                                                 <asp:DropDownList runat="server" ID="ddlActividades" multiple="multiple" 
                                                   DataTextField="Actividad" DataValueField="IDActividad"></asp:DropDownList>    
                            
                            </div>
                        </div>
                    </div>



                    </div>



                    <br />
                    <span id="litResultados"></span>
                    <br /><br />
                    <a href="javascript:buscar();" class="btn btn-success" id="btnBuscar">Buscar</a>
                    <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" />
                </form>
            </div>
            <div class="autocomplete dropdown-menu" style="display: none;"></div>
        </div>
    </div>


    <div id="map-canvas" style="height: 100%; min-height: 100%; width: 100%; margin-top: -20px"></div>

    <div id="map-optionbar-r" style="display:none">
        Mostrar/Ocultar<br>
        <br>

        <br />
        <br />
        <b>Comercios</b>
        <br />

    </div>
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">


<link rel="stylesheet" href="<%= ResolveUrl("~/lib/partido.kml") %>" />

 <link rel="stylesheet" href="<%= ResolveUrl("~/lib/multi-select/css/multi-select.css") %>" />

 <!-- multiselect -->
	<script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.multi-select.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/multi-select/js/jquery.quicksearch.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>


    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvucvVugcM6B5-rcps4y3QAAQQOAwPivI&callback=initialize"
    async defer></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/mapa.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>

</asp:Content>
