﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class franquicias_transacciones : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarCombos();
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
            if (CurrentFranquiciasUser.Tipo == "B")
                Response.Redirect("home.aspx");
        }
    }

    private void cargarCombos()
    {
        try
        {
            bMarca bMarca = new bMarca();
            List<Marcas> listMarcas = bMarca.getMarcasByFranquicia(CurrentFranquiciasUser.IDFranquicia);
            this.ddlMarcas.DataSource = listMarcas;
            this.ddlMarcas.DataValueField = "IDMarca";
            this.ddlMarcas.DataTextField = "Nombre";
            this.ddlMarcas.DataBind();

            this.ddlMarcas.Items.Insert(0, new ListItem("", ""));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.TransaccionesFranquiciasView
                    .Where(x => x.IDFranquicia.Value == idFranquicia || x.IDFranquiciaComercio.Value == idFranquicia)
                    .OrderByDescending(x => x.FechaTransaccion)
                    .Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        ImportePagado = x.ImporteOriginal - x.ImporteAhorro,
                        Puntos = x.Puntos ?? 0,
                        //PuntosTotales = x.PuntosTotales ?? 0,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Comision = x.Comision,
                        Origen = x.Origen,
                        IDMarca = x.IDMarca,
                        Marca = x.Marca,
                        //PubLocal = x.PubLocal,
                        //PubNacional = x.PubNacional,
                        Domicilio = x.DomicilioComercio
                        //ComisionTtCp = (x.IDFranquiciaComercio.HasValue && x.IDFranquiciaComercio.Value == idFranquicia && (!x.IDFranquicia.HasValue || x.IDFranquicia.Value != x.IDFranquiciaComercio.Value)) ? ((x.Arancel * usu.ComisionTtCp) / 100) : 0,
                        //ComisionTpCp = (x.IDFranquicia.HasValue && x.IDFranquiciaComercio.HasValue && (x.IDFranquicia.Value == x.IDFranquiciaComercio.Value && x.IDFranquicia.Value == idFranquicia)) ? ((x.Arancel * usu.ComisionTpCp) / 100) : 0,
                        //ComisionTpCt = (x.IDFranquicia.HasValue && x.IDFranquicia.Value == idFranquicia && (!x.IDFranquiciaComercio.HasValue || x.IDFranquicia.Value != x.IDFranquiciaComercio.Value)) ? ((x.Arancel * usu.ComisionTpCt) / 100) : 0
                    });

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    result = result.Where(x => x.FechaTransaccion >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                    result = result.Where(x => x.FechaTransaccion <= dtHasta);
                }

                return result.ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string documento, string comercio,
        int idMarca, string origen, string operacion)
    {
        string fileName = "Transacciones";
        string path = "/tmp/";
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.CommandTimeout = 1000;
                    var info = dbContext.TransaccionesFranquiciasView
                        .Where(x => x.IDFranquicia.Value == idFranquicia || x.IDFranquiciaComercio.Value == idFranquicia)
                        .OrderByDescending(x => x.FechaTransaccion).AsQueryable();

                    if (tarjeta != "")
                        info = info.Where(x => x.Numero.ToLower().Contains(tarjeta.ToLower()));
                    if (documento != "")
                        info = info.Where(x => x.NroDocumentoSocio.ToLower().Contains(documento.ToLower()));
                    if (comercio != "")
                        info = info.Where(x => x.NombreFantasia.ToLower().Contains(comercio.ToLower()));
                    if (idMarca > 0)
                        info = info.Where(x => x.IDMarca == idMarca);
                    if (origen != "")
                        info = info.Where(x => x.Origen == origen);
                    if (operacion != "")
                        info = info.Where(x => x.Operacion == operacion);

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }

                    dt = info.ToList().Select(x => new
                    {
                        ID = x.IDTransaccion,
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Marca = x.Marca,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        POSTerminal = x.POSTerminal,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        ImportePagado = x.ImporteOriginal - x.ImporteAhorro,
                        Puntos = x.Puntos ?? 0,
                        //PuntosTotales = x.PuntosTotales ?? 0,
                        NroDocumentoSocio = x.NroDocumentoSocio,
                        Origen = x.Origen,
                        //Comision = x.Comision,
                        //PubLocal = x.PubLocal,
                        //PubNacional = x.PubNacional,
                        Domicilio = x.DomicilioComercio,
                        ComisionTtCp = (x.IDFranquiciaComercio.HasValue && x.IDFranquiciaComercio.Value == idFranquicia && (!x.IDFranquicia.HasValue || x.IDFranquicia.Value != x.IDFranquiciaComercio.Value)) ? ((x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.Arancel * usu.ComisionTtCp) / 100) : ((x.Arancel * usu.ComisionTtCp) / 100) * -1) : 0,
                        ComisionTpCp = (x.IDFranquicia.HasValue && x.IDFranquiciaComercio.HasValue && (x.IDFranquicia.Value == x.IDFranquiciaComercio.Value && x.IDFranquicia.Value == idFranquicia)) ? ((x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.Arancel * usu.ComisionTpCp) / 100) : ((x.Arancel * usu.ComisionTpCp) / 100) * -1) : 0,
                        ComisionTpCt = (x.IDFranquicia.HasValue && x.IDFranquicia.Value == idFranquicia && (!x.IDFranquiciaComercio.HasValue || x.IDFranquicia.Value != x.IDFranquiciaComercio.Value)) ? ((x.Operacion == "Venta" || x.Operacion == "Carga") ? ((x.Arancel * usu.ComisionTpCt) / 100) : ((x.Arancel * usu.ComisionTpCt) / 100) * -1) : 0,
                        Descripcion = x.Descripcion
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }

    /*[System.Web.Services.WebMethod(true)]
    public static void grabar(string tipo, string tarjeta, int idComercio, int importe, string descripcion)
    {
        try
        {
            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(tarjeta);
            if (oTarjeta == null)
                throw new Exception("El Número de Tarjeta no existe");
            else
            {
                int puntos = importe;
                bComercio bComercio = new bComercio();
                Comercios comercio = bComercio.getComercio(idComercio);

                Transacciones tr = new Transacciones();
                tr.FechaTransaccion = DateTime.Now;
                tr.Origen = "Web";
                tr.CodigoPremio = "";
                tr.Descripcion = descripcion;
                tr.Descuento = 0;
                tr.Importe = importe;
                tr.ImporteAhorro = 0;
                tr.NumCupon = "";
                tr.NumEst = comercio.POSEstablecimiento;//"Web";
                tr.NumReferencia = "";
                tr.NumRefOriginal = "";
                tr.NumTarjetaCliente = tarjeta;
                tr.NumTerminal = comercio.POSTerminal;// "Web";
                tr.Operacion = tipo;
                tr.PuntosAContabilizar = (tipo == "Venta" ? (puntos) : (puntos * -1));
                tr.PuntosDisponibles = "";
                tr.PuntosIngresados = "";
                tr.TipoMensaje = "1100";
                tr.TipoTransaccion = (tipo == "Venta" ? "000000" : "220000");
                tr.UsoRed = 0;
                tr.Puntos = comercio.POSPuntos;
                tr.Arancel = comercio.POSArancel;

                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Transacciones.Add(tr);
                    dbContext.SaveChanges();

                    dbContext.ActualizarPuntosPorTarjeta(tarjeta);
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
    */

    [System.Web.Services.WebMethod(true)]
    public static List<TransaccionesViewModel> GetBySocio(int id)
    {
        return GetTrBySocio(id, null);
    }

    private static List<TransaccionesViewModel> GetTrBySocio(int id, int? max)
    {
        var list = new List<TransaccionesViewModel>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.TransaccionesView.Where(x => x.IDSocio.HasValue && x.IDSocio == id)
                    .OrderByDescending(x => x.FechaTransaccion).Select(x => new TransaccionesViewModel()
                    {
                        Fecha = x.Fecha,
                        FechaTransaccion = x.FechaTransaccion,
                        Hora = x.Hora,
                        Origen = x.Origen,
                        Operacion = x.Operacion,
                        SDS = x.SDS,
                        Comercio = x.NombreFantasia,
                        NroEstablecimiento = x.NroEstablecimiento,
                        Tarjeta = x.Numero,
                        Socio = x.Apellido + ", " + x.Nombre,
                        ImporteOriginal = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteOriginal : (x.ImporteOriginal * -1),
                        ImporteAhorro = (x.Operacion == "Venta" || x.Operacion == "Carga") ? x.ImporteAhorro : (x.ImporteAhorro * -1),
                        Puntos = x.PuntosAContabilizar ?? 0,
                    });

                if (max.HasValue)
                    list = aux.OrderByDescending(x => x.FechaTransaccion).Take(max.Value).ToList();
                else
                    list = aux.ToList();
            }
        }
        return list;
    }

}