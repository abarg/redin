﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using ClosedXML.Excel;
using System.IO;

public partial class franquicias_dashboard : PaginaFranquiciasBase {
    protected void Page_Load(object sender, EventArgs e) {
        litFecha2.Text = litFechaTit.Text = "Desde " + DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy") + " hasta " + DateTime.Now.ToString("dd/MM/yyyy");
        if (!IsPostBack) {
            if (CurrentFranquiciasUser.Tipo == "B")
                Response.Redirect("home.aspx");

            litLeyenda1.Text = CurrentFranquiciasUser.ComisionTpCp.ToString("#0.00");
            litLeyenda2.Text = CurrentFranquiciasUser.ComisionTpCt.ToString("#0.00");
            litLeyenda3.Text = CurrentFranquiciasUser.ComisionTtCp.ToString("#0.00");
        }
    }


    #region Graficos

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTerminalesPorPOS() {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {

            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TerminalesActivos " + idFranquicia, new object[] { }).ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasAsignadas() {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities()) {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasAsignadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCantTransacciones() {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities()) {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasActivas() {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities()) {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasActivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasInactivas() {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities()) {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasInactivas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasEmitidas() {
        List<Chart> list = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TarjetasEmitidas " + idFranquicia).ToList();
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerEstadoTerminales() {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + "" + "','" + "" + "','" + "" + "'," + 0 + "," + idFranquicia, new object[] { }).ToList();
                /*
                var aux = dbContext.GetReporteTerminales("", "", "", 0, idFranquicia)
                    //.Where(x => x.IDFranquicia.HasValue && x.IDFranquicia.Value == idFranquicia)
                    .Select(x => new TerminalesViewModel() {
                        Estado = "",
                        Fecha_Activ = x.Fecha_Activacion,
                        Fecha_Reprog = x.Fecha_Reprogramacion,
                        Reprogramado = x.Reprogramado ? "Si" : "No",
                        Invalido = x.Comercio_Inválido ? "Si" : "No",
                        Activo = x.Activo ? "Si" : "No",
                        CantTR = x.CantTR,
                        UltimoImporte = x.UltimoImporte,
                        Tipo = x.Tipo
                    }).ToList();
                */
                foreach (var row in aux) {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && !row.Reprogramado && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.FechaReprogramacion != null && !row.Reprogramado && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.CantTR == 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }

                //var prevList = aux.ToList();
                int negro = aux.Count(x => x.Estado == "Negro");
                int naranja = aux.Count(x => x.Estado == "Naranja");
                int verde = aux.Count(x => x.Estado == "Verde");
                int rojo = aux.Count(x => x.Estado == "Rojo");
                int amarillo = aux.Count(x => x.Estado == "Amarillo");
                int azul = aux.Count(x => x.Estado == "Azul");
                int desconocido = aux.Count(x => x.Estado == "Desconocido");
                int noprobado = aux.Count(x => x.Estado == "Marron");

                list.Add(new Chart() { data = negro, label = "<a href=\"javascript:verDetalleTerminales('Negro');\">Baja (" + negro + ")</a>" });
                list.Add(new Chart() { data = naranja, label = "<a href=\"javascript:verDetalleTerminales('Naranja');\">Inválido (" + naranja + ")</a>" });
                list.Add(new Chart() { data = verde, label = "<a href=\"javascript:verDetalleTerminales('Verde');\">Activo Mayor a $1 (" + verde + ")</a>" });
                list.Add(new Chart() { data = rojo, label = "<a href=\"javascript:verDetalleTerminales('Rojo');\">No Reprogramado (" + rojo + ")</a>" });
                list.Add(new Chart() { data = amarillo, label = "<a href=\"javascript:verDetalleTerminales('Amarillo');\">Activo Menor a $1 (" + amarillo + ")</a>" });
                list.Add(new Chart() { data = azul, label = "<a href=\"javascript:verDetalleTerminales('Azul');\">Web (" + azul + ")</a>" });
                list.Add(new Chart() { data = desconocido, label = "<a href=\"javascript:verDetalleTerminales('Desconocido');\">Desconocido (" + desconocido + ")</a>" });
                list.Add(new Chart() { data = noprobado, label = "<a href=\"javascript:verDetalleTerminales('Marron');\">No Probado (" + noprobado + ")</a>" });
            }
        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Socios() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Franq_TopSocios " + idFranquicia, new object[] { }).ToList();
                if (list.Any()) {
                    foreach (var detalle in list) {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10SociosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Franq_TopSociosPorMes '" + fechaDesde + "','" + fechaHasta + "', "+  idFranquicia , new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='3'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10ComerciosMensual()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Franq_TopComerciosPorMes '" + fechaDesde + "','" + fechaHasta + "', " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Comercios() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Franq_TopComercios " + idFranquicia, new object[] { }).ToList();
                if (list.Any()) {
                    foreach (var detalle in list) {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerComision(string tipo) {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            string fechaDesde = "";
            string fechaHasta = "";
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'" + tipo + "','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? (int)mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'" + tipo + "','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? (int)mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'" + tipo + "','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? (int)mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'" + tipo + "','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? (int)mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerComisionDiario(string tipo) {
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {

            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);//"01-20-2014";// 
            int dias = 30;

            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Franq_ComisionesPorDias " + idFranquicia + ",'" + tipo + "','" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart() {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioArancel() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {


            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromedioArancel '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(Convert.ToDouble(total[0].data)).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioPuntos() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {


            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromedioPuntos '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())

                    html = Math.Abs(Convert.ToDouble(total[0].data)).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioDescuentos() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {


            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDouble>("exec Dashboard_Franq_PromedioDescuentos '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(Convert.ToDouble(total[0].data)).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }


    [System.Web.Services.WebMethod(true)]
    public static string obtenerFacturacionComercios() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {


            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_FacturacionComercios '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }



    [System.Web.Services.WebMethod(true)]
    public static string obtenerCantidadTarjetas() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {


            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_CantidadTarjetasUnicas '" + fechaDesde + "','" + fechaHasta + "'," + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("");
                else
                    html = "0";
            }
        }

        return html;
    }


    #endregion

    #region Estadisticas Superiores

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalSociosPorSexo() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalSociosPorSexo " + idFranquicia, new object[] { }).ToList();
                if (list.Any()) {
                    html = "F: " + list[0].data + " - M: " + list[1].data + " <br/> I: " + list[2].data;// + list[3].data);
                    decimal total = list[0].data + list[1].data + list[2].data;// +list[3].data;
                    html += "," + total;
                }
                else
                    html = "Error";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalComisionMensual() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                /*var total = 0;dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_TotalComisionMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else*/
                html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTRMensual() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTRMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTasaUsoMensual() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_TotalTasaUsoMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("#0.00");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPromedioTicketMensual() {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_PromedioTicketMensual " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerComisiones() {
        var html = string.Empty;
        var com1 = string.Empty;
        var com2 = string.Empty;
        var com3 = string.Empty;
        decimal tot = 0;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any()) {
                    com1 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com1 = "0";

                total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TpCt','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any()) {
                    com2 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com2 = "0";

                total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_ComisionesMensual " + idFranquicia + ",'TtCp','" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any()) {
                    com3 = Math.Abs(total[0].data).ToString("N2");
                    tot += total[0].data;
                }
                else
                    com3 = "0";

                //var entity = dbContext.Franquicias.Where(x => x.IDFranquicia == usu.IDFranquicia).FirstOrDefault();
                html = com1 + "_" + com2 + "_" + com3 + "_" + tot.ToString("N2") + "_" + (tot * 1.21M).ToString("N2");
            }
        }

        return html;
    }

    /*
    [System.Web.Services.WebMethod(true)]
    public static string obtenerPubLocal()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            html = Math.Abs(usu.PubLocal).ToString("N2");
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerPubNacional()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            html = Math.Abs(usu.PubNacional).ToString("N2");
        }

        return html;
    }
    */
    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalEmails() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalEmails " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalCelulares() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalCelulares " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = int.Parse(total[0].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTotalTarjetasActivas() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Franq_TotalTarjetasActivas " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                    html = int.Parse(list[0].data.ToString()).ToString("N").Replace(",00", "") + " / " + int.Parse(list[1].data.ToString()).ToString("N").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    #endregion

    #region Otros

    [WebMethod(true)]
    public static string obtenerDetalleTerminales(string estado) {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;

            using (var dbContext = new ACHEEntities()) {

                var aux = dbContext.Database.SqlQuery<TerminalesViewModelSP>("exec GetReporteTerminales '" + estado + "','" + "" + "','" + "" + "'," + 0 + "," + 0, new object[] { }).ToList();
/*
                var list = dbContext.GetReporteTerminales(estado, "", "", 0, idFranquicia).ToList();
                var aux = list.Select(x => new TerminalesViewModel() {
                    SDS = x.SDS,
                    Nombre = x.Nombre,
                    Estado = "",
                    Domicilio = x.Domicilio,
                    Localidad = x.Localidad,
                    Fecha_Carga = x.Fecha_Carga,
                    Fecha_Alta = x.Fecha_Alta,
                    Dealer = x.Dealer,
                    Tipo = x.Tipo + " " + x.Tipo_Terminal,
                    Terminal = x.Terminal,
                    Establecimiento = x.Establecimiento,
                    Fecha_Activ = x.Fecha_Activacion,
                    Fecha_Reprog = x.Fecha_Reprogramacion,
                    Reprogramado = x.Reprogramado ? "Si" : "No",
                    Invalido = x.Comercio_Inválido ? "Si" : "No",
                    Activo = x.Activo ? "Si" : "No",
                    Observaciones = x.Observaciones,
                    CantTR = x.CantTR,
                    UltimoImporte = x.UltimoImporte,
                    Marca = x.NombreMarca,
                    Franquicia = x.NombreFranquicia
                }).ToList(); //.ToDataTable();
                */
                foreach (var row in aux)
                {
                    if (row.Tipo.ToUpper() == "WEB")
                        row.Estado = "Azul";
                    else if (!row.Activo)
                        row.Estado = "Negro";
                    else if (row.ComercioInvalido && row.CantTR == 0)
                        row.Estado = "Naranja";
                    else if (row.ComercioInvalido && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    else if (row.Activo && row.FechaReprogramacion != null && row.Reprogramado && row.CantTR == 0 && row.UltimoImporte == 0)
                        row.Estado = "Marron";
                    else if (row.Activo && !row.Reprogramado && row.CantTR == 0)//else if (row.Activo == "Si" && row.Fecha_Reprog == string.Empty && row.Reprogramado == "No" && row.CantTR == 0)
                        row.Estado = "Rojo";
                    else if (row.Activo && row.Reprogramado && row.FechaReprogramacion != null && row.CantTR == 0 && row.FechaActivacion == string.Empty)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.UltimoImporte < 1 && row.UltimoImporte > 0)
                        row.Estado = "Amarillo";
                    else if (row.Activo && row.CantTR > 0 && row.UltimoImporte > 1)
                        row.Estado = "Verde";
                    //else if (row.Activo == "Si" && row.CantTR == 0)
                    //    row.Estado = "Amarillo";
                    else
                        row.Estado = "Desconocido";
                }

                aux = aux.Where(x => x.Estado.ToLower() == estado.ToLower()).ToList();

                if (aux.Any()) {
                    foreach (var detalle in aux) {
                        html += "<tr>";
                        html += "<td>" + detalle.SDS + "</td>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.NombreMarca + "</td>";
                        html += "<td>" + detalle.Domicilio + "</td>";
                        html += "<td>" + detalle.FechaCarga + "</td>";
                        html += "<td>" + detalle.FechaAlta + "</td>";
                        html += "<td>" + detalle.Terminal + "</td>";
                        html += "<td>" + detalle.Establecimiento + "</td>";
                        html += "<td>" + detalle.FechaActivacion + "</td>";
                        html += "<td>" + detalle.FechaReprogramacion.ToString("dd/MM/yyyy") + "</td>";
                        html += "<td>" + (detalle.Reprogramado ? "Si" : "No") + "</td>";
                        html += "<td>" + (detalle.ComercioInvalido ? "Si" : "No") + "</td>";
                        html += "<td>" + (detalle.Activo ? "Si" : "No") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='14'>No hay un detalle disponible</td></tr>";

            }
        }

        return html;
    }

    #endregion

    #region Tablas HTML

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<TarjetasImpresas> ObtenerTarjetasEmitidas() {
        List<TarjetasImpresas> result = null;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            using (var dbContext = new ACHEEntities()) {
                result = dbContext.Database.SqlQuery<TarjetasImpresas>("exec Dashboard_Franq_TarjetasEmitidas_Listado " + idFranquicia).ToList();
                result = result.Select(x => new TarjetasImpresas() {
                    Marca = x.Marca,
                    Activas = x.Activas,
                    Inactivas = x.Total - x.Activas,
                    Total = x.Total
                }).ToList();
            }
        }
        return result;
    }
    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string obtenerDineroDisponibleCredito()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                int idFranquicia = usu.IDFranquicia;
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_DineroDisponibleCredito " + idFranquicia).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }
    [WebMethod(true)]
    public static string ExportarTarjetasEmitidas() {
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null) {
            try {
                var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
                int idFranquicia = usu.IDFranquicia;
                string fileName = "emitidas";
                string path = "/tmp/";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var info = dbContext.Database.SqlQuery<TarjetasImpresas>("exec Dashboard_Franq_TarjetasEmitidas_Listado " + idFranquicia).ToList();
                    info = info.Select(x => new TarjetasImpresas() {
                        Marca = x.Marca,
                        Activas = x.Activas,
                        Inactivas = x.Total - x.Activas,
                        Total = x.Total
                    }).ToList();
                    if (info.Any()) {
                        dt = info.Select(x => new {
                            Marca = x.Marca,
                            Activas = x.Activas,
                            Inactivas = x.Inactivas,
                            Total = x.Total
                        }).ToList().ToDataTable();

                        if (dt.Rows.Count > 0)
                            generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                        else
                            throw new Exception("No se encuentran datos para los filtros seleccionados");
                        return path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                    }
                }
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName) {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");
    }
    #endregion
}