﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;

public partial class franquicias_dashboard_gift : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litFechaTit.Text = "Desde " + DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy") + " hasta " + DateTime.Now.ToString("dd/MM/yyyy");
    }

    #region Graficos

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerTarjetasUtilizadas()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_TarjetasUtilizadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-3).ToString("MMM"), data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_TarjetasUtilizadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-2).ToString("MMM"), data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_TarjetasUtilizadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.AddMonths(-1).ToString("MMM"), data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_TarjetasUtilizadas " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = DateTime.Now.ToString("MMM"), data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCarga()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Carga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Carga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Carga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Carga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerDescarga()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = "";
            string fechaHasta = "";

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes4 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Descarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "1", data = (mes4.Any() ? mes4[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes3 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Descarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "2", data = (mes3.Any() ? mes3[0].data : 0) });

                fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes2 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Descarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "3", data = (mes2.Any() ? mes2[0].data : 0) });

                fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);
                var mes1 = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_Descarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                list.Add(new Chart() { label = "4", data = (mes1.Any() ? mes1[0].data : 0) });

            }
        }

        return list;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Socios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Gift_Franq_TopSocios " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos + "</td>";
                        html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }
    [WebMethod(true)]
    public static string obtenerDineroDisponibleGiftCard()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Franq_DineroDisponibleGiftcard " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = Math.Abs(total[0].data).ToString("N2");
                else
                    html = "0";
            }
        }

        return html;
    }

    [System.Web.Services.WebMethod(true)]
    public static string obtenerTop10Comercios()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.Database.SqlQuery<TableHtml>("exec Dashboard_Gift_Franq_TopComercios " + idFranquicia, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.uno + "</td>";
                        html += "<td>" + detalle.dos.Truncate(20, "...") + "</td>";
                        //html += "<td>" + detalle.tres + "</td>";
                        html += "<td style='text-align:right'>$" + Math.Abs(detalle.cuatro).ToString("N2") + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";
            }
        }

        return html;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCargaDiario()
    {
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Gift_Franq_CargaPorDias " + idFranquicia + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerDescargaDiario()
    {
        var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
        int idFranquicia = usu.IDFranquicia;
        
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        List<Chart> list = new List<Chart>();
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            string fechaDesde = DateTime.Now.AddDays(-30).ToString(formato);
            //string fechaHasta = DateTime.Now.ToString(formato);
            int dias = 30;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Database.SqlQuery<ChartDate>("exec Dashboard_Gift_Franq_DescargaPorDias " + idFranquicia + ",'" + fechaDesde + "','" + dias + "'", new object[] { }).ToList();
                foreach (var x in aux)
                    list.Add(new Chart()
                    {
                        label = x.fecha.Year.ToString() + ", " + (x.fecha.Month - 1).ToString() + ", " + x.fecha.Day.ToString(),
                        data = x.importe
                    });
            }
        }

        return list;
    }

    #endregion

    #region Estadisticas Superiores

    //Mensuales    
    [WebMethod(true)]
    public static string obtenerCantTarjetasDinero()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_CantTarjetasDinero " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString();
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerCantTarjetasStock()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<Chart>("exec Dashboard_Gift_Franq_CantTarjetasStock " + idFranquicia, new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString();
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalSaldoCargado()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Gift_Franq_TotalSaldoCargado " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalSaldoUtilizado()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Gift_Franq_TotalSaldoUtilizado " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalArancelCarga()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Gift_Franq_TotalArancelCarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }

    [WebMethod(true)]
    public static string obtenerTotalArancelDescarga()
    {
        var html = string.Empty;
        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentFranquiciasUser"] != null)
        {
            var usu = (WebFranquiciasUser)HttpContext.Current.Session["CurrentFranquiciasUser"];
            int idFranquicia = usu.IDFranquicia;
            
            string fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
            string fechaHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1).ToString(formato);

            using (var dbContext = new ACHEEntities())
            {
                var total = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_Gift_Franq_TotalArancelDescarga " + idFranquicia + ",'" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                if (total.Any())
                    html = total[0].data.ToString("N2").Replace(",00", "");
                else
                    html = "0";
            }
        }

        return html;
    }
    
    #endregion
}