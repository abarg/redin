﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="rpt-terminales.aspx.cs" Inherits="franquicias_rpt_terminales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/rpt-terminales.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Reportes</li>
            <li class="last">Estado de Terminales</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Reporte de Estado de Terminales</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formReporteComercios" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Estado</label>
                            <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Rojo" Value="Rojo"></asp:ListItem>
                                <asp:ListItem Text="Naranja" Value="Naranja"></asp:ListItem>
                                <asp:ListItem Text="Amarillo" Value="Amarillo"></asp:ListItem>
                                <asp:ListItem Text="Verde" Value="Verde"></asp:ListItem>
                                <asp:ListItem Text="Negro" Value="Negro"></asp:ListItem>
                                <asp:ListItem Text="Azul" Value="Azul"></asp:ListItem>
                                <asp:ListItem Text="Marron" Value="Marron"></asp:ListItem>
                                <asp:ListItem Text="Desconocido" Value="Desconocido"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label>SDS</label>
                            <asp:TextBox runat="server" ID="txtSDS" CssClass="form-control" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Nombre</label>
                            <asp:TextBox runat="server" ID="txtNumeroEst" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" ID="ddlMarcas" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <%-- <div class="col-md-3">
                            <label>Nombre</label>
                            <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" MaxLength="50" />
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha Reprog. desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha Reprog. hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan" MaxLength="10" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="buscar();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Terminales" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="divGrid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

