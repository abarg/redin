﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="rpt-facturacion-detalle.aspx.cs" Inherits="franquicias_rpt_facturacion_detalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/rpt-facturacion-detalle.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Reportes</li>
            <li class="last">Facturación Detalle</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Facturación Detalle</h3>
            
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form runat="server" id="formReporte" class="form-horizontal" role="form">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2 hide">
                            <label>Operaciones mínimas</label>
                            <asp:TextBox runat="server" ID="txtCantOper" CssClass="form-control" MaxLength="10" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Marca</label>
                            <asp:DropDownList runat="server" ID="ddlMarcas" CssClass="form-control" />
                        </div>
                        <div class="col-md-3">
                            <label>Nombre Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2">
                            <label>SDS</label>
                            <asp:TextBox runat="server" ID="txtSDS" CssClass="form-control" MaxLength="10" />
                        </div>
                        <%--<div class="col-md-2">
                            <label>Numero Est</label>
                            <asp:TextBox runat="server" ID="txtNumeroEst" CssClass="form-control" MaxLength="50" />
                        </div>
                        <div class="col-md-2" style="display:none">
                            <label> Operaciones mínimas</label>
                            <asp:TextBox runat="server" ID="txtCantOper" CssClass="form-control" MaxLength="10" />
                        </div>--%>
                        <%--<div style="clear:both"></div>--%>
                    </div>
                    <%--<div class="row">
                        <div class="col-md-6">
                            <label class="col-lg-2 control-label" style="padding-left:0px; text-align:left">Tipo</label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" ID="rdbAgrupado" GroupName="grpTipo" Checked="true" Text="Agrupado" Width="100px" />
                            </label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" ID="rdbDetallado" GroupName="grpTipo" Checked="false" Text="Detallado" Width="150px" />
                            </label>
                        </div>
                    </div>--%>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar</button>
                            <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Facturacion" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

