﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="Ticketn.aspx.cs" Inherits="franquicias_Ticketn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="../../lib/chosen/chosen.css" rel="stylesheet" />
    <script src="../../lib/chosen/chosen.jquery.js"></script><link href="../../lib/chosen/chosen.css" rel="stylesheet" />
    <script src="../../js/jquery.maskMoney.min.js"></script>
    <script src="../../js/views/tickets/ticketsenvio.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="<%= ResolveUrl("~/franquicias/Ticket.aspx") %>">Tickets</a></li>
               <li>Nuevo</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Ticket Nuevo</h3>
        <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">El ticket se ha creado correctamente.</div></asp:Literal>
        <form runat="server" id="formTicket" class="form-horizontal" role="form">
            <div  class="form-group" >
                <label class="col-lg-2 control-label"><span class="f_req">*</span> Área</label>
                <div class="col-sm-2 col-md-2">
                    <asp:DropDownList runat="server" class="form-control" ID="cmbAreas" > </asp:DropDownList>
                </div>
            </div>
            <div  class="form-group" >
                <label class="col-lg-2 control-label"><span class="f_req">*</span> Prioridad</label>
                 <div class="col-sm-2 col-md-2">
                <asp:DropDownList runat="server" class="form-control" ID="cmbPrioridad">
                    <asp:ListItem Text="Baja" Value="Baja"></asp:ListItem>
                    <asp:ListItem Text="Media" Value="Media"></asp:ListItem>
                    <asp:ListItem Text="Alta" Value="Alta"></asp:ListItem>
                </asp:DropDownList>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-2 control-label"><span class="f_req">*</span> Asunto:</label>           
                <div class="col-sm-2 col-md-2">
                     <asp:TextBox runat="server" ID="txtAsunto" Rows="1" TabIndex="1" CssClass="form-control" MaxLength="50" Width="370"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtAsunto"
                    ErrorMessage="No puede dejar vacio el campo Asunto"
                    ForeColor="Red">
                </asp:RequiredFieldValidator> 
                </div> 
            </div>
            <div  class="form-group" >
                <label class="col-lg-2 control-label"><span class="f_req">*</span> Mensaje:</label>           
                <div class="col-sm-2 col-md-2">
                     <asp:TextBox runat="server" ID="txtMensaje" TextMode="MultiLine" Rows="3" TabIndex="1" CssClass="full" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                  ControlToValidate="txtMensaje"
                  ErrorMessage="Debe agregar un mensaje"
                  ForeColor="Red">
                </asp:RequiredFieldValidator>
                </div> 
            </div> 
            <div class="form-group " >
                <div class="col-sm-8 col-sm-offset-2">
                    <asp:FileUpload ID="file_upload" runat="server" />
                    <asp:ListView runat="server" ID="rptImagenes">                                        
                        <ItemTemplate>
                                <label id='<%# Eval("FileName") %>'><%# Eval("FileName") %></label>
                        </ItemTemplate>
                    </asp:ListView>
                    <br />
                    <asp:Button ID="btnFileUpload" runat="server" Text="Cargar Archivo" OnClick="btnFileUpload_Click" />
                    <br />
                    <asp:Label ID="lblUploadStatus" runat="server"></asp:Label>
                    <br />
                    <asp:Button ID="Button1" runat="server" Text="Eliminar Todos" OnClick="btnFileDelete_Click" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><span class="f_req"></span> Poner en copia de ticket nuevo</label>
                <div class="col-lg-9">
					<div class="fieldwrap">
                        <asp:DropDownList runat="server" ID="cmbEmails" ClientIDMode="Static" Width="100%"
                        data-placeholder="Seleccione 1 o más mails" multiple="true" CssClass="chzn-select-deselect"></asp:DropDownList>
                        <span style="color:red"><asp:Literal runat="server" ID="litDetalleErrores" Text="No se han podido enviar los mails correctamente" Visible="false"></asp:Literal></span>
                        <%--<span style="color:green"><asp:Literal runat="server" ID="litOk" Text="Se han enviado los mails correctamente" Visible="false"></asp:Literal></span>--%>
                        <asp:HiddenField runat="server" ID="searchable" ClientIDMode="Static" />
					</div>                        
                </div>
            </div>
            <div class="col-sm-8 col-sm-offset-2">
                <asp:Button runat="server" id="btnCrearTicket" class="btn btn-success" type="button" OnClick="CrearTicket" OnClientClick="$('#searchable').val($('#cmbEmails').val());" Text="Crear Nuevo Ticket"></asp:Button>
                <a href="Ticket.aspx" class="btn btn-link">Cancelar</a>
            </div>
        </form>
    </div>
</div>
</asp:Content>

