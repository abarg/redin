﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageFranquicias.master" AutoEventWireup="true" CodeFile="Ticket.aspx.cs" Inherits="franquicias_Ticket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.numeric.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
     <script type="text/javascript" src="<%= ResolveUrl("~/js/views/franquicias/tickets.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/franquicias/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Listado Tickets</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Listado Tickets</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <asp:Literal runat="server" id="litOk" Visible="false"><div class="alert alert-success alert-dismissable">El ticket se ha creado correctamente.</div></asp:Literal>
            <asp:Literal runat="server" id="litErrorMail" Visible="false"><div class="alert alert-danger alert-dismissable">El mail no se ha enviado correctamente</div></asp:Literal>
            <form id="formTikets" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                    <div class="col-sm-2">
                        <label>Numero Ticket</label>
                        <input type="text" id="txtNroTicket" value="" maxlength="12" class="form-control numeric" />                        
                   </div>
                    <div class="col-md-2">
                        <label><span class="f_req">*</span> Fecha desde</label>
                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                    </div>
                    <div class="col-md-2">
                        <label><span class="f_req">*</span> Fecha hasta</label>
                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                    </div>
                      <div class="col-sm-2">
                        <label>Estado</label>
                        <asp:DropDownList runat="server" class="form-control" ID="txtEstado">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="Abierto" Value="Abierto" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Cerrado" Value="Cerrado"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <label>Prioridad</label>
                        <asp:DropDownList runat="server" class="form-control" ID="txtPrioridad">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                            <asp:ListItem Text="Baja" Value="Baja"></asp:ListItem>
                            <asp:ListItem Text="Media" Value="Media"></asp:ListItem>
                            <asp:ListItem Text="Alta" Value="Alta"></asp:ListItem>
                        </asp:DropDownList>
                     </div>
                 </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <label>Asunto</label>
                            <input type="text" id="txtAsunto" value="" maxlength="50" class="form-control" />                        
                       </div>
                        <div class="col-md-2">
                            <label>Área</label>
                            <asp:DropDownList runat="server" class="form-control" ID="cmbAreas">
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Usuario</label>
                            <input type="text" id="txtUsuario" value="" maxlength="50" class="form-control" />                        
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-md-8">
                        <br>
                        <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                        <button class="btn" type="button" id="btnNuevo" onclick="Nuevo();">Nuevo</button>
                        <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                        <img alt="" src="../../img/ajax_loader.gif" id="imgLoading" style="display:none" />
                        <a href="" id="lnkDownload" download="Tickets" style="display:none">Descargar</a>
                    </div>
                </div>                
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">            
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

