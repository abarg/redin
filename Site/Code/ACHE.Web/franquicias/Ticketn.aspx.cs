﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using System.Configuration;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections;
using System.Collections.Generic;


public partial class franquicias_Ticketn : PaginaFranquiciasBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litDetalleErrores.Visible = false;
        litOk.Visible = false;
        if (!IsPostBack)
        {
            txtAsunto.Text = "";
            txtMensaje.Text = "";
            cargarCombos();
            CargarMailsYUsuarios();
        }
    }

    private void CargarMailsYUsuarios() {
        using (var dbContext = new ACHEEntities()) {
            var listFranq = dbContext.UsuariosFranquicias
                .Where(x => x.IDFranquicia == CurrentFranquiciasUser.IDFranquicia)
                .Select(x => new ComboViewModel {
                    ID = x.IDUsuario.ToString(),
                    Nombre = x.Usuario + " - " + x.Email
                }).OrderBy(x => x.Nombre).ToList();//
            cmbEmails.DataTextField = "Nombre";
            cmbEmails.DataValueField = "ID";
            cmbEmails.DataSource = listFranq.ToArray();
            cmbEmails.DataBind();

        }
    }

    private void cargarCombos()
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                List<TicketsArea> listaAreas = dbContext.TicketsArea.Where(x => x.Activa && x.VisibleEnFranquicias).ToList();
                this.cmbAreas.DataSource = listaAreas;
                this.cmbAreas.DataValueField = "IDAreaTicket";
                this.cmbAreas.DataTextField = "Nombre";
                this.cmbAreas.DataBind();
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void CrearTicket(Object sender, EventArgs e)
    {
            using (var dbContext = new ACHEEntities()) {
                Tickets ticket = new Tickets();//TipoUsuario == 2 es franquicia
                ticket.FechaAlta = DateTime.Now;
                ticket.IDArea = int.Parse(cmbAreas.SelectedValue);//todo: corregit que guarde el idarea
                //ticket.TicketsArea = cmbAreas.SelectedItem;
                ticket.Estado = "Abierto";
                ticket.TicketsDetalle = new List<TicketsDetalle>();
                ticket.Asunto = txtAsunto.Text;
                ticket.UsuarioAlta = CurrentFranquiciasUser.Usuario;
                ticket.Prioridad = cmbPrioridad.SelectedValue;
                ticket.IDUsuarioAlta = CurrentFranquiciasUser.IDUsuario;
                ticket.IDFranquicia = CurrentFranquiciasUser.IDFranquicia;


                var ticketDetalle = new TicketsDetalle();
                ticketDetalle.IDFranquicia = CurrentFranquiciasUser.IDFranquicia;
                ticketDetalle.IDUsuario = CurrentFranquiciasUser.IDUsuario;
                ticketDetalle.Descripcion = txtMensaje.Text;
                ticketDetalle.Fecha = DateTime.Now;

                ticket.TicketsDetalle.Add(ticketDetalle);
                dbContext.Tickets.Add(ticket);
                dbContext.SaveChanges();

                #region envioMail
                string iDTicket = ticketDetalle.IDTicket.ToString();
                string asunto = "Se genero un nuevo ticket : '" + txtAsunto.Text + "'";
                string emailArea = dbContext.TicketsArea.Where(x => x.IDAreaTicket == ticket.IDArea).FirstOrDefault().Email;
                if (emailArea == null)
                    throw new Exception("El email del área no puede estar vacio");
                string mensaje = ticketDetalle.Descripcion;

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", ticket.UsuarioAlta);
                replacements.Add("<IDTICKET>", iDTicket);
                replacements.Add("<MENSAJE>", mensaje);
                List<string> attachments = null;
                bool envioOk = false;
                envioOk = enviar(searchable.Value, replacements, asunto, mensaje, attachments, emailArea);
                #endregion
                
                limpiarForm();
                if (envioOk)
                    Response.Redirect("Ticket.aspx?Envio=true");
                else {
                    Response.Redirect("Ticket.aspx?Envio=false");
                }
            }       

    }

    protected void limpiarForm() {
        txtAsunto.Text = "";
        txtMensaje.Text = "";
        searchable.Value = "";

    }

    protected bool enviar(string ids, ListDictionary replacements, string asunto, string mensaje, List<string> attachments,string emailArea) {       
            using (var dbContext = new ACHEEntities()) {           
                string[] aux = ids.Split(',');
                string[] item;
                MailAddressCollection listaMail;
                int idUsuario = 0;
                string detalleErrores = string.Empty;
                string titulo = string.Empty;
                string mail = "";
                string nombreUsu = "";
                bool send = false;
                listaMail = new MailAddressCollection();
                if (aux.Length > 0) {
                    #region EnvioMasivo
                    foreach (var id in aux) {
                        if (id != "") {
                            item = id.Split('_');
                            if (item != null) {                              
                                idUsuario = int.Parse(item[0]);
                                if (item[1] == "FRANQUICIA") {
                                    UsuariosFranquicias usuFran = new UsuariosFranquicias();
                                    usuFran = dbContext.UsuariosFranquicias.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                                    nombreUsu = usuFran.Usuario;
                                    mail = usuFran.Email.Trim();
                                }
                                if (mail != "" && item[1] != "") {
                                    if (mail.Trim().IsValidEmailAddress()) {
                                        listaMail.Add(new MailAddress(mail.Trim()));
                                    }
                                }
                            }
                        }
                    }                                                    
                }
                listaMail.Add(new MailAddress(emailArea));
                try
                {
                    send = EmailHelper.SendMessage(EmailTemplate.TicketArea, replacements, ConfigurationManager.AppSettings["Email.From"], listaMail, "RedIN :: " + asunto, attachments);
                }
                catch (Exception ex)
                {
                    send = false;
                }                              
                    #endregion   
                return send;   
            }
        }

    protected void crearNuevoArchivoTemp() {
        string pathString = "~/files/tickets/temp";
        string path = Server.MapPath(pathString);
        System.IO.Directory.CreateDirectory(path);
    }

    protected void eliminarArchTemp() {
        string pathString = "~/files/tickets/temp";
        string path = Server.MapPath(pathString);
        if (System.IO.Directory.Exists(path)) {
            System.IO.Directory.Delete(path, true);
        }
    }

    protected void btnFileDelete_Click(object sender, EventArgs e) {
        eliminarArchTemp();
        Response.Redirect("Ticketn.aspx");
    }

    protected void btnFileUpload_Click(object sender, EventArgs e) {

        string pathString = "~/files/tickets/temp";
        System.IO.Directory.CreateDirectory(Server.MapPath(pathString));
        string nombre_carpeta = Server.MapPath(pathString);
        guardarArchivos(nombre_carpeta);
        DirectoryInfo di = new DirectoryInfo(nombre_carpeta);
        int j = 0;
        int max = di.GetFiles().Count();
        foreach (var fi in di.GetFiles()) {
            j++;
            //litArch.Text += fi.Name + " <button type='button' id='" + fi.Name + "' onclick=DeleteFile('" + fi.Name + "');return false;>Delete</button> <br>  ";
            //if (j < max && max > 1)
            //litArch.Text += "  <br>  ";
        }
        var imagenes = di.GetFiles().Select(x => new {
            FileName = x.Name
        });
        rptImagenes.DataSource = imagenes;
        rptImagenes.DataBind();
    }

    protected void guardarArchivos(string carpetaDestino) {
        try {
            if (file_upload.HasFile) {
                foreach (var file in file_upload.PostedFiles) {
                    string ext = System.IO.Path.GetExtension(file.FileName);
                    string uniqueName = DateTime.Now.ToString("ddMMyyyyHHmmss") + "_" + file.FileName;
                    string path = System.IO.Path.Combine(carpetaDestino, uniqueName);
                    file_upload.SaveAs(path);
                }
                lblUploadStatus.Text = "File(s) uploaded successfully.";
            }
            else {
                lblUploadStatus.Text = "Please upload proper file.";
            }
        }
        catch (Exception ex) {
            lblUploadStatus.Text = "Error in uploading file." + ex.Message;
        }
    }

    protected void guardarArchivos(TicketsDetalle ticketDetalle, string carpetaOrigen, string carpetaDestino) {
        DirectoryInfo di = new DirectoryInfo(carpetaOrigen);
        if (di.Exists) {
            foreach (var file in di.GetFiles()) {
                string nombre_carpeta = System.IO.Path.Combine(carpetaDestino, file.Name);
                file.CopyTo(nombre_carpeta);
                var ticketAdjunto = new TicketsArchivos();
                ticketAdjunto.NombreArchivo = file.Name;
                ticketDetalle.TicketsArchivos.Add(ticketAdjunto);
            }
            eliminarArchTemp();
        }
    }

}