﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections.Specialized;

public partial class login_multimarcas : PaginaMultimarcasBase {
    protected static string mensaje = "";

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

            if (!String.IsNullOrEmpty(Request.QueryString["logOut"])) {
                if (Request.QueryString["logOut"].Equals("true")) {
                    Session.Remove("CurrentMultimarcasUser");
                    Session.Remove("Detalle_Fc");
                }
            }
        }
    }

    [WebMethod(true)]
    public static void ingresar(string usuario, string pwd) {
        using (var dbContext = new ACHEEntities()) {
            var usu = dbContext.UsuariosMultimarcas.Where(x => x.Usuario == usuario && x.Pwd == pwd && x.Activo).FirstOrDefault();
            if (usu != null) {
                usu.FechaUltLogin = DateTime.Now;
                dbContext.SaveChanges();
                HttpContext.Current.Session.Remove("CurrentUser");
                HttpContext.Current.Session["CurrentMultimarcasUser"] = new WebMultimarcasUser(
                    usu.IDUsuario, usu.Email, usu.Multimarcas.Nombre, usu.IDMultimarca, usu.Usuario, "",
                    usu.Multimarcas.Color, usu.Multimarcas.MostrarSoloPOSPropios,
                    usu.Multimarcas.MostrarSoloTarjetasPropias, usu.Multimarcas.HabilitarPOSWeb,
                    usu.Multimarcas.TipoCatalogo == "C", usu.Multimarcas.HabilitarSMS,
                    usu.Multimarcas.EnvioMsjBienvenida, usu.Multimarcas.MsjBienvenida,
                    usu.Multimarcas.EnvioMsjCumpleanios, usu.Multimarcas.MsjCumpleanios,
                    usu.Multimarcas.CostoSMS, usu.Multimarcas.EnvioEmailRegistroSocio, usu.Multimarcas.EnvioEmailCumpleanios, usu.Multimarcas.EnvioEmailRegistroComercio, usu.Multimarcas.EmailRegistroSocio, usu.Multimarcas.EmailCumpleanios, usu.Multimarcas.EmailRegistroComercio,0);
            }
            else
                throw new Exception("Usuario y/o contraseña incorrecta.");
        }
    }

    [WebMethod(true)]
    public static void RecuperarDatos(string email) {

        if (!email.IsValidEmailAddress())
            throw new Exception("Email incorrect.");

        using (var dbContext = new ACHEEntities()) {
            var usu = dbContext.UsuariosMultimarcas.Where(x => x.Email == email && x.Activo).FirstOrDefault();
            if (usu != null) {
                string newPwd = string.Empty;
                newPwd = newPwd.GenerateRandom(6);

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", usu.Usuario);
                replacements.Add("<PASSWORD>", newPwd);

                bool send = EmailHelper.SendMessage(EmailTemplate.RecuperoPwd, replacements, usu.Email, "RedIN: Recupero de contraseña");
                if (!send)
                    throw new Exception("El email con su nueva contraseña no pudo ser enviado.");
                else {
                    usu.Pwd = newPwd;
                    dbContext.SaveChanges();
                }
            }
            else
                throw new Exception("El email es inexistente.");
        }
    }
}