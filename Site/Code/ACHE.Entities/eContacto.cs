﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eContacto
    {
        public int IDContacto { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cargo { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Estado { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Observaciones { get; set; }
        public string NroDocumento { get; set; }
    }
}
