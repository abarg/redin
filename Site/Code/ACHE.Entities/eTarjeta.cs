﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eTarjeta
    {
        public int IDTarjeta { get; set; }
        public int IDMarca { get; set; }
        public string Numero { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string Entidad { get; set; }
        public string Estado { get; set; }
        public string Affinity { get; set; }
        public int IDSocio { get; set; }
        public string MotivoBaja { get; set; }
        public DateTime? FechaBaja { get; set; }
        public string UsuarioBaja { get; set; }
        public int PuntosDisponibles { get; set; }
        public int PuntosTotales { get; set; }
    }
}
