﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eMarca
    {
        public int IDMarca { get; set; }
        public string Nombre { get; set; }
        public string Prefijo { get; set; }
        public DateTime FechaAlta { get; set; }
    }
}
