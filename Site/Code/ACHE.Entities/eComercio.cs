﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eComercio
    {
        public int IDComercio { get; set; }
        public string SDS { get; set; }
        public string NombreFantasia { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Responsable { get; set; }
        public string Cargo { get; set; }
        public string Actividad { get; set; }
        public string CondicionIva { get; set; }
        public string Web { get; set; }
        public string Email { get; set; }
        public DateTime FechaAlta { get; set; }
        public int IDDomicilio { get; set; }
        public int IDDomicilioFiscal { get; set; }
        public string POSTipo { get; set; }
        public string POSSistema { get; set; }
        public string POSTerminal { get; set; }
        public string POSEstablecimiento { get; set; }
        public string POSMarca { get; set; }
        public string POSObservaciones { get; set; }
        public DateTime? POSFechaReprogramacion { get; set; }
        public DateTime? POSFechaActivacion { get; set; }
        public bool POSReprogramado { get; set; }
        public string Observaciones { get; set; }
        public string Estado { get; set; }
        public string TipoMov { get; set; }
        public string NumEst { get; set; }
        public string NroNumEst { get; set; }
        public string NombreEst { get; set; }
        public string CodAct { get; set; }
        public string NumEstBenef { get; set; }
        public string EstadoBenef { get; set; }
        public string Descuento { get; set; }
        //public string ModalidadDescuento { get; set; }
        public bool ModalidadDebito { get; set; }
        public bool ModalidadCredito { get; set; }
        public bool ModalidadEfectivo { get; set; }
        public string DescuentoDescripcion { get; set; }
        public string DescuentoVip { get; set; }
        public string ModalidadDescuentoVip { get; set; }
        public string DescuentoDescripcionVip { get; set; }
        public string AffinityBenef { get; set; }
        public bool Activo { get; set; }
        public eDomicilio oDomicilio { get; set; }
        public eDomicilio oDomicilioFiscal { get; set; }
        public string FormaPago { get; set; }
        public string FormaPago_Banco { get; set; }
        public string FormaPago_TipoCuenta { get; set; }
        public string FormaPago_NroCuenta { get; set; }
        public string FormaPago_CBU { get; set; }
        public string FormaPago_Tarjeta { get; set; }
        public string FormaPago_BancoEmisor { get; set; }
        public string FormaPago_NroTarjeta { get; set; }
        public string FormaPago_FechaVto { get; set; }
        public string FormaPago_CodigoSeg { get; set; }
        public int IDContacto { get; set; }
        public eContacto oContacto { get; set; }
        public string Rubro { get; set; }
        public string FidelyUsuario { get; set; }
        public string FidelyPwd { get; set; }
        public string FidelyClaveLicencia { get; set; }
        public string CodigoDealer { get; set; }
    }
}
    