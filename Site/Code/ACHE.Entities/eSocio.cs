﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eSocio
    {
        public int IDSocio { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Sexo { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string EmpresaCelular { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Estado { get; set; }
        public int IDDomicilio { get; set; }
        public string Observaciones { get; set; }
        public string TipoReg { get; set; }
        public string TipoMov { get; set; }
        public string CodTitular { get; set; }
        public string CodResol { get; set; }
        public string AammVto { get; set; }
        public string CuentaAsoc { get; set; }
        public string ImpDisponible { get; set; }
        public DateTime Fecha { get; set; }
        public string Reservado { get; set; }
        public string FrecDisponible { get; set; }
        public eDomicilio oDomicilio { get; set; }
        public eTarjeta oTarjeta { get; set; }
        public string LimCompra { get; set; }
        public string LimAdelanto { get; set; }
        public string LimCuotas { get; set; }
        public string LimProp { get; set; }
        public string AffinityBenef { get; set; }
        public string NroCuenta { get; set; }
        public bool Activo { get; set; }
        public List<eTarjeta> ListTarjetas { get; set; }
    }
}
