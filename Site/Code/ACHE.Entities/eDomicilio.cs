﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Entities
{
    public class eDomicilio
    {
        public int IDDomicilio { get; set; }
        public string TipoDomicilio { get; set; }
        public string Pais { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Domicilio { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string PisoDepto { get; set; }
        public string Estado { get; set; }
        public DateTime FechaAlta { get; set; }
    }
}
