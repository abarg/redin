﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using ACHE.Business;


namespace ACHE.AlertaNuevosComerciosActivos
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        public static void EnviarAlerta(string emailTo)
        {
            using (var dbContext = new ACHEEntities())
            {
                var comercios = dbContext.NuevosComerciosActivosView.Select(x => x.Nombre).ToList();

                ListDictionary datos = new ListDictionary();
                var listComercios = "";
                foreach (var comercio in comercios)
                {
                    listComercios += comercio + "<br/>";
                }
                if (listComercios.Any())
                {
                    datos.Add("<LISTACOMERCIOS>", listComercios);

                    bool send = EmailHelper.SendMessage(EmailTemplate.AlertaNuevosComerciosActivos, datos, emailTo, "Alerta por nuevos comercios activos ");
                    if (!send)
                    {
                        var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                        BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por nuevos comercios activos");
                    }
                }
            }
        }
    }
}
