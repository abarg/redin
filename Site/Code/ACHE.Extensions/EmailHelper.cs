﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using ACHE.Extensions;

namespace ACHE.Model
{
    public enum EmailTemplate {
        //RedIn
        RecuperoPwd,
        EnvioFactura,
        NuevaCampaniaSMS,
        Notificacion,
        NotificacionesAlertas,
        Alertas,
        NotificacionTr,
        NotificacionTRTransaccional,
        NotificacionTxUsuario,
        //EnvioSMS y Email
        EnvioCampaniaSMS,
        EnvioCumpleanios,
        EnvioSoloHTML,
        NotificacionAlertas,
        AlertaNuevosComerciosActivos,
        Aviso,
        ConfirmacionKeyword,
        //WebSocios
        SociosSolicitudAcceso,
        SugerenciaReclamo,
        ErrorCargaVirtual,
        ErrorCanjePin,
        TicketArea,
        PagoFactura
    }

    public static class EmailHelper
    {
        //public static readonly string HOST = ConfigurationManager.AppSettings["Email.Host"] ?? "No hay host definido";
        //public static readonly int PORT = int.Parse(ConfigurationManager.AppSettings["Email.Port"]);

        public static bool SendMessage(EmailTemplate template, ListDictionary replacements, string to, string subject)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject);

            return SendMailMessage(mailMessage);
        }

        public static bool SendMessage(EmailTemplate template, ListDictionary replacements, string to, string from, string subject)
        {
            string emailFrom = from;
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject);

            return SendMailMessage(mailMessage);
        }

        public static bool SendMessage(EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string subject, List<string> attachments)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject, attachments);

            return SendMailMessage(mailMessage);
        }

        public static bool SendMessage(EmailTemplate template, ListDictionary replacements, string from, MailAddressCollection to, string subject, List<string> attachments)
        {
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject, attachments);

            return SendMailMessage(mailMessage);
        }

        private static MailMessage CreateMessage(EmailTemplate template, ListDictionary replacements, string to, string from, string cc, string subject)
        {
            string physicalPath = ConfigurationManager.AppSettings["Email.Templates"] + template.ToString() + ".txt";
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid Email Template Passed into the NotificationManager", "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
            {
                string val = param.Value == null ? "" : param.Value.ToString();
                html = html.Replace(param.Key.ToString(), val);
            }

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            mailMessage.Subject = subject;
            mailMessage.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(cc))
                mailMessage.CC.Add(new MailAddress(cc));
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = html;

            return mailMessage;
        }

        private static MailMessage CreateMessage(EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments)
        {

            string physicalPath = ConfigurationManager.AppSettings["Email.Templates"] + template.ToString() + ".txt";
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid Email Template Passed into the NotificationManager", "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from);
            mailMessage.Subject = subject;
            foreach (var mail in to)
                mailMessage.To.Add(mail);
            if (!string.IsNullOrEmpty(cc))
                mailMessage.CC.Add(new MailAddress(cc));
            string emailReply = ConfigurationManager.AppSettings["Email.ReplyTo"];
            if (!string.IsNullOrEmpty(emailReply))
                mailMessage.ReplyToList.Add(new MailAddress(emailReply));

            if (attachments != null)
            {
                foreach (string attach in attachments)
                {
                    mailMessage.Attachments.Add(new Attachment(attach));
                }
            }
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = html;
            if (from != string.Empty)
            {
                mailMessage.Headers.Add("Disposition-Notification-To", from);
                mailMessage.DeliveryNotificationOptions = System.Net.Mail.DeliveryNotificationOptions.OnSuccess;
            }

            return mailMessage;
        }

        private static bool SendMailMessage(MailMessage mailMessage)
        {
            bool send = true;

            try
            {
                SmtpClient client = new SmtpClient();
                //client.UseDefaultCredentials = true;
                //client.Credentials = new NetworkCredential("wajid849@gmail.com", "password");
                //client.EnableSsl = false;                
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                send = false;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le error", ex.ToString());
                //throw;// new Exception();
            }

            return send;
        }
    }


}