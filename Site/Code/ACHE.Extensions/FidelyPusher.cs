﻿using PusherServer;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ACHE.Extensions { 
    public class FidelyPusher
    {
        public static async Task<ActionResult> PushMessage()
        {
            var options = new PusherOptions
            {
                Cluster = "us2",
                Encrypted = true
            };

            var pusher = new Pusher(
              "618260",
              "95c40a21a900c1438033",
              "45a520f8fb79633acc9e",
              options);

            var result = await pusher.TriggerAsync(
              "my-channel",
              "my-event",
              new { message = "hello world" });

            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
        }
    }
}