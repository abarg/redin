﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;


namespace ACHE.TRTransaccional
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        static void Main()
        {
            RealizarTransaccion();
        }
        
        public static void RealizarTransaccion()
        {
          
            string NumEst = ConfigurationManager.AppSettings["NumEst"];
            string Terminal = ConfigurationManager.AppSettings["NumTerminal"];
            int cantTr = 0;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    string fecha = DateTime.Now.ToString(formato);
                    cantTr = dbContext.Database.SqlQuery<int>("exec CrearTransaccionesCostoTransaccional '" + NumEst + "','" + Terminal + "','" + fecha + "'").FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/TRTransaccional_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            
            ListDictionary datos = new ListDictionary();
            datos.Add("<CANTTOTAL>", cantTr);
            string emailTo = ConfigurationManager.AppSettings["Email.To"];
            bool send = EmailHelper.SendMessage(EmailTemplate.NotificacionTr, datos, emailTo, "Envíos de mail por transacciones finalizado");
            if (!send)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador");
            }
            
        }
    }
}
