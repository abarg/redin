﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;

namespace ACHE.Business
{
    public class bUsuario
    {
        cUsuario cUsuario;

        public Usuarios add(Usuarios oUsuario)
        {
            try
            {
                cUsuario = new cUsuario();
                
                if (oUsuario.IDUsuario == 0) //insert
                {
                    if (this.existeUsuarioPorUsuario(oUsuario.Usuario))
                    {
                        throw new Exception("Ya existe un Usuario con el Nombre de Usuario ingresado");
                    }
                    
                    if (this.existeUsuario(oUsuario.Email))
                    {
                        throw new Exception("Ya existe un Usuario con el Email ingresado");
                    }

                    cUsuario.insertUsuario(oUsuario);
                }
                else //update
                {
                    cUsuario.updateUsuario(oUsuario);
                }

                return oUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuarios> getUsuarios()
        {
            List<Usuarios> listUsuarios;
            try
            {
                cUsuario = new cUsuario();
                listUsuarios = cUsuario.getUsuarios();
                
                return listUsuarios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void deleteUsuario(int IDUsuario)
        {
            try
            {
                cUsuario = new cUsuario();
                cUsuario.deleteUsuario(IDUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuarios getUsuario(string Usuario, string Pwd)
        {
            Usuarios oUsuario;
            try
            {
                cUsuario = new cUsuario();
                oUsuario = cUsuario.getUsuario(Usuario, Pwd);

                return oUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuarios getUsuario(string Email)
        {
            Usuarios oUsuario;
            try
            {
                cUsuario = new cUsuario();
                oUsuario = cUsuario.getUsuario(Email);

                return oUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuarios getUsuario(int IDUsuario)
        {
            Usuarios oUsuario;
            try
            {
                cUsuario = new cUsuario();
                oUsuario = cUsuario.getUsuario(IDUsuario);

                return oUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeUsuario(string Email)
        {
            Usuarios oUsuario;
            try
            {
                cUsuario = new cUsuario();
                oUsuario = cUsuario.getUsuario(Email);

                return (oUsuario != null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeUsuarioPorUsuario(string Usuario)
        {
            Usuarios oUsuario;
            try
            {
                cUsuario = new cUsuario();
                oUsuario = cUsuario.getUsuarioPorUsuario(Usuario);

                return (oUsuario != null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
