﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bFranquicia
    {
        cFranquicia cFranquicia;
        
        public List<Franquicias> getFranquicias()
        {
            List<Franquicias> list;
            try
            {
                cFranquicia = new cFranquicia();
                list = cFranquicia.getFranquicias();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
    }
}
