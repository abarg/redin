﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bTokens
    {

        public static PaymentGateawayTokens getTokens(string publicKey)
        {

            using (var dbContext = new ACHEEntities()) {

                var tokens = dbContext.PaymentGateawayTokens.Include("Comercios").Where(x => x.PublicKey == publicKey).FirstOrDefault();

                return tokens;

            }

        }
       
    }
}
