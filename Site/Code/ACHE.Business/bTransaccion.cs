﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bTransaccion
    {
        cTransaccion cTransaccion;

        public void limpiarTransaccionesTmp()
        {
            try
            {
                cTransaccion = new cTransaccion();
                cTransaccion.limpiarTransaccionesTmp();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarTransaccionesTmp(DataTable dt)
        {
            try
            {
                cTransaccion = new cTransaccion();
                cTransaccion.insertarTransaccionesTmp(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarTransacciones(string nombreArchivo, int idUsuario)
        {
            try
            {
                cTransaccion = new cTransaccion();
                cTransaccion.actualizarTransacciones(nombreArchivo, idUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
