﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ACHE.Model;
using ACHE.Extensions;

public class PlusMobile {
    public static bool SendSMS(string Mensaje, TelefonoSMS Telefono) {
        string MsgRefId = ConfigurationManager.AppSettings["Plusmobile.IdEnvio"];
        
        int IdSmsCode = 0;

        if (Telefono.EmpresaCelular.Trim() != "")
        {
            switch (Telefono.EmpresaCelular.Trim().ToLower())
            {
                case "personal":
                    IdSmsCode = 14;
                    break;
                case "movistar":
                    IdSmsCode = 15;
                    break;
                case "claro":
                    IdSmsCode = 16;
                    break;
                case "nextel":
                    IdSmsCode = 22;
                    break;
            }
        }
        else if (Telefono.IdSmsCode > 0)
            IdSmsCode = Telefono.IdSmsCode;

        /*23123 (14) AR Personal OnDemand KeyWords
        23123 (15) AR Movistar OnDemand KeyWords
        23123 (16) AR Claro OnDemand KeyWords
        23123 (22) AR Nextel OnDemand KeyWords
        [03:59:53 p.m.] Plusmobile - germantanhauzer: el nro que esta al lado del nro corto es el smscode
        [03:59:57 p.m.] Plusmobile - germantanhauzer: 14,15,16,22*/

        var sms = MsgRefId + " - " + Telefono.Celular + " - " + Mensaje + " - " + IdSmsCode + " (" + Telefono.EmpresaCelular.Trim().ToLower() + ")";
        BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogResponse"], "Data: ", sms);

        if (!string.IsNullOrEmpty(MsgRefId) && !string.IsNullOrEmpty(Telefono.Celular) && !string.IsNullOrEmpty(Mensaje) && IdSmsCode > 0) {
            ACHE.Business.wsPlusmobile.AuthHeader Auth = new ACHE.Business.wsPlusmobile.AuthHeader();
            Auth.User = ConfigurationManager.AppSettings["Plusmobile.User"];
            Auth.Pass = ConfigurationManager.AppSettings["Plusmobile.Pwd"];
            ACHE.Business.wsPlusmobile.GatewaySoapClient Gateway = new ACHE.Business.wsPlusmobile.GatewaySoapClient();
            try {
                string mensajeXml = Gateway.sendMessage(Auth, Telefono.Celular.Trim(), IdSmsCode, Mensaje);
                BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogResponse"], "Response: ", mensajeXml);
                string id = mensajeXml.Split(new string[] { "<Id>" }, StringSplitOptions.None)[1];
                id = id.Split(new string[] { "</Id>" }, StringSplitOptions.None)[0];
                if (id == "0" || id == "10000" || id == "10001" || id == "10002" || id == "10003" || id == "10004" || id == "10005") {
                    string error = mensajeXml.Split(new string[] { "<Detalle>" }, StringSplitOptions.None)[1];
                    error = error.Split(new string[] { "</Detalle>" }, StringSplitOptions.None)[0];
                    return false;
                }
            }
            catch (Exception ex) {
                BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogResponse"], "Exception: ", ex.Message);
                return false;
            }
        }
        return true;
    }

    public static List<EnvioSMS> SendSMS(string Mensaje, List<TelefonoSMS> Telefonos) {
        List<EnvioSMS> result = new List<EnvioSMS>();
        var sms = "0 - " + Telefonos.Count() + " teléfonos - " + Mensaje + " - 0";
        BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogResponse"], "Data: ", sms);
        if (!string.IsNullOrEmpty(Mensaje)) {
            foreach (var tel in Telefonos) {
                try
                {
                    TelefonoSMS telefono = new TelefonoSMS();
                    EnvioSMS envio = new EnvioSMS();
                    telefono.Celular = envio.Celular = tel.Celular;
                    telefono.EmpresaCelular = tel.EmpresaCelular;
                    envio.Enviado = SendSMS(Mensaje, telefono);
                    envio.FechaEnvio = DateTime.Now;
                    result.Add(envio);
                }
                catch (Exception ex)
                {
                    BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogResponse"], "Error en : ", tel.Celular);
                }
            }
        }
        return result;
    }

    public static void SendSMSBienvenida(int? IDMarca, decimal CostoSMS, string Mensaje, string Telefono, string Empresa) {
        if (!string.IsNullOrEmpty(Telefono)) {
            if (!string.IsNullOrEmpty(Mensaje)) {
                
                TelefonoSMS aux = new TelefonoSMS();
                aux.Celular = Telefono;
                aux.EmpresaCelular = Empresa;
                bool sms = PlusMobile.SendSMS(Mensaje, aux);
                
                using (var dbContext = new ACHEEntities()) {
                    SMSEnvios entity = new SMSEnvios();
                    entity.IDMarca = IDMarca;
                    entity.FechaEnvio = DateTime.Now;
                    entity.PhoneNumber = Telefono;
                    entity.Costo = CostoSMS;
                    entity.Tipo = "BIE";
                    entity.Enviado = sms;

                    dbContext.SMSEnvios.Add(entity);
                    dbContext.SaveChanges();
                }
                if (!sms)
                    throw new Exception("La tarjeta ha sido asignada, pero no se ha podido enviar SMS de bienvenida.");
            }
        }
    }
}