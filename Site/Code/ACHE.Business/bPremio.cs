﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bPremio
    {
        cPremio cPremio;

        public void limpiarPremiosTmp()
        {
            try
            {
                cPremio = new cPremio();
                cPremio.limpiarPremiosTmp();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Premios getPremio(int IDComercio)
        {
            Premios Premio;
            try
            {
                cPremio = new cPremio();
                Premio = cPremio.getPremio(IDComercio);

                return Premio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Premios getPremioPorImporte(decimal importe)
        {
            Premios Premio;
            try
            {
                cPremio = new cPremio();
                Premio = cPremio.getPremioPorImporte(importe);

                return Premio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarPremiosTmp(DataTable dt,string path)
        {
            try
            {
                cPremio = new cPremio();
                cPremio.insertarPremiosTmp(dt,path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarPremios(bool esPrivado)
        {
            try
            {
                cPremio = new cPremio();
                cPremio.actualizarPremios(esPrivado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
