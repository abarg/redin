﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;


namespace ACHE.Business {
    public class bMultimarca {
        cMultimarca cMultimarca;

        public List<Marcas> getMarcas(int idMultimarca) {
            List<Marcas> list;
            try {
                cMultimarca = new cMultimarca();
                list = cMultimarca.getMarcas(idMultimarca);

                return list;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
