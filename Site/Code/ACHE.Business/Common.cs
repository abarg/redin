﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using Image = System.Drawing.Image;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Configuration;
using ACHE.FacturaElectronica;
using ACHE.FacturaElectronica.Lib;
using System.Data.Entity;


namespace ACHE.Business
{
    public static class Common
    {
        public static string CrearTransaccion(ACHEEntities dbContext, DateTime fechaTransaccion, int idTerminal, string origen, string codigoPremio, string descripcion, decimal? importe
        , string numCupon, string numEst, string numReferencia, string numRefOriginal, string numTarjetaCliente, string numTerminal, string operacion, string puntosDisponibles, string tipoMensaje
        , string puntoDeVenta, string nroComprobante, string tipoComprobante, string usuario)
        {
            string idTransaccion = "";
            int descuento = 0;

            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(numTarjetaCliente, false);
            if (oTarjeta == null)
                throw new Exception("El Número de Tarjeta no existe");
            else
            {
                string codigo = "";
                if (operacion == "Canje" && tipoMensaje != "3300")
                {
                    bPremio bPremio = new bPremio();
                    Premios premio = bPremio.getPremioPorImporte(importe ?? 0);

                    if (premio == null)
                        throw new Exception("No existe un premio para el importe ingresado");

                    codigo = premio.Codigo;
                    int puntosTarjeta = (int)oTarjeta.Credito; /* * 100*/

                    if (premio.ValorPesos > puntosTarjeta)
                        throw new Exception("La tarjeta no tiene crédito suficiente. Crédito actual: " + oTarjeta.Credito.ToString("N2"));
                }

                if (operacion == "Canje" && tipoMensaje == "3300")
                { //Gestion de canjes para marcas privadas
                    var producto = dbContext.Productos.Where(x => x.Precio == importe).FirstOrDefault();

                    if (producto == null)
                        throw new Exception("No existe un premio para el importe ingresado");

                    int puntosTarjeta = (int)(oTarjeta.PuntosTotales);

                    if (producto.Puntos > puntosTarjeta)
                        throw new Exception("La tarjeta no tiene puntos suficientes. Puntos actuales: " + oTarjeta.PuntosTotales.ToString("N2"));

                    tipoMensaje = "1100";
                }

                //bComercio bComercio = new bComercio();
                //Comercios comercio = bComercio.getComercio(idComercio);
                Terminales terminal = dbContext.Terminales.Where(t => t.IDTerminal == idTerminal).FirstOrDefault();

                if (terminal == null)
                    throw new Exception("La terminal no existe");

                Transacciones tr = new Transacciones();
                tr.FechaTransaccion = fechaTransaccion;
                tr.Origen = origen;
                tr.CodigoPremio = codigoPremio;
                tr.Descripcion = descripcion;

                if (tipoMensaje != "3300") //no es plusmobile
                {
                    descuento = bComercio.obtenerDescuento(terminal, dbContext, oTarjeta);
                    numTerminal = terminal.POSTerminal;
                    numEst = terminal.NumEst.PadLeft(15, '0');//"Web";
                }

                tr.Descuento = descuento;
                tr.Importe = importe;

                if (operacion != "Canje" && operacion != "Carga" && operacion != "Descarga")
                    tr.ImporteAhorro = (importe * descuento) / 100;
                else
                    tr.ImporteAhorro = 0;

                tr.NumCupon = numCupon;
                tr.NumEst = numEst;
                tr.NumReferencia = numReferencia;
                tr.NumRefOriginal = numRefOriginal;
                tr.NumTarjetaCliente = numTarjetaCliente;
                tr.NumTerminal = null;
                tr.Operacion = operacion;

                int puntosAContabilizar = (int)(importe - tr.ImporteAhorro);

                if (operacion == "Canje" || operacion == "CuponIN" || operacion == "Carga" || operacion == "Descarga")
                    puntosAContabilizar = puntosAContabilizar * 100;
                else if (operacion != "Carga" && operacion != "Descarga")
                {
                    decimal POSpuntos = bComercio.obtenerPuntos(terminal, oTarjeta, dbContext);
                    tr.Puntos = POSpuntos;
                    puntosAContabilizar = puntosAContabilizar * bComercio.obtenerMulPuntos(terminal, dbContext, numTarjetaCliente);
                }
                //else
                //    tr.Puntos = terminal.GiftcardGeneraPuntos ? 1 : 0;

                if (operacion == "Venta" || operacion == "Carga")
                    tr.PuntosAContabilizar = puntosAContabilizar;
                else
                    tr.PuntosAContabilizar = puntosAContabilizar * -1; // anulacion

                tr.PuntosDisponibles = puntosDisponibles;
                tr.PuntosIngresados = (importe.ToString().Replace(",", "")).PadLeft(12, '0');
                tr.TipoMensaje = tipoMensaje;
                tr.PuntoDeVenta = puntoDeVenta;
                tr.NroComprobante = nroComprobante;
                tr.TipoComprobante = tipoComprobante;
                tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, oTarjeta);
                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;
                switch (operacion)
                {
                    case "Anulacion":
                        tr.TipoTransaccion = "220000";
                        break;
                    case "Canje":
                        tr.TipoTransaccion = "000005";
                        tr.Puntos = 1;
                        tr.CodigoPremio = codigo;
                        tr.Arancel = 0;
                        tr.UsoRed = 0;
                        break;
                    case "Venta":
                        tr.TipoTransaccion = "000000";
                        break;
                    case "CuponIn":
                        tr.TipoTransaccion = "300000";
                        tr.Puntos = 1;
                        break;
                    case "Descarga":
                        tr.TipoTransaccion = "220005";
                        tr.Arancel = terminal.GifcardArancelDescarga;
                        tr.UsoRed = terminal.GiftcardCobrarUsoRed ? terminal.CostoGifcard : 0;
                        tr.Puntos = 0;
                        tr.Descuento = 0;
                        break;
                    case "Carga":
                        tr.TipoTransaccion = "000005";
                        tr.CodigoPremio = "999999";
                        tr.Arancel = terminal.GifcardArancelCarga;
                        tr.UsoRed = terminal.GiftcardCobrarUsoRed ? terminal.CostoGifcard : 0;
                        tr.Puntos = terminal.GiftcardGeneraPuntos ? 1 : 0;
                        tr.Descuento = 0;
                        break;
                    default:
                        break;
                }


                tr.IDMarca = oTarjeta.IDMarca;
                tr.IDFranquicia = oTarjeta.IDFranquicia;
                tr.IDComercio = terminal.IDComercio;
                tr.IDTerminal = terminal.IDTerminal;

                tr.Usuario = usuario;


                dbContext.Transacciones.Add(tr);

                try { 
                dbContext.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {

                            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName + " -- " + ve.ErrorMessage);

                        }
                    }
                    throw;
                }


                idTransaccion = tr.IDTransaccion.ToString();
                dbContext.ActualizarPuntosPorTarjeta(numTarjetaCliente);
            }

            return idTransaccion;
        }


        public static int Reverse(ACHEEntities dbContext, Transacciones transaccion, string tipo)
        {

            int result = -501;

            try
            { 
                result = ReverseOperacion(dbContext, transaccion, tipo);
            }
            catch(Exception e)
            {
                result = -501;
            }

            return result;
        }

        private static bool validarTarjeta(Transacciones tx, Tarjetas oTarjeta)
        {
            // giftcard
            if ((oTarjeta.TipoTarjeta == "G" && (tx.Operacion == "Carga" || tx.Operacion == "Descarga" || tx.Operacion == "Anulacion")))        
                return true;
            
            // beneficios
            if ((oTarjeta.TipoTarjeta == "B" && (tx.Operacion == "Venta" || tx.Operacion == "Canje" || tx.Operacion == "Anulacion")))
                return true;

            return false;

        }

        private static int ReverseOperacion(ACHEEntities dbContext, Transacciones transaccion, string tipo)
        {

            int result = -501;

            Transacciones trVieja;

            //Valido que no se haya anulado previamente (al menos que sea un reverso de anulacion forzado)
            if(tipo == "Cancel")
            {
                trVieja = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == transaccion.IDTransaccion.ToString() && x.Descripcion.Substring(0, 7) == "Reverso").FirstOrDefault();
            }
            else
            {
                trVieja = dbContext.Transacciones.Where(x => x.Operacion == "Anulacion" && x.NumRefOriginal == transaccion.IDTransaccion.ToString()).FirstOrDefault();
            }

            if (trVieja == null)
            {
                var terminal = dbContext.Terminales.Include("Comercios").Where(x => x.POSTerminal == transaccion.Terminales.POSTerminal && x.Activo).FirstOrDefault();
                if (terminal != null)
                {
                    bTarjeta bTarjeta = new bTarjeta();
                    Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(transaccion.NumTarjetaCliente, false);
                    if (oTarjeta != null && (oTarjeta.FechaBaja == null || (oTarjeta.FechaBaja.HasValue && oTarjeta.FechaBaja.Value > DateTime.Now)))
                    {
                        if(validarTarjeta(transaccion, oTarjeta)) { 

                            Transacciones tr = new Transacciones();

                            tr = CrearTransaccionReverso(tr, transaccion, terminal, oTarjeta);

                            bool isValid = true;

                            if (isValid)
                            {

                                dbContext.Transacciones.Add(tr);
                                dbContext.SaveChanges();
                                dbContext.ActualizarPuntosPorTarjeta(transaccion.NumTarjetaCliente);

                                result = 0;

                            }
                            else
                            {
                                result = -104;
                            }
                        }
                        else
                        {
                            result = -100;
                        }
                    }
                    else
                    {
                        result = -100;
                    }
                }
                else
                {
                    result = -101;
                }
            }
            else
            {
                result = -106;
            }

            return result;
        }

        private static Transacciones CrearTransaccionReverso(Transacciones tr, Transacciones transaccion, Terminales terminal, Tarjetas tarjeta)
        {

            tr.FechaTransaccion = DateTime.Now;
            tr.Origen = "POS";
            tr.CodigoPremio = "";

            tr.Descripcion = "Reverso por Operacion de " + transaccion.Operacion; // IMPORTANTE: NO MODIFICAR DESCRIPCION. DEBIDO A VALIDACION POR SUBSTR EN REVERSO Y CANCELACION

            if (tarjeta.TipoTarjeta == "B")
            {
                // tipo mensaje 2200 para giftcard, 1100 para beneficios por la funcion actualizar puntos en db
                tr.TipoMensaje = "1100";
            }
            else
            {
                tr.TipoMensaje = "2200";
            }
            tr.Descuento = transaccion.Descuento;
            tr.Importe = transaccion.Importe;
            if (tr.Descuento.HasValue && tr.Descuento.Value > 0)
                tr.ImporteAhorro = (transaccion.Importe * tr.Descuento.Value) / 100;
            else
                tr.ImporteAhorro = 0;
            tr.NumCupon = "";
            tr.NumEst = terminal.NumEst.PadLeft(15, '0');
            tr.NumReferencia = "";
            tr.NumRefOriginal = transaccion.IDTransaccion.ToString();
            tr.NumTarjetaCliente = transaccion.NumTarjetaCliente;
            tr.NumTerminal = transaccion.Terminales.POSTerminal;
            tr.Operacion = "Anulacion";
            //tr.PuntosAContabilizar = (int)(datos.totalPrice) * -1;
            tr.PuntosAContabilizar = transaccion.PuntosAContabilizar * -1;
            tr.PuntosDisponibles = transaccion.PuntosDisponibles;
            tr.PuntosIngresados = (transaccion.Importe.ToString().Replace(",", "")).PadLeft(12, '0');

            tr.PuntoDeVenta = "";

            tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

            tr.NroComprobante = "";
            tr.TipoComprobante = "Ticket";
            tr.TipoTransaccion = "220000";//preguntar
            tr.Puntos = transaccion.Puntos;
            tr.Arancel = transaccion.Arancel;
            tr.IDMarca = terminal.Comercios.IDMarca;
            tr.IDFranquicia = terminal.Comercios.IDFranquicia;
            tr.IDComercio = terminal.IDComercio;
            tr.IDTerminal = terminal.IDTerminal;
            tr.Usuario = "POS";
            tr.TransaccionesDetalle = new List<TransaccionesDetalle>();


            return tr;
        }

        private static int ReverseAnulacion(ACHEEntities dbContext, Transacciones transaccion)
        {
            return 0;
        }


        public static string CrearTransaccionCanje(ACHEEntities dbContext, DateTime fechaTransaccion, int idTerminal, string origen, string codigoPremio, string descripcion, decimal? importe
             , string numEst, string numTarjetaCliente, string numTerminal, string operacion, string puntosDisponibles, string tipoMensaje, string usuario, int cantidadProductos, int idProducto, string numRefOriginal)
        {
            string idTransaccion = "";
            int descuento = 0;

            bTarjeta bTarjeta = new bTarjeta();
            Tarjetas oTarjeta = bTarjeta.getTarjetaPorNumero(numTarjetaCliente, false);
            if (oTarjeta == null)
                throw new Exception("El Número de Tarjeta no existe");
            else
            {
                string codigo = "";
                var producto = dbContext.Productos.Where(x => x.IDProducto == idProducto).FirstOrDefault();

                if (operacion == "Canje")
                {
                    if (producto == null)
                        throw new Exception("No existe un premio para el importe ingresado");

                    int puntosTarjeta = (int)(oTarjeta.PuntosTotales);

                    if ((producto.Puntos * cantidadProductos) > puntosTarjeta)
                        throw new Exception("La tarjeta no tiene puntos suficientes. Puntos actuales: " + oTarjeta.PuntosTotales.ToString("N2"));
                }

                //bComercio bComercio = new bComercio();
                //Comercios comercio = bComercio.getComercio(idComercio);

                Terminales terminal = dbContext.Terminales.Where(t => t.IDTerminal == idTerminal).FirstOrDefault();

                if (terminal == null)
                    throw new Exception("No existe una terminal con ese ID");

                Transacciones tr = new Transacciones();
                tr.FechaTransaccion = fechaTransaccion;
                tr.Origen = origen;
                tr.CodigoPremio = codigoPremio;
                tr.Descripcion = descripcion;

                if (tipoMensaje != "3300") //no es plusmobile
                {
                    descuento = bComercio.obtenerDescuento(terminal, dbContext, oTarjeta);
                    numTerminal = terminal.POSTerminal;
                }

                tr.Descuento = descuento;
                tr.Importe = importe;

                if (operacion != "Canje" && operacion != "Carga")
                    tr.ImporteAhorro = (importe * descuento) / 100;
                else
                    tr.ImporteAhorro = 0;

                tr.NumCupon = "";
                tr.NumEst = numEst;
                tr.NumReferencia = "";
                tr.NumRefOriginal = numRefOriginal;
                tr.NumTarjetaCliente = numTarjetaCliente;
                tr.NumTerminal = numTerminal;
                tr.Operacion = operacion;

                //int puntosAContabilizar = (int)(importe - tr.ImporteAhorro);
                //if (operacion == "Canje" ||  operacion == "Carga")
                //    puntosAContabilizar = puntosAContabilizar * 100 * cantidadProductos;

                int puntosAContabilizar = producto.Puntos * cantidadProductos;

                if (operacion == "Carga")
                    tr.PuntosAContabilizar = puntosAContabilizar;
                else
                    tr.PuntosAContabilizar = puntosAContabilizar * -1;

                tr.PuntosDisponibles = puntosDisponibles;
                tr.PuntosIngresados = (importe.ToString().Replace(",", "")).PadLeft(12, '0');
                tr.TipoMensaje = tipoMensaje;
                tr.PuntoDeVenta = "";
                tr.NroComprobante = "";
                tr.TipoComprobante = "";
                tr.Arancel = bComercio.obtenerArancel(terminal, dbContext, oTarjeta);
                tr.UsoRed = terminal.CobrarUsoRed ? terminal.CostoPOSWeb : 0;

                switch (operacion)
                {
                    case "Canje":
                        tr.TipoTransaccion = "000005";
                        tr.Puntos = 1;
                        tr.CodigoPremio = codigo;
                        tr.Arancel = 0;
                        tr.UsoRed = 0;
                        break;
                    case "Carga":
                        tr.TipoTransaccion = "000005";
                        tr.CodigoPremio = "999999";
                        tr.Arancel = terminal.GifcardArancelCarga;
                        tr.UsoRed = terminal.GiftcardCobrarUsoRed ? terminal.CostoGifcard : 0;
                        tr.Puntos = terminal.GiftcardGeneraPuntos ? 1 : 0;
                        tr.Descuento = 0;
                        break;
                    default:
                        break;
                }


                tr.IDMarca = oTarjeta.IDMarca;
                tr.IDFranquicia = oTarjeta.IDFranquicia;
                tr.IDComercio = terminal.IDComercio;
                tr.IDTerminal = terminal.IDTerminal;
                tr.Usuario = usuario;
                dbContext.Transacciones.Add(tr);
                dbContext.SaveChanges();


                idTransaccion = tr.IDTransaccion.ToString();
                dbContext.ActualizarPuntosPorTarjeta(numTarjetaCliente);
            }

            return idTransaccion;
        }
        
        public static void CrearArchivoDetalleFacturacion(ACHEEntities dbContext, HttpContext context, List<FacturasDetViewModel> detalle, Comercios comercio, int idComercio, int idFc, DateTime dtDesde, DateTime dtHasta)
        {
            //Creo el pdf
            string path = context.Server.MapPath("/files/fc-detalles");
            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream(path + "/" + idFc.ToString("#00000000") + ".pdf", FileMode.Create));
            doc.Open();

            //Se usa para saber cuanto se ahorro
            var factNeto = detalle.Sum(x => x.Ticket);

            #region Header

            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(context.Server.MapPath("~/img/redinlogo.jpg"));
            gif.Alignment = iTextSharp.text.Image.MIDDLE_ALIGN;
            gif.ScalePercent(50f);
            doc.Add(gif);

            Paragraph p = new Paragraph("Razon Social: " + comercio.RazonSocial, new Font(Font.BOLD, 9F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            p = new Paragraph("Nombre Fantasía: " + comercio.NombreFantasia, new Font(Font.BOLD, 9F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            p = new Paragraph("Período: " + dtDesde.ToString("dd/MM/yyyy") + " - " + dtHasta.ToString("dd/MM/yyyy"), new Font(Font.BOLD, 9F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            p = new Paragraph("CUIT: " + comercio.NroDocumento, new Font(Font.BOLD, 9F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            //p = new Paragraph("Terminal: ", new Font(Font.BOLD, 9F));
            //p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            //doc.Add(p);

            p = new Paragraph(" ");
            doc.Add(p);

            p = new Paragraph("RESUMEN DE OPERACIONES RED IN", new Font(Font.BOLD, 16F));
            p.Alignment = 1;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            p = new Paragraph(" ");
            doc.Add(p);

            p = new Paragraph("A través de RED IN la facturación neta de su comercio fue la siguiente: $" + Math.Round(factNeto, 2).ToString("N2"), new Font(Font.HELVETICA, 10F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            #endregion

            #region Detalle

            string[] col = { "Fecha", "Tarjeta", "Operación", "Ticket", "Arancel", "Puntos", "Costo Red", "Neto Grabado", "IVA", "Total" };
            PdfPTable table = new PdfPTable(10);
            float[] widths = new float[] { 100f, 150f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;
            table.SpacingBefore = 10;
            PdfPCell cell;

            for (int i = 0; i < col.Length; ++i)
            {
                cell = new PdfPCell(new Phrase(col[i], new Font(Font.HELVETICA, 7F)));
                cell.BackgroundColor = new BaseColor(204, 204, 204);
                cell.HorizontalAlignment = 1;
                table.AddCell(cell);
            }

            foreach (var det in detalle)
            {
                cell = new PdfPCell(new Phrase(det.Fecha, new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(det.Tarjeta, new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(det.Operacion, new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 1;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.Ticket.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.Arancel.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.Puntos.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.CostoRed.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.NetoGrabado.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.Iva.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("$" + det.Total.ToString("N2"), new Font(Font.HELVETICA, 7F)));
                cell.HorizontalAlignment = 2;
                table.AddCell(cell);
            }

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("TOTAL", new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.Ticket), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.Arancel), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.Puntos), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.CostoRed), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.NetoGrabado), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.Iva), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("$" + MathExt.Round(detalle.Sum(x => x.Total), 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cell.HorizontalAlignment = 2;
            cell.BackgroundColor = new BaseColor(204, 204, 204);
            table.AddCell(cell);

            doc.Add(table);

            #endregion

            var factura = dbContext.Facturas.Where(x => x.IDFactura == idFc).First();

            #region TOTALES

            PdfPTable tableFoot = new PdfPTable(3);
            tableFoot.WidthPercentage = 100;
            tableFoot.SpacingBefore = 10;

            var auxListComercios = dbContext.Comercios.Include("Terminales").Where(x => x.NroDocumento == factura.NroDocumento).ToList();
            //var costoPOSPropioList = dbContext.Terminales.Where(x => x.IDComercio == idComercio).ToList();//.Sum(x => x.CostoPOSPropio);
            //if (costoPOSPropioList.Any())
            //    costoPOSPropio = costoPOSPropioList.Sum(x => x.CostoPOSPropio);
            decimal costoPOSPropio = 0;
            foreach (var com in auxListComercios)
            {
                foreach (var term in com.Terminales)
                {
                    costoPOSPropio += term.CostoPOSPropio;
                }
            }



            decimal costosFijos = costoPOSPropio + (comercio.CostoFijo ?? 0);
            decimal totalNeto = MathExt.Round(detalle.Sum(x => x.NetoGrabado), 2) + costosFijos;
            //  decimal totalIva = 0;
            decimal totalIva = detalle.Sum(x => x.Iva) + (costosFijos * 0.21M);
            //if (factura.Tipo == "RI")

            //   totalIva = totalNeto * 0.21M; //Math.Round(detalle.Sum(x => x.Iva), 2);
            decimal total = totalNeto + totalIva;// Math.Round(detalle.Sum(x => x.Total), 2);
            decimal totalCanjes = MathExt.Round(Math.Abs(detalle.Where(x => x.Operacion == "Canje").Sum(x => x.NetoGrabado)), 2);
            //decimal totalADebitar = total - (totalCanjes * 1.21M);

            decimal totalADebitar = factura.ImporteTotal;

            var marca = dbContext.Marcas.Where(x => x.IDMarca == comercio.IDMarca).FirstOrDefault();
            if (marca != null)
            {
                if (marca.TipoTarjeta.ToUpper() != "FIDELIZACION")
                    totalADebitar -= (totalCanjes * 1.21M);
            }
            else
                totalADebitar -= (totalCanjes * 1.21M);

            factura.ImporteADebitar = totalADebitar;// > 0 ? totalADebitar : 0;
            /*PdfPCell cellFoot = new PdfPCell(new Phrase("TOTAL $" + totalNeto.ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cellFoot.HorizontalAlignment = 1;
            cellFoot.BackgroundColor = new BaseColor(204, 204, 204);
            tableFoot.AddCell(cellFoot);

            if (factura.Tipo == "RI")
                cellFoot = new PdfPCell(new Phrase("IVA $" + MathExt.Round(totalIva, 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            else
                cellFoot = new PdfPCell(new Phrase("", new Font(Font.HELVETICA, 7F)));
            cellFoot.HorizontalAlignment = 1;
            cellFoot.BackgroundColor = new BaseColor(204, 204, 204);
            tableFoot.AddCell(cellFoot);*/

            /*PdfPCell cellFoot = new PdfPCell(new Phrase("TOTAL CANJES $" + Math.Abs(totalCanjes).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cellFoot.HorizontalAlignment = 1;
            cellFoot.BackgroundColor = new BaseColor(204, 204, 204);
            tableFoot.AddCell(cellFoot);

            cellFoot = cellFoot = new PdfPCell(new Phrase("TOTAL A DEBITAR $" + MathExt.Round(total - totalCanjes, 2).ToString("N2"), new Font(Font.HELVETICA, 7F)));
            cellFoot.HorizontalAlignment = 1;
            cellFoot.BackgroundColor = new BaseColor(204, 204, 204);
            tableFoot.AddCell(cellFoot);

            doc.Add(tableFoot);*/

            //POS Propio
            //decimal posPropio = dbContext.Comercios.Where(x => x.NroDocumento == comercio.NroDocumento).Sum(x => x.CostoPOSPropio);
            if (costoPOSPropio > 0)
            {
                p = new Paragraph("ABONO POS Propio $ " + MathExt.Round(Math.Abs(costoPOSPropio * 1.21M), 2).ToString("N2"), new Font(Font.BOLD, 11F));
                p.Alignment = 0;//0=Left, 1=Centre, 2=Right
                doc.Add(p);
            }

            //Abono
            decimal abonoRedin = 0;
            var auxAbono = dbContext.FacturasDetalle.Where(x => x.IDFactura == factura.IDFactura && x.Concepto == "ABONO FIJO - Costo mínimo de abono Red-IN").FirstOrDefault();
            if (auxAbono != null)
                abonoRedin = auxAbono.PrecioUnitario;
            if (abonoRedin > 0)
            {
                p = new Paragraph("ABONO FIJO $ " + MathExt.Round(Math.Abs(abonoRedin * 1.21M), 2).ToString("N2"), new Font(Font.BOLD, 11F));
                p.Alignment = 0;//0=Left, 1=Centre, 2=Right
                doc.Add(p);
            }

            //Canjes
            if (totalCanjes > 0)
            {
                p = new Paragraph("TOTAL DE CANJES $" + Math.Abs(totalCanjes).ToString("N2") + " - ORDEN DE PAGO NRO " + idFc.ToString("#00000000"), new Font(Font.BOLD, 11F));
                p.Alignment = 0;//0=Left, 1=Centre, 2=Right
                doc.Add(p);
            }

            p = new Paragraph("TOTAL A DEBITAR $ " + MathExt.Round(Math.Abs(totalADebitar), 2).ToString("N2"), new Font(Font.BOLD, 11F));
            p.Alignment = 0;//0=Left, 1=Centre, 2=Right
            doc.Add(p);

            //AdjuntarPiePagina(nombreArchivoRuta,factura,dbContext,doc);            

            #endregion

            doc.Close();


            //Si hay canjes creo la orden de pago
            string[] readerList = new string[2];
            readerList[0] = (path + "/" + idFc.ToString("#00000000") + ".pdf");
            if (totalCanjes > 0 && marca.TipoTarjeta.ToUpper() != "FIDELIZACION")
            {
                var detalleList = detalle.Where(x => x.Operacion == "Canje").ToList();

                CrearOrdenDePago(idFc.ToString("#00000000"), comercio, detalleList, path + "/" + idFc.ToString("#00000000") + "_op.pdf");
                readerList[1] = (path + "/" + idFc.ToString("#00000000") + "_op.pdf");
                CombineMultiplePDFs(readerList, path + "/" + idFc.ToString("#00000000") + "_v2.pdf");//Mergeo los 2 archivos pdf
                factura.ArchivoDetalle = idFc.ToString("#00000000") + "_v2.pdf";
                File.Delete(path + "/" + idFc.ToString("#00000000") + "_op.pdf");//Elimino archivo tmp
                File.Delete(path + "/" + idFc.ToString("#00000000") + ".pdf");//Elimino archivo tmp

                OrdenesDePago op = new OrdenesDePago();
                op.IDFactura = idFc;
                op.Numero = idFc.ToString("#00000000");

                decimal opImporte = Math.Abs(detalleList.Sum(x => x.NetoGrabado));
                //decimal opIva = Math.Abs(detalleList.Sum(x => x.Iva));

                op.Importe = opImporte;

                dbContext.OrdenesDePago.Add(op);
            }
            else
                factura.ArchivoDetalle = idFc.ToString("#00000000") + ".pdf";

            dbContext.SaveChanges();
        }

        public static int GenerarFcRecurrente(ACHEEntities dbContext, List<FacturasDetViewModel> detalle, Comercios comercio, int idComercio, DateTime dtDesde, DateTime dtHasta, string usuario)
        {
            int idFc = 0;
            Facturas fc = new Facturas();
            fc.FechaAlta = DateTime.Now;
            fc.IDComercio = idComercio;
            fc.PeriodoDesde = dtDesde;
            fc.PeriodoHasta = dtHasta;
            fc.Modo = "R";
            fc.Tipo = comercio.CondicionIva;
            fc.NroDocumento = comercio.NroDocumento;
            fc.UsuarioProceso = usuario;
            fc.Enviada = false;
            fc.Visible = true;
            fc.FacturasDetalle = new List<FacturasDetalle>();
            decimal totalIva = 0;
            decimal totalPrecioUnitario = 0;
            //Arancel en concepto de publicidad

            var auxListComercios = dbContext.Comercios.Include("Terminales").Where(x => x.NroDocumento == fc.NroDocumento).ToList();

            decimal costoFijo = auxListComercios.Sum(x => x.CostoFijo.HasValue ? x.CostoFijo.Value : 0);

            //Sumo el costopropio de todas las terminales del comercio en base al cuit
            decimal costoPOSPropio = 0;
            foreach (var com in auxListComercios)
            {
                foreach (var term in com.Terminales)
                {
                    costoPOSPropio += term.CostoPOSPropio;
                }
            }

            //var costoPOSPropioList = auxListComercios.Terminales.Where(x => x.IDComercio == idComercio).ToList();//.Sum(x => x.CostoPOSPropio);
            //if(costoPOSPropioList.Any())
            //    costoPOSPropio = auxListComercios.Sum(x => x.Terminales.CostoPOSPropio);

            if (detalle.Count() > 0)
            {
                decimal arancel = detalle.Where(x => x.Operacion == "Venta" || x.Operacion == "Anulacion").Select(x => x.Arancel).DefaultIfEmpty(0).Sum();
                if (costoFijo > 0)
                {
                    if (arancel > costoFijo)
                        arancel = arancel - costoFijo;
                    else
                        arancel = 0;
                }
                if (arancel > 0)
                {
                    FacturasDetalle detArancel = new FacturasDetalle();
                    detArancel.Cantidad = 1;
                    detArancel.Concepto = "ARANCEL - Arancel por uso de Red";
                    detArancel.Iva = 21;
                    detArancel.PrecioUnitario = arancel;
                    detArancel.IDPlanCuentaContabilium = 3075;
                    totalIva += arancel * 0.21M;
                    totalPrecioUnitario += arancel;
                    fc.FacturasDetalle.Add(detArancel);
                }

                //Arancel Puntos usuarios
                decimal puntos = detalle.Where(x => x.Operacion == "Venta" || x.Operacion == "Anulacion").Select(x => x.Puntos).DefaultIfEmpty(0).Sum();
                FacturasDetalle detPuntos = new FacturasDetalle();
                detPuntos.Cantidad = 1;
                detPuntos.Concepto = "PUNTOS - Fondos recibidos por puntos de usuarios";
                detPuntos.Iva = 21;//0
                detPuntos.PrecioUnitario = puntos;
                detPuntos.IDPlanCuentaContabilium = 3071;
                totalIva += puntos * 0.21M;
                totalPrecioUnitario += puntos;
                fc.FacturasDetalle.Add(detPuntos);

                //Costo Red In
                decimal costoRed = detalle.Where(x => x.Operacion == "Venta" || x.Operacion == "Anulacion").Select(x => x.CostoRed).DefaultIfEmpty(0).Sum();
                FacturasDetalle detCostoRed = new FacturasDetalle();
                detCostoRed.Cantidad = 1;
                detCostoRed.Concepto = "POS (LAPOS/POSNET) - Costo uso Red POS";
                detCostoRed.Iva = 21;
                detCostoRed.PrecioUnitario = costoRed;
                detCostoRed.IDPlanCuentaContabilium = 3070;
                totalIva += costoRed * 0.21M;
                totalPrecioUnitario += costoRed;
                fc.FacturasDetalle.Add(detCostoRed);

                //Arancel giftcard carga
                decimal carga = detalle.Where(x => x.Operacion == "Carga").Select(x => x.Arancel).DefaultIfEmpty(0).Sum();
                if (carga != 0)
                {
                    FacturasDetalle detCarga = new FacturasDetalle();
                    detCarga.Cantidad = 1;
                    detCarga.Concepto = "GIFTCARD - Arancel por uso de Tarjetas de Regalo";
                    detCarga.Iva = 21;
                    detCarga.PrecioUnitario = carga;
                    detCarga.IDPlanCuentaContabilium = 3072;
                    totalIva += carga * 0.21M;
                    totalPrecioUnitario += carga;
                    fc.FacturasDetalle.Add(detCarga);
                }

                //Arancel giftcard descarga
                decimal descarga = detalle.Where(x => x.Operacion == "Descarga").Select(x => x.Arancel).DefaultIfEmpty(0).Sum();
                if (descarga != 0)
                {
                    FacturasDetalle detDescarga = new FacturasDetalle();
                    detDescarga.Cantidad = 1;
                    detDescarga.Concepto = "GIFTCARD - Arancel por uso de Tarjetas de Regalo";
                    detDescarga.Iva = 21;
                    detDescarga.PrecioUnitario = descarga * -1;
                    detDescarga.IDPlanCuentaContabilium = 3073;
                    totalIva += descarga * -1 * 0.21M;
                    totalPrecioUnitario += descarga * -1;
                    fc.FacturasDetalle.Add(detDescarga);
                }

                //Costo Red In giftcard
                decimal costoRedGift = detalle.Where(x => x.Operacion == "Carga" || x.Operacion == "Descarga").Select(x => x.CostoRed).DefaultIfEmpty(0).Sum();
                if (costoRedGift != 0)
                {
                    FacturasDetalle detCostoRedGift = new FacturasDetalle();
                    detCostoRedGift.Cantidad = 1;
                    detCostoRedGift.Concepto = "GIFT CARDS (LAPOS/POSNET) - Costo uso Red POS";
                    detCostoRedGift.Iva = 21;
                    detCostoRedGift.PrecioUnitario = costoRedGift;
                    detCostoRedGift.IDPlanCuentaContabilium = 3070;
                    totalIva += costoRedGift * 0.21M;
                    totalPrecioUnitario += costoRedGift;
                    fc.FacturasDetalle.Add(detCostoRedGift);
                }
                //EnviosSMS
                var marca = dbContext.Marcas.Where(x => x.IDComercioFacturanteSMS == idComercio).FirstOrDefault();
                if (marca != null)
                {
                    var envios = dbContext.SMSEnvios.Where(x => x.IDMarca.HasValue && x.IDMarca.Value == marca.IDMarca && x.Enviado == true).ToList();
                    if (envios.Any())
                    {
                        FacturasDetalle detEnviosSMS = new FacturasDetalle();
                        detEnviosSMS.Cantidad = 1;
                        detEnviosSMS.Concepto = "Envios SMS x " + envios.Count().ToString();
                        detEnviosSMS.Iva = 21;
                        detEnviosSMS.IDPlanCuentaContabilium = 3074;
                        detEnviosSMS.PrecioUnitario = envios.Sum(x => x.Costo);
                        totalIva += detEnviosSMS.PrecioUnitario * 0.21M;

                        totalPrecioUnitario += detEnviosSMS.PrecioUnitario;
                        fc.FacturasDetalle.Add(detEnviosSMS);
                    }
                }
                //COSTO FIJO 
                //decimal abonoRedin = 0;
                //fc.ImporteTotal = totalPrecioUnitario + totalIva;
                //abonoRedin = costoFijo - fc.ImporteTotal;


                if (costoFijo > 0)
                {
                    FacturasDetalle detCostoAbonoRedin = new FacturasDetalle();
                    detCostoAbonoRedin.Cantidad = 1;
                    detCostoAbonoRedin.Concepto = "ABONO FIJO - Costo mínimo de abono Red-IN";
                    detCostoAbonoRedin.Iva = 21;
                    detCostoAbonoRedin.PrecioUnitario = costoFijo;
                    totalIva += costoFijo * 0.21M;
                    totalPrecioUnitario += costoFijo;// +totalIva;
                    detCostoAbonoRedin.IDPlanCuentaContabilium = 3070;
                    fc.FacturasDetalle.Add(detCostoAbonoRedin);
                }
                if (costoPOSPropio > 0)
                {
                    FacturasDetalle detCostoPOSPropio = new FacturasDetalle();
                    detCostoPOSPropio.Cantidad = 1;
                    detCostoPOSPropio.Concepto = "ABONO POS Propio - Costo de POS Propio";
                    detCostoPOSPropio.Iva = 21;
                    detCostoPOSPropio.PrecioUnitario = costoPOSPropio;
                    detCostoPOSPropio.IDPlanCuentaContabilium = 3070;
                    fc.FacturasDetalle.Add(detCostoPOSPropio);
                    totalIva += costoPOSPropio * 0.21M;
                    totalPrecioUnitario += costoPOSPropio;// +(costoPOSPropio * 0.21M);
                }

                fc.ImporteTotal = totalPrecioUnitario + totalIva;
                fc.TotalIva = totalIva;
            }
            else
            {
                if (costoPOSPropio > 0)
                {
                    FacturasDetalle detCostoPOSPropio = new FacturasDetalle();
                    detCostoPOSPropio.Cantidad = 1;
                    detCostoPOSPropio.Concepto = "ABONO POS Propio - Costo de POS Propio";
                    detCostoPOSPropio.Iva = 21;
                    detCostoPOSPropio.PrecioUnitario = costoPOSPropio;
                    detCostoPOSPropio.IDPlanCuentaContabilium = 3070;
                    fc.FacturasDetalle.Add(detCostoPOSPropio);
                    totalIva += costoPOSPropio * 0.21M;
                    totalPrecioUnitario += costoPOSPropio;// +(costoPOSPropio * 0.21M);
                    //fc.TotalIva = totalIva;
                }

                if (costoFijo > 0)
                {
                    FacturasDetalle detCostoAbonoRedin = new FacturasDetalle();
                    detCostoAbonoRedin.Cantidad = 1;
                    detCostoAbonoRedin.Concepto = "ABONO FIJO - Costo mínimo de abono Red-IN";
                    detCostoAbonoRedin.Iva = 21;
                    totalIva += costoFijo * 0.21M;
                    detCostoAbonoRedin.PrecioUnitario = costoFijo;
                    detCostoAbonoRedin.IDPlanCuentaContabilium = 3070;
                    fc.FacturasDetalle.Add(detCostoAbonoRedin);
                    totalPrecioUnitario += costoFijo;// +(costoFijo * 0.21M);
                    //fc.TotalIva = totalIva;
                }

                fc.ImporteTotal = totalPrecioUnitario + totalIva;
                fc.TotalIva = totalIva;
            }

            dbContext.Facturas.Add(fc);
            dbContext.SaveChanges();

            idFc = fc.IDFactura;

            return idFc;
        }

        private static void CrearOrdenDePago(string nroOrden,
            Comercios comercio, List<FacturasDetViewModel> detalle, string archivoDestino)
        {
            var template = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["TemplateOP"]);
            NFPDFWriter pdf = new NFPDFWriter(MedidasDocumento.A4, template);

            pdf.EscribirXY("ORDEN DE PAGO Nº: " + nroOrden, 331, 50, 10, Alineado.Izquierda);
            pdf.EscribirXY("Fecha: " + DateTime.Now.ToString("dd/MM/yyyy"), 331, 63, 10, Alineado.Izquierda);

            pdf.EscribirXY("Señor/es: " + comercio.RazonSocial, 27, 133, 10, Alineado.Izquierda);
            pdf.EscribirXY("Domicilio: " + comercio.DomiciliosFiscal.Domicilio, 27, 148, 10, Alineado.Izquierda);
            pdf.EscribirXY("Localidad: " + (comercio.DomiciliosFiscal.Ciudad.HasValue ? comercio.DomiciliosFiscal.Ciudades.Nombre : ""), 27, 163, 10, Alineado.Izquierda);
            pdf.EscribirXY("IVA: " + (comercio.CondicionIva == "RI" ? "Responsable Inscripto" : comercio.CondicionIva), 27, 178, 10, Alineado.Izquierda);
            pdf.EscribirXY("CUIT: " + comercio.NroDocumento, 330, 178, 10, Alineado.Izquierda);

            decimal impNeto = Math.Abs(detalle.Sum(x => x.NetoGrabado));

            List<FEItemDetalle> items = new List<FEItemDetalle>();

            items.Add(new FEItemDetalle { Cantidad = 1, Codigo = "Canje", Descripcion = "CANJE - Aplicacion de fondos por puntos de usuarios", Precio = Math.Abs(double.Parse(impNeto.ToString())) });
            pdf.EscribirXY(Math.Abs(impNeto).ToString("N2"), 570, 727, 10, Alineado.Derecha);
            pdf.EscribirBoxXY("Son Pesos " + FEFacturaElectronica.NumeroALetrasMoneda(double.Parse(impNeto.ToString())) + ".", 27, 630, 10, 400);

            int fontDetalleFE = 10;
            if (ConfigurationManager.AppSettings["FontDetalleFE"] != null) fontDetalleFE = int.Parse(ConfigurationManager.AppSettings["FontDetalleFE"]);

            pdf.InsertarTablaDetalle(items);

            var streamPDF = pdf.GenerarPDFStream();
            using (Stream destination = File.Create(archivoDestino))
            {
                for (int a = streamPDF.ReadByte(); a != -1; a = streamPDF.ReadByte())
                    destination.WriteByte((byte)a);
            }
        }

        public static bool validateTypeAgainstTx(string type, string operacion)
        {

            bool valid = false;

            switch (type)
            {
                case "Swap":
                    valid = operacion == "Canje";
                    break;
                case "Venta":
                    valid = operacion == "Venta";
                    break;
                case "Charge":
                    valid = operacion == "Carga";
                    break;
                case "Discharge":
                    valid = operacion == "Descarga";
                    break;
                case "Cancel":
                    valid = operacion == "Anulacion";
                    break;
            }

            return valid;
        }

        public static void CrearLiqProducto(string numero, DateTime fecha,
            Socios socio, decimal impNeto, decimal totalIva, string archivoDestino, string nombreArchivoRuta)
        {
            var template = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["TemplateLiqProducto"]); ;
            NFPDFWriter pdf = new NFPDFWriter(MedidasDocumento.A4, template);

            pdf.EscribirXY("B", 300, 50, 22, Alineado.Centro);
            pdf.EscribirXY("Cod. 61", 300, 60, 10, Alineado.Centro);

            pdf.EscribirXY("CUENTA DE VENTA Y LIQUIDO PRODUCTO Nº: " + numero, 331, 50, 10, Alineado.Izquierda);
            pdf.EscribirXY("Fecha: " + fecha.ToString("dd/MM/yyyy"), 331, 63, 10, Alineado.Izquierda);

            pdf.EscribirXY("Comitente: " + (socio != null ? socio.Apellido + "," + socio.Nombre : ""), 27, 133, 10, Alineado.Izquierda);
            pdf.EscribirXY("Domicilio: " + (socio != null ? socio.Domicilios.Domicilio : ""), 27, 148, 10, Alineado.Izquierda);
            pdf.EscribirXY("Localidad: " + (socio != null ? socio.Domicilios.Ciudades.Nombre : ""), 27, 163, 10, Alineado.Izquierda);
            pdf.EscribirXY("IVA: Consumidor Final", 27, 178, 10, Alineado.Izquierda);
            pdf.EscribirXY("DNI: " + (socio != null ? socio.NroDocumento : ""), 330, 178, 10, Alineado.Izquierda);

            decimal puntos = impNeto;
            decimal comision = impNeto - (impNeto / 1.21M);

            double impTotal = double.Parse(puntos.ToString());// + double.Parse(totalIva.ToString()));

            List<FEItemDetalle> items = new List<FEItemDetalle>();

            pdf.EscribirXY("Subtotal: " + Math.Abs(puntos - comision).ToString("N2"), 570, 650, 10, Alineado.Derecha);
            items.Add(new FEItemDetalle { Cantidad = 1, Codigo = "1", Descripcion = "PUNTOS - Fondos percibidos por cuenta y orden según anexo", Precio = double.Parse(puntos.ToString()) });
            items.Add(new FEItemDetalle { Cantidad = 1, Codigo = "2", Descripcion = "COMISIÓN - Arancel por uso de red", Precio = double.Parse((comision).ToString()) });

            pdf.EscribirXY("IVA 21%: " + comision.ToString("N2"), 570, 695, 10, Alineado.Derecha);

            pdf.EscribirXY(puntos.ToString("N2"), 570, 727, 10, Alineado.Derecha);
            pdf.EscribirBoxXY("Son Pesos " + FEFacturaElectronica.NumeroALetrasMoneda(impTotal) + ".", 27, 630, 10, 400);
            //pdf.EscribirBoxXY(comprobante.Observaciones, 27, 680, 10, 430);

            int fontDetalleFE = 10;
            if (ConfigurationManager.AppSettings["FontDetalleFE"] != null) fontDetalleFE = int.Parse(ConfigurationManager.AppSettings["FontDetalleFE"]);

            pdf.InsertarTablaDetalle(items);

            if (System.IO.File.Exists(nombreArchivoRuta))
            {
                try
                {
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(nombreArchivoRuta);
                    img.SetAbsolutePosition(10, 30);
                    pdf.document.Add(img);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var streamPDF = pdf.GenerarPDFStream();
            using (Stream destination = File.Create(archivoDestino))
            {
                for (int a = streamPDF.ReadByte(); a != -1; a = streamPDF.ReadByte())
                    destination.WriteByte((byte)a);
            }

        }

        public static void CombineMultiplePDFs(string[] fileNames, string outFile)
        {
            // step 1: creation of a document-object
            Document document = new Document();

            // step 2: we create a writer that listens to the document
            PdfCopy writer = new PdfCopy(document, new FileStream(outFile, FileMode.Create));
            if (writer == null)
            {
                return;
            }

            // step 3: we open the document
            document.Open();

            foreach (string fileName in fileNames)
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(fileName);
                reader.ConsolidateNamedDestinations();

                // step 4: we add content
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                    writer.AddPage(page);
                }

                PRAcroForm form = reader.AcroForm;
                if (form != null)
                {
                    writer.CopyAcroForm(reader);
                }

                reader.Close();
            }

            // step 5: we close the document and writer
            writer.Close();
            document.Close();
        }

    }

}
