﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Entities;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bContacto
    {
        cContacto cContacto;

        public Contactos getContacto(int IDContacto)
        {
            Contactos oContacto;
            try
            {
                cContacto = new cContacto();
                oContacto = cContacto.getContacto(IDContacto);

                return oContacto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Contactos add(Contactos oContacto)
        {
            try
            {
                cContacto = new cContacto();

                if (oContacto.IDContacto == 0) //insert
                    cContacto.insertContacto(oContacto);
                else //update
                    cContacto.updateContacto(oContacto);

                return oContacto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
