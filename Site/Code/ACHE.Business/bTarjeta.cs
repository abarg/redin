﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;
using System.Web;
using ACHE.Extensions;
using System.Configuration;

namespace ACHE.Business
{
    public class bTarjeta
    {
        cTarjeta cTarjeta;
        public static int[] pan = new int[16];
        public string _numero = "";
        public string _PanAux = "";
        public string _track1 = "";
        public string _track2 = "";

        public Tarjetas getTarjeta(int IDTarjeta)
        {
            Tarjetas oTarjeta;
            try
            {
                cTarjeta = new cTarjeta();
                oTarjeta = cTarjeta.getTarjeta(IDTarjeta);

                return oTarjeta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Tarjetas CrearTarjetaWS(int idSocio, int idMarca, ACHEEntities context)
        {

            Tarjetas oTarjeta;

            RangoAffinityNumeroPorMarca rango = GetRango(idMarca, context); 

            string rangoAffinityNumero = rango.BIN.ToString() + rango.Affinity.ToString();

            bTarjeta bTarjeta = new bTarjeta();

            // busco la ultima tarjeta creada dentro de un rango
            List<Tarjetas> tarjetasEnRango = context.Tarjetas.Where(t => t.Numero.Substring(0, 10) == rangoAffinityNumero).ToList();
            Tarjetas lastTarjeta = tarjetasEnRango.OrderByDescending(x => Convert.ToInt64(x.Numero.Substring(0, 15))).FirstOrDefault();

            string nuevoNumero = "";

            // si existen tarjeta/s dentro del rango la recuperamos y sumamos 1 al numero
            if (lastTarjeta != null)
            {
                nuevoNumero = (int.Parse(lastTarjeta.Numero.Substring(10, 5)) + 1).ToString().PadLeft(5, '0');
            }
            else
            {
                 nuevoNumero = "00001";
            }

            bTarjeta.CalcularPAN(rango.BIN.ToString() + rango.Affinity.ToString() + nuevoNumero);
            bTarjeta.Numero = nuevoNumero;
            bTarjeta.PanAux = bTarjeta.ObtenerPan();
            bTarjeta.Track1 = ("%B" + bTarjeta.PanAux + "^                    ^491210100000?");
            bTarjeta.Track2 = (";" + bTarjeta.PanAux + "=491210100000?");


            oTarjeta = new Tarjetas();

            DateTime fechaActual = DateTime.Now;
            oTarjeta.FechaAlta = fechaActual;
            oTarjeta.FechaEmision = fechaActual;
            oTarjeta.FechaVencimiento = Convert.ToDateTime("31/12/2049");
            oTarjeta.Estado = "A";
            oTarjeta.Numero = bTarjeta.PanAux;
            oTarjeta.IDMarca = idMarca;
            oTarjeta.TipoTarjeta = "B"; //datos.TipoTarjeta;

            oTarjeta.IDSocio = idSocio;
            oTarjeta.Credito = 0;
            oTarjeta.Giftcard = 0;
            oTarjeta.PuntosTotales = 0;

            // restamos uno a la capacidad del rango
            int capacidadActual = rango.Capacidad;

            rango.Capacidad = capacidadActual - 1;

            context.Tarjetas.Add(oTarjeta);

            context.SaveChanges();

            return oTarjeta;
        }

        public static RangoAffinityNumeroPorMarca GetRango(int idMarca, ACHEEntities context)
        {

            RangoAffinityNumeroPorMarca rango;

            rango = context.RangoAffinityNumeroPorMarca.Where(x => x.IDMarca == idMarca)
                                                        .OrderByDescending(x => x.Hasta)
                                                        .FirstOrDefault();
            
            if(rango == null || rango.Capacidad == 0)
            {
                rango = CreateRango(idMarca, context);
            }
  
            return rango;
        }

        private static RangoAffinityNumeroPorMarca CreateRango(int idMarca, ACHEEntities context)
        {
            RangoAffinityNumeroPorMarca rango;

            // recupero ultimo rango creado
            var lastRango = context.RangoAffinityNumeroPorMarca.OrderByDescending(x => x.IDRango).FirstOrDefault();

            rango = new RangoAffinityNumeroPorMarca();
            rango.BIN = 608894; // BIN CONSTANTE FIDELY (PROBABLEMENTE TEMPORAL)
            rango.IDMarca = idMarca;

            if (lastRango != null)
            {
                // validamos que el affinity tenga menos de 99.999 tarjetas
                if (lastRango.Hasta != 99999)
                {
                    rango.Affinity = lastRango.Affinity;
                    rango.Desde = lastRango.Hasta + 1;
                    rango.Hasta = lastRango.Hasta + 1000;
                    rango.Capacidad = 1000;
                    if (rango.Hasta == 100000) // ultimo rango affinity
                    {
                        rango.Hasta = 99999;
                        rango.Capacidad = 999;
                    }
                }
                else // creamos un nuevo affinity
                {
                    rango.Affinity = lastRango.Affinity + 1;
                    rango.Desde = 1;
                    rango.Hasta = 1000;
                    rango.Capacidad = 1000;
                }
            }
            else // Creamos rango Genesis
            {
                rango.Affinity = 1111;
                rango.Desde = 1;
                rango.Hasta = 1000;
                rango.Capacidad = 1000;
            }


            context.RangoAffinityNumeroPorMarca.Add(rango);
            context.SaveChanges();


            return rango;

        }


        public Tarjetas getTarjetaPorNumero(string numero, bool includeSocio)
        {
            Tarjetas oTarjeta;
            try
            {
                cTarjeta = new cTarjeta();
                oTarjeta = cTarjeta.getTarjetaPorNumero(numero, includeSocio);

                return oTarjeta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tarjetas getTarjetaPorNFC(string nfcCode)
        {
            Tarjetas oTarjeta;
            try
            {
                using (var context = new ACHEEntities())
                  return context.Tarjetas.Include("Marcas").Include("Socios").FirstOrDefault(t => t.NFC == nfcCode);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tarjetas getTarjetaPorDNI(string numero)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    // busco Socio

                    var socio = context.Socios.Where(x => x.NroDocumento == numero).FirstOrDefault();

                    if (socio != null)
                    {
                        return context.Tarjetas.Include("Marcas").Include("Socios").FirstOrDefault(t => t.Socios.NroDocumento == numero);
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*public List<Tarjetas> getTarjetasForVisa()
        {
            List<Tarjetas> listTarjetas;
            try
            {
                cTarjeta = new cTarjeta();
                listTarjetas = cTarjeta.getTarjetasForVisa();

                return listTarjetas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        /*public List<Tarjetas> getTarjetas(int IDSocio)
        {
            List<Tarjetas> listTarjetas;
            try
            {
                cTarjeta = new cTarjeta();
                listTarjetas = cTarjeta.getTarjetas(IDSocio);

                return listTarjetas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        public Tarjetas add(Tarjetas oTarjeta)
        {
            try
            {
                cTarjeta = new cTarjeta();

                if (oTarjeta.IDTarjeta == 0) //insert
                {
                    var oT = this.getTarjetaPorNumero(oTarjeta.Numero, false);
                    if (oT != null) throw new Exception("El Número de Tarjeta ya existe");

                    cTarjeta.insertTarjeta(oTarjeta);
                }
                else //update
                    cTarjeta.updateTarjeta(oTarjeta);

                return oTarjeta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void add(List<Tarjetas> ListTarjetas)
        {
            try
            {
                cTarjeta = new cTarjeta();
                DataTable dt =  this.ConvertToDataTable(ListTarjetas);
                //Se eliminan columnas que no son usadas en el bulkInsert
                dt.Columns.Remove("Marcas");
                //dt.Columns.Remove("MarcasReference");
                dt.Columns.Remove("Socios");
                //dt.Columns.Remove("SociosReference");
                dt.Columns.Remove("Franquicias");
                dt.Columns.Remove("AlertasTarjetas");
                dt.Columns.Remove("Canjes");
                dt.Columns.Remove("TransaccionesSorteos");
                //dt.Columns.Remove("FranquiciasReference");
                //dt.Columns.Remove("EntityState");
                //dt.Columns.Remove("EntityKey");
                cTarjeta.insertarTarjetasMasivamente(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool deleteTarjeta(int IDTarjeta)
        {
            try
            {
                cTarjeta = new cTarjeta();
                var oTarjeta = this.getTarjeta(IDTarjeta);
                bSocio bSocio = new bSocio();
                if (oTarjeta.IDSocio.HasValue)
                    throw new Exception("La Tarjeta que desea eliminar tiene un Socio vinculado");
                return cTarjeta.deleteTarjeta(IDTarjeta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        #region CalcularTajetas
        public string PanAux
        {
            set
            {
                _PanAux = value;
            }
            get
            {
                return _PanAux;
            }
        }

        public string Numero
        {
            set
            {
                _numero = value;
            }
            get
            {
                return _numero;
            }
        }

        public string Track1
        {
            set
            {
                _track1 = value;
            }
            get
            {
                return _track1;
            }
        }

        public string Track2
        {
            set
            {
                _track2 = value;
            }
            get
            {
                return _track2;
            }
        }

        int[] GetPAN(string n)
        {
            for (int i = 0; i < n.Length; i++)
            {
                pan[i] = Convert.ToInt16(n.Substring(i, 1));

            }
            return pan;
        }

        public void CalcularPAN(string n)
        {

            int[] aux = GetPAN(n);
            int verif = 0;
            for (int i = 0; i < 15; i++)
            {
                if (i % 2 == 0)
                {
                    aux[i] = aux[i] * 2;
                    if (aux[i] > 9)
                        aux[i] = aux[i] - 9;
                }
            }
            for (int i = 0; i < 15; i++)
            {
                verif = aux[i] + verif;
            }
            int x = 0;
            for (x = 10; x < verif; x = x + 10)
            { }
            verif = x - verif;
            pan = GetPAN(n);
            pan[15] = verif;
        }

        public string ObtenerPan()
        {
            StringBuilder builder = new StringBuilder();
            foreach (int value in pan)
            {
                builder.Append(value);
            }
            string rta = builder.ToString();
            return rta;
        }
        #endregion
    }
}
