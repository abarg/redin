﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Entities;
using ACHE.Model;
using ACHE.Model.EntityData;

namespace ACHE.Business
{
    public class bDomicilio
    {
        cDomicilio cDomicilio;

        public eDomicilio getDomicilio(int IDDomicilio)
        {
            eDomicilio oDomicilio;
            try
            {
                cDomicilio = new cDomicilio();
                oDomicilio = cDomicilio.getDomicilio(IDDomicilio);

                return oDomicilio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public eDomicilio add(eDomicilio oDomicilio)
        {
            try
            {
                cDomicilio = new cDomicilio();

                if (oDomicilio.IDDomicilio == 0) //insert
                    cDomicilio.insertDomicilio(oDomicilio);
                else //update
                    cDomicilio.updateDomicilio(oDomicilio);

                return oDomicilio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
