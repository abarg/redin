﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using WS = ACHE.Model.WS4;
using ACHE.Model.EntityData;
using System.Data;
using ACHE.Extensions;
using System.Web;
using System.Configuration;

namespace ACHE.Business
{
    public class bSocio
    {
        cSocio cSocio;

        /*public void limpiarSociosTmp()
        {
            try
            {
                cSocio = new cSocio();
                cSocio.limpiarSociosTmp();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarSociosTmp(DataTable dt)
        {
            try
            {
                cSocio = new cSocio();
                cSocio.insertarSociosTmp(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarSocios()
        {
            try
            {
                cSocio = new cSocio();
                cSocio.actualizarSocios();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        /*public List<Socios> getSocios()
        {
            List<Socios> listSocios;
            try
            {
                cSocio = new cSocio();
                listSocios = cSocio.getSocios();

                return listSocios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        /*public List<Socios> getSociosConTarjetas(bool loadDomicilio)
        {
            List<Socios> listSocios;
            try
            {
                cSocio = new cSocio();
                listSocios = cSocio.getSociosConTarjetas(loadDomicilio);

                return listSocios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        public Socios getSocio(int IDSocio)
        {
            Socios Socios;
            try
            {
                cSocio = new cSocio();
                Socios = cSocio.getSocio(IDSocio);

                return Socios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNumTarjeta(int IDSocio)
        {
            try
            {
                cSocio = new cSocio();
                return cSocio.existeSocioNumTarjeta(IDSocio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNroCuenta(string NroCuenta)
        {
            try
            {
                cSocio = new cSocio();
                return cSocio.existeSocioNroCuenta(NroCuenta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNroDoc(string NroDoc)
        {
            try
            {
                cSocio = new cSocio();
                return cSocio.existeSocioNroDoc(NroDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*
        public Socios add(Socios oSocio)
        {
            try
            {
                cSocio = new cSocio();

                if (oSocio.IDSocio == 0) //insert
                {
                    //if (this.existeSocioNumTarjeta(oSocio.NumTar))
                    //{
                    //    throw new Exception("Ya existe un Socio con el Número de Tarjeta ingresada");
                    //}
                    //if (this.existeSocioNroCuenta(oSocio.NroCuenta))
                    //{
                    //    throw new Exception("Ya existe un Socio con el Nro de Cuenta ingresada");
                    //}
                    if (oSocio.NroDocumento.Trim() != "00" && this.existeSocioNroDoc(oSocio.NroDocumento))
                    {
                        throw new Exception("Ya existe un Socio con el Nro de Documento ingresado");
                    }

                    cSocio.insertSocio(oSocio);
                }
                else //update
                {
                    cSocio.updateSocio(oSocio);
                }

                return oSocio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
*/

        public static Socios crearSocioWS(WS.NewMemberRequest datos, Terminales terminal, ACHEEntities dbContext)
        {

            Socios entity;

            entity = new Socios();
            entity.FechaAlta = DateTime.Now;

            Random rnd = new Random();

            entity.Pwd = rnd.Next(000000, 999999).ToString();

            entity.IDFranquiciaAlta = terminal.Comercios.Franquicias.IDFranquicia;

            entity.TipoMov = "A";
            entity.TipoReg = "D";
            entity.Fecha = DateTime.Now;
            entity.NroCuenta = 0.ToString();
            entity.Nombre = datos.nombre;
            entity.Apellido = datos.apellido;
            entity.Email = datos.email;
            entity.FechaNacimiento = Convert.ToDateTime(datos.fechaNacimiento);
            entity.Sexo = "I";
            entity.TipoDocumento = datos.tipoDoc;
            entity.NroDocumento = datos.nroDocumento;
            entity.Telefono = "no tiene";
            entity.Celular = datos.celular;
            entity.EmpresaCelular = datos.empresaCelular;

            entity.Observaciones = "no";
            entity.NumeroTarjetaSube = "0";
            entity.NumeroTarjetaMonedero = "0";
            entity.NumeroTarjetaTransporte = "0";
            entity.PatenteCoche = "0";
            entity.Twitter = "0";
            entity.Facebook = "0";
            entity.Activo = true;

            entity.Banco = "0";
            entity.TipoCuenta = "0";
            entity.NroCuentaPlusIN = "0";
            entity.CBU = "0";
            entity.EmailPlusIn = "0";


            if (datos.domicilio != null)
            {
                try
                {
                    // recuperamos id's y "validamos"
                    entity.Domicilios = new Domicilios();
                    entity.Domicilios.FechaAlta = DateTime.Now;
                    
                    entity.Domicilios.Pais = dbContext.Paises.FirstOrDefault(x => x.IDPais == datos.domicilio.countryID).Nombre;
                    entity.Domicilios.Provincia = datos.domicilio.provinceID;
                    entity.Domicilios.Ciudad = datos.domicilio.cityID;
                    entity.Domicilios.Domicilio = datos.domicilio.address;
                    
                    
                    entity.Domicilios.Estado = "A";
                    entity.Domicilios.TipoDomicilio = "C";
                                    }
                catch (Exception e)
                {
                   // datos de domicilio invalidos
                }
            }
                
             dbContext.Socios.Add(entity);

            try
            {
                dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {

                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName + " -- " + ve.ErrorMessage);

                    }
                }
                throw;
            }

            return entity;
        }

        public bool deleteSocio(int IDSocio)
        {
            try
            {
                cSocio = new cSocio();
                return cSocio.deleteSocio(IDSocio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
