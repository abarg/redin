﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bRubros
    {
        cRubros cRubros;
        
        public List<Rubros> getRubros()
        {
            List<Rubros> list;
            try
            {
                cRubros = new cRubros();
                list = cRubros.getRubros();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
