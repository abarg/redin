﻿using ACHE.Model;
using ACHE.Model.EntityData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Business
{
    public class bComercio
    {
        cComercio cComercio;

        public List<Terminales> getComerciosForVisa()
        {
            List<Terminales> listComercios;
            try
            {
                cComercio = new cComercio();
                listComercios = cComercio.getComerciosForVisa();

                return listComercios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool deleteComercio(int IDComercio)
        {
            try
            {
                cComercio = new cComercio();
                return cComercio.deleteComercio(IDComercio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Comercios getComercio(int IDComercio)
        {
            Comercios Comercios;
            try
            {
                cComercio = new cComercio();
                Comercios = cComercio.getComercio(IDComercio);

                return Comercios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Comercios getComercio(string terminal, string establecimiento)
        {
            Comercios Comercios;
            try
            {
                cComercio = new cComercio();
                Comercios = cComercio.getComercio(terminal, establecimiento);

                return Comercios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Comercios> getComercios() {
            List<Comercios> list;
            try {
                cComercio = new cComercio();
                list = cComercio.getComercios();

                return list;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public DataTable getReportComercios(string Tipo, DateTime FechaDesde, DateTime FechaHasta, string SDS, string Nombre, int CantOper)
        {
            try
            {
                cComercio = new cComercio();
                DataTable dt = cComercio.getReportComercios(Tipo, FechaDesde, FechaHasta, SDS, Nombre, CantOper);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int obtenerDescuento(Terminales terminal, ACHEEntities dbContext, Tarjetas oTarjeta)
        {
            int descuento = -1;
            descuento = DescuentoPromocion(terminal, dbContext, DateTime.Now.Date, oTarjeta != null ? oTarjeta.Numero : "");
            if (descuento == -1)
            {
                int diaActual = (int)DateTime.Now.DayOfWeek;
                if (esVip(terminal, dbContext, oTarjeta != null ? oTarjeta.Numero : ""))
                {
                    if (diaActual == 1)
                        descuento = terminal.DescuentoVip ?? terminal.Descuento;
                    if (diaActual == 2)
                        descuento = terminal.DescuentoVip2 ?? terminal.Descuento2;
                    if (diaActual == 3)
                        descuento = terminal.DescuentoVip3 ?? terminal.Descuento3;
                    if (diaActual == 4)
                        descuento = terminal.DescuentoVip4 ?? terminal.Descuento4;
                    if (diaActual == 5)
                        descuento = terminal.DescuentoVip5 ?? terminal.Descuento5;
                    if (diaActual == 6)
                        descuento = terminal.DescuentoVip6 ?? terminal.Descuento6;
                    if (diaActual == 7 || diaActual == 0)
                        descuento = terminal.DescuentoVip7 ?? terminal.Descuento7;
                }
                else
                {
                    if (diaActual == 1)
                        descuento = terminal.Descuento;
                    if (diaActual == 2)
                        descuento = terminal.Descuento2;
                    if (diaActual == 3)
                        descuento = terminal.Descuento3;
                    if (diaActual == 4)
                        descuento = terminal.Descuento4;
                    if (diaActual == 5)
                        descuento = terminal.Descuento5;
                    if (diaActual == 6)
                        descuento = terminal.Descuento6;
                    if (diaActual == 7 || diaActual == 0)
                        descuento = terminal.Descuento7;
                }
            }
            return descuento;
        }

        private static int DescuentoPromocion(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual, string numTarjetaCliente)
        {
            int descuento = -1;
            var prom = dbContext.PromocionesPorComercio.Include("PromocionesPuntuales").Where(x => x.IDComercio == terminal.IDComercio && x.PromocionesPuntuales.FechaDesde <= fechaActual && x.PromocionesPuntuales.FechaHasta >= fechaActual).FirstOrDefault();
            if (prom != null)
            {
                if (esVip(terminal, dbContext, numTarjetaCliente))
                    descuento = prom.PromocionesPuntuales.DescuentoVip ?? prom.PromocionesPuntuales.Descuento;
                else
                    descuento = prom.PromocionesPuntuales.Descuento;
            }
            return descuento;
        }

        private decimal PuntosPromocion(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual)
        {
            decimal puntos = -1;
            var prom = dbContext.PromocionesPorComercio.Include("PromocionesPuntuales").Where(x => x.IDComercio == terminal.IDComercio && x.PromocionesPuntuales.FechaDesde <= fechaActual && x.PromocionesPuntuales.FechaHasta >= fechaActual).FirstOrDefault();
            if (prom != null)
            {
                puntos = prom.PromocionesPuntuales.PuntosVip ?? 0;
                if (puntos == 0)
                    puntos = prom.PromocionesPuntuales.Puntos ?? 0;
            }
            return puntos;
        }

        public static decimal obtenerPuntos(Terminales terminal, Tarjetas oTarjeta, ACHEEntities dbContext)
        {
            decimal puntos = -1;
            puntos = PuntosPromocion(terminal, dbContext, DateTime.Now.Date, oTarjeta.Numero);
            if (puntos == -1)
            {
                int diaActual = (int)DateTime.Now.DayOfWeek;
                if (esVip(terminal, dbContext, oTarjeta.Numero))
                {
                    if (diaActual == 1)
                        puntos = terminal.PuntosVip1 ?? (terminal.POSPuntos ?? 0);
                    if (diaActual == 2)
                        puntos = terminal.PuntosVip2 ?? (terminal.Puntos2 ?? 0);
                    if (diaActual == 3)
                        puntos = terminal.PuntosVip3 ?? (terminal.Puntos3 ?? 0);
                    if (diaActual == 4)
                        puntos = terminal.PuntosVip4 ?? (terminal.Puntos4 ?? 0);
                    if (diaActual == 5)
                        puntos = terminal.PuntosVip5 ?? (terminal.Puntos5 ?? 0);
                    if (diaActual == 6)
                        puntos = terminal.PuntosVip6 ?? (terminal.Puntos6 ?? 0);
                    if (diaActual == 7 || diaActual == 0)
                        puntos = terminal.PuntosVip7 ?? (terminal.Puntos7 ?? 0);
                }
                else
                {
                    if (diaActual == 1)
                        puntos = terminal.POSPuntos ?? 0;
                    if (diaActual == 2)
                        puntos = terminal.Puntos2 ?? 0;
                    if (diaActual == 3)
                        puntos = terminal.Puntos3 ?? 0;
                    if (diaActual == 4)
                        puntos = terminal.Puntos4 ?? 0;
                    if (diaActual == 5)
                        puntos = terminal.Puntos5 ?? 0;
                    if (diaActual == 6)
                        puntos = terminal.Puntos6 ?? 0;
                    if (diaActual == 7 || diaActual == 0)
                        puntos = terminal.Puntos7 ?? 0;
                }
            }
            return puntos;
        }

        private static decimal PuntosPromocion(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual, string numTarjetaCliente)
        {
            decimal puntos = -1;
            var prom = dbContext.PromocionesPorComercio.Include("PromocionesPuntuales").Where(x => x.IDComercio == terminal.IDComercio && x.PromocionesPuntuales.FechaDesde <= fechaActual && x.PromocionesPuntuales.FechaHasta >= fechaActual).FirstOrDefault();
            if (prom != null)
            {
                if (esVip(terminal, dbContext, numTarjetaCliente))
                    puntos = prom.PromocionesPuntuales.PuntosVip ?? (prom.PromocionesPuntuales.Puntos ?? 1);
                else
                    puntos = prom.PromocionesPuntuales.Puntos ?? 1;
            }
            return puntos;
        }

        private static decimal ArancelPromocion(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual,string numTarjetaCliente)
        {
            decimal arancel = -1;

            var prom = dbContext.PromocionesPorComercio.Include("PromocionesPuntuales").Where(x => x.IDComercio == terminal.IDComercio && x.PromocionesPuntuales.FechaDesde <= fechaActual && x.PromocionesPuntuales.FechaHasta >= fechaActual).FirstOrDefault();
            if (prom != null)
            {
                if (esVip(terminal, dbContext, numTarjetaCliente))
                    arancel = prom.PromocionesPuntuales.ArancelVip ?? (prom.PromocionesPuntuales.Arancel ?? -1);
                else
                    arancel = prom.PromocionesPuntuales.Arancel ?? -1;
            }
            return arancel;
        }

       public static decimal obtenerArancel(Terminales terminal, ACHEEntities dbContext, Tarjetas oTarjeta)
        {
            decimal arancel = -1;
            arancel = ArancelPromocion(terminal, dbContext, DateTime.Now.Date,oTarjeta.Numero);
            if (arancel == -1)
            {
                int diaActual = (int)DateTime.Now.DayOfWeek;
                if (diaActual == 1)
                    arancel = terminal.POSArancel ?? 0;
                if (diaActual == 2)
                    arancel = terminal.Arancel2 ?? 0;
                if (diaActual == 3)
                    arancel = terminal.Arancel3 ?? 0;
                if (diaActual == 4)
                    arancel = terminal.Arancel4 ?? 0;
                if (diaActual == 5)
                    arancel = terminal.Arancel5 ?? 0;
                if (diaActual == 6)
                    arancel = terminal.Arancel6 ?? 0;
                if (diaActual == 7 || diaActual == 0)
                    arancel = terminal.Arancel7 ?? 0;
            }
            return arancel;
        }

        private static bool esVip(Terminales terminal, ACHEEntities dbContext, string numTarjetaCliente)
        {
           var  comercio=dbContext.Comercios.Where(x => x.IDComercio == terminal.IDComercio).FirstOrDefault();
            Marcas marca = dbContext.Marcas.Where(x => x.IDMarca == comercio.IDMarca).FirstOrDefault();
            if (numTarjetaCliente != string.Empty)
            {
                string affinityTarjeta = numTarjetaCliente.Substring(6, 4);
                if (marca != null && marca.Affinity == affinityTarjeta)
                    return true;
            }
            return false;
        }

        public static int obtenerMulPuntos(Terminales terminal, ACHEEntities dbContext, string numTarjetaCliente)
        {
            int puntos = -1;
            puntos = MulPuntosPromocion(terminal, dbContext, DateTime.Now.Date, numTarjetaCliente);
            if (puntos == -1)
            {
                if (puntos == -1)
                {
                    int diaActual = (int)DateTime.Now.DayOfWeek;
                    if (esVip(terminal, dbContext, numTarjetaCliente))
                    {
                        if (diaActual == 1)
                            puntos = terminal.MultiplicaPuntosVip1 ?? terminal.MultiplicaPuntos1;
                        if (diaActual == 2)
                            puntos = terminal.MultiplicaPuntosVip2 ?? terminal.MultiplicaPuntos2;
                        if (diaActual == 3)
                            puntos = terminal.MultiplicaPuntosVip3 ?? terminal.MultiplicaPuntos3;
                        if (diaActual == 4)
                            puntos = terminal.MultiplicaPuntosVip4 ?? terminal.MultiplicaPuntos4;
                        if (diaActual == 5)
                            puntos = terminal.MultiplicaPuntosVip5 ?? terminal.MultiplicaPuntos5;
                        if (diaActual == 6)
                            puntos = terminal.MultiplicaPuntosVip6 ?? terminal.MultiplicaPuntos6;
                        if (diaActual == 7 || diaActual == 0)
                            puntos = terminal.MultiplicaPuntosVip1 ?? terminal.MultiplicaPuntos7;
                    }
                    else
                    {
                        if (diaActual == 1)
                            puntos = terminal.MultiplicaPuntos1;
                        if (diaActual == 2)
                            puntos = terminal.MultiplicaPuntos2;
                        if (diaActual == 3)
                            puntos = terminal.MultiplicaPuntos3;
                        if (diaActual == 4)
                            puntos = terminal.MultiplicaPuntos4;
                        if (diaActual == 5)
                            puntos = terminal.MultiplicaPuntos5;
                        if (diaActual == 6)
                            puntos = terminal.MultiplicaPuntos6;
                        if (diaActual == 7 || diaActual == 0)
                            puntos = terminal.MultiplicaPuntos7;
                    }
                }
            }
            var benef = MulPuntosBeneficios(terminal, dbContext, DateTime.Now.Date, numTarjetaCliente);
            if (benef != -1)
                puntos += benef;
            return puntos;
        }

        private static int MulPuntosPromocion(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual, string numTarjetaCliente)
        {
            int mulPuntos = -1;
            var prom = dbContext.PromocionesPorComercio.Include("PromocionesPuntuales").Where(x => x.IDComercio == terminal.IDComercio && x.PromocionesPuntuales.FechaDesde <= fechaActual && x.PromocionesPuntuales.FechaHasta >= fechaActual).FirstOrDefault();
            if (prom != null)
            {
                if (esVip(terminal, dbContext, numTarjetaCliente))
                    mulPuntos = prom.PromocionesPuntuales.MultiplicaPuntosVip ?? prom.PromocionesPuntuales.MultiplicaPuntos;
                else
                    mulPuntos = prom.PromocionesPuntuales.MultiplicaPuntos;
            }
            return mulPuntos;
        }

        private static int MulPuntosBeneficios(Terminales terminal, ACHEEntities dbContext, DateTime fechaActual, string numTarjetaCliente)
        {
            int mulPuntos = -1;
            try
            {
                var comercio = dbContext.Comercios.Where(x => x.IDComercio == terminal.IDComercio).FirstOrDefault();
                var benef = dbContext.Beneficios.Where(x => (x.FechaDesde <= fechaActual && x.FechaHasta >= fechaActual) && (x.IDComercio == terminal.IDComercio || x.IDMarca == comercio.Marcas.IDMarca || comercio.EmpresasComercios.Any(y => y.IDEmpresa == x.IDEmpresa))).FirstOrDefault();
                if (benef != null)
                {
                    var tarjeta = dbContext.Tarjetas.Where(x => x.Numero == numTarjetaCliente).FirstOrDefault();
                    if (benef.Sexo == tarjeta.Socios.Sexo && benef.Sexo != "T")
                    {
                        if (benef.Edad != "Todas")
                            mulPuntos = BeneficioPorEdad(tarjeta, benef);
                        else
                            mulPuntos = benef.MultiplicaPuntos;
                    }
                    else
                        mulPuntos = BeneficioPorEdad(tarjeta, benef);
                }
            }
            catch (Exception ex) { }
            return mulPuntos;
        }

        private static int BeneficioPorEdad(Tarjetas tarjeta, Beneficios benef)
        {
            int mulPuntos = -1;
            if (benef.Edad.Contains("0-18"))
            {
                if (tarjeta.Socios.Edad <= 18)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("19-25"))
            {
                if (tarjeta.Socios.Edad >= 19 && tarjeta.Socios.Edad <= 25)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("26-30"))
            {
                if (tarjeta.Socios.Edad >= 26 && tarjeta.Socios.Edad <= 30)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("31-40"))
            {
                if (tarjeta.Socios.Edad >= 31 && tarjeta.Socios.Edad <= 40)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("41-50"))
            {
                if (tarjeta.Socios.Edad >= 41 && tarjeta.Socios.Edad <= 50)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("51-60"))
            {
                if (tarjeta.Socios.Edad >= 51 && tarjeta.Socios.Edad <= 60)
                    mulPuntos = benef.MultiplicaPuntos;
            }
            if (benef.Edad.Contains("61+"))
            {
                if (tarjeta.Socios.Edad >= 60)
                    mulPuntos = benef.MultiplicaPuntos;
            }
   
            return mulPuntos;
        }
        
        public List<ZonasViewModel> obtenerZonas(Terminales terminal)
        {
            try
            {
                cComercio = new cComercio();
                List<ZonasViewModel> zonas = cComercio.getZonas(terminal.Domicilios.Provincia);
                return zonas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
