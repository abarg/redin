﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.EntityData;
using System.Data;

namespace ACHE.Business
{
    public class bMarca
    {
        cMarca cMarca;
        
        public List<Marcas> getMarcas()
        {
            List<Marcas> list;
            try
            {
                cMarca = new cMarca();
                list = cMarca.getMarcas();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Marcas> getMarcasByFranquicia(int idFranquicia)
        {
            List<Marcas> list;
            try
            {
                cMarca = new cMarca();
                list = cMarca.getMarcasByFranquicia(idFranquicia);

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
