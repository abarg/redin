﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using ACHE.Business;

namespace ACHE.TRSeguro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            RealizarTransaccion();
        }

        public static void RealizarTransaccion()
        {

            string NumEst = ConfigurationManager.AppSettings["NumEst"];
            string Terminal = ConfigurationManager.AppSettings["NumTerminal"];
            int cantTr = 0;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                    string fecha = DateTime.Now.ToString(formato);
                    cantTr = dbContext.Database.SqlQuery<int>("exec CrearTransaccionesCostoSeguro '" + NumEst + "','" + Terminal + "','" + fecha + "'").FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/TRSeguro_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            /*
            var comercio = dbContext.Comercios.Where(x => x.POSTerminal == Terminal && x.NumEst == NumEst).FirstOrDefault();
            if (comercio != null)
            {
                List<TarjetasCostoSeguro> list = new List<TarjetasCostoSeguro>();
                string sql = "SELECT Numero, s.CostoSeguro as CostoSeguroSocio, m.CostoSeguro as CostoSeguroMarca from Tarjetas t inner join  Marcas m on t.IDMarca=m.IDMarca left join Socios s on t.IDSocio=s.IDSocio where t.FechaBaja is null and((s.CostoSeguro is not null and s.CostoSeguro>0) or (m.CostoSeguro is not null and m.CostoSeguro>0))order by t.Numero ";
                list = dbContext.Database.SqlQuery<TarjetasCostoSeguro>(sql, new object[] { }).ToList();
                if (list.Any())
                {
                    foreach (var tarjeta in list)
                    {
                        try
                        {
                            decimal importe = 0;
                            if (tarjeta.CostoSeguroSocio.HasValue && tarjeta.CostoSeguroSocio.Value > 0)
                                importe = tarjeta.CostoSeguroSocio.Value;
                            else if (!tarjeta.CostoSeguroSocio.HasValue && tarjeta.CostoSeguroMarca.HasValue && tarjeta.CostoSeguroMarca.Value > 0)
                                importe = tarjeta.CostoSeguroMarca.Value;

                            if (importe > 0)
                            {
                                Common.CrearTransaccion(dbContext, DateTime.Now, comercio.IDComercio, "Web", "", "COSTO SEGURO MENSUAL", importe, "", "", "", "", tarjeta.Numero, "", "Venta", "000000000000", "1100", "", "", "", "proceso");
                                cantTr++;
                            }
                        }
                        catch (Exception ex)
                        {
                            var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/TRSeguro_XX.log";
                            BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                        }
                    }
                }
            }
                */

            ListDictionary datos = new ListDictionary();
            datos.Add("<CANTTOTAL>", cantTr);
            string emailTo = ConfigurationManager.AppSettings["Email.To"];
            bool send = EmailHelper.SendMessage(EmailTemplate.NotificacionTr, datos, emailTo, "Proceso COSTO SEGURO finalizado.");
            if (!send)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador");
            }

        }
    }
}

