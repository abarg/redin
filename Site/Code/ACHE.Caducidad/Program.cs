﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using ACHE.Business;

namespace ACHE.Caducidad {
    class Program {
        public static void Main(string[] args) {
            ejecutarProcesoCaducidad();
        }
       public static void ejecutarProcesoCaducidad() {
           string NumEst = ConfigurationManager.AppSettings["NumEst"];
           string Terminal = ConfigurationManager.AppSettings["NumTerminal"];
    
           try
           {
               using (var dbContext = new ACHEEntities())
               {
                   string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
                   string fecha = DateTime.Now.ToString(formato);
                    dbContext.Database.SqlQuery<int>("exec ProcesarCaducidad '" + NumEst + "','" + Terminal + "','" + fecha + "'").FirstOrDefault();
                    dbContext.Database.SqlQuery<int>("exec ProcesarCaducidadSocios '" + NumEst + "','" + Terminal + "','" + fecha + "'").FirstOrDefault();
               }
           }
           catch (Exception ex)
           {
               var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/TRCaducidad_XX.log";
               BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
           }


        }    
    }
}
