﻿(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('PagosListadoController', function ($scope, $http) {
            $scope.Page = 1;
            $scope.PageSize = 25;
            $scope.disableRight = false;
            $scope.disableLeft = true;
            $scope.filter = function () {
                $http.post(BASE_PATH + "/Grilla/GetPagos" )
                .success(function (data) {
                    $scope.Items = data.result.Data.Items;
                    $scope.TotalItems = data.result.Data.TotalItems;
                    $scope.TotalPage = data.result.Data.TotalPage;
                    if ($scope.TotalItems > 0) {
                        $scope.divPagination = "show";
                        $scope.lnkPrevPage = "";
                        $scope.lnkNextPage = "";
                        if ($scope.TotalPage == 1) {
                            $scope.disableLeft = true;
                            $scope.disableRight = true;
                        }
                        else if ($scope.Page == $scope.TotalPage)
                            $scope.disableRight = true;
                        else if ($scope.Page == 1) {
                            $scope.disableLeft = true;
                            $scope.disableRight = false;
                        }
                        else if ($scope.Page > 1)
                            $scope.disableLeft = false;

                        var aux = ($scope.Page * $scope.PageSize);
                        if (aux > $scope.TotalItems)
                            aux = $scope.TotalItems;
                        $scope.msgResults = "Mostrando " + (($scope.Page * $scope.PageSize) - $scope.PageSize + 1) + " - " + aux + " de " + $scope.TotalItems;
                        $scope.noResults = "hide";
                    } else {
                        $scope.disableLeft = $scope.disableRight = true;
                        $scope.noResults = "ng-scope";
                        $scope.msgResults = "Mostrando 0 - 0 de 0";
                    }
                });
            };
            $scope.export = function () {
                $scope.imgLoading = true;
                $scope.divIconoDescargar = false;
                $http.post(BASE_PATH + "/Grilla/ExportarPagos", { nombre: $scope.Titulo })
               .success(function (data) {
                   $scope.imgLoading = false;
                   $scope.lnkDownload = true;
                   $scope.hrefExpo = data.result.Data;
               });
            };
            $scope.resetExport = function () {
                $scope.imgLoading = false;
                $scope.lnkDownload = false;
                $scope.descargar = true;
            };
            $scope.showNextPage = function () {
                $scope.Page++;
                $scope.filter();
            };
            $scope.showPreviousPage = function () {
                $scope.Page--;
                $scope.filter();
            };
            /*
            $scope.new = function () {
                window.location.href = BASE_PATH + "/#/Novedades/Creacion";
            };
            $scope.edit = function (id) {
                window.location.href = BASE_PATH + "/#/Novedades/Edicion/" + id;
            };
            */
            /*
            $scope.delete = function (id, nombre) {
                bootbox.confirm("\u00BFEst\u00E1 seguro que desea eliminar el contacto: " + nombre + "?", function (result) {
                    if (result) {
                        $http.post(BASE_PATH + "/Grilla/DeleteNovedad", { id: id })
                         .success(function (data) {
                             if (data.result.HasError)
                                 bootbox.alert(data.result.Message);
                             else
                                 $scope.filter();
                         });
                    }
                });
            };
            */
            $scope.seeAll = function () {
                $scope.Titulo = "";
                $scope.filter();
            };
            /*
            $scope.changeActivo = function (id) {
                $http.post(BASE_PATH + "/Grillas/ChangeActivoNovedad", { id: id })
                .success(function (data) {
                    if (data.result.HasError) {
                        bootbox.alert(data.result.Message);
                        $scope.filter();
                    }
                    else
                        $scope.filter();
                });
            };
            */
            $scope.filter();
            $scope.resetExport();
        })
})();