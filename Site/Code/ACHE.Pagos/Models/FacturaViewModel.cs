﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Pagos.Models
{
    public class FacturaViewModel
    {
        public string Estado { get; set; }
        public int ID { get; set; }
        public string Comercio { get; set; }
        public string NroDocumento { get; set; }
        public string CondicionIva { get; set; }
        public string Numero { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVto { get; set; }
        public decimal Importe { get; set; }
        public string EmailComercio { get; set; }
        public bool Pagada { get; set; }
        public List<FacturasDetalleViewModel> factDet { get; set; }
        public string RutaMercadoPago { get; set; }
    }
    public class FacturasDetalleViewModel
    {
        public int IDFacturaDetalle { get; set; }
        public string Concepto { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Iva { get; set; }
        public int Cantidad { get; set; }
        public decimal SubTotal { get; set; }
    }
}