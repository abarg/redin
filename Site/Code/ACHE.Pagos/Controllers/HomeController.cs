﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Extensions;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ClosedXML.Excel;
using System.Data;
using System.IO;
using ACHE.Pagos.Models;
using System.Collections.Specialized;

namespace ACHE.Pagos.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Grilla/

        public ActionResult Index(string id)
        {

            FacturaViewModel model = new FacturaViewModel();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    int idFactura = 0;
                    idFactura = int.Parse(Cryptography.Decrypt(id));

                    var fact = dbContext.Facturas.Where(x => x.IDFactura == idFactura).FirstOrDefault();
                    model.Pagada = true;
                    if (fact != null)
                    {
                        model.Comercio = fact.Comercios.NombreFantasia;
                        model.EmailComercio = fact.Comercios.EmailsEnvioFc;
                        model.Importe = fact.ImporteTotal;
                        model.Numero = fact.Numero;
                        model.factDet = dbContext.FacturasDetalle.Where(x => x.IDFactura == idFactura).Select(x => new FacturasDetalleViewModel()
                        {
                            IDFacturaDetalle = x.IDFacturaDetalle,
                            Concepto = x.Concepto,
                            PrecioUnitario = x.PrecioUnitario,
                            Iva = x.Iva,
                            Cantidad = x.Cantidad,
                            SubTotal = (x.Cantidad * x.PrecioUnitario) + ((x.Cantidad * x.PrecioUnitario * x.Iva) / 100)
                        }).ToList();
                        
                        if (!dbContext.Pagos.Any(x => x.NroFactura == fact.Numero))
                            model.Pagada = false;

                        Session["RedIN.Factura"] = model;
                        //mercado pago
                        //var mpRef = API.AddPreference(idFactura.ToString(), "Pago de factura", string.Empty, 1, fact.ImporteTotal, API.Mondeda.ARS, "https://www.redin.com.ar/pagos/img/redinlogo.jpg");
                        //model.RutaMercadoPago = mpRef;// new HtmlString("<a class='lightblue-L-Sq-ArAll' id='lnkComprar' href='" + mpRef + "' name='MP-Checkout' mp-mode='modal' onreturn='pagoCompleto'><span>PAGAR</span></a>");
                    }
                }
            }
            catch
            {
            }
            return View(model);
        }

        public ActionResult PagoFin(int estado)
        {
            FacturaViewModel model = new FacturaViewModel();
            switch (estado)
            {
                case 1:
                    model.Estado = "Aprobado";
                    break;
                case 2:
                    model.Estado = "Pendiente";
                    break;
                case 3:
                    model.Estado = "En proceso";
                    break;
                case 4:
                    model.Estado = "Rechazado";
                    break;
                case 5:
                    model.Estado = "Incompleto";
                    break;
            }
            mandarMail(model.Estado);
            return View(model);
        }

        private void mandarMail(string estado)
        {
            FacturaViewModel factura = (FacturaViewModel)System.Web.HttpContext.Current.Session["RedIN.Factura"];
            if (factura != null)
            {
                ListDictionary replacements = new ListDictionary();
                replacements.Add("<CLIENTE>", factura.Comercio);
                replacements.Add("<NROFACTURA>", factura.Numero);
                replacements.Add("<ESTADO>", estado);

                bool send = EmailHelper.SendMessage(EmailTemplate.PagoFactura, replacements, factura.EmailComercio, "RedIn - Pago de factura");
                if (!send)
                {
                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                    BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador");
                }
            }
            else
                throw new Exception("Error al recuperar la fc");
        }

    }
}