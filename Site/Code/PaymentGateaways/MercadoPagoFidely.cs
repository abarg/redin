﻿using System;
using MercadoPago.Resources;
using MercadoPago.DataStructures.Preference;

using ACHE.Extensions;
using ACHE.Model;
using ACHE.Business;
using WS = ACHE.Model.WS4;
using System.Configuration;
using MercadoPago.Common;
using System.Collections.Generic;

namespace PaymentGateaway
{
    public class MercadoPagoFidely
    {

        public static Payment CreatePayment(ACHEEntities dbContext, WS.NewPayment tmpPayment, PaymentGateawayTokens tokens)
        {
            try {

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var payment = new Payment
                {
                    UserAccessToken = tokens.PrivateKey,
                    TransactionAmount = tmpPayment.txAmount,
                    Token = tmpPayment.cardToken,
                    Description = tmpPayment.description,
                    ExternalReference = "myReference",
                    PaymentMethodId = tmpPayment.paymentMethodID,
                    Installments = tmpPayment.installments,
                    Payer = new MercadoPago.DataStructures.Payment.Payer
                    {
                        Email = tmpPayment.Payer.email ?? "sistemas@fidely.com",
                        FirstName = tmpPayment.Payer.firstName,
                        LastName = tmpPayment.Payer.lastName
                    },
                };

                payment.Save();

                return payment;

            }
            catch(Exception e)
            {
                BasicLog.AppendToFile(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le payment error in mp", e.ToString());
                return new Payment();
            }
        }

        public static Preference CreatePreference(ACHEEntities dbContext, WS.NewPreference tmpPreference, PaymentGateawayTokens tokens)
        {

            try
            {

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                MercadoPago.SDK.ClientId = tokens.PublicKey;
                MercadoPago.SDK.ClientSecret = tokens.PrivateKey;

                // Create a preference object
                var preference = new Preference
                {
                    Items =
                {
                    new Item
                    {
                        Id = "1234",
                        Title = "Small Silk Plate",
                        Quantity = 5,
                        CurrencyId = CurrencyId.ARS,
                    }
                },
                    Payer = new Payer
                    {
                        Email = "agustinbarg@gmail.com"
                    }
                };

                preference.Save();

                return preference;
            }
            catch(Exception e)
            {
                BasicLog.AppendToFile(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le MP error: " + tokens.PublicKey, e.ToString());
                return null;
            }


        }
    }
}