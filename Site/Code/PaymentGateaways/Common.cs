﻿using System;
using MercadoPago.Resources;
using MercadoPago.DataStructures.Preference;

using ACHE.Extensions;
using ACHE.Model;
using ACHE.Business;
using WS = ACHE.Model.WS4;
using System.Configuration;
using MercadoPago.Common;
using System.Collections.Generic;
using System.Web;

namespace PaymentGateaway
{
    public class Common
    {

        public static WS.CreatePaymentResponse createMercadoPagoPayment(ACHEEntities dbContext, WS.CreatePaymentRequest data, PaymentGateawayTokens tokens)
        {

            // we build payer
            WS.Payer payer = new WS.Payer();
            if (data.Payer != null)
            {
                payer = new WS.Payer
                {
                    cardHolderName = data.Payer.cardHolderName ?? "",
                    email = data.Payer.email ?? "",
                    firstName = data.Payer.firstName ?? "",
                    lastName = data.Payer.lastName ?? ""
                };
            }

            // we build description

            string description = "";
            string shopName = tokens.Comercios.NombreFantasia;

            if (data.detail != "")
            {
                description = data.detail + " - " + shopName + " - a " + payer.cardHolderName ;
            }
            else
            {
                description = shopName + " - a " + payer.cardHolderName;
            }

            WS.NewPayment tmpPayment = new WS.NewPayment
            {
                publicKey = tokens.PublicKey,
                txAmount = data.txAmount,
                cardToken = data.cardToken,
                paymentMethodID = data.paymentMethodID,
                installments = data.installments,
                description = description,
                Payer = payer
            };

            try {

                var payment = PaymentGateaway.MercadoPagoFidely.CreatePayment(dbContext, tmpPayment, tokens);

                WS.CreatePaymentResponse paymentStatus = new WS.CreatePaymentResponse();

                if (payment.Id != null)
                {
                    paymentStatus = new WS.CreatePaymentResponse
                    {
                        paymentID = payment.Id.ToString(),
                        paymentStatus = payment.Status.ToString(),
                        paymentStatusDetail = payment.StatusDetail.ToString()
                    };
                }
                else
                {
                    paymentStatus = new WS.CreatePaymentResponse
                    {
                        paymentID = "0",
                        paymentStatus = "error",
                        paymentStatusDetail = payment.Errors.ToString()
                    };
                }

                return paymentStatus;

            }
            catch (Exception e)
            {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "le payment error in common", e.ToString());
                return new WS.CreatePaymentResponse();
            }

        }

        public static string createPaypalPayment()
        {
            return "";
        }

        public static string createStripePayment()
        {
            return "";
        }
        
    }
}