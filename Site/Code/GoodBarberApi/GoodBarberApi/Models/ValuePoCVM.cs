﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBarberApi.Models
{
    public class ValuePoCVM
    {
        public ValuePoCVM()
        {
            this.items = new List<ValueItemPoCVM>();
        }

        public bool dedup { get; set; }
        public string generated_in { get; set; }
        public List<ValueItemPoCVM> items { get; set; }
        public string next_page { get; set; }
        public string stat { get; set; }
        public string title { get; set; }
        public string url { get; set; }
    }

    public class ValueItemPoCVM
    {
        public ValueItemPoCVM()
        {
            this.images = new List<ValueImagePoCVM>();
        }

        public string address { get; set; }
        public string author { get; set; }
        public bool commentsEnabled { get; set; }
        public string commentsPostUrl { get; set; }
        public string commentsUrl { get; set; }
        public string content { get; set; }
        //Revisar
        public string contentTemplate { get; set; }
        public string date { get; set; }
        public string email { get; set; }
        public long id { get; set; }
        public List<ValueImagePoCVM> images { get; set; }
        public string isFeatured { get; set; }
        public int isHeadline { get; set; }
        public string largeThumbnail { get; set; }
        public string latitude { get; set; }
        //Revisar
        public string leadin { get; set; }
        public string longitude { get; set; }
        public int nbcomments { get; set; }
        public string originalThumbnail { get; set; }
        public string phoneNumber { get; set; }
        public string pinIconColor { get; set; }
        public int pinIconHeight { get; set; }
        public string pinIconUrl { get; set; }
        public int pinIconWidth { get; set; }
        public string smallThumbnail { get; set; }
        public Dictionary<string, List<string>> subsections { get; set; }
        public string subtype { get; set; }
        public string summary { get; set; }
        public string thumbnail { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string website { get; set; }
    }

    public class ValueImagePoCVM
    {
        public string id { get; set; }
        public string url { get; set; }
    }
}