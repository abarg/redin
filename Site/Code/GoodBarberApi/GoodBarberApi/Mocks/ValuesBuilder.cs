﻿using GoodBarberApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBarberApi.Mocks
{
    public static class ValuesBuilder
    {
        public static ValuePoCVM GetValuePoCVM(long countItems, string key)
        {
            var retorno = new ValuePoCVM()
            {
                dedup = true,
                generated_in = "0.010000 s",
                items = new List<ValueItemPoCVM>(),
                next_page = "",
                stat = "ok",
                title = "Titulo",
                url = ""
            };

            //Agregamos los items
            for (int iPos = 0; iPos < countItems; iPos++)
            {
                var itemToAdd = new ValueItemPoCVM()
                {
                    address = "12 Rue General Fiorella Ajaccio",
                    author = "Author of the post",
                    commentsEnabled = false,
                    //commentsPostUrl = "http://dume112.goodbarber.com/apiv3/submitComment/10844986/8608047/?username=[name]&comment=[content]&email=[email]",
                    commentsPostUrl = "",
                    //commentsUrl = "http://dume112.goodbarber.com/apiv3/getCommentsByItem/10844986/8608047/1/24/",
                    commentsUrl = "",
                    content = GetContentHtml(),
                    contentTemplate = "",
                    date = "2016-11-18T09:59:00+01:00",
                    email = "",
                    id = iPos,
                    images = new List<ValueImagePoCVM>()
                    {
                        new ValueImagePoCVM()
                        {
                            id = "img-8608047-13570563",
                            url = "http://dume112.goodbarber.com/photo/art/default/8608047-13570563.jpg?v=1449133145"
                        }
                    },
                    isFeatured = "img-8608047-13570563",
                    isHeadline = 0,
                    largeThumbnail = "http://dume112.goodbarber.com/photo/art/large_x2_16_9/8608047-13570563.jpg?v=1449133146",
                    latitude = "41.9189404",
                    leadin = "",
                    longitude = "8.7350822",
                    nbcomments = 1,
                    originalThumbnail = "http://dume112.goodbarber.com/photo/art/grande/8608047-13570563.jpg?v=1449133144",
                    phoneNumber = "",
                    pinIconColor = "#e94f06",
                    pinIconHeight = 300,
                    pinIconUrl = "http://dume112.goodbarber.com/assets/img/gbpreview/mask/map-pin1.png",
                    pinIconWidth = 150,
                    smallThumbnail = "http://dume112.goodbarber.com/photo/art/imagette_16_9/8608047-13570563.jpg?v=1449133150",
                    subsections = GetSubsections(key),
                    //subtype = "mcms",
                    subtype = "custom",
                    summary = "Creative Ecosystem! - Ítem " + iPos,
                    thumbnail = "http://dume112.goodbarber.com/photo/art/large_16_9/8608047-13570563.jpg?v=1449133155",
                    title = "CampusPlex - Ítem " + iPos,
                    type = "maps",
                    //url = "http://dume112.goodbarber.com/#!section=10844986&item=8608047",
                    url = "",
                    website = "",
                };
                retorno.items.Add(itemToAdd);
            }

            return retorno;
        }

        private static Dictionary<string, List<string>> GetSubsections(string key)
        {
            var retorno = new Dictionary<string, List<string>>();
            retorno.Add("15512120", new List<string>() { key, "Categoría " + key, "Todas las Categorías" });
            return retorno;
        }

        private static string GetContentHtml()
        {
            var retorno = 
                "<div class=\"photo top\" style=\"text-align:center\"> " +
                    "<a href=\"http://dume112.goodbarber.com/photo/art/grande/8608047-13570563.jpg\" target=\"_blank\">" +
                        "<img id=\"img-8608047-13570563\" src=\"http://dume112.goodbarber.com/photo/art/default/8608047-13570563.jpg?v=1449133145\" alt=\"CampusPlex\" title=\"CampusPlex\" />" +
                    "</a>" +
                "</div>" +
                "<br class=\"clear\" />" +
                "<div class=\"texte\">" +
                    "<p>Creative Ecosystem!</p>" +
                    "</div>" +
                "<br class=\"clear\" />";
            return retorno;
        }
    }
}