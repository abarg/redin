﻿using GoodBarberApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBarberApi.Mocks
{
    public static class ValuesMock
    {
        private static readonly Dictionary<string, ValuePoCVM> Values;

        static ValuesMock()
        {
            Values = new Dictionary<string, ValuePoCVM>();
            for (int iPos = 1; iPos <= 200; iPos++)
            {
                var itemToAdd = ValuesBuilder.GetValuePoCVM(iPos, iPos.ToString());
                Values.Add(iPos.ToString(), itemToAdd);
            }
        }

        public static ValuePoCVM GetValuesByKey(string key)
        {
            var retorno = new ValuePoCVM();
            if (Values.ContainsKey(key))
                retorno = Values[key];
            return retorno;
        }
    }
}