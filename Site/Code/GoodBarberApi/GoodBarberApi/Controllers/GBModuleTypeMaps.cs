﻿using GoodBarberApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodBarberApi.Controllers {
   public class GBModuleTypeMaps {
        public bool dedup { get; set; }
        public string generated_in { get; set; }
        public List<ValueItemPoCVM> items { get; set; }
        public string next_page { get; set; }
        public string stat { get; set; }
        public string title { get; set; }
        public string url { get; set; }
    }
}
