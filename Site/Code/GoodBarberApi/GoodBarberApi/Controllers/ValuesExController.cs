﻿using GoodBarberApi.Log;
using GoodBarberApi.Mocks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace GoodBarberApi.Controllers
{
    public class ValuesExController : Controller
    {
        [HttpGet]
        public JsonResult GetAll(string filter = null)
        {
            var retorno = ValuesMock.GetValuesByKey("10");
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            //LogAccess.Log(logContent, filter);
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetByCategoria(string id)
        {
            var retorno = ValuesMock.GetValuesByKey(id);
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            //LogAccess.Log(logContent, id);
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetByRubro(string id) {
            //var retorno = ValuesMock.GetValuesByKey(id);
            var retorno = CommonController.getRubrosById(id);
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            //LogAccess.Log(logContent, id);
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAllRubros() {
            var retorno = CommonController.getAllRubros();
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            //LogAccess.Log(logContent, filter);
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDescuentosComerciosByRubro(string id) {
            var retorno = CommonController.GetDescuentosComerciosByRubro(id);
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            //LogAccess.Log(logContent, filter);
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //public JsonResult GetSingleByCategoria(string id)
        //{
        //    var retorno = ValuesMock.GetValuesByKey(id).FirstOrDefault();
        //    var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
        //    //LogAccess.Log(logContent, id);
        //    return Json(retorno, JsonRequestBehavior.AllowGet);
        //}
    }
}