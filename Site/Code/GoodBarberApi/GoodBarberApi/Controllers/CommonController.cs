﻿using GoodBarberApi.Mocks;
using GoodBarberApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Model.EntityData;


namespace GoodBarberApi.Controllers
{
    public class CommonController : Controller
    {
        //
        // GET: /Common/
        //[HttpGet]
        //[Route("api/common/getDestinos")]
        //public IEnumerable<ValueItemPoCVM> getRubros() {
        //    try {
              
        //        using (var dbContext = new ACHEEntities()) {
        //            return dbContext.Rubros.ToList()
        //                    .Select(x => new ValueItemPoCVM {
        //                        //title = x.
        //                    }).ToList();
        //        }
        //    }
        //    catch (Exception ex) {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
        //    }
        //}

        public static ValuePoCVM getRubrosById(string id) {
            int idAux = int.Parse(id);
            if (idAux > 0) {
                cRubros rubro = new cRubros();
                var result = rubro.getRubros();
                ValuePoCVM retorno = new ValuePoCVM();
                retorno.title = result.Where(x => x.IDRubro == idAux).FirstOrDefault().Nombre;
                return retorno;
            }
            else return null;
        }

        public static ValuePoCVM getAllRubros() {
                cRubros rubroC = new cRubros();
                var result = rubroC.getRubros();
               
                ValuePoCVM retorno = new ValuePoCVM();
                foreach(var x in result){
                    ValueItemPoCVM aux = new ValueItemPoCVM();                
                    aux.title = x.Nombre;
                    retorno.items.Add(aux);
                }

                if (retorno != null)
                return retorno;

             else return null;
        }

        public static ValuePoCVM GetDescuentosComerciosByRubro(string id) {

            int nroDia=(int)DateTime.Now.DayOfWeek;
            ValuePoCVM retorno = new ValuePoCVM();
            int idAux = int.Parse(id);
            if (idAux > 0) {
                 using (var context = new ACHEEntities()) {
               
                var result = context.Comercios.Include("Terminales").Include("Domicilios").Where(x => x.Rubro == idAux && x.IDSubRubro == null).ToList();                
                foreach (var x in result) {
                    ValueItemPoCVM aux = new ValueItemPoCVM();
                    aux.title = x.NombreFantasia;                    
                    aux.address = x.Domicilios.Pais+ "-" +x.Domicilios.Provincia+"-"+ x.Domicilios.Domicilio;
                    if(x.Terminales.FirstOrDefault() != null)
                        aux.content = "Descuento: % " + getDescuentoByDia(nroDia, x.Terminales.FirstOrDefault()) + " DescuentoVip: % " + getDescuentoVipByDia(nroDia, x.Terminales.FirstOrDefault());
                    retorno.items.Add(aux);
                    }
                }
            }

            if (retorno != null)
                return retorno;

            else return null;
        }

        public static string getDescuentoByDia(int nroDia,Terminales terminal) { 
            switch(nroDia){
                case 1: return terminal.Descuento.ToString();
                case 2: return terminal.Descuento2.ToString();
                case 3: return terminal.Descuento3.ToString();
                case 4: return terminal.Descuento4.ToString();
                case 5: return terminal.Descuento5.ToString();
                case 6: return terminal.Descuento6.ToString();
                case 7: return terminal.Descuento7.ToString();
                default:
                    return "0";        
            }
        }

        public static string getDescuentoVipByDia(int nroDia, Terminales terminal) {
            switch (nroDia) {
                case 1: return terminal.DescuentoVip.ToString();
                case 2: return terminal.DescuentoVip2.ToString();
                case 3: return terminal.DescuentoVip3.ToString();
                case 4: return terminal.DescuentoVip4.ToString();
                case 5: return terminal.DescuentoVip5.ToString();
                case 6: return terminal.DescuentoVip6.ToString();
                case 7: return terminal.DescuentoVip7.ToString();
                default:
                    return "0";
            }
        }



        //        GBModuleTypeMaps retorno = new GBModuleTypeMaps();
        //        retorno.title=result.FirstOrDefault().Nombre;
        //        //foreach(var x in result){
        //        //    ValueItemPoCVM aux = new ValueItemPoCVM();                
        //        //    aux.title = x.Nombre;
        //        //    retorno.items.Add(aux);
        //        //}

        //        //retorno.title = result.FirstOrDefault().Nombre;
        //        if (retorno != null)
        //        return retorno;

        //     else return null;
        //}
        
            
    }
	
}