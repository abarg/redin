﻿using GoodBarberApi.Log;
using GoodBarberApi.Mocks;
using GoodBarberApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;

namespace GoodBarberApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values/5
        //public String Get(string id)
        //{
        //    var values = ValuesMock.GetValuesByKey(id);
        //    var retorno = Newtonsoft.Json.JsonConvert.SerializeObject(values);
        //    return retorno;
        //}

        public ValuePoCVM Get(string id)
        {
            var retorno = ValuesMock.GetValuesByKey(id);
            var logContent = Newtonsoft.Json.JsonConvert.SerializeObject(retorno);
            LogAccess.Log(logContent, id);
            return retorno;
        }
    }
}
