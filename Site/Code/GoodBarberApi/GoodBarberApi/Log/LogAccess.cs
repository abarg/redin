﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace GoodBarberApi.Log
{
    public static  class LogAccess
    {
        public static void Log(string content, string filter = null)
        {
            var path = HostingEnvironment.MapPath("~/App_Data/");
            var key = Guid.NewGuid().ToString();
            string fileName = Path.Combine(path, key, ".txt");
            var sb = new StringBuilder();
            sb.AppendLine("filter: " + filter);
            sb.AppendLine("content: " + content);
            System.IO.File.WriteAllText(path + "accesonuevo-" + key + ".txt", sb.ToString());
        }
    }
}