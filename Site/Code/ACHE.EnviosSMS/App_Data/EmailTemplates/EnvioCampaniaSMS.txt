﻿<table style="font-family: Arial,Helvetica,sans-serif;" width="550" align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="3" style="padding:10px 0 15px 0" align="left">
				<img src="http://www.redin.com.ar/img/redinlogo.jpg" alt="RedIN" width="150px"  />
			</td>
		</tr>
		<tr><td colspan="3"  width="100%" style="border-top:solid 1px #e8e8e8;display:block"></td></tr>
		<tr>
			<td colspan="3" style="padding: 20px 20px 10px; font-size: 12px; color: rgb(51, 51, 51); line-height: 18px;">
				<span style="font-family:Arial;font-size:18px;color:#333333;padding-bottom:3px">
					<p>Estimado/a administrador:</p>
				</span>
				<p>
					<br />
					Se ha enviado una campaña SMS, aqui están los datos:<br>
					<br>
					Marca: <MARCA>
					<br>
					Nombre de la campaña: <NOMBRECAMPANIA>
					<br>
					Tipo de campaña: <TIPOCAMPANIA>
					<br>
					Cantidad de mensajes a enviar: <CANTTOTAL>
					<br>
					Cantidad de mensajes enviados: <CANTENVIADOS>
					<br>
					Cantidad de mensajes no enviados: <CANTNOENVIADOS>
					<br>
					Costo total: <COSTOTOTAL>
					<br>
					Mensaje: <MENSAJE>
					<br>
					Fecha y hora del envío: <FECHAENVIO>
					<br>
					<br>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3"  style="padding-top:10px;border-top:solid 1px #e8e8e8">
				<span style="font-family:Arial;font-size:12px;color:#999999">
					No responder este mail.
				</span>
			</td>
		</tr>
		<tr><td colspan="3"  height="15" style="font-size:1px">&nbsp;</td></tr>
	</tbody>
</table>