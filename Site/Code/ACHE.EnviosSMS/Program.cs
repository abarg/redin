﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Business;
using System.Collections.Specialized;
using System.Configuration;

namespace EnviosSMS
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var dbContext = new ACHEEntities())
            {
                string emailTo = ConfigurationManager.AppSettings["Email.To"];
                DateTime fechaHoy = DateTime.Now;

                #region campañas
                var campList = dbContext.SMS
                    .Include("Marcas")
                    .Where(x => x.Estado == "A"
                        && x.FechaEnvioSolicitada.Day == fechaHoy.Day
                        && x.FechaEnvioSolicitada.Month == fechaHoy.Month
                        && x.FechaEnvioSolicitada.Year == fechaHoy.Year)
                    .ToList();
                if (campList.Any())
                {
                    foreach (var camp in campList)
                    {
                        try
                        {
                            List<TelefonoSMS> telefonos = new List<TelefonoSMS>();
                            int cantAEnviar = 0;

                            #region obtengo teléfonos
                            if (camp.Tipo.ToUpper() == "I")
                            {
                                //Si es interna
                                //string sql = "select distinct count(*) from SociosView where Celular is not null and IDMarca = " + camp.IDMarca + " ";

                                string sql = "select count(*) from SociosView  join Domicilios d on d.IDDomicilio = SociosView.IDDomicilio join Ciudades c on d.Ciudad = c.IDCiudad join Provincias p on d.Provincia = p.IDProvincia where Celular is not null and IDMarca = " + camp.IDMarca + " ";

                                #region Transacciones
                                if (camp.ConTransacciones != null) {
                                    if (camp.FechaDesdeTransacciones != null) {
                                        string estadoTrans = "";

                                        if (camp.ConTransacciones == false)
                                            estadoTrans = " not in ";
                                        else
                                            estadoTrans = " in ";

                                        sql += " and SociosView.idsocio " + estadoTrans + " (select distinct(tar.IDSocio) from Transacciones tr,Tarjetas tar where tr.NumTarjetaCliente = tar.Numero and  tr.FechaTransaccion >= '" + camp.FechaDesdeTransacciones.ToString() + "' )";
                                    }
                                    else
                                        throw new Exception("Debe ingresar una fecha");

                                }

                                #endregion

                                #region Ubicacion
                                if (camp.IDProvincia != null) {
                                    sql += " and ";
                                    sql += camp.IDProvincia + " = p.IDProvincia ";

                                    if (camp.IDCiudad != null) {
                                        sql += " and ";
                                        sql += camp.IDCiudad + " = c.IDCiudad ";
                                    }
                                }
                                #endregion

                                #region edad
                                if (camp.Edad.Trim() != "" && camp.Edad != "Todas")
                                {
                                    sql += " and (";
                                    string[] edades = camp.Edad.Split(";");
                                    string[] min = new string[6];
                                    string[] max = new string[6];
                                    for (int i = 0; i < edades.Count(); i++)
                                    {
                                        if (!string.IsNullOrEmpty((edades[i])))
                                        {
                                            min[i] = edades[i].Split("-")[0];
                                            max[i] = edades[i].Split("-")[1];
                                        }
                                    }

                                    int hasta = min.Count();
                                    for (int j = 0; j < hasta; j++)
                                    {
                                        if (!string.IsNullOrEmpty(min[j]) && !string.IsNullOrEmpty(max[j]))
                                        {
                                            if (j == 0)
                                                sql += "(Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                                            else
                                                sql += " or (Edad >= " + min[j] + " and Edad <= " + max[j] + ")";
                                        }
                                    }
                                    sql += " )";
                                }
                                #endregion

                                #region sexo
                                if (camp.Sexo.Trim() != "" && camp.Sexo != "T")
                                {
                                    sql += " and Sexo = ";
                                    if (camp.Sexo == "F")
                                        sql += "'F'";
                                    else
                                        sql += "'M'";
                                }
                                #endregion

                                #region profesion
                                if (camp.IDProfesion != null) {
                                    sql += " and IDSocio in (select s.IDSocio from Socios s left join Profesiones prof on s.IDProfesion = prof.IDProfesion where prof.IDProfesion = " + camp.IDProfesion + " )";
                                }
                                #endregion

                                cantAEnviar = dbContext.Database.SqlQuery<Int32>(sql, new object[] { }).First();
                                //costoTotal = (cantAEnviar * camp.Marcas.CostoSMS).ToString("N2");
                                string sql2 = sql.Replace("count(*)", "Celular, EmpresaCelular");
                                telefonos = dbContext.Database.SqlQuery<TelefonoSMS>(sql2, new object[] { }).ToList();
                            }
                            else if (camp.Tipo.ToUpper() == "E")
                            {
                                //Si es externa
                                string[] array = camp.Telefonos.Split(';');
                                cantAEnviar = array.Length;
                                foreach (var item in array)
                                {
                                    if (item.Split("-").Count() == 2)
                                    {
                                        TelefonoSMS aux = new TelefonoSMS();
                                        aux.Celular = item.Split("-")[0];
                                        aux.EmpresaCelular = item.Split("-")[1];
                                        telefonos.Add(aux);
                                    }
                                }
                                //costoTotal = (cantAEnviar * camp.Marcas.CostoSMS).ToString("N2");
                            }
                            #endregion

                            #region envío
                            if (cantAEnviar > 0)
                            {
                                string costoTotal = string.Empty;
                                int cantEnviados, cantNoEnviados;
                                cantEnviados = cantNoEnviados = 0;
                                List<EnvioSMS> envios = PlusMobile.SendSMS(camp.Mensaje, telefonos);
                                foreach (var aux in envios)
                                {
                                    if (aux.Enviado)
                                        cantEnviados++;
                                    else
                                        cantNoEnviados++;

                                    SMSEnvios entity = new SMSEnvios();
                                    entity.IDSMS = camp.IDSMS;
                                    entity.PhoneNumber = aux.Celular;
                                    entity.FechaEnvio = aux.FechaEnvio;
                                    entity.Costo = camp.Marcas.CostoSMS;
                                    entity.IDMarca = camp.IDMarca;
                                    entity.Enviado = aux.Enviado;
                                    entity.Tipo = "B";
                                    dbContext.SMSEnvios.Add(entity);
                                }
                                camp.Estado = "E";
                                costoTotal = (cantEnviados * camp.Marcas.CostoSMS).ToString("N2");
                                dbContext.SaveChanges();

                                ListDictionary datos = new ListDictionary();
                                datos.Add("<MARCA>", camp.Marcas.Nombre);
                                datos.Add("<NOMBRECAMPANIA>", camp.Nombre);
                                datos.Add("<TIPOCAMPANIA>", camp.Tipo.ToUpper() == "I" ? "Interna" : "Externa");
                                datos.Add("<CANTENVIADOS>", cantEnviados);
                                datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                                datos.Add("<CANTTOTAL>", cantAEnviar);
                                datos.Add("<COSTOTOTAL>", (camp.Marcas.CostoSMS * cantEnviados).ToString("N2"));
                                datos.Add("<MENSAJE>", camp.Mensaje);
                                datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));

                                bool send = EmailHelper.SendMessage(EmailTemplate.EnvioCampaniaSMS, datos, emailTo, "Envio de campañas "+ camp.Nombre +" (SMS) finalizada");
                                if (!send)
                                {
                                    //var dir ="C:/ACHE/Clientes/RedIn/Site/Code/ACHE.EnviosSMS/log/"+ "Email_XX.log";
                                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                    BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por campaña: " + camp.IDSMS);
                                }
                            }
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ErrorCampaña_XX.log";
                            BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                        }
                    }
                }
                #endregion

                #region cumpleaños
                var sociosList = dbContext.Tarjetas
                    .Include("Socios").Include("Marcas")
                    .Where(x => !x.FechaBaja.HasValue && x.IDSocio.HasValue
                        && x.Marcas.EnvioMsjCumpleanios && !string.IsNullOrEmpty(x.Marcas.MsjCumpleanios)
                        && !string.IsNullOrEmpty(x.Socios.EmpresaCelular) && !string.IsNullOrEmpty(x.Socios.Celular)
                        && x.Socios.FechaNacimiento.HasValue
                        && x.Socios.FechaNacimiento.Value.Month == fechaHoy.Month && x.Socios.FechaNacimiento.Value.Day == fechaHoy.Day)
                        //&& x.Socios.Apellido.ToLower() == "naftali" && x.Socios.NroDocumento == "38995145")
                        .Select(x => new
                        {
                            IDSocio = x.IDSocio.Value,
                            Mensaje = x.Marcas.MsjCumpleanios,
                            EmpresaCelular = x.Socios.EmpresaCelular,
                            Numero = x.Socios.Celular,
                            Nombre = x.Socios.Nombre,
                            Costo = x.Marcas.CostoSMS,
                            Apellido = x.Socios.Apellido
                        }).Distinct().ToList();//probarlo

                if (sociosList.Any())
                {
                    int cantAEnviar = sociosList.Count();
                    int cantEnviados, cantNoEnviados;
                    cantEnviados = cantNoEnviados = 0;
                    foreach (var socio in sociosList)
                    {
                        try
                        {
                            TelefonoSMS aux = new TelefonoSMS();
                            aux.Celular = socio.Numero;
                            aux.EmpresaCelular = socio.EmpresaCelular;
                            string mensaje = socio.Mensaje.Replace("XNOMBREX", socio.Nombre).Replace("XAPELLIDOX", socio.Apellido);
                            bool enviado = PlusMobile.SendSMS(mensaje, aux);
                            if (enviado)
                                cantEnviados++;
                            else
                                cantNoEnviados++;

                            SMSEnvios entity = new SMSEnvios();
                            entity.PhoneNumber = aux.Celular;
                            entity.FechaEnvio = DateTime.Now;
                            entity.Costo = socio.Costo;
                            entity.Enviado = enviado;
                            entity.Tipo = "C";
                            dbContext.SMSEnvios.Add(entity);

                        }
                        catch (Exception ex)
                        {
                            var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ErrorCumpleaños_XX.log";
                            BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                        }
                    }
                    dbContext.SaveChanges();
                    ListDictionary datos = new ListDictionary();
                    datos.Add("<CANTENVIADOS>", cantEnviados);
                    datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                    datos.Add("<CANTTOTAL>", cantAEnviar);
                    datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));

                    bool send = EmailHelper.SendMessage(EmailTemplate.EnvioCumpleanios, datos, emailTo, "Envio de SMS por cumpleaños finalizado");
                    if (!send)
                    {
                        var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                        BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por SMS de cumpleaños");
                    }
                }
                #endregion
            }
        }
    }
}