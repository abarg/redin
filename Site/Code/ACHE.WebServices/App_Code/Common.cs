﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Business;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;

/// <summary>
/// Summary description for Common
/// </summary>
public static class Common
{
    public static int obtenerPuntosGrupoFamiliar(ACHEEntities dbContext, Socios socio)
    {
        int puntos = 0;
        if (socio.Responsable == true)
            puntos = (dbContext.Tarjetas.Where(x => x.Socios.IDSocioPadre == socio.IDSocio).Sum(x => x.PuntosTotales)) + socio.Tarjetas.Sum(x => x.PuntosTotales);
        else if (socio.IDSocioPadre.HasValue)
            puntos = dbContext.Tarjetas.Where(x => x.Socios.IDSocioPadre == socio.IDSocioPadre || x.Socios.IDSocio == socio.IDSocioPadre).Sum(x => x.PuntosTotales);

        return puntos;
    }

    public static bool ValidarSessionID(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    public static void RenovarFechaValida(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }


}