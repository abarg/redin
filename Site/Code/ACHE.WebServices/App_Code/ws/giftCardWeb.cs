﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using ACHE.Model;

/// <summary>
/// Summary description for giftcardweb
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class giftcardweb : System.Web.Services.WebService {

    public enum ErrorIntegracion
    {
        SessionIDInvalido = -1,
        ComercioNoEncontrado = -101,
        DatosObligatoriosNoInformadosCorrectamente = -102,
        MontoNoPermitido = -103,
        IDMarcaIncorrecto = -104,
        SocioNoEncontrado = -105
    }
    public giftcardweb()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [WebMethod]
    public GetInfoResponse PurchaseCard(GetInfoRequest datos)
    {
        GetInfoResponse info = new GetInfoResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (Common.ValidarSessionID(datos.SessionID, dbContext))
            {
                Common.RenovarFechaValida(datos.SessionID, dbContext);
                if (dbContext.Marcas.Where(x => x.IDMarca == datos.IDMarca).Any())
                {
                    var aux = dbContext.Tarjetas.Where(x => x.IDMarca == datos.IDMarca && x.TipoTarjeta == "G" && x.Giftcard == 0 && x.Credito == 0 && !x.FechaBaja.HasValue && !x.IDSocio.HasValue).FirstOrDefault();
                    if (aux != null)
                    {
                        //Asigno tarjeta al comprador
                        aux.FechaAsignacion = DateTime.Now;
                        var socio = dbContext.Socios.Where(x => x.NroDocumento == datos.DNIComprador && x.Nombre.Contains(datos.NombreComprador.ToLower()) && x.Apellido.Contains(datos.ApellidoComprador.ToLower())).FirstOrDefault();
                        if (socio != null)
                        {
                            aux.IDSocio = socio.IDSocio;
                            if (datos.Monto > 0)
                            {
                                string NumEst = ConfigurationManager.AppSettings["Giftcard.NumEst"];
                                string Terminal = ConfigurationManager.AppSettings["Giftcard.NumTerminal"];
                                var terminal = dbContext.Terminales.Where(x => x.POSTerminal == Terminal && x.NumEst == NumEst).FirstOrDefault();
                                if (terminal != null)
                                {
                                    ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, terminal.IDComercio, "POS", "", "", datos.Monto,
                                     "", "", "", "", aux.Numero, "", "Carga", "000000000000", "2200", "", "", "", "giftCard web");
                                    info.SessionID = datos.SessionID;
                                    info.AnswerCode = 0;
                                    info.NumeroTarjeta = aux.Numero;
                                }
                                else
                                    info.AnswerCode = (int)ErrorIntegracion.ComercioNoEncontrado;
                            }
                            else
                                info.AnswerCode = (int)ErrorIntegracion.MontoNoPermitido;
                        }
                        else
                            info.AnswerCode = (int)ErrorIntegracion.SocioNoEncontrado;
                    }
                    else
                        info.AnswerCode = (int)ErrorIntegracion.DatosObligatoriosNoInformadosCorrectamente;
                }
                else
                    info.AnswerCode = (int)ErrorIntegracion.IDMarcaIncorrecto;
            }
            else
            {
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

            }
        }
        return info;
    }

    [Serializable]
    public class GetInfoResponse
    {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public string NumeroTarjeta { get; set; }
    }

    [Serializable]
    public class GetInfoRequest
    {
        public string SessionID { get; set; }
        public int IDMarca { get; set; }
        public decimal Monto { get; set; }
        public string NombreComprador { get; set; }
        public string ApellidoComprador { get; set; }
        public string EmailComprador { get; set; }
        public string DNIComprador { get; set; }
        public string NombreBeneficiario { get; set; }
        public string ApellidoBeneficiario { get; set; }
        public string EmailBeneficiario { get; set; }
        public string EmpresaCelular { get; set; }
        public string NumeroCelular { get; set; }
    }
    
}
