﻿using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;

/// <summary>
/// Summary description for farmatronic
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class farmatronic : System.Web.Services.WebService
{

    [WebMethod]
    public string CrearTransaccion(string usuario, string pwd, string mensaje)
    {
        var msj = "";
        if (usuario == "farmatronic" && pwd == "Tr0n1c")
        {
            //mensaje = @"<?xml version='1.0' encoding='ISO-8859-1'?><MensajeADESFA><EncabezadoMensaje><TipoMsj>200</TipoMsj><CodAccion>290720</CodAccion><IdMsj>8665</IdMsj><InicioTrx><Fecha>20160321</Fecha><Hora>122229</Hora></InicioTrx><Terminal><Tipo>PC</Tipo><Numero>05</Numero></Terminal><Software><Nombre>FARMATRONIC</Nombre><Version>5.5</Version></Software><Validador><Nombre>5044</Nombre><Version/></Validador><VersionMsj>2.0</VersionMsj><Prestador><Cuit>20111111112</Cuit><Sucursal>132</Sucursal><RazonSocial>FT DESARROLLO   DANIEL</RazonSocial><Codigo>1</Codigo><Vendedor>12</Vendedor></Prestador><SetCaracteres>AX</SetCaracteres></EncabezadoMensaje><EncabezadoReceta><Prescriptor><Apellido/><Nombre/><TipoMatricula>N</TipoMatricula><Provincia>N</Provincia><NroMatricula>1</NroMatricula><TipoPrescriptor>M</TipoPrescriptor><Cuit/><Especialidad/></Prescriptor><Beneficiario><TipoDoc/><NroDoc>0000000000</NroDoc><Apellido/><Nombre/><Sexo>M</Sexo><FechaNacimiento/><Parentesco/><EdadUnidad/><Edad/></Beneficiario><Financiador><Codigo>1</Codigo><Cuit/><Sucursal/></Financiador><Credencial><Numero/><Track/><Version/><Vencimiento/><ModoIngreso>A</ModoIngreso><EsProvisorio/><Plan/></Credencial><CoberturaEspecial/><Preautorizacion><Codigo/><Fecha/></Preautorizacion><FechaReceta/><Dispensa><Fecha>20160321</Fecha><Hora>120720</Hora></Dispensa><Formulario><Fecha/><Tipo>C</Tipo><Numero/><Serie/></Formulario><TipoTratamiento>N</TipoTratamiento><Diagnostico/><Institucion><Codigo/><Cuit/><Sucursal/></Institucion><Retira><Apellido/><Nombre/><TipoDoc/><NroDoc/><NroTelefono/></Retira><Bonos><Bono><Numero>12691425</Numero><Uso>1</Uso></Bono></Bonos></EncabezadoReceta><DetalleReceta><Item><NroItem>1</NroItem><CodBarras>7793640001973</CodBarras><CodTroquel>2272664</CodTroquel><Alfabeta/><Kairos/><Codigo/><ImporteUnitario>35.79</ImporteUnitario><ImporteUnitarioNeto>35.79</ImporteUnitarioNeto><CantidadSolicitada>1</CantidadSolicitada><PorcentajeCobertura>0.00</PorcentajeCobertura><CodPreautorizacion/><ImporteCobertura/><ExcepcionPrescripcion/><Diagnostico/><DosisDiaria/><DiasTratamiento/><Generico/><CodConflicto/><CodIntervencion/><CodAccion/><BonosItem><BonoItem><Numero>12691425</Numero><ExisteCobertura>N</ExisteCobertura></BonoItem></BonosItem></Item></DetalleReceta></MensajeADESFA>";

            BasicLog.AppendToFile(ConfigurationManager.AppSettings["FarmatronicLogError"], "XML recibido", mensaje);

            try
            {
                var xml = new XmlDocument();
                xml.LoadXml(mensaje);

                if (!mensaje.Contains("NroReferencia"))
                {

                    string prestador = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Prestador")[0].InnerXml;
                    string software = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Software")[0].InnerXml;
                    string terminal = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Terminal")[0].InnerXml;
                    string encabezadoReceta = xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml;
                    encabezadoReceta += "<Prescriptor>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml + "</Prescriptor>";
                    encabezadoReceta += "<Beneficiario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Beneficiario")[0].InnerXml + "</Beneficiario>";
                    encabezadoReceta += "<Financiador>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Financiador")[0].InnerXml + "</Financiador>";
                    encabezadoReceta += "<Credencial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Credencial")[0].InnerXml + "</Credencial>";
                    encabezadoReceta += "<CoberturaEspecial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/CoberturaEspecial")[0].InnerXml + "</CoberturaEspecial>";
                    encabezadoReceta += "<Preautorizacion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Preautorizacion")[0].InnerXml + "</Preautorizacion>";
                    encabezadoReceta += "<FechaReceta>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/FechaReceta")[0].InnerXml + "</FechaReceta>";
                    encabezadoReceta += "<Dispensa><Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha><Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora></Dispensa>";
                    encabezadoReceta += "<Formulario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Formulario")[0].InnerXml + "</Formulario>";
                    encabezadoReceta += "<TipoTratamiento>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/TipoTratamiento")[0].InnerXml + "</TipoTratamiento>";
                    encabezadoReceta += "<Diagnostico>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Diagnostico")[0].InnerXml + "</Diagnostico>";
                    encabezadoReceta += "<Institucion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Institucion")[0].InnerXml + "</Institucion>";
                    encabezadoReceta += "<Retira>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Retira")[0].InnerXml + "</Retira>";

                    string detalleReceta = xml.SelectNodes("/MensajeADESFA/DetalleReceta")[0].InnerXml;

                    int cantBonos = 0;
                    string bonos = "<Bonos>";
                    foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Bonos/Bono"))
                    {
                        int puntos = 0;
                        int puntosAcum = 0;

                        var importe = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("ImporteUnitario").InnerText;
                        var cant = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("CantidadSolicitada").InnerText;

                        if (importe != "" && cant != "")
                        {
                            puntos = (int)((decimal.Parse(importe.Replace(".", ",")) * int.Parse(cant)) * 2);//cambiar logica
                            puntosAcum = puntos * 12;
                        }

                        bonos += "<Bono>";
                        bonos += "<Uso>" + node.SelectSingleNode("Uso").InnerText + "</Uso><Numero>" + node.SelectSingleNode("Numero").InnerText + "</Numero><PuntosCliente>" + puntos + "</PuntosCliente><PuntosAccCliente>" + puntosAcum + "</PuntosAccCliente><PuntosPrestador>0</PuntosPrestador><PuntosAccPrestador>0</PuntosAccPrestador>";
                        bonos += "</Bono>";
                        cantBonos++;
                    }
                    bonos += "</Bonos>";

                    msj += @"<?xml version='1.0' encoding='iso-8859-1'?>";
                    msj += @"<MensajeADESFA>";
                    msj += @"<EncabezadoMensaje>";
                    msj += @"<Rta>";
                    msj += @"<CodRtaGeneral>00</CodRtaGeneral>";
                    msj += @"<Descripcion>Aprobada</Descripcion>";
                    msj += @"<Mensaje>La transacción fue realizada con éxito.</Mensaje>";
                    msj += @"</Rta>";
                    msj += @"<NroReferencia>" + Guid.NewGuid().ToString().Substring(0, 20) + "</NroReferencia>";
                    msj += @"<IdMsj>10</IdMsj>";
                    msj += @"<TipoMsj>210</TipoMsj>";
                    msj += @"<CodAccion>290720</CodAccion>";
                    msj += @"<InicioTrx>";
                    msj += @"<Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha>";
                    msj += @"<Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora>";
                    msj += @"</InicioTrx>";
                    msj += @"<Terminal>" + terminal + "</Terminal>";
                    msj += @"<Software>" + software + "</Software>";
                    msj += @"<Validador>";
                    msj += @"<Nombre>RedIN</Nombre>";
                    msj += @"<Version/>";
                    msj += @"</Validador>";
                    msj += @"<VersionMsj>2.0</VersionMsj>";
                    msj += @"<Prestador>" + prestador + "</Prestador>";
                    msj += @"<SetCaracteres>AX</SetCaracteres>";
                    msj += @"</EncabezadoMensaje>";
                    msj += @"<EncabezadoReceta>" + encabezadoReceta + bonos + "</EncabezadoReceta>";
                    msj += @"<DetalleReceta>";


                    foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item"))
                    {
                        msj += "<Item>";// +node.InnerXml;
                        msj += "<NroItem>" + node.SelectSingleNode("NroItem").InnerText + "</NroItem>";
                        msj += "<CodBarras>" + node.SelectSingleNode("CodBarras").InnerText + "</CodBarras>";
                        msj += "<CodTroquel>" + node.SelectSingleNode("CodTroquel").InnerText + "</CodTroquel>";
                        msj += "<Alfabeta>" + node.SelectSingleNode("Alfabeta").InnerText + "</Alfabeta>";
                        msj += "<Kairos>" + node.SelectSingleNode("Kairos").InnerText + "</Kairos>";
                        msj += "<Codigo>" + node.SelectSingleNode("Codigo").InnerText + "</Codigo>";
                        msj += "<ImporteUnitario>" + node.SelectSingleNode("ImporteUnitario").InnerText + "</ImporteUnitario>";
                        msj += "<ImporteUnitarioNeto>" + node.SelectSingleNode("ImporteUnitarioNeto").InnerText + "</ImporteUnitarioNeto>";
                        msj += "<CantidadSolicitada>" + node.SelectSingleNode("CantidadSolicitada").InnerText + "</CantidadSolicitada>";
                        msj += "<PorcentajeCobertura>" + node.SelectSingleNode("PorcentajeCobertura").InnerText + "</PorcentajeCobertura>";
                        msj += "<CodPreautorizacion>" + node.SelectSingleNode("CodPreautorizacion").InnerText + "</CodPreautorizacion>";
                        msj += "<ImporteCobertura>" + node.SelectSingleNode("ImporteCobertura").InnerText + "</ImporteCobertura>";
                        msj += "<ExcepcionPrescripcion>" + node.SelectSingleNode("ExcepcionPrescripcion").InnerText + "</ExcepcionPrescripcion>";
                        msj += "<Diagnostico>" + node.SelectSingleNode("Diagnostico").InnerText + "</Diagnostico>";
                        msj += "<DosisDiaria>" + node.SelectSingleNode("DosisDiaria").InnerText + "</DosisDiaria>";
                        msj += "<DiasTratamiento>" + node.SelectSingleNode("DiasTratamiento").InnerText + "</DiasTratamiento>";
                        msj += "<Generico>" + node.SelectSingleNode("Generico").InnerText + "</Generico>";
                        msj += "<CodConflicto>" + node.SelectSingleNode("CodConflicto").InnerText + "</CodConflicto>";
                        msj += "<CodIntervencion>" + node.SelectSingleNode("CodIntervencion").InnerText + "</CodIntervencion>";
                        msj += "<CodAccion>" + node.SelectSingleNode("CodAccion").InnerText + "</CodAccion>";
                        msj += "<CodRta>0</CodRta><MensajeRta>Aprobada</MensajeRta><CodAutorizacion>" + Guid.NewGuid().ToString().Substring(0, 20) + "</CodAutorizacion>";

                        msj += "<BonoItem>";
                        msj += "<CostoBono>0</CostoBono>";
                        msj += "<Uso>1</Uso>";
                        msj += "<ImporteCobertura>0</ImporteCobertura>";
                        msj += "<ImporteGentileza>0</ImporteGentileza>";
                        msj += "<Numero>0</Numero>";
                        msj += "<CantidadAprobada>1</CantidadAprobada>";
                        //msj += "<Descripcion>" + node.SelectSingleNode("Descripcion").InnerText + "</Descripcion>";
                        msj += "</BonoItem>";
                        msj += "</Item>";
                    }
                    msj += @"</DetalleReceta>";

                    //+detalleReceta + detalleRecetaAdic + "</DetalleReceta>";
                    msj += @"</MensajeADESFA>";
                }
                else
                {
                    string nroRef = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/NroReferencia")[0].InnerXml;
                    string prestador = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Prestador")[0].InnerXml;
                    string software = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Software")[0].InnerXml;
                    string terminal = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Terminal")[0].InnerXml;
                    string encabezadoReceta = "";// xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml;
                    //encabezadoReceta += "<Prescriptor>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml + "</Prescriptor>";
                    encabezadoReceta += "<Beneficiario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Beneficiario")[0].InnerXml + "</Beneficiario>";
                    encabezadoReceta += "<Financiador>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Financiador")[0].InnerXml + "</Financiador>";
                    encabezadoReceta += "<Credencial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Credencial")[0].InnerXml + "</Credencial>";
                    //encabezadoReceta += "<CoberturaEspecial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/CoberturaEspecial")[0].InnerXml + "</CoberturaEspecial>";
                    //encabezadoReceta += "<Preautorizacion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Preautorizacion")[0].InnerXml + "</Preautorizacion>";
                    encabezadoReceta += "<FechaReceta>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/FechaReceta")[0].InnerXml + "</FechaReceta>";
                    encabezadoReceta += "<Dispensa><Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha><Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora></Dispensa>";
                    encabezadoReceta += "<MedioPago><MontoTrx>0.00</MontoTrx><CantidadCuotas>1</CantidadCuotas></MedioPago>";

                    //encabezadoReceta += "<Formulario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Formulario")[0].InnerXml + "</Formulario>";
                    //encabezadoReceta += "<TipoTratamiento>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/TipoTratamiento")[0].InnerXml + "</TipoTratamiento>";
                    //encabezadoReceta += "<Diagnostico>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Diagnostico")[0].InnerXml + "</Diagnostico>";
                    //encabezadoReceta += "<Institucion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Institucion")[0].InnerXml + "</Institucion>";
                    //encabezadoReceta += "<Retira>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Retira")[0].InnerXml + "</Retira>";

                    string detalleReceta = xml.SelectNodes("/MensajeADESFA/DetalleReceta")[0].InnerXml;

                    //int cantBonos = 0;
                    //string bonos = "<Bonos>";
                    //foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Bonos/Bono"))
                    //{
                    //    int puntos = 0;
                    //    int puntosAcum = 0;

                    //    var importe = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("ImporteUnitario").InnerText;
                    //    var cant = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("CantidadSolicitada").InnerText;

                    //    if (importe != "" && cant != "")
                    //    {
                    //        puntos = (int)((decimal.Parse(importe) * int.Parse(cant)) * 2);//cambiar logica
                    //        puntosAcum = puntosAcum * 12;
                    //    }

                    //    bonos += "<Bono>";
                    //    bonos += "<Uso>" + node.SelectSingleNode("Uso").InnerText + "</Uso><Numero>" + node.SelectSingleNode("Numero").InnerText + "</Numero><PuntosCliente>" + puntos + "</PuntosCliente><PuntosAccCliente>" + puntosAcum + "</PuntosAccCliente><PuntosPrestador>0</PuntosPrestador><PuntosAccPrestador>0</PuntosAccPrestador>";
                    //    bonos += "</Bono>";
                    //    cantBonos++;
                    //}
                    //bonos += "</Bonos>";

                    msj += @"<?xml version='1.0' encoding='iso-8859-1'?>";
                    msj += @"<MensajeADESFA>";
                    msj += @"<EncabezadoMensaje>";
                    msj += @"<Rta>";
                    msj += @"<CodRtaGeneral>0</CodRtaGeneral>";
                    msj += @"<Descripcion>Aprobada</Descripcion>";
                    msj += @"<Mensaje>La anulacion ha sido exitosa</Mensaje>";
                    msj += @"</Rta>";
                    msj += @"<NroReferenciaCancel>" + Guid.NewGuid().ToString().Substring(0, 20) + "</NroReferenciaCancel>";
                    msj += @"<IdMsj>403</IdMsj>";
                    msj += @"<TipoMsj>210</TipoMsj>";
                    msj += @"<CodAccion>290720</CodAccion>";
                    msj += @"<InicioTrx>";
                    msj += @"<Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha>";
                    msj += @"<Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora>";
                    msj += @"</InicioTrx>";
                    msj += @"<Terminal>" + terminal + "</Terminal>";
                    msj += @"<Software>" + software + "</Software>";
                    msj += @"<Validador>";
                    msj += @"<Nombre>RedIN</Nombre>";
                    msj += @"<Version/>";
                    msj += @"</Validador>";
                    msj += @"<VersionMsj>2.0</VersionMsj>";
                    msj += @"<Prestador>" + prestador + "</Prestador>";
                    //msj += @"<SetCaracteres>AX</SetCaracteres>";
                    msj += @"</EncabezadoMensaje>";
                    msj += @"<EncabezadoReceta>" + encabezadoReceta + "</EncabezadoReceta>";
                    msj += @"<DetalleReceta>";


                    foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item"))
                    {
                        msj += "<Item>";// +node.InnerXml;
                        msj += "<NroItem>" + node.SelectSingleNode("NroItem").InnerText + "</NroItem>";
                        msj += "<CodAutOri>" + node.SelectSingleNode("CodAutOri").InnerText + "</CodAutOri>";
                        msj += "<CodBarras>" + node.SelectSingleNode("CodBarras").InnerText + "</CodBarras>";
                        msj += "<CodTroquel>" + node.SelectSingleNode("CodTroquel").InnerText + "</CodTroquel>";
                        msj += "<Alfabeta>" + node.SelectSingleNode("Alfabeta").InnerText + "</Alfabeta>";
                        msj += "<Kairos>" + node.SelectSingleNode("Kairos").InnerText + "</Kairos>";
                        msj += "<Codigo>" + node.SelectSingleNode("Codigo").InnerText + "</Codigo>";
                        //msj += "<ImporteUnitario>" + node.SelectSingleNode("ImporteUnitario").InnerText + "</ImporteUnitario>";
                        //msj += "<ImporteUnitarioNeto>" + node.SelectSingleNode("ImporteUnitarioNeto").InnerText + "</ImporteUnitarioNeto>";
                        //msj += "<CantidadSolicitada>" + node.SelectSingleNode("CantidadSolicitada").InnerText + "</CantidadSolicitada>";
                        //msj += "<PorcentajeCobertura>" + node.SelectSingleNode("PorcentajeCobertura").InnerText + "</PorcentajeCobertura>";
                        //msj += "<CodPreautorizacion>" + node.SelectSingleNode("CodPreautorizacion").InnerText + "</CodPreautorizacion>";
                        //msj += "<ImporteCobertura>" + node.SelectSingleNode("ImporteCobertura").InnerText + "</ImporteCobertura>";
                        //msj += "<ExcepcionPrescripcion>" + node.SelectSingleNode("ExcepcionPrescripcion").InnerText + "</ExcepcionPrescripcion>";
                        //msj += "<Diagnostico>" + node.SelectSingleNode("Diagnostico").InnerText + "</Diagnostico>";
                        //msj += "<DosisDiaria>" + node.SelectSingleNode("DosisDiaria").InnerText + "</DosisDiaria>";
                        //msj += "<DiasTratamiento>" + node.SelectSingleNode("DiasTratamiento").InnerText + "</DiasTratamiento>";
                        //msj += "<Generico>" + node.SelectSingleNode("Generico").InnerText + "</Generico>";
                        //msj += "<CodConflicto>" + node.SelectSingleNode("CodConflicto").InnerText + "</CodConflicto>";
                        //msj += "<CodIntervencion>" + node.SelectSingleNode("CodIntervencion").InnerText + "</CodIntervencion>";
                        //msj += "<CodAccion>" + node.SelectSingleNode("CodAccion").InnerText + "</CodAccion>";
                        msj += "<CodRta>0</CodRta><MensajeRta>Anulacion Exitosa</MensajeRta><CodAutorizacion>" + Guid.NewGuid().ToString().Substring(0, 20) + "</CodAutorizacion>";

                        //msj += "<BonoItem>";
                        //msj += "<CostoBono>0</CostoBono>";
                        //msj += "<Uso>1</Uso>";
                        //msj += "<ImporteCobertura>0</ImporteCobertura>";
                        //msj += "<ImporteGentileza>0</ImporteGentileza>";
                        //msj += "<Numero>0</Numero>";
                        //msj += "<CantidadAprobada>1</CantidadAprobada>";
                        ////msj += "<Descripcion>" + node.SelectSingleNode("Descripcion").InnerText + "</Descripcion>";
                        //msj += "</BonoItem>";
                        msj += "</Item>";
                    }
                    msj += @"</DetalleReceta>";

                    //+detalleReceta + detalleRecetaAdic + "</DetalleReceta>";
                    msj += @"</MensajeADESFA>";
                }
            }
            catch (Exception ex)
            {
                msj = "<?xml version='1.0' encoding='ISO-8859-1'?><Mensaje>Error inesperado al obtener el mensaje. Detalle: " + ex.Message + "</Mensaje>";
            }

        }
        else
            msj = "<?xml version='1.0' encoding='ISO-8859-1'?><Mensaje>Usuario y/o contraseña incorrecta</Mensaje>";

        return msj;
    }

    [WebMethod]
    public string AnularTransaccion(string usuario, string pwd, string mensaje)
    {
        var msj = "";
        if (usuario == "farmatronic" && pwd == "Tr0n1c")
        {
            //mensaje = @"<?xml version='1.0' encoding='ISO-8859-1'?><MensajeADESFA><EncabezadoMensaje> <NroReferencia>1574</NroReferencia> <TipoMsj>200</TipoMsj> <CodAccion>20710</CodAccion> <IdMsj>8666</IdMsj> <InicioTrx> <Fecha>20160321</Fecha> <Hora>122443</Hora> </InicioTrx> <Terminal> <Tipo>PC</Tipo> <Numero>05</Numero> </Terminal> <Software> <Nombre>FARMATRONIC</Nombre> <Version>5.5</Version> </Software> <Validador> <Nombre>5044</Nombre> <Version/> </Validador> <VersionMsj>2.0</VersionMsj> <Prestador> <Cuit>20111111112</Cuit> <Sucursal>132</Sucursal> <RazonSocial>FT DESARROLLO DANIEL</RazonSocial> <Codigo>1</Codigo> <Vendedor>012</Vendedor> </Prestador> </EncabezadoMensaje> <EncabezadoReceta> <Beneficiario>...</Beneficiario> <Financiador>...</Financiador> <Credencial>...</Credencial> <FechaReceta>20160321</FechaReceta> <Dispensa>...</Dispensa> </EncabezadoReceta> <DetalleReceta> <Item> <NroItem>1</NroItem> <CodAutOri>1574</CodAutOri> <CodBarras>7793640001973</CodBarras> <CodTroquel>2272664</CodTroquel> <Alfabeta/> <Kairos/> <Codigo/> </Item> </DetalleReceta> </MensajeADESFA>";

            BasicLog.AppendToFile(ConfigurationManager.AppSettings["FarmatronicLogError"], "XML Anulacion recibido", mensaje);

            try
            {
                var xml = new XmlDocument();
                xml.LoadXml(mensaje);

                string nroRef = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/NroReferencia")[0].InnerXml;
                string prestador = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Prestador")[0].InnerXml;
                string software = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Software")[0].InnerXml;
                string terminal = xml.SelectNodes("/MensajeADESFA/EncabezadoMensaje/Terminal")[0].InnerXml;
                string encabezadoReceta = "";// xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml;
                //encabezadoReceta += "<Prescriptor>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Prescriptor")[0].InnerXml + "</Prescriptor>";
                encabezadoReceta += "<Beneficiario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Beneficiario")[0].InnerXml + "</Beneficiario>";
                encabezadoReceta += "<Financiador>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Financiador")[0].InnerXml + "</Financiador>";
                encabezadoReceta += "<Credencial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Credencial")[0].InnerXml + "</Credencial>";
                //encabezadoReceta += "<CoberturaEspecial>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/CoberturaEspecial")[0].InnerXml + "</CoberturaEspecial>";
                //encabezadoReceta += "<Preautorizacion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Preautorizacion")[0].InnerXml + "</Preautorizacion>";
                encabezadoReceta += "<FechaReceta>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/FechaReceta")[0].InnerXml + "</FechaReceta>";
                encabezadoReceta += "<Dispensa><Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha><Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora></Dispensa>";
                encabezadoReceta += "<MedioPago><MontoTrx>0.00</MontoTrx><CantidadCuotas>1</CantidadCuotas></MedioPago>";

                //encabezadoReceta += "<Formulario>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Formulario")[0].InnerXml + "</Formulario>";
                //encabezadoReceta += "<TipoTratamiento>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/TipoTratamiento")[0].InnerXml + "</TipoTratamiento>";
                //encabezadoReceta += "<Diagnostico>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Diagnostico")[0].InnerXml + "</Diagnostico>";
                //encabezadoReceta += "<Institucion>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Institucion")[0].InnerXml + "</Institucion>";
                //encabezadoReceta += "<Retira>" + xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Retira")[0].InnerXml + "</Retira>";

                string detalleReceta = xml.SelectNodes("/MensajeADESFA/DetalleReceta")[0].InnerXml;

                //int cantBonos = 0;
                //string bonos = "<Bonos>";
                //foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/EncabezadoReceta/Bonos/Bono"))
                //{
                //    int puntos = 0;
                //    int puntosAcum = 0;

                //    var importe = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("ImporteUnitario").InnerText;
                //    var cant = xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item")[cantBonos].SelectSingleNode("CantidadSolicitada").InnerText;

                //    if (importe != "" && cant != "")
                //    {
                //        puntos = (int)((decimal.Parse(importe) * int.Parse(cant)) * 2);//cambiar logica
                //        puntosAcum = puntosAcum * 12;
                //    }

                //    bonos += "<Bono>";
                //    bonos += "<Uso>" + node.SelectSingleNode("Uso").InnerText + "</Uso><Numero>" + node.SelectSingleNode("Numero").InnerText + "</Numero><PuntosCliente>" + puntos + "</PuntosCliente><PuntosAccCliente>" + puntosAcum + "</PuntosAccCliente><PuntosPrestador>0</PuntosPrestador><PuntosAccPrestador>0</PuntosAccPrestador>";
                //    bonos += "</Bono>";
                //    cantBonos++;
                //}
                //bonos += "</Bonos>";

                msj += @"<?xml version='1.0' encoding='iso-8859-1'?>";
                msj += @"<MensajeADESFA>";
                msj += @"<EncabezadoMensaje>";
                msj += @"<Rta>";
                msj += @"<CodRtaGeneral>0</CodRtaGeneral>";
                msj += @"<Descripcion>Aprobada</Descripcion>";
                msj += @"<Mensaje>La anulacion ha sido exitosa</Mensaje>";
                msj += @"</Rta>";
                msj += @"<NroReferenciaCancel>" + Guid.NewGuid().ToString().Substring(0, 20) + "</NroReferenciaCancel>";
                msj += @"<IdMsj>403</IdMsj>";
                msj += @"<TipoMsj>210</TipoMsj>";
                msj += @"<CodAccion>290720</CodAccion>";
                msj += @"<InicioTrx>";
                msj += @"<Fecha>" + DateTime.Now.ToString("yyyyMMdd") + "</Fecha>";
                msj += @"<Hora>" + DateTime.Now.ToString("hhMMss") + "</Hora>";
                msj += @"</InicioTrx>";
                msj += @"<Terminal>" + terminal + "</Terminal>";
                msj += @"<Software>" + software + "</Software>";
                msj += @"<Validador>";
                msj += @"<Nombre>RedIN</Nombre>";
                msj += @"<Version/>";
                msj += @"</Validador>";
                msj += @"<VersionMsj>2.0</VersionMsj>";
                msj += @"<Prestador>" + prestador + "</Prestador>";
                //msj += @"<SetCaracteres>AX</SetCaracteres>";
                msj += @"</EncabezadoMensaje>";
                msj += @"<EncabezadoReceta>" + encabezadoReceta + "</EncabezadoReceta>";
                msj += @"<DetalleReceta>";


                foreach (XmlNode node in xml.SelectNodes("/MensajeADESFA/DetalleReceta/Item"))
                {
                    msj += "<Item>";// +node.InnerXml;
                    msj += "<NroItem>" + node.SelectSingleNode("NroItem").InnerText + "</NroItem>";
                    msj += "<CodAutOri>" + node.SelectSingleNode("CodAutOri").InnerText + "</CodAutOri>";
                    msj += "<CodBarras>" + node.SelectSingleNode("CodBarras").InnerText + "</CodBarras>";
                    msj += "<CodTroquel>" + node.SelectSingleNode("CodTroquel").InnerText + "</CodTroquel>";
                    msj += "<Alfabeta>" + node.SelectSingleNode("Alfabeta").InnerText + "</Alfabeta>";
                    msj += "<Kairos>" + node.SelectSingleNode("Kairos").InnerText + "</Kairos>";
                    msj += "<Codigo>" + node.SelectSingleNode("Codigo").InnerText + "</Codigo>";
                    //msj += "<ImporteUnitario>" + node.SelectSingleNode("ImporteUnitario").InnerText + "</ImporteUnitario>";
                    //msj += "<ImporteUnitarioNeto>" + node.SelectSingleNode("ImporteUnitarioNeto").InnerText + "</ImporteUnitarioNeto>";
                    //msj += "<CantidadSolicitada>" + node.SelectSingleNode("CantidadSolicitada").InnerText + "</CantidadSolicitada>";
                    //msj += "<PorcentajeCobertura>" + node.SelectSingleNode("PorcentajeCobertura").InnerText + "</PorcentajeCobertura>";
                    //msj += "<CodPreautorizacion>" + node.SelectSingleNode("CodPreautorizacion").InnerText + "</CodPreautorizacion>";
                    //msj += "<ImporteCobertura>" + node.SelectSingleNode("ImporteCobertura").InnerText + "</ImporteCobertura>";
                    //msj += "<ExcepcionPrescripcion>" + node.SelectSingleNode("ExcepcionPrescripcion").InnerText + "</ExcepcionPrescripcion>";
                    //msj += "<Diagnostico>" + node.SelectSingleNode("Diagnostico").InnerText + "</Diagnostico>";
                    //msj += "<DosisDiaria>" + node.SelectSingleNode("DosisDiaria").InnerText + "</DosisDiaria>";
                    //msj += "<DiasTratamiento>" + node.SelectSingleNode("DiasTratamiento").InnerText + "</DiasTratamiento>";
                    //msj += "<Generico>" + node.SelectSingleNode("Generico").InnerText + "</Generico>";
                    //msj += "<CodConflicto>" + node.SelectSingleNode("CodConflicto").InnerText + "</CodConflicto>";
                    //msj += "<CodIntervencion>" + node.SelectSingleNode("CodIntervencion").InnerText + "</CodIntervencion>";
                    //msj += "<CodAccion>" + node.SelectSingleNode("CodAccion").InnerText + "</CodAccion>";
                    msj += "<CodRta>0</CodRta><MensajeRta>Anulacion Exitosa</MensajeRta><CodAutorizacion>" + Guid.NewGuid().ToString().Substring(0, 20) + "</CodAutorizacion>";

                    //msj += "<BonoItem>";
                    //msj += "<CostoBono>0</CostoBono>";
                    //msj += "<Uso>1</Uso>";
                    //msj += "<ImporteCobertura>0</ImporteCobertura>";
                    //msj += "<ImporteGentileza>0</ImporteGentileza>";
                    //msj += "<Numero>0</Numero>";
                    //msj += "<CantidadAprobada>1</CantidadAprobada>";
                    ////msj += "<Descripcion>" + node.SelectSingleNode("Descripcion").InnerText + "</Descripcion>";
                    //msj += "</BonoItem>";
                    msj += "</Item>";
                }
                msj += @"</DetalleReceta>";

                //+detalleReceta + detalleRecetaAdic + "</DetalleReceta>";
                msj += @"</MensajeADESFA>";
            }
            catch (Exception ex)
            {
                msj = "<?xml version='1.0' encoding='ISO-8859-1'?><Mensaje>Error inesperado al obtener el mensaje. Detalle: " + ex.Message + "</Mensaje>";
            }

        }
        else
            msj = "<?xml version='1.0' encoding='ISO-8859-1'?><Mensaje>Usuario y/o contraseña incorrecta</Mensaje>";

        return msj;
    }
}
