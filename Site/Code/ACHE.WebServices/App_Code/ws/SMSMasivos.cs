﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for SMSMasivos
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class SMSMasivos : System.Web.Services.WebService {
    public enum ErrorIntegracion
    {
        SessionIDInvalido = -1,
        MensajeNoEnviado = -100
        
    }
    public SMSMasivos () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public SMSResponse SendSMS(SMSRequest datos)
    {
        SMSResponse info = new SMSResponse();
        using (var dbContext = new ACHEEntities())
        {
            if (ValidarSessionID(datos.sessionID, dbContext))
            {
                RenovarFechaValida(datos.sessionID, dbContext);
                TelefonoSMS aux = new TelefonoSMS();
                aux.Celular = datos.phoneNumber;
                aux.EmpresaCelular =datos.company;
                bool enviado = PlusMobile.SendSMS(datos.message, aux);
                if (!enviado)
                    info.AnswerCode = (int)ErrorIntegracion.MensajeNoEnviado;
                else
                    info.sessionID = datos.sessionID;
            }else            
                info.AnswerCode = (int)ErrorIntegracion.SessionIDInvalido;

        }
        return info;

    }



    #region Metodos Login

    [WebMethod]
    public LoginResponse Login(LoginRequest datos)
    {
        LoginResponse info = new LoginResponse();
        using (var dbContext = new ACHEEntities())
        {
            var aux = dbContext.Integraciones.Where(x => x.Usuario == datos.userName && x.Pwd == datos.pwd && x.Activo).FirstOrDefault();
            if (aux != null)
            {
                Guid g = Guid.NewGuid();
                IntegracionesTokens entity = new IntegracionesTokens();
                entity.FechaValidez = DateTime.Now.AddHours(3);
                entity.IDUsuario = aux.IDUsuario;
                entity.SessionID = g.ToString();
                dbContext.IntegracionesTokens.Add(entity);
                dbContext.SaveChanges();
                info.AnswerCode = 0;
                info.SessionID = entity.SessionID;
                Operador op = new Operador();
                op.Name = datos.userName;
                op.LastName = datos.userName;
                info.Operador = op;
            }
            else
            {
                info.AnswerCode = -10;
            }
        }
        return info;
    }

    private bool ValidarSessionID(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            DateTime fechaLimite = aux.FechaValidez;
            DateTime fechaActual = DateTime.Now;
            if (fechaActual <= fechaLimite)
                return true;

        }

        return false;
    }

    private void RenovarFechaValida(string sessionID, ACHEEntities dbContext)
    {

        var aux = dbContext.IntegracionesTokens.Where(x => x.SessionID == sessionID).FirstOrDefault();
        if (aux != null)
        {
            aux.FechaValidez = DateTime.Now.AddHours(3);
            dbContext.SaveChanges();
        }

    }

    #endregion
}
