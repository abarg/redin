﻿<table style="font-family: Arial,Helvetica,sans-serif;" width="550" align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="3" style="padding:10px 0 15px 0" align="left">
				<img src="http://www.redin.com.ar/img/redinlogo.jpg" alt="RedIN" height="100px"  />
			</td>
		</tr>
		<tr><td colspan="3"  width="100%" style="border-top:solid 1px #e8e8e8;display:block"></td></tr>
		<tr>
			<td colspan="3" style="padding: 20px 20px 10px; font-size: 12px; color: rgb(51, 51, 51); line-height: 18px;">
				<span style="font-family:Arial;font-size:18px;color:#333333;padding-bottom:3px">
					Estimado <CLIENTE>:
				</span>
				<p>
					<br />
					Por medio del presente, cumplimos en enviarle la factura correspondiente al mes actual por los servicios prestados.<br /><br />
					
                    Adjuntamos el total de transacciones realizadas por nuestros usuarios.<br />
                    Para acceder a su panel de control y visualizar dichas operaciones en forma online, utilice los siguientes datos:<br /><br />
                    
					URL: <a href="http://comercios.redin.com.ar">http://comercios.redin.com.ar</a><br />
                    USUARIO: Su CUIT<br />
                    CONTRASEÑA: Su EMAIL
                    <br /><br />
                    Ante cualquier consulta, no dude en comunicarse con nosotros a través del 0800-220-4646 de Lunes a Viernes de 9 a 19hs, o bien enviando un mail a <a href="mailto:facturacion@redin.com.ar">facturacion@redin.com.ar</a><br />
					
					<MERCADOPAGO>
					<URLPAGO>
					
					<br />
					<strong>Le pedimos, por favor, nos confirme recepción de este email.</strong>
					
					<br /><br />
					Sin más, le saludamos muy atentamente <br /><br />
					Red IN<br />
					Email: <a href="mailto:empresas@redin.com.ar">empresas@redin.com.ar</a><br />
					Atencion al cliente: 0800-220-4646
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3"  style="padding-top:10px;border-top:solid 1px #e8e8e8">
				<span style="font-family:Arial;font-size:12px;color:#999999">
					No respondas este mail.
				</span>
			</td>
		</tr>
		<tr><td colspan="3"  height="15" style="font-size:1px">&nbsp;</td></tr>
	</tbody>
</table>
