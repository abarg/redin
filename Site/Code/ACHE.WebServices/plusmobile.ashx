﻿<%@ WebHandler Language="C#" Class="plusmobile" %>

using ACHE.Model;
using System.Collections.Generic;
using System.Linq;
using ACHE.Extensions;
using System;
using System.Web;
using System.Configuration;

public class plusmobile : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        var MsgRefId = context.Request.Form["MsgRefId"];
        var PhoneNumber = context.Request.Form["PhoneNumber"];
        var Message = context.Request.Form["Message"];
        var IdSmsCode = context.Request.Form["IdSmsCode"];

        /*var MsgRefId = context.Request.QueryString["MsgRefId"];
        var PhoneNumber = context.Request.QueryString["PhoneNumber"];
        var Message = context.Request.QueryString["Message"];
        var IdSmsCode = context.Request.QueryString["IdSmsCode"];*/


        //EJ: plusmobile.ashx?PhoneNumber=3233&Message=TESLELE&IdSmsCode=111&MsgRefId=222


        var msg = MsgRefId + " - " + PhoneNumber + " - " + Message + " - " + IdSmsCode;
        BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Datos recibidos", msg);

        if (!string.IsNullOrEmpty(MsgRefId) && !string.IsNullOrEmpty(PhoneNumber) && !string.IsNullOrEmpty(Message))
        {
            using (var dbContext = new ACHEEntities())
            {
                if (Message.ToLower().StartsWith("in"))
                {
                    try
                    {
                        var codigo = Message.ToLower().Substring(2, Message.Length - 2).Trim();//.Replace("in", "").Trim();
                        BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Codigo de Keyword:", codigo);

                        wsPlusmobile.AuthHeader Auth = new wsPlusmobile.AuthHeader();
                        Auth.User = ConfigurationManager.AppSettings["Plusmobile.User"];
                        Auth.Pass = ConfigurationManager.AppSettings["Plusmobile.Pwd"];
                        wsPlusmobile.GatewaySoapClient Gateway = new wsPlusmobile.GatewaySoapClient();
                        long intValue;
                        bool esEntero = long.TryParse(codigo, out intValue);

                        //int idMarca = 0, idFranquicia = 0;
                        if (esEntero)//Puede consultar por DNI o NumeroTarjeta
                        {
                            smsDniTarjeta(dbContext, Auth, codigo, PhoneNumber, int.Parse(IdSmsCode), MsgRefId, Message);
                        }
                        else
                        {
                            Keywords key = dbContext.Keywords.Include("Comercios").Where(x => x.Codigo.ToLower() == codigo).FirstOrDefault();
                            if (key != null)
                            {
                                BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Codigo de Keyword:", "existe");
                                if (key.Activo && key.Estado == "Confirmado" && key.FechaInicio <= DateTime.Now && (!key.FechaFin.HasValue) || (key.FechaFin.Value >= DateTime.Now))
                                {
                                    BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Respuesta de Keyword:", key.Respuesta);
                                    var rta = key.Respuesta;
                                    //Valido el rango
                                    if (!string.IsNullOrEmpty(key.NumeroDesde) && !string.IsNullOrEmpty(key.NumeroHasta))
                                    {
                                        //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Rango de tarjetas: ", "OK");

                                        string sql = "select TOP 1 IDTarjeta, Numero from tarjetas where numero>=" + key.NumeroDesde + " and numero<=" + key.NumeroHasta + " and fechabaja is null and tipotarjeta='C'";
                                        var listDisp = dbContext.Database.SqlQuery<TarjetaDisponible>(sql, new object[] { }).ToList();

                                        if (listDisp.Any())
                                        {
                                            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Tarjetas disp: ", "OK");
                                            try
                                            {
                                                var idTarjetaDisp = listDisp[0].IDTarjeta;
                                                var tarjeta = dbContext.Tarjetas.FirstOrDefault(x => x.IDTarjeta == idTarjetaDisp);
                                                if (tarjeta != null)
                                                {
                                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Tarjeta nro: ", tarjeta.Numero);
                                                    rta = rta.Replace("XTARJETAX", tarjeta.Numero);

                                                    tarjeta.FechaBaja = DateTime.Now;//Al validar el rango, pongo la tarjeta como baja para que no se use más.

                                                    //Registro el consumo
                                                    KeywordsConsumos consumos = new KeywordsConsumos();
                                                    consumos.IDKeyword = key.IDKeyword;
                                                    consumos.Fecha = DateTime.Now;
                                                    consumos.PhoneNumber = PhoneNumber;
                                                    consumos.MsgRefId = MsgRefId;
                                                    consumos.Valido = true;
                                                    consumos.TarjetaUsada = tarjeta.Numero;
                                                    dbContext.KeywordsConsumos.Add(consumos);

                                                    key.Stock--;
                                                    dbContext.SaveChanges();

                                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Entro a enviar la TR", "");

                                                    //ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, key.Comercios.IDComercio, "Web", "", "", 0, "", key.Comercios.NumEst.PadLeft(15, '0'), MsgRefId, "", tarjeta.Numero, key.Comercios.POSTerminal,
                                                    //  "CuponIn", "000000000000", "3300", "", "", "", "Plusmobile");

                                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Fin Entro a enviar la TR", "");
                                                }
                                                else
                                                    BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Tarjeta null: ", idTarjetaDisp.ToString());
                                            }
                                            catch (Exception ex)
                                            {
                                                BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Error en crear tr: ", ex.Message);
                                            }

                                        }
                                        else
                                        {
                                            // BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "MensajeSinStock", "");

                                            rta = key.MensajeSinStock;

                                            //Registro el consumo
                                            KeywordsConsumos consumos = new KeywordsConsumos();
                                            consumos.IDKeyword = key.IDKeyword;
                                            consumos.Fecha = DateTime.Now;
                                            consumos.PhoneNumber = PhoneNumber;
                                            consumos.MsgRefId = MsgRefId;
                                            consumos.Valido = false;
                                            consumos.Observations = Message;
                                            dbContext.KeywordsConsumos.Add(consumos);
                                            dbContext.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Rango de tarjetas: ", "Sin Stock");

                                        rta = key.MensajeSinStock;

                                        //Registro el consumo
                                        KeywordsConsumos consumos = new KeywordsConsumos();
                                        consumos.IDKeyword = key.IDKeyword;
                                        consumos.Fecha = DateTime.Now;
                                        consumos.PhoneNumber = PhoneNumber;
                                        consumos.MsgRefId = MsgRefId;
                                        consumos.Valido = false;
                                        consumos.Observations = Message;
                                        dbContext.KeywordsConsumos.Add(consumos);
                                        dbContext.SaveChanges();
                                    }

                                    //try
                                    //{
                                    //Envio la respuesta
                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Respuesta: ", rta);
                                    string Mensaje = rta;
                                    string respuestaXml = Gateway.sendMessageOneToOne(Auth, int.Parse(MsgRefId), PhoneNumber, int.Parse(IdSmsCode), Mensaje);
                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Respuesta de plusmobile:", respuestaXml);
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Exception en sendMessageOneToOne: ", ex.Message);
                                    //}
                                }
                                else//Fuera de vigencia
                                {
                                    KeywordsConsumos consumos = new KeywordsConsumos();
                                    consumos.IDKeyword = key.IDKeyword;
                                    consumos.Fecha = DateTime.Now;
                                    consumos.PhoneNumber = PhoneNumber;
                                    consumos.MsgRefId = MsgRefId;
                                    consumos.Valido = false;
                                    consumos.Observations = Message;
                                    dbContext.KeywordsConsumos.Add(consumos);
                                    dbContext.SaveChanges();

                                    //Envio la respuesta
                                    BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Codigo de Keyword:", "Fuera de vigencia");
                                    string Mensaje = key.MensajeVencida;
                                    string respuestaXml = Gateway.sendMessageOneToOne(Auth, int.Parse(MsgRefId), PhoneNumber, int.Parse(IdSmsCode), Mensaje);
                                    //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Respuesta de plusmobile:", respuestaXml);
                                }
                            }
                            else//Inexistente
                            {
                                KeywordsConsumos consumos = new KeywordsConsumos();
                                consumos.Fecha = DateTime.Now;
                                consumos.PhoneNumber = PhoneNumber;
                                consumos.MsgRefId = MsgRefId;
                                consumos.Valido = false;
                                consumos.Observations = Message;
                                dbContext.KeywordsConsumos.Add(consumos);
                                dbContext.SaveChanges();

                                //Envio la respuesta
                                BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Codigo de Keyword:", "Inexistente");
                                string Mensaje = "Codigo incorrecto";
                                string respuestaXml = Gateway.sendMessageOneToOne(Auth, int.Parse(MsgRefId), PhoneNumber, int.Parse(IdSmsCode), Mensaje);
                                //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Respuesta de plusmobile:", respuestaXml);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        BasicLog.AppendToFile(ConfigurationManager.AppSettings["PlusMobileLogError"], "Error inesperado: ", ex.Message);
                    }
                }
                else//Código incorrecto
                {
                    KeywordsConsumos consumos = new KeywordsConsumos();
                    consumos.Fecha = DateTime.Now;
                    consumos.PhoneNumber = PhoneNumber;
                    consumos.MsgRefId = MsgRefId;
                    consumos.Valido = false;
                    consumos.Observations = Message;
                    dbContext.KeywordsConsumos.Add(consumos);
                    dbContext.SaveChanges();
                }
            }


        }

    }

    private void smsDniTarjeta(ACHEEntities dbContext, wsPlusmobile.AuthHeader Auth, string codigo, string PhoneNumber, int IdSmsCode, string MsgRefId, string Message)
    {
        string numTarjeta = "";
        decimal costoSMS = 0;
        if (codigo.Length < 10)//DNI
        {
            var socio = dbContext.Socios.Include("Tarjetas").Where(x => x.NroDocumento == codigo && x.Tarjetas.Any()).FirstOrDefault();
            var msj = "";
            if (socio == null)
            {
                msj = "Tu DNI no se encuentra dado de alta en RedIN.";
                PlusMobile.SendSMS(msj, new TelefonoSMS() { Celular = PhoneNumber, EmpresaCelular = "", IdSmsCode = IdSmsCode });
            }
            else
            {
                var puntos = socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).Sum(x => x.PuntosTotales);
                var credito = socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).Sum(x => x.Credito);
                if (puntos < 0)
                {
                    puntos = 0;
                    credito = 0;
                }
                var puntosGrupoFamiliar = Common.obtenerPuntosGrupoFamiliar(dbContext, socio);

                if (puntosGrupoFamiliar > 0)
                    msj = "Hola " + socio.Nombre + " " + socio.Apellido + ", usted tiene " + puntosGrupoFamiliar + " puntos en el grupo familiar equivalente a $" + (puntosGrupoFamiliar / 100).ToString("N2") + " PESOS disponible para utilizar.";
                else
                    msj = "Hola " + socio.Nombre + " " + socio.Apellido + ", usted tiene " + puntos + " puntos equivalente a $" + credito.ToString("N2") + " PESOS disponible para utilizar.";

                PlusMobile.SendSMS(msj, new TelefonoSMS() { Celular = PhoneNumber, EmpresaCelular = "", IdSmsCode = IdSmsCode });

                costoSMS = socio.CostoSMS.HasValue ? socio.CostoSMS.Value : 0;
                var unaTar = socio.Tarjetas.ElementAt(0);
                //var tarjeta = dbContext.Tarjetas.Where(x => x.Numero == unaTar.Numero && !x.FechaBaja.HasValue).FirstOrDefault();
                numTarjeta = unaTar.Numero;

                if (costoSMS == 0)//Busco el costo de la marca
                {
                    var tarjetasMarcasConCosto = socio.Tarjetas.Where(x => !x.FechaBaja.HasValue && x.Marcas.CostoSMS2.HasValue && x.Marcas.CostoSMS2.Value > 0).ToList();
                    if (tarjetasMarcasConCosto.Any())
                    {
                        numTarjeta = tarjetasMarcasConCosto.First().Numero;
                        costoSMS = tarjetasMarcasConCosto.First().Marcas.CostoSMS2.Value;
                    }
                    /*costoSMS = tarjeta.Marcas.CostoSMS2.HasValue ? tarjeta.Marcas.CostoSMS2.Value : 0;
                    int i = 1;
                    while ((!(costoSMS > 0)) && (i < socio.Tarjetas.Count()))
                    {
                        unaTar = socio.Tarjetas.ElementAt(i);
                        tarjeta = dbContext.Tarjetas.Where(x => x.Numero == unaTar.Numero && !x.FechaBaja.HasValue).FirstOrDefault();
                        //costoSMS = costoSMS;
                        numTarjeta = tarjeta.Numero;
                        i++;
                    }*/
                }
                /*idMarca = tarjeta.Marcas.IDMarca;
                if (tarjeta.Marcas.IDFranquicia.HasValue)
                {
                    idFranquicia = tarjeta.Marcas.IDFranquicia.Value;
                }*/
            }

            //Registro el consumo
            KeywordsConsumos consumos = new KeywordsConsumos();
            consumos.IDKeyword = null;
            consumos.Fecha = DateTime.Now;
            consumos.PhoneNumber = PhoneNumber;
            consumos.MsgRefId = MsgRefId;
            consumos.Valido = false;
            consumos.Observations = Message;
            dbContext.KeywordsConsumos.Add(consumos);
            dbContext.SaveChanges();

            if (costoSMS > 0)
            {
                ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, 0, "Web", "", "", costoSMS, "", ConfigurationManager.AppSettings["Plusmobile.NumEst"], "", "", numTarjeta, ConfigurationManager.AppSettings["Plusmobile.NumTerminal"],
                "Canje", "000000000000", "3300", "", "", "", "Plusmobile");
            }
        }
        else
        {
            var tar = dbContext.Tarjetas.Include("Socios").Include("Marcas").Where(x => x.Numero == codigo && !x.FechaBaja.HasValue).FirstOrDefault();
            var msj = "";
            if (tar == null)
            {
                msj = "El numero de tarjeta ingresado es inexistente.";
                PlusMobile.SendSMS(msj, new TelefonoSMS() { Celular = PhoneNumber, EmpresaCelular = "", IdSmsCode = IdSmsCode });
            }
            else
            {
                var puntos = tar.PuntosTotales;
                var credito = tar.Credito;
                var nombre = tar.IDSocio.HasValue ? tar.Socios.Nombre + " " + tar.Socios.Apellido : "";
                if (puntos < 0)
                {
                    puntos = 0;
                    credito = 0;
                }

                var puntosGrupoFamiliar = Common.obtenerPuntosGrupoFamiliar(dbContext, tar.Socios);

                if (puntosGrupoFamiliar > 0)
                    msj = "Hola " + nombre + ", usted tiene " + puntosGrupoFamiliar + " puntos en el grupo familiar equivalente a $" + (puntosGrupoFamiliar / 100).ToString("N2") + " PESOS disponible para utilizar.";
                else
                    msj = "Hola " + nombre + ", usted tiene " + puntos + " puntos equivalente a $" + credito.ToString("N2") + " PESOS disponible para utilizar.";

                //msj = "Tienes " + puntos + " puntos disponibles";
                PlusMobile.SendSMS(msj, new TelefonoSMS() { Celular = PhoneNumber, EmpresaCelular = "", IdSmsCode = IdSmsCode });

                numTarjeta = codigo;
                costoSMS = tar.Socios.CostoSMS.HasValue ? tar.Socios.CostoSMS.Value : 0;
                if (costoSMS == 0)
                    costoSMS = tar.Marcas.CostoSMS2.HasValue ? tar.Marcas.CostoSMS2.Value : 0;

                if (costoSMS > 0)
                {
                    ACHE.Business.Common.CrearTransaccion(dbContext, DateTime.Now, 0, "Web", "", "", costoSMS, "", ConfigurationManager.AppSettings["Plusmobile.NumEst"], "", "", numTarjeta, ConfigurationManager.AppSettings["Plusmobile.NumTerminal"],
                    "Canje", "000000000000", "3300", "", "", "", "Plusmobile");
                }

                /*idMarca = tar.Marcas.IDMarca;
                if (tar.Marcas.IDFranquicia.HasValue)
                {
                    idFranquicia = tar.Marcas.IDFranquicia.Value;
                }*/
            }

            //Registro el consumo
            KeywordsConsumos consumos = new KeywordsConsumos();
            consumos.IDKeyword = null;
            consumos.Fecha = DateTime.Now;
            consumos.PhoneNumber = PhoneNumber;
            consumos.MsgRefId = MsgRefId;
            consumos.Valido = false;
            consumos.Observations = Message;
            dbContext.KeywordsConsumos.Add(consumos);
            dbContext.SaveChanges();
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}