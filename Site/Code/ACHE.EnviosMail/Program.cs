﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.Mail;


namespace ACHE.EnviosMail
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var dbContext = new ACHEEntities())
            {
                string emailTo = ConfigurationManager.AppSettings["Email.To"];
                EnviarEmailCumpleanios(dbContext, emailTo);
                EnviarAlertasMarcas(dbContext, emailTo);
                EnviarAlertasComercio(dbContext, emailTo);
                EnviarCampaniasEmail(dbContext, emailTo);
            }

        }

        private static void EnviarEmailCumpleanios(ACHEEntities dbContext, string emailTo)
        {
            //Obtener los socios que tengan tarjetas pertenecientes a una marca que tenga este parametro habilitado.
            DateTime fechaHoy = DateTime.Now;

            var sociosList = dbContext.Tarjetas.Include("Socios").Where(x => !x.FechaBaja.HasValue && x.IDSocio.HasValue
                && x.Marcas.EnvioEmailCumpleanios //&& !string.IsNullOrEmpty(x.Marcas.EmailCumpleanios)
                && !string.IsNullOrEmpty(x.Socios.Email)
                && x.Socios.FechaNacimiento.HasValue
                && x.Socios.FechaNacimiento.Value.Month == fechaHoy.Month && x.Socios.FechaNacimiento.Value.Day == fechaHoy.Day).Select(x => new
                {
                    //Mensaje = x.Marcas.EmailCumpleanios,
                    IDMarca = x.IDMarca,
                    Email = x.Socios.Email,
                    Nombre = x.Socios.Nombre,
                    Apellido = x.Socios.Apellido
                }).Distinct().ToList();

            if (sociosList.Any())
            {
                int cantAEnviar = sociosList.Count();
                int cantEnviados, cantNoEnviados;
                cantEnviados = cantNoEnviados = 0;
                bool send;
                foreach (var socio in sociosList)
                {
                    try
                    {
                        var marca = dbContext.Marcas.Where(x => x.IDMarca == socio.IDMarca).First();
                        if (!string.IsNullOrEmpty(marca.EmailCumpleanios))
                        {

                            ListDictionary mensaje = new ListDictionary();
                            string mailHtml = marca.EmailCumpleanios.Replace("XNOMBREX", socio.Nombre).Replace("XAPELLIDOX", socio.Apellido);
                            mensaje.Add("<MENSAJE>", mailHtml);
                            send = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, mensaje, socio.Email, "Te deseamos un feliz cumpleaños!");
                            if (!send)
                            {
                                cantNoEnviados++;
                                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                BasicLog.AppendToFile(dir, "Email: " + socio.Email, "No se ha podido enviar email de cumpleaños");
                            }
                            else
                                cantEnviados++;
                        }
                        else
                        {
                            cantNoEnviados++;
                            var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                            BasicLog.AppendToFile(dir, "Email: " + socio.Email, "No hay mensaje predefinido");
                        }
                    }
                    catch (Exception ex)
                    {
                        var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                        BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    }
                }
                //dbContext.SaveChanges();

                ListDictionary datos = new ListDictionary();
                datos.Add("<CANTENVIADOS>", cantEnviados);
                datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                datos.Add("<CANTTOTAL>", cantAEnviar);
                datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));

                send = EmailHelper.SendMessage(EmailTemplate.Notificacion, datos, emailTo, "Envíos de mail por cumpleaños finalizado");
                if (!send)
                {
                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                    BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por Email de cumpleaños");
                }
            }

        }


        private static void EnviarCampaniasEmail(ACHEEntities dbContext, string emailTo)
        {
            //Obtener los socios que tengan tarjetas pertenecientes a una marca que tenga este parametro habilitado.
            DateTime fechaHoy = DateTime.Now;

            var sociosList = dbContext.Tarjetas.Include("Socios").Where(x => !x.FechaBaja.HasValue && x.IDSocio.HasValue
                && x.Marcas.EnvioEmailCumpleanios //&& !string.IsNullOrEmpty(x.Marcas.EmailCumpleanios)
                && !string.IsNullOrEmpty(x.Socios.Email)
                && x.Socios.FechaNacimiento.HasValue
                && x.Socios.FechaNacimiento.Value.Month == fechaHoy.Month && x.Socios.FechaNacimiento.Value.Day == fechaHoy.Day).Select(x => new
                {
                    //Mensaje = x.Marcas.EmailCumpleanios,
                    IDMarca = x.IDMarca,
                    Email = x.Socios.Email,
                    Nombre = x.Socios.Nombre,
                    Apellido = x.Socios.Apellido
                }).Distinct().ToList();

            if (sociosList.Any())
            {
                int cantAEnviar = sociosList.Count();
                int cantEnviados, cantNoEnviados;
                cantEnviados = cantNoEnviados = 0;
                bool send;
                foreach (var socio in sociosList)
                {
                    try
                    {
                        var marca = dbContext.Marcas.Where(x => x.IDMarca == socio.IDMarca).First();
                        if (!string.IsNullOrEmpty(marca.EmailCumpleanios))
                        {

                            ListDictionary mensaje = new ListDictionary();
                            string mailHtml = marca.EmailCumpleanios.Replace("XNOMBREX", socio.Nombre).Replace("XAPELLIDOX", socio.Apellido);
                            mensaje.Add("<MENSAJE>", mailHtml);
                            send = EmailHelper.SendMessage(EmailTemplate.EnvioSoloHTML, mensaje, socio.Email, "Te deseamos un feliz cumpleaños!");
                            if (!send)
                            {
                                cantNoEnviados++;
                                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                BasicLog.AppendToFile(dir, "Email: " + socio.Email, "No se ha podido enviar email de cumpleaños");
                            }
                            else
                                cantEnviados++;
                        }
                        else
                        {
                            cantNoEnviados++;
                            var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                            BasicLog.AppendToFile(dir, "Email: " + socio.Email, "No hay mensaje predefinido");
                        }
                    }
                    catch (Exception ex)
                    {
                        var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                        BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    }
                }
                //dbContext.SaveChanges();

                ListDictionary datos = new ListDictionary();
                datos.Add("<CANTENVIADOS>", cantEnviados);
                datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                datos.Add("<CANTTOTAL>", cantAEnviar);
                datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));

                send = EmailHelper.SendMessage(EmailTemplate.Notificacion, datos, emailTo, "Envíos de mail por cumpleaños finalizado");
                if (!send)
                {
                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                    BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por Email de cumpleaños");
                }
            }

        }

        private static void EnviarAlertasComercio(ACHEEntities dbContext, string emailTo)
        {
            var alertasComercio = dbContext.Alertas.Include("Comercios").Where(x => x.Activa && x.IDComercio.HasValue).ToList();

            if (alertasComercio.Any())
            {
                int cantTotalAlertas = alertasComercio.Count();
                int cantAEnviar = 0;
                int cantEnviados, cantNoEnviados;
                cantEnviados = cantNoEnviados = 0;
                bool send;

                foreach (var alerta in alertasComercio)
                {
                    
                    int cant = obtenerCantidadComercios(alerta, dbContext, alerta.IDComercio.Value);
                    if (cant > 0)
                    {
                        cantAEnviar++;
                        var usuariosList = alerta.Comercios.UsuariosComercios.Where(x => x.Activo && x.Tipo == "A");
                        //var usuariosList = dbContext.UsuariosComercios.Where(x => x.Activo && x.Tipo == "A" && alerta.IDComercio == x.IDComercio);
                        if (usuariosList.Any())
                        {
                            MailAddressCollection toList = new MailAddressCollection();
                            foreach (var usuario in usuariosList)
                            {
                                if (usuario.Email != string.Empty)
                                    toList.Add(usuario.Email);
                            }


                            if(!string.IsNullOrEmpty(alerta.Comercios.EmailAlertas))
                                toList.Add(alerta.Comercios.EmailAlertas);

                            //string emailAlerta = dbContext.Comercios.Where(x => x.IDComercio == alerta.IDComercio).FirstOrDefault().EmailAlertas;
                            //if (!string.IsNullOrEmpty(emailAlerta))
                            //    toList.Add(emailAlerta);


                            try
                            {
                                ListDictionary mensaje = new ListDictionary();
                                //mensaje.Add("<NOMBREALERTA>", alerta.Nombre);
                                mensaje.Add("<DETALLE>", "Se han encontrado" + cant + " transacciones en la alerta denominada " + alerta.Nombre);
                                send = EmailHelper.SendMessage(EmailTemplate.Alertas, mensaje, toList, "Red IN - Alerta generada para " + alerta.Nombre, null);
                                if (!send)
                                {
                                    cantNoEnviados++;
                                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                    BasicLog.AppendToFile(dir, "Alerta: " + alerta.Nombre, "No se ha podido enviar email de alerta de comercios");
                                }
                                else
                                {
                                    cantEnviados++;
                                }
                            }
                            catch (Exception ex)
                            {
                                cantEnviados++;
                                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                            }
                            ListDictionary datos = new ListDictionary();
                            datos.Add("<CANTENVIADOS>", cantEnviados);
                            datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                            datos.Add("<CANTTOTAL>", cantAEnviar);
                            datos.Add("<CANTTOTALALERTAS>", cantTotalAlertas);
                            datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                            datos.Add("<LOGIN>", "<a href='https://www.redin.com.ar/login-comercios.aspx'>aqui</a>");

                            send = EmailHelper.SendMessage(EmailTemplate.NotificacionesAlertas, datos, emailTo, "Envíos de mail por alertas a comercios");
                            if (!send)
                            {
                                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por alertas de comercios");
                            }
                        }
                    }
                }
            }
        }

        private static void EnviarAlertasMarcas(ACHEEntities dbContext, string emailTo)
        {
            var alertasMarcas = dbContext.Alertas.Where(x => x.Activa && x.IDMarca.HasValue).ToList();
            if (alertasMarcas.Any())
            {
                int cantTotalAlertas = alertasMarcas.Count();
                int cantAEnviar = 0;
                int cantEnviados, cantNoEnviados;
                cantEnviados = cantNoEnviados = 0;
                bool send;

                foreach (var alerta in alertasMarcas)
                {
                    int cant = obtenerCantidadMarcas(alerta, dbContext, alerta.IDMarca.Value);
                    if (cant > 0)
                    {
                        cantAEnviar++;
                        //var usuariosList = alerta.Marcas.UsuariosMarcas.Where(x => x.Activo && x.Tipo == "A");
                        //if (usuariosList.Any())
                        //{
                            MailAddressCollection toList = new MailAddressCollection();
                            //foreach (var usuario in usuariosList)
                            //{
                            //    if (usuario.Email != string.Empty)
                            //        toList.Add(usuario.Email);
                            //}

                            #region multiEmails
                            if(!string.IsNullOrEmpty(alerta.Marcas.EmailAlertas)){
                                var emails=alerta.Marcas.EmailAlertas.Split(",");
                                foreach (var email in emails) {
                                    if(!toList.Any(x=> x.Address == email))
                                        toList.Add(email);
                                }
                            }
                            #endregion

                            try
                            {
                                ListDictionary mensaje = new ListDictionary();
                                //mensaje.Add("<NOMBREALERTA>", alerta.Nombre);
                                mensaje.Add("<DETALLE>", "Se han encontrado" + cant + " transacciones en la alerta denominada " + alerta.Nombre);
                                send = EmailHelper.SendMessage(EmailTemplate.Alertas, mensaje, toList, "Red IN - Alerta generada para " + alerta.Nombre, null);
                                if (!send)
                                {
                                    cantNoEnviados++;
                                    var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                    BasicLog.AppendToFile(dir, "Alerta: " + alerta.Nombre, "No se ha podido enviar email de alerta de marca");
                                }
                                else
                                {
                                    cantEnviados++;
                                }
                            }
                            catch (Exception ex)
                            {
                                cantEnviados++;
                                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                            }
                        //}
                    }
                    string loginMarcas = ConfigurationManager.AppSettings["LoginMarcas"];

                    ListDictionary datos = new ListDictionary();
                    datos.Add("<CANTENVIADOS>", cantEnviados);
                    datos.Add("<CANTNOENVIADOS>", cantNoEnviados);
                    datos.Add("<CANTTOTAL>", cantAEnviar);
                    datos.Add("<CANTTOTALALERTAS>", cantTotalAlertas);
                    datos.Add("<FECHAENVIO>", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                    datos.Add("<LOGIN>", "<a href='https://www.redin.com.ar/login-marcas.aspx'>aqui</a>");

                   
                    send = EmailHelper.SendMessage(EmailTemplate.NotificacionesAlertas, datos, emailTo, "Envíos de mail por alertas a marcas");
                    if (!send)
                    {
                        var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/Email_XX.log";
                        BasicLog.AppendToFile(dir, "Email: ", "No se ha podido enviar email a administrador por alertas de marcas");
                    }
                }
            }
        }

        private static int obtenerCantidadMarcas(Alertas alerta, ACHEEntities dbContext, int IDMarca)
        {
            int cant = 0;
            string sql = "";
            string tarjetas = "";

            if (alerta.Restringido)
            {
                foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDMarca == IDMarca))
                    tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";

                if (tarjetas.Length > 0)
                    tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
            }

            DateTime Fecha = DateTime.Now;
            string fechaHoy = Fecha.ToShortDateString();
            /*
            if (alerta.FechaDesde.HasValue)
                fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);
            */
            if (alerta.Tipo == "Por monto")
            {
                sql = "select count(Numero) from TransaccionesMarcasView where IDMarca=" + IDMarca;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)

                    sql += " and Fecha = '" + fechaHoy + "'";

            }
            else if (alerta.Tipo == "Por cantidad")
            {
                sql = "select count(cant) from (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + IDMarca;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)
                    sql += " and Fecha = '" + fechaHoy + "'";
                sql += " group by Numero";
                sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";

                //cant = dbContext.Database.SqlQuery<int>(sql, null).First();
            }
            else if (alerta.Tipo == "Por monto y cantidad")
            {
                sql = "select count(cant) from (select distinct Numero as cant from TransaccionesMarcasView where IDMarca=" + IDMarca;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)
                    sql += " and Fecha = '" + fechaHoy + "'";
                sql += " group by Numero";
                sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";
            }

            cant = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();

            return cant;
        }

        private static int obtenerCantidadComercios(Alertas alerta, ACHEEntities dbContext, int IDComercio)
        {
            int cant = 0;
            string sql = "";
            string tarjetas = "";

            if (alerta.Restringido)
            {
                foreach (var tarj in dbContext.AlertasTarjetas.Include("Tarjetas").Where(x => x.IDComercio == IDComercio))
                    tarjetas += "'" + tarj.Tarjetas.Numero.ToString() + "',";

                if (tarjetas.Length > 0)
                    tarjetas = tarjetas.Substring(0, tarjetas.Length - 1);
            }

            DateTime Fecha = DateTime.Now;
            string fechaHoy = Fecha.ToShortDateString();
            /*
            if (alerta.FechaDesde.HasValue)
                fechaDesde = DateTime.Now.AddDays((alerta.FechaDesde.Value) * -1);
            */
            if (alerta.Tipo == "Por monto")
            {
                sql = "select count(Numero) from TransaccionesView where IDComercio=" + IDComercio;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)
                    sql += " and Fecha = '" + fechaHoy + "'";

            }
            else if (alerta.Tipo == "Por cantidad")
            {
                sql = "select count(cant) from (select distinct Numero as cant from TransaccionesView where IDComercio=" + IDComercio;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                //sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)
                    sql += " and Fecha = '" + fechaHoy + "'";

                sql += " group by Numero";
                sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";

                //cant = dbContext.Database.SqlQuery<int>(sql, null).First();
            }
            else if (alerta.Tipo == "Por monto y cantidad")
            {
                sql = "select count(cant) from (select distinct Numero as cant from TransaccionesView where IDComercio=" + IDComercio;
                if (alerta.Restringido && tarjetas.Length > 0)
                    sql += " and Numero in (" + tarjetas + ")";
                sql += " and ImporteOriginal >= " + alerta.MontoDesde.Value + " and ImporteOriginal <= " + alerta.MontoHasta.Value;
                if (alerta.FechaDesde.HasValue)
                    sql += " and Fecha = '" + fechaHoy + "'";
                sql += " group by Numero";
                sql += " having (count(fecha) >= " + alerta.CantTR.Value + ")) as T";
            }

            cant = dbContext.Database.SqlQuery<int>(sql, new object[] { }).First();

            return cant;
        }
    }

}
