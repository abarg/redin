//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class RankingSocios
    {
        public string Socio { get; set; }
        public string NroDoc { get; set; }
        public string Tarjeta { get; set; }
        public Nullable<int> IDFranquicia { get; set; }
        public int IDMarca { get; set; }
        public string Marca { get; set; }
        public string Franquicia { get; set; }
        public Nullable<int> CantTr { get; set; }
        public decimal Importe { get; set; }
        public decimal Ahorro { get; set; }
    }
}
