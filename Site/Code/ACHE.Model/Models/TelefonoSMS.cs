﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class TelefonoSMS {
        public string Celular { get; set; }
        public string EmpresaCelular { get; set; }
        public int IdSmsCode { get; set; }
    }

    public class EnvioSMS {
        public string Celular { get; set; }
        public bool Enviado { get; set; }
        public DateTime FechaEnvio { get; set; }
    }
}
