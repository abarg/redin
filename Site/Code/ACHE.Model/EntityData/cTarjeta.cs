﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cTarjeta
    {
        public Tarjetas getTarjeta(int IDTarjeta)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    return context.Tarjetas.FirstOrDefault(t => t.IDTarjeta == IDTarjeta);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tarjetas getTarjetaPorNumero(string numero, bool includeSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    if(!includeSocio)
                        return context.Tarjetas.Include("Marcas").FirstOrDefault(t => t.Numero == numero);
                    else
                        return context.Tarjetas.Include("Marcas").Include("Socios").FirstOrDefault(t => t.Numero == numero);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*public List<Tarjetas> getTarjetasForVisa()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _listTarjetas = (from s in context.Tarjetas

                                         select new Tarjetas
                                         {
                                             IDTarjeta = s.IDTarjeta,
                                             IDMarca = s.IDMarca,
                                             Numero = s.Numero,
                                             FechaAlta = s.FechaAlta,
                                             FechaEmision = s.FechaEmision.HasValue ? s.FechaEmision.Value : DateTime.MinValue,
                                             FechaVencimiento = s.FechaVencimiento.HasValue ? s.FechaVencimiento.Value : DateTime.MinValue,
                                             Affinity = s.Marcas.Affinity,
                                             Estado = s.Estado,
                                             PuntosDisponibles = s.PuntosDisponibles ?? 0,
                                             PuntosTotales = s.PuntosDisponibles ?? 0,
                                             MotivoBaja = s.MotivoBaja,
                                             FechaBaja = s.FechaBaja,
                                             UsuarioBaja = s.UsuarioBaja,
                                             IDSocio = s.IDSocio ?? 0
                                         }).ToList();

                    return _listTarjetas;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        /*public List<Tarjetas> getTarjetas(int IDSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _listTarjetas = (from s in context.Tarjetas
                                         where s.IDSocio == IDSocio

                                         select new Tarjetas
                                         {
                                             IDTarjeta = s.IDTarjeta,
                                             IDMarca = s.IDMarca,
                                             Numero = s.Numero,
                                             FechaAlta = s.FechaAlta,
                                             FechaEmision = s.FechaEmision.HasValue ? s.FechaEmision.Value : DateTime.MinValue,
                                             FechaVencimiento = s.FechaVencimiento.HasValue ? s.FechaVencimiento.Value : DateTime.MinValue,
                                             Affinity = s.Marcas.Affinity,
                                             Estado = s.Estado,
                                             PuntosDisponibles = s.PuntosDisponibles ?? 0,
                                             PuntosTotales = s.PuntosDisponibles ?? 0,
                                             MotivoBaja = s.MotivoBaja,
                                             FechaBaja = s.FechaBaja,
                                             UsuarioBaja = s.UsuarioBaja,
                                             IDSocio = s.IDSocio ?? 0
                                         }).ToList();

                    return _listTarjetas;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        public bool deleteTarjeta(int IDTarjeta)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oTarjeta = context.Tarjetas.FirstOrDefault(t => t.IDTarjeta == IDTarjeta);
                    context.Tarjetas.Remove(_oTarjeta);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tarjetas insertTarjeta(Tarjetas oTarjeta)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    context.Tarjetas.Add(oTarjeta);
                    context.SaveChanges();
                    return oTarjeta;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarTarjetasMasivamente(DataTable dt)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();
                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                    {
                        bulkCopy.DestinationTableName = " [dbo].[Tarjetas]";

                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        //Conversión de Tipo de datos datetime, etc.
                        bulkCopy.WriteToServer(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tarjetas updateTarjeta(Tarjetas oTarjeta)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    context.Entry(oTarjeta).State = System.Data.Entity.EntityState.Modified; 
                    
                    //context.Tarjetas.Attach(oTarjeta);
                    //context.Tarjetas.ApplyCurrentValues(oTarjeta);
                    //context.ObjectStateManager.ChangeObjectState(oTarjeta, EntityState.Modified);

                    context.SaveChanges();
                    return oTarjeta;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
