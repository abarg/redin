﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.EntityData {
    public class cMultimarca {
        public List<Marcas> getMarcas(int idMultimarca) {
            try {
                using (var context = new ACHEEntities()) {
                    var aux = context.MarcasAsociadas.Where(x => x.IDMultimarca == idMultimarca).Select(x => x.IDMarca).ToList();
                    var _list = context.Marcas.Where(m=> aux.Contains(m.IDMarca)).OrderBy(x => x.Nombre).ToList();

                    return _list;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
