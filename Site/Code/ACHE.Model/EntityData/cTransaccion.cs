﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cTransaccion
    {
        public void limpiarTransaccionesTmp()
        {
            try
            {
                SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, System.Data.CommandType.Text, "DELETE FROM TransaccionesTmp");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarTransaccionesTmp(DataTable dt)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();
                    using (var bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = " [dbo].[TransaccionesTmp]";

                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarTransacciones(string nombreArchivo, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.CommandTimeout = 2000;
                    dbContext.Database.ExecuteSqlCommand("exec tmpActualizarTransacciones @nombreArchivo = '" + nombreArchivo + "', @idUsuario = " + idUsuario + "", new object[] { });
                    dbContext.Database.ExecuteSqlCommand("exec actualizarPuntosPorTarjetas", new object[] { });

                }

                //SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "tmpActualizarTransacciones", new object[] { nombreArchivo, idUsuario });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
