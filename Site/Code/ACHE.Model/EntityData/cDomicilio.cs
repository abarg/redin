﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ACHE.Entities;

namespace ACHE.Model.EntityData
{
    public class cDomicilio
    {
        public Domicilios getDomicilio(int IDDomicilio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var entity = context.Domicilios.FirstOrDefault(d => d.IDDomicilio == IDDomicilio);
                    return entity;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Domicilios insertDomicilio(Domicilios oDomicilio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    context.Domicilios.AddObject(oDomicilio);
                    context.SaveChanges();
                    
                    return oDomicilio;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Domicilios updateDomicilio(Domicilios oDomicilio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var entity = context.Domicilios.FirstOrDefault(d => d.IDDomicilio == oDomicilio.IDDomicilio);
                    if (entity != null)
                    {
                        entity.TipoDomicilio = oDomicilio.TipoDomicilio;
                        entity.Estado = oDomicilio.Estado;
                        entity.FechaAlta = oDomicilio.FechaAlta;
                        entity.Pais = oDomicilio.Pais;
                        entity.Provincia = oDomicilio.Provincia;
                        entity.Ciudad = oDomicilio.Ciudad;
                        entity.Domicilio = oDomicilio.Domicilio;
                        entity.CodigoPostal = oDomicilio.CodigoPostal;
                        entity.Telefono = oDomicilio.Telefono;
                        entity.Fax = oDomicilio.Fax;
                        entity.PisoDepto = oDomicilio.PisoDepto;

                        context.SaveChanges();
                        return oDomicilio;
                    }
                    else
                        throw new Exception("El domicilio no existe");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
