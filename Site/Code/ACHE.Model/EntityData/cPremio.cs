﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cPremio
    {
        public void limpiarPremiosTmp()
        {
            try
            {
                SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, System.Data.CommandType.Text, "DELETE FROM PremiosTmp");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarPremiosTmp(DataTable dt,string path)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();
                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                    {
                        bulkCopy.DestinationTableName = " [dbo].[PremiosTmp]";

                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarPremios(bool esPrivado)
        {
            try
            {
                if(!esPrivado)
                    SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "tmpActualizarPremios");
                else
                    SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "tmpActualizarProductos");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Premios> getPremios()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _listPremios = context.Premios
                        .Include("Marcas")
                        .Where(x => !x.IDMarca.HasValue || (x.IDMarca.HasValue && x.Marcas.TipoCatalogo == "A")).ToList();
                    return _listPremios;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Premios getPremio(int IDPremio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    return context.Premios.FirstOrDefault(x => x.IDPremio == IDPremio);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Premios getPremioPorImporte(decimal importe)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    return context.Premios.FirstOrDefault(x => x.ValorPesos == importe && x.TipoMov != "B" && !x.IDMarca.HasValue);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
