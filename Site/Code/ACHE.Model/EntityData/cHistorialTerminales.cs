﻿using System;
using System.Linq;

namespace ACHE.Model.EntityData
{
    public class cHistorialTerminales
    {
        public bool deleteHistorial(int id)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oEntity = context.HistorialTerminales.FirstOrDefault(s => s.IDHistorial == id);
                    context.HistorialTerminales.Remove(_oEntity);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
