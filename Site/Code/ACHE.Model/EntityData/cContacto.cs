﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ACHE.Entities;

namespace ACHE.Model.EntityData
{
    public class cContacto
    {
        public Contactos getContacto(int IDContacto)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var entity = context.Contactos.FirstOrDefault(c => c.IDContacto == IDContacto);
                    return entity;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Contactos insertContacto(Contactos oContacto)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    context.Contactos.AddObject(oContacto);
                    context.SaveChanges();

                    return oContacto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Contactos updateContacto(Contactos oContacto)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var entity = context.Contactos.FirstOrDefault(c => c.IDContacto == oContacto.IDContacto);
                    if (entity != null)
                    {
                        entity.Nombre = oContacto.Nombre;
                        entity.Apellido = oContacto.Apellido;
                        entity.Cargo = oContacto.Cargo;
                        entity.Telefono = oContacto.Telefono;
                        entity.Celular = oContacto.Celular;
                        entity.Email = oContacto.Email;
                        entity.Estado = oContacto.Estado;
                        entity.FechaAlta = oContacto.FechaAlta;
                        entity.Observaciones = oContacto.Observaciones;
                        entity.NroDocumento = oContacto.NroDocumento;

                        context.SaveChanges();
                        return entity;
                    }
                    else
                        throw new Exception("El contacto no existe");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
