﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace ACHE.Model.EntityData
{
    public class cUsuario
    {
        /// <summary>
        /// Add Usuario
        /// </summary>
        public Usuarios insertUsuario(Usuarios oUsuario)
        {
            try
            {               
                using (var context = new ACHEEntities())
                {
                    context.Usuarios.Add(oUsuario);
                    context.SaveChanges();

                    return oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Usuario
        /// </summary>
        public Usuarios updateUsuario(Usuarios oUsuario)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.IDUsuario == oUsuario.IDUsuario);

                    _oUsuario.Nombre = oUsuario.Nombre;
                    _oUsuario.Apellido = oUsuario.Apellido;
                    _oUsuario.Usuario = oUsuario.Usuario;
                    _oUsuario.Email = oUsuario.Email;
                    _oUsuario.Pwd = oUsuario.Pwd;
                    _oUsuario.Activo = oUsuario.Activo;
                    _oUsuario.FechaAlta = oUsuario.FechaAlta;

                    context.SaveChanges();

                    return oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Usuarios
        /// </summary>
        public void deleteUsuario(int IDUsuario)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.IDUsuario == IDUsuario);
                    context.Usuarios.Remove(_oUsuario);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Usuarios
        /// </summary>
        public List<Usuarios> getUsuarios()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _listUsuarios = context.Usuarios.ToList();

                    return _listUsuarios;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Usuario
        /// </summary>
        public Usuarios getUsuario(string Usuario, string Pwd)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.Usuario == Usuario && u.Pwd == Pwd && u.Activo == true);

                    return _oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Usuario
        /// </summary>
        public Usuarios getUsuario(int IDUsuario)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.IDUsuario == IDUsuario);

                    return _oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Usuario
        /// </summary>
        public Usuarios getUsuario(string Email)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.Email == Email);

                    return _oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Usuario
        /// </summary>
        public Usuarios getUsuarioPorUsuario(string Usuario)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oUsuario = context.Usuarios.FirstOrDefault(u => u.Usuario == Usuario);

                    return _oUsuario;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
