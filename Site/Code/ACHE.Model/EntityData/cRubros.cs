﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cRubros
    {
        public List<Rubros> getRubros()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _list = context.Rubros.Where(x => !x.IDRubroPadre.HasValue).OrderBy(x => x.Nombre).ToList();

                    return _list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
