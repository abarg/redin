﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cComercio {

        public List<Comercios> getComercios() {
            try {
                using (var context = new ACHEEntities()) {
                    var _list = context.Comercios.OrderBy(x => x.NombreFantasia).ToList();

                    return _list;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public DataTable getReportComercios(string Tipo, DateTime FechaDesde, DateTime FechaHasta, string SDS, string Nombre, int CantOper) {
            try {
                DataTable dt = new DataTable();

                SqlParameter p_Tipo = new SqlParameter("@Tipo", Tipo);
                SqlParameter p_FechaDesde = new SqlParameter("@FechaDesdeTrans", FechaDesde.ToString("yyyy/MM/dd"));
                SqlParameter p_FechaHasta = new SqlParameter("@FechaHastaTrans", FechaHasta.ToString("yyyy/MM/dd"));
                SqlParameter p_SDS = new SqlParameter("@SDS", SDS);
                SqlParameter p_Nombre = new SqlParameter("@NombreEst", Nombre);
                SqlParameter p_CantOper = new SqlParameter("@CantOperaciones", CantOper);

                SqlParameter[] parametros = new SqlParameter[6];
                parametros[0] = p_Tipo;
                parametros[1] = p_FechaDesde;
                parametros[2] = p_FechaHasta;
                parametros[3] = p_SDS;
                parametros[4] = p_Nombre;
                parametros[5] = p_CantOper;

                SqlDataReader reader = SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "GetReportComercioss", parametros);
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public List<Terminales> getComerciosForVisa() {
            try {
                using (var context = new ACHEEntities()) {
                    var _listComercios = context.Terminales.Include("Comercios").Include("Domicilios").Include("Domicilios.Provincias").Include("Domicilios.Ciudades")
                        .Where(x => x.POSTipo.ToUpper() != "WEB" && x.POSSistema != "SOFT" && x.POSSistema != "POS").ToList();
                    return _listComercios;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool deleteComercio(int IDComercio) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Comercios.FirstOrDefault(s => s.IDComercio == IDComercio);
                    _oComercio.Activo = false;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public Comercios getComercio(int IDComercio) {
            try {
                using (var context = new ACHEEntities()) {
                    var entity = context.Comercios.Include("Domicilios").Include("DomiciliosFiscal").Include("Contactos").FirstOrDefault(c => c.IDComercio == IDComercio);
                    return entity;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public Comercios getComercio(string terminal, string establecimiento) {
            try {
                using (var context = new ACHEEntities()) {
                    var aux = context.Terminales.Include("Comercios").
                        FirstOrDefault(c => c.NumEst == establecimiento && c.POSTerminal == terminal);
                    var entity = context.Comercios.Include("Domicilios").Include("DomiciliosFiscal").Include("Contactos").FirstOrDefault(c => c.IDComercio == aux.IDComercio);
                    return entity;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public List<ZonasViewModel> getZonas(int idProvincia) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var zonas = dbContext.Zonas.Where(x => x.IDProvincia == idProvincia)
                        .Select(x => new ZonasViewModel() { 
                            IDZona = x.IDZona,
                            Nombre = x.Nombre,
                            IDProvincia = x.IDProvincia,
                            Provincia = x.Provincias.Nombre,
                        }).ToList();
                    return zonas;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioSDS(string SDS) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Comercios.FirstOrDefault(c => c.SDS == SDS);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioNroDoc(string NroDoc) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Comercios.FirstOrDefault(c => c.NroDocumento == NroDoc);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioRazonSocial(string RazonSocial) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Comercios.FirstOrDefault(c => c.RazonSocial == RazonSocial);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioNroEst(string NroEst) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Terminales.FirstOrDefault(c => c.NumEst == NroEst);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioPOSTerminal(string POSTerminal) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Terminales.FirstOrDefault(c => c.POSTerminal == POSTerminal);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public bool existeComercioPOSEstablecimiento(string POSEstablecimiento) {
            try {
                using (var context = new ACHEEntities()) {
                    var _oComercio = context.Terminales.FirstOrDefault(c => c.POSEstablecimiento == POSEstablecimiento);
                    return (_oComercio != null);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
