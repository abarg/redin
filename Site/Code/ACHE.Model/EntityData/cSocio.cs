﻿using System;
using System.Linq;

namespace ACHE.Model.EntityData
{
    public class cSocio
    {
        /*
        public void limpiarSociosTmp()
        {
            try
            {
                SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, System.Data.CommandType.Text, "DELETE FROM SociosTmp");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void insertarSociosTmp(DataTable dt)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString))
                {
                    sqlConnection.Open();
                    using (var bulkCopy = new SqlBulkCopy(sqlConnection.ConnectionString))
                    {
                        bulkCopy.DestinationTableName = " [dbo].[SociosTmp]";

                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        //Conversión de Tipo de datos datetime, etc.
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["FechaNacimiento"] != System.DBNull.Value)
                            {
                                DateTime FechaNacimiento = Convert.ToDateTime(dr["FechaNacimiento"].ToString());
                                dr["FechaNacimiento"] = FechaNacimiento.ToShortDateString();
                            }
                            if (dr["FechaAlta"] != System.DBNull.Value)
                            {
                                DateTime FechaAlta = Convert.ToDateTime(dr["FechaAlta"].ToString());
                                dr["FechaAlta"] = FechaAlta.ToShortDateString();
                            }
                            if (dr["Fecha"] != System.DBNull.Value)
                            {
                                DateTime Fecha = Convert.ToDateTime(dr["Fecha"].ToString());
                                dr["Fecha"] = Fecha.ToShortDateString();
                            }
                            if (dr["FechaEmision"] != System.DBNull.Value)
                            {
                                DateTime FechaEmision = Convert.ToDateTime(dr["FechaEmision"].ToString());
                                dr["FechaEmision"] = FechaEmision.ToShortDateString();
                            }
                            if (dr["FechaVencimiento"] != System.DBNull.Value)
                            {
                                DateTime FechaVencimiento = Convert.ToDateTime(dr["FechaVencimiento"].ToString());
                                dr["FechaVencimiento"] = FechaVencimiento.ToShortDateString();
                            }
                        }
                        bulkCopy.WriteToServer(dt);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void actualizarSociosTmp()
        {
            try
            {
                SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["ACHEConnectionString"].ConnectionString, CommandType.StoredProcedure, "tmpActualizarSocios");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        /*
        public List<Socios> getSocios()
        {
            cDomicilio cDomicilio;
            cTarjeta cTarjeta;
            try
            {
                cDomicilio = new cDomicilio();
                cTarjeta = new cTarjeta();
                using (var context = new ACHEEntities())
                {
                    var _listSocios = (from s in context.Socios

                                       select new Socios
                                       {
                                           IDSocio = s.IDSocio,
                                           Nombre = s.Nombre,
                                           Apellido = s.Apellido,
                                           Email = s.Email,
                                           FechaNacimiento = s.FechaNacimiento ?? DateTime.MinValue,
                                           Sexo = s.Sexo,
                                           TipoDocumento = s.TipoDocumento,
                                           NroDocumento = s.NroDocumento,
                                           Telefono = s.Telefono,
                                           Celular = s.Celular,
                                           EmpresaCelular = s.EmpresaCelular,
                                           FechaAlta = s.FechaAlta ?? DateTime.MinValue,
                                           IDDomicilio = s.IDDomicilio ?? 0,
                                           Observaciones = s.Observaciones,
                                           TipoReg = s.TipoReg,
                                           TipoMov = s.TipoMov,
                                           ImpDisponible = s.ImpDisponible,
                                           Fecha = s.Fecha ?? DateTime.MinValue,
                                           AffinityBenef = s.AffinityBenef,
                                           NroCuenta = s.NroCuenta,
                                           Activo = s.Activo ?? true
                                       }).ToList();

                    foreach (var oSoc in _listSocios)
                    {
                        oSoc.oDomicilio = cDomicilio.getDomicilio(oSoc.IDDomicilio);
                    }

                    return _listSocios;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        /*
        public List<Socios> getSociosConTarjetas(bool loadDomicilio)
        {
            cDomicilio cDomicilio;
            cTarjeta cTarjeta;

            List<Socios> _listSocios;
            try
            {
                cDomicilio = new cDomicilio();
                cTarjeta = new cTarjeta();
                using (var context = new ACHEEntities())
                {
                    _listSocios = (from s in context.Socios

                                   select new Socios
                                   {
                                       IDSocio = s.IDSocio,
                                       Nombre = s.Nombre,
                                       Apellido = s.Apellido,
                                       Email = s.Email,
                                       FechaNacimiento = s.FechaNacimiento ?? DateTime.MinValue,
                                       Sexo = s.Sexo,
                                       TipoDocumento = s.TipoDocumento,
                                       NroDocumento = s.NroDocumento,
                                       Telefono = s.Telefono,
                                       Celular = s.Celular,
                                       EmpresaCelular = s.EmpresaCelular,
                                       FechaAlta = s.FechaAlta ?? DateTime.MinValue,
                                       IDDomicilio = s.IDDomicilio ?? 0,
                                       Observaciones = s.Observaciones,
                                       TipoReg = s.TipoReg,
                                       TipoMov = s.TipoMov,
                                       ImpDisponible = s.ImpDisponible,
                                       Fecha = s.Fecha ?? DateTime.MinValue,
                                       AffinityBenef = s.AffinityBenef,
                                       NroCuenta = s.NroCuenta,
                                       Activo = s.Activo ?? true
                                   }).ToList();
                }

                foreach (var oSoc in _listSocios)
                {
                    if (loadDomicilio)
                        oSoc.oDomicilio = cDomicilio.getDomicilio(oSoc.IDDomicilio);
                    oSoc.ListTarjetas = cTarjeta.getTarjetas(oSoc.IDSocio);
                }

                return _listSocios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        public Socios getSocio(int IDSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oSocio = context.Socios.Include("Domicilios").FirstOrDefault(s => s.IDSocio == IDSocio);

                    return _oSocio;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNumTarjeta(int IDSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oSocio = context.Tarjetas.FirstOrDefault(s => s.IDSocio == IDSocio);
                    return (_oSocio != null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNroCuenta(string NroCuenta)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oSocio = context.Socios.FirstOrDefault(s => s.NroCuenta == NroCuenta);
                    return (_oSocio != null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool existeSocioNroDoc(string NroDoc)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oSocio = context.Socios.FirstOrDefault(s => s.NroDocumento == NroDoc);
                    return (_oSocio != null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Socios insertSocio(Socios oSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    context.Socios.Add(oSocio);
                    context.SaveChanges();
                    return oSocio;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*public Socios updateSocio(Socios oSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    //var entity = context.Socios.FirstOrDefault(s => s.IDSocio == oSocio.IDSocio);
                    //if (entity != null)
                    //{
                        /*entity.IDSocio = oSocio.IDSocio;
                        entity.Nombre = oSocio.Nombre;
                        entity.Apellido = oSocio.Apellido;
                        entity.Email = oSocio.Email;
                        entity.FechaNacimiento = oSocio.FechaNacimiento;
                        entity.Sexo = oSocio.Sexo;
                        entity.TipoDocumento = oSocio.TipoDocumento;
                        entity.NroDocumento = oSocio.NroDocumento;
                        entity.Telefono = oSocio.Telefono;
                        entity.Celular = oSocio.Celular;
                        entity.EmpresaCelular = oSocio.EmpresaCelular;
                        entity.FechaAlta = oSocio.FechaAlta;
                        entity.IDDomicilio = oSocio.IDDomicilio;
                        entity.Observaciones = oSocio.Observaciones;
                        entity.TipoReg = oSocio.TipoReg;
                        entity.TipoMov = oSocio.TipoMov;
                        entity.ImpDisponible = oSocio.ImpDisponible;
                        entity.Fecha = oSocio.Fecha;
                        entity.AffinityBenef = oSocio.AffinityBenef;
                        entity.NroCuenta = oSocio.NroCuenta;
                        entity.Activo = oSocio.Activo;*

                        context.Socios.Attach(oSocio);
                        context.Socios.ApplyCurrentValues(oSocio);    
                        context.ObjectStateManager.ChangeObjectState(oSocio, EntityState.Modified);
                        
                        context.SaveChanges();

                        //context.SaveChanges();
                        return oSocio;
                    //}
                    //else
                    //    throw new Exception("El socio no existe");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        public bool deleteSocio(int IDSocio)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _oSocio = context.Socios.FirstOrDefault(s => s.IDSocio == IDSocio);
                    _oSocio.Activo = false;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
