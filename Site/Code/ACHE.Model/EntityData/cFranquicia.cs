﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cFranquicia
    {
        public List<Franquicias> getFranquicias()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _list = context.Franquicias.OrderBy(x => x.NombreFantasia).ToList();

                    return _list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
    }
}
