﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
//using ACHE.Extensions;
using System.Data;

namespace ACHE.Model.EntityData
{
    public class cMarca
    {
        public List<Marcas> getMarcas()
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _list = context.Marcas.OrderBy(x => x.Nombre).ToList();

                    return _list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Marcas> getMarcasByFranquicia(int idFranquicia)
        {
            try
            {
                using (var context = new ACHEEntities())
                {
                    var _list = context.Marcas.Where(x => x.IDFranquicia == idFranquicia).OrderBy(x => x.Nombre).ToList();

                    return _list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
