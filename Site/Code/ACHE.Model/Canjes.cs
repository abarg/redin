//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Canjes
    {
        public Canjes()
        {
            this.CanjesHistorial = new HashSet<CanjesHistorial>();
        }
    
        public int IDCanje { get; set; }
        public int IDTarjeta { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public int Cantidad { get; set; }
        public string Observaciones { get; set; }
        public System.DateTime FechaUltEstado { get; set; }
        public int IDProducto { get; set; }
        public Nullable<int> IDMarca { get; set; }
        public int IDEstado { get; set; }
        public string MotivoRechazo { get; set; }
        public string ObservacionesRechazo { get; set; }
        public Nullable<int> IDTransaccion { get; set; }
    
        public virtual Productos Productos { get; set; }
        public virtual Tarjetas Tarjetas { get; set; }
        public virtual ICollection<CanjesHistorial> CanjesHistorial { get; set; }
        public virtual EstadosCanjes EstadosCanjes { get; set; }
        public virtual Transacciones Transacciones { get; set; }
        public virtual Marcas Marcas { get; set; }
    }
}
