//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pagos
    {
        public int IDPago { get; set; }
        public Nullable<int> IDComercio { get; set; }
        public System.DateTime Fecha { get; set; }
        public decimal Importe { get; set; }
        public string FormaDePago { get; set; }
        public string NroComprobante { get; set; }
        public string Observaciones { get; set; }
        public string NroFactura { get; set; }
        public decimal RetGanancias { get; set; }
        public decimal IIBB { get; set; }
        public decimal Redondeo { get; set; }
        public decimal SUSS { get; set; }
        public decimal Otros { get; set; }
        public Nullable<int> IDProveedor { get; set; }
        public int EnContabilium { get; set; }
    
        public virtual Proveedores Proveedores { get; set; }
        public virtual Comercios Comercios { get; set; }
    }
}
