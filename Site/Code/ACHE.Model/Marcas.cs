//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Marcas
    {
        public Marcas()
        {
            this.Alertas = new HashSet<Alertas>();
            this.AlertasTarjetas = new HashSet<AlertasTarjetas>();
            this.Beneficios = new HashSet<Beneficios>();
            this.Beneficios1 = new HashSet<Beneficios>();
            this.Canjes = new HashSet<Canjes>();
            this.Comercios = new HashSet<Comercios>();
            this.ComerciosVieja = new HashSet<ComerciosVieja>();
            this.EstadosCanjes = new HashSet<EstadosCanjes>();
            this.FamiliaProductos = new HashSet<FamiliaProductos>();
            this.MarcasAsociadas = new HashSet<MarcasAsociadas>();
            this.MarcasMenu = new HashSet<MarcasMenu>();
            this.Motivos = new HashSet<Motivos>();
            this.PremiosTmp = new HashSet<PremiosTmp>();
            this.Promociones = new HashSet<Promociones>();
            this.RangoAffinityNumeroPorMarca = new HashSet<RangoAffinityNumeroPorMarca>();
            this.SMSEnvios = new HashSet<SMSEnvios>();
            this.Sorteos = new HashSet<Sorteos>();
            this.TarjetasTmp = new HashSet<TarjetasTmp>();
            this.UsuariosMarcas = new HashSet<UsuariosMarcas>();
            this.Premios = new HashSet<Premios>();
            this.Productos = new HashSet<Productos>();
            this.SMS = new HashSet<SMS>();
            this.Tarjetas = new HashSet<Tarjetas>();
            this.LandingMarcasPage = new HashSet<LandingMarcasPage>();
            this.LandingAdmin = new HashSet<LandingAdmin>();
        }
    
        public int IDMarca { get; set; }
        public string Nombre { get; set; }
        public string Prefijo { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public string Affinity { get; set; }
        public decimal POSArancel { get; set; }
        public string Color { get; set; }
        public string Logo { get; set; }
        public bool MostrarSoloTarjetasPropias { get; set; }
        public bool MostrarSoloPOSPropios { get; set; }
        public Nullable<int> IDFranquicia { get; set; }
        public string TipoCatalogo { get; set; }
        public bool HabilitarPOSWeb { get; set; }
        public string Codigo { get; set; }
        public bool HabilitarGiftcard { get; set; }
        public bool HabilitarCuponIN { get; set; }
        public bool HabilitarSMS { get; set; }
        public decimal CostoSMS { get; set; }
        public bool EnvioMsjBienvenida { get; set; }
        public string MsjBienvenida { get; set; }
        public bool EnvioMsjCumpleanios { get; set; }
        public string MsjCumpleanios { get; set; }
        public Nullable<int> IDComercioFacturanteSMS { get; set; }
        public bool EnvioEmailRegistroSocio { get; set; }
        public bool EnvioEmailCumpleanios { get; set; }
        public bool EnvioEmailRegistroComercio { get; set; }
        public string EmailRegistroSocio { get; set; }
        public string EmailCumpleanios { get; set; }
        public string EmailRegistroComercio { get; set; }
        public string EmailAlertas { get; set; }
        public string CelularAlertas { get; set; }
        public string EmpresaCelularAlertas { get; set; }
        public string RazonSocial { get; set; }
        public string CondicionIva { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public Nullable<int> IDDomicilio { get; set; }
        public Nullable<decimal> CostoTransaccional { get; set; }
        public Nullable<decimal> CostoSeguro { get; set; }
        public Nullable<decimal> CostoPlusin { get; set; }
        public Nullable<decimal> CostoSMS2 { get; set; }
        public bool MostrarProductos { get; set; }
        public Nullable<System.DateTime> fechaCaducidad { get; set; }
        public Nullable<System.DateTime> fechaTopeCanje { get; set; }
        public string TipoTarjeta { get; set; }
        public bool POSMostrarFormaPago { get; set; }
        public bool POSMostrarNumeroTicket { get; set; }
        public string POSFooter1 { get; set; }
        public string POSFooter2 { get; set; }
        public string POSFooter3 { get; set; }
        public string POSFooter4 { get; set; }
        public bool POSMostrarMenuFidelidad { get; set; }
        public bool POSMostrarMenuGift { get; set; }
        public bool POSMostrarChargeGift { get; set; }
        public bool POSMostrarLOGO { get; set; }
        public string PuntoDeVenta { get; set; }
        public string TipoComprobante { get; set; }
        public string NroComprobante { get; set; }
        public bool SeFactura { get; set; }
        public string CuitEmisor { get; set; }
        public string Moneda { get; set; }
        public string colorApp { get; set; }
        public string rutaLogoApp { get; set; }
        public string rutaLogoTicket { get; set; }
        public Nullable<bool> notiShowSMS { get; set; }
        public Nullable<bool> notiShowEmail { get; set; }
        public Nullable<int> DefaultViewID { get; set; }
        public Nullable<bool> showPoll { get; set; }
        public string urlGestion { get; set; }
        public Nullable<decimal> CostoEmail { get; set; }
    
        public virtual ICollection<Alertas> Alertas { get; set; }
        public virtual ICollection<AlertasTarjetas> AlertasTarjetas { get; set; }
        public virtual ICollection<Beneficios> Beneficios { get; set; }
        public virtual ICollection<Beneficios> Beneficios1 { get; set; }
        public virtual ICollection<Canjes> Canjes { get; set; }
        public virtual ICollection<Comercios> Comercios { get; set; }
        public virtual ICollection<ComerciosVieja> ComerciosVieja { get; set; }
        public virtual ComerciosVieja ComerciosVieja1 { get; set; }
        public virtual Domicilios Domicilios { get; set; }
        public virtual ICollection<EstadosCanjes> EstadosCanjes { get; set; }
        public virtual ICollection<FamiliaProductos> FamiliaProductos { get; set; }
        public virtual Franquicias Franquicias { get; set; }
        public virtual ICollection<MarcasAsociadas> MarcasAsociadas { get; set; }
        public virtual ICollection<MarcasMenu> MarcasMenu { get; set; }
        public virtual ICollection<Motivos> Motivos { get; set; }
        public virtual ICollection<PremiosTmp> PremiosTmp { get; set; }
        public virtual ICollection<Promociones> Promociones { get; set; }
        public virtual ICollection<RangoAffinityNumeroPorMarca> RangoAffinityNumeroPorMarca { get; set; }
        public virtual ICollection<SMSEnvios> SMSEnvios { get; set; }
        public virtual ICollection<Sorteos> Sorteos { get; set; }
        public virtual ICollection<TarjetasTmp> TarjetasTmp { get; set; }
        public virtual ICollection<UsuariosMarcas> UsuariosMarcas { get; set; }
        public virtual ICollection<Premios> Premios { get; set; }
        public virtual ICollection<Productos> Productos { get; set; }
        public virtual ICollection<SMS> SMS { get; set; }
        public virtual ICollection<Tarjetas> Tarjetas { get; set; }
        public virtual TerminalDefaultViewOptions TerminalDefaultViewOptions { get; set; }
        public virtual ICollection<LandingMarcasPage> LandingMarcasPage { get; set; }
        public virtual ICollection<LandingAdmin> LandingAdmin { get; set; }
    }
}
