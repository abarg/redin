//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class PuntosDeVenta
    {
        public int IDPuntoVenta { get; set; }
        public int IDComercio { get; set; }
        public int Numero { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public bool EsDefault { get; set; }
    
        public virtual Comercios Comercios { get; set; }
        public virtual ComerciosVieja ComerciosVieja { get; set; }
    }
}
