//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class LandingCampos
    {
        public LandingCampos()
        {
            this.LandingMarcasPage = new HashSet<LandingMarcasPage>();
        }
    
        public int Id { get; set; }
        public Nullable<int> IdSeccion { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string PlaceHolder { get; set; }
        public string Etiqueta { get; set; }
        public string ClassCss { get; set; }
        public Nullable<int> Orden { get; set; }
        public bool Activo { get; set; }
    
        public virtual ICollection<LandingMarcasPage> LandingMarcasPage { get; set; }
    }
}
