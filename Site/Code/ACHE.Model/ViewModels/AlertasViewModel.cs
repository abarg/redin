﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for AlertasViewModel
    /// </summary>
    public class AlertasViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Activa { get; set; }
        public string Prioridad { get; set; }
        public string Restringido { get; set; }
        public string Tipo { get; set; }
    }
}