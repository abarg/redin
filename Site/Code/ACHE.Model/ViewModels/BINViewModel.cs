﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHE.Model {

    public class BINViewModel {
        public int IDBIN { get; set; }
        public string Nombre { get; set; }
        public int Numero { get; set; }
    }
}