﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class ZonasViewModel {
        public int IDZona { get; set; }
        public int IDProvincia { get; set; }
        public int IDCiudad { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Nombre { get; set; }
    }
}
