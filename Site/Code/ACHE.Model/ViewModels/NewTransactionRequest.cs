﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewTransactionRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string txMethod { get; set; }
        public string cardNumber { get; set; }
        public string dniNumber { get; set; }
        public decimal totalPrice { get; set; }
        public string ticket { get; set; }
        public string paymentMethod { get; set; }
        public string observation { get; set; }
        public List<Product> products { get; set; }
    }
}
