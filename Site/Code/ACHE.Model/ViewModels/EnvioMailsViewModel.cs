﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{

    /// <summary>
    /// Summary description for EnvioMailsViewModel
    /// </summary>
    public class EnvioMailsViewModel
    {
        public int CantErrors { get; set; }
        public int CantEnvios { get; set; }
        public string Mensaje { get; set; }
        //public string Detalle { get; set; }
    }
}