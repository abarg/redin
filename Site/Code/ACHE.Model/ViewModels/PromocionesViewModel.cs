﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class PromocionesViewModel
    {
        public int IDPromocion { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }

        public int descuento { get; set; }
        public int mulPuntos { get; set; }
        public string mulPuntosVip { get; set; }
        public string descuentoVip { get; set; }

        public decimal puntos { get; set; }
        public decimal arancel { get; set; }
        public string puntosVip { get; set; }
        public string arancelVip { get; set; }
     
    }
}
