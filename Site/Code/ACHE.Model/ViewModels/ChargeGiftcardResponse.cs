﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class ChargeGiftcardResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public int IdTransaction { get; set; }
        public int Points { get; set; }
        public decimal Credits { get; set; }
    }
}
