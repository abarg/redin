﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// View utilizada en el Tab LandingPage de Marcas
    /// </summary>
    public class LandingPageView
    {
        public int IDLandingControl { get; set; }
        public string Campo { get; set; }
        public string Seccion { get; set; }
        public string Habilitado { get; set; }
    }
}