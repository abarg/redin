﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for PremiosViewModel
    /// </summary>
    public class PremiosViewModel
    {
        public int IDPremio { get; set; }
        public int? IDRubro { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public int StockActual { get; set; }
        public int StockMinimo { get; set; }
        public int ValorPuntos { get; set; }
        public int ValorPesos { get; set; }
        public DateTime? FechaVigenciaDesde { get; set; }
        public DateTime? FechaVigenciaHasta { get; set; }
    }

    /// <summary>
    /// Summary description for SeguimientoCanjesViewModel
    /// </summary>
    public class SeguimientoCanjesViewModel
    {
        public int IDCanje { get; set; }
        public string Estado { get; set; }
        public string Iestado { get; set; }
        public int IDEstado { get; set; }
        public DateTime Fecha { get; set; }
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public string NroDocumento { get; set; }
        public string Socio { get; set; }
        public string Tarjeta { get; set; }
        public string Premio { get; set; }

        public int IDFamilia { get; set; }
        public string Familia { get; set; }
    }
}