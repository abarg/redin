﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class KeywordsViewModel
    {
        public int IDKeyword { get; set; }
        public string Codigo { get; set; }
        public int IDComercio { get; set; }
        public string Comercio { get; set; }
        public int IDMarca { get; set; }
        public int Stock { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Activo { get; set; }
        public string Estado { get; set; }
    }

    public class KeywordsConsumosViewModel
    {
        public int IDConsumo { get; set; }
        public int? IDKeyword { get; set; }
        public string Keyword { get; set; }
        public string PhoneNumber { get; set; }
        public string Valido { get; set; }
        public string TarjetaUsada { get; set; }
        public DateTime Fecha { get; set; }
        public string Observaciones { get; set; }
    }
}
