﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using System.Collections;


namespace ACHE.Model
{
    [FixedLengthRecord()]
    public class TransaccionTotalFH
    {
        [FieldFixedLength(5)]
        public string EMPRESA;

        [FieldFixedLength(6)]
        public string FPRES;

        [FieldFixedLength(2)]
        public string TIPOREG;

        [FieldFixedLength(10)]
        public string NUMCOM;

        [FieldFixedLength(10)]
        public string NUMEST;

        [FieldFixedLength(4)]
        public string CODOP;

        [FieldFixedLength(1)]
        public string TIPOAPLIC;

        [FieldFixedLength(38)]
        public string LIBRE1;

        [FieldFixedLength(6)]
        public string FPAGO;

        [FieldFixedLength(8)]
        public string LIBRE2;

        [FieldFixedLength(10)]
        public string IMPORTE;

        [FieldFixedLength(1)]
        public string SIGNO;

        [FieldFixedLength(25)]
        public string LIBRE3;

        [FieldFixedLength(1)]
        public string MCA_PEX;

        [FieldFixedLength(12)]
        public string LIBRE4;

        [FieldFixedLength(1)]
        public string ASTER;
    }
}
