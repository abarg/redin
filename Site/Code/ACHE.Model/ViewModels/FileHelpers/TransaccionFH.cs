﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using System.Collections;


namespace ACHE.Model
{
    [FixedLengthRecord()]
    public class TransaccionFH
    {
        [FieldFixedLength(5)]
        public string EMPRESA;

        [FieldFixedLength(6)]
        public string FPRES;

        [FieldFixedLength(2)]
        public string TIPOREG;

        [FieldFixedLength(10)]
        public string NUMCOM;

        [FieldFixedLength(10)]
        public string NUMEST;

        [FieldFixedLength(4)]
        public string CODOP;

        [FieldFixedLength(1)]
        public string TIPOAPLIC;

        [FieldFixedLength(4)]
        public string LOTE;

        [FieldFixedLength(3)]
        public string CODBCO;

        [FieldFixedLength(3)]
        public string CODCASA;

        [FieldFixedLength(3)]
        public string BCOEST;

        [FieldFixedLength(3)]
        public string CASAEST;

        [FieldFixedLength(16)]
        public string NUMTAR;

        [FieldFixedLength(6)]
        public string FORIG_COMPRA;

        [FieldFixedLength(6)]
        public string FPAG;

        [FieldFixedLength(8)]
        public string NUMCOMP;

        [FieldFixedLength(9)]
        public string IMPORTE;

        [FieldFixedLength(1)]
        public string SIGNO1;

        [FieldFixedLength(5)]
        public string NUMAUT;

        [FieldFixedLength(2)]
        public string NUMCUOT;

        [FieldFixedLength(2)]
        public string PLANCUOT;

        [FieldFixedLength(1)]
        public string REC_ACEP;

        [FieldFixedLength(2)]
        public string RECH_PRINC;

        [FieldFixedLength(2)]
        public string RECH_SECUN;

        [FieldFixedLength(11)]
        public string IMP_PLAN;

        [FieldFixedLength(1)]
        public string SIGNO2;

        [FieldFixedLength(1)]
        public string MCA_PEX;

        [FieldFixedLength(8)]
        public string NROLIQ;

        [FieldFixedLength(1)]
        public string CCO_ORIGEN;

        [FieldFixedLength(2)]
        public string CCO_MOTIVO;

        [FieldFixedLength(1)]
        public string MONEDA;

        [FieldFixedLength(1)]
        public string ASTER;

        [FieldFixedLength(3)]
        public string PROMO_BONIF_USU;

        [FieldFixedLength(3)]
        public string PROMO_BONIF_EST;

        [FieldFixedLength(1)]
        public string ID_PROMO;

        [FieldFixedLength(7)]
        public string DTO_PROMO;

        [FieldFixedLength(1)]
        public string SIGNO_DTO_PROMO;

        [FieldFixedLength(1)]
        public string ID_IVA_CF;

        [FieldFixedLength(10)]
        public string DEALER;

        [FieldFixedLength(11)]
        public string CUIT_EST;

        [FieldFixedLength(6)]
        public string FPAGO_AJU_LQE;

        [FieldFixedLength(4)]
        public string COD_MOTIVO_AJU_LQE;

        [FieldFixedLength(9)]
        public string RESERVADO1;

        [FieldFixedLength(12)]
        public string RESERVADO2;

        [FieldFixedLength(4)]
        public string PORCDTO_ARANCEL;

        [FieldFixedLength(7)]
        public string ARANCEL;

        [FieldFixedLength(1)]
        public string SIGNO_ARANCEL;

        [FieldFixedLength(5)]
        public string TNA;

        [FieldFixedLength(7)]
        public string COSTO_FIN;

        [FieldFixedLength(1)]
        public string SIGNO_CF;

        [FieldFixedLength(6)]
        public string RESERVADO3;

        [FieldFixedLength(1)]
        public string RESERVADO4;

        [FieldFixedLength(1)]
        public string TIPODEPLAN;

        [FieldFixedLength(8)]
        public string LIBRE;

        [FieldFixedLength(1)]
        public string ID_FIN_REGISTRO;
    }
}
