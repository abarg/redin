﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {

    [DelimitedRecord(";")]
    public class TarjetaFH {

        public int IDTarjeta;

        public string Socio;

        //public int IDFranquicia;

        [FieldTrim(TrimMode.Both)]
        public string Franquicia;

        //public int IDMarca;

        [FieldTrim(TrimMode.Both)]
        public string Marca;

        [FieldTrim(TrimMode.Both)]
        public string Numero;

        public string Estado;

        public string FechaAlta;

        public string FechaEmision;

        public string FechaVencimiento;

        public string FechaBaja;

        public int Puntos;

        public decimal Credito;

        public decimal Giftcard;

        public decimal POS;

        public decimal Total;
    }

    [DelimitedRecord(";")]
    public class TarjetaFHSimple
    {

        public int IDTarjeta;

        [FieldTrim(TrimMode.Both)]
        public string Franquicia;

        [FieldTrim(TrimMode.Both)]
        public string Marca;

        [FieldTrim(TrimMode.Both)]
        public string Numero;

        //public string Estado;

        public string FechaAlta;

        public string FechaEmision;

        public string FechaVencimiento;

        public string FechaBaja;

        public int Puntos;

        public decimal Credito;

        public decimal Giftcard;

        public decimal POS;

        public decimal Total;
    }

    [DelimitedRecord(";")]
    public class TarjetaFHExport
    {

        public int IDTarjeta;

        [FieldTrim(TrimMode.Both)]
        public string Franquicia;

        [FieldTrim(TrimMode.Both)]
        public string Marca;

        public string Numero;

        //public string Estado;

        public string FechaAlta;

        public string FechaEmision;

        public string FechaVencimiento;

        public string FechaBaja;

        public int Puntos;

        public decimal Credito;

        public decimal Giftcard;

        public decimal POS;

        public decimal Total;
        public string Tipo;
    }
}