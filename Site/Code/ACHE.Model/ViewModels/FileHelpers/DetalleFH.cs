﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using System.Collections;


namespace ACHE.Model
{
    [FixedLengthRecord()]
    public class DetalleFH
    {
        [FieldFixedLength(4)]
        public string TipoRegistro;

        [FieldFixedLength(22)]
        [FieldTrim(TrimMode.Right)]
        public string IdCliente;

        [FieldFixedLength(8)]
        public string Bloque1;

        [FieldFixedLength(1)]
        public string Digito1;

        [FieldFixedLength(16)]
        public string Bloque2;

        [FieldFixedLength(1)]
        public string Digito2;

        [FieldFixedLength(15)]
        [FieldTrim(TrimMode.Right)]
        public string ReferenciaUnivoca;

        [FieldFixedLength(8)]
        public string FechaVto1;

        [FieldFixedLength(14)]
        public string ImporteVto1;

        [FieldFixedLength(8)]
        public string FechaVto2;

        [FieldFixedLength(14)]
        public string ImporteVto2;

        [FieldFixedLength(8)]
        public string FechaVto3;

        [FieldFixedLength(14)]
        public string ImporteVto3;

        [FieldFixedLength(1)]
        public int MonedaFactura;

        [FieldFixedLength(3)]
        public string MotivoRechazo;

        [FieldFixedLength(4)]
        public string TipoDocumento;

        [FieldFixedLength(11)]
        public string NroDocumento;

        [FieldFixedLength(22)]
        public string NuevoID;

        [FieldFixedLength(26)]
        public string NuevaCBU;

        [FieldFixedLength(14)]
        public string ImporteMinimo;

        [FieldFixedLength(8)]
        public string FechaProxVto;

        [FieldFixedLength(22)]
        public string IdCuentaAnterior;

        [FieldFixedLength(40)]
        public string MensajeATM;

        [FieldFixedLength(10)]
        public string ConceptoFactura;

        [FieldFixedLength(8)]
        public string FechaCobro;

        [FieldFixedLength(14)]
        public string ImporteCobrado;

        [FieldFixedLength(8)]
        public string FechaAcreditamiento;

        [FieldFixedLength(26)]
        public string Libre;
    }
}
