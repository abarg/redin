﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class LoginComercioRequest {
        public string userName { get; set; }
        public string pwd { get; set; }
        public string terminal { get; set; }
    }
}
