﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class TicketsViewModel
    {
        public int IDTicket { get; set; }
        public string Area { get; set; }
        public int IDAreaTicket { get; set; }
        public string Franquicia { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Estado { get; set; }
        public string Fecha { get; set; }
        public string Descripcion { get; set; }
        public string Usuario { get; set; }
        public string Adjunto { get; set; }
        public string Asunto { get; set; }
        public string FechaCierre { get; set; }
        public string Prioridad { get; set; }
        public int IDUsuarioAlta { get; set; }
        public int IDFranquicia { get; set; } 
    }
}
