﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewMemberResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public int memberID { get; set; }
        public string nroTarjeta { get; set; }

    }
}
