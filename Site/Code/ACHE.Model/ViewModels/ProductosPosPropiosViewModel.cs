﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class ProductosPosPropiosViewModel {
        public int IDTransaccionDetalle { get; set; }
         public int IDTransaccion { get; set; }
         public int IDMarca { get; set; }
         public int IDFranquicia { get; set; }
         public string Nombre { get; set; }
         public int IDProducto { get; set; }
         public string NroTicket { get; set; }
         public string FamiliaProducto { get; set; }
         public int Cantidad { get; set; }
         public decimal Precio { get; set; }
         public string Tarjeta { get; set; }
    }
}
