﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for SociosViewModel
    /// </summary>
    [Serializable]
    public class SociosViewModel
    {
        public int IDSocio { get; set; }
        public string Tarjeta { get; set; }
        public string Marca { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Sexo { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string EmpresaCelular { get; set; }
        public string Observaciones { get; set; }
        public string NumeroTarjetaSube { get; set; }
        public string NumeroTarjetaMonedero { get; set; }
        public string NumeroTarjetaTransporte{ get; set; }
        public string PatenteCoche { get; set; }
        public string NroCuenta { get; set; }
        public string Numero { get; set; }
        public string Foto { get; set; }
        public string Domicilio { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int Puntos { get; set; }
        public decimal POS { get; set; }
        public decimal Credito { get; set; }
        public decimal Giftcard { get; set; }
        public decimal Total { get; set; }
        public decimal TotalAhorro { get; set; }
        public string EstadoTarjeta { get; set; }
        public int IDMarca { get; set; }
        public int IDFranquicia { get; set; }
        public string Franquicia { get; set; }
        public string Actividades { get; set; }
        public string Ciudad { get; set; }

    }
}