﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
   public class UsuariosDormidosViewModel {
       public int IDSocio { get; set; }
       public string Nombre { get; set; }
       public string Apellido { get; set; }
       public int CantTrans { get; set; }
       public int IDFranquicia { get; set; }
       public string Franquicia { get; set; }
       public int IDMarca { get; set; }
        public string Marca { get; set; }
        public string Sexo { get; set; }
        public Nullable<int> Edad { get; set; }
        public string Email { get; set; }
        public string EmpresaCelular { get; set; }
        public string Celular { get; set; }

    }
}
