﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for PagosViewModel
    /// </summary>
    public class PagosViewModel
    {
        public int ID { get; set; }
        public int IDEntidad { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Importe { get; set; }
        public string FormaDePago { get; set; }
        public string NroComprobante { get; set; }
        public string Observaciones { get; set; }
        public string CUIT { get; set; }
        public string EnContabilium { get; set; }
    }



}