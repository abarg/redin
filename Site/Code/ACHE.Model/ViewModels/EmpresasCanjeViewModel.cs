﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class EmpresasCanjeViewModel {
        public string Empresa { get; set; }
        public string Descripcion { get; set; }
        public string Link { get; set; }
        public string Logo { get; set; }
        public int CantidadTotal { get; set; }
        public int CantidadDisponibles { get; set; }
        public int CantidadNoDisponibles { get; set; }
        public decimal CostoInterno { get; set; }
    }
}
