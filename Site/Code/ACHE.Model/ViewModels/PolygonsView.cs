﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class PolygonsView
    {

          public int IDPolygon { get; set; }
          public int? IDPolygroup { get; set; }
          public string Nombre { get; set; }
          public string Paths { get; set; }
          public string StrokeColor { get; set; }
          public string StrokeOpacity { get; set; }
          public string StrokeWeight { get; set; }
          public string FillColor { get; set; }
          public string FillOpacity { get; set; }

    }
}
