﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for CCEmpresaViewModel
    /// </summary>
    public class CCEmpresaViewModel
    {
        //public int ID { get; set; }
        public string Comercio { get; set; }
        public string Detalle { get; set; }
        public string Fecha { get; set; }
        public decimal Importe { get; set; }
        public string Observaciones { get; set; }

    }
}