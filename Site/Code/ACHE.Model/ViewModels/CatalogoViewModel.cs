﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for CatalogoViewModel
    /// </summary>
    public class CatalogoViewModel
    {
        public int IDPremio { get; set; }
        public int? IDRubro { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public int StockActual { get; set; }
        public int ValorPuntos { get; set; }
        public int ValorPesos { get; set; }
        public string Imagen { get; set; }
        public string Rubro { get; set; }
        public string Familia { get; set; }
        public int IDFamilia { get; set; }
        public string SubFamilia { get; set; }
        public int IDSubFamilia { get; set; }
        public decimal Precio { get; set; }
        public string Nombre { get; set; }
        //public DateTime? FechaVigenciaDesde { get; set; }
        //public DateTime? FechaVigenciaHasta { get; set; }
    }

    public class ResultadosCatalogoViewModel
    {
        public IList<CatalogoViewModel> Items;
        public int Cantidad { get; set; }
    }
}