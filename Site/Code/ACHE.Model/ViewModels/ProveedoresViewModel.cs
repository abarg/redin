﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Summary description for ProveedoresViewModel
/// </summary>
public class ProveedoresViewModel
{
    public int ID { get; set; }
    public string NombreFantasia { get; set; }
    public string RazonSocial{ get; set; } 
    public string TipoDocumento { get; set; }
    public string NroDocumento { get; set; }
    public string Telefono { get; set; }
    public string Celular { get; set; }
    public string CondicionIva{ get; set; }
    public string Web { get; set; }
    public string Email { get; set; }
}

