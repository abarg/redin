﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for PagosViewModel
    /// </summary>
    public class PaymentGateawayTokensView
    {
        public int IDToken { get; set; }
        public string publicKey { get; set; }
        public string privateKey { get; set; }
        public string Pasarela { get; set; }

    }



}