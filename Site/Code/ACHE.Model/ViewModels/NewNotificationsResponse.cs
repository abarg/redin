﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewNotificationsResponse {
        public int AnswerCode { get; set; }
        public bool sentEmail { get; set; }
        public bool sentSMS { get; set; }
    }
}
