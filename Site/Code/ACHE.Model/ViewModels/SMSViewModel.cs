﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for SMSViewModel
    /// </summary>
    public class SMSViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Mensaje { get; set; }
        public string Estado { get; set; }
        public int Enviados { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaEnvio { get; set; }
        public string Tipo { get; set; }
    }

    /// <summary>
    /// Summary description for SMSAdminViewModel
    /// </summary>
    public class SMSAdminViewModel
    {
        public int ID { get; set; }
        public string Marca { get; set; }
        public string UsuarioAlta { get; set; }
        public string Nombre { get; set; }
        public string Mensaje { get; set; }
        public string Estado { get; set; }
        public int Enviados { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaEnvio { get; set; }
        public string Tipo { get; set; }
    }
}