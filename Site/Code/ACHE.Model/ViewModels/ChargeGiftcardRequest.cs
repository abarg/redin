﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class ChargeGiftcardRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string cardNumber { get; set; }
        public decimal amount { get; set; }
        public string cellPhoneCompany { get; set; }
        public string cellPhone { get; set; }
        public string ticket { get; set; }
    }
}
