﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class RubrosViewModel
    {
        public int IDRubro { get; set; }
        public string Nombre { get; set; }
        public int? IDRubroPadre { get; set; }
    }
}
