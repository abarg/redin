﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class ReprogramacionesViewModel {
        public string Nro { get; set; }
        public string Provincia { get; set; }
        public string Zona { get; set; }
        public string Compras { get; set; }
        public string Puntos { get; set; }
        public string Giftcard { get; set; }
        public string RazonSocial { get; set; }
        public string NombreFantasia { get; set; }
        public string CUIT { get; set; }
        public string Domicilio { get; set; }
        public string NroTerminal { get; set; }
        public string NroEstablecimiento { get; set; }
        public string DescLunes { get; set; }
        public string DescMartes { get; set; }
        public string DescMiercoles { get; set; }
        public string DescJueves { get; set; }
        public string DescViernes { get; set; }
        public string DescSabado { get; set; }
        public string DescDomingo { get; set; }
        public string PuntosPOS { get; set; }
        public string Calco { get; set; }
        public string Display { get; set; }
        public string CalcoPuerta { get; set; }
        public string VerifPOSCompra { get; set; }
        public string VerifPOSPuntos { get; set; }
        public string Folletos { get; set; }
        public string EntregaTarjetas { get; set; }
        public string RetiroTarjetas { get; set; }
        public string CambioTarj99 { get; set; }
        public string Observaciones { get; set; }
        public string ObservacionesPOS { get; set; }
    }

    public class ReporteReprogramacionesViewModel {
        public string Tipo { get; set; }
        public string NombreFantasia { get; set; }
        public string RazonSocial { get; set; }
        public string CUIT { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string CodigoPostal { get; set; }
        public string Contacto { get; set; }
        public string NroTerminal { get; set; }
        public string MarcaVisa { get; set; }
        public string Moneda { get; set; }
        public string NumEst { get; set; }
        public int CodigoSolicitante { get; set; }
        public string Observacion { get; set; }
        public string Marca { get; set; }
    }
}
