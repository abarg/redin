﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{

    /// <summary>
    /// Summary description for TerminalesViewModel
    /// </summary>
    /// 

    public class TerminalesViewModel
    {
        public string SDS { get; set; }
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public string Fecha_Carga { get; set; }
        public string Fecha_Alta { get; set; }
        public string Dealer { get; set; }
        public string Tipo { get; set; }
        public string Terminal { get; set; }
        public string Establecimiento { get; set; }
        public string Fecha_Activ { get; set; }
        public string Fecha_Reprog { get; set; }
        public DateTime FechaReprog { get; set; }
        public string Reprogramado { get; set; }
        public string Invalido { get; set; }
        public string Activo { get; set; }
        public string Observaciones { get; set; }
        public string Estado { get; set; }
        public string EstadoGift { get; set; }
        public string EstadoCanjes { get; set; }
        public string EstadoCompras { get; set; }
        public int CantTR { get; set; }
        public decimal UltimoImporte { get; set; }
        public string Marca { get; set; }
        public string Franquicia { get; set; }
    }

    public class TerminalesFullViewModel
    {

        public int IDComercio { get; set; }
        public int IDTerminal { get; set; }
        public int IDMarca { get; set; }
        public int? IDFranquicia { get; set; }
        public string SDS { get; set; }
        public string NombreFantasia { get; set; }
        public string RazonSocial { get; set; }
        public string NroDocumento { get; set; }
        public string POSTerminal { get; set; }
        public string POSSistema { get; set; }
        public string NumEst { get; set; }
        public bool CobrarUsoRed { get; set; }

    }

    public class TerminalesViewModelSP
    {
        public string SDS { get; set; }
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public int IDMarca { get; set; }
        public string NombreMarca { get; set; }
        public int IDFranquicia { get; set; }
        public string NombreFranquicia { get; set; }
        public string Localidad { get; set; }
        public string FechaCarga { get; set; }
        public string FechaAlta { get; set; }
        public string Dealer { get; set; }
        public string Tipo { get; set; }
        public string TipoTerminal { get; set; }
        public string Terminal { get; set; }
        public string Establecimiento { get; set; }
        public string FechaActivacion { get; set; }
        public DateTime FechaReprogramacion { get; set; }
        public bool Reprogramado { get; set; }
        public bool ComercioInvalido { get; set; }
        public bool Activo { get; set; }
        public string Observaciones { get; set; }
        public int CantTR { get; set; }
        public decimal UltimoImporte { get; set; }
        public int Franquicia { get; set; }
        public string EstadoCanjes { get; set; }
        public string EstadoGift { get; set; }
        public string EstadoCompras { get; set; }
        public string Estado { get; set; }
    }
}