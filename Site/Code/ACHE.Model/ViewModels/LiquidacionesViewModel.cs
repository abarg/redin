﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class LiquidacionesViewModel
    {
        public int IDLiquidacion { get; set; }
        public int IDFranquicia { get; set; }
        public DateTime FechaGeneracion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string Estado { get; set; }
        public string Franquicia { get; set; }
        public decimal ImporteTotal { get; set; }
    }

    public class ComprobanteLiquidacionesViewModel
    {
        public string Factura { get; set; }
        public string Transferencia { get; set; }
    }
}
