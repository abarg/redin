﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class GetShopInfoResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string DocNumber { get; set; }
        public string Terminal { get; set; }
        public string Establishment { get; set; }
        public int Discount { get; set; }
        public bool ShowProducts { get; set; }
        public string Footer1 { get; set; }
        public string Footer2 { get; set; }
        public string Footer3 { get; set; }
        public string Footer4 { get; set; }
        public bool ShowMenuFidelity { get; set; }
        public bool ShowMenuGift { get; set; }
        public bool ShowChargeGift { get; set; }
        public bool ShowLot { get; set; }
        public bool PrintLOGO { get; set; }
        public bool InputPayments { get; set; }
        public bool InputTicket { get; set; }
        public int Logo { get; set; }

    }
}
