﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class GetProductsInfoResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public Products Products { get; set; }

    }
}
