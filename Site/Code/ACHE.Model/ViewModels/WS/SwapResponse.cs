﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class SwapResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public int TotalPoints { get; set; }
        public decimal Credits { get; set; }
        public decimal NetworkCost { get; set; }
        public int IdTransaction { get; set; }
    }
}
