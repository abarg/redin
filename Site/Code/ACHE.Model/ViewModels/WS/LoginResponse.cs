﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class LoginResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public Operador Operador { get; set; }

    }
}
