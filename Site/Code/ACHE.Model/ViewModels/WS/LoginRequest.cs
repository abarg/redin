﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class LoginRequest {
        public string userName { get; set; }
        public string pwd { get; set; }
    }
}
