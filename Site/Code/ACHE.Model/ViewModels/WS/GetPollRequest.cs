﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class GetPollRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
    }
}
