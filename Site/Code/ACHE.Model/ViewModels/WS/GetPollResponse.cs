﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class GetPollResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string question { get; set; }
        public byte minRank { get; set; }
        public byte maxRank { get; set; }

    }
}
