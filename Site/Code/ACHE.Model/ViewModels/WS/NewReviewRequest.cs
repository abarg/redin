﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewReviewRequest {
        public string sessionID { get; set; }
        public string cardNumber { get; set; }
        public string terminal { get; set; }
        public byte rating { get; set; }
    }
}
