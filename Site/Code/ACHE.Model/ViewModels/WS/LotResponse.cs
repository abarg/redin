﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model{
    [Serializable]
    public class LotResponse {
        public string sessionID { get; set; }
        public int AnswerCode { get; set; }
        public string Title { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Message3 { get; set; }
        public string Message4 { get; set; }
        public string Customer { get; set; }
        public string CardNumber { get; set; }
        public string DateUp { get; set; }
        public string DateFrom { get; set; }
    }
}
