﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Ws {
    [Serializable]
    public class Sale {
        public string Title { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Message3 { get; set; }
        public string Message4 { get; set; }
        public int TypeCode { get; set; }
        public string InformationEncoded { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateUp { get; set; }
    }
}
