﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewTransactionResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public int IdTransaction { get; set; }
        public decimal Points { get; set; }
        public int TotalPoints { get; set; }
        public int Discount { get; set; }
        public decimal Credits { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TotalFinal { get; set; }
        public decimal NetworkCost { get; set; }
        public bool InfoHasChanged { get; set; }
        public bool HasSale { get; set; }
        public Sale Sale { get; set; }
    }
}
