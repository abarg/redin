﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class ActividadesView
    {

        // ID's
        public int? IDCategoria { get; set; }
        public int? IDSubCategoria { get; set; }
        public int IDActividad { get; set; }
        public string IDHorarioIDSede { get; set; }
        public int IDNivel { get; set; }
        public int IDSocio { get; set; }


        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public string Actividad { get; set; }
        public string ActividadNivel { get; set; }
        public string Nivel { get; set; }
        public string Horario { get; set; }
        public string ActividadNivelHorario { get; set; }
        public string Sede { get; set; }
        public int CantidadBeneficiarios { get; set; }
        public string Secretaria { get; set; }

    }
}
