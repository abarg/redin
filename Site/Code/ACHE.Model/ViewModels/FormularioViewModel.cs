﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class FormularioViewModel
    {
        public int ID { get; set; }
        public int IDFranquicia { get; set; }
        public DateTime? Fecha { get; set; }
        public string UnidadNegocio { get; set; }
        public string Franquicia { get; set; }
        public DateTime? FechaPedido { get; set; }
        public string Estado { get; set; }
        public string Cliente { get; set; }
        public DateTime? FechaEnvioProveedor { get; set; }
        public int Counter { get; set; }


    }
}

