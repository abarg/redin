﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class TicketseViewModel
    {
        public int IDUsuario { get; set; }
        public int TipoUsuario { get; set; }
        public string Area { get; set; }
        public int IDTicket { get; set; }
        public string Estado { get; set; }
        public string Fecha { get; set; }
        public string FechaCierre { get; set; }
        public string Descripcion { get; set; }
        public string Usuario { get; set; }
        public string Adjunto { get; set; }
        public string MostrarAdjunto { get; set; }
        public string Asunto { get; set; }

    }

}
