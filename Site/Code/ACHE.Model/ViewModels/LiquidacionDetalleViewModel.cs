﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class LiquidacionDetalleViewModel
    {
        public int IDLiquidacionDetalle { get; set; }
        public string Concepto { get; set; }
        public decimal SubTotal { get; set; }
    }
    public class LiquidacionDetalleObservacionesViewModel
    {
        public DateTime FechaGeneracion { get; set; }
        public string Origen { get; set; }
        public string Observaciones { get; set; }
    }
}
