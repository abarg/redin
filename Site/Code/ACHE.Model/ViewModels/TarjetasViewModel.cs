﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    /// <summary>
    /// Summary description for TarjetasViewModel
    /// </summary>
    [Serializable]
    public class TarjetasViewModel {
        public int IDTarjeta { get; set; }
        public int IDMarca { get; set; }
        public int? IDFranquicia { get; set; }
        public int IDSocio { get; set; }
        public string Marca { get; set; }
        public string Franquicia { get; set; }
        public string Numero { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public int Puntos { get; set; }
        public decimal POS { get; set; }
        public decimal Credito { get; set; }
        public decimal Giftcard { get; set; }
        public decimal Total { get; set; }
        public decimal TotalAhorro { get; set; }
        public string Estado { get; set; }
        public string MotivoBaja { get; set; }
        public DateTime? FechaBaja { get; set; }
        public string Socio { get; set; }
        public string Tipo { get; set; }
    }
}
