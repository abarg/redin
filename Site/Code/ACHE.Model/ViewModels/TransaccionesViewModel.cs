﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for TransaccionesViewModel
    /// </summary>
    public class TransaccionesViewModel
    {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string Hora { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
        public string SDS { get; set; }
        public string Comercio { get; set; }
        public string NroEstablecimiento { get; set; }
        public string Tarjeta { get; set; }
        public string Socio { get; set; }
        public decimal ImporteOriginal { get; set; }
        public decimal ImporteAhorro { get; set; }
        public decimal Puntos { get; set; }
        public decimal PuntosTotales { get; set; }
        public string NroDocumentoSocio { get; set; }
        public int IDMarca { get; set; }
        public string Marca { get; set; }
        public int? IDFranquicia { get; set; }
        public string NumTerminal { get; set; }
    }

        public class TransaccionesLiquidacionViewModel
    {
        public string Fecha { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string Hora { get; set; }
        public string Origen { get; set; }
        public string Operacion { get; set; }
        public string SDS { get; set; }
        public string Tarjeta { get; set; }
        public string Socio { get; set; }
        public decimal ImporteOriginal { get; set; }
        public decimal ImporteAhorro { get; set; }
        public decimal Puntos { get; set; }
        
      }


             

    [Serializable]
    public class TransaccionesSociosViewModel
    {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string Hora { get; set; }
        public string Operacion { get; set; }
        public string Comercio { get; set; }
        public string Tarjeta { get; set; }
        public string Marca { get; set; }
        public decimal ImporteOriginal { get; set; }
        public decimal ImporteAhorro { get; set; }
        public decimal Puntos { get; set; }
        public decimal Credito { get; set; }
        public decimal Giftcard { get; set; }
        public decimal POS { get; set; }
        public decimal Total { get; set; }
    }
}
