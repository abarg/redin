﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class DealerViewModel
    {
        public int IDDealer { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        public string Codigo { get; set; }
        public decimal ComisionAlta { get; set; }
        public decimal ComisionTarjetas { get; set; }
        public decimal ComisionGiftCard { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Activo { get; set; }
    }

}
