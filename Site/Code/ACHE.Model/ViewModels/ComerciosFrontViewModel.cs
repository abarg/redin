﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class ComerciosFrontViewModel
    {
     public   int IDComercio  { get; set; }
    public  string SDS  { get; set; }
    public  string NombreFantasia { get; set; }
    public  string RazonSocial { get; set; }
    public string TipoDocumento { get; set; }
    public string NroDocumento { get; set; }
    public string Telefono { get; set; }
    public string Franquicia { get; set; }
    public string Marca { get; set; }
    public string Rubro { get; set; }
    public string SubRubro { get; set; }
    public string NombreEstablecimiento { get; set; }
    public string NroEstablecimiento { get; set; }
    }
}
