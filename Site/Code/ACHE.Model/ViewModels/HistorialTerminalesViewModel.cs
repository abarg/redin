﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Summary description for HistorialTerminalesViewModel
/// </summary>
public class HistorialTerminalesViewModel
{
    public int ID { get; set; }
    public int IDComercio { get; set; }
    public string SDS { get; set; }
    public string Tipo { get; set; }
    public string NombreFantasia { get; set; }
    public string RazonSocial{ get; set; } 
    public string Anterior { get; set; }
    public string Nuevo { get; set; }
    public string Observaciones { get; set; }
    public DateTime FechaBaja { get; set; }
}

