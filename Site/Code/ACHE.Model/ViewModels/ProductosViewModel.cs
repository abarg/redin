﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class ProductosViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public string Familia { get; set; }
        public int IDFamilia { get; set; }
        public int IDPadre { get; set; }
        public string NombrePadre { get; set; }
        public string SubFamilia { get; set; }
        public int IDSubFamilia { get; set; }
        public int StockActual { get; set; }
        public int StockMinimo { get; set; }
        public int Puntos { get; set; }
        public string Activo { get; set; }
        public decimal PrecioXCantidad { get; set; }
    }
}
