﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetMemberResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string memberName { get; set; }
        public string memberLastName { get; set; }
        public string memberEmail { get; set; }
        public string memberCellphone { get; set; }
        public int memberID { get; set; }
        public List<Card> cards { get; set; }

    }
}
