﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{
    public class GetLocationsRequest
    {
        public string sessionID { get; set; }
        public int? countryID { get; set; }
        public int? provinceID { get; set; }

    }
}