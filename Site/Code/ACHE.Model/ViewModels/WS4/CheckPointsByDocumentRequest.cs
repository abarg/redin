﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class CheckPointsByDocumentRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string nroDocument { get; set; }
        public string type { get; set; }
    }
}
