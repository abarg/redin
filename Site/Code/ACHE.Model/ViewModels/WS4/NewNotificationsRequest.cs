﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class NewNotificationsRequest {

        public int transactionID { get; set; }
        public bool sendSMS { get; set; }
        public string phoneNumber { get; set; }
        public bool sendEmail { get; set; }
        public string email { get; set; }
        public string sessionID { get; set; }

    }
}
