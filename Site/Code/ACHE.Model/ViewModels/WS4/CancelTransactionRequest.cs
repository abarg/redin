﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class CancelTransactionRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string txMethod { get; set; }
        public string dniNumber { get; set; }
        public string nfcCode { get; set; }
        public string cardNumber { get; set; }
        public int transactionID { get; set; }
        public string observation { get; set; }
    }
}
