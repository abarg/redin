﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{
    [Serializable]
    public class City
    {
        public int provinceID { get; set; }
        public int cityID { get; set; }
        public string name { get; set; }
    }
}
