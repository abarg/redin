﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetShopInfoResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public int shopID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string docNumber { get; set; }
        public string terminal { get; set; }
        public string establishment { get; set; }
        public int discount { get; set; }
        public int logo { get; set; }
        public bool showProducts { get; set; }
        public string footer1 { get; set; }
        public string footer2 { get; set; }
        public string footer3 { get; set; }
        public string footer4 { get; set; }
        public bool showMenuFidelity { get; set; }
        public bool showMenuGift { get; set; }
        public bool showChargeGift { get; set; }
        public bool showLot { get; set; }
        public bool printLogo { get; set; }
        public bool inputPayments { get; set; }
        public bool inputTicket { get; set; }
        public string logoAppRoute { get; set; }
        public string logoTicketRoute { get; set; }
        public string colorApp { get; set; }
        public bool? showSMSNotification { get; set; }
        public bool? showEmailNotification { get; set; }
        public bool showPoll { get; set; }
        public string defaultView { get; set; }
        public string urlGestion { get; set; }
        public List<MarcaMenu> menu { get; set; }
        public List<PaymentGateaway> paymentGateaways { get; set; }

    }

}
