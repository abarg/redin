﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4{

    [Serializable]
    public class NewMemberRequest {
        public string sessionID { get; set; }
        public bool hasCard { get; set; }
        public string cardNumber { get; set; }
        public string terminal { get; set; }
        public string nroDocumento { get; set; }
        public string tipoDoc { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string fechaNacimiento { get; set; }
        public string celular { get; set; }
        public string empresaCelular { get; set; }
        public string email { get; set; }
        public Domicilio domicilio { get; set; }

    }
}
