﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class Shop {

        public int shopID { get; set; }
        public string shopName { get; set; }
        public string address { get; set; }
        public string category { get; set; }
        public string logoURL { get; set; }
        public string city { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int pointsMultiplier1 { get; set; }
        public int pointsMultiplier2 { get; set; }
        public int pointsMultiplier3 { get; set; }
        public int pointsMultiplier4 { get; set; }
        public int pointsMultiplier5 { get; set; }
        public int pointsMultiplier6 { get; set; }
        public int pointsMultiplier7 { get; set; }
        public int discounts1 { get; set; }
        public int discounts2 { get; set; }
        public int discounts3 { get; set; }
        public int discounts4 { get; set; }
        public int discounts5 { get; set; }
        public int discounts6 { get; set; }
        public int discounts7 { get; set; }
        public string benefitDescription { get; set; }

    }
}
