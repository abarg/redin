﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class NewPayment {
        public string publicKey { get; set; }
        public string cardToken { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public int installments { get; set; }
        public decimal txAmount { get; set; }
        public string description { get; set; }
        
        public string paymentMethodID { get; set; }
        public Payer Payer { get; set; }
    }
}
