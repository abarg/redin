﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetTotalTransactionsRequest {
        public string sessionID { get; set; }
        public int idTerminal { get; set; }
        public string dateSince { get; set; }
        public string dateUntil { get; set; }
    }
}
