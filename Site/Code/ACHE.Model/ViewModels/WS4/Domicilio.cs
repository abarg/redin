﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{
    [Serializable]
    public class Domicilio
    {
        public int countryID { get; set; }
        public int provinceID { get; set; }
        public int cityID { get; set; }
        public string address { get; set; }
    }
}
