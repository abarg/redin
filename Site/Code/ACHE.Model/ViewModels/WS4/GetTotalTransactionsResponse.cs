﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{
    [Serializable]
    public class GetTotalTransactionsResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string amountPurchases { get; set; }
        public string amountSales { get; set; }
        public string amountAnnulment { get; set; }
        public string amountLoads { get; set; }
        public string amountDownloads { get; set; }
        public int quantityPurchases { get; set; }
        public int quantitySales { get; set; }
        public int quantityAnnulment { get; set; }
        public int quantityLoads { get; set; }
        public int quantityDownloads { get; set; }
    }
}
