﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class CreatePaymentRequest {
        public string sessionID { get; set; }
        public string paymentGateaway { get; set; }
        public string publicKey { get; set; }
        public string cardToken { get; set; }
        public string paymentMethodID { get; set; }
        public int installments { get; set; }
        public decimal txAmount { get; set; }
        public string detail { get; set; }
        public Payer Payer { get; set; }
    }
}
