﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class CheckPointsByDocumentResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public int points { get; set; }
        public decimal credits { get; set; }
    }
}
