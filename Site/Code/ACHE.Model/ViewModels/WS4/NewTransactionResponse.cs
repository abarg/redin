﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class NewTransactionResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public int transactionID { get; set; }
        public decimal points { get; set; }
        public int totalPoints { get; set; }
        public int discount { get; set; }
        public decimal credits { get; set; }
        public decimal discountAmount { get; set; }
        public decimal totalFinal { get; set; }
        public decimal networkCost { get; set; }
        public bool infoHasChanged { get; set; }
        public bool hasSale { get; set; }
        public Sale sale { get; set; }
    }
}
