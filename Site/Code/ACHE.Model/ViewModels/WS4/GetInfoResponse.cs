﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetInfoResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string customerName { get; set; }
        public string customerLastName { get; set; }
        public int customerID { get; set; }
        public int points { get; set; }
        public decimal credits { get; set; }
        public string type { get; set; }

    }
}
