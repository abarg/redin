﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class Notification {
        public int notificationID { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public string action { get; set; }
        public DateTime dateCreated { get; set; }
        public bool isRead { get; set;}
    }
}
