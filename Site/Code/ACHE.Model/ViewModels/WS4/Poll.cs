﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class Poll {
        public int pollID { get; set; }
        public string question { get; set; }
        public byte minRank { get; set; }
        public byte maxRank { get; set; }
    }
}
