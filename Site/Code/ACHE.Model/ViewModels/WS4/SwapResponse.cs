﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class SwapResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public int totalPoints { get; set; }
        public decimal credits { get; set; }
        public decimal networkCost { get; set; }
        public int transactionID { get; set; }
    }
}
