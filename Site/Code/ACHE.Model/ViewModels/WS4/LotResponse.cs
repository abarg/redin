﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4{
    [Serializable]
    public class LotResponse {
        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string title { get; set; }
        public string message1 { get; set; }
        public string message2 { get; set; }
        public string message3 { get; set; }
        public string message4 { get; set; }
        public string customer { get; set; }
        public string cardNumber { get; set; }
        public string dateUp { get; set; }
        public string dateFrom { get; set; }
    }

    [Serializable]
    public class SMSResponse
    {
        public string sessionID { get; set; }
        public int AnswerCode { get; set; }
        
    }
}
