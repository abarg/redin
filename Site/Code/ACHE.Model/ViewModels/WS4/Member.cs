﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class Member {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public List<Card> cards { get; set; }
    }
}
