﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{
    [Serializable]
    public class Country
    {
        public int countryID { get; set; }
        public string name { get; set; }
    }
}
