﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class CreatePaymentResponse {

        public string sessionID { get; set; }
        public int answerCode { get; set; }
        public string paymentID { get; set; }
        public string paymentStatus { get; set; }
        public string paymentStatusDetail { get; set; }

    }
}
