﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4{

    [Serializable]
    public class GetMemberRequest {
        public string sessionID { get; set; }
        public string nationalID { get; set; }
    }
}
