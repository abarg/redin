﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetNotificationsResponse
    {
        public int answerCode { get; set; }
        public List<Notification> notificaciones { get; set; }

    }
}
