﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class CreatePreferenceRequest {
        public string sessionID { get; set; }
        public string paymentGateaway { get; set; }
        public string publicKey { get; set; }
        public float txAmount { get; set; }
    }
}
