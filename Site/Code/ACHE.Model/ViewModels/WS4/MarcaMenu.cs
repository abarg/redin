﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class MarcaMenu {
        public string nombre { get; set; }
        public int? posicion { get; set; }
    }
}
