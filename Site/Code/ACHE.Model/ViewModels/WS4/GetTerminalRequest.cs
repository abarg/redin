﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {
    [Serializable]
    public class GetTerminalRequest {
        public string sessionID { get; set; }
        public ShopsFilters filters { get; set; }
    }
}
