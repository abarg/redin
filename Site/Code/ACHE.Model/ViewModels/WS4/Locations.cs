﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4
{

    [Serializable]
    public class Locations
    {
       public List<Country> countries { get; set; }
       public List<Province> provincies { get; set; }
       public List<City> cities { get; set; }
    }
}