﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.WS4 {

    [Serializable]
    public class PaymentGateaway {
        public string gateaway { get; set; }
        public string publicKey { get; set; }
        public string customColor { get; set; }
        public string logo { get; set; }
        public bool hasInstallments { get; set; }
        public bool requiredPayerIdentity { get; set; }
        public bool isCrypto { get; set; }
        public bool allowSMS { get; set; }
        public bool allowEmail { get; set; }
    }
}
