﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for TarjetaDisponible
    /// </summary>
    public class TarjetaDisponible
    {
        public int IDTarjeta { get; set; }
        public string Numero { get; set; }
    }
}