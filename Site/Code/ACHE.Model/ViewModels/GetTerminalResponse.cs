﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class GetTerminalResponse {

        public string Terminal { get; set; }
        public int AnswerCode { get; set; }

    }
}
