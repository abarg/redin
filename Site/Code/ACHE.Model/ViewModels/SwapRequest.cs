﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class SwapRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string txMethod { get; set; }
        public string dniNumber { get; set; }
        public string cardNumber { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public string ticket { get; set; }
    }
}
