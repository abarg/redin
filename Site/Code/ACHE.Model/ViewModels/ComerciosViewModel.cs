﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Summary description for ComerciosViewModel
/// </summary>
public class ComerciosViewModel
{
    public int IDComercio { get; set; }
    public string SDS { get; set; }
    public string NombreFantasia { get; set; }
    public string RazonSocial { get; set; }
    public string TipoDocumento { get; set; }
    public string NroDocumento { get; set; }
    public string Telefono { get; set; }
    public string Celular { get; set; }
    public string Responsable { get; set; }
    public string Cargo { get; set; }
    public string Actividad { get; set; }
    public string CondicionIva { get; set; }
    public string Web { get; set; }
    public string Email { get; set; }
    public string Domicilio { get; set; }
    /*public DateTime FechaAlta { get; set; }
    public string IDDomicilio { get; set; }
    public string IDDomicilioFiscal { get; set; }
    public string POSTipo { get; set; }*/
    public string POSSistema { get; set; }
    public string POSTerminal { get; set; }
    /*public string POSEstablecimiento { get; set; }
    public string POSMarca { get; set; }
    public string Observaciones { get; set; }
    public string Estado { get; set; }
    public string CodigoPostal { get; set; }
    public string TipoMov { get; set; }*/
    public string NumEst { get; set; }
    /*public string NroNumEst { get; set; }
    public string NombreEst { get; set; }
    public string CodAct { get; set; }
    public string NumEstBenef { get; set; }
    public string EstadoBenef { get; set; }
    public int Descuento { get; set; }
    public string DescuentoVip { get; set; }
    public string AffinityBenef { get; set; }
    public string Activo { get; set; }
    public string CodigoDealer { get; set; }
    public string FormaPago { get; set; }
    public string FormaPago_Banco { get; set; }
    public string FormaPago_TipoCuenta { get; set; }
    public string FormaPago_NroCuenta { get; set; }
    public string FormaPago_CBU { get; set; }
    public string FormaPago_Tarjeta { get; set; }
    public string FormaPago_BancoEmisor { get; set; }
    public string FormaPago_NroTarjeta { get; set; }
    public string FormaPago_FechaVto { get; set; }
    public string FormaPago_CodigoSeg { get; set; }*/
    public bool CobrarUsoRed { get; set; }
    public int IDMarca { get; set; }
    public int? IDFranquicia { get; set; }
    public string CBU { get; set; }
}

