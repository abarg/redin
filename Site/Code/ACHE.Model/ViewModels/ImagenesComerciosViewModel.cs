﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {

   public class ImagenesComerciosViewModel {
        public int IDImagenComercio { get; set; }
        public int IDComercio { get; set; }
        public string URL { get; set; }
    }
}

