﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class EmpresasViewModel
    {
        public int IDEmpresa { get; set; }
        public string Nombre { get; set; }
        public string Color { get; set; }
        public string Logo { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Activo { get; set; }
    }
}
