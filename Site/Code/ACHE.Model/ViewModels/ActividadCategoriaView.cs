﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class ActividadCategoriasView
    {
        public int? IDCategoria { get; set; }
        public int IDSubCategoria { get; set; }
        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public string Secretaria { get; set; }
        public int CantidadBeneficiarios { get; set; }
    }
}
