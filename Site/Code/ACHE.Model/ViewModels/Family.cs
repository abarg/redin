﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class Family {
        public int familyID { get; set; }
        public string name { get; set; }
        public List<Product> list { get; set; }
    }
}
