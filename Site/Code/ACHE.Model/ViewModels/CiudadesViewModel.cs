﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class CiudadesViewModel
    {
        public int IDCiudad { get; set; }
        public int IDProvincia { get; set; }
        public int IDPais { get; set; }
        public int CP { get; set; }
        public string Provincia { get; set; }
        public string Pais { get; set; }
        public string Nombre { get; set; }
    }
}
