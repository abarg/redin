﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class CancelTransactionRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string cardNumber { get; set; }
        public int idTransaction { get; set; }
        public string observation { get; set; }
        public string ticket { get; set; }
    }
}
