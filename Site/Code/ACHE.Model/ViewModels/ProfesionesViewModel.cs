﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class ProfesionesViewModel
    {
        public int IDProfesion { get; set; }
        public string Nombre { get; set; }
    }
}
