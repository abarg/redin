﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class ReverseTransactionResponse
    {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
    }
}
