﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    public class PuntoDeVentaViewModel
    {
        public int IDPuntoVenta { get; set; }
        public int IDComercio { get; set; }
        public int Punto { get; set; }
        public DateTime FechaAlta { get; set; }
        public string EsDefault { get; set; }
    }
}
