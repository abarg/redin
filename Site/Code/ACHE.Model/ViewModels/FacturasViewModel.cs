﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model {
    /// <summary>
    /// Summary description for FacturasViewModel
    /// </summary>
    public class FacturasViewModel {
        public int ID { get; set; }
        public int IDEntidad { get; set; }
        public string Nombre { get; set; }
        public string Numero { get; set; }
        public DateTime FechaDesde { get; set; }
        public string CBU { get; set; }
        public DateTime FechaHasta { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }
        public string CAE { get; set; }
        public string Enviada { get; set; }
        public string CUIT { get; set; }
        public string Descargada { get; set; }
        public string CondicionIva { get; set; }
        public string Tipo { get; set; }
        public string ArchivoDetalle { get; set; }
        public string EnContabilium { get; set; }
        public decimal ImporteDebitar { get; set; }
    }

    public class FacturasFrontViewModel {
        public int ID { get; set; }
        public string Comercio { get; set; }
        public string NroDocumento { get; set; }
        public string CondicionIva { get; set; }
        public string Numero { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVto { get; set; }
        public decimal Importe { get; set; }
        public bool Pagada { get; set; }
        public List<FacturasDetalleSinCAEViewModel> factDet { get; set; }
    }
  
    public class FacturasDetViewModel {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Operacion { get; set; }
        public string Tarjeta { get; set; }
        public string NroDocumento { get; set; }
        public decimal Ticket { get; set; }
        public decimal Arancel { get; set; }
        public decimal Puntos { get; set; }
        public decimal CostoRed { get; set; }
        public decimal NetoGrabado { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }
    }

    public class FacturasGaliciaViewModel {
        public int IDFactura { get; set; }
        public string Nombre { get; set; }
        public int IDComercio { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public string Documento { get; set; }
        public decimal ImporteTotal { get; set; }
        public decimal ImporteADebitar { get; set; }

        public string FormaPago { get; set; }
        public string FormaPagoCBU { get; set; }
        public string FormaPagoTipoCuenta { get; set; }
        public string TipoDocumento { get; set; }
        public string TipoTarjeta { get; set; }
        public decimal Retenciones { get; set; }
        public decimal Canjes { get; set; }
    }
    public class FacturasSinCaeViewModel
    {
        public string Tipo { get; set; }
        public List<FacturasDetalleSinCAEViewModel> factDet { get; set; }
        public ClientesSinCaeViewModel cliente { get; set; }
                                  
    }
    public class ClientesSinCaeViewModel{
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string CoindicionIva { get; set; }

    }
    public class FacturasDetalleSinCAEViewModel
    {
        public int IDFacturaDetalle { get; set; }
        public string Concepto { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Iva { get; set; }
        public int Cantidad { get; set; }
    }

    public class FacturasProveedoresViewModel {
        public int IDComercio { get; set; }
        public int IDFactura { get; set; }
        public int IDProveedor { get; set; }
        public string Numero { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string NombreFantasia { get; set; }
    }
}