﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Summary description for FranquiciasViewModel
/// </summary>
public class FranquiciasViewModel
{
    public int IDFranquicia { get; set; }
    public string Nombre { get; set; }
    public string RazonSocial{ get; set; } 
    public string TipoDocumento { get; set; }
    public string NroDocumento { get; set; }
    public string CondicionIva{ get; set; }
    public decimal Arancel { get; set; }
    public decimal ComisionTtCp { get; set; }
    public decimal ComisionTpCp { get; set; }
    public decimal ComisionTpCt { get; set; }
    public decimal PubLocal { get; set; }
    public decimal PubNacional { get; set; }
    public string Email { get; set; }
    /*public DateTime FechaAlta { get; set; }
    public string IDDomicilio { get; set; }
    public string Observaciones { get; set; }
    public string Estado { get; set; }
    public string FormaPago { get; set; }
    public string FormaPago_Banco { get; set; }
    public string FormaPago_TipoCuenta { get; set; }
    public string FormaPago_NroCuenta { get; set; }
    public string FormaPago_CBU { get; set; }
    public string FormaPago_Tarjeta { get; set; }
    public string FormaPago_BancoEmisor { get; set; }
    public string FormaPago_NroTarjeta { get; set; }
    public string FormaPago_FechaVto { get; set; }
    public string FormaPago_CodigoSeg { get; set; }*/
}

