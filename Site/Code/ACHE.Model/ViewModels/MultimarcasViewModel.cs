﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class MultimarcasViewModel {
        public int IDMultimarca { get; set; }
        public string Nombre { get; set; }
        public string Affinity { get; set; }
        public string Prefijo { get; set; }
        public string MostrarSoloTarjetasPropias { get; set; }
        public string MostrarSoloPOSPropios { get; set; }
        public decimal Arancel { get; set; }
        public int? IDFranquicia { get; set; }
        public string Franquicia { get; set; }
        public DateTime FechaAlta { get; set; }

        //public int IDMultimarca { get; set; }
        //public int IDMarca1 { get; set; }
        //public int IDMarca2 { get; set; }
    }
}
