﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for MovimientosViewModel
    /// </summary>
    public class MovimientosViewModel
    {
        public int ID { get; set; }
        public int IDEntidad { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Importe { get; set; }
        public string Observaciones { get; set; }
        public string Concepto { get; set; }
        public string CUIT { get; set; }
        public string Facturada { get; set; }
    }
}