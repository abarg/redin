﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class RptRankingSociosViewModel
    {
        public int IDFranquicia { get; set; }
        public int IDMarca { get; set; }
        public int CantTr { get; set; }
        public string Franquicia { get; set; }
        public string Marca { get; set; }
        public string Socio { get; set; }
        public string NroDocumento { get; set; }
        public string Tarjeta { get; set; }
        public decimal Importe { get; set; }
        public decimal ImporteAhorro { get; set; }
    }
}
