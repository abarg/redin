﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class NewNotificationsRequest {

        public int IdTransaction { get; set; }

        public bool sendSMS { get; set; }
        public string numeroTelefono { get; set; }
        public bool sendEmail { get; set; }
        public string email { get; set; }
        public string sessionID { get; set; }

    }
}
