﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for UsuariosViewModel
    /// </summary>
    public class UsuariosViewModel
    {
        public int IDUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }
        public string Pwd { get; set; }
        public string Tipo { get; set; }
        public string Activo { get; set; }
    }
}