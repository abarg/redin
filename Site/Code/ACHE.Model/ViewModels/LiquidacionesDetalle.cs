﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class LiquidacionesDetalle
    {
        int IDLiquidacionDetalle { get; set; }
        int IDLiquidacion { get; set; }
        int IDComercio { get; set; }
        string Comercio { get; set; }
        decimal ImporteTotalOriginal { get; set; }
        decimal ImporteTotal { get; set; }
        string Observaciones { get; set; }

    }
}
