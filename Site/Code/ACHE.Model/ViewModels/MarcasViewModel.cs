﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for MarcasViewModel
    /// </summary>
    public class MarcasViewModel
    {
        public int IDMarca { get; set; }
        public string Nombre { get; set; }
        public string Affinity { get; set; }
        public string Prefijo { get; set; }
        public string MostrarSoloTarjetasPropias { get; set; }
        public string MostrarSoloPOSPropios { get; set; }
        public decimal Arancel { get; set; }
        public int? IDFranquicia { get; set; }
        public string Franquicia { get; set; }
        public DateTime FechaAlta { get; set; }
    }
}