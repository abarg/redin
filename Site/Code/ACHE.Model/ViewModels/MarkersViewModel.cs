﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{

    /// <summary>
    /// Summary description for MarkersViewModel
    /// </summary>
    public class MarkersViewModel
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Categoria { get; set; }
        public string Icono { get; set; }
        public string Contenido { get; set; }
        public string Ficha { get; set; }
        public string Foto { get; set; }
    }
}