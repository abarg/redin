﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class PinesViewModel {
        public int IDPin { get; set; }
        public int IDEmpresaCanje { get; set; }
        public string EmpresaCanje { get; set; }
        public string Control { get; set; }
        public string Parte1 { get; set; }
        public string Parte2 { get; set; }
        public decimal CostoInterno { get; set; }
        public int CostoPuntos { get; set; }
        public string Socio { get; set; }
        public DateTime? FechaCompra { get; set; }
        public string Comprado { get; set; }
    }
}
