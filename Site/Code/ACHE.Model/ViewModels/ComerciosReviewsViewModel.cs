﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class ComerciosReviewsViewModel {
        public int IDComercio { get; set; }
        public int IDEncuesta { get; set; }
        public int Rating { get; set; }
        public string Detalle { get; set; }
        public string NombreFantasia { get; set; }
        public string numTerminal { get; set; }
        public string nombreYApellido { get; set; }
        public double avgRating { get; set; }
        public int totalAnswers { get; set; }
        public byte RangoMin { get; set; }
        public byte RangoMax { get; set; }
        public string Rango { get; set; }
    }
}
