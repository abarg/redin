﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class GeneracionDatViewModel
    {
    }
    public class PremiosPuntosViewModel
    {
        public string codProd { get; set; }
        public string valorPuntos { get; set; }
    }
    public class TarjetasPuntosViewModel
    {
        public string numTar { get; set; }
        public string EstadoTarjeta { get; set; }


    }
    public class CuentasPuntosViewModel
    {
        public string puntosDisponibles { get; set; }
        public string numCuenta { get; set; }
    }
    public class ComerciosBeneficiosViewModel
    {
        public string NumEst { get; set; }
        public string NombreEstab { get; set; }
        public string descuento { get; set; }
        public string descuentoVip { get; set; }

    }
    public class TarjetasBeneficiosViewModel
    {
        public string numTar { get; set; }

        public string affinity { get; set; }
      

    }
  
}
