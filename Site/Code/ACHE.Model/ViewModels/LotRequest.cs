﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {

    [Serializable]
    public class LotRequest {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string cardNumber { get; set; }

        public string phoneNumber { get; set; }
        public string company { get; set; }
        public string message { get; set; }

    }

    [Serializable]
    public class SMSRequest
    {
        public string sessionID { get; set; }
        public string terminal { get; set; }
        public string cardNumber { get; set; }

        public string phoneNumber { get; set; }
        public string company { get; set; }
        public string message { get; set; }

    }
}
