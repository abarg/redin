﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    [Serializable]
    public class GetInfoResponse {
        public string SessionID { get; set; }
        public int AnswerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerLastName { get; set; }
        public int CustomerId { get; set; }
        public int Points { get; set; }
        public decimal Credits { get; set; }

    }
}
