﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for CCEmpresaGeneralViewModel
    /// </summary>
    public class CCEmpresaGeneralViewModel
    {
        public int ID { get; set; }
        public string Comercio { get; set; }
        public decimal Importe { get; set; }
        public decimal Abonado { get; set; }
        public string FechaUltimoPago { get; set; }
        public decimal Saldo { get; set; }
        
        
    }
}