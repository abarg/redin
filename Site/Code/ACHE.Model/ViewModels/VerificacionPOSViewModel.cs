﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class VerificacionPOSViewModel {
        public int IDVerificacionPOS { get; set; }
        public string EstadoCompras { get; set; }
        public string EstadoGift { get; set; }
        public string EstadoCanjes { get; set; }

        public string Calco { get; set; }
        public string Folletos { get; set; }
        public string Display { get; set; }
        public string CalcoPuerta { get; set; }

        /*
        public string ObservacionesCompras { get; set; }
        public string ObservacionesCanjes { get; set; }
        public string ObservacionesGift{ get; set; }

        public string PuntosCompras { get; set; }
        public string PuntosCanjes { get; set; }
        public string PuntosGift { get; set; }
        */
        public DateTime FechaPrueba { get; set; }
        public string UsuarioPrueba { get; set; }
        public string ObservacionesGenerales { get; set; }
    }

    public class ReporteVerificacionPOSViewModel
    {
        public string Estado { get; set; }
        public string Provincia { get; set; }
        public string Localidad { get; set; }
        public string Zona { get; set; }
        public int CantidadComerciosSeleccionados { get; set; }
        public string NumeroPlanilla { get; set; }
        public string Franquicia { get; set; }
        public int IDFranquicia { get; set; }
        public string Fecha { get; set; }
    }

  
}
