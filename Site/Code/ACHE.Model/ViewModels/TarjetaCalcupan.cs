﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {

    [DelimitedRecord(";")]
    public class TarjetaCalcupan {
        public string Numero;
        public string PAN;
        public string PANFrente;
        public string Trak1;
        public string Trak2;
    }

}