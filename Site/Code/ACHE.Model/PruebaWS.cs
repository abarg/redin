﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model {
    public class PruebaWS {

        public FrontSociosInfo GetInfoConsulta(string tipo, string numero) {

            FrontSociosInfo info = new FrontSociosInfo();
            info.Info = null;
            info.Error = string.Empty;

            try {
                using (var dbContext = new ACHEEntities()) {
                    if (tipo == "DNI") {
                        var socio = dbContext.Socios.Include("Tarjetas").Where(x => x.NroDocumento == numero && x.Tarjetas.Any()).FirstOrDefault();
                        if (socio == null)
                            throw new Exception("DNI incorrecto.");
                        else {
                            info.Info = new SociosViewModel();
                            info.Info.IDSocio = socio.IDSocio;
                            info.Info.Nombre = socio.Nombre;
                            info.Info.Apellido = socio.Apellido;
                            info.Info.Email = socio.Email;
                            info.Info.NroDocumento = socio.NroDocumento;
                            info.Info.Telefono = socio.Telefono;
                            info.Info.Celular = socio.Celular;
                            info.Info.Foto = socio.Foto;
                            info.Info.Total = socio.Tarjetas.Sum(x => x.Credito + x.Giftcard);
                            if (info.Info.Total < 0)
                                info.Info.Total = 0;
                            info.Info.Puntos = socio.Tarjetas.Sum(x => x.PuntosTotales);
                            if (info.Info.Puntos < 0)
                                info.Info.Puntos = 0;

                            foreach (var tar in socio.Tarjetas.Where(x => !x.FechaBaja.HasValue).ToList())
                                info.Info.TotalAhorro += dbContext.Transacciones.Where(x => x.NumTarjetaCliente == tar.Numero && x.ImporteAhorro.HasValue).Sum(x => x.ImporteAhorro.Value);

                            if (info.Info.TotalAhorro < 0)
                                info.Info.TotalAhorro = 0;
                        }
                    }
                    else {
                        var tarjeta = dbContext.Tarjetas.Include("Socios").Where(x => x.Numero == numero && !x.FechaBaja.HasValue).FirstOrDefault();
                        if (tarjeta == null)
                            throw new Exception("Tarjeta incorrecta.");
                        else {
                            var socio = tarjeta.Socios;
                            string dom = "";

                            info.Info = new SociosViewModel();
                            if (socio != null) {
                                info.Info.IDSocio = socio.IDSocio;
                                info.Info.Nombre = socio.Nombre;
                                info.Info.Apellido = socio.Apellido;
                                info.Info.Email = socio.Email;
                                info.Info.NroDocumento = socio.NroDocumento;
                                info.Info.Telefono = socio.Telefono;
                                info.Info.Celular = socio.Celular;
                                info.Info.Foto = socio.Foto;
                                info.Info.Domicilio = "";
                            }

                            info.Info.Puntos = tarjeta.PuntosTotales;
                            if (info.Info.Puntos < 0)
                                info.Info.Puntos = 0;
                            info.Info.Total = tarjeta.Credito + tarjeta.Giftcard;
                            if (info.Info.Total < 0)
                                info.Info.Total = 0;

                            info.Info.TotalAhorro = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == numero && x.ImporteAhorro.HasValue).Sum(x => x.ImporteAhorro.Value);
                            if (info.Info.TotalAhorro < 0)
                                info.Info.TotalAhorro = 0;

                        }
                    }
                }
            }
            catch (Exception ex) {
                info.Error = ex.Message;
            }

            return info;
        }

        public FrontSociosTarjetas GetTarjetas(int idSocio) {
            FrontSociosTarjetas listado = new FrontSociosTarjetas();
            listado.Tarjetas = null;
            listado.Error = string.Empty;

            try {
                using (var dbContext = new ACHEEntities()) {
                    listado.Tarjetas = dbContext.Tarjetas.Where(x => x.IDSocio == idSocio && !x.FechaBaja.HasValue)
                        .Select(x => new TarjetasViewModel {
                            Credito = x.Credito,
                            Giftcard = x.Giftcard,
                            Puntos = x.PuntosTotales,
                            Numero = x.Numero,
                            IDTarjeta = x.IDTarjeta
                        }).ToList();

                    foreach (var tar in listado.Tarjetas)
                        tar.TotalAhorro = dbContext.Transacciones.Where(x => x.NumTarjetaCliente == tar.Numero && x.ImporteAhorro.HasValue).Sum(x => x.ImporteAhorro.Value);
                }
            }
            catch (Exception ex) {
                listado.Error = ex.Message;
            }

            return listado;
        }

        public FrontTransaccionesAction InsertTransaccionesCanje(List<TarjetasViewModel> tarjetas, int puntos) {
            FrontTransaccionesAction action = new FrontTransaccionesAction();
            action.Action = false;
            action.Error = string.Empty;
            action.Transacciones = new List<TransaccionCarga>();

            try {
                using (var dbContext = new ACHEEntities()) {
                    decimal credito = decimal.Divide(puntos, 100);
                    foreach (var tarj in tarjetas) {
                        if (puntos > 0) {
                            TransaccionCarga transaccion = new TransaccionCarga();
                            transaccion.NroTarjeta = tarj.Numero;
                            transaccion.Error = false;
                            try {
                                #region transacción
                                Transacciones tr = new Transacciones();
                                tr.FechaTransaccion = DateTime.Now;
                                tr.Origen = "Web";
                                tr.CodigoPremio = "";
                                tr.Descripcion = string.Empty;
                                tr.Descuento = 0;
                                tr.Importe = credito;
                                tr.ImporteAhorro = 0;
                                tr.NumCupon = string.Empty;
                                tr.NumEst = ConfigurationManager.AppSettings["Canje.NumEst"];
                                tr.NumReferencia = string.Empty;
                                tr.NumRefOriginal = string.Empty;
                                tr.NumTarjetaCliente = tarj.Numero;
                                tr.NumTerminal = string.Empty;
                                tr.Operacion = "Canje";
                                tr.PuntosDisponibles = "000000000000";
                                tr.TipoMensaje = "1100";
                                tr.PuntoDeVenta = string.Empty;
                                tr.NroComprobante = string.Empty;
                                tr.TipoComprobante = string.Empty;
                                tr.TipoTransaccion = "000005";
                                tr.CodigoPremio = ConfigurationManager.AppSettings["Canje.CodigoPremio"];
                                tr.UsoRed = 0;
                                tr.Puntos = 1;
                                tr.Arancel = 0;
                                tr.UsoRed = 0;
                                tr.Usuario = string.Empty;
                                tr.IDMarca = tarj.IDMarca;
                                tr.IDFranquicia = tarj.IDFranquicia;

                                if (tarj.Puntos > 0) {
                                    if (tarj.Puntos >= puntos) {
                                        tr.PuntosAContabilizar = puntos * -1;
                                        tr.PuntosIngresados = (credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                        puntos = 0;
                                    }
                                    else {
                                        tr.PuntosAContabilizar = tarj.Puntos * -1;
                                        tr.PuntosIngresados = (tarj.Credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                        puntos -= tarj.Puntos;
                                    }
                                }
                                dbContext.Transacciones.Add(tr);
                                #endregion
                            }
                            catch (Exception ex) {
                                transaccion.Error = true;
                            }
                            action.Transacciones.Add(transaccion);
                        }
                    }

                    dbContext.SaveChanges();

                    foreach (var tarj in tarjetas)
                        dbContext.ActualizarPuntosPorTarjeta(tarj.Numero);
                    action.Action = true;
                }
            }
            catch (Exception ex) {
                action.Error = ex.Message;
            }
            return action;
        }

        public FrontTransaccionesAction InsertTransaccionesCanjePin(List<TarjetasViewModel> tarjetas, int puntos) {
            FrontTransaccionesAction action = new FrontTransaccionesAction();
            action.Action = false;
            action.Error = string.Empty;
            action.Transacciones = new List<TransaccionCarga>();

            try {
                using (var dbContext = new ACHEEntities()) {
                    decimal credito = decimal.Divide(puntos, 100);
                    foreach (var tarj in tarjetas) {
                        if (puntos > 0) {
                            TransaccionCarga transaccion = new TransaccionCarga();
                            transaccion.NroTarjeta = tarj.Numero;
                            transaccion.Error = false;
                            #region transacción
                            Transacciones tr = new Transacciones();
                            tr.FechaTransaccion = DateTime.Now;
                            tr.Origen = "Web";
                            tr.CodigoPremio = "";
                            tr.Descripcion = string.Empty;
                            tr.Descuento = 0;
                            tr.Importe = credito;
                            tr.ImporteAhorro = 0;
                            tr.NumCupon = string.Empty;
                            tr.NumEst = ConfigurationManager.AppSettings["Canje.NumEst"];
                            tr.NumReferencia = string.Empty;
                            tr.NumRefOriginal = string.Empty;
                            tr.NumTarjetaCliente = tarj.Numero;
                            tr.NumTerminal = string.Empty;
                            tr.Operacion = "Canje";
                            tr.PuntosDisponibles = "000000000000";
                            tr.TipoMensaje = "1100";
                            tr.PuntoDeVenta = string.Empty;
                            tr.NroComprobante = string.Empty;
                            tr.TipoComprobante = string.Empty;
                            tr.TipoTransaccion = "000005";
                            tr.CodigoPremio = ConfigurationManager.AppSettings["Canje.CodigoPremio"];
                            tr.UsoRed = 0;
                            tr.Puntos = 1;
                            tr.Arancel = 0;
                            tr.UsoRed = 0;
                            tr.Usuario = string.Empty;
                            tr.IDMarca = tarj.IDMarca;
                            tr.IDFranquicia = tarj.IDFranquicia;

                            if (tarj.Puntos > 0) {
                                if (tarj.Puntos >= puntos) {
                                    tr.PuntosAContabilizar = puntos * -1;
                                    tr.PuntosIngresados = (credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                    puntos = 0;
                                }
                                else {
                                    tr.PuntosAContabilizar = tarj.Puntos * -1;
                                    tr.PuntosIngresados = (tarj.Credito.ToString().Replace(",", "")).PadLeft(12, '0');
                                    puntos -= tarj.Puntos;
                                }
                            }
                            dbContext.Transacciones.Add(tr);
                            #endregion
                            action.Transacciones.Add(transaccion);
                        }
                    }

                    dbContext.SaveChanges();

                    foreach (var tarj in tarjetas)
                        dbContext.ActualizarPuntosPorTarjeta(tarj.Numero);
                    action.Action = true;
                }
            }
            catch (Exception ex) {
                action.Error = ex.Message;
            }
            return action;
        }

        public FrontPinesAction GetCostosPines(int idEmpresa) {
            FrontPinesAction listado = new FrontPinesAction();
            listado.Pines = null;
            listado.Error = string.Empty;
            listado.Action = false;
            try {
                using (var dbContext = new ACHEEntities()) {
                    string sql = "SELECT DISTINCT(CostoPuntos) FROM Pines WHERE IDEmpresaCanje = " + idEmpresa + " AND IDSocio IS NULL ORDER BY CostoPuntos";
                    var pines = dbContext.Database.SqlQuery<int>(sql, new object[] { }).ToList();
                    if (pines != null && pines.Count() > 0) {
                        listado.Pines = pines;
                        listado.Action = true;
                    }
                }
            }
            catch (Exception ex) {
                listado.Error = ex.Message;
            }
            return listado;
        }

        public FrontPinInfo GetPin(int costo, int idEmpresa) {
            FrontPinInfo action = new FrontPinInfo();
            action.Error = string.Empty;
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Pines
                        .Include("EmpresasCanje")
                        .Where(x => x.CostoPuntos == costo && x.IDEmpresaCanje == idEmpresa && x.IDSocio == null)
                        .Select(x => new PinesViewModel() {
                            IDPin = x.IDPin,
                        //    Codigo = x.Codigo,
                            Control=x.Control,
                            Parte1=x.Parte1,
                            Parte2=x.Parte2,
                            EmpresaCanje = x.EmpresasCanje.Nombre,
                            CostoInterno = x.CostoInterno,
                            CostoPuntos = x.CostoPuntos
                        }).FirstOrDefault();
                    if (entity != null) {
                        action.Info = entity;
                    }
                }
            }
            catch (Exception ex) {
                action.Error = ex.Message;
            }
            return action;
        }

        public FrontPinAction AsignarSocio(int idPin, int idSocio) {
            FrontPinAction action = new FrontPinAction();
            action.Action = false;
            action.Error = string.Empty;
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Pines.Where(x => x.IDPin == idPin).FirstOrDefault();
                    if (entity != null) {
                        entity.IDSocio = idSocio;
                        entity.FechaCompra = DateTime.Now;
                    }
                    else
                        throw new Exception("No se ha encontrado el pin");
                }
            }
            catch (Exception ex) {
                action.Error = ex.Message;
            }
            return action;
        }
    }

    [Serializable]
    public class FrontSociosTarjetas {
        public string Error { get; set; }
        public List<TarjetasViewModel> Tarjetas { get; set; }
    }

    [Serializable]
    public class FrontTransaccionesAction {
        public string Error { get; set; }
        public bool Action { get; set; }
        public List<TransaccionCarga> Transacciones { get; set; }
    }

    [Serializable]
    public class TransaccionCarga {
        public string NroTarjeta { get; set; }
        public bool Error { get; set; }
    }

    [Serializable]
    public class FrontPinInfo {
        public string Error { get; set; }
        public PinesViewModel Info { get; set; }
    }

    [Serializable]
    public class FrontPinAction {
        public string Error { get; set; }
        public bool Action { get; set; }
    }

    [Serializable]
    public class FrontPinesAction {
        public string Error { get; set; }
        public bool Action { get; set; }
        public List<int> Pines { get; set; }
    }

    [Serializable]
    public class FrontSociosInfo {
        public string Error { get; set; }
        public SociosViewModel Info { get; set; }
    }
}
