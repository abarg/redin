//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class MarcasAsociadas
    {
        public int IDMarcaAsociada { get; set; }
        public int IDMultimarca { get; set; }
        public int IDMarca { get; set; }
    
        public virtual Multimarcas Multimarcas { get; set; }
        public virtual Marcas Marcas { get; set; }
    }
}
