﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Configuration;
using ACHE.Model;
using ACHE.Business;
using ACHE.Extensions;
using System.Data;
using System.IO;
using System.IO.Compression;

namespace ACHE.ImportarVISA
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                generarImportacion();
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                var dir = AppDomain.CurrentDomain.BaseDirectory + "Log\\GeneracionDat_XX.log";
                BasicLog.AppendToFile(dir, msg, e.ToString());
                //enviarEmailError(msg, "Proceso DAT: Error en Main");
                //throw e;
            }
        }

        private static void generarImportacion()
        {
            string RutaArchivo = ConfigurationManager.AppSettings["RutaArchivo"];
            string TipoArchivo = ConfigurationManager.AppSettings["TipoArchivo"];

            // string RutaArchivo = @"C:\Users\Admin\Desktop\ClubIn";
            // string TipoArchivo = "3";
            var fecha = "20190301";
            DirectoryInfo dir = new DirectoryInfo(RutaArchivo);
            FileInfo[] Files = dir.GetFiles("*.txt");
            string ruta = "";
            foreach (FileInfo file in Files)
            {
                var nombre = file.Name.Split('_');
                if (nombre[0] == "ClubIn")
                {
                    string fechaArchivo = file.Name.Substring(7, 8);

                    Console.WriteLine("Procesando archivo " + fechaArchivo);

                    //if (fechaArchivo > fecha)
                    //{
                        ruta = RutaArchivo + "/" + file.Name;
                        importarArchivos(ruta, file.Name, TipoArchivo);
                    //}
                }
            }
        }

        private static void importarArchivos(string NombreArchivo, string Nombre, string TipoArchivo)
        {

            try
            {
                if (Path.GetExtension(Nombre).Equals(".xls")
                    || Path.GetExtension(Nombre).Equals(".xlsx")
                    || Path.GetExtension(Nombre).Equals(".txt")
                    || Path.GetExtension(Nombre).Equals(".csv"))
                {

                    //Transacciones Visa
                    if (TipoArchivo.Equals("3"))
                        importarArchivoTransaccionesVisa(NombreArchivo, Nombre.Split(".txt")[0]);
                    //Transacciones Fidely
                    else if (TipoArchivo.Equals("4"))
                        importarArchivoTransaccionesFidely(NombreArchivo, Nombre.Split(".csv")[0]);
                    //Premios
                    else if (TipoArchivo.Equals("5"))
                        importarArchivoPremios(NombreArchivo);

                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ImportarVisa_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }

        }

        private static void importarArchivoTransaccionesVisa(string nombreArchivoRuta, string nombreArchivo)
        {
            bTransaccion bTransaccion;
            DataTable dt;
            StreamReader arch = null;
            string linea;
            string numEst = "";
            int longNumEst = 15;
            string numTer = "";
            int longNumTer = 8;
            string TipoMensaje = "";
            int longTipoMensaje = 4;
            string TipoTrans = "";
            int longTipoTrans = 6;
            string fecTrans = "";
            int longFecTrans = 8;
            string HorTrans = "";
            int longHorTrans = 6;
            string numCupon = "";
            int longNumCupon = 6;
            string numRef = "";
            int longNumRef = 12;
            string numRefOrg = "";
            int longNumRefOrg = 12;
            string numTarCli = "";
            int longNumTarCli = 16;
            string codPrem = "";
            int longCodPrem = 11;
            string puntIng = "";
            int longPuntIng = 12;
            string puntDis = "";
            int longPuntDis = 12;
            try
            {
                arch = new StreamReader(nombreArchivoRuta);

                //Estructura tabla
                dt = new DataTable();
                dt.Columns.Add("NumEst", typeof(string));
                dt.Columns.Add("NumTerminal");
                dt.Columns.Add("TipoMensaje");
                dt.Columns.Add("TipoTransaccion");
                dt.Columns.Add("FechaTransaccion", typeof(DateTime));
                dt.Columns.Add("NumCupon");
                dt.Columns.Add("NumReferencia");
                dt.Columns.Add("NumRefOriginal");
                dt.Columns.Add("NumTarjetaCliente");
                dt.Columns.Add("CodigoPremio");
                dt.Columns.Add("PuntosIngresados");
                dt.Columns.Add("PuntosDisponibles");
                dt.Columns.Add("NombreArchivo");
                dt.Columns.Add("IDUsuario", typeof(int));
                dt.Columns.Add("Origen");
                dt.Columns.Add("Operacion");
                dt.Columns.Add("Importe");
                dt.Columns.Add("ImporteAhorro");
                dt.Columns.Add("Descuento", typeof(int));
                dt.Columns.Add("PuntosAContabilizar", typeof(int));
                //dt.Columns.Add("IDMarca", typeof(int));


                string[] vlineas;
                while ((linea = arch.ReadLine()) != null)
                {
                    vlineas = linea.Split(';');

                    numEst = vlineas[0];
                    numEst = numEst.PadLeft(longNumEst, ' ');

                    numTer = vlineas[1];
                    numTer = numTer.PadLeft(longNumTer, ' ');

                    TipoMensaje = vlineas[2];
                    TipoMensaje = TipoMensaje.PadLeft(longTipoMensaje, ' ');

                    TipoTrans = vlineas[3];
                    TipoTrans = TipoTrans.PadLeft(longTipoTrans, ' ');

                    fecTrans = vlineas[4];
                    fecTrans = fecTrans.PadLeft(longFecTrans, ' ');

                    HorTrans = vlineas[5];
                    HorTrans = HorTrans.PadLeft(longHorTrans, ' ');

                    numCupon = vlineas[6];
                    numCupon = numCupon.PadLeft(longNumCupon, ' ');

                    numRef = vlineas[7];
                    numRef = numRef.PadLeft(longNumRef, ' ');

                    numRefOrg = vlineas[8];
                    numRefOrg = numRefOrg.PadLeft(longNumRefOrg, ' ');

                    numTarCli = vlineas[9];
                    numTarCli = numTarCli.PadLeft(longNumTarCli, ' ');

                    codPrem = vlineas[10];
                    codPrem = codPrem.PadLeft(longCodPrem, ' ');

                    puntIng = vlineas[11];
                    puntIng = puntIng.PadLeft(longPuntIng, ' ');

                    puntDis = vlineas[12];
                    puntDis = puntDis.PadLeft(longPuntDis, ' ');

                    DataRow dr = dt.NewRow();
                    dr["NumEst"] = numEst;
                    dr["NumTerminal"] = numTer;
                    dr["TipoMensaje"] = TipoMensaje;
                    dr["TipoTransaccion"] = TipoTrans;

                    DateTime dFec = DateTime.ParseExact(fecTrans, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    dFec = dFec.AddHours(Convert.ToInt32(HorTrans.Substring(0, 2)));
                    dFec = dFec.AddMinutes(Convert.ToInt32(HorTrans.Substring(2, 2)));
                    dFec = dFec.AddSeconds(Convert.ToInt32(HorTrans.Substring(4, 2)));
                    dr["FechaTransaccion"] = dFec;

                    dr["NumCupon"] = numCupon;
                    dr["NumReferencia"] = numRef;
                    dr["NumRefOriginal"] = numRefOrg;
                    dr["NumTarjetaCliente"] = numTarCli;
                    dr["CodigoPremio"] = codPrem;
                    dr["PuntosIngresados"] = puntIng;
                    dr["PuntosDisponibles"] = puntDis;
                    dr["NombreArchivo"] = nombreArchivo;
                    dr["IDUsuario"] = 0;
                    dr["Origen"] = "Visa";
                    dr["Operacion"] = "Carga";
                    dr["Descuento"] = 0;
                    dr["PuntosAContabilizar"] = 0;
                    //dr["IDMarca"] = 0;
                    dt.Rows.Add(dr);
                }

                if (dt.Rows.Count > 0)
                {
                    bTransaccion = new bTransaccion();
                    bTransaccion.limpiarTransaccionesTmp();
                    bTransaccion.insertarTransaccionesTmp(dt);
                    Console.WriteLine("Actualizando tx");
                    bTransaccion.actualizarTransacciones(nombreArchivo, 0);
                    
                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ImportarVisa_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            finally
            {
                arch.Close();
            }
        }

        private static void importarArchivoTransaccionesFidely(string nombreArchivoRuta, string nombreArchivo)
        {
            bTransaccion bTransaccion;
            DataTable dt;
            StreamReader arch = null;
            string linea;
            int i = 0;
            string fechaOperacion = "";
            string tarjeta = "";
            string operacion = "";
            string descripcion = "";
            string valorOperacion = "";
            try
            {
                arch = new StreamReader(nombreArchivoRuta);

                //Estructura tabla
                dt = new DataTable();
                dt.Columns.Add("FechaTransaccion", typeof(DateTime));
                dt.Columns.Add("Importe", typeof(Decimal));
                dt.Columns.Add("NumTarjetaCliente");
                dt.Columns.Add("Descripcion");
                dt.Columns.Add("Origen");
                dt.Columns.Add("Operacion");
                dt.Columns.Add("NombreArchivo");
                dt.Columns.Add("IDUsuario", typeof(int));

                //   Usuarios oUsu = (Usuarios)Session["CurrentUser"];

                string[] vlineas;
                while ((linea = arch.ReadLine()) != null)
                {
                    if (i != 0)
                    {
                        vlineas = linea.Split(",");

                        if (vlineas[4] != "") // Operación
                        {
                            if (vlineas[4].Contains("Carga") || vlineas[4].Contains("Descarga"))
                            {
                                if (vlineas[0] != "") fechaOperacion = CsvToTabDelimited(vlineas[0]);
                                if (vlineas[3] != "") tarjeta = CsvToTabDelimited(vlineas[3]);
                                if (vlineas[5] != "") descripcion = CsvToTabDelimited(vlineas[5]);
                                if (vlineas[6] != "") valorOperacion = CsvToTabDelimited(vlineas[6]);

                                DataRow dr = dt.NewRow();

                                if (fechaOperacion != "") dr["FechaTransaccion"] = Convert.ToDateTime(fechaOperacion);
                                Double _valorOperacion;
                                if (valorOperacion != "")
                                {
                                    valorOperacion = valorOperacion.Replace(".", ",");
                                    if (Double.TryParse(valorOperacion, out _valorOperacion))
                                        dr["Importe"] = Convert.ToDouble(_valorOperacion);
                                }
                                if (vlineas[4].Contains("Carga")) operacion = "Carga"; else operacion = "Descarga";

                                dr["NumTarjetaCliente"] = tarjeta;
                                dr["Descripcion"] = descripcion;
                                dr["Origen"] = "Fidely";
                                dr["Operacion"] = operacion;
                                dr["NombreArchivo"] = nombreArchivo;
                                dr["IDUsuario"] = 0;

                                dt.Rows.Add(dr);
                            }
                        }
                    }

                    i++;
                }

                if (dt.Rows.Count > 0)
                {
                    bTransaccion = new bTransaccion();
                    bTransaccion.limpiarTransaccionesTmp();
                    bTransaccion.insertarTransaccionesTmp(dt);
                    bTransaccion.actualizarTransacciones(nombreArchivo, 0);
                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ImportarVisa_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            finally
            {
                arch.Close();
            }
        }

        private static void importarArchivoPremios(string nombreArchivoRuta)
        {
            bPremio bPremio;
            try
            {
                string columnas = @" F3 as TipoMov, F4 as Codigo, F5 as ValorPuntos, F6 as FechaVigenciaDesde, F7 as FechaVigenciaHasta, F9 as Descripcion ";
                DataTable dt = excelToDatable(nombreArchivoRuta, "Premios", columnas);
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.RemoveAt(0);
                    bPremio = new bPremio();
                    bPremio.limpiarPremiosTmp();
                    bPremio.insertarPremiosTmp(dt, nombreArchivoRuta);
                    bPremio.actualizarPremios(false);
                }
            }
            catch (Exception ex)
            {
                var dir = AppDomain.CurrentDomain.BaseDirectory + "/log/ImportarVisa_XX.log";
                BasicLog.AppendToFile(dir, "Exception: ", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        private static DataTable excelToDatable(string filePath, string SheetName, string selectColumns)
        {
            string strHeader7 = "no"; //Yes:Trae el nombre de las columnas - No: Pone por defecto f1,f2,f3, etc a los nombres de las columnas,
            System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + strHeader7 + ";IMEX=1\"");
            System.Data.DataSet DtSet = new System.Data.DataSet();
            System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select " + selectColumns + " from [" + SheetName + "$]", MyConnection);
            MyCommand.TableMappings.Add("Table", "Table1");
            MyCommand.Fill(DtSet);
            DataTable dtExcel = DtSet.Tables[0];
            MyConnection.Close();
            return dtExcel;
        }

        public static string CsvToTabDelimited(string line)
        {
            var ret = new StringBuilder(line.Length);
            bool inQuotes = false;
            for (int idx = 0; idx < line.Length; idx++)
            {
                if (line[idx] == '"')
                {
                    inQuotes = !inQuotes;
                }
                else
                {
                    if (line[idx] == ',')
                    {
                        ret.Append(inQuotes ? ',' : '\t');
                    }
                    else
                    {
                        ret.Append(line[idx]);
                    }
                }
            }
            return ret.ToString();
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { }
            return false;
        }
    }
}
