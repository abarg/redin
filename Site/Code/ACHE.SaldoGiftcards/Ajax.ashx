﻿<%@ WebHandler Language="C#" Class="Ajax" %>

using System;
using System.Web;
using System.Linq;

public class Ajax : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");

        var cardNumber = context.Request.QueryString["cardNumber"];
        //try
        //{
            using (var dbContext = new ACHE.Model.ACHEEntities())
            {
                var aux = dbContext.Tarjetas.Where(x => x.TipoTarjeta == "G" && !x.FechaBaja.HasValue && x.Numero == cardNumber).FirstOrDefault();
                if (aux != null)
                    context.Response.Write(aux.Giftcard.ToString("N2"));
                else
                    context.Response.Write("INVALIDO");
            }
        //}
        //catch (Exception ex)
        //{
        //    context.Response.Write("Error al consultar. Intente nuevamente en unos minutos");
        //}
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}