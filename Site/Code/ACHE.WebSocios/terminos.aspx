﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="terminos.aspx.cs" Inherits="terminos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Terminos y Condiciones</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" Runat="Server">
    <div id="mobile-menu" class="mm-menu mm-offcanvas">
        <div class="mm-panel mm-opened mm-current" id="mm-1">
            <ul class="mm-listview mm-first mm-last">

                <li><a href="index.aspx"><span>Inicio</span></a></li>
                <li><a href="http://clubin.com.ar/localidad/" target="_blank"><span>Comercios Adheridos</span></a></li>
                <li><a href="atencion-cliente.aspx"><span>Atención al Cliente</span></a></li>
                <li><a href="preguntas-frecuentes.aspx"><span>Preguntas Frecuentes</span></a></li>
                <li><a href="sugerencias.aspx"><span>Sugerencias y reclamos</span></a></li>
                <li><a href="terminos.aspx" class="mm-selected"><span>Términos y Condiciones</span></a></li>
            </ul>
        </div>
    </div>
    <div id="mm-0" class="mm-page mm-slideout" style="min-height: 600px;">
        <div id="wrapper">

            <header id="header" class="">
                <nav class="navbar" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="#mobile-menu" class="navbar-toggle">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="navbar-brand" href="index.aspx">
                            <img src="/img/logosocios.png" class="img-responsive" />
                        </a>
                        <p class="nav-title">Red de beneficios</p>
                    </div>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.aspx"><img src="/img/logos_header/icon1.png" /><span><br>Inicio</span></a></li>
                            <li><a href="http://clubin.com.ar/localidad/" target="_blank"><img src="/img/logos_header/icon2.png" /><span>Comercios<br>Adheridos</span></a></li>
                            <li><a href="atencion-cliente.aspx"><img src="/img/logos_header/icon3.png" /><span>Atención<br>al Cliente</span></a></li>
                            <li><a href="preguntas-frecuentes.aspx"><img src="/img/logos_header/icon4.png" /><span>Preguntas<br>Frecuentes</span></a></li>
                            <li><a href="sugerencias.aspx"><img src="/img/logos_header/icon5.png" /><span>Sugerencias<br>y reclamos</span></a></li>
                            <li><a href="terminos.aspx"><img src="/img/logos_header/icon6.png" /><span>Términos y<br>Condiciones</span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </header>
            <!-- /header -->

            <div class='row'>
			
			    <div class='col-md-6 terminos'>

			    </div>
			    <div class='col-md-6 terminos-text'>
                    <div id='faq'>
                        <br />
                        <h1>Términos y Condiciones</h1>
                        <br />
                    </div>
                    <div class="text scroll faqfont">
			            <p style="font-size: 18px !important; font-family:'NeutraDisplayLight' !important; font-weight: bold !important;">
				        TARJETA IN es un servicio que brinda POUNAMU S.A., y que alcanza a los clientes o empleados de las empresas que contraten el servicio, y consiste esencialmente, en un sistema que brinda a los socios la oportunidad de obtener distintos descuentos, obsequios, ventajas y/o beneficios (en adelante, BENEFICIOS o PROMOCIONES) para la adquisición de bienes y/o servicios en los comercios y/o prestadores de servicios adheridos (en adelante, ESTABLECIMIENTOS ADHERIDOS) a este sistema. Con este fin, POUNAMU S.A creó una tarjeta que le permitirá a los COMERCIOS o PRESTADORES poder identificar a los adherentes, y así estos puedan acceder a los BENEFICIOS o PROMOCIONES, en los términos y condiciones aquí establecidos.
				        <br><br>
				        1) ADHESION.
				        <br>1.1. Podrá ser asociado a TARJETA IN toda persona física que resida en territorio nacional.
				        <br>1.2. Los asociados de TARJETA IN podrán adherirse la RED CLUB IN de forma gratuita manifestando su voluntad al momento de concretar la adhesión. A tal fin, es posible que se le solicite información personal adicional.
				        <br>1.3. La aceptación de los Términos y Condiciones del Programa se materializará al momento de la adhesión a TARJETA IN. Los Términos y Condiciones se aplicarán a todas las transacciones que efectúe un Socio con posterioridad a la aceptación por POUNAMU S.A. de la solicitud de adhesión escrita, telefónica o a través del Sitio, la que se exteriorizará a través de la entrega al Socio de su TARJETA IN.
				        <br>1.4. Los Socios deberán notificar a la brevedad posible a POUNAMU S.A., al teléfono que ésta informará a los Socios, por correo o por medio del Sitio, cualquier cambio de domicilio para su actualización en la base de datos del Programa.
				        <br>1.5. POUNAMU S.A., podrá razonablemente, rechazar cualquier solicitud de adhesión a la RED CLUB IN a través de la TARJETA IN.
				        <br><br>
				        2) TARJETA IN.
				        <br>2.1. La TARJETA IN es una tarjeta plástica de Socio, con numeración única e irrepetible, que lo habilita a operar en los ESTABLECIMIENTOS ADHERIDOS y permite el registro de la utilización de los Beneficios por parte del Socio en la compra o contratación que se realice en los mismos Establecimiento Adheridos.
				        <br>2.2. La TARJETA IN es personal e intransferible y sólo puede ser utilizada por la persona a cuyo nombre está extendida.
				        <br>2.3. La TARJETA IN no tiene fecha de vencimiento, por lo cual se mantendrá vigente sin un plazo específico o determinado.
				        <br>2.4. La TARJETA IN no es una tarjeta de crédito, ni de compra, ni de débito, no sirve como medio de pago y es en todo momento de propiedad de POUNAMU S.A..
				        <br>2.5. POUNAMU S.A. se reserva el derecho a solicitar la devolución de la TARJETA IN cuando, a su sólo criterio, se haga un uso indebido o inconveniente de ésta. Asimismo, POUNAMU S.A. se reserva el derecho de solicitar la devolución de la TARJETA IN por cualquier otro motivo que perjudique el normal desarrollo de la RED CLUB IN.
				        <br>2.6. Se considerarán tarjetas ilegalmente obtenidas a las que lo sean por cualquier medio o procedimiento no autorizado por POUNAMU S.A.. Estas quedarán fuera de la RED CLUB IN y no gozarán de los beneficios que la misma brinda a las TARJETA IN autorizadas. POUNAMU S.A. se reserva el derecho de iniciar las acciones legales que correspondan contra cualquier persona que intente un uso fraudulento de una TARJETA IN.
				        <br><br>
				        3) BENEFICIOS.
				        <br>3.1. Los Socios podrán acceder a beneficios en la compra de productos o contratación de servicios en los ESTABLECIMIENTOS ADHERIDOS a la RED CLUBIN los que serán anunciados en cada oportunidad.
				        <br>3.2. Los beneficios serán otorgados al Socio exclusivamente por los propietarios de los ESTABLECIMIENTOS ADHERIDOS al momento de la adquisición de un bien o servicio de dicho establecimiento. Los beneficios podrán consistir en regalos, descuentos variables en la adquisición de bienes o contratación de servicios, vendidos o provistos por los ESTABLECIMIENTOS ADHERIDOS, los cuales serán notificados oportunamente a los Socios por los medios que POUNAMU S.A. considere adecuado, (en adelante los Beneficios). En algunos casos se establecerá el límite disponible para utilizar el Beneficio por parte de los Socios, en dicho caso al comunicarse el Beneficio se informará el stock disponible.
				        <br>3.3. En ningún caso y bajo ninguna circunstancia estará permitido canjear Beneficios por dinero en efectivo.
				        <br><br>
				        4) MODO DE USO DE LA TARJETA Y ACCESO A LOS BENEFICIOS.
				        <br>4.1. EL Socio podrá acceder a los Beneficios con la sola exhibición de la Tarjeta al momento de efectuar una compra y/o contratar un servicio en los ESTABLECIMIENTOS ADHERIDOS, cualquiera fuera la forma de pago admitida por el ESTABLECIMIENTO ADHERIDO.
				        <br>4.2. El Socio sólo podrá obtener el Beneficio otorgado por cada ESTABLECIMIENTO ADHERIDO si presenta la TARJETA IN antes de la emisión de la factura correspondiente por parte del ESTABLECIMIENTO ADHERIDO por el bien adquirido o servicio contratado.
				        <br><br>
				        5) PROMOCIONES Y SORTEOS. ACCIONES PUBLICITARIAS
				        <br>5.1. En forma directa o a través de terceros POUNAMU S.A., con la frecuencia que establezca a su exclusivo criterio, podrá realizar promociones y sorteos de bienes y/o servicios, de conformidad con las Bases que se determinen en cada oportunidad.
				        <br>5.2. En los mismos no podrán participar empleados y contratados directos de POUNAMU S.A., así como tampoco sus familiares parientes por consanguinidad o afinidad en segundo grado.
				        <br>5.3. Adicionalmente, POUNAMU S.A. podrá emprender acciones publicitarias y promocionales con terceras personas ajenas o no al Programa, a fin de acercarle al Socio diversa información, ofertas y beneficios que pueden resultar de su interés.
				        <br>5.4. En caso de entrega de los beneficios por parte del Programa, la misma estará sujeta a disponibilidad de stock. Ninguno de los beneficios podrá ser canjeado por dinero en efectivo.
				        <br><br>
				        6) CANCELACION.
				        <br>6.1. POUNAMU S.A. podrá cancelar o finalizar cualquier adhesión sin aviso previo, incluyendo, entre otros, los casos en que el Socio Titular no cumpliera con los Términos y Condiciones, abusara de cualquier privilegio concedido bajo la RED CLUB IN, proveyera cualquier información falsa a POUNAMU S.A. o a cualquier punto de venta adherido de un Participante, pretendiera vender a terceros los beneficios obtenidos u obtuviere beneficios de manera indebida, contrariando los Términos y Condiciones del Programa.
				        <br>6.2. El Socio podrá solicitar la cancelación de la TARJETA IN, la que quedará automáticamente inhabilitada a partir de la fecha de aceptación de la baja por parte de POUNAMU S.A..
				        <br>6.3. Finalizada la adhesión por cualquier causa, la información relativa al Socio existente permanecerá en la base de datos de POUNAMU S.A., teniendo el mismo los derechos conferidos por la Ley 25.326.
				        <br><br>
				        7) CONDICIONES GENERALES.
				        <br>7.1. En cualquier momento, POUNAMU S.A. podrá efectuar cambios a estos Términos y Condiciones, a la denominación de la RED CLUB IN y a su logo, a los Beneficios publicados, las condiciones de asociación y la cantidad y características de los ESTABLECIMIENTOS ADHERIDOS. Las modificaciones mencionadas precedentemente, podrán ser informadas a los Socios por cualquier medio masivo de comunicación, a través del Sitio o de cualquier otro que implique su difusión pública, a elección de POUNAMU S.A..
				        <br>7.2. POUNAMU S.A. podrá efectuar cambios en cualquier momento en los Beneficios incluidos en la RED CLUB IN y en las condiciones para el acceso a dichos Beneficios y la vigencia de los mismos.
				        <br>7.3. Los datos y ofertas relativas a los Beneficios comunicados por POUNAMU S.A. revisten un carácter exclusivamente informativo y en modo alguno suponen que los establecimientos, productos y/o servicios indicados, y la calidad de los mismos, son responsabilidad de POUNAMU S.A.. La información referida a los ESTABLECIMIENTOS ADHERIDOS, incluidas sus características, marcas, logos y foto/s, es suministrada exclusivamente por cada uno de los establecimientos, por la cual POUNAMU S.A. no es responsable del contenido de dicha información.
				        <br>7.4. La información concerniente a los Socios y la referida a las transacciones que resulten en la utilización de la Tarjeta (“Información”) será almacenada y custodiada por POUNAMU S.A.. La Información estará a disposición de POUNAMU S.A. para su utilización dentro de la RED CLUB IN y en tanto resulte necesario, estará a disposición de los ESTABLECIMIENTOS ADHERIDOS autorizados de POUNAMU S.A. y con fines publicitarios y promocionales, tal como se indica en el apartado 5) de los presente Términos y Condiciones del Programa.
				        <br>La RED CLUB IN utiliza los datos para conocer los intereses y/o afinidades de los Socios de tal forma que los Beneficios se adecuen a los intereses de los Socios y para el máximo rendimiento de la RED CLUB IN
				        <br>7.5. El Socio expresamente acepta y acuerda:
				        <br>(a) Proveer la información solicitada para adherirse a la RED CLUB IN y autorizar a POUNAMU S.A. al acceso y tratamiento de la información allí contenida;
				        <br>(b) Que cada ESTABLECIMIENTO ADHERIDO revele a POUNAMU S.A. y/o a sus agentes o dependientes la información referida a las transacciones que realicen los Socios a los fines anteriormente indicados cuando fuera necesario.
				        <br>(c) Que POUNAMU S.A. trate y/o transfiera los datos contenidos en su base de datos, a sus agentes y/o a los ESTABLECIMIENTOS ADHERIDOS para los fines anteriormente indicados, en consonancia con lo dispuesto en 6.3.
				        <br>(d)Que POUNAMU S.A. trate y/o transfiera los datos a los ESTABLECIMIENTOS ADHERIDOS, a fin de que los mismos le envíen al Socio diversa información, ofertas y beneficios que pueden resultar de su interés.
				        <br>(e)Que frente a un reclamo de un Socio, POUNAMU S.A. utilice la información obrante en su base de datos.
				        <br>7.6. POUNAMU S.A. podrá terminar o suspender la RED CLUB IN en cualquier momento, notificando dicha decisión con un mínimo de treinta (30) días de anticipación a la fecha de terminación o en su caso de suspensión, por cualquier medio masivo de comunicación.
				        <br>7.7. Los ESTABLECIMIENTOS ADHERIDOS no tienen la autoridad, expresa o implícita, para formular ninguna declaración, manifestación ni ofrecer garantías en nombre de POUNAMU S.A. o de la RED CLUB IN y ni POUNAMU S.A. ni la RED CLUB IN asumen ninguna responsabilidad en relación a tales declaraciones, manifestaciones o garantías, ni tienen responsabilidad alguna por los productos y/o servicios brindados por los ESTABLECIMIENTOS ADHERIDOS.
				        <br>7.8. Cualquier comunicación cursada por POUNAMU S.A. a un Socio se considerará notificada si fue remitida al domicilio del mismo obrante en los registros de Socios de la base de datos la RED CLUB IN
				        <br>7.9. El Cliente, al adherirse la RED CLUB IN, brinda su conformidad y autoriza a POUNAMU S.A. a enviarle y trasmitirle todo tipo de comunicaciones, avisos y mensajes que guarden relación con la RED CLUB IN y con los fines publicitarios y promocionales a los domicilios mencionados precedentemente, como así también a las direcciones de e-mail y teléfonos, que figuren en los registros de Socios. El Socio podrá revocar dicha autorización manifestando por escrito al domicilio de POUNAMU S.A. o por teléfono al 0800 999 4646 su expreso deseo de no recibir aquellas comunicaciones.
				        <br>7.10. La RED CLUB IN será válido únicamente en los puntos de venta de los ESTABLECIMIENTOS ADHERIDOS. La RED CLUB IN podrá ser extendida a otros países.
				        <br>7.11. POUNAMU S.A. tiene su domicilio comercial en Avellaneda 1432, PB OF.2ª, en la ciudad de Mar del Plata, Provincia de Buenos Aires, Argentina.
				        <br>7.12. Cualquier exclusión o limitación de responsabilidad contenida en el presente en favor de POUNAMU S.A. se extiende a cada uno de sus socios y sus personas jurídicas vinculadas. El término “persona jurídica vinculada” tiene el significado dado por la Ley Nº 19.550 de Sociedades Comerciales.
				        <br>7.13. En ningún caso POUNAMU S.A. será responsable, por la utilización indebida que pudieran hacer terceros de la TARJETA IN, ni por los daños y perjuicios que tal circunstancia pudiera ocasionar. En este sentido no responderá en caso de robo, hurto, pérdida o extravío de la TARJETA IN, ni ningún uso por extraños empleando impropiamente las Tarjetas, o en cualquier otra que contraríe la voluntad de los Socios.
				        <br>7.14. El robo, hurto, extracción, pérdida o deterioro sustancial de la TARJETA IN deberá ser denunciado, de manera inmediata por el Socio al teléfono 0800 999 4646 o en el lugar donde informe oportunamente POUNAMU S.A. La responsabilidad de POUNAMU S.A. se limitará a la reposición de la TARJETA IN robada, hurtada, perdida o deteriorada dentro de los cuarenta y cinco (45) días de efectuada la denuncia. POUNAMU S.A. no se responsabiliza por demoras por causas no imputables a POUNAMU S.A. en el reemplazo de una TARJETA IN o por el uso fraudulento de la misma.
				        <br>7.15. La eventual nulidad de alguna de las cláusulas de los Términos y Condiciones no importará la nulidad de las restantes cláusulas.
				        <br>7.16. Cualquier impuesto, tasa, derecho, contribución u obligación aplicable como consecuencia de la participación de un Socio en la RED CLUB IN estará a cargo del Socio.
				        <br>7.17. POUNAMU S.A. se reserva el derecho de variar los términos y condiciones de este Reglamento, como así también los Beneficios, en tanto fuera necesario, durante el desarrollo de la RED CLUB IN
				        <br>7.18. La participación en la RED CLUB IN implica la aceptación de todas las condiciones estipuladas en los presentes Términos y Condiciones, los que se reputan conocidos por los Socios.
				        <br>7.19. Cualquier cuestión que se suscite con el Socio en relación a la RED CLUB IN, será resuelto en forma definitiva e inapelable por POUNAMU S.A..
				        <br>7.20. El Socio y POUNAMU S.A., acuerdan someter cualquier disputa o divergencia derivada de los presentes Términos y Condiciones de la RED CLUB IN a la jurisdicción y competencia de los Tribunales Ordinarios en lo Comercial de la Provincia de Buenos Aires.
                        </p>
                    </div>
                    <br />		  
		        </div>
      
                <div class='col-md-6 terminos hide'>

                </div>
                <div class="table-like"></div>
		    </div>
            
            <UC:Logos runat="server" id="ucLogos"></UC:Logos>
            <!--end footer-->
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContainer" Runat="Server">
</asp:Content>

