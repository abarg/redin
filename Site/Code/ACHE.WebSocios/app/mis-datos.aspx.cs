﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;

public partial class mis_datos : PaginaSociosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            this.cargarDatosUsuario();
    }

    [WebMethod(true)]
    public static void grabar(string email)
    {
        if (HttpContext.Current.Session["CurrentSociosUser"] != null)
        {
            var user = (WebSociosUser)HttpContext.Current.Session["CurrentSociosUser"];

            var sociosSoapClient = new wsSocios.sociosSoapClient();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
            wsSocios.FrontSociosAction action = sociosSoapClient.UpdateInfo(userCredentials, user.IDSocio, email, "");
            if (!action.Action)
                throw new Exception(action.Error);
            else
            {
                user.Email = email;
                HttpContext.Current.Session["CurrentSociosUser"] = user;
            }
        }
    }

    private void cargarDatosUsuario()
    {
        WebSociosUser user = CurrentSociosUser;
        //this.txtName.Text = user.Nombre;
        //this.txtLastname.Text = user.Apellido;
        this.litUsuario.Text = user.Nombre;
        this.litDNI.Text = user.NroDocumento;
        this.litTel.Text = user.Telefono;
        this.litCel.Text = user.Celular;
        this.litDom.Text = user.Domicilio;
        this.txtEmail.Text = user.Email;
    }
}