﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Configuration;
using System.Collections.Specialized;
using wsSocios;

public partial class app_pines : PaginaSociosBase {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            string id = Request.QueryString["Id"];
            int idEmpresa = 0;
            if (!string.IsNullOrEmpty(id)) {
                try {
                    idEmpresa = int.Parse(id);
                    if (idEmpresa > 0) {
                        LoadInfo(idEmpresa);
                        hdnIDEmpresa.Value = idEmpresa.ToString();
                    }
                }
                catch (Exception ex) {
                    Response.Redirect("canjes.aspx");
                }
            }
            else
                Response.Redirect("canjes.aspx");
        }
    }

    private void LoadInfo(int idEmpresa) {
        var usu = CurrentSociosUser;
        if (usu != null) {
            var sociosSoapClient = new wsSocios.ws();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
            int idSocio = usu.IDSocio;
            wsSocios.FrontSociosInfo info = new wsSocios.FrontSociosInfo();
            if (usu.NroDocumento != "00")
                info = wsSocios.GetInfoConsulta(userCredentials, "DNI", usu.NroDocumento);
            else
                info = sociosSoapClient.GetInfoConsulta(userCredentials, "Tarjeta", usu.NroLogin);
            //PruebaWS ws = new PruebaWS();
            var action = sociosSoapClient.GetCostosPines(userCredentials, idEmpresa);
            if (action.Action) {
                int maxPuntos = info.Info.Puntos;
                List<int> maxPines = new List<int>();
                foreach (var pin in action.Pines) {
                    if (pin <= maxPuntos)
                        maxPines.Add(pin);
                }
                cmbPines.DataSource = maxPines;
                cmbPines.DataBind();
            }
            else {
                if (!string.IsNullOrEmpty(action.Error))
                    showError("Hubo un error obteniendo los pines disponibles");
                else
                    showError("No se han encontrado pines disponibles para canjear de esta empresa");

                pnlFormulario.Visible = false;
                lnkVolver.Visible = true;
            }
        }
        else
            throw new Exception("No se ha podido obtener sus datos, por favor intente nuevamente");
    }

    protected void btnCanjear_Click(object sender, EventArgs e) {
        int costo = int.Parse(cmbPines.SelectedValue);
        decimal puntos = decimal.Parse(cmbPines.SelectedValue);
        var credito = decimal.Divide(puntos, 100);
        try {
            CanjearPin(credito, costo);
        }
        catch (Exception ex) {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            showError(msg);
        }
    }

    private bool CanjearPin(decimal credito, int puntos) {
        bool result = false;
        int idEmpresa = int.Parse(hdnIDEmpresa.Value);
        int idSocio = CurrentSociosUser.IDSocio;
        var sociosSoapClient = new wsSocios.sociosSoapClient();
        var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
        //PruebaWS ws = new PruebaWS();
        var pin = sociosSoapClient.GetPin(userCredentials, puntos, idEmpresa);
        if (string.IsNullOrEmpty(pin.Error)) {
            //si no hay error obteniendo pin
            if (pin.Info != null) {
                //si pin no está vacío
                //obtengo tarjetas
                var tarjetas = sociosSoapClient.GetTarjetas(userCredentials, idSocio);
                var tarjetasOrden = tarjetas.Tarjetas.OrderByDescending(x => x.Credito).ToArray();
                //creo transacciones
                var action = sociosSoapClient.InsertTransaccionesCanjePin(userCredentials, tarjetasOrden, puntos);
                if (action.Action) {
                    //si no hubo error creando transacciones, asigno usuario a pin
                    var action2 = sociosSoapClient.AsignarSocio(userCredentials, pin.Info.IDPin, idSocio);
                    divError.Visible = false;
                    divOk.Visible = true;
                    divError.InnerText = string.Empty;
                    divOk.InnerText = "Se ha cargado realizado el canje correctamente!";
                    foreach (var transaccion in action.Transacciones) {
                        //si hubo algún error creando transacción, mando mail
                        if (transaccion.Error) {
                            var usu = CurrentSociosUser;
                            ListDictionary datos = new ListDictionary();
                            datos.Add("<DOCUSUARIO>", CurrentSociosUser.NroDocumento);
                            datos.Add("<NOMBREUSUARIO>", CurrentSociosUser.Nombre);
                            datos.Add("<NROTARJETA>", transaccion.NroTarjeta);
                            datos.Add("<FECHA>", DateTime.Now);
                            EmailHelper.SendMessage(EmailTemplate.ErrorCanjePin, datos, ConfigurationManager.AppSettings["Email.To"], "Canjes: Error en canje de pin");
                        }
                    }
                }
                else
                    throw new Exception("Hubo un error almacenando la transacción, por favor intente nuevamente");
            }
        }
        else
            throw new Exception("Hubo un error obteniendo el pin, por favor intente nuevamente");

        return result;
    }

    private void showError(string msg) {
        divOk.Visible = false;
        divError.Visible = true;
        divOk.InnerText = string.Empty;
        divError.InnerText = msg;
    }

    //private bool VerificarCantidadPuntos(decimal credito) {
    //    bool result = false;
    //    try {
    //        var usu = CurrentSociosUser;
    //        if (usu != null) {
    //            int idSocio = usu.IDSocio;
    //            var sociosSoapClient = new wsSocios.sociosSoapClient();
    //            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
    //            wsSocios.FrontSociosInfo info = new wsSocios.FrontSociosInfo();
    //            if (usu.NroDocumento != "00")
    //                info = sociosSoapClient.GetInfoConsulta(userCredentials, "DNI", usu.NroDocumento);
    //            else
    //                info = sociosSoapClient.GetInfoConsulta(userCredentials, "Tarjeta", usu.NroLogin);

    //            //PruebaWS ws = new PruebaWS();
    //            //var info = ws.GetInfoConsulta("DNI", usu.NroDocumento);
    //            if (info != null && string.IsNullOrEmpty(info.Error)) {
    //                if (info.Info.Total < credito)
    //                    throw new Exception("Usted no tiene suficiente crédito para realizar el canje por ese pin");
    //                else
    //                    result = true;
    //            }
    //            else
    //                throw new Exception("No se ha podido obtener sus tarjetas, por favor intente nuevamente");
    //        }
    //        else
    //            throw new Exception("No se ha podido obtener sus datos, por favor intente nuevamente");
    //    }
    //    catch (Exception ex) {
    //        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
    //        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
    //        showError(msg);
    //    }
    //    return result;
    //}
}