﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .v-heading {
            font-size: 18px;
        }

        .main_content li {
            line-height: 32px;
        }

        .vcard-item, .item-key {
            font-size: 14px;
        }

        .item-key {
            width: 150px;
        }

        .logo_home {
            /* vertical-align: top; */
            /* clear: both; */
            position: relative;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <%--<h1 class="heading">Bienvenido</h1>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>--%>

            <form runat="server" id="formEdicion" class="form-horizontal" role="form">
                <div class="col-sm-3 col-md-3">
                    <br />
                    <br />
                    <div class="vcard">

                        <asp:Image runat="server" ID="imgFoto" Style="max-width: 300px" />
                        <br /><br />
                        <h3>Gracias a Red IN ahorraste $ <asp:Literal runat="server" ID="litAhorro"></asp:Literal></h3>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4" style="margin-left: -30px;">
                    <br />
                    <br />
                    
                    <div class="vcard">

                        <ul style="margin: 0;">
                            <li class="v-heading" style="border-bottom: 0px; margin-left:0px">Bienvenido <br /><asp:Literal runat="server" ID="litNombre"></asp:Literal></li>
                            <%--<li>
                                <span class="item-key">Nombre y Apellido</span><br />
                                <div class="vcard-item" style="margin-left:0"><asp:Literal runat="server" ID="litNombre2"></asp:Literal></div>
                            </li>--%>
                            <li>
                                <span class="item-key">DNI</span>
                                <div class="vcard-item"><asp:Literal runat="server" ID="litDNI"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">Domicilio</span><br />
                                <div class="vcard-item" style="margin-left:0"><asp:Literal runat="server" ID="litDom"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">Teléfono</span>
                                <div class="vcard-item"><asp:Literal runat="server" ID="litTel"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">Celular</span>
                                <div class="vcard-item"><asp:Literal runat="server" ID="litCel"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">Email</span>
                                <div class="vcard-item"><asp:Literal runat="server" ID="litEmail"></asp:Literal></div>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="col-sm-4 col-md-4" style="margin-left: -30px;">
                    <br />
                    <br />
                    
                    <div class="vcard">

                        <ul style="margin: 0;">
                            <li class="v-heading" id="liSocia" style="font-size: 20px;">Usted tiene $ <asp:Literal runat="server" ID="litTotal"></asp:Literal> <br />disponibles para utilizar</li>
                        
                            <li>
                                <span class="item-key">Puntos</span>
                                <div class="vcard-item"><asp:Literal runat="server" ID="litPuntos"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">Crédito</span>
                                <div class="vcard-item">$ <asp:Literal runat="server" ID="litCredito"></asp:Literal></div>
                            </li>
                            <li>
                                <span class="item-key">TOTAL Giftcard</span>
                                <div class="vcard-item">$ <asp:Literal runat="server" ID="litGiftcard"></asp:Literal></div>

                                <ul><asp:Literal runat="server" ID="litDetalleGiftcard"></asp:Literal></ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-3"></div>
        <div class="col-sm-8 col-md-8">
            <br /><br />
            Para modificar sus datos, por favor comuníquese con atención al cliente: 0800-220-4646 o envíe un mail a <a href="mailto:socios@redin.com.ar">socios@redin.com.ar</a>
        </div>
    </div>
</asp:Content>

