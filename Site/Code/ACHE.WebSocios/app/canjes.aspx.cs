﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;

public partial class app_canjes : PaginaSociosBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadInfo();
    }

    private void LoadInfo()
    {

        var sociosSoapClient = new wsSocios.sociosSoapClient();
        var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
        wsSocios.FrontEmpresasCanje canjes = sociosSoapClient.EmpresasCanje(userCredentials);

        rptCanjes.DataSource = canjes.Empresas;
        rptCanjes.DataBind();
    }
}