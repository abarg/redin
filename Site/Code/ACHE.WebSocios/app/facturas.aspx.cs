﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.IO;
using System.Web.Services;

public partial class app_facturas : PaginaSociosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CargarFacturas();
    }

    private void CargarFacturas() {
        using (var dbContext = new ACHEEntities()) {
            if (HttpContext.Current.Session["CurrentSociosUser"] != null) {
                var usu = (WebSociosUser)HttpContext.Current.Session["CurrentSociosUser"];
                int idSocio = usu.IDSocio;

                var result = dbContext.Facturas
                    .Where(x => x.FechaCAE.HasValue && x.Visible && x.Modo=="S" && x.IDSocio == idSocio)
                    .Select(x => new {
                        ID = x.IDFactura,
                        //Comercio = x.Comercios.RazonSocial,
                        NroDocumento = x.NroDocumento,
                        Numero = x.Numero,
                        //Importe = x.Tipo == "RI" ? (x.ImporteTotal + (x.ImporteTotal * 0.21M)) : x.ImporteTotal,
                        Importe = x.ImporteTotal,
                        //Importe = x.ImporteTotal + (x.ImporteToal * 0.21M),
                        FechaEmision = x.PeriodoDesde,
                        FechaVto = x.PeriodoHasta,
                        CondicionIva = x.Tipo
                    }).OrderByDescending(x => x.FechaEmision).ToList();

                /*FacturasFrontViewModel list = new FacturasFrontViewModel();
                foreach (var fc in result)
                {
                    var aux = fc;
                    if (aux.CondicionIva == "RI")
                        aux.Importe += aux.Importe * 0.21M;
                }*/

                rptFacturas.DataSource = result;
                rptFacturas.DataBind();

                if (!result.Any())
                    trSinFacturas.Visible = true;
            }
        }
    }

    //[WebMethod(true)]
    //public static void Descargar(int id) {
    //    using (var dbContext = new ACHEEntities()) {
    //        var factura = dbContext.Facturas.Include("Socios").Where(x => x.IDFactura == id).FirstOrDefault();
    //        if (factura != null) {
    //            string patch = HttpContext.Current.Server.MapPath("~/files/facturas/" + factura.Numero + ".pdf");
    //            ACHE.Business.Common.CrearLiqProducto(factura.Numero, factura.FechaProceso.Value, factura.Socios, factura.ImporteTotal, factura.TotalIva, patch);
    //        }
    //        else throw new Exception("La factura no existe");
    //    }
    //}
}