﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Configuration;
using System.Collections.Specialized;

public partial class app_cargar : PaginaSociosBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!VerificarCantidadPuntos(10))
            {
                pnlFormulario.Visible = false;
                lnkVolver.Visible = true;
            }
        }
    }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        if (txtArea.Text != string.Empty && txtTelefono.Text.Trim() != string.Empty && cmbCantidad.SelectedValue != "" && cmbEmpresa.SelectedValue != "")
        {
            string numero = txtArea.Text.Trim() + txtTelefono.Text.Trim();
            string empresa = cmbEmpresa.SelectedValue.ToLower();
            int cantidad = int.Parse(cmbCantidad.SelectedValue);
            int idSocio = CurrentSociosUser.IDSocio;
            try
            {
                CargarCredito(empresa, numero, cantidad, idSocio);
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                showError("Error al cargar el credito: " + msg);
            }
        }
    }

    private void CargarCredito(string empresa, string numero, int cantidad, int idSocio)
    {
        CargaVirtual aux = new CargaVirtual();
        bool carga = aux.Cargar(empresa, numero, cantidad.ToString(), "12345567890", "1");
        if (carga)
        {
            var sociosSoapClient = new wsSocios.sociosSoapClient();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
            wsSocios.FrontSociosTarjetas tarjetas = sociosSoapClient.GetTarjetas(userCredentials, idSocio);
            //PruebaWS ws = new PruebaWS();
            //var tarjetas = ws.GetTarjetas(idSocio);
            var tarjetasOrden = tarjetas.Tarjetas.OrderByDescending(x => x.Credito).ToArray();
            int puntos = cantidad * 100;
            var action = sociosSoapClient.InsertTransaccionesCanje(userCredentials, tarjetasOrden, puntos);
            if (action.Action)
            {
                txtTelefono.Text = cmbEmpresa.SelectedValue = cmbCantidad.SelectedValue = string.Empty;
                divError.Visible = false;
                divOk.Visible = true;
                divError.InnerText = string.Empty;
                divOk.InnerText = "Se ha cargado crédito correctamente!";

                foreach (var transaccion in action.Transacciones)
                {
                    if (transaccion.Error)
                    {
                        var usu = CurrentSociosUser;
                        ListDictionary datos = new ListDictionary();
                        datos.Add("<DOCUSUARIO>", CurrentSociosUser.NroDocumento);
                        datos.Add("<NOMBREUSUARIO>", CurrentSociosUser.Nombre);
                        datos.Add("<NROTARJETA>", transaccion.NroTarjeta);
                        datos.Add("<FECHA>", DateTime.Now);
                        EmailHelper.SendMessage(EmailTemplate.ErrorCargaVirtual, datos, ConfigurationManager.AppSettings["Email.To"], "Canjes: Error en Carga Virtual");
                    }
                }
            }
            else if (!string.IsNullOrEmpty(action.Error))
                throw new Exception("Hubo un error, por favor intente nuevamente. Detalle: " + action.Error);
        }
        else
            throw new Exception(aux.Mensaje);
    }

    private void cargarOpciones(decimal max)
    {
        cmbCantidad.Items.Add(new ListItem("$10", "10"));
        if (max >= 20)
            cmbCantidad.Items.Add(new ListItem("$20", "20"));
        if (max >= 30)
            cmbCantidad.Items.Add(new ListItem("$30", "30"));
        if (max >= 40)
            cmbCantidad.Items.Add(new ListItem("$40", "40"));
        if (max >= 50)
            cmbCantidad.Items.Add(new ListItem("$50", "50"));
        if (max >= 50)
            cmbCantidad.Items.Add(new ListItem("$50", "50"));
        if (max >= 60)
            cmbCantidad.Items.Add(new ListItem("$60", "60"));
        if (max >= 70)
            cmbCantidad.Items.Add(new ListItem("$70", "70"));
        if (max >= 80)
            cmbCantidad.Items.Add(new ListItem("$80", "80"));
        if (max >= 90)
            cmbCantidad.Items.Add(new ListItem("$90", "90"));
        if (max >= 100)
            cmbCantidad.Items.Add(new ListItem("$100", "100"));
        cmbCantidad.Items.Insert(0, new ListItem("", ""));
    }

    private bool VerificarCantidadPuntos(int cantidad)
    {
        bool result = false;
        try
        {
            var usu = CurrentSociosUser;
            if (usu != null)
            {
                int idSocio = usu.IDSocio;
                var sociosSoapClient = new wsSocios.sociosSoapClient();
                var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
                wsSocios.FrontSociosInfo info = new wsSocios.FrontSociosInfo();
                if (usu.NroDocumento != "00")
                    info = sociosSoapClient.GetInfoConsulta(userCredentials, "DNI", usu.NroDocumento);
                else
                    info = sociosSoapClient.GetInfoConsulta(userCredentials, "Tarjeta", usu.NroLogin);
                if (info != null && string.IsNullOrEmpty(info.Error))
                {
                    if (info.Info.Total < cantidad)
                        throw new Exception("Usted no tiene suficiente crédito para realizar ninguna carga");
                    else
                    {
                        result = true;
                        cargarOpciones(info.Info.Total);
                    }
                }
                else
                    throw new Exception("No se ha podido obtener sus tarjetas, por favor intente nuevamente");
            }
            else
                throw new Exception("No se ha podido obtener sus datos, por favor intente nuevamente");
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            showError(msg);
        }
        return result;
    }

    private void showError(string msg)
    {
        divOk.Visible = false;
        divError.Visible = true;
        divOk.InnerText = string.Empty;
        divError.InnerText = msg;
    }
}