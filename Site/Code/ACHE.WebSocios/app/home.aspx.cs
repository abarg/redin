﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;

public partial class home : PaginaSociosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litNombre.Text = CurrentSociosUser.Nombre;

        if (CurrentSociosUser.OAuthTipo == "FB")
            imgFoto.ImageUrl = string.Format("https://graph.facebook.com/{0}/picture?type=normal", CurrentSociosUser.OAuthID);
        else
        {
            if (!string.IsNullOrEmpty(CurrentSociosUser.Foto))
                imgFoto.ImageUrl = "http://www.redin.com.ar/files/socios/" + CurrentSociosUser.Foto;

            else
                imgFoto.ImageUrl = "http://www.placehold.it/300x200/EFEFEF/AAAAAA";
        }

        //litNombre2.Text = CurrentSociosUser.Nombre;
        litDNI.Text = CurrentSociosUser.NroDocumento;
        litTel.Text = CurrentSociosUser.Telefono;
        litCel.Text = CurrentSociosUser.Celular;
        litDom.Text = CurrentSociosUser.Domicilio;
        litEmail.Text = CurrentSociosUser.Email;

        var sociosSoapClient = new wsSocios.sociosSoapClient();
        var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
        wsSocios.FrontSociosTarjetas tarjetas = sociosSoapClient.GetTarjetas(userCredentials, CurrentSociosUser.IDSocio);

        if (tarjetas.Tarjetas != null)
        {
            var puntos = tarjetas.Tarjetas.Sum(x => x.Puntos);
            if (puntos < 0)
                puntos = 0;

            var credito = tarjetas.Tarjetas.Sum(x => x.Credito);
            if (credito < 0)
                credito = 0;

            var giftcard = tarjetas.Tarjetas.Sum(x => x.Giftcard);
            if (giftcard < 0)
                giftcard = 0;

            litPuntos.Text = puntos.ToString();
            litCredito.Text = credito.ToString("N2");
            litGiftcard.Text = giftcard.ToString("N2");

            litTotal.Text = (credito + giftcard).ToString("N2");

            var html = "";
            foreach (var tarjeta in tarjetas.Tarjetas)
            {
                if (tarjeta.Giftcard > 0)
                    html = "<li>" + tarjeta.Numero + ": $" + tarjeta.Giftcard.ToString("N2") + "</li>";
            }
            litDetalleGiftcard.Text = html;
            litAhorro.Text = tarjetas.Tarjetas.Sum(x => x.TotalAhorro).ToString("N2");

            /*Puntos = x.PuntosTotales ?? 0,
            Credito = x.Credito,
            Giftcard = x.Giftcard,
            POS = Math.Round(x.Credito + x.Giftcard),
            Total = x.Credito + x.Giftcard*/
        }
    }
}