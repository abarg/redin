﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="transacciones.aspx.cs" Inherits="transacciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   <%-- <link rel="stylesheet" href="<%= ResolveUrl("~/lib/chosen/chosen.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/chosen/chosen.jquery.js") %>"></script>--%>
    <script type="text/javascript" src="<%= ResolveUrl("~/app/js/views/transacciones.js") %>"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/app/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Transacciones</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Mis Transacciones</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <form id="formTransacciones" runat="server">
                <div class="formSep col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha desde</label>
                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label><span class="f_req">*</span> Fecha hasta</label>
                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control  validDate greaterThan required" MaxLength="10" />
                        </div>
                        <div class="col-md-2">
                            <label>Operación</label>
                            <asp:DropDownList runat="server" ID="ddlOperacion" CssClass="form-control">
                                <asp:ListItem Text="Todas" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Compra" Value="Compra"></asp:ListItem>
                                <asp:ListItem Text="Anulacion" Value="Anulacion"></asp:ListItem>
                                <asp:ListItem Text="Canje" Value="Canje"></asp:ListItem>
                                <asp:ListItem Text="Giftcard" Value="Giftcard"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <label>Nro. Tarjeta</label>
                            <input type="text" id="txtTarjeta" value="" maxlength="20" class="form-control" />
                        </div>
                        <div class="col-md-2">
                            <label>Comercio</label>
                            <asp:TextBox runat="server" ID="txtComercio" CssClass="form-control" MaxLength="50" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-md-8">
                            <button class="btn" type="button" id="btnBuscar" onclick="filter();">Buscar</button>
                            <button class="btn btn-success" type="button" id="btnExportar" onclick="exportar();">Exportar a Excel</button>
                            <img alt="" src="/app/img/ajax_loader.gif" id="imgLoading" style="display:none" />
                            <a href="" id="lnkDownload" download="Transacciones" style="display:none">Descargar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div id="grid"></div>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
