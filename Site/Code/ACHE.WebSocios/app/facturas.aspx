﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="facturas.aspx.cs" Inherits="app_facturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script type="text/javascript" src="<%= ResolveUrl("~/app/js/views/facturas.js?v=" + ConfigurationManager.AppSettings["App.Version"]) %>"></script>
    <style type="text/css">
        th, td {
            text-align: center
        }
        th {
            height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/app/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Facturas</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h3 class="heading">Mis Facturas</h3>
            <p>Aquí podrás visualizar la información sobre tu facturación </p>
            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <table class="table table-condensed" id="tableFacturas">
				<thead id="headFacturas">
					<tr>
                        <th>Fecha de Emisión</th>
                        <th>Nro Factura</th> 
                        <th>Fecha de Vto.</th> 
                        <th>Importe</th>
                        <th>Ver detalle</th>                          
                        <th>Descargar</th> 
                    </tr>
				</thead>
				<tbody id="bodyFacturas">
					<asp:Repeater runat="server" ID="rptFacturas">
                        <ItemTemplate>
                            <tr>
                                <%--<td><%# Eval("NroDocumento").ToString() %></td>--%>
                                <td><%# DateTime.Parse(Eval("FechaEmision").ToString()).ToString("dd/MM/yyyy") %></td>
                                <td><%# Eval("Numero").ToString() %></td>
                                <td><%# DateTime.Parse(Eval("FechaVto").ToString()).ToString("dd/MM/yyyy") %></td>
                                <td>$ <%# Decimal.Parse(Eval("Importe").ToString()).ToString("N2") %></td>
                                <td><a href="factura-detalle.aspx?Id=<%# Eval("ID").ToString() %>"><img src='img/gCons/search.png' style='cursor:pointer;width: 24px' title='Ver detalle'/></a></td>
                                <td>
                                    <a href=# onclick="descargarPdf('<%# Eval("ID").ToString() %>','<%# Eval("Numero").ToString() %>')">
                                        <img src="img/gCons/download.png" style='cursor:pointer;width: 24px' title='Descargar'/>
                                    </a>
                                </td>
                            </tr>
                        </ItemTemplate>
					</asp:Repeater>
                    <tr runat="server" id="trSinFacturas" visible="false">
                        <td colspan="6">Aún no tienes períodos facturados</td>
                    </tr>
				</tbody>
			</table>
        </div>
    </div>

</asp:Content>

