﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="canjes.aspx.cs" Inherits="app_canjes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/js/views/canjes.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/lib/tmpl/jquery.tmpl.min.js") %>"></script>
    <style type="text/css">
        .search_page .box_view .search_item {
            height: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/marcas/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Canjes</li>
        </ul>
    </div>
    <form runat="server" id="formEdicion" class="form-horizontal" role="form">
        <div class="row search_page">
            <div class="col-sm-12 col-md-12">
                <h3 class="heading">Canjes disponibles</h3>
                <div class="search_panel clearfix box_view" id="results">
                    <asp:Repeater runat="server" ID="rptCanjes">
                        <ItemTemplate>
                            <div class="search_item clearfix" style="max-height: 112px;">
                                <a href="<%# Eval("Link") %>" class="sepV_a">
                                    <div class="thumbnail pull-left">
                                        <img alt="<%# Eval("Empresa") %>" title="<%# Eval("Empresa") %>" src="http://www.redin.com.ar<%# Eval("Logo") %>" style="width: 80px; height: 80px" />
                                    </div>
                                    <div class="search_content" style="max-height: 95px">
                                        <h4>
                                            <%# Eval("Empresa") %>
                                            <%--<a href="<%# Eval("Link") %>" style="float: right;">
                                                <img alt="canjear" title="canjear" src="/img/premios/redeem.png" />
                                            </a>--%>
                                        </h4>
                                        <p class="sepH_b item_description" style="font-size: 11px;">
                                            <%# Eval("Descripcion") %>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
