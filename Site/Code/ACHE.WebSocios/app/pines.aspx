﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="pines.aspx.cs" Inherits="app_pines" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/app/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="canjes.aspx">Catálogo</a></li>
            <li class="last">Canjear Pines</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Carga de Crédito</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" runat="server" visible="false"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" runat="server" visible="false"></div>
            <form class="form-horizontal" id="form_carga" runat="server">
                <asp:HiddenField runat="server" ID="hdnIDEmpresa" />
                <asp:Panel runat="server" ID="pnlFormulario">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Pines Disponibles</label>
                        <div class="col-sm-3">
                            <asp:DropDownList runat="server" ID="cmbPines" ClientIDMode="Static" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ID="rqvPines" ErrorMessage="Debe seleccionar una opción"
                                ControlToValidate="cmbPines" Display="Dynamic" CssClass="errorRequired" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-2">
                            <asp:Button runat="server" ID="btnCanjear" CssClass="btn btn-success" OnClick="btnCanjear_Click" Text="Canjear" CausesValidation="true" />
                        </div>
                    </div>
                </asp:Panel>
                <a href="canjes.aspx" runat="server" id="lnkVolver" visible="false">Volver</a>
            </form>
        </div>
    </div>
</asp:Content>

