﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="cargar.aspx.cs" Inherits="app_cargar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="<%= ResolveUrl("~/app/lib/smoke/themes/gebo.css") %>" />
    <script type="text/javascript" src="<%= ResolveUrl("~/app/lib/smoke/smoke.js") %>"></script>
    <style type="text/css">
        .errorRequired {
            color:red
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/app/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="canjes.aspx">Catálogo</a></li>
            <li class="last">Cargar Crédito</li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Carga de Crédito</h3>
            <div class="alert alert-danger alert-dismissable" id="divError" runat="server" visible="false"></div>
            <div class="alert alert-success alert-dismissable" id="divOk" runat="server" visible="false"></div>
            <form class="form-horizontal" id="form_carga" runat="server">
                <asp:Panel runat="server" ID="pnlFormulario">
                    <%--<div class="form-group">
                        <label class="control-label col-sm-2">Tarjeta</label>
                        <div class="col-sm-3">
                            <asp:DropDownList runat="server" ID="cmbTarjeta" CssClass="form-control"/>
                            <asp:RequiredFieldValidator runat="server" ID="rqvTarjeta" ErrorMessage="Debe seleccionar una tarjeta"
                                ControlToValidate="cmbTarjeta" Display="Dynamic" CssClass="errorRequired" />
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><span class="f_req">*</span> Teléfono</label>
                        <div class="col-sm-6">
                            <%--<div class="col-sm-2" style=" margin-top: 10px;">54-</div>--%>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtArea" CssClass="form-control" ClientIDMode="Static" placeholder="" MaxLength="3" />
                                <small>Ejemplo: 11</small>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="obligatorio"
                                ControlToValidate="txtArea" Display="Dynamic" CssClass="errorRequired" />
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" ClientIDMode="Static" />
                                <small>Ejemplo: 40999845</small>
                                <br />
                                <asp:RequiredFieldValidator runat="server" ID="rqvTelefono" ErrorMessage="obligatorio"
                                ControlToValidate="txtTelefono" Display="Dynamic" CssClass="errorRequired" />
                            </div>
                            
                            
<%--                            <asp:RegularExpressionValidator runat="server" ID="regTelefono" ErrorMessage="Debe ingresar un teléfono válido"
                                Display="Dynamic" CssClass="errorRequired" ControlToValidate="txtTelefono" ValidationExpression="^[0-9]+" />--%>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><span class="f_req">*</span> Empresa</label>
                        <div class="col-sm-3">
                            <asp:DropDownList runat="server" ID="cmbEmpresa" ClientIDMode="Static" CssClass="form-control">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="Claro" Value="Claro"></asp:ListItem>
                                <asp:ListItem Text="Movistar" Value="Movistar"></asp:ListItem>
                                <asp:ListItem Text="Nextel" Value="Nextel"></asp:ListItem>
                                <asp:ListItem Text="Personal" Value="Personal"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rqvEmpresa" ErrorMessage="Debe seleccionar una opción"
                                ControlToValidate="cmbEmpresa" Display="Dynamic" CssClass="errorRequired" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><span class="f_req">*</span> Importe</label>
                        <div class="col-sm-3">
                            <asp:DropDownList runat="server" ID="cmbCantidad" ClientIDMode="Static" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ID="rqvCantidad" ErrorMessage="Debe seleccionar una opción"
                                ControlToValidate="cmbCantidad" Display="Dynamic" CssClass="errorRequired" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-2">
                            <asp:Button runat="server" ID="btnCargar" CssClass="btn btn-success" OnClientClick="return confirmarCarga();" OnClick="btnCargar_Click" Text="Cargar" CausesValidation="true" />
                        </div>
                    </div>
                </asp:Panel>
                <a href="canjes.aspx" runat="server" id="lnkVolver" visible="false">Volver</a>
            </form>
        </div>
    </div>

   <script type="text/javascript">
       function confirmarCarga() {
           if ($("#txtArea").val() == "" || $("#txtTelefono").val() != "" || $("#cmbEmpresa").val() != "" || $("#cmbCantidad").val() != "") {
               return true;
           }
           else {
               return confirm("¿Está seguro que desea realizar una carga de " + $("#cmbCantidad option:selected").text() + " en su celular?");
           }
       }

       /*function confirmarCarga()
       {
           var msg = "¿Está seguro que desea realizar una carga de " + $("#cmbCantidad option:selected").text() + " en su celular?";
           smoke.confirm(msg, function (e) {
               if (e) {
                   return true;
               }
               else {
                   return false;
               }
           }, { ok: "Aceptar", cancel: "Cancelar" });
       }*/

       $(document).ready(function () {
           $("#txtArea,#txtTelefono").numeric();
       });
    </script>
</asp:Content>
