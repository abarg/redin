﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.UI.HtmlControls;
using ACHE.Model;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;

public partial class transacciones : PaginaSociosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.GetLastDayOfMonth().ToString("dd/MM/yyyy");
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static DataSourceResult GetListaGrilla(int take, int skip, IEnumerable<Sort> sort, Filter filter, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentSociosUser"] != null)
        {
            var usu = (WebSociosUser)HttpContext.Current.Session["CurrentSociosUser"];
            int idSocio = usu.IDSocio;

            var sociosSoapClient = new wsSocios.sociosSoapClient();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
            wsSocios.FrontSociosTransacciones transacciones = sociosSoapClient.GetTransacciones(userCredentials, idSocio, fechaDesde, fechaHasta);

            if (transacciones.Transacciones != null)
            {
                return transacciones.Transacciones.OrderByDescending(x => x.FechaTransaccion).AsQueryable().ToDataSourceResult(take, skip, sort, filter);//.ToList();
            }
            else
                return null;
        }
        else
            return null;
    }

    [System.Web.Services.WebMethod(true)]
    public static string Exportar(string fechaDesde, string fechaHasta, string tarjeta, string comercio, string operacion)
    {
        string fileName = "Transacciones";
        string path = "/app/tmp/";
        if (HttpContext.Current.Session["CurrentSociosUser"] != null)
        {
            var usu = (WebSociosUser)HttpContext.Current.Session["CurrentSociosUser"];
            int idSocio = usu.IDSocio;

            try
            {
                DataTable dt = new DataTable();
                var sociosSoapClient = new wsSocios.sociosSoapClient();
                var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
                wsSocios.FrontSociosTransacciones transacciones = sociosSoapClient.GetTransacciones(userCredentials, idSocio, fechaDesde, fechaHasta);

                if (transacciones.Transacciones != null)
                {
                    var info = transacciones.Transacciones.AsQueryable();
                    if (tarjeta != "")
                        info = info.Where(x => x.Tarjeta.ToLower().Contains(tarjeta.ToLower()));
                    if (comercio != "")
                        info = info.Where(x => x.Comercio.ToLower().Contains(comercio.ToLower()));
                    if (operacion != "")
                        info = info.Where(x => x.Operacion.ToLower().Contains(operacion.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        info = info.Where(x => x.FechaTransaccion >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta).AddDays(1);
                        info = info.Where(x => x.FechaTransaccion <= dtHasta);
                    }

                    dt = info.Select(x => new
                    {
                        Fecha = x.Fecha,
                        Hora = x.Hora,
                        Operacion = x.Operacion,
                        Comercio = x.Comercio,
                        Tarjeta = x.Tarjeta,
                        Marca = x.Marca,
                        ImporteOriginal = x.ImporteOriginal,
                        ImporteAhorro = x.ImporteAhorro,
                        Puntos = x.Puntos,
                        Credito = x.Credito,
                        Giftcard = x.Giftcard,
                        POS = x.POS,
                        Total = x.Total
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    generarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                {
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
                }
                return path + fileName + "_" + DateTime.Now.ToString("yyymmdd") + ".xlsx";
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        return "";
    }

    public static void generarArchivo(DataTable dt, string path, string fileName)
    {
        var wb = new XLWorkbook();
        wb.Worksheets.Add(dt, fileName);
        wb.SaveAs(path + "_" + DateTime.Now.ToString("yyymmdd") + ".xlsx");
    }
}