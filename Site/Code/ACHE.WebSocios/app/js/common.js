﻿/*** DATES ***/
var MONTH_NAMES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
var MONTH_NAMES_SHORT = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

/*
Date.prototype.getMonthName = function () {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};
*/

Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    var y = this.getFullYear();
    return (((y % 4 === 0) && (y % 100 !== 0)) || (y % 400 === 0));
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

function gd(year, month, day) {
    return new Date(year, month, day).getTime();
}

function convertToDate(timestamp) {
    var newDate = new Date(timestamp);
    var dateString = newDate.getMonth();
    var monthName = getMonthName(dateString);

    return monthName;
}

function configDatePicker() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:2049"
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    configFechas('dd/mm/yy');
    $(".validDate").datepicker();
}

function cleanErrors() {
    $('form').validate();
    if ($('form').valid()) {
        $('label .error').hide();
    }
}

function parseEnDate(value) {
    var bits = value.match(/([0-9]+)/gi), str;
    str = bits[1] + '/' + bits[0] + '/' + bits[2];
    return new Date(str);
}

function configFechas(format) {
    $.validator.addMethod("validDate", function (value, element) {
        /*var bits = value.match(/([0-9]+)/gi), str;
        if (!bits) return this.optional(element) || false; str = bits[1] + '/' + bits[0] + '/' + bits[2];
        return this.optional(element) || !/Invalid|NaN/.test(new Date(str));*/

        var check = false;
        var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (format == 'mm/yy')
            re = /^\d{1,2}\/\d{4}$/;
        if (re.test(value)) {
            var adata = value.split('/');
            var dd = 0;
            var mm = 0;
            var yyyy = 1900;
            if (format != 'mm/yy') {
                dd = parseInt(adata[0], 10);
                mm = parseInt(adata[1], 10);
                yyyy = parseInt(adata[2], 10);
            }
            else {
                dd = 1;
                mm = parseInt(adata[0], 10);
                yyyy = parseInt(adata[1], 10);
            }

            var xdata = new Date(yyyy, mm - 1, dd);

            if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                check = true;
            else
                check = false;
        }
        else
            check = false;
        return this.optional(element) || check;

    }, "La fecha no es válida.");
}

function configFechasDesdeHasta(date, end) {

    $.validator.addMethod("greaterThan", function () {
        var valid = true;
        var desde = $("#" + date).val();
        var hasta = $("#" + end).val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        return valid;
    }, "La fecha desde es mayor a la fecha hasta");
    $('form').validate({
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
    //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

    $('#' + date).change(cleanErrors);
    $('#' + end).change(cleanErrors);
}

function CuitEsValido(cuit) {

    if (typeof (cuit) == 'undefined')
        return true;

    cuit = cuit.toString().replace(/[-_]/g, "");
    if (cuit == '')
        return true;

    if (cuit.length != 11)
        return false;
    else {
        var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
        var total = 0;

        for (var i = 0; i < mult.length; i++) {
            total += parseInt(cuit.charAt(i)) * mult[i];
        }

        var mod = total % 11;
        var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
    }
    return digito == parseInt(cuit.charAt(10));
}

function LoadComercios(url, control) {
    $('#' + control).html("<option value=''></option>").trigger('liszt:updated');
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            
            $(data.d).each(function () {
                $("#" + control).append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $('#' + control).trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}

function LoadProveedores(url, control) {
    $('#' + control).html("<option value=''></option>").trigger('liszt:updated');
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            $(data.d).each(function () {
                $("#" + control).append($("<option></option>").attr("value", this.ID).text(this.Nombre));
            });
            $('#' + control).trigger('liszt:updated');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}