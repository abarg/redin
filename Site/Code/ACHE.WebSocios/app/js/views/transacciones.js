﻿function exportar() {
    $("#divError").hide();
    $("#lnkDownload").hide();
    $("#imgLoading").show();
    $("#btnExportar").attr("disabled", true);

    var info = "{ fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', tarjeta: '" + $("#txtTarjeta").val()
            + "', comercio: '" + $("#txtComercio").val()
            + "', operacion: '" + $("#ddlOperacion").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "transacciones.aspx/Exportar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
                $("#btnExportar").attr("disabled", false);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#divError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);
        }
    });
}

function configControls() {
    configDatePicker();
    configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    $("#txtTarjeta").numeric();

    $("#txtFechaDesde, #txtFechaHasta, #txtTarjeta, #txtComercio").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            filter();
            return false;
        }
    });

    $("#grid").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                     fields: {
                        Fecha: { type: "string" },
                        Hora: { type: "string" },
                        Operacion: { type: "string" },
                        Comercio: { type: "string" },
                        Tarjeta: { type: "string" },
                        ImporteOriginal: { type: "number" },
                        ImporteAhorro: { type: "number" },
                        Puntos: { type: "integer" },
                        Credito: { type: "number" },
                        Giftcard: { type: "number" },
                        POS: { type: "integer" },
                        Total: { type: "number" }

                    }
                }
            },
            pageSize: 50,
            batch: true,
            transport: {
                read: {
                    url: "Transacciones.aspx/GetListaGrilla", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                        return JSON.stringify({ products: data.models })
                    } else {
                        // web services need default values for every parameter
                        data = $.extend({ sort: null, filter: null, fechaDesde: $("#txtFechaDesde").val(), fechaHasta: $("#txtFechaHasta").val() }, data);

                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 500,
        //scrollable: true,
        sortable: true,
        //filterable: true,
        pageable: { input: false, numeric: true },
        columns: [
            { field: "Fecha", title: "Fecha", width: "80px" },
            { field: "Hora", title: "Hora", width: "70px" },
            { title: "Operacion", template: "#= renderOperacion(data) #", width: "80px" },
            //{ field: "Operacion", title: "Operacion", width: "80px" },
            { field: "Comercio", title: "Comercio", width: "150px" },
            { field: "Tarjeta", title: "Tarjeta", width: "140px" },
            { field: "Marca", title: "Marca", width: "100px" },
            { field: "ImporteOriginal", title: "Importe", width: "70px" },
            { field: "ImporteAhorro", title: "Ahorro", width: "70px" },
            { field: "Puntos", title: "Puntos", width: "80px" },
            { field: "Credito", title: "Crédito $", width: "80px", format: "{0:c}" },
            { field: "Giftcard", title: "Giftcard $", width: "80px", format: "{0:c}" },
            { field: "Total", title: "Total $", width: "80px", format: "{0:c}" }
            //{ field: "POS", title: "POS", width: "80px", groupFooterTemplate: "Age: #= value # total: #= sum #" },
            
        ]
        //dataBound: onDataBound,
        //dataBinding: onDataBinding
        /*
        dataBound: function () {
            alert("aa");
            $('td').each(function () { alert($(this).text());  if ($(this).text() == 'Jane') { $(this).addClass('customClass') } })
        }*/
        //group: { field: "POS", aggregates: [{ field: "POS", aggregate: "sum" }] }
    });

    //Buscador
    $('#formTransacciones').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',

        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
}

/*
function onDataBound(arg) {
    alert("Grid data bound");
}

function onDataBinding(arg) {
    alert("Grid data binding");
}

function updateGridRows() {
    dataView = this.dataSource.view();
    for (var i = 0; i < dataView.length; i++) {
        if (dataView[i].Selected == false) {
            var uid = dataView[i].uid;
            $("#grid tbody").find("tr[data-uid=" + uid + "]").addClass("customClass");
        }
    }

}*/

function renderOperacion(data) {
    var html = "<div align='center'><span class='label label-" + data.Operacion + "'>" + data.Operacion + "</span></div>";
    
    return html;
}

function filter() {
    $("#imgLoading").hide();
    $("#lnkDownload").hide();

    if ($('#formTransacciones').valid()) {
        var grid = $("#grid").data("kendoGrid");
        var $filter = new Array();

        var Tarjeta = $("#txtTarjeta").val();
        if (Tarjeta != "") {
            $filter.push({ field: "Tarjeta", operator: "contains", value: Tarjeta });
        }

        var operacion = $("#ddlOperacion").val();
        if (operacion != "") {
            $filter.push({ field: "Operacion", operator: "equal", value: operacion });
        }

        var comercio = $("#txtComercio").val();
        if (comercio != "") {
            $filter.push({ field: "Comercio", operator: "contains", value: comercio });
        }

        grid.dataSource.filter($filter);
        /*
        var data = grid.dataSource.data();
        $.each(data, function (i, row) {
            if (row.Operacion == 'Compra') {
                alert("aa");
                $('tr[data-uid="' + row.uid + '"] ').css("background-color", "green");
            }
        });
        grid.refresh();*/
    }
}

$(document).ready(function () {
    configControls();
});