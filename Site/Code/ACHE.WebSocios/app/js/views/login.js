﻿function Login() {
    if ($("#Usuario").val() == "" || $("#Password").val() == "") {
        $("#divError").html("Por favor, ingrese el usuario y contraseña");
        $("#divError").show();
        $("#imgLoading").hide();
        return false;
    }
    else {

        $("#divError").hide();
        $("#divError").hide();
        $('#login_form').validate();
        $("#imgLoading").show();

        if ($('#login_form').valid()) {
            var info = "{ nroDoc: '" + $("#Usuario").val() + "', pwd: '" + $("#Password").val() + "'}";

            $.ajax({
                type: "POST",
                url: "login.aspx/ingresar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    window.location.href = "/home.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $("#imgLoading").hide();
                }
            });
        }
        else {
            $("#imgLoading").hide();
            return false;
        }
    }
}

function Recuperar() {
    $("#divError2").hide();
    $("#imgLoadingPwd").show();
    $("#divTituloRecuperarDatos").show();
    $('#pass_form').validate

    if ($('#pass_form').valid()) {
        var info = "{ nroDoc: '" + $("#p_Email").val() + "'}";

        $.ajax({
            type: "POST",
            url: "login.aspx/RecuperarDatos",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#p_Email").val("");
                $("#lnkVolver").click();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divTituloRecuperarDatos").hide();
                $("#divError2").html(r.Message);
                $("#divError2").show();
                $("#imgLoadingPwd").hide();
            }
        });
    }
    else {
        $("#imgLoadingPwd").hide();
        return false;
    }
}

$(document).ready(function () {

    $("#Usuario, #Password").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            Login();
        }
    });

    $("#p_Email").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            Recuperar();
        }
    });


    //* boxes animation
    form_wrapper = $('.login_box');
    /*function boxHeight() {
        form_wrapper.animate({ marginTop: (-(form_wrapper.height() / 2) - 24) }, 400);
    };*/
    form_wrapper.css({ marginTop: (-(form_wrapper.height() / 2) - 24) });
    $('.linkform a,.link_reg a').on('click', function (e) {

        $("#divError").hide();
        $("#divError2").hide();
        $("#divTituloRecuperarDatos").show();

        var target = $(this).attr('href');
        target_height = $(target).actual('height');
        $(form_wrapper).css({
            'height': form_wrapper.height()
        });
        $(form_wrapper.find('form:visible')).fadeOut(400, function () {
            form_wrapper.stop().animate({
                height: target_height,
                marginTop: (-(target_height / 2) - 24)
            }, 500, function () {
                $(target).fadeIn(400);
                $('.links_btm .linkform').toggle();
                $(form_wrapper).css({
                    'height': ''
                });
            });
        });
        e.preventDefault();
    });

    //* validation login_form
    $('#login_form').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        rules: {
            Usuario: { required: true, minlength: 3 },
            Password: { required: true, minlength: 3 }
        },
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
            setTimeout(function () {
                //boxHeight()
            }, 200)
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
            setTimeout(function () {
                //boxHeight()
            }, 200)
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //* validation pass_form
    $('#pass_form').validate({
        onkeyup: false,
        errorClass: 'error',
        validClass: 'valid',
        rules: {
            p_Email: { required: true }
        },
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
            setTimeout(function () {
                //boxHeight()
            }, 200)
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
            setTimeout(function () {
                //boxHeight()
            }, 200)
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});