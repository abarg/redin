﻿function showSinResultados() {
    $("#divLoading").remove();
    $("#divSinResultados").show();
}

function SearchMore(obj) {
    var page = parseInt($("#hdnPage").val());
    page++;

    $(obj).remove();

    $("#hdnPage").val(page);
    buscar();
}

function createLoading() {
    $("#results").append("<br/><img alt='Buscando..' src='../../img/ajax_loader.gif' id='divLoading' style='margin-left:10px' />");
    //<a class='showMore' id='divLoading'>Cargando...</a>");
}

function reiniciar() {
    $("#divSinResultados").hide();
    $("#divLoading").remove();
    $("#hdnPage").val("1");
    buscar();
}

function buscar() {
    var page = parseInt($("#hdnPage").val());

    if (page == 1)//Si es la pagina 1, borro todos los resultados
        $("#results").empty();//$("#resultados").html("");

    createLoading();

    var desde = 0;
    var hasta = 0;
    if ($("#txtPuntosDesde").val() != "")
        desde = parseInt($("#txtPuntosDesde").val());
    if ($("#txtPuntosHasta").val() != "")
        hasta = parseInt($("#txtPuntosHasta").val());

    var info = "{ pageSize: " + parseInt($("#ddlPageSize").val())
        + ", page: " + page
        + ", puntosDesde: " + desde
        + ", puntosHasta: " + hasta
        + ", idRubro: " + parseInt($("#ddlRubro").val())
        + "}";

    $.ajax({
        //dataType: "jsonp",
        type: "POST",
        url: "catalogo.aspx/buscar",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //jsonp: "$callback",
        success: function (data, text) {
            // Render using the template
            if (data.d.Items.length > 0) {

                $("#resultTmpl").tmpl(data.d.Items).appendTo("#results");

                /*var html = "";// "<div id='div" + page + "' style='display:none'>";
                //html += $("#itemTemplate").tmpl(data.d).appendTo("#resultados").html();
                $.each(data.d.Items, function (num, el) {
                    html += $("#resultTmpl").tmpl(el).html();
                    alert(html);
                });
                //html += "</div>";


                $("#results").append(html);
                */
                //$("#div" + page).waitForImages(function () {
                $("#divLoading").remove();
                //$("#div" + page).show();
                if (data.d.Cantidad == 1)
                    $("#results").append("<div align='center' id='showMoreResults' onclick='SearchMore(this);'><button class='btn btn-primary btn-sm'>MOSTRAR M&Aacute;S</button></div>");
                //<a title='' >MOSTRAR M&Aacute;S</a></div>");
                /*$("#contenidos").getNiceScroll().resize();
            });*/
            }
            else {
                if (page == 1)
                    showSinResultados();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
            $("#results").empty();
            /*$("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $("#imgLoading").hide();
            $("#lnkDownload").hide();
            $("#btnExportar").attr("disabled", false);*/
        }
    });
}

function configControls() {
    $("#txtPuntosDesde, #txtPuntosHasta").numeric();
}

$(document).ready(function () {
    configControls();
});