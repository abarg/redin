﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cambiar_pwd : PaginaSociosBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            litUsuario.Text = CurrentSociosUser.NroDocumento;
    }

    [WebMethod(true)]
    public static void grabar(string pwdActual, string pwd)
    {
        if (HttpContext.Current.Session["CurrentSociosUser"] != null)
        {
            var user = (WebSociosUser)HttpContext.Current.Session["CurrentSociosUser"];

            var sociosSoapClient = new wsSocios.sociosSoapClient();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
            wsSocios.FrontSociosAction action = sociosSoapClient.UpdatePwd(userCredentials, user.IDSocio, pwdActual, pwd);
            if (!action.Action)
                throw new Exception(action.Error);
        }
    }
}