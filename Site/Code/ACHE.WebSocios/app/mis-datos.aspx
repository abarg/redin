﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/MasterPage.master" AutoEventWireup="true" CodeFile="mis-datos.aspx.cs" Inherits="mis_datos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="<%= ResolveUrl("~/app/js/views/misDatos.js") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="jCrumbs" class="breadCrumb module">
        <ul>
            <li><a href="<%= ResolveUrl("~/app/home.aspx") %>"><i class="glyphicon glyphicon-home"></i></a></li>
            <li class="last">Mis Datos</li>
        </ul>
    </div>
    
    <div class="row">
        <div class="col-sm-8 col-md-8">
            <h3 class="heading">Mis Datos</h3>

            <div class="alert alert-danger alert-dismissable" id="divError" style="display: none"></div>
            <div class="alert alert-success alert-dismissable" id="divOK" style="display: none">Los datos se han actualizado correctamente.</div>

			<form class="form-horizontal" id="form_misDatos" runat="server">
				<fieldset>
					<div class="form-group">
						<label class="control-label col-sm-2">Nombre</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<strong><asp:Literal runat="server" ID="litUsuario"></asp:Literal></strong>
							</p>
						</div>
					</div>
                    <div class="form-group">
						<label class="control-label col-sm-2">DNI</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<strong><asp:Literal runat="server" ID="litDNI"></asp:Literal></strong>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">Domicilio</label>
						<div class="col-sm-10">
							<p class="form-control-static">
                                <strong><asp:Literal runat="server" ID="litDom"></asp:Literal></strong>
                            </p>
						</div>
					</div>
                    <div class="form-group">
						<label class="control-label col-sm-2">Teléfono</label>
						<div class="col-sm-10">
							<p class="form-control-static">
                                <strong><asp:Literal runat="server" ID="litTel"></asp:Literal></strong>
                            </p>
						</div>
					</div>
                    <div class="form-group">
						<label class="control-label col-sm-2">Celular</label>
						<div class="col-sm-10">
							<p class="form-control-static">
                                <strong><asp:Literal runat="server" ID="litCel"></asp:Literal></strong>
                            </p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2"><span class="f_req">*</span> Email</label>
						<div class="col-sm-4">
							<asp:TextBox runat="server" ID="txtEmail" AutoCompleteType="Email" CssClass="form-control required email" MaxLength="128"></asp:TextBox>
						</div>
					</div>
					<%--<div class="form-group">
						<label class="control-label col-sm-2"><span class="f_req">*</span> Contraseña</label>
						<div class="col-sm-4">
							<asp:TextBox runat="server" ID="txtPassword" CssClass="form-control required" TextMode="Password" MaxLength="10" MinLength="3" />
						</div>
					</div>--%>
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-2">
							<button id="btnGrabar" class="btn btn-success" type="button" onclick="grabar();">Grabar</button>
						</div>
					</div>
                    <div class="row">
                        <div class="col-sm-3 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <br /><br />
                            Para modificar sus datos, por favor comuníquese con atención al cliente: 0800-220-4646 o envíe un mail a <a href="mailto:socios@redin.com.ar">socios@redin.com.ar</a>
                        </div>
                    </div>
				</fieldset>
			</form>
        </div>
    </div>      
    
            
</asp:Content>

