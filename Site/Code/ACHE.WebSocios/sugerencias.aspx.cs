﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Configuration;

public partial class sugerencias : System.Web.UI.Page {

    protected void Page_Load(object sender, EventArgs e) {

    }

    [System.Web.Services.WebMethod]
    public static void EnviarReclamo(string motivo, int consulta, string credencial, string nombre, string apellido, string tipoDoc, string documento, string telefono,
        string email, string fechaCompra, string horaCompra, string nombreComercio, string domicilioComercio, string importeCompra, string motivoConsulta,
        string sugerencias, string observaciones) {

        using (var dbContext = new ACHEEntities()) {
            ListDictionary datosReclamo = new ListDictionary();
            string html = string.Empty;

            html += "Se ha recibido una nueva consulta o reclamo desde Web Socios, aqui están los datos: <br /><br />Credencial: " + credencial + "<br />Nombre: " + apellido + ", " + nombre + "<br />Tipo de Documento: " + tipoDoc + "<br />Documento: " + documento + "<br />Teléfono: " + telefono + "<br />Email: " + email;


            if (motivo == "b") {
                html += "<br /><br />Motivo: Beneficios y Puntos";
                if (consulta == 1 || consulta == 2) {
                    if (consulta == 1)
                        html += "<br />Consulta: No me otorgaron el descuento";
                    else if (consulta == 2)
                        html += "<br />Consulta: No me otorgaron los puntos";

                    html += "<br />Fecha de la compra: " + fechaCompra + "<br /> Hora de la compra: " + horaCompra + "<br /> Nombre del comercio:" + nombreComercio + "<br /> Domicilio del comercio: " + domicilioComercio + "<br /> Importe de la compra: " + importeCompra + "<br /> Observaciones: " + observaciones;
                }
                else if (consulta == 3) {
                    html += "<br />Consulta: No existe más el comercio<br /> Nombre del comercio:" + nombreComercio + "<br /> Domicilio del comercio: " + domicilioComercio;
                }
                else if (consulta == 4) {
                    html += "<br />Consulta: Otros<br /> Motivo de la consulta:" + motivoConsulta + "<br /> Observaciones: " + observaciones;
                }
                else if (consulta == 5) {
                    html += "<br />Consulta: Dejanos tus sugerencias<br /> Sugerencias:" + sugerencias;
                }
            }
            else if (motivo == "t") {
                html += "<br /><br />Motivo: Tarjetas";
                switch (consulta) {
                    case 1:
                        html += "<br />Consulta: Perdida y/o Robo";
                        break;
                    case 2:
                        html += "<br />Consulta: Desmagnetizada";
                        break;
                    case 3:
                        html += "<br />Consulta: Datos Erroneos";
                        break;
                    case 4:
                        html += "<br />Consulta: No consigo la tarjeta";
                        break;
                }
                html += "<br /> Observaciones: " + observaciones;
            }

            html += "<br /><br /><b>Muchas Gracias, RedIN.</b>";

            datosReclamo.Add("<HTML>", html);

            bool send = EmailHelper.SendMessage(EmailTemplate.SugerenciaReclamo, datosReclamo, ConfigurationManager.AppSettings["Email.To"], "Web Socios: Sugerencia o Reclamo");
            if (!send)
                throw new Exception("Hubo un error enviando los datos, por favor intente nuevamente");
        }
    }
}