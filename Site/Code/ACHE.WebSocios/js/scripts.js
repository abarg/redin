$(document).ready(function() {
  
    !function ($) {
    $(function(){
      // carousel demo
      $('#myCarousel').carousel()
    })
  }(window.jQuery)

  //console.log('ready!');
  $("#mobile-menu").mmenu({
     // Options
  });
  var API = $("#mobile-menu").data( "mmenu" );

  $(".navbar-toggle").click(function() {
     API.open();
  });
  $('#scrollbar_container').tinyscrollbar();

  $('#txtConsultaNro,#Usuario, #Password').poshytip({
      className: 'tip-yellowsimple',
      showOn: 'focus',
      alignTo: 'target',
      alignX: 'center',
      alignY: 'bottom',
      offsetX: 0,
      offsetY: 5,
      showTimeout: 100,
      timeOnScreen: 4000
  });
  
});