﻿$(document).ready(function () {
    $("#cmbTajeta").hide();
    $("#divDatos2, #divDatos3, #divDatos4, #divDatos5").hide();

    chequearMotivo();

    $("input[name='motivo']").click(function () {
        chequearMotivo();
    });

    $("input[name='consulta']").change(function () {
        chequearConsulta();
    });

});

function chequearMotivo() {
    var motivo = $("input[name='motivo']:checked").val();

    if (motivo == "b") {
        $("#cmbTajeta").hide();
        $("#divDatos5").hide();
        $("#cmbBeneficios").show();
        chequearConsulta();
    }

    if (motivo == "t") {
        $("#cmbBeneficios").hide();
        $("#divDatos1, #divDatos2, #divDatos3, #divDatos4").hide();
        $("#cmbTajeta").show();
        $("#divDatos5").show();
    }
}

function chequearConsulta() {
    $("#divDatos1, #divDatos2, #divDatos3, #divDatos4, #divDatos5").hide();
    var motivo = $("input[name='motivo']:checked").val();
    var consulta = parseInt($(".consulta option:selected").val());
    switch (motivo) {
        case "b":
            $("#divDatos5").hide();
            switch (consulta) {
                case 1:
                case 2:
                    $("#divDatos1").show();
                    break;
                case 3:
                    $("#divDatos2").show();
                    break;
                case 4:
                    $("#divDatos3").show();
                    break;
                case 5:
                    $("#divDatos4").show();
                    break;
            }
            break;

        case "t":
            $("#divDatos1, #divDatos2, #divDatos3, #divDatos4").hide();
            $("#divDatos5").show();
            break;
    }
}

function enviarReclamo() {
    var isValid = true;
    var validEmail = true;

    var motivo = $("input[name='motivo']:checked").val();
    var consulta = parseInt($(".consulta option:selected").val());

    var credencial = $("#txtCredencial").val();
    var nombre = $("#txtNombre").val();
    var apellido = $("#txtApellido").val();
    var tipoDocumento = $("#cmbTipoDocumento").val();
    var documento = $("#txtDocumento").val();
    var telefono = $("#txtTelefono").val();
    var email = $("#txtEmail").val();

    if (credencial == "")
        isValid = false;
    if (nombre == "")
        isValid = false;
    else if (apellido == "")
        isValid = false;
    else if (documento == "")
        isValid = false;
    else if (telefono == "")
        isValid = false;
    else if (email == "")
        isValid = false;
    else if (!IsValidEmail(email)) {
        isValid = false;
        validEmail = false;
    }

    var info = "{ "
        + "motivo: '" + motivo
        + "', consulta: " + consulta
        + ", credencial: '" + credencial
        + "', nombre: '" + nombre
        + "', apellido: '" + apellido
        + "', tipoDoc: '" + tipoDocumento
        + "', documento: '" + documento
        + "', telefono: '" + telefono
        + "', email: '" + email + "'";

    var fechaCompra = "";
    var horaCompra = "";
    var nombreComercio = "";
    var domicilioComercio = "";
    var importeCompra = 0;
    var observaciones = "";
    var motivoConsulta = "";
    var sugerencias = "";

    if (motivo == "b") {

        if (consulta == 1 || consulta == 2) {
            fechaCompra = $("#txtFecha1").val();
            horaCompra = $("#cmbHora1").val();
            nombreComercio = $("#txtNombre1").val();
            domicilioComercio = $("#txtDomicilio1").val();
            importeCompra = $("#txtImporte1").val();
            observaciones = $("#txtObservaciones1").val();
            if (fechaCompra == "")
                isValid = false;
            else if (horaCompra == "")
                isValid = false;
            else if (nombreComercio == "")
                isValid = false;
            else if (domicilioComercio == "")
                isValid = false;
            else if (importeCompra == "")
                isValid = false;
        }
        else if (consulta == 3) {
            nombreComercio = $("#txtNombre2").val();
            domicilioComercio = $("#txtDomicilio2").val();
            if (nombreComercio == "")
                isValid = false;
            else if (domicilioComercio == "")
                isValid = false;
        }
        else if (consulta == 4) {
            motivoConsulta = $("#txtMotivo3").val();
            observacionesCompra = $("#txtObservaciones3").val();
            if (motivoConsulta == "")
                isValid = false;
            else if (observaciones == "")
                isValid = false;
        }
        else if (consulta == 5) {
            sugerencias = $("#txtSugerencias4").val();
            if (sugerencias == "")
                isValid = false;
        }
    }
    else if (motivo == "t") {
        observaciones = $("#txtObservaciones5").val();
        if (observaciones == "")
            isValid = false;
    }

    info += ", fechaCompra: '" + fechaCompra
        + "', horaCompra: '" + horaCompra
        + "', nombreComercio: '" + nombreComercio
        + "', domicilioComercio: '" + domicilioComercio
        + "', importeCompra: " + importeCompra
        + ", motivoConsulta: '" + motivoConsulta
        + "', sugerencias: '" + sugerencias + "'"
        + ", observaciones: '" + observaciones + "' }";

    if (!isValid) {
        if (!validEmail)
            $("#spnError").html("Debe ingresar un email válido");
        else
            $("#spnError").html("Debe completar todos los campos obligatorios");
        $("#spnError").show();
    }
    else {
        $("#spnError").hide();
        
        $.ajax({
            type: "POST",
            url: "sugerencias.aspx/EnviarReclamo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtCredencial").val("");
                $("#txtNombre").val("");
                $("#txtApellido").val("");
                $("#cmbTipoDocumento").val("");
                $("#txtDocumento").val("");
                $("#txtTelefono").val("");
                $("#txtEmail").val("");
                $("#txtFecha1").val("");
                $("#cmbHora1").val("");
                $("#txtNombre1").val("");
                $("#txtDomicilio1").val("");
                $("#txtImporte1").val("");
                $("#txtObservaciones1").val("");

                $("#txtNombre2").val("");
                $("#txtDomicilio2").val("");

                $("#txtMotivo3").val();
                $("#txtObservaciones3").val("");

                $("#txtSugerencias4").val("");

                $("#txtObservaciones5").val("");

                $("#spnError").hide();
                $("#spnOk").show();
                $("#spnOk").html("Los datos han sido enviados correctamente!");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#spnError").html(r.Message);
                $("#spnError").show();
            }
        });
    }
}