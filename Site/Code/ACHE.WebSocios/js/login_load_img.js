/* VARIABLES PARA PRECARGA DE IMAGENES */
var fondo = {
    file : '2014-11-28-sitiosimple_esp.jpg',    // REQUERIDO
    posV : 'bottom',        //Tener en cuenta según la FOTOGRAFÍA
    posH : 'right'          //Tener en cuenta según la FOTOGRAFÍA
};

var textIMG = {
    archivo     : '', // Dejar Vacío si el fondo no tiene imagen con texto de descripción
    alt         : '',     // REQUERIDO
    width       : '500',            	  // REQUERIDO SI file NO ES VACIO
    height      : '400',
    clase       : '',
    position    : {position:'absolute',right:'3%',bottom:'25%'},
    //link_url    : 'http://encuentrosdonweb.com/una-despedida-a-lo-donweb/?utm_source=sitios_donweb&utm_medium=landing&utm_campaign=ingreso_clientes_fondo_asado_3', // A menos que sea algo nuestro o esté autorizado, vá vacío (CAMBIAR FINAL DE URL PARA MEDICION EN SITIO GOOGLE)
    link_url    : '', // A menos que sea algo nuestro o esté autorizado, vá vacío (CAMBIAR FINAL DE URL PARA MEDICION EN SITIO GOOGLE)
    link_target : '_blank'
};

switch (codPaisActual){
    case 'ar':
        fondo.file = 'img/login/univ-donweb.jpg';
        fondo.posV = 'bottom';
        fondo.posH = 'right';
        textIMG.archivo = '';//univ-donweb-txt.png
        textIMG.alt     = 'Eventos Donweb';
        textIMG.width   = '814';
        textIMG.height  = '60';
        textIMG.clase   = '';
        textIMG.position = {position:'absolute',right:'5%',bottom:'5%'};
        textIMG.link_url = 'http://www.socios.com.ar/';
        textIMG.link_target = '_blank';
        break;
}
/*
if(IdiomaFix == 'pt'){
    fondo.file = '2014-11-28-sitiosimple_pt.jpg';
    fondo.posV = 'bottom';
    fondo.posH = 'right';
    textIMG.archivo = 'px.gif';
    textIMG.alt     = '';
    textIMG.width   = '556';
    textIMG.height  = '281';
    textIMG.clase   = '';
    textIMG.position = {position:'absolute',right:'5%',bottom:'50%'};
    textIMG.link_url = 'http://sitiosimple.com';
    textIMG.link_target = '_blank';
}


--PROMOS
if (entreFechas('13/11/2014','17/11/2014') && codPaisActual == 'mx'){
    fondo.file = 'promo_dominio_mx_11-2014.png';
    fondo.posV = 'center';
    fondo.posH = 'right';
    textIMG.archivo = 'px.gif';
    textIMG.alt     = 'Promo Dominios MX';
    textIMG.width   = '650';
    textIMG.height  = '500';
    textIMG.clase   = '';
    textIMG.position = {position:'absolute',right:'0',top:'20%'};
    textIMG.link_url = 'http://donweb.com/es-mx/registro-de-dominios';
    textIMG.link_target = '_blank';
}

if (entreFechas('1/12/2014','1/12/2014') && codPaisActual == 'co'){
    fondo.file = 'colombia_ciberlunes.jpg';
    fondo.posV = 'top';
    fondo.posH = 'right';
    textIMG.archivo = 'colombia_ciberlunes_txt.png';
    textIMG.alt     = 'Ciberlunes 50% OFF';
    textIMG.width   = '667';
    textIMG.height  = '676';
    textIMG.clase   = '';
    textIMG.position = {position:'absolute',right:'0%',top:'0%'};
    textIMG.link_url = 'http://donweb.com/es-co/cyberlunes-co-2014';
    textIMG.link_target = '_blank';
}

if(PaginaActual == 'ingresar'){
    //ShowShare();
}
*/
$('<img/>').load(function() {
    //if(PaginaActual == 'ingresar'){
        $('.wrap').css({"background-image":"url("+fondo.file+")","background-position":fondo.posH+" "+fondo.posV});
    /*}
    if(textIMG.archivo != ''){
        $('<img/>').load(function() {
            if(PaginaActual == 'ingresar'){
                $('.text_img_container').html('<img width="'+textIMG.width+'" height="'+textIMG.height+'" alt="'+textIMG.alt+'" src="'+ImageProtocol+'://'+ImageHostName+'/fondos_ingresar/'+textIMG.archivo+'" class="'+textIMG.clase+'" id="fdo_description" />');
                $('#fdo_description').css(textIMG.position);
                if(textIMG.link_url != ''){
                    $('#fdo_description').wrap('<a href="'+textIMG.link_url+'" title="Ver m&aacute;s" id="fdo_description_lk" target="'+textIMG.link_target+'" />');
                }

            }
        }).attr('src', ImageProtocol+'://'+ImageHostName+'/fondos_ingresar/'+textIMG.archivo);
    }*/
}).attr('src', fondo.file);



function entreFechas(ini,fin){
    ini = ini.split('/');
    fin = fin.split('/');

    var inicio = new Date();
    inicio.setFullYear(ini[2]*1, (ini[1]*1)-1, ini[0]*1); //TENER EN CUENTA QUE LOS MESES EMPIEZAN EN 0 ENTONCES Junio = 5
    inicio.setHours(0, 0, 0);

    var final = new Date();
    final.setFullYear(fin[2]*1, (fin[1]*1)-1, fin[0]*1); //TENER EN CUENTA QUE LOS MESES EMPIEZAN EN 0 ENTONCES Junio = 5
    final.setHours(23, 59, 59);

    var today = new Date();

    if (today > inicio && today < final){
        return true;
    }else{
        return false;
    }
}


