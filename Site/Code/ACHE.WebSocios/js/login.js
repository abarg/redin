

/* JQUERY Cookies Plugin */
jQuery.cookie = function (name, value, options) {
    if (typeof value != 'undefined') {
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString();
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1).replace(/\+/g, '%20'));
                    break;
                }
            }
        }
        return cookieValue;
    }
};


/* ERROR CARTEL JQUERY PLUGIN by Diego */
; (function ($) {

    var w = [];

    $.msgError = function (element, options) {
        return $.msgError.impl.init(element, options);
    };

    $.msgError.close = function () {
        $.msgError.impl.close();
    };

    $.fn.msgError = function (options) {
        return $.msgError.impl.init(this, options);
    };

    $.msgError.defaults = {
        mensaje: lang['ERROR. �Por favor reportar!'],
        container: '#access_box',
        leftOffset: 5,
        topOffset: 0
    };

    $.msgError.impl = {

        o: null,
        d: {},
        e: null,

        init: function (element, options) {
            var s = this;
            if (s.d.data) { return false; }
            s.o = $.extend({}, $.msgError.defaults, options);
            s.e = element;
            s.create(element);
            s.setPosition(element);
            s.show();
            return s;
        },

        create: function (element) {
            var s = this;
            w = s.getDimensions(s.e);
            s.d.flag = $('<div></div>').attr('class', 'flag-error').attr('data-element', s.e.attr('name')).appendTo(s.o.container);
            s.d.msg = $('<div></div>').html(s.o.mensaje).appendTo(s.d.flag);
        },

        getDimensions: function (element) {
            var el = element;
            if (element.offset() == null) {
                var h = $.browser.opera && $.browser.version > '9.5' && $.fn.jquery <= '1.2.6' ? document.documentElement['clientHeight'] :
					$.browser.opera && $.browser.version < '9.5' && $.fn.jquery > '1.2.6' ? window.innerHeight :
					el.height();
                return [h, el.width()];
            } else {
                return [el.outerHeight(), el.outerWidth()];
            }
        },

        setPosition: function (element) {
            var s = this, top, left, oLeft = 0, oTop = 0, Contenedor = $(s.o.container),
                FlagDim = s.getDimensions(s.d.flag),
				he = w[0] / 2,
				ve = w[1];

            if (element.offset() != null && Contenedor.offset() != null) {
                oLeft = element.offset().left - (Contenedor.offset().left + 10);
                oTop = element.offset().top - (Contenedor.offset().top + 10);
                oLeft += (ve + s.o.leftOffset) + 10;
                oTop += ((he - (FlagDim[0] / 2)) + s.o.topOffset);
            }
            s.d.flag.css({ left: oLeft, top: oTop });
        },

        show: function () {
            var s = this;
            s.d.flag.css({ opacity: 0 });
            s.d.flag.animate({ opacity: 1, left: "-=10" }, 300);
        },

        close: function () {
            var s = this;
            if (s.d.flag) {
                s.d.flag.hide().remove();
                s.d = {};
            }
        }
    };
})(jQuery);

function changeTitle() {
    if ($("#rdbConsultaTarjeta").is(':checked')) {
        $("#txtConsultaNro").poshytip('update', 'Ingrese los 16 d&iacute;gitos de la tarjeta sin espacios. Ej: 6371170100558815');
    }
    else {
        $("#txtConsultaNro").poshytip("update", "Ingrese el Nro de DNI sin puntos. Ej: 21447339");
    }

    if ($("#rdbTarjeta").is(':checked')) {
        $("#Usuario").poshytip("update", "Ingrese los 16 d&iacute;gitos de la tarjeta sin espacios. Ej: 6371170100558815");
    }
    else {
        $("#Usuario").poshytip("update", "Ingrese el Nro de DNI sin puntos. Ej: 21447339");
    }
}


var AccesoH = {
    init: function () {

        if ($('#access_box [data-name="consulta"]')) {
            $('#access_box [data-name="consulta"]').focus();
        }

        if ($('#access_box [data-name="user"]')) {
            $('#access_box [data-name="user"]').focus();
        }

        if ($('#access_box [name="email"]')) {
            $('#access_box [name="email"]').focus();
        }

        if ($('#access_box [name="forget"]')) {
            $('#access_box [name="forget"]').focus();
        }

        $('#access_box').on('keypress.access_box', '[data-name="user"],[data-name="pass"]', function (e) {
            //e.preventDefault();
            if (e.which == 13) {
                $('#btnIngresar').click();
                return false;
            }
        }).on('keypress.access_box', 'input[name="txtConsultaNro"]', function (e) {
            //e.preventDefault();
            if (e.which == 13) {
                $('#btnConsultar').click();
                return false;
            }
        }).on('keypress.access_box', 'input[name="forget"]', function (e) {
            //e.preventDefault();
            if (e.which == 13) {
                $('#access_box .bt_forget').click();
                return false;
            }
        }).on('blur.access_box', 'input', function (e) {
            $(this).removeClass('error');

            /*if ($('.error').size() < 1 || $(this).attr('name') == $('.flag-error').attr('data-element') || $(this).attr('name') == 'email2') {
                AccesoH.remover_errors();
            }
            if($(this).attr('name') == 'email' && $.trim($(this).val()) != ''){
                var oEmail = $(this);
                var data = {Email:$.trim(oEmail.val())};
                $(this).removeClass('UserNoDisp');
                $.ajax({
                    url: '/ajax-check-user.php',
                    dataType: 'json',
                    type:'POST',
                    data: data
                }).done(function(a){
                        if(a.root.site.disponible == 'NO'){
                            oEmail.addClass('error').msgError({mensaje:lang['Este email ya est� siendo utilizado.']+'<br />'+lang['Si eres el propietario de la cuenta,']+'<br />'+lang['intenta restablecer tu contrase�a.']});
                            oEmail.addClass('UserNoDisp');
                        }
                    });
            }*/


        }).on('click.access_box', '#btnIngresar', function (e) {
            e.preventDefault();
            AccesoH.remover_errors();

            var User = $.trim($('#access_box [data-name="user"]').val());
            var Pass = $.trim($('#access_box [data-name="pass"]').val());

            if (User == '') {
                $('#access_box [data-name="user"]').addClass('error').msgError({ mensaje: 'Ingresa tu DNI o Nro de Tarjeta' });
            } else if (Pass == '') {
                $('#access_box [data-name="pass"]').addClass('error').msgError({ mensaje: 'Ingresa tu Contrase&ntilde;a' });
            } else {
                $('#access_box .login_elements').fadeOut('fast', function () {
                    $('#access_box .login_spinner').show();
                    AccesoH.login(User, Pass);
                });
            }

            return false;
        }).on('click.access_box', '#btnConsultar', function (e) {
            e.preventDefault();
            AccesoH.remover_errors();

            var User = $.trim($('#access_box [data-name="consulta"]').val());
            
            if (User == '') {
                $('#access_box [data-name="consulta"]').addClass('error').msgError({ mensaje: 'Ingresa tu DNI o Nro de Tarjeta' });
            } else {
                $('#access_box .login_elements').fadeOut('fast', function () {
                    $('#access_box .login_spinner').show();
                    AccesoH.consulta(User);
                });
            }

            return false;
        }).on('click.access_box', '.bt_crear', function (e) {

            e.preventDefault();
            AccesoH.remover_errors();
            var OKenvio = true;
            reg_exp = /[0-9a-z]([-_.+0-9a-z])*@[0-9a-z����������]([-.]?[0-9a-z����������])*\.[a-z]{2,7}/i;
            reg_exp_letras = /([a-zA-Z])/;
            reg_exp_numeros = /([0-9])/;
            oEmail = $('#access_box input[name="emailReg"]');
            valEmail = $.trim(oEmail.val());

            // VALIDACIONES 
            if (OKenvio && $('#access_box input[name="provincia"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="provincia"]').addClass('error').msgError({ mensaje: 'Ingrese su provincia' });
            }
            if (OKenvio && $('#access_box input[name="ciudad"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="ciudad"]').addClass('error').msgError({ mensaje: 'Ingrese su ciudad' });
            }
            if (OKenvio && $('#access_box input[name="nombre"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="nombre"]').addClass('error').msgError({ mensaje: 'Ingrese su nombre' });
            }
            if (OKenvio && $('#access_box input[name="apellido"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="apellido"]').addClass('error').msgError({ mensaje: 'Ingrese su apellido' });
            }
            if (OKenvio && (valEmail == '' || !reg_exp.test(valEmail))) {
                OKenvio = false;
                oEmail.addClass('error').msgError({ mensaje: 'Ingrese un email de contacto valido' });
            }
            if (OKenvio && $('#access_box input[name="dni"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="dni"]').addClass('error').msgError({ mensaje: 'Ingrese su DNI' });
            }
            if (OKenvio && $('#access_box input[name="telefono"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="telefono"]').addClass('error').msgError({ mensaje: 'Ingrese su telefono' });
            }
            if (OKenvio && $('#access_box input[name="celular"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="celular"]').addClass('error').msgError({ mensaje: 'Ingrese su celular' });
            }
            if (OKenvio && $('#access_box input[name="fechaNac"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="fechaNac"]').addClass('error').msgError({ mensaje: 'Ingrese su fecha de nacimiento' });
            }
            if (OKenvio && $('#access_box input[name="tarjeta"]').val() == '') {
                OKenvio = false;
                $('#access_box input[name="tarjeta"]').addClass('error').msgError({ mensaje: 'Ingrese su Nro de Tarjeta' });
            }
            if (OKenvio && $('#access_box input[name="tarjeta"]').length() < 16) {
                OKenvio = false;
                $('#access_box input[name="tarjeta"]').addClass('error').msgError({ mensaje: 'Ingrese los 16 digitos del Nro de Tarjeta' });
            }

            if (OKenvio) {
                $('#access_box .new_elements').fadeOut('fast', function () {
                    $('#access_box .new_spinner').show();
                    AccesoH.new_account();
                });
            }

            return false;
        }).on('click.access_box', '.bt_forget', function (e) {
            e.preventDefault();
            AccesoH.remover_errors();
            var oForget = $('#access_box [name="forget"]'),
                valForget = $.trim(oForget.val());
            if (valForget == '') {
                oForget.addClass('error').msgError({ mensaje: 'Ingresa el email asociado a tu tarjeta' });
            } else {
                $('#access_box .olvido_elements').fadeOut('fast', function () {
                    $('#access_box .olvido_spinner').show();
                    AccesoH.olvido(valForget);
                });
            }

            return false;
        }).on('click.access_box', '.forget_lk', function (e) {
            e.preventDefault();
            $("#access_box").flip({
                direction: 'lr',
                content: $('#flip_olvido'),
                speed: 100,
                color: 'rgba(255,255,255,0.3)',
                onAnimation: function () {
                    AccesoH.remover_errors();
                },
                onEnd: function () {
                    $('#access_box input[name="forget"]').focus();
                }
            });
        }).on('click.access_box', '.new_lk', function (e) {
            e.preventDefault();
            $("#access_box").flip({
                direction: 'rl',
                content: $('#flip_new'),
                speed: 100,
                color: 'rgba(255,255,255,0.3)',
                onAnimation: function () {
                    AccesoH.remover_errors();
                },
                onEnd: function () {
                    $('#access_box input[name="email"]').focus();
                }
            });
        }).on('click.access_box', '.content_volver, .olvido_volver', function (e) {
            e.preventDefault();
            $("#access_box").flip({
                direction: $(this).attr('rel'),
                content: $('#flip_login'),
                speed: 100,
                color: 'rgba(255,255,255,0.3)',
                onAnimation: function () {
                    AccesoH.remover_errors();
                },
                onEnd: function () {
                    $('#access_box [data-name="user"]').focus();
                }
            });
        }).on('click.access_box', '.redes', function (e) {
            e.preventDefault();
            AccesoH.loginOAUTH($(this).attr('rel'));
        });

    },

    remover_errors: function () {
        $('#access_box input').removeClass('error');
        $.msgError.close();
    },

    login: function (User, Pass, origen) {

        var tipo = "DNI";
        if ($("#rdbTarjeta").is(':checked'))
            tipo = "TAR";
        var info = "{ tipo: '" + tipo + "', usuario: '" + User + "', pwd: '" + Pass + "'}";

        $("#hdnAction").val("");

        $.ajax({
            type: "POST",
            url: "index.aspx/ingresar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "app/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);

                if (r.Message == "SOLICTAR") {
                    $('#access_box .login_spinner').fadeOut('fast', function () {
                        $("#access_box").flip({
                            direction: 'rl',
                            content: $('#flip_new'),
                            speed: 100,
                            color: 'rgba(255,255,255,0.3)',
                            onAnimation: function () {
                                AccesoH.remover_errors();
                            },
                            onEnd: function () {
                                $('#access_box input[name="email"]').focus();
                            }
                        });
                    });
                }
                else {
                    $('#access_box .login_spinner').fadeOut('fast', function () {
                        $('#access_box .login_elements').show();
                        $('#access_box [data-name="user"],#access_box [data-name="pass"]').addClass('error');
                        $('#access_box [data-name="user"]').parent().msgError({ mensaje: r.Message });
                    });
                }
            }
        });
    },

    new_account: function () {

        var info = "{ provincia: '" + $("#provincia").val() + "', ciudad: '" + $("#ciudad").val() + "', nombre: '" + $("#nombre").val() + "', apellido: '" + $("#apellido").val() + "', email: '" + $("#emailReg").val() + "', dni: '" + $("#dni").val() + "', telefono: '" + $("#telefono").val() + "', celular: '" + $("#celular").val() + "', fechaNac: '" + $("#fechaNac").val() + "', tarjeta: '" + $("#tarjeta").val() + "'}";

        $.ajax({
            type: "POST",
            url: "index.aspx/solicitar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#access_box .new_spinner').fadeOut('fast', function () {
                    $('#access_box .new_result').show();
                });
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);

                $('#access_box .new_spinner').fadeOut('fast', function () {
                    $('#access_box .new_elements').show();
                    $('#access_box input[name="nombre"]').addClass('error').msgError({ mensaje: r.Message });
                });
            }
        });
    },

    olvido: function (User) {
        var info = "{ nroDoc: '" + $("#forget").val() + "'}";
        $("#hdnAction").val("");
        $.ajax({
            type: "POST",
            url: "index.aspx/RecuperarDatos",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#access_box .olvido_spinner').fadeOut('fast', function () {
                    $('#access_box .olvido_result').show();
                });
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $('#access_box .olvido_spinner').fadeOut('fast', function () {
                    $('#access_box .olvido_elements').show();
                    $('#access_box [name="forget"]').addClass('error').msgError({ mensaje: r.Message });
                });
            }
        });
    },

    consulta: function (User) {

        var tipo = "DNI";
        if ($("#rdbConsultaTarjeta").is(':checked'))
            tipo = "TAR";
        var info = "{ tipo: '" + tipo + "', usuario: '" + User + "'}";

        $("#hdnAction").val("");

        $.ajax({
            type: "POST",
            url: "index.aspx/consulta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null)
                {
                    $("#consultaTitulo").html("Al dia de hoy registras");
                    $("#consultaPuntos").html("PUNTOS ACUMULADOS <span>" + data.d.Puntos + "</span>");
                    $("#consultaPesos").html("EQUIVALENCIA EN DINERO <span>$" + data.d.Total + "</span>");
                    $("#consultaAhorro").html("DINERO AHORRADO <span>$" + data.d.TotalAhorro + "</span>");

                    $('#access_box .login_spinner').fadeOut('fast', function () {
                        $('#access_box .login_result').show();
                    });
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);

                $('#access_box .login_spinner').fadeOut('fast', function () {
                    $('#access_box .login_elements').show();
                    $('#access_box [data-name="consulta"]').addClass('error');
                    $('#access_box [data-name="consulta"]').parent().msgError({ mensaje: r.Message });
                });
            }
        });
    },


    /*loginOAUTH: function(providerOAUTH){
        var opOAUTH, opOAUTHw, opOAUTHh;
        var urlOAUTH = 'https://donweb.com/'+urlIdiomaPais+'/login-'+providerOAUTH+'?origen=login';
        ////_gaq.push(['_trackEvent', 'INGRESAR', 'Acceso OAuth', 'Click '+providerOAUTH]);

        switch (providerOAUTH) {
            case "live":
                opOAUTHw = 650;
                opOAUTHh = 520;
                break;
            case "google":
                opOAUTHw = 780;
                opOAUTHh = 430;
                break;
            case "facebook":
                opOAUTHw = 650;
                opOAUTHh = 430;
                break;
            default:
                opOAUTHw = 780;
                opOAUTHh = 430;
                break;
        }
        opOAUTH = 'width='+opOAUTHw+',height='+opOAUTHh+',left='+(screen.width-opOAUTHw)/2+',top='+(screen.height-opOAUTHh)/2+',status=NO';
        var dattatecOAuth = window.open(urlOAUTH, 'dattatecOAuth', opOAUTH);
    }*/
};

/*
function oAuthSuccess(oaprovider) {
    ////_gaq.push(['_trackEvent', 'INGRESAR', 'Acceso OAuth', 'Success '+oaprovider]);
    $('#access_box .login_elements').fadeOut('fast',function(){
        $('#access_box .login_spinner').show();
        setTimeout(function(){
            var URL = 'https://administracion.donweb.com/clientes/index.php?init_page='+InitPage;
            window.location.href=URL;
        },500);
    });
}
*/
function ShowShare() {
    var Archivo = encodeURIComponent(fondo.file.replace('.jpg', ''));
    Archivo = $.trim(Archivo);
    $('.linkDownload').html('<a title="Descargar este fondo" href="http://media.donweb.com/#' + Archivo + '" target="_blank">Descargar</a>');
    $('.linkShare ul')
        .append('<li><a title="Comparte la imagen en Facebook" class="shaFace" href="http://www.facebook.com/sharer.php?s=100&p[url]=http://donweb.com/fondos_ingresar/downloads/' + fondo.file + '&p[images][0]=https://donweb.com/fondos_ingresar/downloads/' + fondo.file + '&p[title]=' + encodeURIComponent("Descargate el Wallpaper DonWeb de hoy") + '&p[summary]=' + textIMG.alt + '" target="_blank">Facebook</a></li>')
        .append('<li><a title="Pinea la imagen en Pinterest" class="shaPint" href="http://pinterest.com/pin/create/button/?url=https://donweb.com/fondos_ingresar/downloads/' + encodeURIComponent(fondo.file) + '&media=https://donweb.com/fondos_ingresar/downloads/' + encodeURIComponent(fondo.file) + '&description=' + encodeURIComponent(textIMG.alt) + '" target="_blank">Pinterest</a></li>')
        .append('<li><a title="Comparte la imagen en Twitter" class="shaTwi" href="https://twitter.com/intent/tweet?original_referer=https://donweb.com/fondos_ingresar/downloads/' + encodeURIComponent(fondo.file) + '&source=tweetlink&text=' + encodeURIComponent("Descargate el Wallpaper DonWeb de hoy") + '&url=https://donweb.com/fondos_ingresar/downloads/' + encodeURIComponent(fondo.file) + '" data-via="DonWeb" data-lang="es" target="_blank">Twitter</a></li>')
        .append('<li><a title="Comparte la imagen en Google Plus" class="shaGoo" href="https://plus.google.com/share?url=' + encodeURIComponent("https://donweb.com/fondos_ingresar/downloads/index.php?img=") + Archivo + encodeURIComponent("&tit=Descarga+este+Wallpaper+DonWeb") + encodeURIComponent("&desc=") + encodeURIComponent(textIMG.alt.replace(/ /g, "+")) + '" target="_blank">Google +</a></li>');
}

$(document).ready(function () {

    // if($.cookie("login_userName") != null){
    // window.location.href='https://administracion.donweb.com/clientes/index.php?ingresar=logued&init_page='+InitPage;
    // }else{
    ////_gaq.push(['_trackEvent', 'INGRESAR', 'Visita', '']);

    //$.getScript(ImageProtocol+'://'+ImageHostName+'/js/sp/ingresar_load_img.js');

    var vista = 'login';
    if (window.location.href.match('view=([^&amp;]+)')) {
        vista = window.location.href.match('view=([^&amp;]+)')[1];
    }

    if ($("#hdnAction").val() == "solicitar")
        vista = 'register';
    //alert(vista);

    if (vista == 'login') {
        $('#access_box').html($('#flip_login').html());
    } else if (vista == 'register') {
        $('#access_box').html($('#flip_new').html());
    } else if (vista == 'olvido') {
        $('#access_box').html($('#flip_olvido').html());
    } else {
        $('#access_box').html($('#flip_login').html());
    }


    AccesoH.init();

    $('#tarjeta, #dni, #telefono, #celular, #txtConsultaNro, #Usuario').numeric();


    $('#content').on('click', '#fdo_description_lk', function (e) {
        ////_gaq.push(['_trackEvent', 'INGRESAR', 'Click Texto Fondo', $(this).children('img').attr('alt')]);
        var URL = $(this).attr('href');
        if ($(this).attr('target') != '_blank') {
            e.preventDefault();
            setTimeout(function () {
                window.location.href = URL;
            }, 300);
        }
    }).on('click', 'a.linkShare', function (e) {
        e.preventDefault();
        $(".share-box ul").slideToggle('fast');
    }).on('click', '.share-box ul li a', function () {
        ////_gaq.push(['_trackEvent', 'INGRESAR', 'IMG Share Click', $(this).attr('class')]);
        $(".share-box ul").slideUp('fast');
    }).on('click', '.linkDownload a', function () {
        ////_gaq.push(['_trackEvent', 'INGRESAR', 'IMG Download Click', fondo.file]);
    });

    /* CARGA FUENTES GOOGLE WEB FONTS */
    WebFontConfig = {
        google: { families: ['Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700:latin'] }
    };

    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
    //}

});


/**aux*/
/*function solicitarRegistro()
{
    $('#access_box .login_spinner').hide();
        $("#access_box").flip({
            direction: 'rl',
            content: $('#flip_new'),
            speed: 100,
            color: 'rgba(255,255,255,0.3)',
            onAnimation: function () {
                AccesoH.remover_errors();
            },
            onEnd: function () {
                $('#access_box input[name="email"]').focus();
            }
        });
   
    
}*/