﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="atencion-cliente.aspx.cs" Inherits="atencion_cliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Atencion al cliente</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" Runat="Server">
    <div id="mobile-menu" class="mm-menu mm-offcanvas">
        <div class="mm-panel mm-opened mm-current" id="mm-1">
            <ul class="mm-listview mm-first mm-last">

                <li><a href="index.aspx"><span>Inicio</span></a></li>
                <li><a href="http://clubin.com.ar/localidad/" target="_blank"><span>Comercios Adheridos</span></a></li>
                <li><a href="atencion-cliente.aspx" class="mm-selected"><span>Atención al Cliente</span></a></li>
                <li><a href="preguntas-frecuentes.aspx"><span>Preguntas Frecuentes</span></a></li>
                <li><a href="sugerencias.aspx"><span>Sugerencias y reclamos</span></a></li>
                <li><a href="terminos.aspx"><span>Términos y Condiciones</span></a></li>
            </ul>
        </div>
    </div>
    <div id="mm-0" class="mm-page mm-slideout" style="min-height: 600px;">
        <div id="wrapper">

            <header id="header" class="">
                <nav class="navbar" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="#mobile-menu" class="navbar-toggle">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="navbar-brand" href="index.aspx">
                            <img src="/img/logosocios.png" class="img-responsive" />
                        </a>
                        <p class="nav-title">Red de beneficios</p>
                    </div>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.aspx"><img src="/img/logos_header/icon1.png" /><span><br>Inicio</span></a></li>
                            <li><a href="http://clubin.com.ar/localidad/" target="_blank"><img src="/img/logos_header/icon2.png" /><span>Comercios<br>Adheridos</span></a></li>
                            <li><a href="atencion-cliente.aspx"><img src="/img/logos_header/icon3.png" /><span>Atención<br>al Cliente</span></a></li>
                            <li><a href="preguntas-frecuentes.aspx"><img src="/img/logos_header/icon4.png" /><span>Preguntas<br>Frecuentes</span></a></li>
                            <li><a href="sugerencias.aspx"><img src="/img/logos_header/icon5.png" /><span>Sugerencias<br>y reclamos</span></a></li>
                            <li><a href="terminos.aspx"><img src="/img/logos_header/icon6.png" /><span>Términos y<br>Condiciones</span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </header>
            <!-- /header -->

            <div id="main override" class="height-not-needed">
                <div id="client" class="row">
                  <div class="col-md-6 col-sm-12 col-xs-12 client">
                    <br />
                    <h1>Atención al cliente</h1>
                    <br />
                    <p>Comunicate con nuestro<br>
                    centro de atención de socios:</p>
                    <h1>0800-220-4646</h1>
                    <p>socios@redin.com.ar</p>
                    <br />
                    <h1><span>Horario de atencion<span></h1>
                    <p class="negrita"> Lunes a Viernes<br />
                    10 a 18hs.
                    </p>
                    <br />
                  </div>
                  <div class="col-md-6 col-sm-12 col-xs-12 telemarketer">
                    &nbsp;
                    <img src="/img/telemarketer.png" alt="" class='img-responsive'/>
                  </div>
                </div>
            </div>
            
            <!-- End Main-->
            
            <UC:Logos runat="server" id="ucLogos"></UC:Logos>
            <!--end footer-->
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContainer" Runat="Server">
</asp:Content>

