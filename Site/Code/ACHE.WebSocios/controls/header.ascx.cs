﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentSociosUser"] != null)
        {
            var oUsuario = (WebSociosUser)Session["CurrentSociosUser"];
            this.lblUsuario.Text = oUsuario.Nombre;
            //lblNombre.Text = oUsuario.Comercio

            if (oUsuario.IDSocio == 16889 || oUsuario.IDSocio == 9905)
                liCanjes.Visible = true;
            else
                liCanjes.Visible = true;
        }
    }
}