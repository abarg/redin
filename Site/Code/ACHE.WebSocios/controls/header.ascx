﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="controls_header" %>

<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand pull-left" style="width: 400px" href="<%= ResolveUrl("~/app/home.aspx") %>">RED IN - 0800-220-4646</a>
                <ul class="nav navbar-nav" id="mobile-nav">
                    <li runat="server" id="liTransacciones">
                        <a href="<%= ResolveUrl("~/app/transacciones.aspx") %>"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Mis Transacciones</a>
                    </li>
                    <li runat="server" id="liCanjes">
                        <a href="<%= ResolveUrl("~/app/canjes.aspx") %>"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;Canjes</a>
                    </li>
                    <li runat="server" id="litFacturacion">
                        <a href="<%= ResolveUrl("~/app/facturas.aspx") %>"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;Mis Facturas</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav user_menu pull-right">
                    <li class="divider-vertical hidden-sm hidden-xs"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/app/img/user_avatar.png" CssClass="user_avatar" /><b class="caret"></b>
                            <asp:Label runat="server" ID="lblUsuario" />
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<%= ResolveUrl("~/app/cambiar-pwd.aspx") %>">Cambiar contraseña</a></li>
                            <li><a href="<%= ResolveUrl("~/app/mis-datos.aspx") %>">Mis datos</a></li>
                            <li><a href="<%= ResolveUrl("~/index.aspx?logOut=true") %>">Cerrar sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
