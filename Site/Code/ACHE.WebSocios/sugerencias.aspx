﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="sugerencias.aspx.cs" Inherits="sugerencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Sugerencias y reclamos</title>
    <script src="/js/sugerencias.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="Server">
    <div id="mobile-menu" class="mm-menu mm-offcanvas">
        <div class="mm-panel mm-opened mm-current" id="mm-1">
            <ul class="mm-listview mm-first mm-last">
                <li><a href="index.aspx"><span>Inicio</span></a></li>
                <li><a href="http://clubin.com.ar/localidad/" target="_blank"><span>Comercios Adheridos</span></a></li>
                <li><a href="atencion-cliente.aspx"><span>Atención al Cliente</span></a></li>
                <li><a href="preguntas-frecuentes.aspx" class="mm-selected"><span>Preguntas Frecuentes</span></a></li>
                <li><a href="sugerencias.aspx"><span>Sugerencias y reclamos</span></a></li>
                <li><a href="terminos.aspx"><span>Términos y Condiciones</span></a></li>
            </ul>
        </div>
    </div>
    <div id="mm-0" class="mm-page mm-slideout" style="min-height: 600px;">
        <div id="wrapper">
            <header id="header" class="">
                <nav class="navbar" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="#mobile-menu" class="navbar-toggle">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="navbar-brand" href="#">
                            <img src="/img/logosocios.png" class="img-responsive" />
                        </a>
                        <p class="nav-title">Red de beneficios</p>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.aspx"><img src="/img/logos_header/icon1.png" /><span><br>Inicio</span></a></li>
                            <li><a href="http://clubin.com.ar/localidad/" target="_blank"><img src="/img/logos_header/icon2.png" /><span>Comercios<br>Adheridos</span></a></li>
                            <li><a href="atencion-cliente.aspx"><img src="/img/logos_header/icon3.png" /><span>Atención<br>al Cliente</span></a></li>
                            <li><a href="preguntas-frecuentes.aspx"><img src="/img/logos_header/icon4.png" /><span>Preguntas<br>Frecuentes</span></a></li>
                            <li><a href="sugerencias.aspx"><img src="/img/logos_header/icon5.png" /><span>Sugerencias<br>y reclamos</span></a></li>
                            <li><a href="terminos.aspx"><img src="/img/logos_header/icon6.png" /><span>Términos y<br>Condiciones</span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </header>
            <!-- /header -->
            <div id="main height-not-needed">
                <div id="terminos" class="sugerencias">
                    <div class="content">
                        <div class="col suggestion">
                        </div>
                        <div class="col terminos">
                            <div id='faq'>
                                <br />
                                <h1>Sugerencias y reclamos</h1>
                                <br />
                            </div>
                            <div class="text scroll" style='margin: 0 5rem'>
                                <form action="" id="frmDatos">
                                    <div class="form_head">
                                        <ul class="" style='padding: 0; margin: 0; font-family: arial;'>
                                            <li>
                                                <p>Selecciona el motivo de tu consulta:</p>
                                            </li>
                                            <li>
                                                <input type="radio" checked="checked" name="motivo" id="rdbBeneficios" value="b" />
                                                <label for="rdbBeneficios">Beneficios y Puntos</label>
                                            </li>
                                            <li>
                                                <input type="radio" name="motivo" id="rdbTarjeta" value="t" />
                                                <label for="rdbTarjeta">Tarjetas</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="form_contenedor">
                                        <div class="form_radio">
                                            <label style="margin: 0.5em 0 0 1em;">Asunto: </label>
                                            <select id="cmbBeneficios" onchange="chequearConsulta();" class="consulta">
                                                <option value="1">No me otorgaron el descuento</option>
                                                <option value="2">No me otorgaron los puntos</option>
                                                <option value="3">No existe más el comercio</option>
                                                <option value="4">Otros</option>
                                                <option value="5">Dejanos tus sugerencias</option>
                                            </select>
                                            <select id='cmbTajeta' onchange="chequearConsulta();" class="consulta">
                                                <option value="1">Perdida y/o Robo</option>
                                                <option value="2">Desmagnetizada</option>
                                                <option value="3">Datos Erroneos</option>
                                                <option value="4">No consigo la tarjeta</option>
                                            </select>
                                            <br />
                                            <br />
                                            <!-- <ul style='padding: 0; margin:0;'>
													<li>
														<input type="radio" name="tipo" id="r1" value="1" />
														<label for="r1"> No me otorgaron beneficios</label>
													</li>
													<li>
														<input type="radio" name="tipo" id="r2" value="1" />
														<label for="r2"> Dejenos tus sujerencias</label>
													</li>
												</div> -->
                                            <hr style='margin: 0.1em; border-color: #DDD;' />
                                            
                                            <div class="form_datos" id="divDatosPersonales">
                                                
                                                <p style='margin: 0.5em 0 0 1em; font-weight: bold;'>Datos personales:</p>
                                                <!-- <p class="text-right" style='padding-right: 1em;'><span>*</span> Dato obligatorio</p> -->
                                                <ul>
                                                <li>
                                                    <label for="credencial"><span>*&nbsp;</span>Credencial N°:</label>
                                                    <input type="text" name="credencial" placeholder="" id="txtCredencial" />
                                                </li>
                                                <li>
                                                    <label for="nombre"><span>*&nbsp;</span>Nombre:</label>
                                                    <input type="text" name="nombre" placeholder="" id="txtNombre" />
                                                </li>
                                                <li>
                                                    <label for="apellido"><span>*&nbsp;</span>Apellido:</label>
                                                    <input type="text" name="apellido" placeholder="" id="txtApellido" />
                                                </li>
                                                <li>
                                                    <label for="doc_tipo"><span>*&nbsp;</span>Tipo Documento:</label>
                                                    <select id="cmbTipoDocumento">
                                                        <option value="DNI">DNI</option>
                                                        <option value="CUIT">CUIT</option>
                                                        <option value="Otro">Otro</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <label for="doc_tipo"><span>*&nbsp;</span>Documento:</label>
                                                    <input type="text" id="txtDocumento" name="doc_tipo" placeholder="" />
                                                </li>
                                                <li>
                                                    <label for="telefono"><span>*&nbsp;</span>Teléfono:</label>
                                                    <input type="text" id="txtTelefono" name="telefono" placeholder="" />
                                                </li>
                                                <li>
                                                    <label for="email"><span>*&nbsp;</span>Email:</label>
                                                    <input type="text" id="txtEmail" name="emails" placeholder="" />
                                                    
                                                </li>
                                                </ul>
                                            </div>
                                            
                                            <div class="form_datos" id="divDatosAdicionales">
                                                <hr style='margin: 0.1em; border-color: #DDD;' />

                                                <p style='margin: 0.5em 0 0 1em; font-weight: bold;'>Datos adicionales:</p>

                                                <div id='divDatos1'>
                                                    <ul>
                                                    <li>
                                                        <label for="fecha_compra"><span>*&nbsp;</span>Fecha de la compra:</label>
                                                        <input type="text" id="txtFecha1" name="fecha_compra" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="hora_compra"><span>*&nbsp;</span>Hora de la compra:</label>
                                                        <select id="cmbHora1" class="descuento">
                                                            <option value="manana">Mañana</option>
                                                            <option value="tarde">Tarde</option>
                                                            <option value="noche">Noche</option>
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <label for="nombre_comercio"><span>*&nbsp;</span>Nombre del comercio:</label>
                                                        <input type="text" id="txtNombre1" name="nombre_comercio" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="domicilio_comercio"><span>*&nbsp;</span>Domiciio del comercio:</label>
                                                        <input type="text" id="txtDomicilio1" name="domicilio_comercio" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="importe_compra"><span>*&nbsp;</span>Importe de la compra:</label>
                                                        <input type="text" id="txtImporte1" name="importe_compra" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="importe_compra">Observaciones:</label>
                                                        <textarea id="txtObservaciones1" name="observaciones_adicional"></textarea>
                                                    </li>
                                                    </ul>
                                                </div>

                                                <div id='divDatos2'>
                                                    <ul>
                                                    <li>
                                                        <label for="nombre_comercio"><span>*&nbsp;</span>Nombre del comercio:</label>
                                                        <input type="text" id="txtNombre2" name="nombre_comercio" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="domicilio_comercio"><span>*&nbsp;</span>Domicilio del comercio:</label>
                                                        <input type="text" id="txtDomicilio2" name="domicilio_comercio" placeholder="" />
                                                    </li>
                                                    </ul>
                                                </div>

                                                <div id='divDatos3'>
                                                    <ul>
                                                    <li>
                                                        <label for="motivo_consulta"><span>*&nbsp;</span>Motivo de la consulta:</label>
                                                        <input type="text" id="txtMotivo3" name="motivo_consulta" placeholder="" />
                                                    </li>
                                                    <li>
                                                        <label for="observaciones_adicional"><span>*&nbsp;</span>Observaciones:</label>
                                                        <textarea id="txtObservaciones3" name="observaciones_adicional"></textarea>
                                                    </li>
                                                    </ul>
                                                </div>

                                                <div id='divDatos4'>
                                                    <ul>
                                                    <li>
                                                        <label for="sugerencias"><span>*&nbsp;</span>Sugerencias:</label>
                                                        <textarea id="txtSugerencias4" name="sugerencias"></textarea>
                                                    </li>
                                                    </ul>
                                                </div>

                                                <div id='divDatos5'>
                                                    <ul>
                                                    <li>
                                                        <label for="observaciones"><span>*&nbsp;</span>Observaciones:</label>
                                                        <textarea id="txtObservaciones5" name="observaciones"></textarea>
                                                    </li>
                                                    </ul>
                                                </div>


                                                <br />
                                                <br />
                                                <span id="spnError" style="display: none; color: red;">Debe completar todos los campos obligatorios</span>
                                                <span id="spnOk" style="display: none; color: green;"></span>

                                                <button class="btn btn-info" type="submit" onclick="enviarReclamo(); return false;">Enviar</button>
                                                
                                            </div>
                                            
                                        </div>
                                        <!-- <div class="form_datos">
													<p style='margin: 0.5em 0 0 1em; font-weight: bold;'>Completa tus datos personales:</p>
													<p class="text-right" style='padding-right: 1em;'><span>*</span> Dato obligatorio</p>

													<li>
														<label for="credencial">Credencial N°:</label>
														<input type="text" name="credencial" placeholder="" required />
													</li>
													<li>
														<label for="nombre"><span>*</span> Nombre:</label>
														<input type="text" name="nombre" placeholder="" required />
													</li>
													<li>
														<label for="apellido"><span>*</span> Apellido:</label>
														<input type="text" name="apellido" placeholder="" required />
													</li>

													<li>
														<label for="doc_tipo"><span>*</span> Documento Tipo:</label>
														<input type="text" name="doc_tipo" placeholder="" required />
													</li>
													<li>
														<label for="email"><span>*</span>Email:</label>
														<input type="text" name="emails" placeholder="" required />
													</li>
													<li>
														<label for="telefono"><span>*</span> Teléfono:</label>
														<input type="text" name="telefono" placeholder="" required />
													</li>                           
													<li class='text-right'>
														<button class="btn btn-info" type="submit">Enviar</button>
													</li>
												</div> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col suggestion hide">
                        </div>
                    </div>
                    <div style="height: 30px; background-color: #62A9E0">
                    </div>
                </div>
            </div>
            <UC:Logos runat="server" id="ucLogos"></UC:Logos>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContainer" runat="Server">
    <%--<script type="text/javascript">
        $(document).ready(function () {
            $("#cmbTajeta").hide();
            $("#divDatos2, #divDatos3, #divDatos4, #divDatos5").hide();

            chequearMotivo();

            $("input[name='motivo']").click(function () {
                chequearMotivo();
            });

            $("input[name='consulta']").change(function () {
                chequearConsulta();
            });

        });

        function chequearMotivo() {
            var motivo = $("input[name='motivo']:checked").val();

            if (motivo == "b") {
                $("#cmbTajeta").hide();
                $("#divDatos5").hide();
                $("#cmbBeneficios").show();
                chequearConsulta();
            }

            if (motivo == "t") {
                $("#cmbBeneficios").hide();
                $("#divDatos1, #divDatos2, #divDatos3, #divDatos4").hide();
                $("#cmbTajeta").show();
                $("#divDatos5").show();
            }
        }

        function chequearConsulta() {
            $("#divDatos1, #divDatos2, #divDatos3, #divDatos4, #divDatos5").hide();
            var motivo = $("input[name='motivo']:checked").val();
            var consulta = parseInt($(".consulta option:selected").val());
            switch (motivo) {
                case "b":
                    $("#divDatos5").hide();
                    switch (consulta) {
                        case 1:
                        case 2:
                            $("#divDatos1").show();
                            break;
                        case 3:
                            $("#divDatos2").show();
                            break;
                        case 4:
                            $("#divDatos3").show();
                            break;
                        case 5:
                            $("#divDatos4").show();
                            break;
                    }
                    break;

                case "t":
                    $("#divDatos1, #divDatos2, #divDatos3, #divDatos4").hide();
                    $("#divDatos5").show();
                    break;
            }
        }
    </script>--%>
</asp:Content>

