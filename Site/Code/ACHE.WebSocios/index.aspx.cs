﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using System.Web.Services;
using ACHE.Model;
using System.Configuration;
using System.Collections.Specialized;
using ASPSnippets.FaceBookAPI;
using System.Web.Script.Serialization;

public partial class index : PaginaSociosBase
{
    protected static string mensaje = "";

    protected void Login(object sender, EventArgs e)
    {
        FaceBookConnect.Authorize("email", Request.Url.AbsoluteUri.Split('?')[0]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnAction.Value = Request.QueryString["action"];
            //LogOut
            if (!String.IsNullOrEmpty(Request.QueryString["logOut"]))
            {
                if (Request.QueryString["logOut"].Equals("true"))
                {
                    Session.Remove("CurrentSociosUser");
                }
            }
        }

        FaceBookConnect.API_Key = "384414905065444";
        FaceBookConnect.API_Secret = "8a61208d4dcca2e34358c262c8445a5f";
        if (!IsPostBack)
        {
            if (Request.QueryString["error"] == "access_denied")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('User has denied access.')", true);
                return;
            }

            string code = Request.QueryString["code"];
            if (!string.IsNullOrEmpty(code))
            {
                string data = FaceBookConnect.Fetch(code, "me");
                FaceBookUser faceBookUser = new JavaScriptSerializer().Deserialize<FaceBookUser>(data);
                //faceBookUser.PictureUrl = string.Format("https://graph.facebook.com/{0}/picture", faceBookUser.Id);

                var sociosSoapClient = new wsSocios.sociosSoapClient();
                var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
                wsSocios.FrontSociosInfo usu = sociosSoapClient.LoginFb(userCredentials, faceBookUser.Email);
                if (usu.Info != null)//Ya está asociado a FB
                {
                    HttpContext.Current.Session["CurrentSociosUser"] = new WebSociosUser(
                        usu.Info.IDSocio, usu.Info.Email, usu.Info.Apellido + ", " + usu.Info.Nombre,
                        usu.Info.NroDocumento, usu.Info.Telefono, usu.Info.Celular, usu.Info.Domicilio, usu.Info.Foto, "FB", faceBookUser.Id, faceBookUser.Email);

                    var actualizarFb = sociosSoapClient.ActualizarInfoFb(userCredentials, usu.Info.IDSocio, faceBookUser.Id);

                    Response.Redirect("app/home.aspx");
                }
                else
                { //Hay que vincularlo a alguna tarjeta

                    Response.Redirect("login.aspx?action=solicitar");
                }
            }
        }
    }

    [WebMethod(true)]
    public static void ingresar(string tipo, string usuario, string pwd)
    {
        var sociosSoapClient = new wsSocios.sociosSoapClient();
        var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };

        wsSocios.FrontSociosInfo usu = sociosSoapClient.Login(userCredentials, tipo, usuario, pwd);
        if (usu.Info != null)
        {
            HttpContext.Current.Session["CurrentSociosUser"] = new WebSociosUser(
                usu.Info.IDSocio, usu.Info.Email, usu.Info.Apellido + ", " + usu.Info.Nombre,
                usu.Info.NroDocumento, usu.Info.Telefono, usu.Info.Celular, usu.Info.Domicilio, usu.Info.Foto, "", "", usuario);
        }
        else
            throw new Exception(usu.Error);

    }

    [WebMethod(true)]
    public static wsSocios.SociosViewModel consulta(string tipo, string usuario)
    {
        try
        {
            var sociosSoapClient = new wsSocios.sociosSoapClient();
            var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };

            wsSocios.FrontSociosInfo usu = sociosSoapClient.GetInfoConsulta(userCredentials, tipo, usuario);
            if (usu.Info != null)
            {
                return usu.Info;
            }
            else
                throw new Exception(usu.Error);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }

    }

    [WebMethod(true)]
    public static void RecuperarDatos(string nroDoc)
    {

        //if (!email.IsValidEmailAddress())
        //    throw new Exception("Email incorrect.");

        var sociosSoapClient = new wsSocios.sociosSoapClient();
        var userCredentials = new wsSocios.wsUserCredentials() { userName = "socios", password = "Red!n2014" };
        wsSocios.FrontSociosAction action = sociosSoapClient.RecuperarPwd(userCredentials, nroDoc);
        if (!action.Action)
            throw new Exception(action.Error);

        //using (var dbContext = new ACHEEntities())
        //{
        //    var usu = dbContext.Socios.Where(x => x.NroDocumento == nroDoc).FirstOrDefault();
        //    if (usu != null)
        //    {
        //        if (usu.Email != string.Empty)
        //        {

        //            string newPwd = string.Empty;
        //            newPwd = newPwd.GenerateRandom(6);

        //            ListDictionary replacements = new ListDictionary();
        //            replacements.Add("<USUARIO>", usu.Apellido + ", " + usu.Nombre);
        //            replacements.Add("<PASSWORD>", newPwd);

        //            bool send = EmailHelper.SendMessage(EmailTemplate.RecuperoPwd, replacements, usu.Email, "RedIN: Recupero de contraseña");
        //            if (!send)
        //                throw new Exception("El email con su nueva contraseña no pudo ser enviado.");
        //            else
        //            {
        //                usu.Pwd = newPwd;
        //                dbContext.SaveChanges();
        //            }
        //        }
        //        else
        //            throw new Exception("Usted no posee un email registrados. Por favor comuníquese con atención al cliente: 0800-999-4646 o envíe un mail a <a href='mailto:socios@redin.com.ar'>socios@redin.com.ar</a>");
        //    }
        //    else
        //        throw new Exception("El documento ingresado es inexistente.");
        //}
    }

    [WebMethod(true)]
    public static void solicitar(string provincia, string ciudad, string nombre, string apellido, string email, string dni, string telefono, string celular, string fechaNac, string tarjeta)
    {

        if (nombre.Trim() != string.Empty && apellido.Trim() != string.Empty && email.Trim() != string.Empty && dni.Trim() != string.Empty
            && telefono.Trim() != string.Empty && celular.Trim() != string.Empty && tarjeta.Trim() != string.Empty)
        {
            var datos = "Provincia: " + provincia + "<br>";
            datos += "Ciudad: " + ciudad + "<br>";
            datos += "Nombre: " + nombre + "<br>";
            datos += "Apellido: " + apellido + "<br>";
            datos += "Email: " + email + "<br>";
            datos += "DNI: " + dni + "<br>";
            datos += "Tel: " + telefono + "<br>";
            datos += "Cel: " + celular + "<br>";
            datos += "Fecha Nac: " + fechaNac + "<br>";
            datos += "Nro Tarjeta: " + tarjeta + "<br>";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<DATOS>", datos);

            bool send = ACHE.Model.EmailHelper.SendMessage(ACHE.Model.EmailTemplate.SociosSolicitudAcceso, replacements, "socios@redin.com.ar", "RedIN: Solicitud de Acceso");
            //if (!send)
            //    throw new Exception("El email con su nueva contraseña no pudo ser enviado.");

        }
        else
            throw new Exception("Por favor, complete todos los datos");
    }
}