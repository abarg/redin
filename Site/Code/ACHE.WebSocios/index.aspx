﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Socios</title>

    <style id="poshytip-css-tip-yellowsimple" type="text/css">div.tip-yellowsimple{visibility:hidden;position:absolute;top:0;left:0;}div.tip-yellowsimple table.tip-table, div.tip-yellowsimple table.tip-table td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;vertical-align:middle;}div.tip-yellowsimple td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-yellowsimple td.tip-right{background-position:100% 0;}div.tip-yellowsimple td.tip-bottom{background-position:100% 100%;}div.tip-yellowsimple td.tip-left{background-position:0 100%;}div.tip-yellowsimple div.tip-inner{background-position:-10px -10px;}div.tip-yellowsimple div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style>

    <script type="text/javascript">

        lang = {};

        //var RequestLogin = '';
        //var InitPage = '';
        //var PaginaActual = 'ingresar';
        //var sepDecimal = ',';
        //var sepMiles = '.';
        //if (sepDecimal == sepMiles) { sepMiles = ','; }
        //var simbMoneda = '$';
        //if (simbMoneda == '&#128;') { simbMoneda = '€'; }
        //var nameMoneda = 'Pesos Argentinos';
        //var strImpuestos = 'IVA Incluído';
        //var langActual = 'sp';
        var IdiomaFix = 'es';
        var codPaisActual = 'ar';
        //var codTelPais = '54';
        //var UTCPais = '-3.00';
        //var codPaisControl = 'ar';
        //var namePaisActual = 'Argentina';
        //var urlIdiomaPais = 'es-ar';
        //var urlPaisActual = 'argentina';
        //var HostName = '';
        //var Protocol = 'http';
        //var ImageHostName = '';
        //var ImageProtocol = 'http';
        //var Paises = { "ar": { "nombre": "Argentina", "codigo": "ar", "url": "argentina", "idioma": "sp", "simbolo_moneda": "$", "nombre_moneda": "Pesos Argentinos", "separador_decimal": ",", "utc": "-3.00", "codigo_cc": "54" }, "bo": { "nombre": "Bolivia", "codigo": "bo", "url": "bolivia", "idioma": "sp", "simbolo_moneda": "u$s", "nombre_moneda": "D\u00f3lares", "separador_decimal": ".", "utc": "-4.00", "codigo_cc": "591" }, "br": { "nombre": "Brasil", "codigo": "br", "url": "brasil", "idioma": "pt", "simbolo_moneda": "R$", "nombre_moneda": "Reais", "separador_decimal": ",", "utc": "-3.00", "codigo_cc": "55" }, "cl": { "nombre": "Chile", "codigo": "cl", "url": "chile", "idioma": "sp", "simbolo_moneda": "$", "nombre_moneda": "Pesos Chilenos", "separador_decimal": ",", "utc": "-4.00", "codigo_cc": "56" }, "co": { "nombre": "Colombia", "codigo": "co", "url": "colombia", "idioma": "sp", "simbolo_moneda": "$", "nombre_moneda": "Pesos Colombianos", "separador_decimal": ",", "utc": "-5.00", "codigo_cc": "57" }, "es": { "nombre": "Espa\u00f1a", "codigo": "es", "url": "espana", "idioma": "sp", "simbolo_moneda": "&#128;", "nombre_moneda": "Euros", "separador_decimal": ",", "utc": "1.00", "codigo_cc": "34" }, "us": { "nombre": "Estados Unidos", "codigo": "us", "url": "estados-unidos", "idioma": "en", "simbolo_moneda": "u$s", "nombre_moneda": "D\u00f3lares", "separador_decimal": ".", "utc": "-6.00", "codigo_cc": "1" }, "int": { "nombre": "Internacional", "codigo": "int", "url": "internacional", "idioma": "sp", "simbolo_moneda": "u$s", "nombre_moneda": "D\u00f3lares", "separador_decimal": ".", "utc": "0.00", "codigo_cc": "0" }, "mx": { "nombre": "Mexico", "codigo": "mx", "url": "mexico", "idioma": "sp", "simbolo_moneda": "$", "nombre_moneda": "Pesos Mexicanos", "separador_decimal": ".", "utc": "-6.00", "codigo_cc": "52" }, "pe": { "nombre": "Peru", "codigo": "pe", "url": "peru", "idioma": "sp", "simbolo_moneda": "S\/.", "nombre_moneda": "Soles", "separador_decimal": ".", "utc": "-5.00", "codigo_cc": "51" }, "uy": { "nombre": "Uruguay", "codigo": "uy", "url": "uruguay", "idioma": "sp", "simbolo_moneda": "$", "nombre_moneda": "Pesos Uruguayos", "separador_decimal": ",", "utc": "-3.00", "codigo_cc": "598" }, "ve": { "nombre": "Venezuela", "codigo": "ve", "url": "venezuelat", "idioma": "sp", "simbolo_moneda": "Bs", "nombre_moneda": "Bolivares Fuertes", "separador_decimal": ",", "utc": "-4.50", "codigo_cc": "58" } };
        //var VersionSitio = '509';
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" Runat="Server">

    <div id="mobile-menu" class="mm-menu mm-offcanvas">
        <div class="mm-panel mm-opened mm-current" id="mm-1">
            <ul class="mm-listview mm-first mm-last">

                <li><a href="index.aspx"><span>Inicio</span></a></li>
                <li><a href="http://clubin.com.ar/localidad/" target="_blank"><span>Comercios Adheridos</span></a></li>
                <li><a href="atencion-cliente.aspx"><span>Atención al Cliente</span></a></li>
                <li><a href="preguntas-frecuentes.aspx"><span>Preguntas Frecuentes</span></a></li>
                <li><a href="sugerencias.aspx"><span>Sugerencias y reclamos</span></a></li>
                <li><a href="terminos.aspx" class="mm-selected"><span>Términos y Condiciones</span></a></li>
            </ul>
        </div>
    </div>
    <div id="mm-0" class="mm-page mm-slideout" style="min-height: 600px;">
        <div id="wrapper">

            <header id="header" class="">
                <nav class="navbar" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="#mobile-menu" class="navbar-toggle">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="navbar-brand" href="index.aspx">
                            <img src="/img/logosocios.png" class="img-responsive" />
                        </a>
                        <p class="nav-title">Red de beneficios</p>
                    </div>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.aspx"><img src="/img/logos_header/icon1.png" /><span><br>Inicio</span></a></li>
                            <li><a href="http://clubin.com.ar/localidad/" target="_blank"><img src="/img/logos_header/icon2.png" /><span>Comercios<br>Adheridos</span></a></li>
                            <li><a href="atencion-cliente.aspx"><img src="/img/logos_header/icon3.png" /><span>Atención<br>al Cliente</span></a></li>
                            <li><a href="preguntas-frecuentes.aspx"><img src="/img/logos_header/icon4.png" /><span>Preguntas<br>Frecuentes</span></a></li>
                            <li><a href="sugerencias.aspx"><img src="/img/logos_header/icon5.png" /><span>Sugerencias<br>y reclamos</span></a></li>
                            <li><a href="terminos.aspx"><img src="/img/logos_header/icon6.png" /><span>Términos y<br>Condiciones</span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </header>
            <!-- /header -->

            <div id="main" class="needed-height">
                <div id="myCarousel" class="carousel slide">
                    <!-- Wrapper for Slides -->
                    <div class="carousel-inner">
                        <div class="item">
                            <!-- Set the first background image using inline CSS below. -->
                            <div class="fill" style="background-image:url('/img/sslider1.png')"></div>
                        </div>
                        <div class="item active">
                            <!-- Set the second background image using inline CSS below. -->
                            <div class="fill" style="background-image:url('/img/sslider2.png')"></div>
                        </div>
                        <div class="item">
                            <!-- Set the third background image using inline CSS below. -->
                            <div class="fill" style="background-image:url('/img/sslider3.png')"></div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="icon-prev"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="icon-next"></span>
                    </a>

                </div>
                <div class="row">
          
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                    <div id="content">
                        <form runat="server" id="frmLogin">

                            <p xmlns="" class="text_firma_container"></p>
                            <div xmlns="" class="text_img_container"></div>
                            <!--div xmlns="" class="share-box">
			                    <div class="linkDownload"></div>
			                    <div class="linkShare">
			                    <a title="Comparte en las Redes Sociales" class="linkShare" href="#">Compartir</a><ul></ul>
			                    </div>
			                    <div class="clear"></div>
			                    </div-->
                            <div xmlns="" id="access_box"></div>
                            <div xmlns="" id="flip_login" style="display: none;">
                                <div class="access_box_interior">
                                    <%--<div class="logo"><img src="/img/login/logo.png" /></div>--%>
                                    <div class="login_elements">
                                        <a href="#" class="bt_consulta" style="font-size:18px; font-weight:bold; font-style:italic; text-decoration:none">Consulta tus PUNTO$</a>
                                        <br />
                                        <div class="form">
                                            <div>
                                                <input type="radio" name="consulta" id="rdbConsultaTarjeta" checked="checked" onclick="changeTitle();" /><span class="acceder" style="font-size: 13px; color: #989898;">Nro Tarjeta</span>
                                                &nbsp;&nbsp;
                                                <input type="radio" name="consulta" id="rdbConsultaDNI" onclick="changeTitle();" /><span class="acceder" style="font-size: 13px; color: #989898;">DNI</span>
                                
                                                <input type="text" data-name="consulta" id="txtConsultaNro" name="txtConsultaNro" placeholder="DNI o Nro Tarjeta" title="Ingrese los 16 d&iacute;gitos de la tarjeta sin espacios. Ej: 6371170100558815" />
                                
                                            </div>
                                            <a href="#" title="Ingresar" id="btnConsultar" class="bt_grey">Consultar</a>
                                        </div>
                                    </div>
                    
                                    <div class="login_elements" style="margin-top:20px;">
                                        <a href="#" class="bt_ingresar" style="font-size:18px; font-weight:bold; font-style:italic; text-decoration:none">Acceso de Socios</a>
                                        <br />
                                        <div class="form">
                                            <div>
                                                <input type="radio" name="tipo" id="rdbDNI" checked="checked"  onclick="changeTitle();" /><span class="acceder" style="font-size: 13px; color: #989898;">DNI</span>
                                                &nbsp;&nbsp;
                                                <input type="radio" name="tipo" id="rdbTarjeta" onclick="changeTitle();" /><span class="acceder" style="font-size: 13px; color: #989898;">Nro Tarjeta</span>

                                                <input type="text" data-name="user" id="Usuario" name="Username" placeholder="DNI o Nro Tarjeta"  title="Ingrese el Nro de DNI sin puntos. Ej: 21447339" />
                                                <input type="password" data-name="pass" id="Password" name="Passwd" placeholder="Contraseña" title="Si Ud. Ingresa por primera vez, la clave es su fecha de nacimiento bajo el formato DDMMAAAA. Ej.: 03011970" />
                                            </div>
                                            <a href="#" title="Ingresar" id="btnIngresar" class="bt_grey">Acceder</a>
                                        </div>
                                        <div class="oAuth_box">
                                            <%--<span class="acceder">Acceder con:</span>
							                    <!--a class="redes log_live" rel="live" href="#" title="Acceder ingresando a mi cuenta de Windows Live"><span></span></a>
							                    <a class="redes log_gp" rel="google" href="#" title="Acceder ingresando a mi cuenta de Google"><span></span></a-->
							                    <a class="redes log_fb" rel="facebook" href="#" title="Acceder ingresando a mi cuenta de Facebook"><span></span></a>
							                    <div class="clear"></div>--%>
                                            <asp:ImageButton ID="btnLogin" runat="server" Text="Login with FaceBook" OnClick="Login" ImageUrl="~/img/fb-login-button.png" />
                                        </div>
                                        <a class="forget_lk" title="Reestablecer tu contraseña" href="#">
                                            ¿Olvidaste tu contraseña?
                                        </a>
                                        <!--a class="new_lk" title="Crear una cuenta gratuita" href="#">¿Aún no tienes <br /> una cuenta?</a-->
                                        <div class="clear"></div>
                                    </div>
                                    <div class="login_spinner" style="display: none;">
                                        <h2>Un momento por favor</h2>
                                        <p class="fs13">
                                            Estamos verificando
                                            <br />
                                            la información ingresada...
                                        </p>
                                        <span></span>
                                        <img src="/img/ingresar_spinner.gif" width="54" height="55" />
                                    </div>
                                    <div class="login_result" style="display: none;">
                                        <a href="#" class="bt_consulta" style="font-size:18px; font-weight:bold; font-style:italic; text-decoration:none" id="consultaTitulo"></a>
                                        <a href="#" rel="rl" class="content_volver"></a>
                                        <br /><br />
					                    <p class="fs13" id="consultaPuntos"></p>
                                        <p class="fs13" id="consultaPesos"></p>
                                        <p class="fs13" id="consultaAhorro"></p>
                                    </div>
                                </div>
                            </div>
                            <div xmlns="" id="flip_new" style="display:none;">
			                    <div class="access_box_interior">
				                    <%--<div class="logo"><img src="/img/logo.png" /></div>--%>
				                    <div class="new_elements">
					                    <h2>Solicita tu usuario y contraseña!</h2>
					                    <a href="#" rel="lr" class="content_volver"></a>
					                    <div>
						                    <input type="text" class="inputReg" id="provincia" name="provincia" placeholder="Ingresa tu provincia" />
                                            <input type="text" class="inputReg" id="ciudad" name="ciudad" placeholder="Ingresa tu ciudad" />
                                            <input type="text" class="inputReg" id="nombre" name="nombre" placeholder="Ingresa tu nombre" />
                                            <input type="text" class="inputReg" id="apellido" name="apellido" placeholder="Ingresa tu apellido" />
                                            <input type="text" class="inputReg" id="emailReg" name="emailReg" placeholder="Ingresa tu e-mail" />
                                            <input type="text" class="inputReg" id="dni" name="dni" placeholder="Ingresa tu DNI" />
                                            <input type="text" class="inputReg" id="telefono" name="telefono" placeholder="Ingresa tu telefono" />
                                            <input type="text" class="inputReg" id="celular" name="celular" placeholder="Ingresa tu celular" />
                                            <input type="text" class="inputReg" id="fechaNac" name="fechaNac" placeholder="Ingresa tu fecha de nacimiento" />
                                            <input type="text" class="inputReg" id="tarjeta" name="tarjeta" placeholder="Ingresa tu nro de Tarjeta" />
						                    <%--<label class="NewUser_news">
							                    <input type="checkbox" value="si" id="newsletter" checked /><strong>Newsletter DonWeb:</strong> Suscribirme para recibir novedades, ofertas especiales e invitaciones a nuestros exclusivos eventos.
						                    </label>--%>
                                            <br /><br />
					                    </div>
					                    <a href="#" title="Solicitar" id="lnkSolicitar" class="bt_crear">Solicitar</a>
					                    <!--p class="terms">Al crear una cuenta, aceptarás los <a title="Terminos y Condiciones del Uso del Servicio" target="_blank" href="legales">Terminos y Condiciones del Uso del Servicio</a>> y la <a title="Política de Protección de Datos" target="_blank" href="politica-de-la-privacidad">Política de Protección de Datos</a> de DonWeb.com-->
				                    </div>
				                    <div class="new_spinner" style="display:none;">
					                    <h2>Un momento, estamos procesando tu información</h2>
					                    <p class="fs13">Mientras tanto puedes descargarte y compartir la imagen del fondo.</p>
					                    <span></span>
					                    <img src="/img/ingresar_spinner.gif" width="54" height="55" />
				                    </div>
				                    <div class="new_result" style="display:none;">
					                    <h2>Bienvenido a ClubIn</h2>
					                    <p class="fs13"><strong>Gracias por contactarnos.</strong><br>Muy pronto te enviaremos los datos de acceso a tu mail</p>
					                    <%--<br><br>
                                        <a href="http://donweb.com" title="Ir sitio web" class="bt_crear_result">Conocer nuestros productos</a>
					                    <a href="https://www.socios.com.ar" title="Ir area socios" class="bt_crear_result">Visitar tu nueva Area de Cliente</a>--%>
				                    </div>
			                    </div>
                            </div>
                            <div xmlns="" id="flip_olvido" style="display: none;">
                                <div class="access_box_interior">
                                    <%--<div class="logo"><img src="/img/logo.png" /></div>--%>
                                    <div class="olvido_elements">
                                        <h2>Restabler contraseña</h2>
                                        <h2 style="font-size:14px;">Por favor ingrese su DNI para recibir su contraseña en su casilla de correo. Ante cualquier inconveniente, comuníquese con atención al cliente: 0800-220-4646 o envíe un mail a <a href='mailto:socios@redin.com.ar'>socios@redin.com.ar</a></h2>

                                        <a href="#" rel="rl" class="content_volver"></a>
                                        <input type="text" name="forget" id="forget"  placeholder="DNI" />
                                        <a href="#" title="Restablecer" class="bt_forget">Restablecer</a><br />
                                        <br />
                                        <br />
                                        <!--br>
                                        <br>
                                        <p align="center" class="fs11">Si olvidaste tu contraseña, puedes restablecerla ingresando el DNI asociado a tu tarjeta.</p-->
                                    </div>
                                    <div class="olvido_spinner" style="display: none;">
                                        <h2>Verificando...</h2>
                                        <span></span>
                                        <img src="/img/ingresar_spinner.gif" width="54" height="55" />
                                    </div>
                                    <div class="olvido_result" style="display: none;">
                                        <h2>El restablecimiento de tu contraseña ha sido iniciado</h2>
                                        <p class="fs13">Te enviamos un email con las instrucciones para finalizar el proceso.
                                            <br />
                                            <br />
                                            <span class="fs11">(Si luego de unos minutos no lo recibes, por favor verifica en tu Correo No Deseado)</span>
                                        </p>
                                        <br />
                                        <p><a href="#" rel="rl" class="olvido_volver">Cerrar</a></p>
                                    </div>
                                </div>
                            </div>

                            <asp:HiddenField runat="server" ID="hdnAction" ClientIDMode="Static" />
                        </form>

                    </div>
                </div>
            </div>
        </div> 
            
            <UC:Logos runat="server" id="ucLogos"></UC:Logos>
            <!--end footer-->
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContainer" Runat="Server">
    
    <script xmlns="" language="javascript" type="text/javascript">
        lang['ERROR. ¡Por favor reportar!'] = "ERROR. ¡Por favor reportar!";
        lang['Este email ya está siendo utilizado.'] = "Este email ya está siendo utilizado.";
        lang['Si eres el propietario de la cuenta,'] = "Si eres el propietario de la cuenta,";
        lang['intenta restablecer tu contraseña.'] = "intenta restablecer tu contraseña.";
        lang['Ingresa tu email o ID de cliente'] = "Ingresa tu email o ID de cliente";
        lang['Ingresa tu contraseña'] = "Ingresa tu contraseña";
        lang['Ingresa un email de contacto válido'] = "Ingresa un email de contacto válido";
        lang['Los emails ingresados son diferentes'] = "Los emails ingresados son diferentes";
        lang['Elige una contraseña segura'] = "Elige una contraseña segura";
        lang['que incluya letras, números'] = "que incluya letras, números";
        lang['y al menos 8 caracteres'] = "y al menos 8 caracteres";
        lang['Ingresa el email asociado a tu Area de Cliente o'] = "Ingresa el email asociado a tu Area de Cliente o";
        lang['el dominio de alguno de los servicios de tu cuenta'] = "el dominio de alguno de los servicios de tu cuenta";
        lang['Alguno de los datos no es correcto.'] = "Alguno de los datos no es correcto.";
        lang['Por favor, verifícalo y vuelve a intentar.'] = "Por favor, verifícalo y vuelve a intentar.";
        lang['Completa los datos solicitados.'] = "Completa los datos solicitados.";
        lang['El email o dominio ingresado'] = "El email o dominio ingresado";
        lang['no corresponden a un Area de Cliente.'] = "no corresponden a un Area de Cliente.";
        lang['Ha ocurrido un error, vuelve'] = "Ha ocurrido un error, vuelve";
        lang['Los datos ingresados son invalidos.'] = "Los datos ingresados son invalidos.";
    </script>

    <script>
         $(function () {
             $('#txtConsultaNro,#Usuario, #Password').poshytip({
                 className: 'tip-yellowsimple',
                 showOn: 'focus',
                 alignTo: 'target',
                 alignX: 'center',
                 alignY: 'bottom',
                 offsetX: 0,
                 offsetY: 5,
                 showTimeout: 100,
                 timeOnScreen: 4000
             });
         });

         $(function () {
             $('.carousel').carousel({
                 interval: 3000
             });
         });
    </script>

    <script src="js/login_librerias.js"></script>    
    <%--<script src="/js/login_load_img.js"></script>--%>
    <script src="js/login.js?v=1"></script>
</asp:Content>

