﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="preguntas-frecuentes.aspx.cs" Inherits="preguntas_frecuentes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Preguntas frecuentes</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="Server">
    
    <div id="mobile-menu" class="mm-menu mm-offcanvas">
        <div class="mm-panel mm-opened mm-current" id="mm-1">
            <ul class="mm-listview mm-first mm-last">

                <li><a href="index.aspx"><span>Inicio</span></a></li>
                <li><a href="http://clubin.com.ar/localidad/" target="_blank"><span>Comercios Adheridos</span></a></li>
                <li><a href="atencion-cliente.aspx"><span>Atención al Cliente</span></a></li>
                <li><a href="preguntas-frecuentes.aspx" class="mm-selected"><span>Preguntas Frecuentes</span></a></li>
                <li><a href="sugerencias.aspx"><span>Sugerencias y reclamos</span></a></li>
                <li><a href="terminos.aspx"><span>Términos y Condiciones</span></a></li>
            </ul>
        </div>
    </div>
    <div id="mm-0" class="mm-page mm-slideout" style="min-height: 600px;">
        <div id="wrapper">

            <header id="header" class="">
                <nav class="navbar" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a href="#mobile-menu" class="navbar-toggle">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="navbar-brand" href="index.aspx">
                            <img src="/img/logosocios.png" class="img-responsive" />
                        </a>
                        <p class="nav-title">Red de beneficios</p>
                    </div>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.aspx"><img src="/img/logos_header/icon1.png" /><span><br>Inicio</span></a></li>
                            <li><a href="http://clubin.com.ar/localidad/" target="_blank"><img src="/img/logos_header/icon2.png" /><span>Comercios<br>Adheridos</span></a></li>
                            <li><a href="atencion-cliente.aspx"><img src="/img/logos_header/icon3.png" /><span>Atención<br>al Cliente</span></a></li>
                            <li><a href="preguntas-frecuentes.aspx"><img src="/img/logos_header/icon4.png" /><span>Preguntas<br>Frecuentes</span></a></li>
                            <li><a href="sugerencias.aspx"><img src="/img/logos_header/icon5.png" /><span>Sugerencias<br>y reclamos</span></a></li>
                            <li><a href="terminos.aspx"><img src="/img/logos_header/icon6.png" /><span>Términos y<br>Condiciones</span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </header>
            <!-- /header -->

            <div id="mainmin">
                <div id="faq" class="row">
                    <div class="col">
                        &nbsp;
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 faq faqfont">
                        <h1>Preguntas Frecuentes</h1>
                        <div class="content scroll">
                            <h4>¿Que es una RED IN?</h4>
                            <p>Es una Red de Beneficios conformada por comercios y empresas que ofrecen a los socios la oportunidad de acceder a productos y servicios obteniendo importantes descuentos, y sumar puntos.</p>

                            <h4>¿Cuáles son las tarjetas que pertenecen a RED IN?</h4>
                            <p>Son tarjetas magnéticas que incluyen en el frente inferior derecho el logo de RED IN y que permiten obtener descuentos y beneficios en comercios.</p>

                            <h4>¿Cómo funcionan las Tarjetas de RED IN?</h4>
                            <p>Las mismas deben ser presentadas, antes de consumir o comprar, en los comercios y empresas adheridos y automáticamente se recibe el descuento o beneficio ofrecido, y se suman puntos.</p>

                            <h4>¿En qué lugares la puedo utilizar?</h4>
                            <p>En todos los comercios y empresas adheridos que figuran en esta web.</p>

                            <h4>¿Cómo canjeo mis puntos?</h4>
                            <p>Los puntos se canjean en los comercios adheridos, y permiten realizar el pago total o parcial de una compra.</p>


                            <h4>Mi tarjeta perteneciente no tiene números en el frente ¿cómo la cambio?</h4>
                            <p>Si tu Tarjeta no posee números en el frente, no funciona. Podes cambiarla los puntos de entrega que figuran en nuestra web. <a href="http://clubin.com.ar/club-in/socios/hacete-socio/" target="_blank">VER PUNTOS DE ENTREGA</a></p>

                            <h4>Extravié mi Tarjeta CLUB IN, ¿puedo pedir que me la reenvíen?</h4>
                            <p>Si perdiste tu Tarjeta envianos un email a socios@redin.com.ar con tu nombre y apellido, tu teléfono o comunicate al 0800-999-4646 y si la misma pertenece a una Institución, Sindicato o Club, comunicándote con la entidad emisora de la tarjeta.</p>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 notebook">
                        &nbsp;
                    </div>
                </div>
            </div>
            <!-- End Main-->
            
            <UC:Logos runat="server" id="ucLogos"></UC:Logos>
            <!--end footer-->
        </div>
    </div>
       

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContainer" runat="Server">
</asp:Content>

