﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CargaCredito {
    public bool Carga { get; set; }
    public List<TransaccionCarga> Transacciones { get; set; }
}

public class TransaccionCarga {
    public string NroTarjeta { get; set; }
    public bool Error { get; set; }
}