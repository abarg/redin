﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class PaginaSociosBase : System.Web.UI.Page
{
    public class FaceBookUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string PictureUrl { get; set; }
        public string Email { get; set; }
    }

    
    public WebSociosUser CurrentSociosUser
    {
        get { return (Session["CurrentSociosUser"] != null) ? (WebSociosUser)Session["CurrentSociosUser"] : null; }
        set { Session["CurrentSociosUser"] = value; }
    }

    private void ValidateUser()
    {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        if (pageName != "index.aspx")
        {
            if (CurrentSociosUser == null)
            {
                Response.Redirect("~/index.aspx");
            }
        }
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }
        
        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e)
    {
        ValidateUser();
    }
}