﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class WebSociosUser
    {
        public int IDSocio { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Domicilio { get; set; }
        public string NroDocumento { get; set; }
        public string Foto { get; set; }
        public string OAuthTipo { get; set; }
        public string OAuthID { get; set; }
        public string NroLogin { get; set; }

        public WebSociosUser(int idSocio, string email, string nombre, string nroDocumento, string tel, string cel, string dom, string foto, string oAuthTipo, string oAuthID, string nroLogin)
        {
            this.IDSocio = idSocio;
            this.Email = email;
            this.Nombre = nombre;
            this.NroDocumento = nroDocumento;
            this.Telefono = tel;
            this.Celular = cel;
            this.Domicilio = dom;
            this.Foto = foto;
            this.OAuthTipo = oAuthTipo;
            this.OAuthID = oAuthID;
            this.NroLogin = nroLogin;
        }
    }
}