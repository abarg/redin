﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Web;

public class CargaVirtual {
    public int Estado { get; set; }
    public string Mensaje { get; set; }
    public string RefTeCarga { get; set; }
    public string RefOperador { get; set; }

    public bool Cargar(string empresa, string telefono, string importe, string refComercio, string idPV) {
        bool result = false;
        try {
            string codigoEmpresa = string.Empty;
            bool importeOk = false;
            switch (empresa.Trim().ToLower()) {
                case "claro":
                    codigoEmpresa = "CLARO001";
                    break;
                case "movistar":
                    codigoEmpresa = "MOVIS001";
                    break;
                case "nextel":
                    codigoEmpresa = "NEXTE001";
                    break;
                case "personal":
                    codigoEmpresa = "PERSO001";
                    break;
            }
            //switch (int.Parse(importe)) {
            //    case 10:
            //    case 20:
            //    case 50:
            //        importeOk = true;
            //        break;
            //}
            if (!string.IsNullOrEmpty(codigoEmpresa)) {
                TcpClient tcpclnt = new TcpClient();
                
                //tcpclnt.Connect("201.234.19.60", 2700);
                tcpclnt.Connect("200.49.145.157", 16900);

                string idcomercio = "88605240";// "17214321";
                string idterminal = "00013782";// "00012728";

                string Cabecera = "V" + "\x03" + DateTime.Now.ToString("yyyyMMddHHmmss") + "\x03" + idcomercio + "\x03" + idterminal;
                String str = "\x02" + Cabecera + "\x03" + codigoEmpresa + "\x03" + telefono + "\x03" + importe + "00\x03" + refComercio + idPV + "\x03" + "1234567890123456" + "\x04";
                Stream stm = tcpclnt.GetStream();

                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(str);

                stm.Write(ba, 0, ba.Length);

                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);

                string sRespuesta = "";
                for (int i = 0; i < k; i++)
                    sRespuesta = sRespuesta + Convert.ToChar(bb[i]);

                tcpclnt.Close();
                Estado = 1;

                sRespuesta = sRespuesta.Replace("\x02", "_").ToString().Trim();
                sRespuesta = sRespuesta.Replace("\x03", "_").ToString().Trim();
                sRespuesta = sRespuesta.Replace("\x04", "_").ToString().Trim();

                string[] aRespuesta = sRespuesta.Split(new[] { "_" }, StringSplitOptions.None);
                if (aRespuesta[1] == "E") {
                    Estado = 1;
                    Mensaje = aRespuesta[7];
                }
                else {
                    Estado = 0;
                    RefOperador = aRespuesta[9];
                    RefTeCarga = aRespuesta[10];
                    result = true;
                }
            }
        }
        catch (Exception e) {
            Estado = 1;
            Mensaje = e.Message;
        }
        return result;
    }

    public struct Respuesta {
        public int resultado;
        public string mensaje;
        public string referenciaTeCarga;
    }
}
