﻿using ACHE.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Geolocalizacion
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (var dbContext = new ACHEEntities())
            {
                Console.Write("Iniciando");

                var socios = dbContext.Domicilios.Include("Provincias").Include("Ciudades").Where(x => x.Entidad == "S" && x.Domicilio != null && x.Domicilio != ""
                && ((x.Latitud == null || x.Latitud == "") || (x.Longitud == null || x.Longitud == ""))).ToList();
                //string geoURL = "https://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyAEg7-84AYcNlPK0FZQm_zUAfFcYmOFleQ"; PRUEBA

                Console.WriteLine("Socios totales: " + socios.Count());
                int index = 1;
                foreach (var socio in socios)
                {
                    Console.WriteLine("Procesando: " + index + " de " + socios.Count());
                    try
                    {
                        string domicilio = socio.Domicilio;
                        if (!string.IsNullOrEmpty(domicilio))
                        {
                            domicilio += " " + socio.Provincias.Nombre + " " + socio.Ciudades.Nombre + " " + socio.Pais;
                            string geoURL = constructuirGeoURL(domicilio);
                            LatLng latLng = GetLatitudLongitud(geoURL);

                            if (latLng != null)
                            {
                                socio.Latitud = latLng.latitud;
                                socio.Longitud = latLng.longitud;
                            }

                            dbContext.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.InnerException);
                    }

                    index++;
                }
                //dbContext.SaveChanges();
            }
        }

        public class LatLng
        {
            public string latitud { get; set; }
            public string longitud { get; set; }
        }

        static string constructuirGeoURL(string direccion)
        {
            string geoURL = "";
            string key = "&key=AIzaSyAEg7-84AYcNlPK0FZQm_zUAfFcYmOFleQ";

            direccion = direccion.Trim();
            direccion = direccion.Replace(" ", "+");

            geoURL = "https://maps.googleapis.com/maps/api/geocode/xml?address=";
            geoURL += direccion;
            geoURL += key;

            return geoURL;
        }

        static LatLng GetLatitudLongitud(string geoURL)
        {
            string xml = "";
            LatLng latLong = new LatLng();
            latLong.longitud = "0";
            latLong.latitud = "0";

            try
            {
                WebRequest objWebRequest = WebRequest.Create(geoURL);
                WebResponse objWebResponse = objWebRequest.GetResponse();
                Stream objWebStream = objWebResponse.GetResponseStream();

                using (StreamReader objStreamReader = new StreamReader(objWebStream))
                {
                    xml = objStreamReader.ReadToEnd();
                }


                using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                    reader.ReadToFollowing("lat");
                    latLong.latitud = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("lng");
                    latLong.longitud = reader.ReadElementContentAsString();
                }

            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
            return latLong;
        }
    }
}
