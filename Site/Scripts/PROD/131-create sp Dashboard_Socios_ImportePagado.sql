/****** Object:  StoredProcedure [dbo].[Dashboard_ImportePagado]    Script Date: 18/01/2016 12:41:45 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Dashboard_Socios_ImportePagado]
(
	@IDSocio int
)

AS
  
  SELECT  
  '' as label, 
  CAST(ISNULL(SUM(CASE Operacion WHEN 'Venta' THEN CalculoTicket ELSE (-1 * CalculoTicket) END),0) as int) as data
  FROM DashboardView
  inner join Tarjetas t on NumTarjetaCliente = t.Numero
  WHERE t.IDSocio=@IDSocio

GO


