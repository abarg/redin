alter table PromocionesPuntuales add IDComercio int  not null
ALTER TABLE PromocionesPuntuales  WITH CHECK ADD  CONSTRAINT FK_PromocionesPuntuales_Comercios FOREIGN KEY (IDComercio)
REFERENCES Comercios (IDComercio)
GO

ALTER TABLE PromocionesPuntuales CHECK CONSTRAINT FK_PromocionesPuntuales_Comercios
GO
