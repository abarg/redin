GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_TopSocios]    Script Date: 12/30/2015 09:59:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Marcas_TopSocios]
(
	@IDMarca int
)
AS

	declare @POSPropios bit
	SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca
	

	/*
	SELECT  --top 10 
	upper(uno) as uno, dos, tres, sum(cuatro) as cuatro
	FROM (*/
		select TOP 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		--inner join Marcas m on tr.IDMarca=m.IDMarca
		where t.IDSocio is not null and tr.Importe>1
		AND t.FechaBaja is null and t.TipoTarjeta='B'
		AND tr.Operacion not in ('Carga','Descarga')
		--AND ((t.IDMarca = @IDMarca) or (@POSPropios=1 and t.IDMarcaComercio=@IDMarca))
		AND t.IDMarca = @IDMarca --and ((@POSPropios=1 and t.IDMarca=@IDMarca) OR @POSPropios=0)
		--ORDER by cuatro desc
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	
		ORDER BY cuatro DESC
	/*
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc

	*/
	/*
	SELECT  --top 10 
	upper(uno) as uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select TOP 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr--TransaccionesMarcasView t
		inner join tarjetas t on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		--inner join Marcas m on tr.IDMarca=m.IDMarca
		where t.IDSocio is not null and tr.Importe>1
		AND t.FechaBaja is null and t.TipoTarjeta='B'
		AND tr.Operacion not in ('Carga','Descarga')
		--AND ((t.IDMarca = @IDMarca) or (@POSPropios=1 and t.IDMarcaComercio=@IDMarca))
		AND t.IDMarca = @IDMarca --and ((@POSPropios=1 and t.IDMarca=@IDMarca) OR @POSPropios=0)
		--ORDER by cuatro desc
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	
		ORDER BY cuatro DESC
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
	*/