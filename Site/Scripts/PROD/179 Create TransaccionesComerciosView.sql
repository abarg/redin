USE [RedIN-QA]
GO

/****** Object:  View [dbo].[TransaccionesComerciosView]    Script Date: 22/04/2016 02:43:11 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View 
 [dbo].[TransaccionesComerciosView] As 

select year(tr.FechaTransaccion) as anio ,  month(tr.FechaTransaccion) as mes ,DATENAME (month,tr.FechaTransaccion) as nombreMes, c.IDComercio,c.NombreFantasia as Comercio, f.IDFranquicia,f.NombreFantasia as Franquicia,m.IDMarca,m.Nombre as Marca,c.IDDomicilio,d.Domicilio as DomicilioComercio ,count(*) as CantTrans
		from Transacciones tr
		join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		join Tarjetas t on tr.NumTarjetaCliente = t.Numero
		join Marcas m on t.IDMarca = m.IDMarca
		join Franquicias f on f.IDFranquicia = c.IDFranquicia
		    left join Domicilios d on c.IDDomicilio = d.IDDomicilio and d.TipoDomicilio='C' and d.Entidad = 'C'
		where tr.Importe > 1
		group by  year(tr.FechaTransaccion) , month(tr.FechaTransaccion),DATENAME (month,tr.FechaTransaccion) ,c.IDComercio,c.NombreFantasia, c.IDDomicilio,d.Domicilio, f.IDFranquicia,f.NombreFantasia,m.IDMarca,m.Nombre

GO


