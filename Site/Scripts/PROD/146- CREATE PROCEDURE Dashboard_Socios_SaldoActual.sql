USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Gift_TotalSaldoCargado]    Script Date: 15/02/2016 09:05:51 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Dashboard_Socios_SaldoActual]
(
	@idSocio int
)

AS

  SELECT  
  '' as label, 
  SUM(Credito) as data
  FROM Tarjetas
  WHERE IDSocio=@idSocio and FechaBaja is null

GO


