GO

/****** Object:  View [dbo].[ReporteReprogramacionesPOSNET]    Script Date: 04/10/2015 05:43:54 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[ReporteReprogramacionesPOSNET] as

select
c.POSTerminal as NroTerminal,
m.Nombre as Marca,
c.NumEst as NumEst,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observacion
from Comercios c
inner join Marcas m on c.IDMarca=m.IDMarca
where c.Activo = 1 and c.POSSistema = 'POSNET' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'
and c.POSReprogramado=0 and c.POSFechaReprogramacion is null

GO

