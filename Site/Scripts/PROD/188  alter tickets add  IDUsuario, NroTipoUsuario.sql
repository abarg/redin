alter table Tickets
add  
 IDUsuarioAlta int null, --CREAR COMO NULL, DESPUES PONERLES UN VALOR, Y DESPUES CREARLOS COMO NOT NULL
 IDFranquicia int null
 FOREIGN KEY (IDFranquicia) REFERENCES Franquicias(IDFranquicia)

go

go

update Tickets  set  IDUsuarioAlta = isnull (	
	(
	select top 1 IDUsuarioAlta from Tickets 
		where IDUsuarioAlta=(
		select top 1 IDUsuario from TicketsDetalle td where Tickets.idticket=td.IDTicket 
		) and IDUsuarioAlta is null
	),
		
	(
	select  top 1 IDUsuarioAlta from Tickets 
		where IDUsuarioAlta=(
		select top 1 IDUsuario from TicketsDetalle td where Tickets.idticket=td.IDTicket 
		)and IDUsuarioAlta is not null
	)	
)

go

alter table Tickets
alter column  
 IDUsuarioAlta int not null
go

alter table TicketsDetalle
add  IDFranquicia int not null

go

ALTER TABLE TicketsDetalle 
add constraint IDFranquicia foreign key (IDFranquicia)
references Franquicias(IDFranquicia)

go 

update TicketsDetalle 
set IDFranquicia = (select td2.IDUsuario from TicketsDetalle td2 where td2.TipoUsuario = 2 and TicketsDetalle.IDDetalleTicket = td2.IDDetalleTicket)

go

go


alter table TicketsDetalle drop column TipoUsuario

