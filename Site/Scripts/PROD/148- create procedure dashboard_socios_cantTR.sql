USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Socios_ImporteAhorro]    Script Date: 15/02/2016 09:07:14 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Socios_CantTR]
(
	   @IDSocio int
)

AS
  SELECT  
  '' as label, 
  count(IDTransaccion) as data
  FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE  t.IDSocio=@IDSocio


GO


