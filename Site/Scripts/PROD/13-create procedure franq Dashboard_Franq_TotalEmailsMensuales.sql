

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Dashboard_Franq_TotalEmailsMensuales]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDFranquicia int)   
AS
BEGIN
	select 
	isnull (count(IDSocio),0) as data, 'Cant' as label 
	from  SociosView 
	where Email is not null AND Email <> '' and FechaAlta >= @FechaDesde AND FechaAlta <= @FechaHasta AND IDFranquicia = @IDFranquicia
END
GO


