GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Facturacion_Comercios]    Script Date: 07/09/2015 16:14:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Dashboard_Facturacion_Comercios]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime)
  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT ISNULL (SUM (ImporteAhorro +Importe),0)   as data FROM [DashboardView] where FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta 
END
