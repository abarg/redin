GO

/****** Object:  View [dbo].[ReporteReprogramacionesLAPOS]    Script Date: 04/10/2015 05:43:51 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[ReporteReprogramacionesLAPOS] as

select
c.POSTerminal as NroTerminal,
'Red IN' as MarcaVisa,
'032' as Moneda,
c.NumEst as NumEst,
7 as CodigoSolicitante,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observacion
from Comercios c
where c.Activo = 1 and c.POSSistema = 'LAPOS' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'
and c.POSReprogramado=0 and c.POSFechaReprogramacion is null

GO


