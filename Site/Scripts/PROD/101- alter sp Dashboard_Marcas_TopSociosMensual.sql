
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_TopSociosMensual]    Script Date: 12/30/2015 10:00:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_Marcas_TopSociosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDMarca int)    
AS
BEGIN
   --declare @POSPropios bit
	--SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca
	/*
	SELECT  --top 10 
	upper(uno) as uno, dos, tres, sum(cuatro) as cuatro
	FROM (*/
		/*select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesMarcasView t
		inner join tarjetas tr on t.Numero = tr.Numero
		--inner join Marcas m on tr.IDMarca=m.IDMarca
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND tr.IDMarca = @IDMarca  --and ((@POSPropios=1 and t.IDMarca=@IDMarca) OR @POSPropios=0)  
		and t.FechaTransaccion >= @FechaDesde and t.FechaTransaccion <= @FechaHasta
		group by t.Numero, t.Nombre, t.Apellido, t.Operacion	*/

		
		select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		--inner join Marcas m on tr.IDMarca=m.IDMarca
		where t.IDSocio is not null and tr.Importe>1
		AND t.FechaBaja is null and t.TipoTarjeta='B'
		AND tr.Operacion not in ('Carga','Descarga')
		--AND ((t.IDMarca = @IDMarca) or (@POSPropios=1 and t.IDMarcaComercio=@IDMarca))
		AND t.IDMarca = @IDMarca --and ((@POSPropios=1 and t.IDMarca=@IDMarca) OR @POSPropios=0)
		and tr.FechaTransaccion >= @FechaDesde and tr.FechaTransaccion <= @FechaHasta
		--ORDER by cuatro desc
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	
		ORDER BY cuatro DESC
	/*
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc*/
END
