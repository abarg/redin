
GO

/****** Object:  Table [dbo].[UsuariosMarcas]    Script Date: 26/04/2017 04:56:01 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UsuariosMultimarcas](
	[IDUsuario] [int] IDENTITY(1,1) NOT NULL,
	[IDMultimarca] [int] NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Email] [varchar](128) NOT NULL,
	[Pwd] [varchar](50) NOT NULL,
	--[Tipo] [char](1) NOT NULL,
	[Activo] [bit] NOT NULL,
	[FechaUltLogin] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[UsuariosMultimarcas]  WITH CHECK ADD FOREIGN KEY([IDMultimarca])
REFERENCES [dbo].[Multimarcas] ([IDMultimarca])
GO


