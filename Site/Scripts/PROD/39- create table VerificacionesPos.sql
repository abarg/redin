GO

/****** Object:  Table [dbo].[VerificacionesPOS]    Script Date: 04/10/2015 06:40:51 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[VerificacionesPOS](
	[IDVerificacionPOS] [int] IDENTITY(1,1) NOT NULL,
	[IDComercio] [int] NOT NULL,
	[FechaPrueba] [datetime] NOT NULL,
	[EstadoGift] [varchar](10) NULL,
	[Puntos_POSGift] [varchar](50) NULL,
	[Observaciones_POSGift] [text] NULL,
	[Puntos_POSCompras] [varchar](50) NULL,
	[Observaciones_POSCompras] [text] NULL,
	[EstadoCompras] [varchar](10) NULL,
	[Puntos_POSCanjes] [varchar](50) NULL,
	[Observaciones_POSCanjes] [text] NULL,
	[EstadoCanjes] [varchar](10) NULL,
	[UsuarioPrueba] [varchar](50) NULL,
	[ObservacionesGenerales] [text] NULL,
 CONSTRAINT [PK_VerificacionesPOS] PRIMARY KEY CLUSTERED 
(
	[IDVerificacionPOS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[VerificacionesPOS]  WITH CHECK ADD  CONSTRAINT [FK_VerificacionesPOS_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO

ALTER TABLE [dbo].[VerificacionesPOS] CHECK CONSTRAINT [FK_VerificacionesPOS_Comercios]
GO


