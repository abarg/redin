USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[TmpCostoTransaccional]    Script Date: 18/12/2015 01:13:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TmpCostoTransaccional](
	[NumeroTarjeta] [varchar](16) NOT NULL,
	[CostoTransaccionalSocio] [decimal](5, 2) NULL,
	[CostoTransaccionalMarca] [decimal](5, 2) NULL,
	[CostoFinal] [decimal](5, 2) NULL,
	[IDMarca] [int] NULL,
	[IDTransaccion] [int] NULL,
	[NumReferencia] [varchar](12) NULL,
	[TipoMensaje] [varchar](4) NULL,
	[TipoTransaccion] [varchar](6) NULL,
	[Operacion] [varchar](20) NULL,
	[Origen] [varchar](20) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


