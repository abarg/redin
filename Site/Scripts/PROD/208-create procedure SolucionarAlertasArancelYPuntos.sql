USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[SolucionarAlertasArancelYPuntos]    Script Date: 27/07/2016 01:58:39 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[SolucionarAlertasArancelYPuntos] as

CREATE TABLE #TempTableTarjetas(
	Numero varchar(100)
)

insert into #TempTableTarjetas
select distinct NumTarjetaCliente from Transacciones
where (Arancel is null or Puntos is null)

UPDATE Transacciones
  SET Importe =  (CAST(PuntosIngresados AS decimal(18,2)))/100,
  ImporteAhorro = (CAST(PuntosDisponibles AS decimal(18,2)))/100,
  Descuento = convert(int,codigopremio)
  WHERE Origen='Visa' and TipoMensaje='1100' and (TipoTransaccion='000000' or TipoTransaccion='220000')
  and PuntosIngresados<>'' and PuntosDisponibles<>'' 
  and (Arancel is null or Puntos is null) --and Origen='Visa' and TipoMensaje='1100' and (TipoTransaccion='000000' or TipoTransaccion='220000')


--Actualizo PuntosAContabilizar
UPDATE Transacciones
SET PuntosAContabilizar = (Importe-ImporteAhorro)
WHERE TipoTransaccion = '000000' and Origen = 'Visa'--Venta
and (Arancel is null or Puntos is null)

UPDATE Transacciones
SET PuntosAContabilizar = (((CAST(PuntosIngresados AS DECIMAL(18,2)))/10)*-1)
WHERE TipoTransaccion='220005' and TipoMensaje='1100' and Origen='Visa'--Devolucion giftcard
and (Arancel is null or Puntos is null)
 
UPDATE Transacciones
SET PuntosAContabilizar = (((CAST(PuntosIngresados AS DECIMAL(18,2))))*-1)
WHERE TipoTransaccion='000005' and TipoMensaje='1100' and Origen='Visa'--Canje
and (Arancel is null or Puntos is null)

UPDATE Transacciones
SET PuntosAContabilizar = ((CAST(PuntosIngresados AS DECIMAL(18,2)))*100)
WHERE TipoTransaccion='000005' and TipoMensaje='2200' and Origen='Visa'--Carga giftcard
and (Arancel is null or Puntos is null)

UPDATE Transacciones
SET PuntosAContabilizar = (((CAST(PuntosIngresados AS DECIMAL(18,2)))*100)*-1)
WHERE TipoTransaccion='220005' and TipoMensaje='2200' and Origen='Visa'--Devolucion giftcard
and (Arancel is null or Puntos is null)

UPDATE Transacciones
SET 
ImporteAhorro = (SELECT TOP 1 (t1.ImporteAhorro) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa'),
Descuento = (SELECT TOP 1 (t1.Descuento) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa')
WHERE TipoTransaccion = '220000' and Origen = 'Visa' 
and (Puntos is null or Arancel is null)

      
UPDATE Transacciones
--SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-100)
SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-1)
WHERE TipoTransaccion = '220000' and Origen = 'Visa' and TipoMensaje='1100'--Anulacion
and (Arancel is null or Puntos is null)

UPDATE Transacciones
--SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-100)
SET PuntosAContabilizar = (Importe-ImporteAhorro)
WHERE Origen = 'Visa' and TipoMensaje='1420' and TipoTransaccion='000005'--Anulacion online de canje
and (Puntos is null or Arancel is null)

UPDATE Transacciones
SET PuntosAContabilizar = isnull(PuntosAContabilizar,0)*100
WHERE Origen = 'Visa' and (Operacion='Canje' or (TipoMensaje='1420' and TipoTransaccion='000005'))--Canje o anulacion de canje
and (Puntos is null or Arancel is null)

UPDATE Transacciones
SET PuntosAContabilizar = PuntosAContabilizar*dbo.GetPuntosMultiplica(Transacciones.NumEst,Transacciones.NumTerminal, Transacciones.FechaTransaccion, Transacciones.NumTarjetaCliente)
WHERE Operacion not in ('Carga','Descarga','Canje')
and TipoTransaccion<>'000005' 
and (Puntos is null or Arancel is null)

--Pongo las descargas como negativas
UPDATE Transacciones
SET PuntosAContabilizar= PuntosAContabilizar*-1
WHERE Operacion='Descarga' and PuntosAContabilizar>0 
and (Puntos is null or Arancel is null)

UPDATE Transacciones
SET Arancel = dbo.GetArancel(Transacciones.NumEst,Transacciones.NumTerminal,Transacciones.NumTarjetaCliente, Transacciones.TipoMensaje, Transacciones.Operacion, Transacciones.TipoTransaccion, Transacciones.FechaTransaccion),
Puntos = dbo.GetPuntos(Transacciones.NumEst,Transacciones.NumTerminal,Transacciones.NumTarjetaCliente, Transacciones.TipoMensaje, Transacciones.Operacion, Transacciones.TipoTransaccion, Transacciones.FechaTransaccion)
where (Puntos is null or Arancel is null)


UPDATE Tarjetas
SET PuntosTotales = (
SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
),
Credito = (
SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
    FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
),
Giftcard = (
SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
--SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
)
where Tarjetas.Numero in (select Numero from #TempTableTarjetas)
      
GO


