USE [RedIN-QA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetPuntosMultiplica]    Script Date: 20/01/2016 10:27:26 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[GetPuntosMultiplica] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @Fecha datetime,
	@NumTarjeta varchar(100)
)

RETURNS int
AS

BEGIN

    DECLARE @puntos int
    DECLARE @dia int

    SET @dia = (select datepart(dw,@Fecha)-1)
	
    DECLARE @AffinityMarca char(4)
    DECLARE @AffinityTarjeta char(4)
	declare @IDMarca int

	/* PROMOCION VIP*/
	SET @puntos = ISNULL((select TOP 1 p.MultiplicaPuntosVip from PromocionesPuntuales p inner join Comercios c on p.IDComercio = c.IDComercio where
	NumEst = @NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),1)
	IF (@puntos =1) /* PROMOCION */
			SET @puntos = ISNULL((select TOP 1 p.MultiplicaPuntos from PromocionesPuntuales p inner join Comercios c on p.IDComercio = c.IDComercio where
			NumEst =@NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),1)

	IF (@puntos =1) 
	  BEGIN
		set @IDMarca= (select IDMarca from Comercios where NumEst= @NumEst and POSTerminal=@NumTarjeta)
		set @AffinityMarca=(select affinity from Marcas where IDMarca=@IDMarca)

		set @AffinityTarjeta=(select SUBSTRING(@NumTarjeta, 4,4))

		if(@AffinityTarjeta=@AffinityMarca)
			begin
				IF(@dia = 1)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip1 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 2)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 3)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 4)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 5)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 6)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
					SET @puntos = (SELECT TOP 1 MultiplicaPuntosVip7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
			end
		else
			begin
				IF(@dia = 1)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos1 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 2)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 3)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 4)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 5)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 6)
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
					SET @puntos = (SELECT TOP 1 MultiplicaPuntos7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
			end 
	 end


    return @puntos

END


GO


