USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[ArticulosManualAyuda]    Script Date: 15/06/2016 01:24:38 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ArticulosManualAyuda](
	[IDArticulosManualAyuda] [int] IDENTITY(1,1) NOT NULL,
	[IDManualAyuda] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Titulo] [varchar](100) NOT NULL,
	[HTML] [text] NOT NULL,
 CONSTRAINT [PK_ArticulosManualAyuda] PRIMARY KEY CLUSTERED 
(
	[IDArticulosManualAyuda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ArticulosManualAyuda]  WITH CHECK ADD  CONSTRAINT [FK_ArticulosManualAyuda_ManualAyuda] FOREIGN KEY([IDManualAyuda])
REFERENCES [dbo].[ManualAyuda] ([IDManualAyuda])
GO

ALTER TABLE [dbo].[ArticulosManualAyuda] CHECK CONSTRAINT [FK_ArticulosManualAyuda_ManualAyuda]
GO

ALTER TABLE [dbo].[ArticulosManualAyuda]  WITH CHECK ADD  CONSTRAINT [FK_ArticulosManualAyuda_Usuarios] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuarios] ([IDUsuario])
GO

ALTER TABLE [dbo].[ArticulosManualAyuda] CHECK CONSTRAINT [FK_ArticulosManualAyuda_Usuarios]
GO


