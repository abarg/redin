Create table ImagenesComercios(
IDImagenComercio int primary key identity(1,1) ,
IDComercio int foreign key (IDComercio) references Comercios(IDComercio) not null,
URL varchar(255)
)
