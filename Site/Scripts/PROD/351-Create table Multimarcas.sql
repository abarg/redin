USE [Nueva Redin]
GO

/****** Object:  Table [dbo].[Multimarcas]    Script Date: 27/04/2017 12:22:44 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Multimarcas](
	[IDMultimarca] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Prefijo] [varchar](10) NOT NULL,
	[FechaAlta] [smalldatetime] NOT NULL,
	[Affinity] [char](4) NOT NULL,
	[POSArancel] [decimal](10, 2) NOT NULL,
	[Color] [varchar](10) NOT NULL,
	[Logo] [varchar](50) NULL,
	[MostrarSoloTarjetasPropias] [bit] NOT NULL,
	[MostrarSoloPOSPropios] [bit] NOT NULL,
	[IDFranquicia] [int] NULL,
	[TipoCatalogo] [char](1) NOT NULL,
	[HabilitarPOSWeb] [bit] NOT NULL,
	[Codigo] [char](3) NOT NULL,
	[HabilitarGiftcard] [bit] NOT NULL,
	[HabilitarCuponIN] [bit] NOT NULL,
	[HabilitarSMS] [bit] NOT NULL,
	[CostoSMS] [decimal](5, 2) NOT NULL,
	[EnvioMsjBienvenida] [bit] NOT NULL,
	[MsjBienvenida] [varchar](160) NULL,
	[EnvioMsjCumpleanios] [bit] NOT NULL,
	[MsjCumpleanios] [varchar](160) NOT NULL,
	[IDComercioFacturanteSMS] [int] NULL,
	[EnvioEmailRegistroSocio] [bit] NOT NULL,
	[EnvioEmailCumpleanios] [bit] NOT NULL,
	[EnvioEmailRegistroComercio] [bit] NOT NULL,
	[EmailRegistroSocio] [text] NULL,
	[EmailCumpleanios] [text] NULL,
	[EmailRegistroComercio] [text] NULL,
	[EmailAlertas] [varchar](128) NULL,
	[CelularAlertas] [varchar](50) NULL,
	[EmpresaCelularAlertas] [varchar](50) NULL,
	[RazonSocial] [varchar](128) NULL,
	[CondicionIva] [varchar](50) NULL,
	[TipoDocumento] [varchar](15) NULL,
	[NroDocumento] [varchar](20) NULL,
	[IDDomicilio] [int] NULL,
	[CostoTransaccional] [decimal](5, 2) NULL,
	[CostoSeguro] [decimal](5, 2) NULL,
	[CostoPlusin] [decimal](5, 2) NULL,
	[CostoSMS2] [decimal](5, 2) NULL,
	[MostrarProductos] [bit] NOT NULL,
	[fechaCaducidad] [date] NULL,
	[fechaTopeCanje] [date] NULL,
	[TipoTarjeta] [varchar](20) NOT NULL,
	[POSMostrarFormaPago] [bit] NOT NULL,
	[POSMostrarNumeroTicket] [bit] NOT NULL,
	[POSFooter1] [varchar](40) NULL,
	[POSFooter2] [varchar](40) NULL,
	[POSFooter3] [varchar](40) NULL,
	[POSFooter4] [varchar](40) NULL,
	[POSMostrarMenuFidelidad] [bit] NOT NULL,
	[POSMostrarMenuGift] [bit] NOT NULL,
	[POSMostrarChargeGift] [bit] NOT NULL,
	[POSMostrarLOGO] [bit] NOT NULL,
 CONSTRAINT [PK_Multimarcas] PRIMARY KEY CLUSTERED 
(
	[IDMultimarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [MostrarSoloTarjetasPropias]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [MostrarSoloPOSPropios]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ('C') FOR [TipoCatalogo]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ('1') FOR [HabilitarPOSWeb]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((0)) FOR [Codigo]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((0)) FOR [HabilitarGiftcard]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ('REDIN') FOR [TipoTarjeta]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarFormaPago]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarNumeroTicket]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarMenuFidelidad]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarMenuGift]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarChargeGift]
GO

ALTER TABLE [dbo].[Multimarcas] ADD  DEFAULT ((1)) FOR [POSMostrarLOGO]
GO

ALTER TABLE [dbo].[Multimarcas]  WITH CHECK ADD FOREIGN KEY([IDComercioFacturanteSMS])
REFERENCES [dbo].[ComerciosVieja] ([IDComercio])
GO

ALTER TABLE [dbo].[Multimarcas]  WITH CHECK ADD  CONSTRAINT [FK_Multimarcas_Domicilios] FOREIGN KEY([IDDomicilio])
REFERENCES [dbo].[Domicilios] ([IDDomicilio])
GO

ALTER TABLE [dbo].[Multimarcas] CHECK CONSTRAINT [FK_Multimarcas_Domicilios]
GO

ALTER TABLE [dbo].[Multimarcas]  WITH CHECK ADD  CONSTRAINT [FK_Multimarcas_Franquicias] FOREIGN KEY([IDFranquicia])
REFERENCES [dbo].[Franquicias] ([IDFranquicia])
GO

ALTER TABLE [dbo].[Multimarcas] CHECK CONSTRAINT [FK_Multimarcas_Franquicias]
GO


