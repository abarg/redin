USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[IntegracionContabilium]    Script Date: 17/02/2016 01:41:39 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
alter table Pagos
add EnContabilium int null

go

update pagos set EnContabilium=0

go

alter table Pagos
alter column EnContabilium int not null

go
*/
--exec [IntegracionContabiliumPagos]

alter PROCEDURE [dbo].[IntegracionContabiliumPagos]

AS
	DECLARE @IDUsuario INT = 11
	DECLARE @Tipo varchar (1)='C' 
	DECLARE @Personeria varchar (1)='J' 
	DECLARE @TipoDestinatario varchar (1)='C'
	DECLARE @Modo varchar (1)='E'
	DECLARE @IDPuntoDeVenta INT =344
	DECLARE @Observaciones varchar (1)=''
	DECLARE @IDBanco INT=33
	DECLARE @IDCaja INT=2

	select 
	Comercios.IDComercio as Codigo, 
	Comercios.TipoDocumento,
	Comercios.NroDocumento,
	IDPago,
	NroComprobante,
	Pagos.Observaciones,
	FormaDePago,
	Importe,
	Fecha,
	NroFactura,
	/*RetGanancias,
	IIBB,
	N
	SUSS,
	Otros,*/
	0 as IDPersona
	into #temp
	from  [RedIN-QA].dbo.Pagos
	inner join [RedIN-QA].dbo.Comercios on Pagos.IDComercio=Comercios.IDComercio
	inner join [RedIN-QA].dbo.Domicilios on Domicilios.IDDomicilio=Comercios.IDDomicilioFiscal
	inner join [RedIN-QA].dbo.Ciudades on Ciudades.IDCiudad=Domicilios.Ciudad where Pagos.EnContabilium=0

	--Actualizo los idpersonas que existen
	update #temp 
	set IDPersona = isnull((select TOP 1 IDPersona from  ContabiliumProd.dbo.Personas p where #temp.TipoDocumento = p.TipoDocumento and #temp.NroDocumento = p.NroDocumento and p.IDUsuario=@IDUsuario),0)

	--2. Cobranzas cabecera
	insert into ContabiliumProd.dbo.Cobranzas 
	(IDUsuario,
	IDPuntoVenta,
	IDPersona,
	TipoDestinatario,
	TipoDocumento,
	NroDocumento,
	Modo,
	Tipo,
	FechaCobranza,
	ImporteTotal,
	Numero,
	FechaAlta,
	FechaProceso,
	Enviada,
	Observaciones)
	select  @IDUsuario as IDUsuario, @IDPuntoDeVenta as IDPuntoDeVenta, IDPersona
	,@TipoDestinatario as TipoDestinatario,t.TipoDocumento , t.NroDocumento,
	@Modo,'RC',
	Fecha as FechaCobranza, t.Importe,IDPago, 
	getdate() as FechaAlta,getdate() as FechaProceso, 1,Observaciones
	from #temp as t where t.idPersona>0

	--3. Cobranzas detalle
	insert into ContabiliumProd.[dbo].CobranzasDetalle
	(IDCobranza, IDComprobante,	Importe)
	select
	(select top 1 IDCobranza from ContabiliumProd.[dbo].Cobranzas c where c.Numero=t.IDPago and c.IDUsuario=@IDUsuario) as IDCobranza, 
	(select top 1 IDComprobante from ContabiliumProd.[dbo].Comprobantes c 
		where c.Numero= (select top 1 cast(splitdata as int) from  [RedIN-QA].dbo.Split(Numero,'-') order by 1 desc)
		and c.IDUsuario=@IDUsuario
	) as IDComprobante,
	Importe
	from #temp as t where t.idPersona>0
	

	--3. Cobranzas formas de pago por Banco
	insert into ContabiliumProd.[dbo].CobranzasFormasDePago
	(IDCobranza,
	FormaDePago,
	Importe,
	IDBanco)
	select
	(select top 1 IDCobranza from ContabiliumProd.[dbo].Cobranzas c where c.Numero=t.IDPago and c.IDUsuario=@IDUsuario) as IDCobranza, 
	FormaDePago,
	Importe,
	@IDBanco
	from #temp as t where t.idPersona>0
	and FormaDePago<>'Efectivo'

	--3. Cobranzas formas de pago por caja
	insert into ContabiliumProd.[dbo].CobranzasFormasDePago
	(IDCobranza,
	FormaDePago,
	Importe,
	IDCaja)
	select
	(select top 1 IDCobranza from ContabiliumProd.[dbo].Cobranzas c where c.Numero=t.IDPago and c.IDUsuario=@IDUsuario) as IDCobranza, 
	FormaDePago,
	Importe,
	@IDCaja
	from #temp as t where t.idPersona>0
	and FormaDePago='Efectivo'

	--4. Cobranzas retenciones
	insert into ContabiliumProd.[dbo].CobranzasRetenciones
	(IDCobranza,Tipo,Importe,NroReferencia)
	select
	(
		select top 1 IDCobranza from ContabiliumProd.[dbo].Cobranzas c where 
		c.Numero=(select  idpago from [RedIN-QA].dbo.Pagos where NroFactura=[RedIN-QA].dbo.Facturas.Numero)
		and c.IDUsuario=@IDUsuario
	
	) as IDCobranza, 
	(case when FacturasRetenciones.Tipo='GANANCIAS' then 'Ganancias' else FacturasRetenciones.Tipo end),
	FacturasRetenciones.Importe,
	FacturasRetenciones.NroRetencion
	from  [RedIN-QA].dbo.FacturasRetenciones
	inner join [RedIN-QA].dbo.Facturas on FacturasRetenciones.IDFactura=Facturas.IDFactura
	WHERE FacturasRetenciones.Importe>0

 
	-- 4 - los marco como procesados
	update  [RedIN-QA].dbo.Pagos 
	set EnContabilium=1  
	where EnContabilium=0 and IDPago in 
	(select Numero from ContabiliumProd.dbo.Cobranzas where IDUsuario=@IDUsuario)



