/****** Object:  StoredProcedure [dbo].[Dashboard_TotalTasaUsoMensual]    Script Date: 18/01/2016 11:54:23 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Socios_TotalTasaUsoMensual]
(
	@FechaDesde datetime,
  @FechaHasta datetime,
  @IDSocio int
)

AS
  declare @suma decimal(10,2)
  declare @cant decimal(10,2)
  

  SELECT @suma = COUNT(distinct NumTarjetaCliente) FROM Transacciones tr
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
   WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  and Importe>1 and Arancel is not null and Puntos is not null
  AND Operacion not in ('Carga','Descarga') and t.IDSocio=@IDSocio



  /*
  SELECT @suma =count(*) from (
  select distinct NumTarjetaCliente as cant
  FROM DashboardView
  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  group by NumTarjetaCliente) as T*/
  
  
  SELECT @cant = convert(decimal(10,2),count(IDTransaccion)) FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  and Importe>1 and Arancel is not null and Puntos is not null
  AND Operacion not in ('Carga','Descarga') and t.IDSocio=@IDSocio

  
  if(@suma is null or @suma=0)
	set @suma=1


  SELECT '' as label, @cant/@suma as data

GO


