alter table Comercios
add IDSubRubro Int null

ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_SubRubros] FOREIGN KEY([IDSubRubro])
REFERENCES [dbo].[Rubros] ([IDRubro])
GO

ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_SubRubros]
GO
