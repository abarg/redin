SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PruebasPOS](
	[IDPruebaPOS] [int] IDENTITY(1,1) NOT NULL,
	[EstadoTerminal] [varchar](50) NOT NULL,
	[Observaciones_EstadoTerminal] [text] NULL,
	[FechaPrueba_EstadoTerminal] [datetime] NOT NULL,
	[POSPuntos] [varchar](50) NULL,
	[Observaciones_POSPuntos] [text] NULL,
	[FechaPrueba_POSPuntos] [datetime] NULL,
	[POSGift] [varchar](50) NULL,
	[Observaciones_POSGift] [text] NULL,
	[FechaPrueba_POSGift] [datetime] NULL,
	[UsuarioPrueba] [varchar](50) NULL,
	[IDComercio] [int] NOT NULL,
 CONSTRAINT [PK_PruebasPOS] PRIMARY KEY CLUSTERED 
(
	[IDPruebaPOS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PruebasPOS]  WITH CHECK ADD  CONSTRAINT [FK_PruebasPOS_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO

ALTER TABLE [dbo].[PruebasPOS] CHECK CONSTRAINT [FK_PruebasPOS_Comercios]
GO


