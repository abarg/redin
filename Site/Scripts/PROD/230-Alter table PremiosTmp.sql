ALTER TABLE PremiosTmp ADD StockActual int, ValorPesos int, IDRubro int, StockMinimo int

go
ALTER TABLE PremiosTmp ADD FOREIGN KEY (IDMArca) REFERENCES Marcas(IDMarca),
FOREIGN KEY (IDRubro) REFERENCES Rubros(IDRubro)