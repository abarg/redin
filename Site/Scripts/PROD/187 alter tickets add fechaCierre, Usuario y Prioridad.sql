alter table Tickets
add  FechaCierre datetime null,
 UsuarioAlta varchar(50) not null default '',
 Prioridad varchar(50) not null default 'Baja'
go

--update tickets set  Prioridad= 'Baja'

go

update Tickets
set usuarioalta= isnull(
	(
	select usuario from Usuarios where IDUsuario=(
	select top 1 IDUsuario from TicketsDetalle where Tickets.idticket=TicketsDetalle.IDTicket
	and IDFranquicia is null
	)
	), 

	(
	select usuario from UsuariosFranquicias where IDUsuario=(
	select top 1 IDUsuario from TicketsDetalle where Tickets.idticket=TicketsDetalle.IDTicket
	and IDFranquicia is not null
	)
	)
)