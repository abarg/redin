CREATE PROCEDURE Dashboard_TarjetasPorMarca_Listado
as

select distinct m.Nombre as Marca, count(t.IDMarca) as Tarjetas 
from Marcas m
inner join Tarjetas t on t.IDMarca=m.IDMarca
where FechaAsignacion is not null 
group by m.Nombre
order by Tarjetas desc