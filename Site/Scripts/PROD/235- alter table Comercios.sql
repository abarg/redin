---ELIMINAR CONSTRAIN
alter table Comercios alter column  GifcardArancelCarga decimal (10,2)
alter table Comercios alter column GifcardArancelDescarga decimal (10,2)
alter table Comercios alter column CuponInArancel decimal (10,2)

USE [RedIN-QA]
GO

ALTER TABLE [dbo].[Comercios] ADD  DEFAULT ((0)) FOR [GifcardArancelCarga]
GO


USE [RedIN-QA]
GO

ALTER TABLE [dbo].[Comercios] ADD  DEFAULT ((0)) FOR [GifcardArancelDescarga]
GO


