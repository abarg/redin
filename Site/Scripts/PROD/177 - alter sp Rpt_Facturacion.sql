USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Rpt_Facturacion]    Script Date: 18/04/2016 04:47:25 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[Rpt_Facturacion]
 @fechaDesde datetime,
 @fechaHasta datetime
AS
BEGIN
    /*** Creo la table temporal ***/
    SELECT 
    POSTipo, POSSistema, POSTerminal,
  	SDS, NombreFantasia, NroDocumento, NumEst, FranqComercio, Marca, IDMarca,IDFranquicia,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImporteOriginal) WHEN 'Carga' THEN SUM(ImporteOriginal) ELSE SUM(ImporteOriginal*-1) END AS ImporteOriginal,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImporteAhorro) WHEN 'Carga' THEN SUM(ImporteAhorro) ELSE SUM(ImporteAhorro*-1) END AS ImporteAhorro,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImportePagado) WHEN 'Carga' THEN SUM(ImportePagado) ELSE SUM(ImportePagado*-1) END AS ImportePagado,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(CostoRedIn) WHEN 'Carga' THEN SUM(CostoRedIn) ELSE SUM(CostoRedIn*-1) END AS CostoRedIn,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Ticket) WHEN 'Carga' THEN SUM(Ticket) ELSE SUM(Ticket*-1) END AS Ticket,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Arancel) WHEN 'Carga' THEN SUM(Arancel) ELSE SUM(Arancel*-1) END AS Arancel,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Puntos) WHEN 'Carga' THEN SUM(Puntos) WHEN 'Canje' THEN 0 ELSE SUM(Puntos*-1) END AS Puntos,
    --CASE Operacion WHEN 'Venta' THEN SUM(NetoGrabado) ELSE SUM(NetoGrabado*-1) END AS NetoGrabado,
	CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(NetoGrabado) WHEN 'Carga' THEN SUM(NetoGrabado) WHEN 'Canje' THEN 0 ELSE SUM(NetoGrabado*-1) END AS NetoGrabado,
    
	CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(IVA) WHEN 'Carga' THEN SUM(IVA) WHEN 'Canje' THEN 0 ELSE SUM(IVA*-1) END AS IVA,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(TotalFactura) WHEN 'Carga' THEN SUM(TotalFactura) WHEN 'Canje' THEN 0 ELSE SUM(TotalFactura*-1) END AS TotalFactura,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(IIBB) WHEN 'Carga' THEN SUM(IIBB) WHEN 'Canje' THEN 0 ELSE SUM(IIBB*-1) END AS IIBB,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Tish) WHEN 'Carga' THEN SUM(Tish) WHEN 'Canje' THEN 0 ELSE SUM(Tish*-1) END AS Tish,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Canje' THEN 0 ELSE SUM(CostoPuntos) END AS CostoPuntos,
	CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Canje' THEN 0 ELSE SUM(CostoPos) END AS CostoPos,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(NetoTax) WHEN 'Carga' THEN SUM(NetoTax) WHEN 'Canje' THEN 0 ELSE SUM(NetoTax*-1) END AS NetoTax,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Franquicia) WHEN 'Carga' THEN SUM(Franquicia) WHEN 'Canje' THEN 0 ELSE SUM(Franquicia*-1) END AS Franquicia,
	
	/*
	CASE Operacion WHEN 'Venta' THEN SUM(IVA) ELSE SUM(IVA*-1) END AS IVA,
    CASE Operacion WHEN 'Venta' THEN SUM(TotalFactura) ELSE SUM(TotalFactura*-1) END AS TotalFactura,
    CASE Operacion WHEN 'Venta' THEN SUM(IIBB) ELSE SUM(IIBB*-1) END AS IIBB,
    CASE Operacion WHEN 'Venta' THEN SUM(Tish) ELSE SUM(Tish*-1) END AS Tish,
    SUM(CostoPuntos) AS CostoPuntos,
    SUM(CostoPos) AS CostoPos,
    CASE Operacion WHEN 'Venta' THEN SUM(NetoTax) ELSE SUM(NetoTax*-1) END AS NetoTax,
    CASE Operacion WHEN 'Venta' THEN SUM(Franquicia) ELSE SUM(Franquicia*-1) END AS Franquicia,*/
    SUM(CantOperaciones) AS CantOperaciones
    INTO #TMPFACTURACION
    FROM
    (
      SELECT 
        --CONVERT(varchar(10), tr.FechaTransaccion, 103) as Fecha,
    		--tr.Origen,
        c.POSTipo,
    		c.POSSistema,
    	  c.POSTerminal,
    		tr.Operacion,
    		c.SDS,
    		c.NombreFantasia,
        c.NroDocumento,
    		c.NumEst,
        f.NombreFantasia as FranqComercio,
		f.IDFranquicia as IDFranquicia,
    		--d.Ciudad as Localidad,
        m.Nombre as Marca,
        m.IDMarca,
    		ISNULL(SUM(tr.Importe),0) as ImporteOriginal,
    		ISNULL(SUM(tr.ImporteAhorro),0) AS ImporteAhorro,
    		ISNULL(SUM(tr.Importe - tr.ImporteAhorro),0) as ImportePagado,
    		ISNULL(SUM(tr.UsoRed),0) as CostoRedIn,
    		CAST(
          ISNULL(SUM(tr.Importe - tr.ImporteAhorro + tr.UsoRed),0) AS decimal(10,2)
        ) AS Ticket,
        CAST(
          ISNULL(SUM(tr.Arancel * ((Importe - ImporteAhorro) / 100)),0)  AS decimal(10,2)
        ) AS Arancel,
        CAST(
          ISNULL(SUM(tr.Puntos * ((Importe - ImporteAhorro) / 100)),0) AS decimal(10,2)
        ) AS Puntos,
        
    		--SUM(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos,
    		
    		ISNULL(SUM(((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed),0) as NetoGrabado,
    		ISNULL(SUM((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * (0.21)),0) as IVA,
    		ISNULL(SUM((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * (0.21))),0) as TotalFactura,
    		ISNULL(SUM(((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)),0) as IIBB,
    		ISNULL(SUM((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1)),0) as Tish,
    		ISNULL(SUM(-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))),0) as CostoPuntos,
    		SUM(-0.57) as CostoPos,
    		ISNULL(SUM( 
          ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed))
    		  + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)
    		  + ((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1))
    		  + ((-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))))
    		  + ((-0.57)) 
        ),0) as NetoTax,
    		ISNULL(SUM(
          ( ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed))
    		  + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)
    		  + ((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1))
    		  + ((-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))))
    		  + ((-0.57)) ) * (0.33)
        ),0) as Franquicia,
        COUNT(*) as CantOperaciones
      FROM Comercios c
  		INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  		LEFT JOIN Marcas m on m.IDMarca = c.IDMarca
      LEFT JOIN Franquicias f ON f.IDFranquicia = c.IDFranquicia
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN')
		)

      GROUP BY tr.Operacion, c.NroDocumento, c.POSSistema, c.POSTipo, c.SDS, c.NombreFantasia, c.POSSistema, c.NumEst, f.NombreFantasia,f.IDFranquicia, c.POSTerminal, m.IDMarca, m.Nombre
      --, tr.Origen, FechaTransaccion, 
    ) AS T
    GROUP BY Operacion, POSTipo, POSSistema, POSTerminal,--Fecha, 
  	SDS, NombreFantasia, NroDocumento, NumEst,FranqComercio, Marca, IDMarca,IDFranquicia
    
    SELECT 
      POSTipo, POSSistema, POSTerminal,
    	SDS, NombreFantasia, NroDocumento, NumEst, ISNULL(FranqComercio,'') AS FranqComercio, IDFranquicia ,
      ISNULL(Marca,'') as Marca, ISNULL(IDMarca,0) as IDMarca,
      ISNULL(SUM(ImporteOriginal),0) AS ImporteOriginal,
      ISNULL(SUM(ImporteAhorro),0) AS ImporteAhorro,
      ISNULL(SUM(ImportePagado),0) AS ImportePagado,
      ISNULL(SUM(CostoRedIn),0) AS CostoRedIn,
      ISNULL(SUM(Ticket),0) AS Ticket,
      ISNULL(SUM(Arancel),0) AS Arancel,
      ISNULL(SUM(Puntos),0) AS Puntos,
      ISNULL(SUM(NetoGrabado),0) AS NetoGrabado,
      ISNULL(SUM(IVA),0) AS IVA,
      ISNULL(SUM(TotalFactura),0) AS TotalFactura,
      ISNULL(SUM(IIBB),0) AS IIBB,
      ISNULL(SUM(Tish),0) AS Tish,
      ISNULL(SUM(CostoPuntos),0) AS CostoPuntos,
      ISNULL(SUM(CostoPos),0) AS CostoPos,
      ISNULL(SUM(NetoTax),0) AS NetoTax,
      ISNULL(SUM(Franquicia),0) AS Franquicia,
      ISNULL(SUM(CantOperaciones),0) AS CantOperaciones
      FROM #TMPFACTURACION
      GROUP BY POSTipo, POSSistema, POSTerminal, SDS, NombreFantasia, NroDocumento, NumEst,FranqComercio, Marca, IDMarca,IDFranquicia
	  

	  /*
	  SELECT 
      '' as POSTipo, '' as POSSistema, '' as POSTerminal,
     '' as SDS, '' as NombreFantasia, '' as NroDocumento, '' as NumEst, '' as FranqComercio, 0 IDFranquicia , '' as Marca,
		0 as IDMarca,
      0.0 AS ImporteOriginal,
      0.0 AS ImporteAhorro,
      0.0 AS ImportePagado,
      0.0 AS CostoRedIn,
      0.0 AS Ticket,
      0.0 AS Arancel,
      0.0 AS Puntos,
      0.0 AS NetoGrabado,
      0.0 AS IVA,
      0.0 AS TotalFactura,
      0.0 AS IIBB,
      0.0 AS Tish,
      0.0 AS CostoPuntos,
      0.0 AS CostoPos,
      0.0 AS NetoTax,
      0.0 AS Franquicia,
      0 AS CantOperaciones,

	  */

END



