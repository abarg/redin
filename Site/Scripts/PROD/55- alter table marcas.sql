alter table Marcas add RazonSocial varchar(128)
alter table Marcas add CondicionIva varchar(50)
alter table Marcas add TipoDocumento varchar(15)
alter table Marcas add NroDocumento varchar(20)
alter table Marcas add IDDomicilio int

ALTER TABLE [dbo].[Marcas]  WITH CHECK ADD  CONSTRAINT [FK_Marcas_Domicilios] FOREIGN KEY([IDDomicilio])
REFERENCES [dbo].[Domicilios] ([IDDomicilio])
GO

ALTER TABLE [dbo].[Marcas] CHECK CONSTRAINT [FK_Marcas_Domicilios]
GO