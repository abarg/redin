USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Rpt_CantidadSMS]    Script Date: 15/07/2016 03:19:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER  PROCEDURE [dbo].[Rpt_CantidadSMS] (@FechaDesde datetime,
  @FechaHasta datetime, 
  @IDMarca int,
  @Tipo varchar(3)
  )                  --Input parameter ,  Studentid of the student 
AS
BEGIN
		SELECT 
		sum(case s.Enviado when 1 then 1 else 0 end) as cantEnviados,
		sum(case s.Enviado when 0 then 1 else 0 end) as cantNoenviados,
		count(*) as cant,s.IDMarca, m.Nombre, 
		s.Costo,
		s.Tipo 
		FROM SMSEnvios s 
		left join Marcas m On s.IDMarca= m.IDMarca  
		where   (@IDMarca = 0 OR  s.IDMarca = @IDMarca) and s.FechaEnvio >= @FechaDesde AND
		s.FechaEnvio <= @FechaHasta  AND (@Tipo='' or s.Tipo=@Tipo)
		group by s.IDMarca,s.Costo, m.Nombre,s.Tipo
END




