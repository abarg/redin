alter table AlertasTarjetas add IDEmpresa int

ALTER TABLE [dbo].[AlertasTarjetas]  WITH CHECK ADD  CONSTRAINT [FK_AlertasTarjetas_Empresas] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresas] ([IDEmpresa])
GO

ALTER TABLE [dbo].[AlertasTarjetas] CHECK CONSTRAINT [FK_AlertasTarjetas_Empresas]
GO
