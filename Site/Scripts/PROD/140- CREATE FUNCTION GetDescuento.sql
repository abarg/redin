USE [RedIN-QA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDescuento]    Script Date: 21/01/2016 09:09:33 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE FUNCTION [dbo].[GetDescuento] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @Operacion varchar(20),
	@Fecha datetime,
	@TmpDescuento int
	
)

RETURNS int
AS

BEGIN

DECLARE @descuento int 
   SELECT @descuento = 0
if(@Operacion='Canje')
    select @descuento = 0
else
    BEGIN
			SET @descuento = ISNULL((select TOP 1 p.DescuentoVip from PromocionesPuntuales p inner join Comercios c on p.IDComercio = c.IDComercio where
				NumEst = @NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),0)
			if(@descuento=0)
				SET @descuento = ISNULL((select TOP 1 p.Descuento from PromocionesPuntuales p inner join Comercios c on p.IDComercio = c.IDComercio where
					NumEst = @NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),0)
			if(@descuento=0)
			SET @descuento = @TmpDescuento
	END
	return @descuento
end




GO


