USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[Productos]    Script Date: 30/10/2015 17:44:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Productos](
	[IDProducto] [int] IDENTITY(1,1) NOT NULL,
	[IDMarca] [int] NOT NULL,
	[Codigo] [varchar](50) NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](300) NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Activo] [bit] NULL,
	[IDFamilia] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_FamiliaProductos] FOREIGN KEY([IDFamilia])
REFERENCES [dbo].[FamiliaProductos] ([IDFamiliaProducto])
GO

ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_FamiliaProductos]
GO

ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Marcas] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marcas] ([IDMarca])
GO

ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_Marcas]
GO


