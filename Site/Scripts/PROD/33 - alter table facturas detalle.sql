alter table FacturasDetalle
alter column Concepto varchar(300)

go

alter table FacturasDetalle
add Iva decimal(5,2)

go

update FacturasDetalle
set Iva=21

go


alter table FacturasDetalle
alter column Iva decimal(5,2) not null

