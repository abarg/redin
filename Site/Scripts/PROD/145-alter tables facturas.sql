alter table Facturas
add TotalIva decimal(18,2) null

alter table FacturasDetalle
add IDPlanCuentaContabilium int null

go

update facturas
set TotalIva=(select sum(preciounitario*cantidad)*0.21 from facturasdetalle where FacturasDetalle.IDFactura=Facturas.IDFactura)

update facturas
set ImporteTotal = (select sum(preciounitario*cantidad)*1.21 from facturasdetalle where FacturasDetalle.IDFactura=Facturas.IDFactura)

update FacturasDetalle 
set IDPlanCuentaContabilium=3070
where Concepto like 'POS (LAPOS/POSNET) - Costo uso Red POS%'

update FacturasDetalle 
set IDPlanCuentaContabilium=3071
where Concepto like 'PUNTOS - Fondos recibidos por puntos de usuarios%'

update FacturasDetalle 
set IDPlanCuentaContabilium=3075
where Concepto like 'ARANCEL - Arancel por uso de Red%'
or concepto='Arancel giftcard uso de red'

update FacturasDetalle 
set IDPlanCuentaContabilium=769
where Concepto like 'CANJE - Aplicación de fondos por puntos de usuarios%'

update FacturasDetalle 
set IDPlanCuentaContabilium=3072
where Concepto like 'GIFTCARD - Arancel por uso de Tarjetas de Regalo%'
or concepto='Arancel giftcard carga' or concepto='Arancel giftcard descarga' 
or concepto='Arancel giftcard puntos'

go

alter table FacturasDetalle
alter column IDPlanCuentaContabilium int not null

alter table Facturas
alter column  TotalIva decimal(18,2) not null

go

