create view NuevosComerciosActivosView as

select distinct IDComercio,com.NombreFantasia  as Nombre from Transacciones t
inner join Comercios com  on com.POSTerminal = t.NumTerminal and 
com.NumEst = t.NumEst 
where IDComercio not in (select c.IDComercio 
from Transacciones tr inner join Comercios c on c.POSTerminal = tr.NumTerminal and 
c.NumEst = tr.NumEst where CAST(tr.FechaTransaccion AS DATE)  <  CAST((select getdate()) as  DATE) and tr.Operacion='Venta' 
and Importe>1) and (CAST(t.FechaTransaccion AS DATE) = CAST((select getdate()) as  DATE))

go