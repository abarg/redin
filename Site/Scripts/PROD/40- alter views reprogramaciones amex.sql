GO

/****** Object:  View [dbo].[ReporteReprogramacionesAMEX]    Script Date: 04/10/2015 05:43:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[ReporteReprogramacionesAMEX] as

select
c.NombreFantasia,
c.RazonSocial as RazonSocial,
c.NroDocumento as CUIT,
d.Domicilio as Direcci�n,
ci.Nombre as Localidad,
d.CodigoPostal as CP,
co.Telefono as Contacto,
c.POSTerminal as NroTerminal,
c.NumEst as NumEst,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observaciones
from Comercios c
inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
inner join Ciudades ci on d.Ciudad=ci.IDCiudad
inner join Contactos co on c.IDContacto=co.IDContacto
where c.Activo = 1 and c.POSSistema = 'AMEX' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'
and c.POSReprogramado=0 and c.POSFechaReprogramacion is null
GO


