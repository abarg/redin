alter table [FormularioTarjetas] add Telefono varchar(50) null;
alter table [FormularioTarjetas] add PaginaWeb varchar(50) null;
alter table [FormularioTarjetas] add Email varchar(50) null;
alter table [FormularioTarjetas] add Facebook varchar(50) null;
alter table [FormularioTarjetas] add BasesYCondiciones varchar(128) null;
alter table [FormularioTarjetas] add LogoDatReverso bit null;