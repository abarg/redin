USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TotalTasaUsoMensual]    Script Date: 30/12/2015 11:10:47 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Franq_TotalTasaUsoMensual]
(
  @IDFranquicia int,
  @FechaDesde datetime,
  @FechaHasta datetime
)

AS
	declare @suma decimal(10,2)
	declare @cant decimal(10,2)


	SELECT @suma = ISNULL(count(*),0) from (
	select distinct NumTarjetaCliente as cant
	FROM Transacciones
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	AND IDFranquicia = @IDFranquicia
	AND Importe>1 and Arancel is not null and Puntos is not null
    AND Operacion not in ('Carga','Descarga')
 
	group by NumTarjetaCliente) as T
	/*
	SELECT @suma = ISNULL(count(*),0) from (
	select distinct NumTarjetaCliente as cant
	FROM DashboardView
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	AND TarjetaFranquicia = @IDFranquicia
	group by NumTarjetaCliente) as T
	*/
	SELECT @cant = convert(decimal(10,2),count(IDTransaccion))
	FROM Transacciones
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	AND IDFranquicia = @IDFranquicia
	AND Importe>1 and Arancel is not null and Puntos is not null
	AND Operacion not in ('Carga','Descarga')

	/*

	SELECT @cant = convert(decimal(10,2),count(IDTransaccion))
	FROM DashboardView
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	AND TarjetaFranquicia = @IDFranquicia
  */
	if(@suma is null or @suma=0)
		set @suma=1


	SELECT '' as label, @cant/@suma as data
	
GO


