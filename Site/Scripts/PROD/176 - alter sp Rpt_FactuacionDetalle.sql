USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Rpt_FacturacionDetalle]    Script Date: 18/04/2016 03:48:28 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[Rpt_FacturacionDetalle]
 @fechaDesde datetime,
 @fechaHasta datetime
AS
BEGIN
    
    SELECT 
    POSTipo, POSSistema, POSTerminal, Operacion, FechaTransaccion, Origen,
  	SDS, NombreFantasia, NroDocumento, NumEst, ISNULL(FranqComercio,'') as FranqComercio, Marca, IDMarca, Tarjeta, ISNULL(Socio,'') AS Socio,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImporteOriginal WHEN 'Carga' THEN ImporteOriginal ELSE (ImporteOriginal*-1) END AS ImporteOriginal,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImporteAhorro WHEN 'Carga' THEN ImporteAhorro ELSE (ImporteAhorro*-1) END AS ImporteAhorro,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImportePagado WHEN 'Carga' THEN ImportePagado ELSE (ImportePagado*-1) END AS ImportePagado,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN CostoRedIn WHEN 'Carga' THEN CostoRedIn ELSE (CostoRedIn*-1) END AS CostoRedIn,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Ticket WHEN 'Carga' THEN Ticket ELSE (Ticket*-1) END AS Ticket,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Arancel WHEN 'Carga' THEN Arancel ELSE (Arancel*-1) END AS Arancel,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Puntos WHEN 'Carga' THEN Puntos WHEN 'Canje' THEN 0 ELSE (Puntos*-1) END AS Puntos,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN NetoGrabado WHEN 'Carga' THEN NetoGrabado WHEN 'Canje' THEN 0 ELSE (NetoGrabado*-1) END AS NetoGrabado,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN IVA WHEN 'Carga' THEN IVA WHEN 'Canje' THEN 0 ELSE (IVA*-1) END AS IVA,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN TotalFactura WHEN 'Carga' THEN TotalFactura WHEN 'Canje' THEN 0 ELSE (TotalFactura*-1) END AS TotalFactura,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN IIBB WHEN 'Carga' THEN IIBB WHEN 'Canje' THEN 0 ELSE (IIBB*-1) END AS IIBB,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Tish WHEN 'Carga' THEN Tish WHEN 'Canje' THEN 0 ELSE (Tish*-1) END AS Tish,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Canje' THEN 0 ELSE CostoPuntos END AS CostoPuntos,
	CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Canje' THEN 0 ELSE CostoPos END AS CostoPos,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN NetoTax WHEN 'Carga' THEN NetoTax WHEN 'Canje' THEN 0 ELSE (NetoTax*-1) END AS NetoTax,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Franquicia WHEN 'Carga' THEN Franquicia WHEN 'Canje' THEN 0 ELSE (Franquicia*-1) END AS Franquicia,
	IDFranquicia
    --SUM(CantOperaciones) AS CantOperaciones
    --INTO #TMPFACTURACIONDET
    FROM
    (
      SELECT 
        tr.FechaTransaccion,
    		tr.Origen,
        c.POSTipo,
    		c.POSSistema,
    	  c.POSTerminal,
    		tr.Operacion,
    		c.SDS,
    		c.NombreFantasia,
        t.Numero as Tarjeta,
        c.NroDocumento,
    		c.NumEst,
        f.NombreFantasia as FranqComercio,
		f.IDFranquicia as IDFranquicia,
    		--d.Ciudad as Localidad,
        ISNULL(m.Nombre,'') as Marca,
        m.IDMarca,
        (select Apellido+', '+ Nombre from Socios where Socios.IDSocio = t.IDSocio) as Socio,
    		ISNULL(tr.Importe,0) as ImporteOriginal,
    		ISNULL(tr.ImporteAhorro,0) AS ImporteAhorro,
    		ISNULL(tr.Importe - tr.ImporteAhorro,0) as ImportePagado,
    		ISNULL(tr.UsoRed,0) as CostoRedIn,
    		--CAST(
          ISNULL(tr.Importe - tr.ImporteAhorro + (tr.UsoRed * 1.21),0)-- AS decimal(10,2)
        --) 
        AS Ticket,
        --CAST(
          ISNULL(tr.Arancel * ((Importe - ImporteAhorro) / 100),0)--  AS decimal(10,2)
        --) 
        AS Arancel,
        --CAST(
          ISNULL(tr.Puntos * ((Importe - ImporteAhorro) / 100),0)-- AS decimal(10,2)
        --) 
        AS Puntos,
        
    		--SUM(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos,
    		
    		ISNULL(((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed,0) as NetoGrabado,
    		ISNULL((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * (0.21),0) as IVA,
    		ISNULL((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * (0.21)),0) as TotalFactura,
    		ISNULL(((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1),0) as IIBB,
    		ISNULL((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1),0) as Tish,
    		ISNULL(-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)),0) as CostoPuntos,
    		-0.57 as CostoPos,
    		ISNULL(
          ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed))
    		  + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)
    		  + ((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1))
    		  + ((-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))))
    		  + ((-0.57)) 
        ,0) as NetoTax,
    		ISNULL(
          ( ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed))
    		  + ((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)
    		  + ((((((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) * 0.035) * (-1)) * (0.1))
    		  + ((-(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))))
    		  + ((-0.57)) ) * (0.33)
        ,0) as Franquicia
      FROM Comercios c
  		INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  		LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      LEFT JOIN Franquicias f ON f.IDFranquicia = c.IDFranquicia
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN')
		)
      GROUP BY 
      tr.Origen, tr.FechaTransaccion, tr.Operacion, tr.Importe, tr.ImporteAhorro, tr.Puntos, tr.Arancel,tr.UsoRed,
      c.NroDocumento, c.POSSistema, c.POSTipo, c.SDS, c.NombreFantasia, c.POSSistema, c.NumEst, f.NombreFantasia, c.POSTerminal, 
      m.IDMarca, m.Nombre, t.Numero, t.IDSocio,f.IDFranquicia
    
    ) AS T
    /*GROUP BY 
      Origen, FechaTransaccion, Operacion, ImporteOriginal, ImporteAhorro, ImportePagado, Puntos, Arancel,CostoRedIn, Ticket
      NroDocumento, POSSistema, POSTipo, SDS, NombreFantasia, POSSistema, NumEst, POSTerminal, 
      IDMarca,Marca, Socio
    
    SELECT 
      Origen, FechaTransaccion,
      POSTipo, POSSistema, POSTerminal, Operacion,
    	SDS, NombreFantasia, NroDocumento, NumEst, ISNULL(Marca,'') as Marca, IDMarca, Socio,
      ISNULL(SUM(ImporteOriginal),0) AS ImporteOriginal,
      ISNULL(SUM(ImporteAhorro),0) AS ImporteAhorro,
      ISNULL(SUM(ImportePagado),0) AS ImportePagado,
      ISNULL(SUM(CostoRedIn),0) AS CostoRedIn,
      ISNULL(SUM(Ticket),0) AS Ticket,
      ISNULL(SUM(Arancel),0) AS Arancel,
      ISNULL(SUM(Puntos),0) AS Puntos,
      ISNULL(SUM(NetoGrabado),0) AS NetoGrabado,
      ISNULL(SUM(IVA),0) AS IVA,
      ISNULL(SUM(TotalFactura),0) AS TotalFactura,
      ISNULL(SUM(IIBB),0) AS IIBB,
      ISNULL(SUM(Tish),0) AS Tish,
      ISNULL(SUM(CostoPuntos),0) AS CostoPuntos,
      ISNULL(SUM(CostoPos),0) AS CostoPos,
      ISNULL(SUM(NetoTax),0) AS NetoTax,
      ISNULL(SUM(Franquicia),0) AS Franquicia,
      ISNULL(SUM(CantOperaciones),0) AS CantOperaciones
      FROM #TMPFACTURACIONDET
      GROUP BY Origen, FechaTransaccion, Operacion, POSTipo, POSSistema, POSTerminal, SDS, NombreFantasia, NroDocumento, NumEst, Marca, IDMarca, Socio
*/


/*
	  SELECT 
      '' as Origen, '' as FechaTransaccion, '' as POSSistema, '' as POSTerminal, '' as Operacion ,
     '' as SDS, '' as NombreFantasia, '' as NroDocumento, '' as NumEst, '' as FranqComercio, 0 IDFranquicia , '' as Marca,
		0 as IDMarca,
      0.0 AS ImporteOriginal,
      0.0 AS ImporteAhorro,
      0.0 AS ImportePagado,
      0.0 AS CostoRedIn,
      0.0 AS Ticket,
      0.0 AS Arancel,
      0.0 AS Puntos,
      0.0 AS NetoGrabado,
      0.0 AS IVA,
      0.0 AS TotalFactura,
      0.0 AS IIBB,
      0.0 AS Tish,
      0.0 AS CostoPuntos,
      0.0 AS CostoPos,
      0.0 AS NetoTax,
      0 AS Franquicia,
      0 AS CantOperaciones,
	  0 AS IDFranquicia

	  */

END