USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[tmpActualizarPremios]    Script Date: 29/09/2016 02:23:18 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[tmpActualizarPremios] 

AS
BEGIN
	
	DECLARE @Contador int;
	SET @Contador = 1;  
	DECLARE @Regs INT;
	SET @Regs = (SELECT MAX(IDPremio) FROM PremiosTmp);
	
	--Campos PremiosTmp
	DECLARE @IDPremio int
	DECLARE @Codigo varchar(10)
	DECLARE @ValorPuntos varchar(50)
	DECLARE @FechaVigenciaDesde datetime 
	DECLARE @FechaVigenciaHasta datetime 
	DECLARE @Descripcion varchar(20) 
	DECLARE @TipoMov char(1) 
	
	DECLARE @StockActual int
	DECLARE @ValorPesos int
	DECLARE @IDMarca int
	DECLARE @IDRubro int
	DECLARE @StockMinimo int

	--Actualizo los premios actuales
	UPDATE Premios 
	SET 
	[Codigo] = tmp.Codigo,
	[ValorPuntos] = tmp.ValorPuntos,
	[FechaVigenciaDesde] = tmp.FechaVigenciaDesde,
	[FechaVigenciaHasta] = tmp.FechaVigenciaHasta,
	[Descripcion] = tmp.Descripcion,
	[TipoMov] = tmp.TipoMov,
	[StockActual]=tmp.StockActual,
	[ValorPesos]=tmp.ValorPesos,
	[IDMarca] = tmp.IDMarca,
	[IDRubro]= tmp.IDRubro,
	[StockMinimo]=tmp.StockMinimo
	FROM PremiosTmp tmp WHERE tmp.Codigo = Premios.Codigo and Premios.IDMarca is null
  
	--Inserto los nuevos
	INSERT INTO Premios
	SELECT tmp.Codigo,
	tmp.ValorPuntos,
	tmp.FechaVigenciaDesde,
	tmp.FechaVigenciaHasta,
	tmp.Descripcion,
	tmp.TipoMov,'',tmp.StockActual,tmp.ValorPesos, tmp.IDMarca, tmp.IDRubro,tmp.StockMinimo
	FROM PremiosTmp tmp
	WHERE tmp.Codigo not in (select distinct Codigo from Premios)

END