USE [RedIN]
GO
/****** Object:  StoredProcedure [dbo].[ProcesarSociosTmp]    Script Date: 24/07/2017 12:17:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProcesarSociosTmp]
( 
	@idFranquicia int
)

AS

BEGIN
	--Inicializo
	UPDATE SociosTmp set Error=null,IDSocio=null, IDDomicilio=null,IDTarjeta=null,IDProvincia=null,IDCiudad=null
	
	--BEGIN TRY
		--BEGIN TRANSACTION

		--Valido Affinity
		UPDATE SociosTmp
		SET Error='Affinity inexistente'
		where Error is null and SociosTmp.Affinity not in (select Affinity from Marcas)
	
		--Valido tarjeta
		UPDATE SociosTmp
		SET IDTarjeta = (select TOP 1 Tarjetas.IDTarjeta from Tarjetas where Tarjetas.IDFranquicia=@idFranquicia and Tarjetas.Numero like '637117' + SociosTmp.Affinity+ SociosTmp.NroTarjeta+'%')
		where Error is null
	
		UPDATE SociosTmp
		SET Error='Tarjeta inexistente'
		where (IDTarjeta is null or IDTarjeta=0)
		and Error is null

		--Buscar el mismo socio de la tarjeta si ya la tiene asociada asi no lo creo devuelta
		UPDATE SociosTmp
		SET IDSocio = (select IDSocio from Tarjetas where Tarjetas.IDTarjeta=SociosTmp.IDTarjeta)
		where IDTarjeta is not null and IDTarjeta>0 and Error is null and IDSocio is null
	
		--Buscar el mismo socio por mail
		UPDATE SociosTmp
		SET IDSocio = (select TOP 1 IDSocio from Socios where Socios.Email=SociosTmp.Email)
		where Email<>'' AND Error is null and IDSocio is null
	
		--Actualizo el iddomicilio si ya encontre el socio
		UPDATE SociosTmp
		SET IDDomicilio = (select TOP 1 IDDomicilio from Socios where Socios.IDSocio= SociosTmp.IDSocio)
		where Error is null and IDSocio is not null

		--Actualizo las provincias
		UPDATE SociosTmp
		SET IDProvincia = (select top 1 IDProvincia from Provincias where Nombre=upper(SociosTmp.Provincia))
		where Error is null and IDSocio is null and IDDomicilio is null

		UPDATE SociosTmp
		SET Error='Provincia inexistente'
		where (IDProvincia is null or IDProvincia=0)
		and Error is null and IDDomicilio is null

		--Actualizo las ciudades
		UPDATE SociosTmp
		SET IDCiudad = (select top 1 IDCiudad from Ciudades where Nombre=upper(SociosTmp.Ciudad)  and IDProvincia = SociosTmp.IDProvincia)
		where Error is null and IDSocio is null and IDDomicilio is null

		UPDATE SociosTmp
		SET Error='Ciudad inexistente'
		where (IDCiudad is null or IDCiudad=0)
		and Error is null and IDDomicilio is null 
	
		--TODO:CHEQUEAR CIUDAD

		--Asigno las tarjetas a los socios que ya existen
		if( (select count(*) from SociosTmp  where IDSocio is not null and IDTarjeta is not null and Error is null) >0)
			begin
				UPDATE Tarjetas
				SET Tarjetas.IDSocio = (SELECT top 1 IDSocio from SociosTmp where SociosTmp.IDSocio is not null and SociosTmp.IDTarjeta is not null and SociosTmp.IDTarjeta=Tarjetas.IDTarjeta),
				FechaAsignacion = getdate()
				where Tarjetas.IDSocio is null
			end

		--TODO: CREAR SOCIOS, DOMICILIOS Y ASIGNAR TARJETAS
		DECLARE @Nombre as varchar(100)
		DECLARE @Apellido as varchar(100)
		DECLARE @Email as varchar(128)
		DECLARE @FechaNacimiento as smalldatetime
		DECLARE @Sexo as char(1)
		DECLARE @TipoDocumento as char(3)
		DECLARE @NroDocumento as varchar(20)
		DECLARE @Telefono as varchar(50)
		DECLARE @Celular as varchar(50)
		DECLARE @Empresa as varchar(50)
		DECLARE @Observaciones as varchar(8000)
		DECLARE @Pais as varchar(50)
		DECLARE @Provincia as varchar(50)
		DECLARE @Ciudad as varchar(100)
		DECLARE @Domicilio as varchar(100)
		DECLARE @CodigoPostal as varchar(10)
		DECLARE @TelefonoDom as varchar(50)
		DECLARE @Fax as varchar(50)
		DECLARE @PisoDepto as varchar(10)
		DECLARE @IDSocio as int
		DECLARE @IDDomicilio as int
		DECLARE @IDTarjeta as int
		DECLARE @IDProvincia as int
		DECLARE @IDCiudad as int

		DECLARE SociosInfo CURSOR FOR 
		SELECT Nombre,Apellido,Email,FechaNacimiento,Sexo,TipoDocumento,NroDocumento,Telefono,Celular,Empresa,Observaciones,Pais,Provincia,Ciudad,Domicilio,CodigoPostal,TelefonoDom,Fax,PisoDepto,IDSocio,IDDomicilio,IDTarjeta,IDProvincia,IDCiudad
		FROM SociosTmp WHERE Error is null
		
		DECLARE @IDDomAux int
		DECLARE @IDSocioAux int
	
		OPEN SociosInfo
		FETCH NEXT FROM SociosInfo INTO @Nombre, @Apellido, @Email, @FechaNacimiento, @Sexo, @TipoDocumento, @NroDocumento, @Telefono, @Celular, @Empresa, @Observaciones, @Pais, @Provincia, @Ciudad, @Domicilio, @CodigoPostal, @TelefonoDom, @Fax, @PisoDepto, @IDSocio, @IDDomicilio, @IDTarjeta, @IDProvincia,@IDCiudad
		WHILE @@fetch_status = 0
		BEGIN
			--si el socio ya existe, asigno la tarjeta
			IF(@IDSocio IS NOT NULL AND @IDSocio>0)
				BEGIN
					UPDATE TARJETAS
					SET IDSocio = @IDSocio
					WHERE IDTarjeta = @IDTarjeta
				
					--DECLARE @IDCiudadAux int
					SET @IDProvincia = (select top 1 IDProvincia from Provincias where Nombre= upper(@Provincia))
					SET @IDCiudad = (select top 1 IDCiudad from Ciudades where Nombre = upper(@Ciudad) and IDProvincia = @IDProvincia)
						select @IDProvincia , @idciudad

					UPDATE Domicilios
					SET Ciudad = @IDCiudad
					WHERE IDDomicilio = @IDDomicilio
					update socios set NroDocumento=@NroDocumento where IDSocio =@IDSocio
				END
			ELSE--Debo crear el socio, el domicilio y asignar la tarjeta
				BEGIN
					--DECLARE @IDCiudadAux2 int
					--SET @IDProvincia = (select top 1 IDProvincia from Provincias where Nombre=@Provincia)
					--SET @IDCiudadAux2 = (select top 1 IDCiudad from Ciudades where Nombre = @Ciudad and IDProvincia = @IDProvincia) 

					INSERT INTO [dbo].[Domicilios]
					VALUES ('S','Argentina', @IDProvincia,@IDCiudad, @Domicilio, @CodigoPostal, @TelefonoDom,@Fax, @PisoDepto,'A',getdate(),'S', null,null)
					
					SET @IDDomAux = SCOPE_IDENTITY()

					INSERT INTO [dbo].[Socios]
					([Nombre],[Apellido],[Email],[FechaNacimiento],[Sexo],[TipoDocumento],[NroDocumento],[Telefono],[Celular],[EmpresaCelular],[FechaAlta],[IDDomicilio]
					,[Observaciones],[TipoReg],[TipoMov],[ImpDisponible],[Fecha],[AffinityBenef],[NroCuenta],[IDTransaccion]
					,[Activo],[Foto],[Pwd],[AuthTipo],[AuthID],[IDFranquiciaAlta])
					VALUES (@Nombre,@Apellido,@Email,@FechaNacimiento,@Sexo,@TipoDocumento,@NroDocumento,@Telefono,@Celular,@Empresa,getdate(),@IDDomAux,
					@Observaciones,'D','A',null,null,null,null,null,1,null,null,null,null,@idFranquicia)

					SET @IDSocioAux = SCOPE_IDENTITY()

					UPDATE TARJETAS
					SET IDSocio = @IDSocioAux
					WHERE IDTarjeta = @IDTarjeta
					   
				END
		
			FETCH NEXT FROM SociosInfo INTO @Nombre, @Apellido, @Email, @FechaNacimiento, @Sexo, @TipoDocumento, @NroDocumento, @Telefono, @Celular, @Empresa, @Observaciones, @Pais, @Provincia, @Ciudad, @Domicilio, @CodigoPostal, @TelefonoDom, @Fax, @PisoDepto, @IDSocio, @IDDomicilio, @IDTarjeta, @IDProvincia,@IDCiudad
		END
		CLOSE SociosInfo
		DEALLOCATE SociosInfo

		--COMMIT TRANSACTION
	/*END TRY
	BEGIN CATCH
		-- Execute error retrieval routine.
		EXECUTE usp_GetErrorInfo

		-- Test XACT_STATE:
			-- If 1, the transaction is committable.
			-- If -1, the transaction is uncommittable and should 
			--     be rolled back.
			-- XACT_STATE = 0 means that there is no transaction and
			--     a commit or rollback operation would generate an error.

		-- Test whether the transaction is uncommittable.
		--ROLLBACK TRANSACTION
		
		-- Test whether the transaction is committable.
		/*IF (XACT_STATE()) = 1
		BEGIN
			PRINT
				N'The transaction is committable.' +
				'Committing transaction.'
			COMMIT TRANSACTION
		END*/
	END CATCH*/

END


