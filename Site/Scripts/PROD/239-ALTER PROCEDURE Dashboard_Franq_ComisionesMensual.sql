USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_ComisionesMensual]    Script Date: 04/10/2016 12:57:50 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Franq_ComisionesMensual]
(
  @IDFranquicia int,
  @Tipo char(4),
  @FechaDesde datetime,
  @FechaHasta datetime
)

AS
	declare @total decimal(18,2)
	
  /*declare @arancel decimal(8,2)
  declare @comisionTpCp decimal(8,2)
  declare @comisionTtCp decimal(8,2)
  declare @comisionTpCt decimal(8,2)

  select @arancel=Arancel, @comisionTpCp=comisionTpCp,@comisionTtCp=comisionTtCp, @comisionTpCt=comisionTpCt
  from Franquicias where IDFranquicia=@IDFranquicia
  */
  if(@Tipo='TtCp')
  begin
  
    SELECT @total = 
     isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
      dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      --(CalculoArancel*@comisionTtCp)/100 
      ELSE 
      (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
      --(-1 * ((CalculoArancel*@comisionTtCp)/100)) END),0) as decimal(18,2))
    FROM Comercios c
  	  INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )

  	/*SELECT @suma = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
    inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  	AND c.IDFranquicia = @IDFranquicia AND t.IDFranquicia <> @IDFranquicia
    AND Operacion='Venta' AND tr.Importe>1 
  	
  	SELECT @resta = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
  	inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
    AND c.IDFranquicia = @IDFranquicia AND t.IDFranquicia <> @IDFranquicia
  	AND  Operacion in ('Anulacion', 'Devolucion')
  	AND tr.Importe>1 */
  
  end
  if(@Tipo='TpCp')
  begin

    SELECT @total = 
    isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
      dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      --(CalculoArancel*@comisionTtCp)/100 
      ELSE 
       (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
    
    
    ---CAST(ISNULL(SUM(CASE Operacion WHEN 'Venta' THEN (CalculoArancel*@comisionTtCp)/100 ELSE (-1 * ((CalculoArancel*@comisionTtCp)/100)) END),0) as decimal(18,2))
     FROM Comercios c
  	  INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )


  	/*SELECT @suma = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
    inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  	AND c.IDFranquicia = @IDFranquicia AND t.IDFranquicia = @IDFranquicia
    AND Operacion='Venta' AND tr.Importe>1 
  	
  	SELECT @resta = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
  	inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
    AND c.IDFranquicia = @IDFranquicia AND t.IDFranquicia = @IDFranquicia
  	AND  Operacion in ('Anulacion', 'Devolucion')
  	AND tr.Importe>1 */
  
  end
  if(@Tipo='TpCt')
  begin

    SELECT @total = 
    isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
       dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      --(CalculoArancel*@comisionTtCp)/100 
      ELSE 
        (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
    
    --CAST(ISNULL(SUM(CASE Operacion WHEN 'Venta' THEN (CalculoArancel*@comisionTtCp)/100 ELSE (-1 * ((CalculoArancel*@comisionTtCp)/100)) END),0) as decimal(18,2))
    FROM Comercios c
  	  INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )
  	/*SELECT @suma = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
    inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
  	AND c.IDFranquicia <> @IDFranquicia AND t.IDFranquicia = @IDFranquicia
    AND Operacion='Venta' AND tr.Importe>1 
  	
  	SELECT @resta = (((isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))*@comisionTtCp)/100)
  	FROM Transacciones tr
  	inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  	inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
    AND c.IDFranquicia <> @IDFranquicia AND t.IDFranquicia = @IDFranquicia
  	AND  Operacion in ('Anulacion', 'Devolucion')
  	AND tr.Importe>1 */
  
  end
  
	SELECT '' as label, @total as data