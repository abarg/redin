USE [RedIN-QA]
GO
/****** Object:  Table [dbo].[Procesos]    Script Date: 30/03/2016 01:33:25 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Procesos](
	[IDProceso] [int] IDENTITY(1,1) NOT NULL,
	[TipoArchivo] [int] NOT NULL,
	[FechaActualizacion] [datetime] NOT NULL,
	[NroProceso] [int] NOT NULL,
 CONSTRAINT [PK_Procesos] PRIMARY KEY CLUSTERED 
(
	[IDProceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Procesos] ON 

GO
INSERT [dbo].[Procesos] ([IDProceso], [TipoArchivo], [FechaActualizacion], [NroProceso]) VALUES (1, 1, CAST(0x0000A5C500000000 AS DateTime), 1)
GO
INSERT [dbo].[Procesos] ([IDProceso], [TipoArchivo], [FechaActualizacion], [NroProceso]) VALUES (2, 2, CAST(0x0000A5C500000000 AS DateTime), 2)
GO
INSERT [dbo].[Procesos] ([IDProceso], [TipoArchivo], [FechaActualizacion], [NroProceso]) VALUES (3, 4, CAST(0x0000A5C500000000 AS DateTime), 3)
GO
INSERT [dbo].[Procesos] ([IDProceso], [TipoArchivo], [FechaActualizacion], [NroProceso]) VALUES (4, 5, CAST(0x0000A5C500000000 AS DateTime), 4)
GO
INSERT [dbo].[Procesos] ([IDProceso], [TipoArchivo], [FechaActualizacion], [NroProceso]) VALUES (5, 6, CAST(0x0000A5C500000000 AS DateTime), 5)
GO
SET IDENTITY_INSERT [dbo].[Procesos] OFF
GO
