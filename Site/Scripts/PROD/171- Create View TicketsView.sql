USE [RedIN-QA]
GO

/****** Object:  View [dbo].[TicketsView]    Script Date: 08/04/2016 03:23:34 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create view [dbo].[TicketsView] as
select t.IDTicket,t.Estado,t.FechaAlta, td.IDUsuario , u.Usuario as NombreUsuario, td.TipoUsuario ,ta.Nombre as NombreArea,f.NombreFantasia
	
from Tickets t 
 join TicketsDetalle td on td.IDTicket = t.IDTicket
 join TicketsArea ta on t.IDArea = ta.IDAreaTicket
 join UsuariosFranquicias u on td.IDUsuario = u.IDUsuario
join Franquicias f on f.IDFranquicia = td.IDUsuario where TipoUsuario = 2
group by t.IDTicket,t.Estado,t.FechaAlta,td.IDUsuario,u.Usuario,td.TipoUsuario,ta.Nombre,f.NombreFantasia

UNION 

select t.IDTicket,t.Estado,t.FechaAlta, td.IDUsuario, u.Usuario as NombreUsuario, td.TipoUsuario ,ta.Nombre as NombreArea, '' AS NombreFantasia
	
from Tickets t 
 join TicketsDetalle td on td.IDTicket = t.IDTicket
 join TicketsArea ta on t.IDArea = ta.IDAreaTicket
 join Usuarios u on td.IDUsuario = u.IDUsuario
 where TipoUsuario = 1
group by t.IDTicket,t.Estado,t.FechaAlta,td.IDUsuario,u.Usuario,td.TipoUsuario,ta.Nombre


GO

