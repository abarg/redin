
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_CantidadSociosPlusIN]    Script Date: 02/02/2016 09:53:51 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[Dashboard_Franq_CantidadSociosPlusIN]  
(@IDFranquicia int)   
WITH EXEC AS CALLER
AS

	select '' as label , count (*) as data from Socios where NroCuentaPlusIN is not null and IDFranquiciaAlta=@IDFranquicia

GO


