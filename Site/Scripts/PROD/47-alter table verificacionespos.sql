alter table [VerificacionesPOS] add [IDUsuario] [int] NOT NULL

ALTER TABLE [VerificacionesPOS]  WITH CHECK ADD  CONSTRAINT [FK_VerificacionesPOS_Usuarios] FOREIGN KEY([IDUsuario])
REFERENCES [Usuarios] ([IDUsuario])
GO

ALTER TABLE [VerificacionesPOS] CHECK CONSTRAINT [FK_VerificacionesPOS_Usuarios]
GO
