USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[CrearTransaccionesCostoTransaccional]    Script Date: 18/12/2015 01:14:16 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[CrearTransaccionesCostoTransaccional]
(
	@NumEst varchar (20),
	@NumTerminal varchar (10),
	@FechaTransaccion datetime
)
AS
BEGIN

	--1-vacio la tabla tmp
	truncate table TmpCostoTransaccional

	--2 lleno la tabla
	INSERT INTO TmpCostoTransaccional
	SELECT NumTarjetaCliente,s.CostoTransaccional as CostoTransaccionalSocio, m.CostoTransaccional as CostoTransaccionalMarca, null, m.IDMarca, t.IDTransaccion,t.NumReferencia,t.TipoMensaje,t.TipoTransaccion,t.Operacion, t.Origen
	from Transacciones t 
	inner join Tarjetas ta on ta.Numero=t.NumTarjetaCliente
	inner join  Marcas m on t.IDMarca=m.IDMarca 
	left join Socios s on ta.IDSocio=s.IDSocio 
	where Operacion='Venta'
	and ta.FechaBaja is null and YEAR(t.FechaTransaccion)=YEAR (@FechaTransaccion)AND month(t.FechaTransaccion)=month (@FechaTransaccion) AND day(t.FechaTransaccion)=DAY(@FechaTransaccion)
	and ((s.CostoTransaccional is not null and s.CostoTransaccional>0) or (m.CostoTransaccional is not null and m.CostoTransaccional>0))
	--order by t.NumTarjetaCliente

	--3.1 - elimino las TR que pertenezcan a una marca que no tengan habilitado elposweb
	delete TmpCostoTransaccional WHERE Origen <> 'Visa' and IDMarca in (select IDMarca from Marcas where HabilitarPOSWeb=0)

	--3.2 elimino las anulaciones
	/*UPDATE TmpCostoTransaccional
    SET 
    IDTransaccion = (SELECT TOP 1 (t1.IDTransaccion) FROM Transacciones t1 WHERE t1.NumReferencia=TmpCostoTransaccional.NumRefOriginal AND t1.TipoTransaccion='000000' )
    WHERE TipoTransaccion = '220000' and TipoMensaje='1100'
		
	UPDATE TmpCostoTransaccional
    SET 
    IDTransaccion = (SELECT TOP 1 (t1.IDTransaccion) FROM Transacciones t1 WHERE t1.NumReferencia=TmpCostoTransaccional.NumRefOriginal AND t1.TipoTransaccion='000000' )
    WHERE TipoTransaccion = '000000' and TipoMensaje='1420'

		
	delete TmpCostoTransaccional where IDTransaccion is not null 
	Delete TmpCostoTransaccional where Operacion='Anulacion'*/

	DELETE TmpCostoTransaccional
    WHERE 
	Origen = 'Visa'
	and TipoMensaje='1100' and TipoTransaccion = '220000' --Anulacion
	and NumReferencia in (
		SELECT t1.NumReferencia FROM Transacciones t1 WHERE t1.NumRefOriginal=TmpCostoTransaccional.NumReferencia AND t1.TipoTransaccion='220000' AND t1.Origen='Visa'
	)

	DELETE TmpCostoTransaccional
    WHERE 
	Origen = 'Visa'
    and TipoMensaje='1420' and TipoTransaccion='000000'--Anulacion online
	and NumReferencia in (
		SELECT t1.NumReferencia FROM Transacciones t1 WHERE t1.NumRefOriginal=TmpCostoTransaccional.NumReferencia AND t1.TipoTransaccion='220000' AND t1.Origen='Visa'
	)
		
	--3.3 evaluo qu� se debe cobrar

	UPDATE TmpCostoTransaccional
	SET CostoFinal = CASE WHEN CostoTransaccionalSocio > 0 THEN CostoTransaccionalSocio ELSE CostoTransaccionalMarca END

	--4 creo las TR

	insert into Transacciones  (
				[NumTarjetaCliente]
				,[Importe]
				,[PuntosAContabilizar]
				,[NumEst]
				,[NumTerminal]
				,[TipoMensaje]
				,[TipoTransaccion]
				,[FechaTransaccion]
				,[NumCupon]
				,[NumReferencia]
				,[NumRefOriginal]
				,[CodigoPremio]
				,[PuntosDisponibles]
				,[Origen]
				,[Operacion]
				,[Descripcion]
				,[ImporteAhorro]
				,[Descuento], UsoRed, Arancel, Puntos, Usuario)
	select  NumeroTarjeta as NumTarjetaCliente, CostoFinal as Importe, CAST (((CostoFinal * 100)*-1) as int) as PuntosAContabilizar,
			@NumEst,@NumTerminal,'1100','000005',@FechaTransaccion,'','','','',
			'000000000000','Web','Canje', 'COSTO TRANSACCIONAL MENSUAL', 0, 
			0,0,0, 1,'proceso_costotransaccional' 
			from TmpCostoTransaccional where CostoFinal>0

		
	UPDATE Transacciones
	SET PuntosIngresados= CAST (Importe AS varchar(12))
	WHERE Operacion='Canje' and YEAR(FechaTransaccion)=YEAR(@FechaTransaccion) AND month(FechaTransaccion)=month(@FechaTransaccion) AND day(FechaTransaccion)=DAY(@FechaTransaccion) and usuario='proceso_costotransaccional' and NumEst=@NumEst and NumTerminal=@NumTerminal

	UPDATE Transacciones
	SET IDMarca= dbo.GetMarcaTarjeta(NumTarjetaCliente) WHERE  YEAR(FechaTransaccion)=YEAR(@FechaTransaccion) AND month(FechaTransaccion)=month(@FechaTransaccion) AND day(FechaTransaccion)=DAY(@FechaTransaccion) and usuario='proceso_costotransaccional' and NumEst=@NumEst and NumTerminal=@NumTerminal
    
	UPDATE Transacciones
	SET IDFranquicia= dbo.GetFranquiciaTarjeta(NumTarjetaCliente) WHERE YEAR(FechaTransaccion)=YEAR(@FechaTransaccion) AND month(FechaTransaccion)=month(@FechaTransaccion) AND day(FechaTransaccion)=DAY(@FechaTransaccion)and usuario='proceso_costotransaccional' and NumEst=@NumEst and NumTerminal=@NumTerminal


	--5 actualizar puntos por tarjeta
	UPDATE Tarjetas
	SET PuntosTotales = (
	SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
	),
	Credito = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
		FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
	),
	Giftcard = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
	--SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
	FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
	)
	WHERE Tarjetas.Numero IN (SELECT DISTINCT Numero FROM TmpCostoTransaccional)

	 SELECT COUNT (*) FROM TmpCostoTransaccional

END










GO


