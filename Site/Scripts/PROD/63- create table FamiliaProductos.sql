USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[FamiliaProductos]    Script Date: 30/10/2015 17:43:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FamiliaProductos](
	[IDFamiliaProducto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[IDMarca] [int] NOT NULL,
 CONSTRAINT [PK__FamiliaP__D02E90B89A0E3361] PRIMARY KEY CLUSTERED 
(
	[IDFamiliaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FamiliaProductos]  WITH CHECK ADD  CONSTRAINT [FK_FamiliaProductos_Marcas] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marcas] ([IDMarca])
GO

ALTER TABLE [dbo].[FamiliaProductos] CHECK CONSTRAINT [FK_FamiliaProductos_Marcas]
GO


