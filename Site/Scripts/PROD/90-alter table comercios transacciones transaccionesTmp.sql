

alter table Transacciones alter column Arancel decimal(10,2)
alter table Transacciones alter column Puntos decimal(10,2)

alter table Transacciones alter column PuntosAContabilizar decimal(10,2)

alter table TransaccionesTmp alter column PuntosAContabilizar decimal(10,2) 


alter table Comercios alter column POSPuntos decimal(10,2)
alter table Comercios alter column Puntos2 decimal(10,2)
alter table Comercios alter column Puntos3 decimal(10,2)
alter table Comercios alter column Puntos4 decimal(10,2)
alter table Comercios alter column Puntos5 decimal(10,2)
alter table Comercios alter column Puntos6 decimal(10,2)
alter table Comercios alter column Puntos7 decimal(10,2)

alter table Comercios alter column POSArancel decimal(10,2)
alter table Comercios alter column Arancel2 decimal(10,2)
alter table Comercios alter column Arancel3 decimal(10,2)
alter table Comercios alter column Arancel4 decimal(10,2)
alter table Comercios alter column Arancel5 decimal(10,2)
alter table Comercios alter column Arancel6 decimal(10,2)
alter table Comercios alter column Arancel7 decimal(10,2)


alter table Comercios alter column PuntosVip1 decimal(10,2)
alter table Comercios alter column PuntosVip2 decimal(10,2)
alter table Comercios alter column PuntosVip3 decimal(10,2)
alter table Comercios alter column PuntosVip4 decimal(10,2)
alter table Comercios alter column PuntosVip5 decimal(10,2)
alter table Comercios alter column PuntosVip6 decimal(10,2)
alter table Comercios alter column PuntosVip7 decimal(10,2)