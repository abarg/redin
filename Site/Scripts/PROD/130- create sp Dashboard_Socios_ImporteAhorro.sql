/****** Object:  StoredProcedure [dbo].[Dashboard_ImporteAhorro]    Script Date: 18/01/2016 12:14:04 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Dashboard_Socios_ImporteAhorro]
(
	   @IDSocio int
)

AS
  SELECT  
  '' as label, 
  CAST(ISNULL(SUM(CASE Operacion WHEN 'Venta' THEN ImporteAhorro ELSE (-1 * ImporteAhorro) END),0) as int) as data
  FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE  t.IDSocio=@IDSocio

GO


