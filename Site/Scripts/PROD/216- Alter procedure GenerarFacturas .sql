USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[GenerarFacturasSocios]    Script Date: 02/08/2016 01:22:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GenerarFacturasSocios]
(
@fechaDesde datetime,
@fechaHasta datetime,
@imagen text,
@usuario varchar(50)
)

as

declare @idSocio int
declare @importe decimal(8,2)
declare @cant int
declare @nroDocumento varchar(50)
declare @tarjeta varchar(50)
declare @idfactura int
declare @numero int
declare @fechaDesdeAux datetime
declare @fechaHastaAux datetime

set @fechaHastaAux = @fechaHasta--DATEADD(day,1,@fechaHasta)
set @fechaDesdeAux = @fechaDesde

declare CURSORITO cursor for

select t.numero, t.IDSocio, isnull(s.NroDocumento,'') as NroDocumento, Sum(tr.Importe) from Transacciones tr
inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
left join Socios s on t.IDSocio = s.IDSocio
where tr.Operacion='Canje' and tr.Importe>1
and tr.FechaTransaccion  between @fechaDesdeAux and @fechaHastaAux
group by t.numero, t.IDSocio, s.NroDocumento

open CURSORITO
-- Avanzamos un registro y cargamos en las variables los valores encontrados en el primer registro
fetch next from CURSORITO into @tarjeta, @idSocio, @nroDocumento, @importe
while @@fetch_status = 0
    begin
		
		set @numero = (select isnull(Max(cast(numero as int)),0)+1 from facturas where modo='S')
		
		Insert into Facturas
      	values(null,cast(@numero as varchar(50)) ,'CF', @fechaDesde, @fechaHasta, null, null, 
		(@importe*1.21), GETDATE(),null,GETDATE(),null,0,null,null,null, @nroDocumento, 'S', null, @usuario,1, '', @idSocio,0, (@importe*0.21),@imagen)
      	
      	set @idfactura= SCOPE_IDENTITY()
                
        insert into FacturasDetalle
        values(@idfactura, 'PUNTOS - Fondos percibidos por cuenta y orden según anexo' , 1, @importe, 21, 0)

		insert into FacturasDetalle
        values(@idfactura, 'COMISIÓN - Arancel por uso de red' , 1, (@importe*-0.21), 21, 0)
          
      	fetch next from CURSORITO into @tarjeta, @idSocio, @nroDocumento, @importe
    end
    -- cerramos el cursor
close CURSORITO
deallocate CURSORITO

