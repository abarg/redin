create table HobbiesSocios(
	IDHobbieComercio int not null primary key identity (1,1),
	IDHobbie int not null FOREIGN KEY(IDHobbie) REFERENCES Hobbies(IDHobbie),
	IDSocio int not null FOREIGN KEY(IDSocio) REFERENCES Socios(IDSocio),
)
