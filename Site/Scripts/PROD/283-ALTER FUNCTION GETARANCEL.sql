USE [RedIN-QA]
GO
/****** Object:  UserDefinedFunction [dbo].[GetArancel]    Script Date: 19/12/2016 11:09:04 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







ALTER FUNCTION [dbo].[GetArancel] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16),
    @TipoMensaje varchar(4),
    @Operacion varchar(20),
    @TipoTransaccion varchar(6),
	@Fecha datetime
)

RETURNS decimal (10,2)
AS

BEGIN

declare @arancel decimal (10,2)
declare @marcaTarjeta int
declare @marcaComercio int

select @marcaTarjeta = IDMarca FROM Tarjetas where Numero = @NumTarjetaCliente
select @marcaComercio = IDMarca FROM Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
DECLARE @dia int

SET @dia = (select datepart(dw,@Fecha)-1)


if(@Operacion='Canje')
    select @arancel = 0
else
    BEGIN
        IF(@TipoMensaje='1100' or @TipoMensaje='1420' )
          BEGIN
            if(@TipoMensaje='1420' and @TipoTransaccion='000005')--@Operacion='Anulacion'))
                select @arancel = 0
            else
				BEGIN
				if(dbo.EsVip(0,@NumEst, @NumTerminal, @NumTarjetaCliente)=0)
				BEGIN 
					SET @arancel = ISNULL((select TOP 1 p.Arancel from PromocionesPuntuales p 
					inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
					inner join Comercios c on pc.IDComercio = c.IDComercio where
					NumEst = @NumEst and POSTerminal=@NumTerminal and CAST(FechaDesde AS DATE) <= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
				END 
				else 
				BEGIN
					SET @arancel = ISNULL((select TOP 1 p.ArancelVip from PromocionesPuntuales p 
						inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
						inner join Comercios c on pc.IDComercio = c.IDComercio where
						NumEst = @NumEst and POSTerminal=@NumTerminal and CAST(FechaDesde AS DATE) <= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
				END

				IF (@arancel = -1)/*no hay promocion*/
				BEGIN
					if(@marcaTarjeta = @marcaComercio)
						 select @arancel = POSArancel from Marcas where IDMarca = @marcaTarjeta
					ELSE
					BEGIN
						if(@dia = 1) 
								 select @arancel = POSArancel from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 2)
								 select @arancel = Arancel2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 3)
								select @arancel = Arancel3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 4)
								select @arancel = Arancel4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 5)
								select @arancel = Arancel5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 6)
								select @arancel = Arancel6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
								select @arancel = Arancel7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
							end
						END
					end
				END
        ELSE
          BEGIN
            if(@Operacion='Carga')
              select @arancel = GifcardArancelCarga from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
            else 
              select @arancel = GifcardArancelDescarga from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst
          END
    END



if(@arancel=null)
  set @arancel=0

 return @arancel

END






