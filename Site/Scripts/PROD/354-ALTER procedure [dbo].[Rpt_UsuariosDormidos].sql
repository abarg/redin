USE [RedIN]
GO
/****** Object:  StoredProcedure [dbo].[Rpt_UsuariosDormidos]    Script Date: 22/05/2017 05:35:19 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 ALTER procedure [dbo].[Rpt_UsuariosDormidos] @idFranquicia int,@idMarca int,@fechaDesde varchar(20),@sexo char,@edadDesde int,@edadHasta int,@cantTrans int
as

if(@cantTrans = 0)
	BEGIN
		select s.IDSocio,s.Nombre,s.Apellido,0 CantTrans,tar.IDFranquicia,f.NombreFantasia as Franquicia,tar.IDMarca,m.Nombre as Marca,s.Sexo,s.Edad from  Socios s
		join Tarjetas tar on tar.IDSocio = s.IDSocio
		join Franquicias f on tar.IDFranquicia = f.IDFranquicia
		join Marcas m on m.IDMarca = tar.IDMarca
		where (@idFranquicia = 0 or tar.IDFranquicia = @idFranquicia ) and (@idMarca = 0 or tar.IDMarca = @idMarca) and (@sexo ='' or s.Sexo = @sexo) and (@edadDesde = 0 or s.Edad >= @edadDesde) and (@edadHasta = 0 or s.Edad <= @edadHasta)
		and tar.Numero not in 
			(select distinct(tar.Numero) from Transacciones tr
			join  Tarjetas tar  on tar.Numero = tr.NumTarjetaCliente
			 where  tr.FechaTransaccion >= @fechaDesde)
		order by 4 desc
	END
else
	BEGIN
		(select s.IDSocio,s.Nombre,s.Apellido,0 CantTrans,tar.IDFranquicia,f.NombreFantasia as Franquicia,tar.IDMarca,m.Nombre as Marca,s.Sexo,s.Edad,s.Email,s.EmpresaCelular,s.Celular from  Socios s
		join Tarjetas tar on tar.IDSocio = s.IDSocio
		join Franquicias f on tar.IDFranquicia = f.IDFranquicia
		join Marcas m on m.IDMarca = tar.IDMarca
		where (@idFranquicia = 0 or tar.IDFranquicia = @idFranquicia ) and (@idMarca = 0 or tar.IDMarca = @idMarca) and (@sexo ='' or s.Sexo = @sexo) and (@edadDesde = 0 or s.Edad >= @edadDesde) and (@edadHasta = 0 or s.Edad <= @edadHasta)
		and s.IDSocio not in 
			(select distinct(tar.IDSocio) from Transacciones tr
			join  Tarjetas tar  on tar.Numero = tr.NumTarjetaCliente where tr.FechaTransaccion >= @fechaDesde)
		
		UNION

		select tar.IDSocio, s.Nombre, s.Apellido,count(tar.IDSocio) CantTrans,tar.IDFranquicia,f.NombreFantasia as Franquicia,tar.IDMarca,m.Nombre as Marca,s.Sexo,s.Edad,s.Email,s.EmpresaCelular,s.Celular
		from Transacciones tr
		join Tarjetas tar on tar.Numero = tr.NumTarjetaCliente
		join Franquicias f on tar.IDFranquicia = f.IDFranquicia
		join Marcas m on m.IDMarca = tar.IDMarca
		join Socios s on s.IDSocio = tar.IDSocio
		where tr.FechaTransaccion >= @fechaDesde and (@idFranquicia = 0 or tar.IDFranquicia = @idFranquicia ) and (@idMarca = 0 or tar.IDMarca = @idMarca) and (@sexo ='' or s.Sexo = @sexo) and (@edadDesde = 0 or s.Edad >= @edadDesde) and (@edadHasta = 0 or s.Edad <= @edadHasta) and tar.IDSocio is not null 
		group by tar.IDSocio,tar.IDFranquicia,f.NombreFantasia,m.Nombre,tar.IDMarca, s.Nombre, s.Apellido,s.Sexo,s.Edad,s.Email,s.EmpresaCelular,s.Celular
		having count(tar.IDSocio) <= @cantTrans)

	END 
