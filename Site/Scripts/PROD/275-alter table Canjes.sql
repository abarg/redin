
ALTER TABLE [dbo].[Canjes] DROP CONSTRAINT [FK_Canjes_Premios]
alter table Canjes drop column IDPremio 

alter table Canjes add IDProducto int

ALTER TABLE [dbo].[Canjes]  WITH CHECK ADD  CONSTRAINT [FK_Canjes_Productos] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Productos] ([IDProducto])
GO

ALTER TABLE [dbo].[Canjes] CHECK CONSTRAINT [FK_Canjes_Productos]
GO




alter table Canjes add IDMarca int

ALTER TABLE [dbo].[Canjes]  WITH CHECK ADD  CONSTRAINT [FK_Canjes_Marcas] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marcas] ([IDMarca])
GO

ALTER TABLE [dbo].[Canjes] CHECK CONSTRAINT [FK_Canjes_Marcas]
GO



alter table Canjes add IDEstado int 

ALTER TABLE [dbo].[Canjes]  WITH CHECK ADD  CONSTRAINT [FK_Canjes_EstadosCanjes] FOREIGN KEY([IDEstado])
REFERENCES [dbo].[EstadosCanjes] ([IDEstado])
GO

ALTER TABLE [dbo].[Canjes] CHECK CONSTRAINT [FK_Canjes_EstadosCanjes]
GO


