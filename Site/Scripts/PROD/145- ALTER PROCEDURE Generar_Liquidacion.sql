USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Generar_Liquidacion]    Script Date: 07/03/2016 11:37:25 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[Generar_Liquidacion]
     @FechaGeneracion datetime,
     @FechaDesde datetime,
     @FechaHasta datetime,
     @IDFranquicia int
AS
BEGIN
  insert into Liquidaciones (IDFranquicia,FechaGeneracion,FechaDesde,FechaHasta,Estado)values (@IDFranquicia,@FechaGeneracion,@FechaDesde,@FechaHasta,'EnFranquicia')
  declare @idLiquidacion int 
  set @idLiquidacion =(SELECT SCOPE_IDENTITY())

  CREATE TABLE #Result
(
  IDComercio int, 
  Total decimal (10,2)
)
INSERT #Result EXEC Rpt_GetComisionesTotales @fechaDesde,@fechaHasta,@IDFranquicia

  insert into LiquidacionDetalle
  (
 
  IDComercio,
  ImporteTotalOriginal,
   IDLiquidacion
  ) 
  select distinct 
   IDComercio,sum (Total), @idLiquidacion from #Result group by IDComercio

DROP TABLE #Result
END



GO


