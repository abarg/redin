USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[TmpCostoSeguro]    Script Date: 17/12/2015 09:34:41 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TmpCostoSeguro](
	[NumeroTarjeta] [varchar](16) NOT NULL,
	[CostoSeguroSocio] [decimal](5, 2) NULL,
	[CostoSeguroMarca] [decimal](5, 2) NULL,
	[CostoFinal] [decimal](5, 2) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


