alter table [FormularioTarjetas] add DomicilioFiscal varchar(50) null;
alter table [FormularioTarjetas] add ObservacionesDatosFact text null;
alter table [FormularioTarjetas] add IDProvinciaFact int null;
alter table [FormularioTarjetas] add IDCiudadFact int null;
alter table [FormularioTarjetas] add IDPaisFact int null;

ALTER TABLE [dbo].[FormularioTarjetas]  WITH CHECK ADD FOREIGN KEY([IDProvinciaFact])
REFERENCES [dbo].[Provincias] ([IDProvincia])
GO

ALTER TABLE [dbo].[FormularioTarjetas]  WITH CHECK ADD FOREIGN KEY([IDCiudadFact])
REFERENCES [dbo].[Ciudades] ([IDCiudad])
GO

ALTER TABLE [dbo].[FormularioTarjetas]  WITH CHECK ADD FOREIGN KEY([IDPaisFact])
REFERENCES [dbo].[Paises] ([IDPais])
GO

