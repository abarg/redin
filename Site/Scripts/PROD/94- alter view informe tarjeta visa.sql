USE [RedIN-QA]
GO

/****** Object:  View [dbo].[InformeTarjetasVisa]    Script Date: 23/12/2015 10:44:50 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM [InformeTarjetasVisa]

--exec [tmpActualizarArchivosVisa]

ALTER VIEW [dbo].[InformeTarjetasVisa]
AS
   
SELECT IDTarjeta, Credito, Giftcard,FechaBaja, IDSocio
FROM TmpArchivosVisa

/*UPDATE #TMPVISA
set 
Credito=0,
Giftcard=0
WHERE IDSocio IS NOT NULL AND  IDSocioPadre IS NULL*/


/*
SELECT 
	t1.IDTarjeta,
	CASE WHEN (t1.IDSocio IS NOT NULL AND  s.IDSocioPadre IS NULL) THEN
			(SELECT ISNULL(SUM(cast(Credito as int)),0)
			FROM Tarjetas t2
			INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
			WHERE (t2.IDSocio = t1.IDSocio or t2.IDSocio in (select s.IDSocio from Socios s where s.IDSocioPadre=t1.IDSocio)) AND m.TipoCatalogo='A' and t2.FechaBaja is null
			)
		 WHEN s.IDSocioPadre IS NOT NULL THEN	0
	ELSE
		(SELECT ISNULL(SUM(cast(Credito as int)),0)
        FROM Tarjetas t2
        INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
        WHERE t2.IDTarjeta = t1.IDTarjeta AND m.TipoCatalogo='A' and t2.FechaBaja is null
        )
    END AS Credito,
    
	CASE WHEN( t1.IDSocio IS NOT NULL  AND  s.IDSocioPadre IS NULL )THEN
			(SELECT ISNULL(SUM(cast(Giftcard as int)),0)
			FROM Tarjetas t2
			INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
		   WHERE (t2.IDSocio = t1.IDSocio or t2.IDSocio in (select s.IDSocio from Socios s where s.IDSocioPadre=t1.IDSocio)) AND m.TipoCatalogo='A' and t2.FechaBaja is null
			)
		 WHEN s.IDSocioPadre IS NOT NULL THEN	0
	ELSE
		(SELECT ISNULL(SUM(cast(Giftcard as int)),0)
        FROM Tarjetas t2
        INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
        WHERE t2.IDTarjeta = t1.IDTarjeta AND m.TipoCatalogo='A' and t2.FechaBaja is null
        )
    END AS Giftcard,      
	CASE Marcas.TipoCatalogo WHEN 'A' THEN FechaBaja ELSE GETDATE() END AS FechaBaja,
      --SI TIENE CATALOGO PRIVADO LO INFORMO COMO BAJA A VISA
      t1.IDSocio
 FROM Tarjetas t1
 INNER JOIN Marcas on t1.IDMarca = Marcas.IDMarca
 left JOIN Socios s on t1.IDSocio =s.IDSocio
 where t1.FechaBaja is null
 */
GO


