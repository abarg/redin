alter table Facturas add IDSocio int 
ALTER TABLE Facturas  WITH CHECK ADD  CONSTRAINT FK_Facturas_Socios FOREIGN KEY (IDSocio)
REFERENCES Socios (IDSocio)
GO

ALTER TABLE Facturas CHECK CONSTRAINT FK_Facturas_Socios
GO
