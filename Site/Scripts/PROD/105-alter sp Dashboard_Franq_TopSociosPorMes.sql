USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TopSociosPorMes]    Script Date: 30/12/2015 11:39:07 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_Franq_TopSociosPorMes]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDFranquicia int)    
AS
BEGIN



/*
SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		m.Nombre	 as tres,
		*/

	select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		m.Nombre as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		inner join Marcas m on t.IDMarca = m.IDMarca
		--inner join Franquicias f on tr.IDFranquicia=f.IDFranquicia
		where t.IDSocio is not null and tr.Importe>1
		AND t.FechaBaja is null and t.TipoTarjeta='B'
		AND tr.Operacion not in ('Carga','Descarga')
		AND tr.IDFranquicia = @IDFranquicia AND tr.FechaTransaccion >= @FechaDesde AND tr.FechaTransaccion <= @FechaHasta
		group by t.Numero, m.Nombre	, s.Nombre, s.Apellido, tr.Operacion order by cuatro desc	
	
/*	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc*/



/*
SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		t.Marca	 as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesFranquiciasView t
		inner join tarjetas tr on t.Numero = tr.Numero
		--inner join Franquicias f on tr.IDFranquicia=f.IDFranquicia
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND tr.IDFranquicia = @IDFranquicia AND t.FechaTransaccion >= @FechaDesde AND t.FechaTransaccion <= @FechaHasta
		group by t.Numero, t.Marca	, t.Nombre, t.Apellido, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc

	*/

END



GO


