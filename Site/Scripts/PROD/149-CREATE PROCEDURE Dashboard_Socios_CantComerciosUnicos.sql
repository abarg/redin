USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Socios_ImportePagado]    Script Date: 15/02/2016 09:37:09 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Socios_CantComerciosUnicos]
(
	@IDSocio int
)

AS
  
  SELECT  
  '' as label, 
  COUNT (*) as data FROM (select  distinct NumEst, NumTerminal, IDSocio FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE  t.IDSocio=@IDSocio) t1 

GO


