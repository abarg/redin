
/****** Object:  Table [dbo].[OrdenesDePago]    Script Date: 08/08/2016 08:19:07 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OrdenesDePago](
	[IDOrden] [int] IDENTITY(1,1) NOT NULL,
	[IDFactura] [int] NOT NULL,
	[Numero] [varchar](50) NOT NULL,
	[Importe] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_OrdenesDePago] PRIMARY KEY CLUSTERED 
(
	[IDOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OrdenesDePago]  WITH CHECK ADD  CONSTRAINT [FK_OrdenesDePago_Facturas] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Facturas] ([IDFactura])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OrdenesDePago] CHECK CONSTRAINT [FK_OrdenesDePago_Facturas]
GO


