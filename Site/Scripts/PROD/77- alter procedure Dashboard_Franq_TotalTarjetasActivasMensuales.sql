USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TotalTarjetasActivasMensuales]    Script Date: 13/11/2015 16:21:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Dashboard_Franq_TotalTarjetasActivasMensuales]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDFranquicia int)    
AS
BEGIN
  declare @activas int
  declare @total int
  
   
  SELECT @total = count(IDTarjeta) from Tarjetas
  
  SELECT @activas = ISNULL(SUM(cantidad),0) from (
  select sum(1) as cantidad
  FROM DashboardView INNER JOIN Tarjetas on DashboardView.NumTarjetaCliente = Tarjetas.Numero where Tarjetas.FechaAlta >= @FechaDesde AND Tarjetas.FechaAlta <= @FechaHasta AND IDFranquicia = @IDFranquicia
    group by NumTarjetaCliente) as T
  
  
  select 
  @activas as data, 'A' as label
  union all
  select 
  @total as data, 'T' as label
END

GO


