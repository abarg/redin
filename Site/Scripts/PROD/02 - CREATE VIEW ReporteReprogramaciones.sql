create view ReporteReprogramacionesPOSNET as

select
c.POSTerminal as NroTerminal,
m.Nombre as Marca,
c.NumEst as NumEst,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
'Red IN' as Observacion
from Comercios c
inner join Marcas m on c.IDMarca=m.IDMarca
where c.Activo = 1 and c.POSSistema = 'POSNET' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'

go

create view ReporteReprogramacionesLAPOS as

select
c.POSTerminal as NroTerminal,
'Red IN' as MarcaVisa,
'032' as Moneda,
c.NumEst as NumEst,
7 as CodigoSolicitante,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
'' as Observacion
from Comercios c
where c.Activo = 1 and c.POSSistema = 'LAPOS' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'

go

create view ReporteReprogramacionesAMEX as

select
c.NombreFantasia,
c.RazonSocial as RazonSocial,
c.NroDocumento as CUIT,
d.Domicilio as Direcci�n,
ci.Nombre as Localidad,
d.CodigoPostal as CP,
co.Telefono as Contacto,
c.POSTerminal as NroTerminal,
c.NumEst as NumEst,
'' as Observaciones
from Comercios c
inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
inner join Ciudades ci on d.Ciudad=ci.IDCiudad
inner join Contactos co on c.IDContacto=co.IDContacto
where c.Activo = 1 and c.POSSistema = 'AMEX' and c.POSTerminal is not null and c.POSTerminal != '' and c.Estado != 'DE'