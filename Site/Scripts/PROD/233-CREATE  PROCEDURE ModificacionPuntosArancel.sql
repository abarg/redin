USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[ModificacionPuntosArancel]    Script Date: 27/09/2016 02:10:50 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create  PROCEDURE [dbo].[ModificacionPuntosArancel] (@FechaDesde datetime,
  @FechaHasta datetime, 
  @IDMarca int,
  @NumEst varchar (20),
  @NumTerminal varchar (10),
  @Operacion varchar (20),
  @Origen varchar (20),
  @Puntos decimal (10,2),
  @PuntosVip decimal (10,2),
  @Arancel decimal (10,2),
  @CostoRed decimal (10,2)
  )                  
AS
BEGIN
	
	
 CREATE TABLE #Result
(
  IDTransaccion int, 
  NumTarjetaCliente varchar(128),
  Puntos decimal(10,8)
)
INSERT #Result select IDTransaccion, NumTarjetaCliente, @PuntosVip from 
Transacciones where FechaTransaccion >=@FechaDesde and FechaTransaccion<=@FechaHasta
and (@IDMarca=0 or IDMarca=@IDMarca) and 
(@Operacion='' or Operacion=@Operacion) and (@Origen='' or Origen=@Origen) and (@NumEst='' or NumEst =@NumEst) and 
(@NumTerminal='' or NumTerminal=@NumTerminal) and (dbo.EsVip(@IDMarca,@NumEst, @NumTerminal, NumTarjetaCliente)=1)

INSERT #Result select IDTransaccion, NumTarjetaCliente, @Puntos from 
Transacciones where FechaTransaccion >=@FechaDesde and FechaTransaccion<=@FechaHasta
and (@IDMarca=0 or IDMarca=@IDMarca) and 
(@Operacion='' or Operacion=@Operacion) and (@Origen='' or Origen=@Origen) and (@NumEst='' or NumEst =@NumEst) and 
(@NumTerminal='' or NumTerminal=@NumTerminal) and (dbo.EsVip(@IDMarca,@NumEst, @NumTerminal, NumTarjetaCliente)=0)

select * from #Result
update Transacciones  set Arancel=@arancel, Puntos=r.Puntos, UsoRed=@CostoRed from #Result r where Transacciones.IDTransaccion in (r.IDTransaccion)
END


