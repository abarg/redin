USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[Dealer]    Script Date: 03/03/2016 12:59:21 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Dealer](
	[IDDealer] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Apellido] [varchar](100) NOT NULL,
	[Pwd] [varchar](50) NOT NULL,
	[Email] [varchar](128) NOT NULL,
	[ComisionTarjetas] [decimal](10, 2) NULL,
	[Codigo] [varchar](100) NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Activo] [bit] NOT NULL,
	[ComisionAlta] [decimal](10, 2) NULL,
	[ComisionGiftCard] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDDealer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


