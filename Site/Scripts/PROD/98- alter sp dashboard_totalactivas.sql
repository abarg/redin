GO
/****** Object:  StoredProcedure [dbo].[Dashboard_TotalTarjetasActivas]    Script Date: 29/12/2015 11:55:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_TotalTarjetasActivas]
AS
  declare @activas int
  declare @total int
  
  SELECT @total = count(IDTarjeta) from Tarjetas WHERE TipoTarjeta='B' AND FechaBaja is null
  
  SELECT @activas = ( 
	  SELECT count(DISTINCT NumTarjetaCliente)
	  from  Transacciones tr
	  INNER JOIN Tarjetas t on tr.NumTarjetaCliente=t.Numero
	  WHERE t.FechaBaja is null and tr.Importe>1 and Arancel is not null and Puntos is not null
	  AND tr.Operacion ='VENTA'
  )
  
  /*
  select ISNULL(SUM(cantidad),0) from (
  select sum(1) as cantidad
  FROM DashboardView
  group by NumTarjetaCliente) as T
  */
  
  select 
  @activas as data, 'A' as label
  union all
  select 
  @total as data, 'T' as label