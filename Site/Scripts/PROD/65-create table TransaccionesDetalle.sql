USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[TransaccionesDetalle]    Script Date: 29/10/2015 15:13:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TransaccionesDetalle](
	[IDTransaccion] [int] IDENTITY(1,1) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[IDTransaccionDetalle] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDTransaccionDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TransaccionesDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TransaccionesDetalle_Productos] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Productos] ([IDProducto])
GO

ALTER TABLE [dbo].[TransaccionesDetalle] CHECK CONSTRAINT [FK_TransaccionesDetalle_Productos]
GO

ALTER TABLE [dbo].[TransaccionesDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TransaccionesDetalle_Transacciones] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transacciones] ([IDTransaccion])
GO

ALTER TABLE [dbo].[TransaccionesDetalle] CHECK CONSTRAINT [FK_TransaccionesDetalle_Transacciones]
GO


