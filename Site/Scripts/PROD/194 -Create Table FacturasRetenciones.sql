Create table FacturasRetenciones(
IDRetencion int primary key not null identity(1,1),
IDFactura int Foreign key (IDFactura) references Facturas(IDFactura) not null,
Tipo varchar(20) not null,
Importe decimal(18,2) default 0.00 not null,
NroRetencion varchar(50) not null,
)
