ALTER TABLE Socios
add IDProfesion int


ALTER TABLE [dbo].[Socios]  WITH CHECK ADD  CONSTRAINT [FK_Socios_Profesiones] FOREIGN KEY([IDProfesion])
REFERENCES [dbo].[Profesiones] ([IDProfesion])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Socios] CHECK CONSTRAINT [FK_Socios_Profesiones]
GO
