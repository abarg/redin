USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_CantidadTarjetasUnicas]    Script Date: 18/01/2016 11:22:29 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Franq_CantidadTarjetasUnicas]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime, 
    @IDFranquicia int)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	  SELECT '' as label,COUNT(distinct NumTarjetaCliente)  as data 
  FROM Transacciones  
  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta and IDFranquicia=@IDFranquicia and Importe>1 and Arancel is not null and Puntos is not null
AND Operacion not in ('Carga','Descarga')


	/*
    -- Insert statements for procedure here
  SELECT '' as label,COUNT(distinct NumTarjetaCliente)  as data 
  FROM [DashboardView] 
  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta and TarjetaFranquicia=@IDFranquicia


  */
END

GO


