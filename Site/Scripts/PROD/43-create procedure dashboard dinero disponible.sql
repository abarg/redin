GO

CREATE  PROCEDURE Dashboard_DineroDisponibleCredito

AS
  SELECT  
  '' as label, 
  ISNULL(SUM(Credito),0) as data
  FROM Tarjetas where Estado='A' and TipoTarjeta='B'
GO


go

CREATE  PROCEDURE Dashboard_DineroDisponibleGiftcard

AS
  SELECT  
  '' as label, 
  ISNULL(SUM(Giftcard),0) as data
  FROM Tarjetas  WHERE Estado='A' and TipoTarjeta='G'
GO

CREATE  PROCEDURE Dashboard_Franq_DineroDisponibleCredito 
(
@IDFranquicia int
)

AS
  SELECT  
  '' as label, 
  ISNULL(SUM(Credito),0) as data
  FROM Tarjetas where IDFranquicia=@IDFranquicia and Estado='A' and TipoTarjeta='B'
GO


GO

CREATE  PROCEDURE Dashboard_Franq_DineroDisponibleGiftcard
(
	@IDFranquicia int
)


AS
  SELECT  
  '' as label, 
  ISNULL(SUM(Giftcard),0) as data
  FROM Tarjetas where IDFranquicia=@IDFranquicia and Estado='A' and TipoTarjeta='G'
GO

