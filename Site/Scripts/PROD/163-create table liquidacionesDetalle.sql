USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[LiquidacionDetalle]    Script Date: 03/03/2016 12:59:51 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LiquidacionDetalle](
	[IDLiquidacionDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IDLiquidacion] [int] NOT NULL,
	[IDComercio] [int] NOT NULL,
	[ImporteTotalOriginal] [decimal](10, 2) NOT NULL,
	[ImporteTotal] [decimal](10, 2) NULL,
	[Observaciones] [varchar](100) NULL,
	[PendienteDeComisiones] [decimal](10, 2) NULL,
 CONSTRAINT [PK_LiquidacionDetalle] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacionDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LiquidacionDetalle]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionDetalle_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO

ALTER TABLE [dbo].[LiquidacionDetalle] CHECK CONSTRAINT [FK_LiquidacionDetalle_Comercios]
GO

ALTER TABLE [dbo].[LiquidacionDetalle]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionDetalle_Liquidaciones] FOREIGN KEY([IDLiquidacion])
REFERENCES [dbo].[Liquidaciones] ([IDLiquidacion])
GO

ALTER TABLE [dbo].[LiquidacionDetalle] CHECK CONSTRAINT [FK_LiquidacionDetalle_Liquidaciones]
GO


