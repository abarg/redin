alter table Zonas
add IDCiudad int null

go

update zonas
set IDCiudad=1

go

alter table Zonas
alter column IDCiudad int not null
