USE [RedIN-QA]
GO

/****** Object:  UserDefinedFunction [dbo].[GetPuntos]    Script Date: 15/02/2016 12:46:40 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER FUNCTION [dbo].[GetPuntos] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16),
    @TipoMensaje varchar(4),
    @Operacion varchar(20),
    @TipoTransaccion varchar(6),
	@Fecha datetime
)

RETURNS decimal (10,2)
AS

BEGIN

DECLARE @puntos decimal (10,2)
DECLARE @sumaPuntos bit
DECLARE @dia int
SET @dia = (select datepart(dw,@Fecha))
/*declare @marcaTarjeta int
declare @marcaComercio int

select @marcaTarjeta = IDMarca FROM Tarjetas where Numero = @NumTarjetaCliente
select @marcaComercio = IDMarca FROM Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst

if(@marcaTarjeta = @marcaComercio)
  select @puntos = POSPuntos from Marcas where IDMarca = @marcaTarjeta
else*/

if(@Operacion='Canje')
    select @puntos = 0
else
    BEGIN
        if(@TipoMensaje='1420' and @TipoTransaccion='000005')--Anulacion
            select @puntos = 1
        ELSE
            BEGIN
                IF(@TipoMensaje = '1100' or @TipoMensaje = '1420')
					BEGIN
						
						DECLARE @AffinityMarca char(4)
						DECLARE @AffinityTarjeta char(4)
						declare @IDMarca int

						set @IDMarca= (select IDMarca from Comercios where NumEst= @NumEst and POSTerminal=@NumTerminal)
						set @AffinityMarca=(select affinity from Marcas where IDMarca=@IDMarca)

						set @AffinityTarjeta=(select SUBSTRING(@NumTarjetaCliente, 4,4))
						IF(@AffinityTarjeta=@AffinityMarca) /*PUNTOS VIP*/
							BEGIN
								SET @puntos = ISNULL((select TOP 1 p.PuntosVip from PromocionesPuntuales p 
								inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
								inner join Comercios c on pc.IDComercio = c.IDComercio where
								NumEst = @NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),0)
								IF (@puntos =0)/*puntos vip sin promocion*/
								BEGIN 
									IF(@dia = 1)
										SET @puntos = (SELECT TOP 1 PuntosVip1 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 2)
										SET @puntos = (SELECT TOP 1 PuntosVip2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 3)
										SET @puntos = (SELECT TOP 1 PuntosVip3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 4)
										SET @puntos = (SELECT TOP 1 PuntosVip4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 5)
										SET @puntos = (SELECT TOP 1 PuntosVip5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 6)
										SET @puntos = (SELECT TOP 1 PuntosVip6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
										SET @puntos = (SELECT TOP 1 PuntosVip7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
								END
							END
						else /*puntos no vip*/
							BEGIN
									SET @puntos = ISNULL((select TOP 1 p.Puntos from PromocionesPuntuales p 
									inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
									inner join Comercios c on pc.IDComercio = c.IDComercio where
									NumEst = @NumEst and POSTerminal=@NumTerminal and FechaDesde <= @Fecha and FechaHasta >= @Fecha),0)
									IF (@puntos =0)/*PUNTOS NO VIP SIN PROMOCION*/
									BEGIN
										IF(@dia = 1)
										SET @puntos = (SELECT TOP 1 POSPuntos from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 2)
										SET @puntos = (SELECT TOP 1 Puntos2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 3)
										SET @puntos = (SELECT TOP 1 Puntos3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 4)
										SET @puntos = (SELECT TOP 1 Puntos4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 5)
										SET @puntos = (SELECT TOP 1 Puntos5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 6)
										SET @puntos = (SELECT TOP 1 Puntos6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
										SET @puntos = (SELECT TOP 1 Puntos7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
									END
							END 
					END
                ELSE
                    BEGIN
                        SET @sumaPuntos = (SELECT TOP 1 GiftcardGeneraPuntos FROM Comercios WHERE NumEst = @NumEst)
                        IF(@sumaPuntos=1)
                            SET @puntos = 1
                        else
                            SET @puntos = 0

                    END
            END
    end

if(@puntos=null)
  set @puntos=0

 return @puntos

END




GO


