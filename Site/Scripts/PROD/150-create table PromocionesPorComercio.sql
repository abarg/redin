USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[PromocionesPorComercio]    Script Date: 15/02/2016 10:13:09 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromocionesPorComercio](
	[IDPromocionesPorComercio] [int] IDENTITY(1,1) NOT NULL,
	[IDPromocionesPuntuales] [int] NOT NULL,
	[IDComercio] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDPromocionesPorComercio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PromocionesPorComercio]  WITH CHECK ADD  CONSTRAINT [FK_PromocionesPorComercio_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO

ALTER TABLE [dbo].[PromocionesPorComercio] CHECK CONSTRAINT [FK_PromocionesPorComercio_Comercios]
GO

ALTER TABLE [dbo].[PromocionesPorComercio]  WITH CHECK ADD  CONSTRAINT [FK_PromocionesPorComercio_PromocionesPuntuales] FOREIGN KEY([IDPromocionesPuntuales])
REFERENCES [dbo].[PromocionesPuntuales] ([IDPromocionesPuntuales])
GO

ALTER TABLE [dbo].[PromocionesPorComercio] CHECK CONSTRAINT [FK_PromocionesPorComercio_PromocionesPuntuales]
GO


