USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[ProcesarCaducidad]    Script Date: 05/09/2016 05:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ProcesarCaducidad](
@NumEst varchar (20),
	@NumTerminal varchar (10),
	@FechaTransaccion datetime
)
AS
BEGIN

insert into Transacciones  (
				[NumTarjetaCliente]
				,[Importe]
				,[PuntosAContabilizar]
				,[NumEst]
				,[NumTerminal]
				,[TipoMensaje]
				,[TipoTransaccion]
				,[FechaTransaccion]
				,[NumCupon]
				,[NumReferencia]
				,[NumRefOriginal]
				,[CodigoPremio]
				,[PuntosDisponibles]
				,[Origen]
				,[Operacion]
				,[Descripcion]
				,[ImporteAhorro]
				,[Descuento], UsoRed, Arancel, Puntos, IDMarca,Usuario)
	select NumTarjetaCliente, sum(PuntosAContabilizar), CAST (((sum(PuntosAContabilizar) * 100)*-1) as int) as PuntosAContabilizar,
			@NumEst,@NumTerminal,'1100','000005',@fechaTransaccion,'','','','',
			'000000000000','Web','Canje', 'CADUCIDAD', 0, 
			0,0,0, 1,tr.IDMarca,'proceso_caducidad' 
			 from Transacciones tr
			inner join Marcas m on m.IDMarca = tr.IDMarca
			inner join Tarjetas t on t.Numero = tr.NumTarjetaCliente
			where tr.IDMarca in (select  IDMarca from  Marcas m
			where FechaCaducidad = cast( getdate() as date)) 
			and tr.FechaTransaccion< m.fechaTopeCanje
			and t.TipoTarjeta <>'G'
			group by tr.IDMarca,tr.NumTarjetaCliente,tr.NumEst


				
	UPDATE Transacciones
	SET PuntosIngresados= CAST (Importe AS varchar(12))
	WHERE Operacion='Canje' and FechaTransaccion=@FechaTransaccion and usuario='proceso_caducidad' and NumEst=@NumEst and NumTerminal=@NumTerminal

	UPDATE Transacciones
	SET IDFranquicia= dbo.GetFranquiciaTarjeta(NumTarjetaCliente) WHERE FechaTransaccion=@FechaTransaccion and usuario='proceso_caducidad' and NumEst=@NumEst and NumTerminal=@NumTerminal


	--5 actualizar puntos por tarjeta
	UPDATE Tarjetas
	SET PuntosTotales = (
	SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
	),
	Credito = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
		FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
	),
	Giftcard = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
	--SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
	FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
	)where Numero in (select NumTarjetaCliente from Transacciones t
			inner join Marcas m on m.IDMarca = t.IDMarca
			where t.IDMarca in (select  IDMarca from  Marcas m
			where FechaCaducidad = cast( getdate() as date)) 
			and t.FechaTransaccion< m.fechaTopeCanje
			group by NumTarjetaCliente)
	

	select count(*)
	 from Transacciones t
			inner join Marcas m on m.IDMarca = t.IDMarca
			where t.IDMarca in (select  IDMarca from  Marcas m
			where FechaCaducidad = cast( getdate() as date)) 
			and t.FechaTransaccion< m.fechaTopeCanje
			group by t.IDMarca,t.NumTarjetaCliente,t.NumEst

end