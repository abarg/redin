
/****** Object:  StoredProcedure [dbo].[TopSocios]    Script Date: 25/04/2017 12:27:23 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter PROCEDURE [dbo].[Rpt_RankingSociosPorFecha]
(
	@IDFranquicia int,
	@IDMarca int,
	@FechaDesde datetime,
	@fechaHasta datetime
)
AS
		select   
		isnull(s.Nombre + ', '+ s.Apellido,'') as Socio, 
		isnull(s.NroDocumento,'') as NroDoc,
		isnull(t.Numero,'' )as Tarjeta,
		isnull(tr.IDFranquicia,0) as IDFranquicia,
		isnull(tr.IDMarca,0) as IDMarca,
		--isnull(m.Nombre,'') as Marca,
		--isnull(f.NombreFantasia,'') as Franquicia,
		count(s.NroDocumento) as CantTr,
		--'' as Importe
		--sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.ImporteOriginal ELSE tr.ImporteOriginal*-1 END,0)) Importe
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.ImporteOriginal) WHEN 'Carga' THEN SUM(tr.ImporteOriginal) ELSE SUM(tr.ImporteOriginal*-1) END,0) AS Importe
		--0.0 as Ahorro
		
		from Tarjetas t
		inner join TransaccionesView tr on tr.Numero = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		where t.IDSocio is not null  and tr.ImporteOriginal>0
		-- and tr.Importe>1
		--AND t.FechaBaja is null and t.TipoTarjeta='B'
		--AND tr.Operacion not in ('Carga','Descarga')
		AND tr.FechaTransaccion >= @FechaDesde  AND  tr.FechaTransaccion <= @fechaHasta 
		AND (tr.IDMarca = @IDMarca or @IDMarca = 0)
		AND (tr.IDFranquicia = @IDFranquicia or @IDFranquicia = 0)		

		group by 
		t.Numero,
		s.NroDocumento,
		tr.IDFranquicia,
		--f.NombreFantasia,
		tr.IDMarca,
		--m.Nombre,
		s.Nombre, 
		s.Apellido, 
		tr.Operacion	
		order by Importe desc
