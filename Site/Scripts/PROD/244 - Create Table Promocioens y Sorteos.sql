Create Table Sorteos
(IDSorteo int primary key identity(1,1),
IDMarca int not null,
Titulo varchar(40),
Mensaje1 varchar(40),
Mensaje2 varchar(40),
Mensaje3 varchar(40),
Mensaje4 varchar(40),
FechaDesde date,
FechaHasta date,
Activo bit not null,
FOREIGN KEY (IDMarca) REFERENCES Marcas(IDMarca)
)

Create Table TransaccionesSorteos(
IDTransaccionSorteo int primary key identity(1,1),
IDSorteo int,
Fecha date,
IDTarjeta int,
FOREIGN KEY(IDSorteo) REFERENCES Sorteos(IDSorteo),
FOREIGN KEY(IDTarjeta) REFERENCES Tarjetas(IDTarjeta)
)

Create Table Promociones
(IDPromociones int primary key identity(1,1),
IDMarca int  not null,
Titulo varchar(40),
Mensaje1 varchar(40),
Mensaje2 varchar(40),
Mensaje3 varchar(40),
Mensaje4 varchar(40),
TipoCodigo integer not null,
InformacionACodificar varchar(40) not null,
FechaDesde date not null,
FechaHasta date not null,
Activo bit not null,
FOREIGN KEY (IDMarca) REFERENCES Marcas(IDMarca)
)
