ALTER PROCEDURE [dbo].[IntegracionContabiliumFacturas]

AS
	DECLARE @IDUsuario INT = 11
	DECLARE @Tipo varchar (1)='C' 
	DECLARE @Personeria varchar (1)='J' 
	DECLARE @Iva decimal(5,2) = 1.21
	DECLARE @CondicionVenta varchar (50) = 'Cuenta corriente'
	DECLARE @Bonificacion decimal (5,2) = 0
	DECLARE @Saldo decimal(18,2)=0
	DECLARE @ImporteNoGrabado decimal(18,2)=0
	DECLARE @PercepcionIVA decimal(18,2)=0
	DECLARE @PercepcionIIBB decimal(18,2)=0
	DECLARE @TipoDestinatario varchar (1)='C'
	DECLARE @Modo varchar (1)='E'
	DECLARE @IDPuntoDeVenta INT =344
	DECLARE @Observaciones varchar (1)=''
	DECLARE @EmailsEnvioFc varchar (1)=''
	DECLARE @AlicuotaIvaDefecto	varchar (1)=''
	DECLARE @TipoComprobanteDefecto varchar (1)=''
	DECLARE @TipoConcepto int=2 
	
	select 
	Comercios.IDComercio as Codigo, 
	Comercios.FechaAlta,
	RazonSocial,
	TipoDocumento,
	Facturas.NroDocumento,
	NombreFantasia,
	CondicionIva,
	Email,
	Web,
	Observaciones,
	EmailsEnvioFc,
	Comercios.Telefono,
	Comercios.FormaPago_Banco,
	Comercios.FormaPago_CBU,
	Comercios.Celular,
	Comercios.Responsable as Contacto,
	Numero,
	Tipo,
	CAE,
	FechaCAE,
	FechaProceso,
	ImporteTotal,
	TotalIva,
	IDFactura,
	Provincia as IDProvincia,
	Ciudades.Nombre as Ciudad,
	Domicilio,
	CodigoPostal,
	PisoDepto,
	0 as IDCiudad,
	0 as IDPersona
	into #temp
	from  [wi271584_clubin].dbo.Facturas
	--inner join [wi271584_clubin].dbo.FacturasDetalle on Facturas.IDFactura=FacturasDetalle.IDFactura
	inner join [wi271584_clubin].dbo.Comercios on Facturas.IDComercio=Comercios.IDComercio
	inner join [wi271584_clubin].dbo.Domicilios on Domicilios.IDDomicilio=Comercios.IDDomicilioFiscal
	inner join [wi271584_clubin].dbo.Ciudades on Ciudades.IDCiudad=Domicilios.Ciudad 
	where (Facturas.EnContabilium=0 or Facturas.EnContabilium is null) and FechaCAE is not null
	and FechaCAE>'2015-01-01' and Facturas.Modo='R'
	and Facturas.IDfactura in (
		select distinct IDfactura from [wi271584_clubin].dbo.FacturasDetalle where IDPlanCuentaContabilium>0
	)

	--actualizo IDCIUDAD
	update #temp 
	set IDCiudad = (select top 1 c.IDCiudad from  contabilium_prod.dbo.Ciudades c where #temp.IDProvincia = (c.IDProvincia))

	--Actualizo los idpersonas que existen
	update #temp 
	set IDPersona = isnull((select TOP 1 IDPersona from  contabilium_prod.dbo.Personas p where #temp.TipoDocumento = p.TipoDocumento and #temp.NroDocumento = p.NroDocumento and p.IDUsuario=@IDUsuario),0)

	update  P  set 	
		P.Tipo =@Tipo,
		P.IDUsuario=@IDUsuario,
		P.RazonSocial=  t.RazonSocial ,
		P.CondicionIva= t.CondicionIva ,
		P.Telefono=t.Telefono ,
		P.Web= t.Web,
		P.Email= t.Email,
		P.Observaciones= t.Observaciones,
		P.Domicilio=t.Domicilio ,
		P.PisoDepto=t.PisoDepto ,
		P.CodigoPostal=t.CodigoPostal, 
		P.EmailsEnvioFc= t.EmailsEnvioFc,
		P.Personeria=@Personeria,
		P.NombreFantansia=t.NombreFantasia, 
		P.IDProvincia=t.IDProvincia ,
		P.IDCiudad= t.IDCiudad,
		P.Contacto= t.Contacto,
		P.Codigo=t.Codigo 
	from contabilium_prod.dbo.Personas as P 
	inner JOIN #temp as t on P.IDPersona=t.IDPersona
	where t.IDPersona>0

	--inserto en contabilium

	--1. personas
	insert into contabilium_prod.dbo.Personas 
		(Tipo,
		IDUsuario,
		RazonSocial,
		TipoDocumento,
		NroDocumento,
		CondicionIva,
		Telefono,
		Web,
		Email,
		Observaciones,
		Domicilio,
		PisoDepto,
		CodigoPostal,
		EmailsEnvioFc,
		Personeria,
		AlicuotaIvaDefecto,
		TipoComprobanteDefecto,
		FechaAlta, 
		NombreFantansia, 
		IDProvincia,
		IDCiudad,
		Contacto,
		Codigo
		)
	 select distinct @Tipo as Tipo,@IDUsuario as IDUsuario, RazonSocial, TipoDocumento,
	  NroDocumento,CondicionIva, Telefono,Web, Email, @Observaciones,Domicilio, 
	  PisoDepto,CodigoPostal,@EmailsEnvioFc, @Personeria,@AlicuotaIvaDefecto,@TipoComprobanteDefecto, FechaAlta,
	  NombreFantasia,IDProvincia,IDCiudad,Contacto, Codigo  
	  from #temp as t
	  where t.IDPersona=0
  
	update #temp 
	set IDPersona = isnull((select top 1 IDPersona from  contabilium_prod.dbo.Personas p where #temp.TipoDocumento = p.TipoDocumento and #temp.NroDocumento = p.NroDocumento and p.IDUsuario=@IDUsuario),0)
	where IDPersona=0

	--2. Comprobantes cabecera
	insert into contabilium_prod.dbo.Comprobantes 
	(IDUsuario,
	IDPuntoVenta,
	IDPersona,
	TipoDestinatario,
	TipoDocumento,
	NroDocumento,
	Modo,
	Tipo,
	FechaComprobante,
	FechaVencimiento,
	ImporteTotalNeto,
	ImporteTotalBruto,
	CondicionVenta,
	Numero,
	TipoConcepto,
	[CAE],
	FechaCAE,
	FechaAlta,
	FechaProceso,
	Enviada,
	Saldo,
	ImporteNoGrabado,
	[PercepcionIVA],
	[PercepcionIIBB], Observaciones)
	select  @IDUsuario as IDUsuario, @IDPuntoDeVenta as IDPuntoDeVenta, IDPersona
	,@TipoDestinatario as TipoDestinatario,t.TipoDocumento , t.NroDocumento,
	@Modo,
	(CASE WHEN t.CondicionIva='RI' then 'FCA' ELSE 'FCB' END) AS Tipo,
	FechaProceso as FechaComprobante,FechaCAE as FechaVencimiento,t.ImporteTotal,(t.ImporteTotal-t.TotalIva),@CondicionVenta as CondicionVenta,
	--cast(Numero as int) as Numero
	(select top 1 cast(splitdata as int) from  [wi271584_clubin].dbo.Split(Numero,'-') order by 1 desc) as Numero
	,@TipoConcepto as TipoConcepto, t.CAE,t.FechaCAE,
	 getdate() as FechaAlta,t.FechaProceso, 1,@Saldo,@ImporteNoGrabado,@PercepcionIVA,@PercepcionIIBB,'Migrado desde RedIN'
	from #temp as t where t.idPersona>0

	--3. Comprobantes detalle
	insert into contabilium_prod.[dbo].[ComprobantesDetalle]
	(IDComprobante,
	[Cantidad],
	[Concepto],
	[PrecioUnitario],
	[Iva],
	[Bonificacion],
	[IDPlanDeCuenta])
	select
	(select top 1 IDComprobante from contabilium_prod.[dbo].Comprobantes c where c.CAE=f.CAE and c.IDUsuario=@IDUsuario) as IDComprobante, Cantidad, Concepto, PrecioUnitario, Iva, @Bonificacion, IDPlanCuentaContabilium
	 from #temp as t 
	inner join [wi271584_clubin].dbo.FacturasDetalle d on d.IDFactura=t.IDFactura
	inner join [wi271584_clubin].dbo.Facturas f on f.IDFactura=d.IDFactura
	where t.idPersona>0 and d.IDPlanCuentaContabilium>0

	-- 4 - los marco como procesados. Mejorar esto.
	update  [wi271584_clubin].dbo.Facturas set EnContabilium=1  
	where (EnContabilium=0 or EnContabilium is null) and CAE in 
	(select CAE from contabilium_prod.dbo.Comprobantes where IDUsuario=@IDUsuario)



