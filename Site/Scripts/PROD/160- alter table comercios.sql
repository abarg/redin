alter table Comercios add IDDealer int 

ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Dealer] FOREIGN KEY([IDDealer])
REFERENCES [dbo].[Dealer] ([IDDealer])
GO

ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Dealer]
GO


