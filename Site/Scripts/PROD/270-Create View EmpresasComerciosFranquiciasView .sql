Create view EmpresasComerciosFranquiciasView as 
select c.IDComercio,c.NombreFantasia as Comercio,e.IDEmpresa,e.Nombre as Empresa,f.IDFranquicia,f.NombreFantasia  as Franquicia from EmpresasComercios ec 
join Empresas e on e.IDEmpresa = ec.IDEmpresa
join Comercios c on c.IDComercio = ec.IDComercio
join Franquicias f on f.IDFranquicia = c.IDFranquicia
