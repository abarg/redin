USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Comercios_TopSocios]    Script Date: 30/12/2015 11:49:26 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Comercios_TopSocios]
(
	@IDComercio int
)
AS

	
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesView t
		inner join tarjetas tr on t.Numero = tr.Numero
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND t.IDComercio = @IDComercio
		group by t.Numero, t.Nombre, t.Apellido, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
GO


