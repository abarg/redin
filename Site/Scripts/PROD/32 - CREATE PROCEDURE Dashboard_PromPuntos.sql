CREATE  PROCEDURE [dbo].[Dashboard_PromPuntos] (@FechaDesde datetime,
  @FechaHasta datetime)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
 SELECT '' as label,ISNULL((ROUND(AVG(CAST(Puntos AS FLOAT)), 2)/100),0)  as data  FROM [DashboardView] where FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
END

GO


