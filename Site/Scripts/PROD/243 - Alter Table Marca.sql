Alter table marcas add
POSMostrarFormaPago bit not null default 1,
POSMostrarNumeroTicket bit  not null default 1,
POSFooter1 varchar(40),
POSFooter2 varchar(40),
POSFooter3 varchar(40),
POSFooter4 varchar(40),
POSMostrarMenuFidelidad bit  not null default 1, 
POSMostrarMenuGift bit  not null default 1, 
POSMostrarChargeGift bit not null default 1,
POSMostrarLOGO bit  not null default 1

Alter table marcas drop column  POSMostrarFormaPago

Alter table marcas drop column POSMostrarNumeroTicket 
Alter table marcas drop column  POSFooter1
Alter table marcas drop column  POSFooter2
Alter table marcas drop column  POSFooter3
Alter table marcas drop column  POSFooter4
Alter table marcas drop column  POSMostrarMenuFidelidad 
Alter table marcas drop column  POSMostrarMenuGift 
Alter table marcas drop column  POSMostrarChargeGift 
Alter table marcas drop column  POSMostrarLOGO

