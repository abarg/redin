Create table TicketsArchivos(
IDTicketArchivo int primary key identity(1,1),
IDDetalleTicket int foreign key (IDDetalleTicket) references TicketsDetalle(IDDetalleTicket),
NombreArchivo text
)

insert into TicketsArchivos select td.IDDetalleTicket,td.Adjunto from TicketsDetalle td where adjunto is not null

alter table TicketsDetalle drop column adjunto 

