USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[HistoricoFacturacion]    Script Date: 07/11/2016 01:39:22 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HistoricoFacturacion](
	[IDHistoricoFacturacion] [int] IDENTITY(1,1) NOT NULL,
	[PromedioTickets] [decimal](18, 2) NULL,
	[TotalPuntos] [decimal](18, 2) NULL,
	[TotalArancel] [decimal](18, 2) NULL,
	[TotalTRMensual] [int] NULL,
	[TasaDeUso] [decimal](18, 2) NULL,
	[PromedioPuntos] [decimal](18, 2) NULL,
	[PromedioDescuento] [decimal](18, 2) NULL,
	[FacturacionComercios] [decimal](18, 2) NULL,
	[PromedioArancel] [decimal](18, 2) NULL,
	[TotalFacturacion] [decimal](18, 2) NULL,
	[TotalCanjes] [int] NULL,
	[TotalFacturado] [decimal](18, 2) NULL,
	[NuevosCelulares] [int] NULL,
	[NuevosEmails] [int] NULL,
	[TotalTajetasActivas] [int] NULL,
	[NuevosSocios] [int] NULL,
	[Mes] [int] NULL,
	[Anio] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDHistoricoFacturacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


