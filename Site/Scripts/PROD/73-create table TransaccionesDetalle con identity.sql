USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[TransaccionesDetalle]    Script Date: 10/11/2015 16:59:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TransaccionesDetalle](
	[IDTransaccion] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[IDTransaccionDetalle] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__Transacc__D0CC87988219A771] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TransaccionesDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TransaccionesDetalle_Productos] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Productos] ([IDProducto])
GO

ALTER TABLE [dbo].[TransaccionesDetalle] CHECK CONSTRAINT [FK_TransaccionesDetalle_Productos]
GO

ALTER TABLE [dbo].[TransaccionesDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TransaccionesDetalle_Transacciones] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transacciones] ([IDTransaccion])
GO

ALTER TABLE [dbo].[TransaccionesDetalle] CHECK CONSTRAINT [FK_TransaccionesDetalle_Transacciones]
GO


