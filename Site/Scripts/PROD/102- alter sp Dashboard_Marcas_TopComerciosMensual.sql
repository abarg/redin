GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_TopComerciosMensual]    Script Date: 12/30/2015 10:00:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_Marcas_TopComerciosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDMarca int)    
AS
BEGIN
   --declare @POSPropios bit
	--SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca

	SELECT  top 10
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		/*select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesMarcasView t
		inner join Comercios c on t.IDComercio=c.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where t.Empresa is not null and t.ImporteOriginal>1
		AND c.IDMarca = @IDMarca --and ((@POSPropios=1 and C.IDMarca=@IDMarca) OR @POSPropios=0) 
		AND t.FechaTransaccion >= @FechaDesde AND t.FechaTransaccion <= @FechaHasta
		group by c.NombreFantasia, t.SDS, t.NroEstablecimiento,d.Domicilio, t.Operacion	*/
		select c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr--TransaccionesMarcasView t
		inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		--inner join Marcas m on tr.IDMarca=m.IDMarca
		where tr.Importe>1
		AND tr.FechaTransaccion >= @FechaDesde AND tr.FechaTransaccion <= @FechaHasta
		--AND tr.Operacion not in ('Carga','Descarga')
		AND C.IDMarca = @IDMarca
		group by c.NombreFantasia, d.Domicilio, tr.Operacion
		---ORDER BY cuatro DESC
	) AS T
	
	group by uno, dos, tres
	order by cuatro desc

END
