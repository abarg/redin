USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[GenerarFacturasManuales]    Script Date: 02/10/2015 09:41:48 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[GenerarFacturasManuales]
(
@ID int,
@fechaDesde datetime,
@fechaHasta datetime,
@concepto varchar(200),
@usuario varchar(50)
)

as

declare @idComercio int
declare @cant int
declare @condicionIva varchar(50)
declare @cuit varchar(50)
declare @idfactura int
declare @fechaDesdeAux datetime
declare @fechaHastaAux datetime

set @fechaHastaAux = DATEADD(day,1,@fechaHasta)
set @fechaDesdeAux = @fechaDesde

declare CURSORITO cursor for
select idComercio, NroDocumento, CondicionIva from Comercios where IDComercio=@ID
open CURSORITO
-- Avanzamos un registro y cargamos en las variables los valores encontrados en el primer registro
fetch next from CURSORITO into @idComercio, @cuit, @condicionIva
while @@fetch_status = 0
    begin
      
      select @cant = COUNT(IDMovimiento)
      from Movimientos where IDComercio = @idComercio and IDFactura is null
    	and Fecha between @fechaDesdeAux and @fechaHastaAux and Precio <>0
      
      if(@cant>0)
      begin
      
      	Insert into Facturas
      	values(@idComercio, '',@condicionIva, @fechaDesde, @fechaHasta, null, null, 0, GETDATE(),null,GETDATE(),null,0,null,null,null, @cuit, 'M', null,@usuario,1, null)
      	
      	set @idfactura= SCOPE_IDENTITY()
                
        --Insertamos los movimientos
        /*if(@condicionIva='MONOTRIBUTISTA')
          begin
            
          	insert into FacturasDetalle
          	select @idfactura, Concepto  + ' ' + convert(varchar(10),Fecha,103) , 1, (Precio+(Precio * 0.21))
          	from Movimientos where IDComercio = @idComercio and IDFactura is null
          	and Fecha between @fechaDesdeAux and @fechaHastaAux and Precio <>0
          end
        else
          begin*/
            insert into FacturasDetalle
          	select @idfactura, Concepto  + ' ' + convert(varchar(10),Fecha,103) , 1, Precio, 21
          	from Movimientos where IDComercio = @idComercio and IDFactura is null
          	and Fecha between @fechaDesdeAux and @fechaHastaAux and Precio <>0
          --end
          
          
      	update Movimientos
      	set IDFactura=@idfactura
      	where IDComercio = @idComercio and IDFactura is null and Precio<>0
      	and Fecha between @fechaDesdeAux and @fechaHastaAux
          
      	update Facturas
      	set ImporteTotal = (select ISNULL(SUM(PrecioUnitario),0) from FacturasDetalle where IDFactura=@idfactura)
      	where IDFactura = @idfactura
      
      end
      
    	fetch next from CURSORITO into @idComercio, @cuit, @condicionIva
    end
    -- cerramos el cursor
close CURSORITO
deallocate CURSORITO

