create table FormularioTarjetas(
	IDFormularioTarjetas int not null primary key identity(1,1),
	IDFranquicia int foreign key (IDFranquicia) references Franquicias(IDFranquicia),
	Fecha datetime,
	UnidadNegocio varchar(50),
	Cliente varchar (50),
	Descripcion varchar(128),
	ObservacionAnverso text,
	ObservacionReverso text,
	ImprimirSimulacion bit,
	CorregirConSimulacion bit,
	CorregirSinSimulacion bit,
	Observaciones text,
	RazonSocial varchar(128),
	CUIT varchar(20),
	CondicionIVA varchar(50),
	Contacto varchar (50),
	TelContacto varchar(50),
	EmailContacto varchar (50),
	FormaPago varchar (50),
	FechaEnvioProveedor datetime,
	Nombre varchar (50),
	DNI varchar (20),
	Domicilio varchar (128),
	Localidad varchar (50),
	CP varchar (20),
	IDProvincia int foreign key (IDProvincia) references Provincias(IDProvincia),
	FechaEnvio datetime, 
	Empresa varchar(50),
	NroGuia varchar (20),
	FormaEnvio varchar(50),
	logo varchar(50),
	imagenes varchar(50),
	anverso varchar(50),
	reverso varchar(50),
	aprobacionCliente varchar(50),
	comprobante varchar(50),
	formularioAprobacion varchar(50),
	baseTracks varchar(50),
	importeFactura varchar(50),
	adjuntarGuia varchar(50)
)

create table Costos (
	IDCosto int not null primary key identity(1,1),
	IDFormularioTarjetas int foreign key (IDFormularioTarjetas) references FormularioTarjetas(IDFormularioTarjetas),
	Concepto varchar (50),
	PrecioUnitario decimal(10,2),
	Cantidad int, 
	Total decimal (10,2)
)

create table Especificaciones(
	IDEspecificacion int not null primary key identity(1,1),
	IDFormularioTarjetas int foreign key (IDFormularioTarjetas) references FormularioTarjetas(IDFormularioTarjetas),
	Concepto varchar (50),
	Tiene varchar (4),
	Observaciones varchar (100)
)

