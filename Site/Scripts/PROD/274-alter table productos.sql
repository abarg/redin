

alter table productos add StockActual int
alter table productos add StockMinimo int
go

update productos set StockActual=0

update productos set StockMinimo=0
go
alter table productos  alter column StockActual int
alter table productos alter column StockMinimo int not null