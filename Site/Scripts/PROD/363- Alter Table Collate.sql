-- Search Franquicias

ALTER TABLE [dbo].[Franquicias] ALTER COLUMN[NombreFantasia]
            varchar(100)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  
ALTER TABLE [dbo].[Franquicias] ALTER COLUMN [RazonSocial]
            varchar(128)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Marcas

ALTER TABLE [dbo].[Marcas] ALTER COLUMN [Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Socios

ALTER TABLE [dbo].[Socios] ALTER COLUMN [Nombre]
            varchar(100)COLLATE Traditional_Spanish_ci_ai  NULL;  

ALTER TABLE [dbo].[Socios] ALTER COLUMN [Apellido]
            varchar(100)COLLATE Traditional_Spanish_ci_ai  NULL;  

GO  
ALTER TABLE [dbo].[Socios] ALTER COLUMN [Email]
            varchar(100)COLLATE Traditional_Spanish_ci_ai  NULL;  
GO  

--Search Comercios

--Para cambiar el collate de la Tabla Comercio- Campo NombreFantasia, debe hacerse de forma manual


ALTER TABLE [dbo].[Comercios] ALTER COLUMN [RazonSocial]
            varchar(128)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Empresas

ALTER TABLE [dbo].[Empresas] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Multimarcas

ALTER TABLE [dbo].[Multimarcas] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Dealer

ALTER TABLE [dbo].[Dealer] ALTER COLUMN[Nombre]
            varchar(100)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Rubro

ALTER TABLE [dbo].[Rubros] ALTER COLUMN[Nombre]
            varchar(100)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Provincia

ALTER TABLE [dbo].[Provincias] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search ciudades

ALTER TABLE [dbo].[Ciudades] ALTER COLUMN[Nombre]
            varchar(100)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Zonas

ALTER TABLE [dbo].[Zonas] ALTER COLUMN[Nombre]
            varchar(100)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Profesiones

ALTER TABLE [dbo].[Profesiones] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search hobbies

ALTER TABLE [dbo].[Hobbies] ALTER COLUMN[Nombre]
            varchar(40)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search Motivos

ALTER TABLE [dbo].[Motivos] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search TarjetaDeCredito

ALTER TABLE [dbo].[TarjetasDeCredito] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO  

-- Search BIN

ALTER TABLE [dbo].[BIN] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 

-- Search sms	

ALTER TABLE [dbo].[SMS] ALTER COLUMN[Nombre]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 


-- Search Sorteos	

ALTER TABLE [dbo].[Sorteos] ALTER COLUMN[Titulo]
            varchar(50)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 

-- Search Proveedores

ALTER TABLE [dbo].[Proveedores] ALTER COLUMN [NombreFantasia]
            varchar(100)COLLATE Traditional_Spanish_ci_ai  NULL;  
GO  
ALTER TABLE [dbo].[Proveedores] ALTER COLUMN [RazonSocial] 
            varchar(128)COLLATE Traditional_Spanish_ci_ai  NULL;  
GO  


-- Search Premios	

ALTER TABLE [dbo].[Premios]  ALTER COLUMN[Descripcion]
            varchar(200)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 

-- Search Pines	

ALTER TABLE [dbo].[Pines] ALTER COLUMN [Control]
            varchar(6)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 
ALTER TABLE [dbo].[Pines] ALTER COLUMN [Parte1]
            varchar(10)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 
ALTER TABLE [dbo].[Pines] ALTER COLUMN [Parte2]
            varchar(10)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 

-- Search Keywords	

ALTER TABLE [dbo].[Keywords] ALTER COLUMN [Codigo]
            varchar(10)COLLATE Traditional_Spanish_ci_ai NOT NULL;  
GO 

