CREATE  PROCEDURE [dbo].[Dashboard_PromDescuento] (@FechaDesde datetime,
  @FechaHasta datetime)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
   SELECT '' as label,ROUND(AVG(CAST(ImporteAhorro AS FLOAT)*100/Importe), 2)/100  as data FROM [DashboardView] where FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
END

GO


