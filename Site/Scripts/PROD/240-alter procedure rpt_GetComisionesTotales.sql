
/****** Object:  StoredProcedure [dbo].[Rpt_GetComisionesTotales]    Script Date: 04/10/2016 05:32:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[Rpt_GetComisionesTotales]
	@fechaDesde datetime,
  @fechaHasta datetime,
  @IDFranquicia int
AS
BEGIN
    
   SELECT  
  IDComercio, 
	 (CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN  (ComisionTtCp) WHEN 'Carga' THEN  (ComisionTtCp) ELSE  ((ComisionTtCp*-1)) end)+
	 ( CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN  (ComisionTpCp) WHEN 'Carga' THEN  (ComisionTpCp) ELSE  ((ComisionTpCp*-1)) END)+
	 ( CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN  (ComisionTpCt) WHEN 'Carga' THEN  ( ComisionTpCt) ELSE  ((ComisionTpCt*-1)) END) as Total
    FROM
    (
 SELECT 
      tr.FechaTransaccion,
      c.IDComercio,
	  tr.Operacion,
	  c.IDFranquicia,

	  ISNULL(tr.Importe,0) as ImporteOriginal,


      ISNULL(dbo.getComisionFranquicia('TtCp',@IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTtCp,
      ISNULL(dbo.getComisionFranquicia('TpCp',@IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTpCp,
      ISNULL(dbo.getComisionFranquicia('TpCt',@IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTpCt

      FROM Comercios c
  	  INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )

     )as t
        


END



