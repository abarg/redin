CREATE PROCEDURE Dashboard_Franq_TarjetasEmitidas_Listado
(@idFranquicia int)
AS

select distinct
m.Nombre as Marca,
count(t.IDMarca) as Total,
(
  select isnull(SUM(cantidad),0) from (
  select 1 as cantidad
  FROM Transacciones tr
  inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  inner join Tarjetas tt on tr.NumTarjetaCliente = tt.Numero
  WHERE tr.Importe > 1 and tt.IDMarca = m.IDMarca and tt.IDFranquicia = @idFranquicia
  group by NumTarjetaCliente) as R
) as Activas
from Marcas m
inner join Tarjetas t on t.IDMarca=m.IDMarca
where t.IDFranquicia = @idFranquicia
group by m.Nombre,m.IDMarca
order by Activas desc