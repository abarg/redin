USE [RedIN-QA]
GO
/****** Object:  StoredProcedure ProcesarPines    Script Date: 22/09/2015 16:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE ProcesarPines
 AS

BEGIN
	SET NOCOUNT ON;
			
	INSERT INTO Pines 
	(IDEmpresaCanje,
	Control,
	Parte1,
	Parte2,
	Descripcion,
	CostoInterno,
	CostoPuntos)
	SELECT
		IDEmpresaCanje,
		RTRIM(LTRIM(Control)),
		RTRIM(LTRIM(Parte1)),
		RTRIM(LTRIM(Parte2)),
		Descripcion,
		CostoInterno,
		CostoPuntos
	FROM PinesTemp
	WHERE 
	NOT EXISTS (SELECT 1 FROM Pines
	WHERE PinesTemp.Control = Pines.Control
	AND	PinesTemp.Control is not null and  PinesTemp.Parte1 = Pines.Parte1
	AND	PinesTemp.Parte1 is not null and  PinesTemp.Parte2 = Pines.Parte2
	AND	PinesTemp.Parte2 is not null )

	UPDATE T1
	SET
		T1.IDEmpresaCanje = T2.IDEmpresaCanje,
		T1.Control = RTRIM(LTRIM(T2.Control)),
		T1.Parte1 = RTRIM(LTRIM(T2.Parte1)),
		T1.Parte2 = RTRIM(LTRIM(T2.Parte2)),
		T1.Descripcion = T2.Descripcion,
		T1.CostoInterno = T2.CostoInterno,
		T1.CostoPuntos = T2.CostoPuntos
	FROM Pines as T1
	INNER JOIN PinesTemp as T2 ON T1.Control = T2.Control and T1.Parte2 = T2.Parte2 and T1.Parte2 = T2.Parte2
END