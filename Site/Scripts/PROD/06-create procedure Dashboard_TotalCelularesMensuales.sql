

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Dashboard_TotalCelularesMensuales]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime)
  
AS
BEGIN
  select  
	isnull (count(IDSocio),0) as data, 'Cant' as label 
	from Socios 
	where Celular is not null AND Celular <> '' and  FechaAlta >= @FechaDesde AND FechaAlta <= @FechaHasta
END
GO
