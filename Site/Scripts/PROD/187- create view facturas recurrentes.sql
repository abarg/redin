USE [RedIN-QA]
GO

/****** Object:  View [dbo].[FacturasRecurrentesView]    Script Date: 05/05/2016 12:13:55 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[FacturasRecurrentesView] as
  
SELECT 
f.PeriodoDesde,f.PeriodoHasta,
  (select  count(Enviada)
  FROM [RedIN-QA].[dbo].[Facturas] f1
  where modo='R'  and
   Enviada = 1 and
   f1.PeriodoDesde = f.PeriodoDesde and 
   f1.PeriodoHasta = f.PeriodoHasta) as Enviadas,

  (select  count(Enviada)
  FROM [RedIN-QA].[dbo].[Facturas] f2
  where modo='R'  and
   Enviada = 0 and
   f2.PeriodoDesde = f.PeriodoDesde and 
   f2.PeriodoHasta = f.PeriodoHasta) as NoEnviadas,

   count(FechaRecibida) as Leidas

  FROM [RedIN-QA].[dbo].[Facturas] f
  where modo='R'
  group by f.PeriodoDesde,f.PeriodoHasta



GO


