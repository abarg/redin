USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[HistoricoFacturacionFranquicias]    Script Date: 07/11/2016 01:39:26 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HistoricoFacturacionFranquicias](
	[IDHistoricoFacturacionFranquicia] [int] IDENTITY(1,1) NOT NULL,
	[IDFranquicia] [int] NOT NULL,
	[TarjetasPropiasComerciosPropios] [decimal](18, 2) NULL,
	[TarjetasPropiasOtrosComercios] [decimal](18, 2) NULL,
	[OtrasTarjetasComerciosPropios] [decimal](18, 2) NULL,
	[TotalSinIVA] [decimal](18, 2) NULL,
	[TotalConIVA] [decimal](18, 2) NULL,
	[PromedioTickets] [decimal](18, 2) NULL,
	[TotalTRMensual] [int] NULL,
	[TasaDeUso] [decimal](18, 2) NULL,
	[PromedioDescuento] [decimal](18, 2) NULL,
	[FacturacionComercios] [decimal](18, 2) NULL,
	[PromedioArancel] [decimal](18, 2) NULL,
	[NuevosCelulares] [int] NULL,
	[NuevosEmails] [int] NULL,
	[TotalTajetasActivas] [int] NULL,
	[NuevosSocios] [int] NULL,
	[Mes] [int] NULL,
	[Anio] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDHistoricoFacturacionFranquicia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HistoricoFacturacionFranquicias]  WITH CHECK ADD FOREIGN KEY([IDFranquicia])
REFERENCES [dbo].[Franquicias] ([IDFranquicia])
GO


