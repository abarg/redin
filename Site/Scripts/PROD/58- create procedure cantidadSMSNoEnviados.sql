GO

/****** Object:  StoredProcedure [dbo].[Dashboard_CantidadTarjetas]    Script Date: 20/10/2015 17:35:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[CantidadSMSNOEnviados] (@FechaDesde datetime,
  @FechaHasta datetime, @IDMarca int)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
	SELECT count(*) as cantenviados, s.IDMarca, m.Nombre, s.Costo FROM SMSEnvios s left join Marcas m on s.IDMarca= m.IDMarca  where  s.IDMarca=@IDMarca AND s.FechaEnvio >= @FechaDesde AND s.FechaEnvio <= @FechaHasta and s.Enviado=0  group by s.IDMarca, m.Nombre ,s.Costo
END

GO

