Alter table provincias add IDPais int

go
ALTER TABLE provincias ADD  FOREIGN KEY (IDPais)
REFERENCES Paises(IDPais)

go

Update provincias set IDPais = 1 where IDProvincia <= 24
go
Alter Table Provincias alter column IDPais int not null
