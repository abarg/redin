alter table Socios add CostoTransaccionalSoloPuntos decimal(5,2)
alter table Socios add CostoTransaccionalCanje decimal(5,2)
alter table Socios add CostoTransaccionalConDescuento decimal(5,2)

alter table Socios drop column CostoTransaccional