USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[GetReporteTerminales]    Script Date: 12/02/2016 11:34:44 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetReporteTerminalesEmpresas]
(
	@Estado varchar(50),
	@SDS varchar(50),
	@NombreEst varchar(100),
	@IDEmpresa int
)
AS

SELECT 
c.SDS,
c.NombreFantasia as 'Nombre',
d.Domicilio,
e.IDEmpresa,
e.Nombre as 'NombreEmpresa',
ci.Nombre as 'Localidad',
isnull(convert(varchar(10), c.FechaAlta, 103),'') as 'Fecha Carga',
isnull(convert(varchar(10), c.FechaAltaDealer, 103),'') as 'Fecha Alta',
c.CodigoDealer as 'Dealer',
c.POSTipo as Tipo,
c.POSSistema as 'Tipo Terminal',
c.POSTerminal as 'Terminal',
c.POSEstablecimiento as 'Establecimiento',
isnull(convert(varchar(10), c.POSFechaActivacion, 103),'') as 'Fecha Activacion',
isnull(convert(varchar(10), c.POSFechaReprogramacion, 103),'') as 'Fecha Reprogramacion',
c.POSReprogramado as 'Reprogramado',
c.POSInvalido as 'Comercio Inv�lido',
c.Activo as 'Activo',
c.POSObservaciones  as 'Observaciones',
ISNULL((
SELECT count(IDTransaccion) 
FROM Transacciones tr WHERE c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
AND tr.Importe>1 AND tr.Origen='Visa'
),0) as 'CantTR',
ISNULL((
SELECT top 1 ISNULL(Importe,0)
FROM Transacciones tr WHERE c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
AND tr.Origen='Visa' and TipoTransaccion<>'000005' order by FechaTransaccion desc
),0) as 'UltimoImporte',
c.IDFranquicia
FROM Comercios c
LEFT JOIN Domicilios d ON c.IDDomicilio = d.IDDomicilio
LEFT JOIN Ciudades ci on ci.IDCiudad = d.Ciudad
LEFT JOIN EmpresasComercios ec on  ec.IDComercio=c.IDComercio
inner JOIN Empresas e on  e.IDEmpresa= ec.IDEmpresa
WHERE
(@SDS is null OR c.SDS like '%'+@SDS)
AND (@NombreEst is null OR  c.NombreEst like '%'+@NombreEst+'%')
AND ( e.IDEmpresa = @IDEmpresa)

GO


