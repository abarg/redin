USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[IntegracionesTokens]    Script Date: 29/10/2015 17:01:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[IntegracionesTokens](
	[IDIntegracionesTokens] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[SessionID] [varchar](50) NOT NULL,
	[FechaValida] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[IntegracionesTokens]  WITH CHECK ADD  CONSTRAINT [FK_IntegracionesTokens_Integraciones] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Integraciones] ([IDUsuario])
GO

ALTER TABLE [dbo].[IntegracionesTokens] CHECK CONSTRAINT [FK_IntegracionesTokens_Integraciones]
GO


