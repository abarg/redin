alter table Alertas add IDEmpresa int
ALTER TABLE [dbo].[Alertas]  WITH CHECK ADD  CONSTRAINT [FK_Alertas_Empresas] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresas] ([IDEmpresa])
GO

ALTER TABLE [dbo].[Alertas] CHECK CONSTRAINT [FK_Alertas_Empresas]
GO

