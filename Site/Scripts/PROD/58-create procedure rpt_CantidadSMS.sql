GO

/****** Object:  StoredProcedure [dbo].[Rpt_CantidadSMS]    Script Date: 23/10/2015 14:24:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[Rpt_CantidadSMS] (@FechaDesde datetime,
  @FechaHasta datetime, @IDMarca int)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
		SELECT sum(case s.Enviado when 1 then 1 else 0 end) as cantEnviados,
		sum(case s.Enviado when 0 then 1 else 0 end) as cantNoenviados,
		count(*) as cant,s.IDMarca, m.Nombre, s.Costo FROM SMSEnvios s left join Marcas m 
		on s.IDMarca= m.IDMarca  where   (@IDMarca = 0 OR  s.IDMarca = @IDMarca) and s.FechaEnvio >= @FechaDesde AND
		s.FechaEnvio <= @FechaHasta  group by s.IDMarca,s.Costo, m.Nombre
END




GO


