
GO
/****** Object:  StoredProcedure [dbo].[Rpt_Franq_Facturacion]    Script Date: 17/01/2017 05:46:28 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Rpt_Franq_Facturacion]
(
	@fechaDesde datetime,
  @fechaHasta datetime,
  @IDFranquicia int
)
AS
BEGIN
      
    SELECT 
    POSTipo, POSSistema, POSTerminal,
  	SDS, NombreFantasia, NroDocumento, NumEst, Marca, IDMarca,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImporteOriginal) WHEN 'Carga' THEN SUM(ImporteOriginal) ELSE SUM(ImporteOriginal*-1) END AS ImporteOriginal,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImporteAhorro) WHEN 'Carga' THEN SUM(ImporteAhorro) ELSE SUM(ImporteAhorro*-1) END AS ImporteAhorro,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ImportePagado) WHEN 'Carga' THEN SUM(ImportePagado) ELSE SUM(ImportePagado*-1) END AS ImportePagado,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(CostoRedIn) WHEN 'Carga' THEN SUM(CostoRedIn) ELSE SUM(CostoRedIn*-1) END AS CostoRedIn,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(Ticket) WHEN 'Carga' THEN SUM(Ticket) ELSE SUM(Ticket*-1) END AS Ticket,
    SUM(CantOperaciones) AS CantOperaciones,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ComisionTtCp) WHEN 'Carga' THEN SUM(ComisionTtCp) ELSE SUM(ComisionTtCp*-1) END AS ComisionTtCp,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ComisionTpCp) WHEN 'Carga' THEN SUM(ComisionTpCp) ELSE SUM(ComisionTpCp*-1) END AS ComisionTpCp,
    CASE Operacion WHEN 'CuponIN' THEN SUM(ImporteOriginal) WHEN 'Venta' THEN SUM(ComisionTpCt) WHEN 'Carga' THEN SUM(ComisionTpCt) ELSE SUM(ComisionTpCt*-1) END AS ComisionTpCt
    INTO #TMPFACTURACION
    FROM
    (
      SELECT 
        --CONVERT(varchar(10), tr.FechaTransaccion, 103) as Fecha,
    		--tr.Origen,
        ter.POSTipo,
    		ter.POSSistema,
    	  ter.POSTerminal,
    		tr.Operacion,
    		c.SDS,
    		c.NombreFantasia,
        c.NroDocumento,
    		ter.NumEst,
    		--d.Ciudad as Localidad,
        m.Nombre as Marca,
        m.IDMarca,
    		ISNULL(SUM(tr.Importe),0) as ImporteOriginal,
    		ISNULL(SUM(tr.ImporteAhorro),0) AS ImporteAhorro,
    		ISNULL(SUM(tr.Importe - tr.ImporteAhorro),0) as ImportePagado,
    		ISNULL(SUM(tr.UsoRed),0) as CostoRedIn,
    		CAST(
          ISNULL(SUM(tr.Importe - tr.ImporteAhorro + (tr.UsoRed*1.21)),0) AS decimal(10,2)
        ) AS Ticket,
        COUNT(*) as CantOperaciones,
        dbo.getComisionFranquicia('TtCp', @IDFranquicia, c.IDFranquicia, t.IDFranquicia, isnull(sum((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))),0)) as ComisionTtCp,
        dbo.getComisionFranquicia('TpCp', @IDFranquicia, c.IDFranquicia, t.IDFranquicia, isnull(sum((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))),0)) as ComisionTpCp,
        dbo.getComisionFranquicia('TpCt', @IDFranquicia, c.IDFranquicia, t.IDFranquicia, isnull(sum((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))),0)) as ComisionTpCt
        
      FROM Comercios c
	  INNER JOIN Terminales ter on ter.IDComercio = c.IDComercio
  		INNER JOIN Transacciones tr on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  		LEFT JOIN Marcas m on m.IDMarca = c.IDMarca 
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			t.IDFranquicia = @IDFranquicia or c.IDFranquicia = @IDFranquicia
		)
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN')
		)
      GROUP BY t.IDFranquicia, tr.Operacion, tr.Arancel, tr.Importe, tr.ImporteAhorro, tr.Puntos, c.NroDocumento, ter.POSSistema, ter.POSTipo, c.SDS, c.NombreFantasia, ter.POSSistema, ter.NumEst, ter.POSTerminal, c.IDFranquicia, m.IDMarca, m.Nombre
      --, tr.Origen, FechaTransaccion, 
    ) AS T
    GROUP BY Operacion, POSTipo, POSSistema, POSTerminal,--Fecha, 
  	SDS, NombreFantasia, NroDocumento, NumEst, Marca, IDMarca
    
    SELECT 
      POSTipo, POSSistema, POSTerminal,
    	SDS, NombreFantasia, NroDocumento, NumEst, ISNULL(Marca,'') AS Marca, ISNULL(IDMarca,0) as IDMarca,
      ISNULL(SUM(ImporteOriginal),0) AS ImporteOriginal,
      ISNULL(SUM(ImporteAhorro),0) AS ImporteAhorro,
      ISNULL(SUM(ImportePagado),0) AS ImportePagado,
      ISNULL(SUM(CostoRedIn),0) AS CostoRedIn,
      ISNULL(SUM(Ticket),0) AS Ticket,
      ISNULL(SUM(CantOperaciones),0) AS CantOperaciones,
      ISNULL(SUM(ComisionTtCp),0) AS ComisionTtCp,
      ISNULL(SUM(ComisionTpCp),0) AS ComisionTpCp,
      ISNULL(SUM(ComisionTpCt),0) AS ComisionTpCt
      FROM #TMPFACTURACION
      GROUP BY POSTipo, POSSistema, POSTerminal, SDS, NombreFantasia, NroDocumento, NumEst, Marca, IDMarca


     /*SELECT 
      '' AS POSTipo,'' AS  POSSistema,'' AS  POSTerminal,
    	'' AS SDS,'' AS  NombreFantasia,'' AS  NroDocumento, '' AS NumEst, 
      '' AS Marca, 
      0 IDMarca,
      1.5 AS ImporteOriginal,
      1.5 AS ImporteAhorro,
      1.5 AS ImportePagado,
      1.5 AS CostoRedIn,
      1.5 AS Ticket,
      0 AS CantOperaciones,
      1.5 AS ComisionTtCp,
      1.5 AS ComisionTpCp,
      1.5 AS ComisionTpCt*/
  
END