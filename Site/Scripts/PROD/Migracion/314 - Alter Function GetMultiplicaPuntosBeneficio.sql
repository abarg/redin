
GO
/****** Object:  UserDefinedFunction [dbo].[GetMultiplicaPuntosBeneficio]    Script Date: 23/01/2017 03:12:50 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetMultiplicaPuntosBeneficio] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16),
	@Fecha datetime
)

RETURNS int 
AS
BEGIN
DECLARE @mulPuntos int
DECLARE @sexo char(1)
declare @edad varchar(50)
declare @ok bit
set @ok=0
select @mulPuntos=isnull(MultiplicaPuntos,-1), @sexo=Sexo, @edad=Edad from Beneficios 
where FechaDesde <= @Fecha and FechaHasta>=@Fecha and
(IDComercio=(select top 1 IDComercio from Terminales where NumEst=@NumEst and POSTerminal=@NumTerminal) or 
IDEmpresa=(select top 1 IDEmpresa from EmpresasComercios ec join Terminales t on t.IDComercio = ec.IDComercio where t.NumEst=@NumEst and t.POSTerminal=@NumTerminal ) or 
IDMarca=(select top 1 IDMarca from Comercios c join Terminales t on t.IDComercio = c.IDComercio where NumEst=@NumEst and POSTerminal=@NumTerminal))

if (@mulPuntos >-1)
begin
	declare @edadSocio int
	declare @sexoSocio char(1)
	select @edadSocio=Edad, @sexoSocio=Sexo from Socios s inner join Tarjetas t on s.IDSocio=t.IDSocio where t.Numero=@NumTarjetaCliente
	if(@sexoSocio=@sexo)
	begin
		if(@edad !='Todas')
		begin
			if(@edad LIKE ('%0-18%'))
			begin
				if(@edadSocio<=18)
					set @ok=1
			end
			if (@edad LIKE ('%19-25%'))
			begin
				if(@edadSocio<=19 and @edadSocio<=25)
					set @ok=1
			end
			if (@edad LIKE ('%26-30%'))
			begin
				if(@edadSocio>=26 and @edadSocio<=30)
					set @ok=1
			end
			if (@edad LIKE ('%31-40%'))
			begin
				if(@edadSocio>=31 and @edadSocio<=40)
					set @ok=1
			end
			if (@edad LIKE ('%41-50%'))
			begin
				if(@edadSocio>=41 and @edadSocio<=50)
					set @ok=1
			end
			if (@edad LIKE ('%51-60%'))
			begin
				if(@edadSocio>=51 and @edadSocio<=60)
					set @ok=1
			end
			if (@edad LIKE ('%61%'))
			begin
				if(@edadSocio>=61 )
					set @ok=1
			end
			if(@ok=0)
				set @mulPuntos=-1
		end
	end
	else
	begin
		if(@sexo='T')
		begin
			if(@edad LIKE ('%0-18%'))
			begin
				if(@edadSocio<=18)
					set @ok=1
			end
			if (@edad LIKE ('%19-25%'))
			begin
				if(@edadSocio<=19 and @edadSocio<=25)
					set @ok=1
			end
			if (@edad LIKE ('%26-30%'))
			begin
				if(@edadSocio>=26 and @edadSocio<=30)
					set @ok=1
			end
			if (@edad LIKE ('%31-40%'))
			begin
				if(@edadSocio>=31 and @edadSocio<=40)
					set @ok=1
			end
			if (@edad LIKE ('%41-50%'))
			begin
				if(@edadSocio>=41 and @edadSocio<=50)
					set @ok=1
			end
			if (@edad LIKE ('%51-60%'))
			begin
				if(@edadSocio>=51 and @edadSocio<=60)
					set @ok=1
			end
			if (@edad LIKE ('%61%'))
			begin
				if(@edadSocio>=61 )
					set @ok=1
			end
			if(@ok=0)
				set @mulPuntos=-1
			end
		else
			set @mulPuntos=-1
	end
end

return @mulPuntos
END