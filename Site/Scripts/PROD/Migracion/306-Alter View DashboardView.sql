
GO

/****** Object:  View [dbo].[DashboardView]    Script Date: 18/01/2017 02:31:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[DashboardView]
AS

SELECT 
tr.IDTransaccion,
tr.FechaTransaccion, 
tr.Operacion,
/*CASE tr.Operacion
  WHEN 'Carga' THEN 'Venta'
  WHEN 'Descarga' THEN 'Anulacion'
  ELSE tr.Operacion
END as Operacion,*/
tr.ImporteAhorro,
tr.Importe, 
tr.Arancel,
CAST(
  (ISNULL(tr.Arancel,0) * ((ISNULL(Importe,0) - ISNULL(ImporteAhorro,0)) / 100)) 
  AS decimal(10,2)
) AS CalculoArancel,
CAST(
  (ISNULL(tr.Importe,0) - ISNULL(tr.ImporteAhorro,0) + ISNULL(tr.UsoRed * 1.21,0)) 
  AS decimal(10,2)
) AS CalculoTicket,
tr.Puntos,
CAST(
  (ISNULL(tr.Puntos,0) * ((ISNULL(Importe,0) - ISNULL(ImporteAhorro,0)) / 100)) 
  AS decimal(10,2)
) AS CalculoPuntos,
tr.UsoRed, 
tr.NumTarjetaCliente,
tr.PuntosAContabilizar,
c.IDComercio, 
t.IDMarca as TarjetaMarca,
C.IDMarca as ComercioMarca, 
t.IDFranquicia as TarjetaFranquicia,
C.IDFranquicia as ComercioFranquicia
FROM Transacciones tr
inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
inner join Comercios c on c.IDComercio = ter.IDComercio
inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
WHERE tr.Importe>1 and Arancel is not null and Puntos is not null
AND tr.Operacion not in ('Carga','Descarga')


GO


