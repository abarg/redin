
GO
/****** Object:  StoredProcedure [dbo].[GetReporteTerminales]    Script Date: 16/01/2017 01:37:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetReporteTerminales]
(
	@Estado varchar(50),
	@SDS varchar(50),
	@NombreEst varchar(100),
	@IDMarca int,
	@IDFranquicia int
)
AS

SELECT 
c.SDS,
c.NombreFantasia as 'Nombre',
d.Domicilio,
isnull(c.IDMarca,0) as 'IDMarca',
m.Nombre as 'NombreMarca',
isnull(f.IDFranquicia,0) as 'IDFranquicia', 
f.NombreFantasia as 'NombreFranquicia',
ci.Nombre as 'Localidad',
isnull(convert(varchar(10), c.FechaAlta, 103),'') as 'FechaCarga',
isnull(convert(varchar(10), c.FechaAltaDealer, 103),'') as 'FechaAlta',
di.Codigo as 'Dealer',
t.POSTipo as Tipo,
t.POSSistema as 'TipoTerminal',
t.POSTerminal as 'Terminal',
t.POSEstablecimiento as 'Establecimiento',
isnull(convert(varchar(10), t.POSFechaActivacion, 103),'') as 'FechaActivacion',
isnull(t.POSFechaReprogramacion,'') as 'FechaReprogramacion',
t.POSReprogramado as 'Reprogramado',
t.POSInvalido as 'ComercioInvalido',
t.Activo as 'Activo',
t.POSObservaciones  as 'Observaciones',
ISNULL((
SELECT isnull(count(IDTransaccion),0) 
FROM Transacciones tr WHERE t.POSTerminal = tr.NumTerminal and t.NumEst = tr.NumEst
AND tr.Importe>1 AND tr.Origen='Visa'
),0) as 'CantTR',
ISNULL((
SELECT top 1 ISNULL(Importe,0)
FROM Transacciones tr WHERE t.POSTerminal = tr.NumTerminal and t.NumEst = tr.NumEst
AND tr.Origen='Visa' and TipoTransaccion<>'000005' order by FechaTransaccion desc
),0) as 'UltimoImporte',
isnull(c.IDFranquicia,0) as 'Franquicia',
t.EstadoCanjes,
t.EstadoGift,
t.EstadoCompras
FROM Terminales t
JOIN Comercios c on c.IDComercio = t.IDComercio
LEFT JOIN Domicilios d ON c.IDDomicilio = d.IDDomicilio
LEFT JOIN Ciudades ci on ci.IDCiudad = d.Ciudad
LEFT JOIN Marcas m ON c.IDMarca = m.IDMarca
LEFT JOIN Franquicias f ON c.IDFranquicia = f.IDFranquicia
JOIN Dealer di on di.IDDealer = c.IDDealer
WHERE
(@SDS is null OR c.SDS like '%'+@SDS)
AND (@NombreEst is null OR  c.NombreEst like '%'+@NombreEst+'%')
AND (@IDMarca = 0 OR c.IDMarca = @IDMarca)
AND (@IDFranquicia = 0 OR c.IDFranquicia = @IDFranquicia)

