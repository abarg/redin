ALTER TABLE VerificacionesPOS
ADD  IDTerminal int null
FOREIGN KEY REFERENCES Terminales(IDTerminal)

go
--HAY QUE REALIZAR LA MIGRACION DE IDTERMINAL CON RESPECTO A CUANDO SE HAGA LA MIGRACION DE IDCOMERCIO AGRUPADOS
Update VerificacionesPOS set IDTerminal = (select top 1 t.IDTerminal from Terminales t where t.IDComercio = VerificacionesPOS.IDComercio)

go

--delete VerificacionesPOS where idterminal is null

go
ALTER TABLE VerificacionesPOS alter column  IDTerminal  int not null 

go

ALTER TABLE VerificacionesPOSComercios
ADD  IDTerminal int null
FOREIGN KEY REFERENCES Terminales(IDTerminal)

go

--HAY QUE REALIZAR LA MIGRACION DE IDTERMINAL CON RESPECTO A CUANDO SE HAGA LA MIGRACION DE IDCOMERCIO AGRUPADOS
Update VerificacionesPOSComercios set IDTerminal = (select top 1 t.IDTerminal from Terminales t where t.IDComercio = VerificacionesPOSComercios.IDComercio)

go


--delete VerificacionesPOSComercios where idterminal is null
go

ALTER TABLE VerificacionesPOSComercios alter column  IDTerminal  int not null 
