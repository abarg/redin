USE [PruebaRedin]
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_ImportePagadoPorDias]    Script Date: 27/01/2017 04:16:58 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Marcas_ImportePagadoPorDias]
(
  @IDMarca int,
  @FechaDesde datetime,
  @Dias int
)

AS

	declare @POSPropios bit
	SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca

	;WITH CTE AS
	(
		SELECT @FechaDesde as Fecha, 0 as Importe
		UNION ALL
		SELECT DATEADD(DAY,1,Fecha), 0 as Importe
		FROM CTE
		WHERE Fecha < @FechaDesde + @Dias
	)

	SELECT * 
	INTO #temp
	FROM CTE
	ORDER BY Fecha

	UPDATE #temp
	SET Importe = (

    SELECT  
    ISNULL(SUM(CASE tr.Operacion WHEN 'Venta' THEN (CAST(
  (ISNULL(tr.Importe,0) - ISNULL(tr.ImporteAhorro,0) + ISNULL(tr.UsoRed * 1.21,0)) 
  AS decimal(10,2)
)) ELSE (-1 * (CAST(
  (ISNULL(tr.Importe,0) - ISNULL(tr.ImporteAhorro,0) + ISNULL(tr.UsoRed * 1.21,0)) 
  AS decimal(10,2)
))) END),0)
    FROM Transacciones tr
    inner join Comercios c on c.IDComercio = tr.IDComercio
	WHERE tr.Importe>1 and Arancel is not null and Puntos is not null
	AND tr.Operacion not in ('Carga','Descarga')
	
	and tr.FechaTransaccion >= #temp.Fecha AND tr.FechaTransaccion < DATEADD(DAY,1,#temp.Fecha)
    AND tr.IDMarca = @IDMarca and ((@POSPropios=1 and c.IDMarca=@IDMarca) OR @POSPropios=0)
	)

	SELECT Fecha, ISNULL(Importe,0) as Importe FROM #temp
	ORDER BY Fecha