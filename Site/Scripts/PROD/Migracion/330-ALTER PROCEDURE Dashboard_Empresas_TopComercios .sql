
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Empresas_TopComercios]    Script Date: 27/01/2017 03:40:07 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Empresas_TopComercios]
(
	@IDEmpresa int
)

as
	
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr
		inner join Comercios c on  c.IDComercio = tr.IDComercio
		inner join EmpresasComercios e on e.IDComercio=c.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where e.IDEmpresa = @IDEmpresa and tr.Importe>1
		group by c.NombreFantasia, d.Domicilio, tr.Operacion
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
