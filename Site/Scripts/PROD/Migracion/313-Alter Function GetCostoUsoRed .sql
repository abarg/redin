GO
/****** Object:  UserDefinedFunction [dbo].[GetCostoUsoRed]    Script Date: 23/01/2017 03:11:22 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetCostoUsoRed] 
(
@NumEst VARCHAR (20), 
@TipoMensaje varchar(4),
@Operacion varchar(20)
)

RETURNS decimal(18,2)
WITH
EXEC AS CALLER

AS
   BEGIN
      DECLARE @costo   decimal(18,2)
      DECLARE @cobrar   bit
      
      SET @costo = 0.83
      
		if(@Operacion='Canje')
			select @costo = 0
		else
			BEGIN			

				IF(@TipoMensaje = '1100' or @TipoMensaje = '1420')
					SET @cobrar = (SELECT TOP 1 CobrarUsoRed FROM Terminales WHERE NumEst = @NumEst)
				ELSE
					SET @cobrar = (SELECT TOP 1 GiftcardCobrarUsoRed FROM Terminales WHERE NumEst = @NumEst)
			      
				IF(@cobrar = 0)
					SET @costo = 0
			END
     
      RETURN @costo
   END
   
