
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_TopComerciosMensual]    Script Date: 18/01/2017 02:37:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_TopComerciosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime)    
AS
BEGIN

SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst =tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where  tr.Importe>0  and tr.FechaTransaccion >= @FechaDesde and tr.FechaTransaccion <= @FechaHasta
		group by c.NombreFantasia, d.Domicilio, tr.Operacion
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc


END

