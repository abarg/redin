
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TerminalesActivos]    Script Date: 24/01/2017 01:19:21 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Franq_TerminalesActivos]
(
  @IDFranquicia int
)
AS
select distinct POSSistema + ' ('+cast(count(IDTerminal) as varchar(10))+')' as label, count(IDTerminal) as data 
from terminales ter
join comercios c on c.IDComercio = ter.idcomercio
where ter.Activo=1 and IDFranquicia=@IDFranquicia group by POSSistema 
union all
select distinct 'INACTIVOS' + ' ('+cast(count(IDTerminal) as varchar(10))+')' as label, count(IDTerminal) as data 
from terminales ter
join comercios c on c.IDComercio = ter.idcomercio
where ter.Activo=0 and IDFranquicia=@IDFranquicia