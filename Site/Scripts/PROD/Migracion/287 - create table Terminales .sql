Create table Terminales(
	IDTerminal int not null identity(1,1)  primary key ,
	IDComercio int not null,
	IDDomicilio int not null,
	FOREIGN KEY (IDComercio) REFERENCES Comercios(IDComercio),
	FOREIGN KEY (IDDomicilio) REFERENCES Domicilios(IDDomicilio),
	[POSTipo] [varchar](50) NULL,
	[POSSistema] [varchar](50) NULL,
	[POSTerminal] [varchar](50) NULL,
	[POSEstablecimiento] [varchar](50) NULL,
	[POSMarca] [varchar](50) NULL,
	[Estado] [char](2) NULL,				
	[TipoMov] [char](1) NULL,	
	[NroNumEst] [varchar](20) NULL,	
	[NumEstBenef] [varchar](20) NULL,	
	[NumEst] [varchar](20) NULL,
	[Descuento] [int] NOT NULL,				
	[DescuentoVip] [int] NULL,				
	[AffinityBenef] [varchar](4) NULL,				
	[Activo] [bit] NOT NULL,
	[DescuentoDescripcion] [varchar](500) NULL,				
	[DescuentoDescripcionVip] [varchar](500) NULL,				
	[POSObservaciones] [text] NULL,				
	[POSFechaActivacion] [smalldatetime] NULL,							
	[FidelyUsuario] [varchar](50) NULL,				
	[FidelyPwd] [varchar](50) NULL,				
	[FidelyClaveLicencia] [varchar](50) NULL,				
	[POSFechaReprogramacion] [datetime] NULL,				
	[POSReprogramado] [bit] NOT NULL,				
	[ModalidadEfectivo] [bit] NOT NULL,				
	[ModalidadDebito] [bit] NOT NULL,				
	[ModalidadCredito] [bit] NOT NULL,						
	[POSArancel] [decimal](10, 2) NULL,				
	[POSPuntos] [decimal](10, 2) NULL,				
	[POSInvalido] [bit] NOT NULL,
	[Descuento2] [int] NOT NULL,				
	[Descuento3] [int] NOT NULL,				
	[Descuento4] [int] NOT NULL,				
	[Descuento5] [int] NOT NULL,				
	[Descuento6] [int] NOT NULL,				
	[Descuento7] [int] NOT NULL,				
	[DescuentoVip2] [int] NULL,				
	[DescuentoVip3] [int] NULL,				
	[DescuentoVip4] [int] NULL,				
	[DescuentoVip5] [int] NULL,				
	[DescuentoVip6] [int] NULL,				
	[DescuentoVip7] [int] NULL,				
	[ModalidadEfectivoVip] [bit] NOT NULL,				
	[ModalidadDebitoVip] [bit] NOT NULL,				
	[ModalidadCreditoVip] [bit] NOT NULL,				
	[CobrarUsoRed] [bit] NOT NULL,
	[HabilitarPOSWeb] [bit] NOT NULL,				
	[CostoPOSWeb] [decimal](5, 2) NOT NULL,
	[HabilitarGifcard] [bit] NOT NULL,						
	[CostoGifcard] [decimal](5, 2) NOT NULL,				
	[GifcardArancelCarga] [decimal](10, 2) NULL,				
	[GifcardArancelDescarga] [decimal](10, 2) NULL,				
	[GiftcardGeneraPuntos] [bit] NOT NULL,				
	[GiftcardCobrarUsoRed] [bit] NOT NULL,				
	[CuponINArancel] [decimal](10, 2) NULL,				
	[CuponINCobrarUsoRed] [bit] NOT NULL,				
	[MultiplicaPuntos1] [int] NOT NULL,				
	[MultiplicaPuntos2] [int] NOT NULL,				
	[MultiplicaPuntos3] [int] NOT NULL,				
	[MultiplicaPuntos4] [int] NOT NULL,				
	[MultiplicaPuntos5] [int] NOT NULL,				
	[MultiplicaPuntos6] [int] NOT NULL,				
	[MultiplicaPuntos7] [int] NOT NULL,				
	[IDZona] [int] NULL,							
		
	[Puntos2] [decimal](10, 2) NULL,				
	[Puntos3] [decimal](10, 2) NULL,				
	[Puntos4] [decimal](10, 2) NULL,				
	[Puntos5] [decimal](10, 2) NULL,				
	[Puntos6] [decimal](10, 2) NULL,				
	[Puntos7] [decimal](10, 2) NULL,				
	[Arancel2] [decimal](10, 2) NULL,				
	[Arancel3] [decimal](10, 2) NULL,				
	[Arancel4] [decimal](10, 2) NULL,				
	[Arancel5] [decimal](10, 2) NULL,				
	[Arancel6] [decimal](10, 2) NULL,				
	[Arancel7] [decimal](10, 2) NULL,				
	[MultiplicaPuntosVip1] [int] NULL,				
	[MultiplicaPuntosVip2] [int] NULL,				
	[MultiplicaPuntosVip3] [int] NULL,				
	[MultiplicaPuntosVip4] [int] NULL,				
	[MultiplicaPuntosVip5] [int] NULL,				
	[MultiplicaPuntosVip6] [int] NULL,				
	[MultiplicaPuntosVip7] [int] NULL,				
	[PuntosVip1] [decimal](10, 2) NULL,				
	[PuntosVip2] [decimal](10, 2) NULL,				
	[PuntosVip3] [decimal](10, 2) NULL,				
	[PuntosVip4] [decimal](10, 2) NULL,				
	[PuntosVip5] [decimal](10, 2) NULL,				
	[PuntosVip6] [decimal](10, 2) NULL,				
	[PuntosVip7] [decimal](10, 2) NULL,	
	[EstadoCompras] [varchar](10) NULL,				
	[EstadoGift] [varchar](10) NULL,				
	[EstadoCanjes] [varchar](10) NULL,
	[CostoPOSPropio] [decimal](18, 0) NOT NULL,
	[ModeloPOS] [varchar](50) NULL,				
	[SimcardPOS] [varchar](50) NULL,				
	[EmpresaCelularPOS] [varchar](50) NULL,			
	FOREIGN KEY (IDZona) REFERENCES Zonas(IDZona)								
)



ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento2]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento3]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento4]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento5]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento6]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [Descuento7]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [ModalidadEfectivoVip]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [ModalidadDebitoVip]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [ModalidadCreditoVip]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((1)) FOR [CobrarUsoRed]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ('1') FOR [HabilitarPOSWeb]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [CostoPOSWeb]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [HabilitarGifcard]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [CostoGifcard]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [GifcardArancelCarga]
GO

ALTER TABLE [dbo].Terminales ADD  DEFAULT ((0)) FOR [GifcardArancelDescarga]
GO

--ALTER TABLE [dbo].[Comercios] ADD  DEFAULT ((0)) FOR [CostoPOSPropio]
GO