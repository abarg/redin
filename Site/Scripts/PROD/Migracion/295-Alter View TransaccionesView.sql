
GO

/****** Object:  View [dbo].[TransaccionesView]    Script Date: 17/01/2017 01:31:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[TransaccionesView]
AS


select --distinct
		tr.IDTransaccion,
		convert(varchar(10), tr.FechaTransaccion, 103) as Fecha,
		convert(varchar(10), tr.FechaTransaccion, 108) as Hora,
		tr.FechaTransaccion as FechaTransaccion,
		tr.Origen,
    tr.Operacion as Operacion,
    tr.NumTerminal as POSTerminal,
		c.IDComercio,
    c.SDS,
    c.NombreFantasia,
    c.RazonSocial,
		c.NombreEst as Empresa,
		ter.NumEst as NroEstablecimiento,
    c.NroDocumento as NroDocumento,
		t.Numero,
    t.IDSocio,
    (select Nombre from Socios where Socios.IDSocio = t.IDSocio) as Nombre,
		(select Apellido from Socios where Socios.IDSocio = t.IDSocio) as Apellido,
    (select NroDocumento from Socios where Socios.IDSocio = t.IDSocio) as NroDocumentoSocio,
		isnull(tr.Importe,0) as ImporteOriginal,
		isnull(tr.ImporteAhorro,0) as ImporteAhorro,
		tr.PuntosAContabilizar as PuntosAContabilizar,
		isnull(t.PuntosTotales,0) as PuntosTotales,
    isnull(t.Credito,0) as Credito,
    isnull(t.Giftcard,0) as Giftcard,
    t.IDMarca,
    m.Nombre as Marca,
    t.IDFranquicia,
    tr.UsoRed as CostoRedIn,
		(tr.Importe - tr.ImporteAhorro + tr.UsoRed) as Ticket,
		(tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)) as Arancel,
		(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos,
		(((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) as NetoGrabado
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio
		inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
    inner join Marcas m on t.IDMarca = m.IDMarca
		--left join Socios s on t.IDSocio = t.IDSocio




GO


