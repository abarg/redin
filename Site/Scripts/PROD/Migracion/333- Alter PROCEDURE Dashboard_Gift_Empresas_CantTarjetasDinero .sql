
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Gift_Empresas_CantTarjetasDinero]    Script Date: 27/01/2017 03:49:31 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Gift_Empresas_CantTarjetasDinero]
(
	@IDEmpresa int
)

AS

	SELECT '' as label, count(t.IDTarjeta) AS data
	FROM Tarjetas t
	inner join Marcas m on t.IDMarca=m.IDMarca
	inner join Comercios c on c.IDMarca=m.IDMarca
	inner join EmpresasComercios e on e.IDComercio=c.IDComercio
	WHERE Giftcard>0 and t.TipoTarjeta='G' and FechaBaja is null
	and e.IDEmpresa= @IDEmpresa
