
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Gift_Empresas_CantTarjetasStock]    Script Date: 27/01/2017 03:48:35 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Dashboard_Gift_Empresas_CantTarjetasStock]
(
	@IDEmpresa int
)

AS

	SELECT '' as label, count(t.IDTarjeta) AS data
	FROM Tarjetas t
	inner join Marcas m on t.IDMarca=m.IDMarca
	inner join Comercios c on c.IDMarca=m.IDMarca
	inner join EmpresasComercios e on e.IDComercio=c.IDComercio
	WHERE t.TipoTarjeta='G' and FechaBaja is null
	and e.IDEmpresa= @IDEmpresa

