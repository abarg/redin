
/****** Object:  View [dbo].[TransaccionesFranquiciasView]    Script Date: 17/01/2017 02:21:41 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER VIEW [dbo].[TransaccionesFranquiciasView]
AS
select --distinct
		tr.IDTransaccion,
		convert(varchar(10), tr.FechaTransaccion, 103) as Fecha,
		convert(varchar(10), tr.FechaTransaccion, 108) as Hora,
		tr.FechaTransaccion as FechaTransaccion,
		tr.Origen,
    tr.Operacion as Operacion,
    tr.NumTerminal as POSTerminal,
		c.IDComercio,
    c.SDS,
    c.NombreFantasia,
    c.RazonSocial,
    c.NombreEst as Empresa,
		ter.NumEst as NroEstablecimiento,
		t.Numero,
		--s.Nombre as Nombre,
		--s.Apellido as Apellido,
    t.IDSocio,
    (select Nombre from Socios where Socios.IDSocio = t.IDSocio) as Nombre,
		(select Apellido from Socios where Socios.IDSocio = t.IDSocio) as Apellido,
    (select NroDocumento from Socios where Socios.IDSocio = t.IDSocio) as NroDocumentoSocio,
		isnull(tr.Importe,0) as ImporteOriginal,
		isnull(tr.ImporteAhorro,0) as ImporteAhorro,
    isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0) as Arancel,
		tr.PuntosAContabilizar as Puntos,
		isnull(t.PuntosTotales,0) as PuntosTotales,
    isnull(t.Credito,0) as Credito,
    isnull(t.Giftcard,0) as Giftcard,
    t.IDFranquicia,
    t.IDMarca,
    m.Nombre as Marca,
	c.IDMarca as IDMarcaComercio,
    c.IDFranquicia as IDFranquiciaComercio,
    isnull(d.Domicilio,'') as DomicilioComercio,
    0 as Comision,
    0 as PubLocal,
    0 as PubNacional
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio
		inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
		inner join Marcas m on t.IDMarca = m.IDMarca
    left join Domicilios d on c.IDDomicilio = d.IDDomicilio and d.TipoDomicilio='C' and d.Entidad = 'C'
    where (t.IDFranquicia is not null or c.IDFranquicia is not null)





GO


