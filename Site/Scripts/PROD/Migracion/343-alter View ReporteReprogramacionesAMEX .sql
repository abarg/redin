
GO

/****** Object:  View [dbo].[ReporteReprogramacionesAMEX]    Script Date: 27/01/2017 05:19:06 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER view [dbo].[ReporteReprogramacionesAMEX] as

select
c.NombreFantasia,
c.RazonSocial as RazonSocial,
c.NroDocumento as CUIT,
d.Domicilio as Direcci�n,
ci.Nombre as Localidad,
d.CodigoPostal as CP,
co.Telefono as Contacto,
ter.POSTerminal as NroTerminal,
ter.NumEst as NumEst,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observaciones
from Comercios c
inner join Terminales ter on ter.IDComercio = c.IDComercio
inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
inner join Ciudades ci on d.Ciudad=ci.IDCiudad
inner join Contactos co on c.IDContacto=co.IDContacto
where c.Activo = 1 and ter.POSSistema = 'AMEX' and ter.POSTerminal is not null and ter.POSTerminal != '' and ter.Estado != 'DE'
and ter.POSReprogramado=0 and ter.POSFechaReprogramacion is null


GO


