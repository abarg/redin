/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_FacturacionComercios]    Script Date: 08/09/2015 17:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Dashboard_Franq_FacturacionComercios]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime, 
    @IDFranquicia int)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT ISNULL (SUM (ImporteAhorro +Importe),0)    as data FROM [DashboardView] where FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta and ComercioFranquicia=@IDFranquicia
END
