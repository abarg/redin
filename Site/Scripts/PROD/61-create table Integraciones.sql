USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[Integraciones]    Script Date: 29/10/2015 17:01:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Integraciones](
	[IDUsuario] [int] NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Pwd] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


