/****** Object:  StoredProcedure [dbo].[Dashboard_TotalCanjes]    Script Date: 18/01/2016 12:11:41 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Socios_TotalCanjes]
(

    @IDSocio int
)

AS

  SELECT  
  '' as label, 
  ISNULL(SUM(Importe),0) as data
  FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE Operacion='Canje'  and t.IDSocio=@IDSocio


GO


