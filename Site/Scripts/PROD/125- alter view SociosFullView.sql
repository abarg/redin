/****** Object:  View [dbo].[SociosFullView]    Script Date: 18/01/2016 11:38:18 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER VIEW [dbo].[SociosFullView]
AS
SELECT        
	isnull(Tarjetas.Numero,'') as 'NumeroTarjeta',
	isnull(Marcas.Nombre,'') as Marca, 
  isnull(Marcas.IDMarca,0) as IDMarca,
	Socios.*,
  isnull(Domicilios.Pais,'') as Pais,
  isnull(Provincias.Nombre,'') as Provincia,
  isnull(Ciudades.Nombre,'') as Ciudad,
  isnull(Domicilios.Domicilio,'') as Domicilio,
  isnull(Domicilios.PisoDepto,'') as PisoDepto,
  isnull(Domicilios.CodigoPostal,'') as CodigoPostal,
  isnull(Domicilios.Telefono,'') as TelefonoDom,
  isnull(Domicilios.Fax,'') as Fax,
  --isnull(Tarjetas.PuntosDisponibles,0) as PuntosDisponibles,
  isnull(Tarjetas.PuntosTotales,0) as PuntosTotales,
  isnull(Tarjetas.Credito,0) as Credito,
  isnull(Tarjetas.Giftcard,0) as Giftcard,
  Tarjetas.IDFranquicia,
  isnull(Franquicias.NombreFantasia,'') as Franquicia
FROM Socios 
LEFT JOIN Tarjetas ON Socios.IDSocio = Tarjetas.IDSocio 
LEFT JOIN Marcas ON Tarjetas.IDMarca = Marcas.IDMarca
LEFT JOIN Domicilios ON Socios.IDDomicilio = Domicilios.IDDomicilio
LEFT JOIN Provincias ON Domicilios.Provincia = Provincias.IDProvincia
LEFT JOIN Ciudades ON Ciudades.IDCiudad = Domicilios.Ciudad
LEFT JOIN Franquicias ON Tarjetas.IDFranquicia = Franquicias.IDFranquicia




GO


