alter table productos
alter column Nombre varchar(300)

alter table SociosTmp
add  Puntos int null

alter table SociosTmp
add  TarjAux varchar(50)

go

create PROCEDURE [dbo].[tmpActualizarProductos] 

AS
BEGIN
	
	DECLARE @Contador int;
	SET @Contador = 1;  
	DECLARE @Regs INT;
	SET @Regs = (SELECT MAX(IDPremio) FROM PremiosTmp);
	
	--Campos PremiosTmp
	DECLARE @IDPremio int
	DECLARE @Codigo varchar(10)
	DECLARE @ValorPuntos varchar(50)
	DECLARE @FechaVigenciaDesde datetime 
	DECLARE @FechaVigenciaHasta datetime 
	DECLARE @Descripcion varchar(20) 
	DECLARE @TipoMov char(1) 
	
	DECLARE @StockActual int
	DECLARE @ValorPesos int
	DECLARE @IDMarca int
	DECLARE @IDRubro int
	DECLARE @StockMinimo int

	update PremiosTmp
	set IDRubro=(select top 1 IDFamiliaProducto from FamiliaProductos where idpadre is null and FamiliaProductos.IDMarca= PremiosTmp.IDMarca)

	--Actualizo los premios actuales
	UPDATE Productos 
	SET 
	[Codigo] = tmp.Codigo,
	[Puntos] = tmp.ValorPuntos,
	[FechaAlta] = tmp.FechaVigenciaDesde,
	[Descripcion] = tmp.Descripcion,
	[StockActual]=tmp.StockActual,
	[Precio]=tmp.ValorPesos,
	[IDMarca] = tmp.IDMarca,
	[IDFamilia]= tmp.IDRubro,
	[StockMinimo]=tmp.StockMinimo
	FROM PremiosTmp tmp 
	WHERE 
	tmp.IDMarca is not null and
	tmp.Codigo = Productos.Codigo and Productos.IDMarca = tmp.IDMarca
	
  
	--Inserto los nuevos
	INSERT INTO Productos
	SELECT tmp.IDMarca, tmp.Codigo,
	tmp.ValorPesos,
	tmp.Descripcion,
	'',
	tmp.FechaVigenciaDesde,
	1,
	tmp.IDRubro,
	tmp.StockActual,
	tmp.StockMinimo,
	null,
	tmp.Foto,
	tmp.ValorPuntos
	FROM PremiosTmp tmp
	WHERE tmp.IDMarca is not null and 
	tmp.Codigo not in (select distinct Codigo from Productos where Productos.IDMarca = tmp.IDMarca)

END