USE [RedIN-QA]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDescuento]    Script Date: 21/03/2016 02:43:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









ALTER FUNCTION [dbo].[GetDescuento] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @Operacion varchar(20),
	@Fecha datetime,
	@TmpDescuento int,
	@NumTarjetaCliente varchar(16)
	
)

RETURNS int
AS

BEGIN

	DECLARE @sumaPuntos bit
	DECLARE @dia int
	SET @dia = (select datepart(dw,@Fecha))
	DECLARE @descuento int 


   SELECT @descuento = 0
	if(@Operacion='Canje')
		select @descuento = 0
	else
		BEGIN
			DECLARE @AffinityMarca char(4)
			DECLARE @AffinityTarjeta char(4)
			declare @IDMarca int

			set @IDMarca= (select IDMarca from Comercios where NumEst= @NumEst and POSTerminal=@NumTerminal)
			set @AffinityMarca=(select affinity from Marcas where IDMarca=@IDMarca)

			set @AffinityTarjeta=(select SUBSTRING(@NumTarjetaCliente, 7,4))
			IF(@AffinityTarjeta=@AffinityMarca) /*Descuento VIP*/
			BEGIN
				SET @descuento = ISNULL((select TOP 1 p.DescuentoVip from PromocionesPuntuales p 
						inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
						inner join Comercios c on pc.IDComercio = c.IDComercio where
						NumEst = @NumEst and POSTerminal=@NumTerminal and CAST(FechaDesde AS DATE) <= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
				IF (@descuento =-1)/*descuento vip sin promocion*/
				BEGIN 
					IF(@dia = 1)
						SET @descuento = (SELECT TOP 1 DescuentoVip from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 2)
						SET @descuento = (SELECT TOP 1 DescuentoVip2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 3)
						SET @descuento = (SELECT TOP 1 DescuentoVip3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 4)
						SET @descuento = (SELECT TOP 1 DescuentoVip4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 5)
						SET @descuento = (SELECT TOP 1 DescuentoVip5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 6)
						SET @descuento = (SELECT TOP 1 DescuentoVip6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
						SET @descuento = (SELECT TOP 1 DescuentoVip7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				END
			END
			ELSE 
			BEGIN /*descuento no vip*/
				SET @descuento = ISNULL((select TOP 1 p.Descuento from PromocionesPuntuales p 
				inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
						inner join Comercios c on pc.IDComercio = c.IDComercio where
						NumEst = @NumEst and POSTerminal=@NumTerminal and CAST(FechaDesde AS DATE) <= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
				IF (@descuento =-1)/*descuento no vip sin promocion*/
				BEGIN 
					IF(@dia = 1)
						SET @descuento = (SELECT TOP 1 Descuento from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 2)
						SET @descuento = (SELECT TOP 1 Descuento2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 3)
						SET @descuento = (SELECT TOP 1 Descuento3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 4)
						SET @descuento = (SELECT TOP 1 Descuento4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 5)
						SET @descuento = (SELECT TOP 1 Descuento5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 6)
						SET @descuento = (SELECT TOP 1 Descuento6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
						SET @descuento = (SELECT TOP 1 Descuento7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
				END
			END 

			if(@descuento=0)
				SET @descuento = @TmpDescuento
	END
	return @descuento
end



