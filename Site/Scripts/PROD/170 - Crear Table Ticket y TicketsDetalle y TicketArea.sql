USE [RedIN-QA]
GO

CREATE TABLE dbo.TicketsArea
	(
	IDAreaTicket int Identity(1,1) NOT NULL PRIMARY KEY,
	Nombre varchar(50) NOT NULL ,
	Activa bit NOT NULL,
	Email varchar(100) NOT NULL,
	) 

	GO

CREATE TABLE dbo.Tickets
	(
	IDTicket int Identity(1,1) NOT NULL PRIMARY KEY,
	Estado varchar(50) NOT NULL,
	IDArea int NULL,
	FechaAlta  datetime NOT NULL,
	FOREIGN KEY (IDArea) REFERENCES TicketsArea(IDAreaTicket)
	) 

	GO

CREATE TABLE dbo.TicketsDetalle
	(
	IDDetalleTicket int Identity(1,1) NOT NULL PRIMARY KEY,
	IDTicket int NOT NULL ,
	TipoUsuario tinyint NOT NULL,
	IDUsuario int NOT NULL,
	Descripcion text NULL,
	Adjunto varchar(50) NULL,
	Fecha datetime NULL
    FOREIGN KEY (IDTicket) REFERENCES Tickets(IDTicket)
	) 

	GO