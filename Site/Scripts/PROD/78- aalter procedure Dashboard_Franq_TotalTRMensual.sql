USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TotalTRMensual]    Script Date: 13/11/2015 16:22:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Franq_TotalTRMensual]
(
  @IDFranquicia int,
  @FechaDesde datetime,
  @FechaHasta datetime
)

AS
	
	SELECT '' as label, count(IDTransaccion) as data
	FROM DashboardView
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	AND (TarjetaFranquicia = @IDFranquicia or ComercioFranquicia=@IDFranquicia)
GO


