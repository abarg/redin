
Create table MarcasAsociadas (
IDMarcaAsociada int Primary Key identity(1,1),
IDMultimarca int not null foreign key references Multimarcas (IDMultimarca),
IDMarca int not null foreign key references Marcas (IDMarca)
)
