USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[IntegracionesTokens]    Script Date: 09/11/2015 17:07:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[IntegracionesTokens](
	[IDIntegracionesTokens] [int] IDENTITY(1,1) NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[SessionID] [varchar](50) NOT NULL,
	[FechaValidez] [datetime] NOT NULL,
 CONSTRAINT [PK_IntegracionesTokens] PRIMARY KEY CLUSTERED 
(
	[IDIntegracionesTokens] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[IntegracionesTokens]  WITH CHECK ADD  CONSTRAINT [FK_IntegracionesTokens_Integraciones1] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Integraciones] ([IDUsuario])
GO

ALTER TABLE [dbo].[IntegracionesTokens] CHECK CONSTRAINT [FK_IntegracionesTokens_Integraciones1]
GO


