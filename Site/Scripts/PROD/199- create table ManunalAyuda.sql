USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[ManualAyuda]    Script Date: 15/06/2016 01:19:01 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ManualAyuda](
	[IDManualAyuda] [int] IDENTITY(1,1) NOT NULL,
	[Perfil] [varchar](50) NOT NULL,
	[Titulo] [varchar](100) NOT NULL,
	[Orden] [int] NOT NULL,
 CONSTRAINT [PK_ManualAyuda] PRIMARY KEY CLUSTERED 
(
	[IDManualAyuda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


