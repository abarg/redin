alter table canjes drop column Estado

alter table canjes alter column [IDProducto] int not null
go
update canjes set [IDEstado] = 1 where [IDEstado] is null
go
alter table canjes alter column [IDEstado] int not null