USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[Liquidaciones]    Script Date: 03/03/2016 01:00:53 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Liquidaciones](
	[IDLiquidacion] [int] IDENTITY(1,1) NOT NULL,
	[IDFranquicia] [int] NOT NULL,
	[FechaGeneracion] [datetime] NOT NULL,
	[FechaDesde] [datetime] NOT NULL,
	[FechaHasta] [datetime] NOT NULL,
	[Estado] [varchar](50) NOT NULL,
	[ComprobanteTransferencia] [varchar](100) NULL,
	[Factura] [varchar](100) NULL,
 CONSTRAINT [PK_Liquidaciones] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Liquidaciones]  WITH CHECK ADD  CONSTRAINT [FK_Liquidaciones_Franquicias] FOREIGN KEY([IDFranquicia])
REFERENCES [dbo].[Franquicias] ([IDFranquicia])
GO

ALTER TABLE [dbo].[Liquidaciones] CHECK CONSTRAINT [FK_Liquidaciones_Franquicias]
GO


