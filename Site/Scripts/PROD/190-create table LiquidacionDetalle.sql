USE [RedIN-QA]
GO
/****** Object:  Table [dbo].[LiquidacionDetalle]    Script Date: 02/06/2016 12:19:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiquidacionDetalle](
	[IDLiquidacionDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IDLiquidacion] [int] NOT NULL,
	[Tipo] [varchar](50) NULL,
	[SubTotal] [decimal](10, 2) NULL,
 CONSTRAINT [PK_LiquidacionDetalle] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacionDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LiquidacionDetalleTipo]    Script Date: 02/06/2016 12:19:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiquidacionDetalleTipo](
	[IDLiquidacionDetalleTipo] [int] IDENTITY(1,1) NOT NULL,
	[IDLiquidacionDetalle] [int] NOT NULL,
	[IDComercio] [int] NULL,
	[ImporteTotalOriginal] [decimal](10, 2) NULL,
	[ImporteTotal] [decimal](10, 2) NULL,
	[SubConcepto] [varchar](50) NULL,
 CONSTRAINT [PK_LiquidacionDetalleTipo] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacionDetalleTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Liquidaciones]    Script Date: 02/06/2016 12:19:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Liquidaciones](
	[IDLiquidacion] [int] IDENTITY(1,1) NOT NULL,
	[IDFranquicia] [int] NOT NULL,
	[FechaGeneracion] [datetime] NOT NULL,
	[FechaDesde] [datetime] NOT NULL,
	[FechaHasta] [datetime] NOT NULL,
	[Estado] [varchar](50) NOT NULL,
	[ComprobanteTransferencia] [varchar](100) NULL,
	[Factura] [varchar](100) NULL,
 CONSTRAINT [PK_Liquidaciones] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LiquidacionesObservaciones]    Script Date: 02/06/2016 12:19:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiquidacionesObservaciones](
	[IDLiquidacionesObservaciones] [int] IDENTITY(1,1) NOT NULL,
	[IDLiquidacionDetalle] [int] NOT NULL,
	[Observacion] [text] NOT NULL,
	[FechaGeneracion] [datetime] NOT NULL,
	[Origen] [varchar](50) NOT NULL,
 CONSTRAINT [PK_LiquidacionesObservaciones] PRIMARY KEY CLUSTERED 
(
	[IDLiquidacionesObservaciones] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[LiquidacionDetalle]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionDetalle_Liquidaciones] FOREIGN KEY([IDLiquidacion])
REFERENCES [dbo].[Liquidaciones] ([IDLiquidacion])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LiquidacionDetalle] CHECK CONSTRAINT [FK_LiquidacionDetalle_Liquidaciones]
GO
ALTER TABLE [dbo].[LiquidacionDetalleTipo]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionDetalleTipo_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO
ALTER TABLE [dbo].[LiquidacionDetalleTipo] CHECK CONSTRAINT [FK_LiquidacionDetalleTipo_Comercios]
GO
ALTER TABLE [dbo].[LiquidacionDetalleTipo]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionDetalleTipo_LiquidacionDetalle] FOREIGN KEY([IDLiquidacionDetalle])
REFERENCES [dbo].[LiquidacionDetalle] ([IDLiquidacionDetalle])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LiquidacionDetalleTipo] CHECK CONSTRAINT [FK_LiquidacionDetalleTipo_LiquidacionDetalle]
GO
ALTER TABLE [dbo].[Liquidaciones]  WITH CHECK ADD  CONSTRAINT [FK_Liquidaciones_Franquicias] FOREIGN KEY([IDFranquicia])
REFERENCES [dbo].[Franquicias] ([IDFranquicia])
GO
ALTER TABLE [dbo].[Liquidaciones] CHECK CONSTRAINT [FK_Liquidaciones_Franquicias]
GO
ALTER TABLE [dbo].[LiquidacionesObservaciones]  WITH CHECK ADD  CONSTRAINT [FK_LiquidacionesObservaciones_LiquidacionDetalle] FOREIGN KEY([IDLiquidacionDetalle])
REFERENCES [dbo].[LiquidacionDetalle] ([IDLiquidacionDetalle])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LiquidacionesObservaciones] CHECK CONSTRAINT [FK_LiquidacionesObservaciones_LiquidacionDetalle]
GO
