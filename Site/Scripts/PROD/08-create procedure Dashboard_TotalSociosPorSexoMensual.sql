

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Dashboard_TotalSociosPorSexoMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
	count(IDSocio) as data, 'M' as label 
	from Socios 
	where sexo='M'and FechaAlta >= @FechaDesde AND FechaAlta <= @FechaHasta
	union all
	select 
	count(IDSocio) as data, 'F' as label 
	from Socios 
	where sexo='F'and FechaAlta >= @FechaDesde AND FechaAlta <= @FechaHasta
	union all
	select 
	count(IDSocio) as data, 'I' as label 
	from Socios 
	where sexo='I' or sexo is null and FechaAlta >= @FechaDesde AND FechaAlta <= @FechaHasta

END
GO
