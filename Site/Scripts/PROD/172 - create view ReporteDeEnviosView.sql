USE [RedIN-QA]
GO

/****** Object:  View [dbo].[FacturasRecurrentesView]    Script Date: 15/04/2016 04:38:59 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter View [dbo].[ReporteDeEnviosView] as
  
SELECT 
f.PeriodoDesde,f.PeriodoHasta,
  (select  count(Enviada)
  FROM [RedIN-QA].[dbo].[Facturas] f1
  where modo='R'  and
   Enviada = 1 and
   f1.PeriodoDesde = f.PeriodoDesde and 
   f1.PeriodoHasta = f.PeriodoHasta) as Enviadas,

  (select  count(Enviada)
  FROM [RedIN-QA].[dbo].[Facturas] f2
  where modo='R'  and
   Enviada = 0 and
   f2.PeriodoDesde = f.PeriodoDesde and 
   f2.PeriodoHasta = f.PeriodoHasta) as NoEnviadas,

   count(FechaRecibida) as Leidas

  FROM [RedIN-QA].[dbo].[Facturas] f
  where modo='R'
  group by f.PeriodoDesde,f.PeriodoHasta



GO


