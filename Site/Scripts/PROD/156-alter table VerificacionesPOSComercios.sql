delete VerificacionesPOSComercios

alter table VerificacionesPOSComercios add Fecha datetime not null
alter table VerificacionesPOSComercios add IDFranquicia int not null


ALTER TABLE [dbo].[VerificacionesPOSComercios]  WITH CHECK ADD  CONSTRAINT [FK_VerificacionesPOSComercios_Franquicias] FOREIGN KEY([IDFranquicia])
REFERENCES [dbo].[Franquicias] ([IDFranquicia])
GO

ALTER TABLE [dbo].[VerificacionesPOSComercios] CHECK CONSTRAINT [FK_VerificacionesPOSComercios_Franquicias]
GO


