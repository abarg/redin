USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Generar_Liquidacion]    Script Date: 02/06/2016 12:17:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Generar_Liquidacion]
     @FechaGeneracion datetime,
     @FechaDesde datetime,
     @FechaHasta datetime,
     @IDFranquicia int
AS
BEGIN
  insert into Liquidaciones (IDFranquicia,FechaGeneracion,FechaDesde,FechaHasta,Estado)values (@IDFranquicia,@FechaGeneracion,@FechaDesde,@FechaHasta,'CasaMatriz')
  declare @idLiquidacion int 
  set @idLiquidacion =(SELECT SCOPE_IDENTITY())





  CREATE TABLE #Result
(
  IDComercio int, 
  Total decimal (10,2)
)
INSERT #Result EXEC Rpt_GetComisionesTotales @fechaDesde,@fechaHasta,@IDFranquicia

  insert into LiquidacionDetalle
  (
   IDLiquidacion,
   Tipo
  ) 
  values (@idLiquidacion, 'TRANSACCIONES')
  declare @idLiquidacionDetalle int 
  set @idLiquidacionDetalle =(SELECT SCOPE_IDENTITY())


  insert into LiquidacionDetalleTipo
  (
  IDLiquidacionDetalle,
  IDComercio,
  ImporteTotalOriginal
  ) 
  select distinct 
   @idLiquidacionDetalle,IDComercio,sum (Total) from #Result group by IDComercio


   
  update LiquidacionDetalle set SubTotal =(select sum (isnull(ImporteTotalOriginal,0))from 
  LiquidacionDetalleTipo where IDLiquidacionDetalle= @idLiquidacionDetalle) 
  where IDLiquidacionDetalle= @idLiquidacionDetalle

DROP TABLE #Result
END



