USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Comercios_TopSociosMensual]    Script Date: 30/12/2015 11:49:34 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_Comercios_TopSociosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDComercio int) 
AS
BEGIN

		select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		inner join Comercios c on  c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		where t.IDSocio is not null and tr.Importe>1
		AND c.IDComercio = @IDComercio and tr.FechaTransaccion >= @FechaDesde and tr.FechaTransaccion <= @FechaHasta
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	
	/*
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
	*/
/*
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesView t
		inner join tarjetas tr on t.Numero = tr.Numero 
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND t.IDComercio = @IDComercio and t.FechaTransaccion >= @FechaDesde and t.FechaTransaccion <= @FechaHasta
		group by t.Numero, t.Nombre, t.Apellido, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
	*/
END

GO


