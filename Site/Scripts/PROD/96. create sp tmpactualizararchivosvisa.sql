USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[tmpActualizarPremios]    Script Date: 28/12/2015 05:39:31 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[tmpActualizarArchivosVisa] 

AS
BEGIN
	
	TRUNCATE TABLE TmpArchivosVisa

	INSERT INTO TmpArchivosVisa
	SELECT
	t.IDTarjeta, t.IDSocio, s.IDSocioPadre, t.Credito, t.Giftcard, t.IDMarca, t.FechaBaja, m.TipoCatalogo
	FROM Tarjetas t
	INNER JOIN Marcas m ON t.IDMarca = m.IDMarca
	LEFT JOIN Socios s ON t.IDSocio =s.IDSocio
	--WHERE t1.FechaBaja is null

	--Actualizo los que tienen un socio padre a 0
	UPDATE TmpArchivosVisa
	SET 
	Credito=0,
	Giftcard=0
	WHERE IDSocioPadre IS NOT NULL AND IDSocioPadre>0

	--select * from TmpArchivosVisa WHERE IDSocioPadre IS NOT NULL AND IDSocioPadre>0

	--Actualizo los puntos de aquellas tarjetas que tienen grupo familiar
	UPDATE TmpArchivosVisa
	SET 
	Credito = (
		(SELECT ISNULL(SUM(Credito),0)
		FROM Tarjetas t2
		INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
		INNER JOIN Socios s2 on t2.IDSocio=s2.IDSocio
		WHERE (s2.IDSocioPadre = TmpArchivosVisa.IDSocio) --or t2.IDSocio = TmpArchivosVisa.IDSocio) 
		AND m.TipoCatalogo='A' and t2.FechaBaja is null and t2.TipoTarjeta='B'
		)
		+
		(SELECT ISNULL(SUM(Credito),0)
		FROM Tarjetas t2
		INNER JOIN Marcas m on t2.IDMarca=m.IDMarca
		INNER JOIN Socios s2 on t2.IDSocio=s2.IDSocio
		WHERE (t2.IDSocio = TmpArchivosVisa.IDSocio) 
		AND m.TipoCatalogo='A' and t2.FechaBaja is null and t2.TipoTarjeta='B')
	
	)
	WHERE IDSocio IS NOT NULL AND (IDSocioPadre IS NULL OR IDSocioPadre=0)

	--Pongo en 0 las tarjetas que tienen catalogo privado
	UPDATE TmpArchivosVisa
	SET	
	Credito=0,
	FechaBaja = getdate()
	WHERE TipoCatalogo<>'A'



END