GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_TotalTarjetasActivas]    Script Date: 29/12/2015 12:16:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Marcas_TotalTarjetasActivas]
(
  @IDMarca int
)

AS
	  declare @activas int
  declare @total int
  
  SELECT @total = count(IDTarjeta) from Tarjetas WHERE IDMarca = @IDMarca 
  AND TipoTarjeta='B' AND FechaBaja is null
  
  SELECT @activas = ( 
	  SELECT count(DISTINCT NumTarjetaCliente)
	  from  Transacciones tr
	  INNER JOIN Tarjetas t on tr.NumTarjetaCliente=t.Numero
	  WHERE t.IDMarca = @IDMarca AND t.FechaBaja is null and tr.Importe>1 and Arancel is not null and Puntos is not null
	  AND tr.Operacion ='VENTA'
  )

  /*SELECT @activas = count(*) from (
	select distinct NumTarjetaCliente
	FROM DashboardView
  WHERE TarjetaMarca = @IDMarca 
	group by NumTarjetaCliente) as T
  */


  select 
  @activas as data, 'A' as label
  union all
  select 
  @total as data, 'T' as label
