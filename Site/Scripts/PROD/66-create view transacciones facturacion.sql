create VIEW [dbo].[TransaccionesFacturacionView]
AS


select --distinct
		tr.IDTransaccion,
		convert(varchar(10), tr.FechaTransaccion, 103) as Fecha,
		convert(varchar(10), tr.FechaTransaccion, 108) as Hora,
		tr.FechaTransaccion as FechaTransaccion,
		tr.Origen,
    tr.Operacion as Operacion,
    tr.NumTerminal as POSTerminal,
		c.IDComercio,
    c.SDS,
    c.NombreFantasia,
    c.RazonSocial,
		c.NombreEst as Empresa,
		c.NumEst as NroEstablecimiento,
    c.NroDocumento as NroDocumento,
		tr.NumTarjetaCliente as Numero,
  --  t.IDSocio,
  --  (select Nombre from Socios where Socios.IDSocio = t.IDSocio) as Nombre,
		--(select Apellido from Socios where Socios.IDSocio = t.IDSocio) as Apellido,
  --  (select NroDocumento from Socios where Socios.IDSocio = t.IDSocio) as NroDocumentoSocio,
		isnull(tr.Importe,0) as ImporteOriginal,
		isnull(tr.ImporteAhorro,0) as ImporteAhorro,
		tr.PuntosAContabilizar as PuntosAContabilizar,
		0 as PuntosTotales,
    0 as Credito,
    0 as Giftcard,
    tr.IDMarca,
    '' as Marca,
    tr.IDFranquicia,
    tr.UsoRed as CostoRedIn,
		(tr.Importe - tr.ImporteAhorro + tr.UsoRed) as Ticket,
		(tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)) as Arancel,
		(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos
		--(((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) as NetoGrabado
		from Transacciones tr
		inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		--inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
    --inner join Marcas m on t.IDMarca = m.IDMarca
		--left join Socios s on t.IDSocio = t.IDSocio
GO