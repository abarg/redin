USE [RedIN-QA]
GO

/****** Object:  View [dbo].[ReporteVerificacionesPOSView]    Script Date: 03/03/2016 01:01:48 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReporteVerificacionesPOSView]
AS
  select distinct (v.NroReferencia),f.NombreFantasia as Franquicia,f.IDFranquicia,(select count (*) from VerificacionesPOSComercios where NroReferencia=v.NroReferencia)as CantidadComercios,CAST (v.Fecha AS DATE) as Fecha,
 (select top 1(ve.NroReferencia) from VerificacionesPOS as ve where ve.NroReferencia=v.NroReferencia ) as Estado, p.Nombre as Provincia ,ci.Nombre as Ciudad, 
 z.Nombre as Zona from VerificacionesPOSComercios v 
 inner join Comercios c on c.IDComercio = v.IDComercio
 inner join Franquicias f on c.IDFranquicia = f.IDFranquicia
 inner join Domicilios d on d.IDDomicilio = c.IDDomicilio
 inner join Provincias p on p.IDProvincia = d.Provincia
 left join Ciudades ci on ci.IDCiudad= d.Ciudad
 inner join Zonas z on c.IDZona = z.IDZona
 group by v.NroReferencia, f.NombreFantasia, F.IDFranquicia,v.Fecha,  p.Nombre, ci.Nombre,z.Nombre
	

	


GO


