/****** Object:  Table [dbo].[VerificacionesPOSComercios]    Script Date: 08/10/2015 16:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[VerificacionesPOSComercios](
	[IDVerificacionesPOSComercio] [int] IDENTITY(1,1) NOT NULL,
	[IDComercio] [int] NOT NULL,
	[NroReferencia] [varchar](50) NOT NULL,
 CONSTRAINT [PK__Verifica__733348A3E808AF8A] PRIMARY KEY CLUSTERED 
(
	[IDVerificacionesPOSComercio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [VerificacionesPOSComercios]  WITH CHECK ADD  CONSTRAINT [FK_VerificacionesPOSComercios_Comercios] FOREIGN KEY([IDComercio])
REFERENCES [Comercios] ([IDComercio])
GO

ALTER TABLE [VerificacionesPOSComercios] CHECK CONSTRAINT [FK_VerificacionesPOSComercios_Comercios]
GO


