USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[GetReporteTerminales]    Script Date: 01/06/2016 03:30:35 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetReporteTerminales]
(
	@Estado varchar(50),
	@SDS varchar(50),
	@NombreEst varchar(100),
	@IDMarca int,
	@IDFranquicia int
)
AS

SELECT 
c.SDS,
c.NombreFantasia as 'Nombre',
d.Domicilio,
isnull(c.IDMarca,0) as 'IDMarca',
m.Nombre as 'NombreMarca',
isnull(f.IDFranquicia,0) as 'IDFranquicia', 
f.NombreFantasia as 'NombreFranquicia',
ci.Nombre as 'Localidad',
isnull(convert(varchar(10), c.FechaAlta, 103),'') as 'FechaCarga',
isnull(convert(varchar(10), c.FechaAltaDealer, 103),'') as 'FechaAlta',
c.CodigoDealer as 'Dealer',
c.POSTipo as Tipo,
c.POSSistema as 'TipoTerminal',
c.POSTerminal as 'Terminal',
c.POSEstablecimiento as 'Establecimiento',
isnull(convert(varchar(10), c.POSFechaActivacion, 103),'') as 'FechaActivacion',
isnull(c.POSFechaReprogramacion,'') as 'FechaReprogramacion',
c.POSReprogramado as 'Reprogramado',
c.POSInvalido as 'ComercioInvalido',
c.Activo as 'Activo',
c.POSObservaciones  as 'Observaciones',
ISNULL((
SELECT isnull(count(IDTransaccion),0) 
FROM Transacciones tr WHERE c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
AND tr.Importe>1 AND tr.Origen='Visa'
),0) as 'CantTR',
ISNULL((
SELECT top 1 ISNULL(Importe,0)
FROM Transacciones tr WHERE c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
AND tr.Origen='Visa' and TipoTransaccion<>'000005' order by FechaTransaccion desc
),0) as 'UltimoImporte',
isnull(c.IDFranquicia,0) as 'Franquicia',
c.EstadoCanjes,
c.EstadoGift,
c.EstadoCompras
FROM Comercios c
LEFT JOIN Domicilios d ON c.IDDomicilio = d.IDDomicilio
LEFT JOIN Ciudades ci on ci.IDCiudad = d.Ciudad
LEFT JOIN Marcas m ON c.IDMarca = m.IDMarca
LEFT JOIN Franquicias f ON c.IDFranquicia = f.IDFranquicia
WHERE
(@SDS is null OR c.SDS like '%'+@SDS)
AND (@NombreEst is null OR  c.NombreEst like '%'+@NombreEst+'%')
AND (@IDMarca = 0 OR c.IDMarca = @IDMarca)
AND (@IDFranquicia = 0 OR c.IDFranquicia = @IDFranquicia)

