USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_TotalTarjetasActivasMensuales]    Script Date: 04/01/2016 09:22:25 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_TotalTarjetasActivasMensuales]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime)   
AS
BEGIN
  declare @activas int
  declare @total int
  

   SELECT @total = count(IDTarjeta) from Tarjetas where   TipoTarjeta='B' AND FechaBaja is null

  SELECT @activas =  COUNT(distinct NumTarjetaCliente)FROM Transacciones   
  WHERE Importe>1 and Arancel is not null and Puntos is not null
  AND Operacion not in ('Carga','Descarga') and  FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
 

  select 
  @activas as data, 'A' as label
  union all
  select 
  @total as data, 'T' as label

  /*
  SELECT @total = count(IDTarjeta) from Tarjetas
  
  SELECT @activas = ISNULL(SUM(cantidad),0) from (
  select sum(1) as cantidad
  FROM DashboardView INNER JOIN Tarjetas on DashboardView.NumTarjetaCliente = Tarjetas.Numero where Tarjetas.FechaAlta >= @FechaDesde AND Tarjetas.FechaAlta <= @FechaHasta 
    group by NumTarjetaCliente) as T
  
  
  select 
  @activas as data, 'A' as label
  union all
  select 
  @total as data, 'T' as label


  */
END


GO


