
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Dashboard_Franq_TopComerciosPorMes]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDFranquicia int)    
AS
BEGIN
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct 
			c.NombreFantasia as uno, 
			d.Domicilio as dos,
			'' as tres,
			t.Operacion,
			isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesFranquiciasView t
		inner join Comercios c on t.IDComercio=c.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where t.Empresa is not null and t.ImporteOriginal>1
		AND c.IDFranquicia = @IDFranquicia AND t.FechaTransaccion>= @FechaDesde AND t.FechaTransaccion <= @FechaHasta 
		group by c.NombreFantasia, t.Operacion, t.SDS, t.NroEstablecimiento,d.Domicilio
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
END
GO
