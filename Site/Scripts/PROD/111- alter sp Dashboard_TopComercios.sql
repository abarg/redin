USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_TopComercios]    Script Date: 30/12/2015 10:46:15 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_TopComercios]
WITH EXEC AS CALLER
AS

SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr
		inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where tr.Importe>0
		group by c.NombreFantasia, d.Domicilio, tr.Operacion
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc

/*
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesView t
		inner join Comercios c on t.IDComercio=c.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where t.Empresa is not null and t.ImporteOriginal>0
		group by c.NombreFantasia, t.SDS, t.NroEstablecimiento,d.Domicilio, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
	*/
GO


