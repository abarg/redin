USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Socios_SaldoActual]    Script Date: 15/02/2016 09:13:26 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Dashboard_Socios_PuntosActuales]
(
	@idSocio int
)

AS

  SELECT  
  '' as label, 
  SUM(PuntosTotales) as data
  FROM Tarjetas
  WHERE IDSocio=@idSocio and FechaBaja is null


GO


