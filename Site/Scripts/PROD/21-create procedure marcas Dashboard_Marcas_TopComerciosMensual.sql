
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Dashboard_Marcas_TopComerciosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDMarca int)    
AS
BEGIN
declare @POSPropios bit
	SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca

	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct c.NombreFantasia as uno, 
		d.Domicilio as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesMarcasView t
		inner join Comercios c on t.IDComercio=c.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where t.Empresa is not null and t.ImporteOriginal>1
		AND t.IDMarca = @IDMarca and ((@POSPropios=1 and C.IDMarca=@IDMarca) OR @POSPropios=0) AND t.FechaTransaccion >= @FechaDesde AND t.FechaTransaccion <= @FechaHasta
		group by c.NombreFantasia, t.SDS, t.NroEstablecimiento,d.Domicilio, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc

END
GO
