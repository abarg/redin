USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[ComentariosPorArticulo]    Script Date: 15/06/2016 01:27:42 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ComentariosPorArticulo](
	[IDComentariosPorSubTitulo] [int] IDENTITY(1,1) NOT NULL,
	[IDArticuloManualAyuda] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Comentario] [text] NULL,
 CONSTRAINT [PK_ComentariosPorArticulo] PRIMARY KEY CLUSTERED 
(
	[IDComentariosPorSubTitulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ComentariosPorArticulo]  WITH CHECK ADD  CONSTRAINT [FK_ComentariosPorArticulo_ArticulosManualAyuda] FOREIGN KEY([IDArticuloManualAyuda])
REFERENCES [dbo].[ArticulosManualAyuda] ([IDArticulosManualAyuda])
GO

ALTER TABLE [dbo].[ComentariosPorArticulo] CHECK CONSTRAINT [FK_ComentariosPorArticulo_ArticulosManualAyuda]
GO

ALTER TABLE [dbo].[ComentariosPorArticulo]  WITH CHECK ADD  CONSTRAINT [FK_ComentariosPorArticulo_Usuarios] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuarios] ([IDUsuario])
GO

ALTER TABLE [dbo].[ComentariosPorArticulo] CHECK CONSTRAINT [FK_ComentariosPorArticulo_Usuarios]
GO


