/****** Object:  StoredProcedure [dbo].[Dashboard_TotalFacturadoPlusINMensual]    Script Date: 19/01/2016 09:36:28 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_TotalFacturadoPlusINMensual]
(
	@FechaDesde datetime,
  @FechaHasta datetime
)

AS
  declare @suma decimal(10,2)
  declare @resta decimal(10,2)
  
  SELECT  
  '' as label, 
  ISNULL(SUM(ImporteTotal),0) as data from Facturas f
  inner join Socios s on s.IDSocio=f.IDSocio
  where s.NroCuentaPlusIN is not null and FechaCAE >= @FechaDesde AND FechaCAE <= @FechaHasta

GO


