USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[Beneficios]    Script Date: 08/11/2016 02:06:34 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Beneficios](
	[IDBeneficio] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NULL,
	[IDComercio] [int] NULL,
	[IDMarca] [int] NULL,
	[FechaDesde] [datetime] NULL,
	[FechaHasta] [datetime] NULL,
	[Sexo] [char](1) NULL,
	[Edad] [varchar](50) NULL,
	[MultiplicaPuntos] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDBeneficio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Beneficios]  WITH CHECK ADD FOREIGN KEY([IDComercio])
REFERENCES [dbo].[Comercios] ([IDComercio])
GO

ALTER TABLE [dbo].[Beneficios]  WITH CHECK ADD FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresas] ([IDEmpresa])
GO

ALTER TABLE [dbo].[Beneficios]  WITH CHECK ADD FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marcas] ([IDMarca])
GO


