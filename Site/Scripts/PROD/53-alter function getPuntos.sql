GO
/****** Object:  UserDefinedFunction [dbo].[GetPuntos]    Script Date: 19/10/2015 14:09:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetPuntos] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16),
    @TipoMensaje varchar(4),
    @Operacion varchar(20),
    @TipoTransaccion varchar(6),
	@Fecha datetime
)

RETURNS int
AS

BEGIN

DECLARE @puntos int
DECLARE @sumaPuntos bit
DECLARE @dia int
SET @dia = (select datepart(dw,@Fecha)-1)
/*declare @marcaTarjeta int
declare @marcaComercio int

select @marcaTarjeta = IDMarca FROM Tarjetas where Numero = @NumTarjetaCliente
select @marcaComercio = IDMarca FROM Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst

if(@marcaTarjeta = @marcaComercio)
  select @puntos = POSPuntos from Marcas where IDMarca = @marcaTarjeta
else*/

if(@Operacion='Canje')
    select @puntos = 0
else
    BEGIN
        if(@TipoMensaje='1420' and @TipoTransaccion='000005')--Anulacion
            select @puntos = 1
        ELSE
            BEGIN
                IF(@TipoMensaje = '1100' or @TipoMensaje = '1420')
				 BEGIN
					IF(@dia = 1)
						SET @puntos = (SELECT TOP 1 POSPuntos from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 2)
						SET @puntos = (SELECT TOP 1 Puntos2 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 3)
						SET @puntos = (SELECT TOP 1 Puntos3 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 4)
						SET @puntos = (SELECT TOP 1 Puntos4 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 5)
						SET @puntos = (SELECT TOP 1 Puntos5 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 6)
						SET @puntos = (SELECT TOP 1 Puntos6 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
						SET @puntos = (SELECT TOP 1 Puntos7 from Comercios where POSTerminal = @NumTerminal and NumEst = @NumEst)
					END
                ELSE
                    BEGIN
                        SET @sumaPuntos = (SELECT TOP 1 GiftcardGeneraPuntos FROM Comercios WHERE NumEst = @NumEst)
                        IF(@sumaPuntos=1)
                            SET @puntos = 1
                        else
                            SET @puntos = 0

                    END
            END
    end




if(@puntos=null)
  set @puntos=0

 return @puntos

END
