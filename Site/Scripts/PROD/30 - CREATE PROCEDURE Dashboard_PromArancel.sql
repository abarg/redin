CREATE  PROCEDURE [dbo].[Dashboard_PromArancel] (@FechaDesde datetime,
  @FechaHasta datetime)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
   SELECT '' as label,ROUND(AVG(CAST(Arancel AS FLOAT)), 2)/100  as data FROM [DashboardView]where FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta 
END

GO


