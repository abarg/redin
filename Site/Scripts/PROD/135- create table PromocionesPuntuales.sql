/****** Object:  Table [dbo].[PromocionesPuntuales]    Script Date: 19/01/2016 12:25:30 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromocionesPuntuales](
	[IDPromocionesPuntuales] [int] IDENTITY(1,1) NOT NULL,
	[FechaDesde] [datetime] NOT NULL,
	[FechaHasta] [datetime] NOT NULL,
	[Puntos] [decimal](10, 2) NULL,
	[MultiplicaPuntos] [int] NOT NULL,
	[Arancel] [decimal](10, 2) NULL,
	[Descuento] [int] NOT NULL,
	[PuntosVip] [decimal](10, 2) NULL,
	[MultiplicaPuntosVip] [int] NULL,
 CONSTRAINT [PK_PromocionesPuntuales] PRIMARY KEY CLUSTERED 
(
	[IDPromocionesPuntuales] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


