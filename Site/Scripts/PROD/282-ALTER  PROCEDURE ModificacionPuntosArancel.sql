
GO
/****** Object:  StoredProcedure [dbo].[ModificacionPuntosArancel]    Script Date: 19/12/2016 10:23:57 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER  PROCEDURE [dbo].[ModificacionPuntosArancel] (@FechaDesde datetime,
  @FechaHasta datetime, 
  @IDMarca int,
  @NumEst varchar (20),
  @NumTerminal varchar (10),
  @Operacion varchar (20),
  @Origen varchar (20),
  @Puntos decimal (10,2),
  @PuntosVip decimal (10,2),
  @Arancel decimal (10,2),
  @ArancelVip decimal (10,2),
  @CostoRed decimal (10,2)
  )                  
AS
BEGIN
	
	
 CREATE TABLE #Result
(
  IDTransaccion int, 
  NumTarjetaCliente varchar(128),
  Puntos decimal(10,2),
  Arancel decimal (10,2)
)
INSERT #Result select IDTransaccion, NumTarjetaCliente, @PuntosVip, @ArancelVip  from 
Transacciones where FechaTransaccion >=@FechaDesde and FechaTransaccion<=@FechaHasta
and (@IDMarca=0 or IDMarca=@IDMarca) and 
(@Operacion='' or Operacion=@Operacion) and (@Origen='' or Origen=@Origen) and (@NumEst='' or NumEst =@NumEst) and 
(@NumTerminal='' or NumTerminal=@NumTerminal) and (dbo.EsVip(case when @IDMarca>0 then @IDMarca else IDMarca end,@NumEst, @NumTerminal, NumTarjetaCliente)=1)

INSERT #Result select IDTransaccion, NumTarjetaCliente, @Puntos, @Arancel from 
Transacciones where FechaTransaccion >=@FechaDesde and FechaTransaccion<=@FechaHasta
and (@IDMarca=0 or IDMarca=@IDMarca) and 
(@Operacion='' or Operacion=@Operacion) and (@Origen='' or Origen=@Origen) and (@NumEst='' or NumEst =@NumEst) and 
(@NumTerminal='' or NumTerminal=@NumTerminal) and (dbo.EsVip(case when @IDMarca>0 then @IDMarca else IDMarca end,@NumEst, @NumTerminal, NumTarjetaCliente)=0)


---30/11/2016 tickets #165
/*
select * from #Result
update Transacciones  set Arancel=@arancel, Puntos=r.Puntos,PuntosAContabilizar=r.Puntos, UsoRed=@CostoRed from #Result r where Transacciones.IDTransaccion in (r.IDTransaccion)
END*/


select * from #Result
update Transacciones  set Arancel=r.Arancel, Puntos=r.Puntos,PuntosAContabilizar=r.Puntos, UsoRed=@CostoRed from #Result r where Transacciones.IDTransaccion in (r.IDTransaccion)
END


