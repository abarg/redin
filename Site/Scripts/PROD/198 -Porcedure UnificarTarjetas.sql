Create procedure unificarTarjetas @idSocio int, @NroDocumento varchar(50)
as

begin tran
	begin try
	update tarjetas set IDSocio=@idSocio where idsocio in (select idsocio from socios where NroDocumento = @NroDocumento)

	delete socios where  NroDocumento = @NroDocumento and idsocio <> @idSocio

	commit tran
	end try


	begin catch
	rollback tran
	end catch