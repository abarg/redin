/****** Object:  StoredProcedure [dbo].[Rpt_Franq_FacturacionDetalle]    Script Date: 05/10/2016 02:27:15 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Rpt_Franq_FacturacionDetalle]
(
	@fechaDesde datetime,
  @fechaHasta datetime,
  @IDFranquicia int
)
AS
BEGIN
      
    SELECT 
    POSTipo, POSSistema, POSTerminal, Operacion, FechaTransaccion, Origen,
  	SDS, NombreFantasia, NroDocumento, NumEst, Marca, ISNULL(IDMarca,0) AS IDMarca, Tarjeta, ISNULL(Socio,'') AS Socio,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImporteOriginal WHEN 'Carga' THEN ImporteOriginal ELSE (ImporteOriginal*-1) END AS ImporteOriginal,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImporteAhorro WHEN 'Carga' THEN ImporteAhorro ELSE (ImporteAhorro*-1) END AS ImporteAhorro,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ImportePagado WHEN 'Carga' THEN ImportePagado ELSE (ImportePagado*-1) END AS ImportePagado,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN CostoRedIn WHEN 'Carga' THEN CostoRedIn ELSE (CostoRedIn*-1) END AS CostoRedIn,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN Ticket WHEN 'Carga' THEN Ticket ELSE (Ticket*-1) END AS Ticket,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ComisionTtCp WHEN 'Carga' THEN ComisionTtCp ELSE (ComisionTtCp*-1) END AS ComisionTtCp,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ComisionTpCp WHEN 'Carga' THEN ComisionTpCp ELSE (ComisionTpCp*-1) END AS ComisionTpCp,
    CASE Operacion WHEN 'CuponIN' THEN ImporteOriginal WHEN 'Venta' THEN ComisionTpCt WHEN 'Carga' THEN ComisionTpCt ELSE (ComisionTpCt*-1) END AS ComisionTpCt
    --SUM(CantOperaciones) AS CantOperaciones
    --INTO #TMPFACTURACIONDET
    FROM
    (
      SELECT 
        tr.FechaTransaccion,
    		tr.Origen,
        c.POSTipo,
    		c.POSSistema,
    	  c.POSTerminal,
    		tr.Operacion,
    		c.SDS,
    		c.NombreFantasia,
        t.Numero as Tarjeta,
        c.NroDocumento,
    		c.NumEst,
    		--d.Ciudad as Localidad,
        ISNULL(m.Nombre,'') as Marca,
        m.IDMarca,
        (select Apellido+', '+ Nombre from Socios where Socios.IDSocio = t.IDSocio) as Socio,
    		ISNULL(tr.Importe,0) as ImporteOriginal,
    		ISNULL(tr.ImporteAhorro,0) AS ImporteAhorro,
    		ISNULL(tr.Importe - tr.ImporteAhorro,0) as ImportePagado,
    		ISNULL(tr.UsoRed,0) as CostoRedIn,
    		--CAST(
          ISNULL(tr.Importe - tr.ImporteAhorro + (tr.UsoRed * 1.21),0)-- AS decimal(10,2)
        --) 
        AS Ticket,
      ISNULL(dbo.getComisionFranquicia('TtCp', @IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTtCp,
      ISNULL(dbo.getComisionFranquicia('TpCp', @IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTpCp,
      ISNULL(dbo.getComisionFranquicia('TpCt', @IDFranquicia, c.IDFranquicia, tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)),0) as ComisionTpCt  
      FROM Comercios c
  		INNER JOIN Transacciones tr on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  		LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		--AND c.IDFranquicia=@IDFranquicia
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN')
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia = @IDFranquicia
      )
     
    ) AS T
    
    
    /*GROUP BY 
      Origen, FechaTransaccion, Operacion, ImporteOriginal, ImporteAhorro, ImportePagado, Puntos, Arancel,CostoRedIn, Ticket
      NroDocumento, POSSistema, POSTipo, SDS, NombreFantasia, POSSistema, NumEst, POSTerminal, 
      IDMarca,Marca, Socio*/
    
    /*
    SELECT 
    '' AS Operacion, GETDATE() AS FechaTransaccion, '' AS Origen,
    '' AS POSTipo,'' AS  POSSistema,'' AS  POSTerminal,
    	'' AS SDS,'' AS  NombreFantasia,'' AS  NroDocumento, '' AS NumEst, 
      '' AS Marca, 
      '' AS Tarjeta, '' AS Socio,
      0 IDMarca,
      1.5 AS ImporteOriginal,
      1.5 AS ImporteAhorro,
      1.5 AS ImportePagado,
      1.5 AS CostoRedIn,
      1.5 AS Ticket,
      1.5 AS ComisionTtCp,
      1.5 AS ComisionTpCp,
      1.5 AS ComisionTpCt*/
END
