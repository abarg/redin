USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_Empresas_TopSocios]    Script Date: 30/12/2015 11:50:15 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Empresas_TopSocios]
(
	@IDEmpresa int
)
AS
	select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
		inner join EmpresasComercios e on e.IDComercio = c.IDComercio
		where t.IDSocio is not null and tr.Importe>1
		AND e.IDEmpresa = @IDEmpresa
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	order by cuatro desc

	/*
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc*/


/*
	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesEmpresasView t
		inner join tarjetas tr on t.Numero = tr.Numero
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND t.IDEmpresa = @IDEmpresa
		group by t.Numero, t.Nombre, t.Apellido, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
	*/
GO


