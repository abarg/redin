USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_TarjetasEmitidas_Listado]    Script Date: 28/10/2016 03:56:58 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_TarjetasEmitidas_Listado](
	 @numDesde int,
	 @numHasta int
)
AS
select distinct
m.Nombre as Marca,
count(t.IDMarca) as Total,
(
  select isnull(SUM(cantidad),0) from (
  select 1 as cantidad
  FROM Transacciones tr
  inner join Comercios c on c.POSTerminal = tr.NumTerminal and c.NumEst = tr.NumEst
  inner join Tarjetas tt on tr.NumTarjetaCliente = tt.Numero
  WHERE tr.Importe>1 and tt.IDMarca= m.IDMarca and (@numDesde=0 or cast(RIGHT (tt.Numero,5) as int)>=@numDesde) and (@numHasta=0 or cast(RIGHT (tt.Numero,5) as int)<=@numHasta)
  group by NumTarjetaCliente) as R
) as Activas,
(MIN(cast(RIGHT (t.Numero,5) as int))) as Desde,
(MAX(cast(RIGHT (t.Numero,5) as int)))  as Hasta
from Marcas m
inner join Tarjetas t on t.IDMarca=m.IDMarca
where (@numDesde=0 or cast(RIGHT (t.Numero,5) as int)>=@numDesde) and (@numHasta=0 or cast(RIGHT (t.Numero,5) as int)<=@numHasta)
group by m.Nombre,m.IDMarca
order by Activas desc
