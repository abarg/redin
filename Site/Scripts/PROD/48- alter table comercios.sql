
alter table Comercios add Puntos2 int 
alter table Comercios add Puntos3 int 
alter table Comercios add Puntos4 int 
alter table Comercios add Puntos5 int 
alter table Comercios add Puntos6 int
alter table Comercios add Puntos7 int

alter table Comercios add Arancel2 int 
alter table Comercios add Arancel3 int 
alter table Comercios add Arancel4 int 
alter table Comercios add Arancel5 int 
alter table Comercios add Arancel6 int
alter table Comercios add Arancel7 int
 
 go

UPDATE Comercios set Puntos2=POSPuntos
UPDATE Comercios set Puntos3=POSPuntos
UPDATE Comercios set Puntos4=POSPuntos
UPDATE Comercios set Puntos5=POSPuntos
UPDATE Comercios set Puntos6=POSPuntos
UPDATE Comercios set Puntos7=POSPuntos

  
UPDATE Comercios set Arancel2 = POSArancel
UPDATE Comercios set Arancel3   =	   POSArancel
UPDATE Comercios set Arancel4   =	   POSArancel
UPDATE Comercios set Arancel5   =	   POSArancel
UPDATE Comercios set Arancel6   =	   POSArancel
UPDATE Comercios set Arancel7   =	   POSArancel

go


alter table Comercios alter column Puntos2 int not null
alter table Comercios alter column Puntos3 int  not null
alter table Comercios alter column Puntos4 int  not null
alter table Comercios alter column Puntos5 int  not null
alter table Comercios alter column Puntos6 int not null
alter table Comercios alter column Puntos7 int not null

alter table Comercios alter column Arancel2 int  not null
alter table Comercios alter column Arancel3 int  not null
alter table Comercios alter column Arancel4 int  not null
alter table Comercios alter column Arancel5 int  not null
alter table Comercios alter column Arancel6 int not null
alter table Comercios alter column Arancel7 int not null