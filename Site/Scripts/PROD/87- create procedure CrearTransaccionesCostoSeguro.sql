USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[CrearTransaccionesCostoSeguro]    Script Date: 16/12/2015 12:46:46 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CrearTransaccionesCostoSeguro]
(
	@NumEst varchar (20),
	@NumTerminal varchar (10),
	@FechaTransaccion datetime
)
AS
BEGIN

	--1-vacio la tabla tmp
	truncate table TmpCostoSeguro

	--2 lleno la tabla
	INSERT INTO TmpCostoSeguro
	SELECT Numero, s.CostoSeguro as CostoSeguroSocio, m.CostoSeguro as CostoSeguroMarca, null as CostoFinal
	from Tarjetas t 
	inner join  Marcas m on t.IDMarca=m.IDMarca 
	left join Socios s on t.IDSocio=s.IDSocio 
	where t.FechaBaja is null and((s.CostoSeguro is not null and s.CostoSeguro>0) or (m.CostoSeguro is not null and m.CostoSeguro>0))
	order by t.Numero

	--3 evaluo qu� se debe cobrar

	UPDATE TmpCostoSeguro
	SET CostoFinal = CASE WHEN CostoSeguroSocio > 0 THEN CostoSeguroSocio ELSE CostoSeguroMarca END

	--4 creo las TR

	insert into Transacciones  (
				[NumTarjetaCliente]
				,[Importe]
				,[PuntosAContabilizar]
				,[NumEst]
				,[NumTerminal]
				,[TipoMensaje]
				,[TipoTransaccion]
				,[FechaTransaccion]
				,[NumCupon]
				,[NumReferencia]
				,[NumRefOriginal]
				,[CodigoPremio]
				,[PuntosDisponibles]
				,[Origen]
				,[Operacion]
				,[Descripcion]
				,[ImporteAhorro]
				,[Descuento], UsoRed, Arancel, Puntos, Usuario)
	select  NumeroTarjeta as NumTarjetaCliente, CostoFinal as Importe, CAST (((CostoFinal * 100)*-1) as int) as PuntosAContabilizar,
			@NumEst,@NumTerminal,'1100','000005',@FechaTransaccion,'','','','',
			'000000000000','Web','Canje', 'COSTO SEGURO MENSUAL', 0, 
			0,0,0, 1,'proceso_costoseguro' 
			from TmpCostoSeguro where CostoFinal>0

	/*UPDATE Transacciones
	SET PuntosAContabilizar= CAST (Importe*-1 AS int)
	WHERE Operacion='Canje' and FechaTransaccion=@FechaTransaccion and usuario='proceso_costoseguro' and NumEst=@NumEst and NumTerminal=@NumTerminal*/
		
	UPDATE Transacciones
	SET PuntosIngresados= CAST (Importe AS varchar(12))
	WHERE Operacion='Canje' and FechaTransaccion=@FechaTransaccion and usuario='proceso_costoseguro' and NumEst=@NumEst and NumTerminal=@NumTerminal

	UPDATE Transacciones
	SET IDMarca= dbo.GetMarcaTarjeta(NumTarjetaCliente) WHERE FechaTransaccion=@FechaTransaccion and usuario='proceso_costoseguro' and NumEst=@NumEst and NumTerminal=@NumTerminal
    
	UPDATE Transacciones
	SET IDFranquicia= dbo.GetFranquiciaTarjeta(NumTarjetaCliente) WHERE FechaTransaccion=@FechaTransaccion and usuario='proceso_costoseguro' and NumEst=@NumEst and NumTerminal=@NumTerminal


	--5 actualizar puntos por tarjeta
	UPDATE Tarjetas
	SET PuntosTotales = (
	SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
	),
	Credito = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
		FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
	),
	Giftcard = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
	--SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
	FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
	)
	WHERE Tarjetas.Numero IN (SELECT DISTINCT Numero FROM TmpCostoSeguro)

	SELECT COUNT (*) FROM TmpCostoSeguro where CostoFinal>0

END


GO


