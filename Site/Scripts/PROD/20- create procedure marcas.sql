
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE [Dashboard_Marcas_TopSociosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDMarca int)    
AS
BEGIN
 declare @POSPropios bit
	SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca

	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct t.Nombre + ', '+ t.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		isnull(CASE t.Operacion WHEN 'Venta' THEN SUM(t.ImporteOriginal) WHEN 'Carga' THEN SUM(t.ImporteOriginal) ELSE SUM(t.ImporteOriginal*-1) END,0) AS cuatro
		from TransaccionesMarcasView t
		inner join tarjetas tr on t.Numero = tr.Numero
		inner join Marcas m on tr.IDMarca=m.IDMarca
		where t.IDSocio is not null and t.ImporteOriginal>1
		AND t.IDMarca = @IDMarca  and ((@POSPropios=1 and t.IDMarca=@IDMarca) OR @POSPropios=0)  and t.FechaTransaccion >= @FechaDesde and t.FechaTransaccion <= @FechaHasta
		group by t.Numero, t.Nombre, t.Apellido, t.Operacion	
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
END
GO
