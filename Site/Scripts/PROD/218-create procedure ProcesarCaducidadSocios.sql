USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[ProcesarCaducidadSocios]    Script Date: 05/09/2016 05:24:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[ProcesarCaducidadSocios](
@NumEst varchar (20),
	@NumTerminal varchar (10),
	@FechaTransaccion datetime
)
AS
BEGIN
insert into Transacciones  (
				[NumTarjetaCliente]
				,[Importe]
				,[PuntosAContabilizar]
				,[NumEst]
				,[NumTerminal]
				,[TipoMensaje]
				,[TipoTransaccion]
				,[FechaTransaccion]
				,[NumCupon]
				,[NumReferencia]
				,[NumRefOriginal]
				,[CodigoPremio]
				,[PuntosDisponibles]
				,[Origen]
				,[Operacion]
				,[Descripcion]
				,[ImporteAhorro]
				,[Descuento], UsoRed, Arancel, Puntos, Usuario)
	select NumTarjetaCliente, sum(PuntosAContabilizar), CAST (((sum(PuntosAContabilizar) * 100)*-1) as int) as PuntosAContabilizar,
			@NumEst,@NumTerminal,'1100','000005',@fechaTransaccion,'','','','',
			'000000000000','Web','Canje', 'CADUCIDAD', 0, 
			0,0,0, 1,'proceso_caducidad' 
			from Transacciones tr
			inner join Tarjetas t on t.Numero = tr.NumTarjetaCliente
			inner join Socios s on s.IDSocio= t.IDSocio
			where s.IDSocio in (select  IDSocio from  Socios s
			where FechaCaducidad = cast( getdate() as date)) 
			and tr.FechaTransaccion< s.fechaTopeCanje
			and TipoTarjeta<>'G'
			group by NumTarjetaCliente

				
	UPDATE Transacciones
	SET PuntosIngresados= CAST (Importe AS varchar(12))
	WHERE Operacion='Canje' and FechaTransaccion=@FechaTransaccion and usuario='proceso_caducidad' and NumEst=@NumEst and NumTerminal=@NumTerminal

	UPDATE Transacciones
	SET IDFranquicia= dbo.GetFranquiciaTarjeta(NumTarjetaCliente) WHERE FechaTransaccion=@FechaTransaccion and usuario='proceso_caducidad' and NumEst=@NumEst and NumTerminal=@NumTerminal


	--5 actualizar puntos por tarjeta
	UPDATE Tarjetas
	SET PuntosTotales = (
	SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
	),
	Credito = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
		FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
	),
	Giftcard = (
	SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
	--SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
	FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
	)
	select count(*)	from Transacciones tr
			inner join Tarjetas t on t.Numero = tr.NumTarjetaCliente
			inner join Socios s on s.IDSocio= t.IDSocio
			where s.IDSocio in (select  IDSocio from  Socios s
			where FechaCaducidad = cast( getdate() as date)) 
			and tr.FechaTransaccion< s.fechaTopeCanje
			group by NumTarjetaCliente

	


end