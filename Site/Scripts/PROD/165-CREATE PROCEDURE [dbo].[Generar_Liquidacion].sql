USE [RedIN-QA]
GO

/****** Object:  StoredProcedure [dbo].[Generar_Liquidacion]    Script Date: 03/03/2016 01:02:31 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Generar_Liquidacion]
     @FechaGeneracion datetime,
     @FechaDesde datetime,
     @FechaHasta datetime,
     @IDFranquicia int
AS
BEGIN
  insert into Liquidaciones (IDFranquicia,FechaGeneracion,FechaDesde,FechaHasta,Estado)values (@IDFranquicia,@FechaGeneracion,@FechaDesde,@FechaHasta,'Paso2')
  declare @idLiquidacion int 
  set @idLiquidacion =(SELECT SCOPE_IDENTITY())

  CREATE TABLE #Result
(
  IDComercio int, 
  Total decimal (10,2)
)
INSERT #Result EXEC Rpt_GetComisionesTotales @fechaDesde,@fechaHasta,@IDFranquicia

  insert into LiquidacionDetalle
  (
 
  IDComercio,
  ImporteTotalOriginal,
   IDLiquidacion
  ) 
  select distinct 
   IDComercio,sum (Total), @idLiquidacion from #Result group by IDComercio

DROP TABLE #Result
END



GO


