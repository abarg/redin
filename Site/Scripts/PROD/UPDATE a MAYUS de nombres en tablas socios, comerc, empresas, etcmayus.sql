
--begin tran
update [dbo].[Socios]
set 
Nombre = UPPER(Nombre),
Apellido = UPPER(Apellido),
[Banco] = UPPER([Banco])

go

update Comercios
set
[NombreFantasia] = UPPER([NombreFantasia]),
[NombreEst] = UPPER([NombreFantasia]),
[RazonSocial]  = UPPER([RazonSocial]),
[Responsable]  = UPPER([Responsable]),
[Cargo] = UPPER([Cargo]),
[Actividad] = UPPER([Actividad]),
[FormaPago_Banco] =UPPER([FormaPago_Banco]),
[FormaPago_Tarjeta] = UPPER([FormaPago_Tarjeta]),
[FormaPago_BancoEmisor] = UPPER([FormaPago_BancoEmisor])


update comercios set [CodigoDealer] = '' where [CodigoDealer]= 'undefined'
update comercios set [DescuentoDescripcion] = '' where [DescuentoDescripcion]= 'undefined'
update comercios set [DescuentoDescripcionVip] = '' where [DescuentoDescripcionVip]= 'undefined'



go

update Franquicias
set
[NombreFantasia] = UPPER([NombreFantasia]),
[RazonSocial] = UPPER([RazonSocial]),
[FormaPago] = UPPER([FormaPago]),
[FormaPago_Banco] = UPPER([FormaPago_Banco]),
[FormaPago_TipoCuenta] = UPPER([FormaPago_TipoCuenta]),
[FormaPago_Tarjeta] = UPPER([FormaPago_Tarjeta]),
[FormaPago_BancoEmisor] = UPPER([FormaPago_BancoEmisor])

go

update Contactos
set
Nombre = UPPER(Nombre),
Apellido = UPPER(Apellido),
[Cargo] = UPPER([Cargo])

go

update Empresas
set
[Nombre] = UPPER([Nombre])


go
update Marcas
set
[Nombre] = UPPER([Nombre]),
[RazonSocial] = UPPER([RazonSocial])


go

update rubros
set [Nombre] = UPPER([Nombre])

go
update [dbo].[Ciudades]
set [Nombre] = UPPER([Nombre])


go

update [dbo].[Provincias]
set [Nombre] = UPPER([Nombre])
go

update [dbo].[Domicilios]
set [Pais] = UPPER([Pais]),
Domicilio = UPPER(Domicilio)
go

update [dbo].[Zonas]
set [Nombre] = UPPER([Nombre])


go 

update [dbo].[Dealer]
set [Nombre] = UPPER([Nombre]),
[Apellido] = UPPER([Apellido]),
Codigo = UPPER(Codigo)

go



--rollback tran
--commit tran

--select * from socios
--select * from Comercios
--select * from Franquicias
--select * from Empresas
--select * from Marcas
--select * from rubros
--select * from Ciudades
--select * from provincias
--select * from Dealer