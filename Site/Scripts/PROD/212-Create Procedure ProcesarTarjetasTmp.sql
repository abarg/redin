USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[ProcesarSociosTmp]    Script Date: 20/07/2016 05:49:39 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ProcesarTarjetasTmp]
( 
	@idMarca int
)

AS
UPDATE TarjetasTmp set Error=null,IDMarca = @idMarca

UPDATE TarjetasTmp
SET Error='Tarjeta inexistente'
where NumeroTarjeta not in (select Numero from Tarjetas where IDMarca = @idMarca)
and Error is null

UPDATE TarjetasTmp
SET Error='La tarjeta ya estaba activada'
where NumeroTarjeta in (select Numero from Tarjetas where IDMarca = @idMarca and Estado = 'A')
and Error is null

UPDATE TARJETAS
SET Estado = 'A',MotivoBaja= NULL,FechaBaja = NULL,UsuarioBaja = NULL,FechaAlta = GETDATE(),FechaVencimiento='2049-12-31'
 where Numero in (select NumeroTarjeta from TarjetasTmp tmp where tmp.Error is null and IDMarca = @idMarca) 



