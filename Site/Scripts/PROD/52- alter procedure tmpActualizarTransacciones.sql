GO
/****** Object:  StoredProcedure [dbo].[tmpActualizarTransacciones]    Script Date: 19/10/2015 14:43:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[tmpActualizarTransacciones]
(
    @NombreArchivo varchar(100),
    @IDUsuario int
)
AS
BEGIN
        /*
        000000    1100 Venta
        220000    1100 Anulacion
        000000    1420 Anulacion online
        000005    1100 Canje
        220005    1100 Devolucion
        000005    2200 Carga
        220005    2200 Descarga
        000005    1420 Anulacion canje
        300000    3300 CuponIN
        */
    
    
    
    --Verifica que el archivo no haya sido utilizado
    IF NOT EXISTS (SELECT NombreArchivo FROM TransaccionesLog WHERE NombreArchivo = @NombreArchivo)        
        BEGIN
            
        --Se agrega TransaccionLog
            INSERT INTO [TransaccionesLog]
                    ([NombreArchivo]
                    ,[IDUsuario], FechaProceso)
                VALUES
                    (@NombreArchivo
                    ,@IDUsuario, getdate())
    
    
        /*Cambio los nombres de las operaciones*/
        UPDATE TransaccionesTmp
        SET Operacion = 'Anulacion'
        WHERE (TipoTransaccion='220000' or TipoMensaje='1420') and Origen='Visa'

        --000005: Puede ser canje o descarga
        UPDATE TransaccionesTmp
        SET Operacion = 'Canje'
        WHERE TipoTransaccion='000005' and CodigoPremio<>'999999' and Origen='Visa' and TipoMensaje<>'1420'
        and NumTarjetaCliente in (select Numero from Tarjetas where TipoTarjeta <> 'G')

        UPDATE TransaccionesTmp
        SET Operacion = 'Descarga', TipoMensaje='2200'
        WHERE TipoTransaccion='000005' and CodigoPremio<>'999999' and Origen='Visa' and 
        NumTarjetaCliente in (select Numero from Tarjetas where TipoTarjeta='G')

        --220005: Puede ser devolucion o descarga
        UPDATE TransaccionesTmp
        SET Operacion = 'Devolucion'
        WHERE TipoTransaccion='220005' and Origen='Visa' and 
        NumTarjetaCliente in (select Numero from Tarjetas where TipoTarjeta<>'G')

        UPDATE TransaccionesTmp
        SET Operacion = 'Descarga', TipoMensaje='2200'
        WHERE TipoTransaccion='220005' and Origen='Visa' and 
        NumTarjetaCliente in (select Numero from Tarjetas where TipoTarjeta='G')

        UPDATE TransaccionesTmp
        SET Operacion = 'Venta'
        WHERE TipoTransaccion='000000' and TipoMensaje <> '1420' and Origen='Visa'
      
        UPDATE TransaccionesTmp
        SET Operacion = 'Carga', TipoMensaje='2200'
        WHERE CodigoPremio='999999' and TipoTransaccion='000005' and Origen='Visa'
      
        /*UPDATE TransaccionesTmp
        SET Operacion = 'Descarga', TipoMensaje='2200'
        WHERE TipoTransaccion='220005' and Origen='Visa'
        AND NumRefOriginal in (SELECT t1.NumReferencia FROM Transacciones t1 WHERE t1.NumReferencia=TransaccionesTmp.NumRefOriginal 
        AND TipoMensaje='2200' AND t1.TipoTransaccion='000005' AND CodigoPremio='999999' AND Origen='Visa' )*/

        --Actualizo Importes
        UPDATE TransaccionesTmp
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados))/100,
        ImporteAhorro = convert(decimal(18,2),convert(int,PuntosDisponibles))/100,
        Descuento = convert(int,codigopremio)
        WHERE Origen='Visa' and TipoMensaje='1100' and (TipoTransaccion='000000' or TipoTransaccion='220000')--  or TipoTransaccion='220005') --Venta, Devolucion,--Anulacion

        UPDATE TransaccionesTmp--Anulacion online
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados))/100,
        ImporteAhorro = convert(decimal(18,2),convert(int,PuntosDisponibles))/100,
        Descuento = convert(int,codigopremio)
        WHERE Origen='Visa' and TipoMensaje='1420' and TipoTransaccion='000000'

        UPDATE TransaccionesTmp--Anulacion online de canje
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados)),--/100,
        ImporteAhorro = 0,
        Descuento = 0
        WHERE Origen='Visa' and TipoMensaje='1420' and TipoTransaccion='000005'
        
        UPDATE TransaccionesTmp--Canje 
        --SET Importe = convert(decimal(18,2),convert(int,puntosingresados))/100,
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados)),--/100,
        ImporteAhorro = 0,
        Descuento = 0
        WHERE Origen='Visa' and TipoMensaje='1100' and TipoTransaccion='000005'
        and Importe is null
      
        UPDATE TransaccionesTmp--Descarga/Carga giftcard
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados)),
        ImporteAhorro = 0,
        Descuento = 0
        WHERE TipoMensaje='2200' 
      
        /*UPDATE TransaccionesTmp--Descarga giftcard
        SET Importe = convert(decimal(18,2),convert(int,puntosingresados)) * (-1),
        ImporteAhorro = 0,
        Descuento = 0
        WHERE TipoMensaje='2200' AND TipoTransaccion='220005'*/
      
        --Actualizo Puntos
        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = (Importe-ImporteAhorro)
        WHERE TipoTransaccion = '000000' and Origen = 'Visa'--Venta

        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = ((PuntosIngresados/10)*-1)
        WHERE TipoTransaccion='220005' and TipoMensaje='1100' and Origen='Visa'--Devolucion

        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = ((PuntosIngresados/10)*-1)
        WHERE TipoMensaje='1420' and Origen='Visa' and TipoTransaccion = '000000'--Anulacion online
      
        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = (PuntosIngresados*-1)
        WHERE TipoTransaccion='000005' and TipoMensaje='1100' and Origen='Visa'--Canje
      
        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = (PuntosIngresados*100)
        WHERE TipoTransaccion='000005' and TipoMensaje='2200' and Origen='Visa'--Carga giftcard

        UPDATE TransaccionesTmp
        SET PuntosAContabilizar = ((PuntosIngresados*100)*-1)
        WHERE TipoTransaccion='220005' and TipoMensaje='2200' and Origen='Visa'--Devolucion giftcard
      
        DECLARE @IDProceso int
        SET @IDProceso = (SELECT IDENT_CURRENT('TransaccionesLog'))
      
        UPDATE TransaccionesTmp
        SET IDProceso = @IDProceso

        --Se agrega Transacciones
        INSERT INTO [Transacciones]
            ([NumEst]
            ,[NumTerminal]
            ,[TipoMensaje]
            ,[TipoTransaccion]
            ,[FechaTransaccion]
            ,[NumCupon]
            ,[NumReferencia]
            ,[NumRefOriginal]
            ,[NumTarjetaCliente]
            ,[CodigoPremio]
            ,[PuntosIngresados]
            ,[PuntosDisponibles]
            ,[Origen]
            ,[Operacion]
            ,[Descripcion]
            ,[Importe],[ImporteAhorro]
            ,[Descuento], UsoRed, PuntosAContabilizar, Arancel, Puntos, IDMarca, IDFranquicia, IDProceso, Usuario)
        SELECT tmp.NumEst,
            tmp.NumTerminal,
            tmp.TipoMensaje,
            tmp.TipoTransaccion,
            tmp.FechaTransaccion,
            tmp.NumCupon,
            tmp.NumReferencia,
            tmp.NumRefOriginal,
            tmp.NumTarjetaCliente,
            tmp.CodigoPremio,
            tmp.PuntosIngresados,
            tmp.PuntosDisponibles,
            --tmp.NombreArchivo,
            --tmp.IDUsuario,
            tmp.Origen,
            tmp.Operacion,
            tmp.Descripcion,
            tmp.Importe,
        tmp.ImporteAhorro,
            tmp.Descuento,
            dbo.GetCostoUsoRed(tmp.NumEst, tmp.TipoMensaje, tmp.Operacion), 
        tmp.PuntosAContabilizar, 
        dbo.GetArancel(tmp.NumEst,tmp.NumTerminal,tmp.NumTarjetaCliente, tmp.TipoMensaje, tmp.Operacion, tmp.TipoTransaccion, tmp.FechaTransaccion),
        dbo.GetPuntos(tmp.NumEst,tmp.NumTerminal,tmp.NumTarjetaCliente, tmp.TipoMensaje, tmp.Operacion, tmp.TipoTransaccion, tmp.FechaTransaccion),
        dbo.GetMarcaTarjeta(tmp.NumTarjetaCliente),
        dbo.GetFranquiciaTarjeta(tmp.NumTarjetaCliente),
        @IDProceso, 'visa'
        --select top 1 POSArancel FROM Comercios WHERE POSTerminal = tmp.NumTerminal and NumEst = tmp.NumEst),
        --(select top 1 POSPuntos FROM Comercios WHERE POSTerminal = tmp.NumTerminal and NumEst = tmp.NumEst)
            FROM TransaccionesTmp as tmp

        /*Modifico los puntos de cada transaccion*/
        UPDATE Transacciones
        SET 
        ImporteAhorro = (SELECT TOP 1 (t1.ImporteAhorro) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa' AND t1.IDProceso = @IDProceso),
        Descuento = (SELECT TOP 1 (t1.Descuento) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa' AND t1.IDProceso = @IDProceso)
        WHERE TipoTransaccion = '220000' and Origen = 'Visa'--Anulacion
        and IDProceso = @IDProceso and TipoMensaje='1100'

        UPDATE Transacciones
        SET 
        ImporteAhorro = (SELECT TOP 1 (t1.ImporteAhorro) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa' AND t1.IDProceso = @IDProceso),
        Descuento = (SELECT TOP 1 (t1.Descuento) FROM Transacciones t1 WHERE t1.NumReferencia=Transacciones.NumRefOriginal AND t1.TipoTransaccion='000000' AND t1.Origen='Visa' AND t1.IDProceso = @IDProceso)
        WHERE Origen = 'Visa'--Anulacion online
        and IDProceso = @IDProceso and TipoMensaje='1420' and TipoTransaccion='000000'
      
        UPDATE Transacciones
        --SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-100)
        SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-1)
        WHERE TipoTransaccion = '220000' and Origen = 'Visa' and TipoMensaje='1100'--Anulacion
        and IDProceso = @IDProceso

        UPDATE Transacciones
        --SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-100)
        SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-1)
        WHERE Origen = 'Visa' and TipoMensaje='1420' and TipoTransaccion='000000'--Anulacion online
        and IDProceso = @IDProceso

        UPDATE Transacciones
        --SET PuntosAContabilizar = ((Importe-ImporteAhorro)*-100)
        SET PuntosAContabilizar = (Importe-ImporteAhorro)
        WHERE Origen = 'Visa' and TipoMensaje='1420' and TipoTransaccion='000005'--Anulacion online de canje
        and IDProceso = @IDProceso

        UPDATE Transacciones
        SET PuntosAContabilizar = (PuntosAContabilizar*100)
        WHERE Origen = 'Visa' and (Operacion='Canje' or (TipoMensaje='1420' and TipoTransaccion='000005'))--Canje o anulacion de canje
        and IDProceso = @IDProceso

        UPDATE Transacciones
        SET PuntosAContabilizar = PuntosAContabilizar*dbo.GetPuntosMultiplica(Transacciones.NumEst,Transacciones.NumTerminal, Transacciones.FechaTransaccion)
        WHERE IDProceso = @IDProceso and Operacion not in ('Carga','Descarga','Canje')
        and TipoTransaccion<>'000005'--Anulacion canje
        --TipoMensaje<>'1420' and 
        
        --Pongo las descargas como negativas
        UPDATE Transacciones
        SET PuntosAContabilizar= PuntosAContabilizar*-1
        WHERE Operacion='Descarga' and PuntosAContabilizar>0 and IDProceso = @IDProceso 

        /*Actualizo los puntos por Tarjeta*/
        UPDATE Tarjetas
        SET PuntosTotales = (
        SELECT ISNULL(SUM(PuntosAContabilizar),0) FROM Transacciones WHERE NumTarjetaCliente=Tarjetas.Numero
        ),
      Credito = (
        SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
          FROM Transacciones WHERE (TipoMensaje='1100' or TipoMensaje='1420') AND NumTarjetaCliente=Tarjetas.Numero
          
        ),
      Giftcard = (
        SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2)))/100,0)
        --SELECT ISNULL(SUM(CAST(PuntosAContabilizar AS DECIMAL(18,2))),0)
        FROM Transacciones WHERE TipoMensaje='2200' AND  NumTarjetaCliente=Tarjetas.Numero
        
      )
      WHERE Tarjetas.Numero IN 
      (SELECT DISTINCT Transacciones.NumTarjetaCliente FROM Transacciones WHERE IDProceso = @IDProceso)
      
      /*Actualizo POSFechaActivacion y POSInvalido de Comercios*/
      UPDATE Comercios
      SET POSInvalido = 0, POSReprogramado=1
      WHERE POSInvalido = 1 and (
        (SELECT COUNT(tr.IDTransaccion) FROM Transacciones tr
        WHERE Comercios.POSTerminal = tr.NumTerminal and Comercios.NumEst = tr.NumEst) > 1
      )
      
      UPDATE Comercios
      SET POSFechaActivacion = GETDATE(), POSReprogramado=1 WHERE POSFechaActivacion is null 
      and (
        (SELECT max(tr.Importe) FROM Transacciones tr
        WHERE Comercios.POSTerminal = tr.NumTerminal and Comercios.NumEst = tr.NumEst) >= 1
      )
      
      UPDATE Comercios
      SET POSReprogramado=1 WHERE POSReprogramado=0
      and (
        (SELECT TOP 1 tr.Importe FROM Transacciones tr
        WHERE Comercios.POSTerminal = tr.NumTerminal and Comercios.NumEst = tr.NumEst ORDER BY tr.FechaTransaccion ) < 1 
      )

        
      END
    ELSE
        BEGIN
            BEGIN TRY
                RAISERROR('El Archivo ya fue utilizado',16,1);
            END TRY
            BEGIN CATCH
                DECLARE @ErrorMessage NVARCHAR(4000);
                DECLARE @ErrorSeverity INT;
                DECLARE @ErrorState INT;
                SELECT 
                    @ErrorMessage = ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE();
                RAISERROR (@ErrorMessage,
                            @ErrorSeverity,
                            @ErrorState);
            END CATCH
        END

END