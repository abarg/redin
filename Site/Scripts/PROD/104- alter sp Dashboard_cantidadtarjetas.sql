USE [RedIN-QA]
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_CantidadTarjetas]    Script Date: 30/12/2015 10:08:23 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_CantidadTarjetas] (@FechaDesde datetime,
  @FechaHasta datetime)                  --Input parameter ,  Studentid of the student 
AS
BEGIN
    /*SELECT '' as label, COUNT(distinct NumTarjetaCliente) as data 
	FROM [DashboardView]  
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	*/

	SELECT '' as label, COUNT(distinct NumTarjetaCliente) as data 
	FROM Transacciones  
	WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta

END
