/****** Object:  StoredProcedure [dbo].[Dashboard_TotalTRMensual]    Script Date: 18/01/2016 12:09:06 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Dashboard_Socios_TotalTRMensual]
(
	@FechaDesde datetime,
  @FechaHasta datetime,
  @IDSocio int
)

AS

  SELECT '' as label, count(IDTransaccion) as data
  FROM Transacciones tr
  inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta AND t.IDSocio=@IDSocio

GO


