USE [RedIN-QA]
GO

/****** Object:  Table [dbo].[TmpArchivosVisa]    Script Date: 28/12/2015 05:50:31 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TmpArchivosVisa](
	[IDTarjeta] [int] NOT NULL,
	[Credito] [decimal](18, 2) NULL,
	[Giftcard] [decimal](18, 2) NULL,
	[IDSocio] [int] NULL,
	[IDSocioPadre] [int] NULL,
	[IDMarca] [int] NULL,
	[FechaBaja] [datetime] NULL,
	[TipoCatalogo] [char](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


