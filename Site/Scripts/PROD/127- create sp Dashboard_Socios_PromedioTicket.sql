/****** Object:  StoredProcedure [dbo].[Dashboard_PromedioTicketMensual]    Script Date: 18/01/2016 12:04:54 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Dashboard_Socios_PromedioTicket]
(
    @IDSocio int
)

AS
  declare @suma decimal(10,2)
  declare @cant decimal(10,2)
  
  SELECT @suma = ISNULL(SUM(CalculoTicket),0)
  FROM DashboardView
  inner join Tarjetas t on NumTarjetaCliente = t.Numero
  WHERE t.IDSocio=@IDSocio
  AND Operacion='Venta'
  
  SELECT @cant = convert(decimal(10,2),count(IDTransaccion))
  FROM DashboardView
  inner join Tarjetas t on NumTarjetaCliente = t.Numero
  WHERE t.IDSocio=@IDSocio
  AND Operacion='Venta'


  if(@cant=0)
    set @cant=1

  SELECT '' as label, @suma/@cant as data
GO


