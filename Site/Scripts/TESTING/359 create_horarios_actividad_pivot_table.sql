create table ActividadHorarios(
IDHorario int IDENTITY(1, 1) NOT NULL primary key, 
IDActividad int not null,
Dia varchar(20) not null, 
Horario varchar(150) not null,
FOREIGN KEY (IDActividad) REFERENCES Actividades(IDActividad),
)