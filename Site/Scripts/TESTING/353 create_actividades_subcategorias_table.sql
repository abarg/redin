create table ActividadSubCategorias(
IDSubCategoria int IDENTITY(1,1) NOT NULL primary key,
SubCategoria varchar(128),
IDCategoria int not null,
FOREIGN KEY (IDCategoria) REFERENCES ActividadCategorias(IDCategoria),
)