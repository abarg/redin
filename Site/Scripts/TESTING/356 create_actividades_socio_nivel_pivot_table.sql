create table ActividadSocioNivel(
IDBeneficioMAsignado int IDENTITY(1,1) not null  primary key, 
IDSocio int not null, 
IDActividad int not null, 
IDNivel int not null,
FOREIGN KEY (IDSocio) REFERENCES Socios(IDSocio),
FOREIGN KEY (IDActividad) REFERENCES Actividades(IDActividad),
FOREIGN KEY (IDNivel) REFERENCES ActividadNiveles(IDNivel),
)