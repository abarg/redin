USE [RedIN]
GO
/****** Object:  StoredProcedure [dbo].[ProcesarSociosTmp]    Script Date: 8/13/2018 1:24:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProcesarBeneficiariosTmp]
( 
	@idFranquicia int
)

AS

BEGIN
	--Inicializo
	UPDATE BeneficiariosTmp set Error=null,IDSocio=null, IDActividad=null,IDNivel=null,IDHorario=null,IDComercio=null
	
	
	--BEGIN TRY
		--BEGIN TRANSACTION


		-- busco socio
		UPDATE BeneficiariosTmp
		SET IDSocio = (select top 1 IDSocio from Socios where Socios.NroDocumento = BeneficiariosTmp.DNI)


		--busco actividad
		UPDATE BeneficiariosTmp
		SET IDActividad = (select top 1 IDActividad from Actividades where Actividades.Actividad = BeneficiariosTmp.Actividad)
	
		--busco nivel
		UPDATE BeneficiariosTmp
		SET IDNivel = (select TOP 1 IDNivel from ActividadNiveles as AN where AN.Nivel = BeneficiariosTmp.Nivel and AN.IDActividad = BeneficiariosTmp.IDActividad)
	
		--busco horario
		UPDATE BeneficiariosTmp
		SET IDHorario = (select TOP 1 IDHorario from ActividadHorarios as AH where AH.Horario = BeneficiariosTmp.Horario and AH.IDActividad = BeneficiariosTmp.IDActividad)

		--busco sede
		UPDATE BeneficiariosTmp
		SET IDComercio = (select top 1 IDComercio from Comercios where lower(Comercios.NombreFantasia) = lower(BeneficiariosTmp.Sede))
		where Error is null-- and IDSocio is null --and IDDomicilio is null


		--Chequeo que no tenga la misma actividad, horario, sede asignados
		UPDATE BeneficiariosTmp
		SET Error = 'El Socio ya tiene asignado la actividad, horario y sede'
		where BeneficiariosTmp.IDActividad = (select top 1 IDActividad from ActividadSocioNivel as ASN where ASN.IDHorario = BeneficiariosTmp.IDHorario)
		

		-- Cargo errores por socios/actividades/niveles/horarios/sedes que no se encontraron
		UPDATE BeneficiariosTmp
		SET Error = 'No se encontro la actividad'
		where BeneficiariosTmp.IDActividad is null

		UPDATE BeneficiariosTmp
		SET Error = 'No se encontro la sede'
		where BeneficiariosTmp.IDComercio is null

		UPDATE BeneficiariosTmp
		SET Error = 'No se encontro el horario'
		where BeneficiariosTmp.IDHorario is null

		UPDATE BeneficiariosTmp
		SET Error = 'No se encontro el nivel'
		where BeneficiariosTmp.IDNivel is null

		UPDATE BeneficiariosTmp
		SET Error = 'No se encontro el socio'
		where BeneficiariosTmp.IDSocio is null

		--Si la relacion (socio-actividad-sede) no existe la creo
		INSERT INTO [dbo].[ActividadSocioNivel]
		([IDSocio],[IDActividad],[IDHorario],[IDNivel],[IDComercio])
		select IDSocio,IDActividad,IDHorario,IDNivel,IDComercio
		from BeneficiariosTmp
		WHERE (Error is null or Error='')

	

END


