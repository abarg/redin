
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_ComisionesMensual]    Script Date: 24/01/2017 01:07:57 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Franq_ComisionesMensual]
(
  @IDFranquicia int,
  @Tipo char(4),
  @FechaDesde datetime,
  @FechaHasta datetime
)

AS
	declare @total decimal(18,2)

  if(@Tipo='TtCp')
  begin
  
    SELECT @total = 
     isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
      dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      --(CalculoArancel*@comisionTtCp)/100 
      ELSE 
      (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
    FROM Comercios c
	  INNER JOIN Terminales ter on ter.IDComercio = c.IDComercio
  	  INNER JOIN Transacciones tr on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )

  
  end
  if(@Tipo='TpCp')
  begin

    SELECT @total = 
    isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
      dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      --(CalculoArancel*@comisionTtCp)/100 
      ELSE 
       (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
    
    
      FROM Comercios c
	  INNER JOIN Terminales ter on ter.IDComercio = c.IDComercio
  	  INNER JOIN Transacciones tr on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )


  
  end
  if(@Tipo='TpCt')
  begin

    SELECT @total = 
    isnull(sum(CASE  WHEN Operacion in('Venta','Carga')  THEN 
       dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0))
      ELSE 
        (-1 * dbo.getComisionFranquicia(@Tipo, @IDFranquicia, c.IDFranquicia,  tr.IDFranquicia, isnull((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)),0)))
      END
      ),0)
    
    FROM Comercios c
	  INNER JOIN Terminales ter on ter.IDComercio = c.IDComercio
  	  INNER JOIN Transacciones tr on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
      INNER JOIN Tarjetas t on tr.NumTarjetaCliente = t.Numero
  	  LEFT JOIN Marcas m on m.IDMarca = t.IDMarca
      WHERE tr.FechaTransaccion BETWEEN @fechaDesde and @fechaHasta
		AND (
			(tr.Importe>1 AND tr.Arancel is not null and tr.Puntos is not null)
			or (operacion='CuponIN') 
		)
      AND (
        t.IDFranquicia = @IDFranquicia or c.IDFranquicia =@IDFranquicia
      )

  
  end
  
	SELECT '' as label, @total as data