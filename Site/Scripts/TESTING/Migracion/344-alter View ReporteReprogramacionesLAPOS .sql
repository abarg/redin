
GO

/****** Object:  View [dbo].[ReporteReprogramacionesLAPOS]    Script Date: 27/01/2017 05:20:12 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER view [dbo].[ReporteReprogramacionesLAPOS] as

select
ter.POSTerminal as NroTerminal,
'Red IN' as MarcaVisa,
'032' as Moneda,
ter.NumEst as NumEst,
7 as CodigoSolicitante,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observacion
from Comercios c
join Terminales ter on ter.IDComercio = c.IDComercio
where c.Activo = 1 and ter.POSSistema = 'LAPOS' and ter.POSTerminal is not null and ter.POSTerminal != '' and ter.Estado != 'DE'
and ter.POSReprogramado=0 and ter.POSFechaReprogramacion is null



GO


