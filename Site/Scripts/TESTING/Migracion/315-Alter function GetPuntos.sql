GO
/****** Object:  UserDefinedFunction [dbo].[GetPuntos]    Script Date: 23/01/2017 03:16:18 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[GetPuntos] 
(   
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16),
    @TipoMensaje varchar(4),
    @Operacion varchar(20),
    @TipoTransaccion varchar(6),
	@Fecha datetime
)

RETURNS decimal (10,2)
AS

BEGIN

DECLARE @puntos decimal (10,2)
DECLARE @sumaPuntos bit
DECLARE @dia int
SET @dia = (select datepart(dw,@Fecha))

if(@Operacion='Canje')
    select @puntos = 0
else
    BEGIN
        if(@TipoMensaje='1420' and @TipoTransaccion='000005')--Anulacion
            select @puntos = 1
        ELSE
            BEGIN
                IF(@TipoMensaje = '1100' or @TipoMensaje = '1420')
					BEGIN
						
						DECLARE @AffinityMarca char(4)
						DECLARE @AffinityTarjeta char(4)
						declare @IDMarca int

						set @IDMarca= (select IDMarca from Terminales t   join Comercios c on c.idcomercio = t.idcomercio where NumEst= @NumEst and POSTerminal=@NumTerminal)
						set @AffinityMarca=(select affinity from Marcas where IDMarca=@IDMarca)

						set @AffinityTarjeta=(select SUBSTRING(@NumTarjetaCliente, 7,4))
						IF(@AffinityTarjeta=@AffinityMarca) /*PUNTOS VIP*/
							BEGIN
								SET @puntos = ISNULL((select TOP 1 p.PuntosVip from PromocionesPuntuales p 
								inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
								inner join Terminales t on pc.IDComercio = t.IDComercio where
								NumEst = @NumEst and POSTerminal=@NumTerminal and CAST(FechaDesde AS DATE)<= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
								IF (@puntos =-1)/*puntos vip sin promocion*/
								BEGIN 
									IF(@dia = 1)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip1,POSPuntos) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										
									ELSE IF(@dia = 2)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip2,Puntos2) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										
									ELSE IF(@dia = 3)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip3,Puntos3) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 4)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip4,Puntos4) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 5)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip5,Puntos5) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 6)
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip6,Puntos6) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
									ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
										SET @puntos = (SELECT TOP 1 isnull(PuntosVip7,Puntos7) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
								END
							END
						else /*puntos no vip*/
							BEGIN
									
									SET @puntos = ISNULL((select TOP 1 p.Puntos from PromocionesPuntuales p 
									inner join PromocionesPorComercio pc on p.IDPromocionesPuntuales=pc.IDPromocionesPuntuales
									inner join Terminales t on pc.IDComercio = t.IDComercio where
									NumEst = @NumEst and POSTerminal=@NumTerminal and CAST (FechaDesde AS DATE) <= CAST(@Fecha AS DATE) and CAST(FechaHasta AS DATE) >= CAST(@Fecha AS DATE)),-1)
									IF (@puntos =-1)--PUNTOS NO VIP SIN PROMOCION
									BEGIN
										IF(@dia = 1)
											SET @puntos = (SELECT TOP 1 isnull(POSPuntos,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 2)
											SET @puntos = (SELECT TOP 1 isnull(Puntos2,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 3)
											SET @puntos = (SELECT TOP 1 isnull(Puntos3,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 4)
											SET @puntos = (SELECT TOP 1 isnull(Puntos4,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 5)
											SET @puntos = (SELECT TOP 1 isnull(Puntos5,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 6)
											SET @puntos = (SELECT TOP 1 isnull(Puntos6,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
										ELSE IF(@dia = 7 OR @dia = 0 )--Domingo
											SET @puntos = (SELECT TOP 1 isnull(Puntos7,0) from Terminales where POSTerminal = @NumTerminal and NumEst = @NumEst)
									END
							END 
					END
                ELSE
                    BEGIN
                        SET @sumaPuntos = (SELECT TOP 1 GiftcardGeneraPuntos FROM Terminales WHERE NumEst = @NumEst)
                        IF(@sumaPuntos=1)
                            SET @puntos = 1
                        else
                            SET @puntos = 0

                    END
            END
    end

if(@puntos=null)
  set @puntos=0

 return @puntos

END




