
UPDATE Terminales set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM Terminales t
inner join Comercios com on com.IDComercio=t.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [AlertasTarjetas] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [AlertasTarjetas] at
inner join Comercios com on com.IDComercio=at.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [Beneficios] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Beneficios] b
inner join Comercios com on com.IDComercio=b.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [FranquiciasComercios] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [FranquiciasComercios] fc
inner join Comercios com on com.IDComercio=fc.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [ImagenesComercios] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [ImagenesComercios] ic
inner join Comercios com on com.IDComercio=ic.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [LiquidacionDetalleTipo] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [LiquidacionDetalleTipo] l
inner join Comercios com on com.IDComercio=l.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [Marcas] set IDComercioFacturanteSMS=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Marcas] m
inner join Comercios com on com.IDComercio=m.IDComercioFacturanteSMS
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio



UPDATE [Movimientos] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Movimientos] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE [NotasCreditoDebito] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [NotasCreditoDebito] n
inner join Comercios com on com.IDComercio=n.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE [Pagos] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Pagos] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio

UPDATE [PromocionesPorComercio] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [PromocionesPorComercio] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio

UPDATE [Sucursales] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Sucursales] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio

UPDATE [VerificacionesPOS] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [VerificacionesPOS] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE [VerificacionesPOSComercios] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [VerificacionesPOSComercios] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE [Transacciones] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [Transacciones] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE [UsuariosComercios] set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM [UsuariosComercios] m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE Alertas set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM Alertas m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE EmpresasComercios set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM EmpresasComercios m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio

UPDATE Facturas set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM Facturas m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


UPDATE PuntosDeVenta set IDComercio=(select min(c.IDComercio) from Comercios AS c inner join Domicilios d on d.IDDomicilio=c.IDDomicilio
 WHERE c.NroDocumento=com.NroDocumento and dom.Domicilio=d.Domicilio GROUP BY c.NroDocumento,d.Domicilio )
FROM PuntosDeVenta m
inner join Comercios com on com.IDComercio=m.IDComercio
inner join Domicilios dom on com.IDDomicilio=dom.IDDomicilio


DELETE Comercios where IDComercio not in(select min(IDComercio) from Comercios c
inner join Domicilios d  on c.IDDomicilio=d.IDDomicilio 
group by c.NroDocumento,d.Domicilio)


