begin tran

	BEGIN TRY  
			
			--RENOMBRO LA TABLA DE COMERCIO PARA QUE ME DEJE CREAR UNA NUEVA 

			IF OBJECT_ID('dbo.[ComerciosVieja]') IS NULL
			begin
				IF OBJECT_ID('dbo.[Comercios]') IS NOT NULL 
				EXEC sp_rename 'Comercios', 'ComerciosVieja';  

				IF OBJECT_ID('dbo.[PK_Comercios]') IS NOT NULL 
				EXEC sp_rename '[PK_Comercios]', '[PK_ComerciosViejo]';  

				IF OBJECT_ID('dbo.[FK_Comercios_Contactos]') IS NOT NULL 			
				EXEC sp_rename '[FK_Comercios_Contactos]', '[FK_Comercios_ContactosViejo]';  

				IF OBJECT_ID('dbo.[FK_Comercios_Dealer]') IS NOT NULL 	
				EXEC sp_rename '[FK_Comercios_Dealer]', '[FK_Comercios_DealerViejo]';  

				IF OBJECT_ID('dbo.[FK_Comercios_Domicilios]') IS NOT NULL 	
				EXEC sp_rename '[FK_Comercios_Domicilios]', '[FK_Comercios_DomiciliosViejo]'; 

				IF OBJECT_ID('dbo.[FK_Comercios_DomiciliosFiscal]') IS NOT NULL 				 
				EXEC sp_rename '[FK_Comercios_DomiciliosFiscal]', '[FK_Comercios_DomiciliosFiscalViejo]'; 

				IF OBJECT_ID('dbo.[FK_Comercios_Franquicias]') IS NOT NULL 	
				EXEC sp_rename '[FK_Comercios_Franquicias]', '[FK_Comercios_FranquiciasViejo]';  
			
				IF OBJECT_ID('dbo.[FK_Comercios_Marcas]') IS NOT NULL 	
				EXEC sp_rename '[FK_Comercios_Marcas]', '[FK_Comercios_MarcasViejo]';  

				IF OBJECT_ID('dbo.[FK_Comercios_Rubros]') IS NOT NULL 	
				EXEC sp_rename '[FK_Comercios_Rubros]', '[FK_Comercios_RubrosViejo]';  

				IF OBJECT_ID('dbo.[FK_Comercios_SubRubros]') IS NOT NULL
				EXEC sp_rename '[FK_Comercios_SubRubros]', '[FK_Comercios_SubRubrosViejo]'; 

				IF OBJECT_ID('dbo.[FK_Comercios_Zonas]') IS NOT NULL			 
				EXEC sp_rename '[FK_Comercios_Zonas]', '[FK_Comercios_ZonasViejo]';  
				
				PRINT   N'Se termino de renombrar las foraneas de comercios'
			end
			else
				begin 
					PRINT   N'La tabla de Comercios ya fue renombrada a ComerciosVieja y sus respectivas foraneas'
				end


			CREATE TABLE [dbo].[Comercios](
				[IDComercio] [int] IDENTITY(1,1) NOT NULL,
				[SDS] [varchar](50) NOT NULL,
				[NombreFantasia] [varchar](100) NOT NULL,
				[RazonSocial] [varchar](128) NOT NULL,
				[TipoDocumento] [varchar](15) NULL,
				[NroDocumento] [varchar](20) NULL,
				[Telefono] [varchar](50) NULL,
				[Celular] [varchar](50) NULL,
				[Responsable] [varchar](128) NULL,
				[Cargo] [varchar](128) NULL,
				[Actividad] [varchar](128) NULL,
				[CondicionIva] [varchar](50) NULL,
				[Web] [varchar](255) NULL,
				[Email] [varchar](128) NULL,
				[FechaAlta] [smalldatetime] NOT NULL,
				[IDDomicilio] [int] NOT NULL,
				[IDDomicilioFiscal] [int] NOT NULL,
			
				[Observaciones] [text] NULL,

				[NombreEst] [varchar](100) NULL,

				[Activo] [bit] NOT NULL,
				[FormaPago] [char](1) NULL,
				[FormaPago_Banco] [varchar](50) NULL,
				[FormaPago_TipoCuenta] [char](1) NULL,
				[FormaPago_NroCuenta] [varchar](50) NULL,
				[FormaPago_CBU] [varchar](50) NULL,
				[FormaPago_Tarjeta] [varchar](25) NULL,
				[FormaPago_BancoEmisor] [varchar](25) NULL,
				[FormaPago_NroTarjeta] [varchar](25) NULL,
				[FormaPago_FechaVto] [varchar](6) NULL,
				[FormaPago_CodigoSeg] [varchar](5) NULL,
				[IDContacto] [int] NULL,
				[Rubro] [int] NULL,

				[FechaAltaDealer] [datetime] NULL,		

				[IDMarca] [int] NULL,

				[EmailsEnvioFc] [text] NULL,
				[CasaMatriz] [bit] NOT NULL,
				[IDFranquicia] [int] NULL,
				[Logo] [varchar](50) NULL,
				[FichaAlta] [varchar](50) NULL,

				[IDZona] [int] NULL,

				[Url] [varchar](256) NULL,
				[EmailAlertas] [varchar](128) NULL,				
				[CelularAlertas] [varchar](50) NULL,				
				[EmpresaCelularAlertas] [varchar](50) NULL,		

				[IDDealer] [int] NULL,
				[CostoFijo] [decimal](18, 0) NULL,
				[IDSubRubro] [int] NULL,

				[twitter] [varchar](256) NULL,

				[ConRetenciones] [bit] NOT NULL,
			 CONSTRAINT [PK_Comercios] PRIMARY KEY CLUSTERED 
			(
				[IDComercio] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


			PRINT   N'Se termino de crear la tabla de comercios'

			SET ANSI_PADDING OFF

			ALTER TABLE [dbo].[Comercios] ADD  DEFAULT ((1)) FOR [CasaMatriz]
			PRINT   N'Alter successful [CasaMatriz]'



			ALTER TABLE [dbo].[Comercios] ADD  DEFAULT ((0)) FOR [ConRetenciones]
			PRINT   N'Alter successful [ConRetenciones]'



			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Contactos] FOREIGN KEY([IDContacto])
			REFERENCES [dbo].[Contactos] ([IDContacto])
			ON DELETE CASCADE

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Contactos]
			PRINT   N'Alter successful [FK_Comercios_Contactos]'



			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Dealer] FOREIGN KEY([IDDealer])
			REFERENCES [dbo].[Dealer] ([IDDealer])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Dealer]
			PRINT   N'Alter successful [FK_Comercios_Dealer]'




			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Domicilios] FOREIGN KEY([IDDomicilio])
			REFERENCES [dbo].[Domicilios] ([IDDomicilio])
			ON DELETE CASCADE

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Domicilios]
			PRINT   N'Alter successful [FK_Comercios_Domicilios]'




			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_DomiciliosFiscal] FOREIGN KEY([IDDomicilioFiscal])
			REFERENCES [dbo].[Domicilios] ([IDDomicilio])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_DomiciliosFiscal]
			PRINT   N'Alter successful [FK_Comercios_DomiciliosFiscal]'



			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Franquicias] FOREIGN KEY([IDFranquicia])
			REFERENCES [dbo].[Franquicias] ([IDFranquicia])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Franquicias]
			PRINT   N'Alter successful [FK_Comercios_Franquicias]'



			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Marcas] FOREIGN KEY([IDMarca])
			REFERENCES [dbo].[Marcas] ([IDMarca])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Marcas]
			PRINT   N'Alter successful [FK_Comercios_Marcas]'


			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Rubros] FOREIGN KEY([Rubro])
			REFERENCES [dbo].[Rubros] ([IDRubro])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Rubros]
			PRINT   N'Alter successful [FK_Comercios_Rubros]'


			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_SubRubros] FOREIGN KEY([IDSubRubro])
			REFERENCES [dbo].[Rubros] ([IDRubro])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_SubRubros]
			PRINT   N'Alter successful [FK_Comercios_SubRubros]'



			ALTER TABLE [dbo].[Comercios]  WITH CHECK ADD  CONSTRAINT [FK_Comercios_Zonas] FOREIGN KEY([IDZona])
			REFERENCES [dbo].[Zonas] ([IDZona])

			ALTER TABLE [dbo].[Comercios] CHECK CONSTRAINT [FK_Comercios_Zonas]
			PRINT   N'Alter successful [FK_Comercios_Zonas]'


			COMMIT TRAN
			PRINT   N'The transaction is committable.' +  
            'Commit successful.'  
	END TRY  


	BEGIN CATCH  		  
			ROLLBACK TRAN
			PRINT    N'Rollback transaction.' +  
            'No se pudo terminar la ejecucion correctamente.'  
	END CATCH;  
	GO  
