
GO

/****** Object:  View [dbo].[TransaccionesMarcasView]    Script Date: 19/01/2017 03:30:10 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











ALTER VIEW [dbo].[TransaccionesMarcasView]
AS


select --distinct
		tr.IDTransaccion,
		convert(varchar(10), tr.FechaTransaccion, 103) as Fecha,
		convert(varchar(10), tr.FechaTransaccion, 108) as Hora,
		tr.FechaTransaccion as FechaTransaccion,
		tr.Origen,
    tr.Operacion as Operacion,
    tr.NumTerminal as POSTerminal,
		c.IDComercio,
    c.SDS,
    c.NombreFantasia,
    c.RazonSocial,
		c.NombreEst as Empresa,
		ter.NumEst as NroEstablecimiento,
    c.NroDocumento as NroDocumento,
		t.Numero,
		--s.Nombre as Nombre,
		--s.Apellido as Apellido,
    t.IDSocio,
    (select Nombre from Socios where Socios.IDSocio = t.IDSocio) as Nombre,
		(select Apellido from Socios where Socios.IDSocio = t.IDSocio) as Apellido,
    (select NroDocumento from Socios where Socios.IDSocio = t.IDSocio) as NroDocumentoSocio,
		isnull(tr.Importe,0) as ImporteOriginal,
		isnull(tr.ImporteAhorro,0) as ImporteAhorro,
		tr.PuntosAContabilizar as PuntosAContabilizar,
		isnull(t.PuntosTotales,0) as PuntosTotales,
    isnull(t.Credito,0) as Credito,
    isnull(t.Giftcard,0) as Giftcard,
    t.IDMarca,
    c.IDMarca as IDMarcaComercio,
    isnull(d.Domicilio,'') as DomicilioComercio,
    tr.UsoRed as CostoRedIn,
		(tr.Importe - tr.ImporteAhorro + tr.UsoRed) as Ticket,
		(tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)) as Arancel,
		(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos,
		(((tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100))) + ((tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100))) + tr.UsoRed) as NetoGrabado
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio
		inner join Tarjetas t on tr.NumTarjetaCliente = t.Numero
    left join Domicilios d on c.IDDomicilio = d.IDDomicilio and d.TipoDomicilio='C' and d.Entidad = 'C'
    where (t.IDMarca is not null or c.IDMarca is not null)









GO


