	if((select count(*) from comercios) = 0)
		begin 
		--DESACTIVAR EL IDENTITY IDComercio de tabla Comercios  MANUALMENTE
				
		--SET IDENTITY_INSERT Comercios ON		

		insert into Comercios  (
					 IDComercio
					,[SDS]
					,[NombreFantasia]
					,[RazonSocial]
					,[TipoDocumento]
					,[NroDocumento]
					,[Telefono]
					,[Celular]
					,[Responsable]
					,[Cargo]
					,[Actividad]
					,[CondicionIva]
					,[Web]
					,[Email]
					,[FechaAlta]
					,[IDDomicilio]
					,[IDDomicilioFiscal]

					,[Observaciones]

					,[NombreEst]

					,[Activo]
					,[FormaPago]
					,[FormaPago_Banco]
					,[FormaPago_TipoCuenta]
					,[FormaPago_NroCuenta]
					,[FormaPago_CBU]
					,[FormaPago_Tarjeta]
					,[FormaPago_BancoEmisor]
					,[FormaPago_NroTarjeta]
					,[FormaPago_FechaVto]
					,[FormaPago_CodigoSeg]
					,[IDContacto]
					,[Rubro]

					,[FechaAltaDealer]

					,[IDMarca]
					,[EmailsEnvioFc]
					,[CasaMatriz]

					,[IDFranquicia]
					,[Logo]
					,[FichaAlta]
					,[IDZona]

					,[Url]
					,[EmailAlertas]
					,[CelularAlertas]
					,[EmpresaCelularAlertas]

					,[IDDealer]
					,[CostoFijo]
					,[IDSubRubro]
					,[twitter]
					,[ConRetenciones]
					)
		select
					IDComercio
					,[SDS]
					,[NombreFantasia]
					,[RazonSocial]
					,[TipoDocumento]
					,[NroDocumento]
					,[Telefono]
					,[Celular]
					,[Responsable]
					,[Cargo]
					,[Actividad]
					,[CondicionIva]
					,[Web]
					,[Email]
					,[FechaAlta]
					,[IDDomicilio]
					,[IDDomicilioFiscal]

					,[Observaciones]

					,[NombreEst]

					,[Activo]
					,[FormaPago]
					,[FormaPago_Banco]
					,[FormaPago_TipoCuenta]
					,[FormaPago_NroCuenta]
					,[FormaPago_CBU]
					,[FormaPago_Tarjeta]
					,[FormaPago_BancoEmisor]
					,[FormaPago_NroTarjeta]
					,[FormaPago_FechaVto]
					,[FormaPago_CodigoSeg]
					,[IDContacto]
					,[Rubro]

					,[FechaAltaDealer]

					,[IDMarca]
					,[EmailsEnvioFc]
					,[CasaMatriz]

					,[IDFranquicia]
					,[Logo]
					,[FichaAlta]
					,[IDZona]

					,[Url]
					,[EmailAlertas]
					,[CelularAlertas]
					,[EmpresaCelularAlertas]

					,[IDDealer]
					,[CostoFijo]
					,[IDSubRubro]
					,[twitter]
					,[ConRetenciones]
					from ComerciosVieja;

			--SET IDENTITY_INSERT comercios OFF
			end

		--ACTIVAR EL IDENTITY MANUALMENTE


			--FALTA HACER LA MIGRACION CON LOS COMERCIOS AGRUPADOS Y QUE IMPACTE EN TODAS LAS TABLAS QUE INVOLUCRAN IDCOMERCIO
			--TAMBIEN HAY QUE MOIFICAR TODAS LAS TABLAS A MANO QUE APUNTEN A COMERCIOS PARA QUE LA CLAVE FORANEA APUNTE A LA TABLA DE COMERCIOS NUEVA CREADA,
				-- YA QUE AHORA APUNTAN A "COMERCIOS VIEJA" QUE ES LA ANTERIOR TABLA DE COMERCIOS RENOMBRADA PARA PODER CREAR LA NUEVA