ALTER TABLE Transacciones
ADD  IDComercio int not null default(2)
FOREIGN KEY REFERENCES Comercios(IDComercio)

ALTER TABLE Transacciones
ADD  IDTerminal int not null default(3)
FOREIGN KEY REFERENCES Terminales(IDTerminal)
