USE [RedIN-QA]
GO
/****** Object:  UserDefinedFunction [dbo].[GetIDComercio]    Script Date: 23/01/2017 05:26:30 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE FUNCTION [dbo].[GetIDComercio] 
(  
    @NumEst  varchar(20),
    @NumTerminal  varchar(10)
)

RETURNS INT
AS

BEGIN

declare @IDComercio int
set @IDComercio=(select IDComercio from Terminales where NumEst=@NumEst and POSTerminal=@NumTerminal )
return @IDComercio

END

