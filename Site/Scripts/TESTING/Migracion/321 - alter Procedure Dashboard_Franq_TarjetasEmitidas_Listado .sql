
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TarjetasEmitidas_Listado]    Script Date: 24/01/2017 01:25:51 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_Franq_TarjetasEmitidas_Listado]
(@idFranquicia int)
AS

select distinct
m.Nombre as Marca,
count(t.IDMarca) as Total,
(
  select isnull(SUM(cantidad),0) from (
  select 1 as cantidad
  FROM Transacciones tr
  inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
  inner join Comercios c on c.IDComercio = ter.IDComercio
  inner join Tarjetas tt on tr.NumTarjetaCliente = tt.Numero
  WHERE tr.Importe > 1 and tt.IDMarca = m.IDMarca and tt.IDFranquicia = @idFranquicia
  group by NumTarjetaCliente) as R
) as Activas
from Marcas m
inner join Tarjetas t on t.IDMarca=m.IDMarca
where t.IDFranquicia = @idFranquicia
group by m.Nombre,m.IDMarca
order by Activas desc