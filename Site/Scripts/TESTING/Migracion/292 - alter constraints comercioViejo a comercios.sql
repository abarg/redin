begin tran

BEGIN TRY  
--EJECUTARLO SOLO 1 VEZ!! Crea 1 relacion de todas las tablas que apuntan a comercios por cada vez que se ejecuta

	ALTER TABLE [dbo].[AlertasTarjetas]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])



	ALTER TABLE [dbo].[Beneficios] DROP CONSTRAINT [FK__Beneficio__IDCom__009508B4]

	ALTER TABLE [dbo].[Beneficios]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[FranquiciasComercios]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])



	ALTER TABLE [dbo].[FranquiciasComercios]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	--	ALTER TABLE [dbo].[HistorialTerminales]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	--REFERENCES [dbo].[Comercios] ([IDComercio])

	ALTER TABLE [dbo].[ImagenesComercios]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[LiquidacionDetalleTipo]  WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])



	ALTER TABLE [dbo].[Marcas] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[Movimientos] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[NotasCreditoDebito] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[Pagos] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	ALTER TABLE [dbo].[PromocionesPorComercio] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[Sucursales] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	ALTER TABLE [dbo].[VerificacionesPOS] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])




	ALTER TABLE [dbo].[VerificacionesPOSComercios] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])


	
	ALTER TABLE [dbo].[Transacciones] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])



	ALTER TABLE [dbo].[UsuariosComercios] WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	
	ALTER TABLE Alertas WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	ALTER TABLE EmpresasComercios WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	ALTER TABLE Facturas WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

		ALTER TABLE PuntosDeVenta WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])



	ALTER TABLE Terminales WITH CHECK ADD FOREIGN KEY([IDComercio])
	REFERENCES [dbo].[Comercios] ([IDComercio])

	-- delete EmpresasComercios where IDComercio   not in (select IDComercio from Comercios)

	commit tran
END TRY  
BEGIN CATCH  
     		rollback tran
END CATCH  
 
