
/****** Object:  View [dbo].[TransaccionesFacturacionView]    Script Date: 17/01/2017 03:01:21 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[TransaccionesFacturacionView]
AS


select 
		tr.IDTransaccion,
		convert(varchar(10), tr.FechaTransaccion, 103) as Fecha,
		convert(varchar(10), tr.FechaTransaccion, 108) as Hora,
		tr.FechaTransaccion as FechaTransaccion,
		tr.Origen,
    tr.Operacion as Operacion,
    tr.NumTerminal as POSTerminal,
		c.IDComercio,
    c.SDS,
    c.NombreFantasia,
    c.RazonSocial,
		c.NombreEst as Empresa,
		ter.NumEst as NroEstablecimiento,
    c.NroDocumento as NroDocumento,
		tr.NumTarjetaCliente as Numero,
		isnull(tr.Importe,0) as ImporteOriginal,
		isnull(tr.ImporteAhorro,0) as ImporteAhorro,
		tr.PuntosAContabilizar as PuntosAContabilizar,
		0 as PuntosTotales,
    0 as Credito,
    0 as Giftcard,
    tr.IDMarca,
    '' as Marca,
    tr.IDFranquicia,
    tr.UsoRed as CostoRedIn,
		(tr.Importe - tr.ImporteAhorro + tr.UsoRed) as Ticket,
		(tr.Arancel * ((tr.Importe - tr.ImporteAhorro) / 100)) as Arancel,
		(tr.Puntos * ((tr.Importe - tr.ImporteAhorro) / 100)) as Puntos
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio

GO


