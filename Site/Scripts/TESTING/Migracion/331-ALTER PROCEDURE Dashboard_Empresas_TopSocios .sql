
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Empresas_TopSocios]    Script Date: 27/01/2017 03:33:20 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Empresas_TopSocios]
(
	@IDEmpresa int
)
AS
	select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		inner join Comercios c on c.IDComercio = tr.IDComercio 
		inner join EmpresasComercios e on e.IDComercio = c.IDComercio
		where t.IDSocio is not null and tr.Importe>1
		AND e.IDEmpresa = @IDEmpresa
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	order by cuatro desc
