
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Franq_TopComercios]    Script Date: 24/01/2017 01:15:13 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Dashboard_Franq_TopComercios]
(
	@IDFranquicia int
)

as

	SELECT  top 10 
	uno, dos, tres, sum(cuatro) as cuatro
	FROM (
		select distinct 
			c.NombreFantasia as uno, 
			d.Domicilio as dos,
			'' as tres,
			tr.Operacion,
			isnull(CASE tr.Operacion WHEN 'Venta' THEN SUM(tr.Importe) WHEN 'Carga' THEN SUM(tr.Importe) ELSE SUM(tr.Importe*-1) END,0) AS cuatro
		from Transacciones tr
		inner join Terminales ter on ter.POSTerminal = tr.NumTerminal and ter.NumEst = tr.NumEst
		inner join Comercios c on c.IDComercio = ter.IDComercio
		inner join Domicilios d on c.IDDomicilio=d.IDDomicilio
		where tr.Importe>1
		AND c.IDFranquicia = @IDFranquicia
		group by c.NombreFantasia, d.Domicilio, tr.Operacion
	
	) AS T
	where cuatro>0
	group by uno, dos, tres
	order by cuatro desc
