
/****** Object:  StoredProcedure [dbo].[Dashboard_Marcas_TotalTasaUsoMensual]    Script Date: 27/01/2017 04:19:24 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [Dashboard_Marcas_TotalTasaUsoMensual] 10,'2015-12-01','2015-12-31' 
--1.5054359884623

ALTER PROCEDURE [dbo].[Dashboard_Marcas_TotalTasaUsoMensual]
(
  @IDMarca int,
  @FechaDesde datetime,
  @FechaHasta datetime
)

AS
	declare @POSPropios bit
	SELECT @POSPropios = MostrarSoloPOSPropios FROM Marcas where IDMarca=@IDMarca

	declare @suma decimal(10,2)
	declare @cant decimal(10,2)

if(@POSPropios=1)
begin
	 SELECT @suma = COUNT(distinct NumTarjetaCliente) 
	  FROM Transacciones  tr
	  inner join Comercios c on c.IDComercio = tr.IDComercio
	  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	  and tr.IDMarca = @IDMarca and c.IDMarca=@IDMarca
	  and Importe>1 and Arancel is not null and Puntos is not null
	  AND Operacion not in ('Carga','Descarga')

	SELECT @cant = convert(decimal(10,2),count(tr.IDTransaccion)) 
	  FROM Transacciones  tr
	  inner join Comercios c on c.IDComercio = tr.IDComercio 
	  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	  and tr.IDMarca = @IDMarca and c.IDMarca=@IDMarca
	  and Importe>1 and Arancel is not null and Puntos is not null
	  AND Operacion not in ('Carga','Descarga')

end
else
begin

	SELECT @suma = COUNT(distinct NumTarjetaCliente) 
	  FROM Transacciones  tr
	  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	  and tr.IDMarca = @IDMarca 
	  and Importe>1 and Arancel is not null and Puntos is not null
	  AND Operacion not in ('Carga','Descarga')

	SELECT @cant = convert(decimal(10,2),count(tr.IDTransaccion)) 
	  FROM Transacciones  tr
	  WHERE FechaTransaccion >= @FechaDesde AND FechaTransaccion <= @FechaHasta
	  and tr.IDMarca = @IDMarca 
	  and Importe>1 and Arancel is not null and Puntos is not null
	  AND Operacion not in ('Carga','Descarga')


end  
   
	if(@suma is null or @suma=0)
		set @suma=1
  

	SELECT '' as label, @cant/@suma as data