
GO
/****** Object:  StoredProcedure [dbo].[GetReporteTerminalesEmpresas]    Script Date: 27/01/2017 04:54:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetReporteTerminalesEmpresas]
(
	@Estado varchar(50),
	@SDS varchar(50),
	@NombreEst varchar(100),
	@IDEmpresa int
)
AS

SELECT 
c.SDS,
c.NombreFantasia as 'Nombre',
d.Domicilio,
e.IDEmpresa,
e.Nombre as 'NombreEmpresa',
ci.Nombre as 'Localidad',
isnull(convert(varchar(10), c.FechaAlta, 103),'') as 'Fecha Carga',
isnull(convert(varchar(10), c.FechaAltaDealer, 103),'') as 'Fecha Alta',
'' as 'Dealer',
ter.POSTipo as Tipo,
ter.POSSistema as 'Tipo Terminal',
ter.POSTerminal as 'Terminal',
ter.POSEstablecimiento as 'Establecimiento',
isnull(convert(varchar(10), ter.POSFechaActivacion, 103),'') as 'Fecha Activacion',
isnull(convert(varchar(10), ter.POSFechaReprogramacion, 103),'') as 'Fecha Reprogramacion',
ter.POSReprogramado as 'Reprogramado',
ter.POSInvalido as 'Comercio Inválido',
ter.Activo as 'Activo',
ter.POSObservaciones  as 'Observaciones',
ISNULL((
SELECT count(IDTransaccion) 
FROM Transacciones tr WHERE c.IDComercio = tr.IDComercio 
AND tr.Importe>1 AND tr.Origen='Visa'
),0) as 'CantTR',
ISNULL((
SELECT top 1 ISNULL(Importe,0)
FROM Transacciones tr
 WHERE c.IDComercio = tr.IDComercio 
AND tr.Origen='Visa' and TipoTransaccion<>'000005' order by FechaTransaccion desc
),0) as 'UltimoImporte',
c.IDFranquicia
FROM Comercios c
join Terminales ter on ter.IDComercio = c.IDComercio
LEFT JOIN Domicilios d ON c.IDDomicilio = d.IDDomicilio
LEFT JOIN Ciudades ci on ci.IDCiudad = d.Ciudad
LEFT JOIN EmpresasComercios ec on  ec.IDComercio=c.IDComercio
inner JOIN Empresas e on  e.IDEmpresa= ec.IDEmpresa
WHERE
(@SDS is null OR c.SDS like '%'+@SDS)
AND (@NombreEst is null OR  c.NombreEst like '%'+@NombreEst+'%')
AND ( e.IDEmpresa = @IDEmpresa)

