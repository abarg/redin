Alter table HistorialTerminales add IDTerminal int null
go
update HistorialTerminales set IDTerminal = (select top 1 IDTerminal from Terminales t where t.IDComercio = HistorialTerminales.IDComercio)
go
Alter table HistorialTerminales alter column IDTerminal int not null
go
ALTER TABLE HistorialTerminales ADD CONSTRAINT FK_IDTerminal FOREIGN KEY (IDTerminal) REFERENCES Terminales(IDTerminal)
go

ALTER TABLE [dbo].[HistorialTerminales] DROP CONSTRAINT [FK_HistorialTerminales_Comercios]
go
Alter table HistorialTerminales drop column IDComercio
go
