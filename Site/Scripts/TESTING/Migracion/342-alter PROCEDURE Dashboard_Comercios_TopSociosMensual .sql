
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_Comercios_TopSociosMensual]    Script Date: 27/01/2017 05:10:36 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Dashboard_Comercios_TopSociosMensual]
	-- Add the parameters for the stored procedure here
	(@FechaDesde datetime,
    @FechaHasta datetime,
	@IDComercio int) 
AS
BEGIN

		select  top 10 s.Nombre + ', '+ s.Apellido as uno, 
		t.Numero as dos,
		'' as tres,
		sum(isnull(CASE tr.Operacion WHEN 'Venta' THEN tr.Importe ELSE tr.Importe*-1 END,0)) AS cuatro
		from Tarjetas t
		inner join Transacciones tr on tr.NumTarjetaCliente = t.Numero
		inner join Socios s on s.IDSocio = t.IDSocio
		inner join Comercios c on  c.IDComercio = tr.IDComercio
		where t.IDSocio is not null and tr.Importe>1
		AND c.IDComercio = @IDComercio and tr.FechaTransaccion >= @FechaDesde and tr.FechaTransaccion <= @FechaHasta
		group by t.Numero, s.Nombre, s.Apellido, tr.Operacion	
END

