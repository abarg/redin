
GO
/****** Object:  StoredProcedure [dbo].[Dashboard_TerminalesActivos]    Script Date: 18/01/2017 01:53:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Dashboard_TerminalesActivos]
WITH EXEC AS CALLER
AS
select distinct POSSistema + ' ('+cast(count(idcomercio) as varchar(10))+')' as label, count(idcomercio) as data from terminales
where Activo=1 group by POSSistema 
union all
select distinct 'INACTIVOS' + ' ('+cast(count(idcomercio) as varchar(10))+')' as label, count(idcomercio) as data from terminales
where Activo=0