GO
/****** Object:  UserDefinedFunction [dbo].[EsVip]    Script Date: 23/01/2017 03:20:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








ALTER FUNCTION [dbo].[EsVip] 
(   @idMarca int,
    @NumEst  varchar(20),
    @NumTerminal  varchar(10),
    @NumTarjetaCliente varchar(16)
)

RETURNS bit
AS

BEGIN

declare @esVip bit
set @esVip=0
DECLARE @AffinityMarca char(4)
DECLARE @AffinityTarjeta char(4)
declare @IDMarcaCom int
set @IDMarcaCom=@idMarca
if(@idMarca=0)
	set @IDMarcaCom= (select IDMarca from Terminales t join Comercios c on c.IDComercio = t.idcomercio where NumEst= @NumEst and POSTerminal=@NumTerminal)
set @AffinityMarca=(select affinity from Marcas where IDMarca=@IDMarcaCom)

set @AffinityTarjeta=(select SUBSTRING(@NumTarjetaCliente, 7,4))
IF(@AffinityTarjeta=@AffinityMarca) /*PUNTOS VIP*/
	set @esVip=1
return @esVip

END

