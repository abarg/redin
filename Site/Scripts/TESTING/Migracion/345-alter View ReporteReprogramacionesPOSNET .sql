
GO

/****** Object:  View [dbo].[ReporteReprogramacionesPOSNET]    Script Date: 27/01/2017 05:21:06 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[ReporteReprogramacionesPOSNET] as

select
ter.POSTerminal as NroTerminal,
m.Nombre as Marca,
ter.NumEst as NumEst,
c.RazonSocial as RazonSocial,
c.NombreFantasia,
c.NroDocumento as CUIT,
(select top 1 ObservacionesGenerales from VerificacionesPOS v where v.IDComercio= c.IDComercio order by v.fechaprueba desc) as Observacion
from Comercios c
join Terminales ter on ter.IDComercio = c.IDComercio
inner join Marcas m on c.IDMarca=m.IDMarca
where c.Activo = 1 and ter.POSSistema = 'POSNET' and ter.POSTerminal is not null and ter.POSTerminal != '' and ter.Estado != 'DE'
and ter.POSReprogramado=0 and ter.POSFechaReprogramacion is null



GO


