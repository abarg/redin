USE [RedIN-QA]
GO
/****** Object:  UserDefinedFunction [dbo].[EsVip]    Script Date: 23/01/2017 05:23:37 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








create FUNCTION [dbo].[GetIDTerminal] 
(  
    @NumEst  varchar(20),
    @NumTerminal  varchar(10)
)

RETURNS int
AS

BEGIN

declare @IDTerminal int
set @IDTerminal=(select IDTerminal from Terminales where NumEst=@NumEst and POSTerminal=@NumTerminal )
return @IDTerminal

END

