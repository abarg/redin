create table ActividadTerminal(
IDBeneficioMTerminal int IDENTITY(1, 1) NOT NULL primary key, 
IDTerminal int not null, 
IDActividad int not null,
FOREIGN KEY (IDTerminal) REFERENCES Terminales(IDTerminal),
FOREIGN KEY (IDActividad) REFERENCES Actividades(IDActividad),
)