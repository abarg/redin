CREATE TABLE [dbo].[BeneficiariosTmp](
	[IDBeneficiarioTmp] [int] IDENTITY(1,1) NOT NULL,
	[DNI] [varchar](20) COLLATE Traditional_Spanish_CI_AI NOT NULL,
	[Actividad] [varchar](100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	[Nivel] [varchar](100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	[Horario] [varchar](100) COLLATE Modern_Spanish_CI_AS NOT NULL,
	[Sede] [varchar](100) COLLATE Traditional_Spanish_CI_AI NOT NULL,
	[IDSocio] [int] NULL,
	[IDActividad] [int] NULL,
	[IDNivel] [int] NULL,
	[IDHorario] [int] NULL,
	[IDComercio] [int] NULL,
	[Error] [varchar](500) NULL
) ON [PRIMARY]
GO


