create table Actividades(
IDActividad int IDENTITY(1,1) NOT NULL primary key,
Actividad varchar(128),
IDSubCategoria int not null,
FOREIGN KEY (IDSubCategoria) REFERENCES ActividadSubCategorias(IDSubCategoria),
)