create table ActividadNiveles(
IDNivel int IDENTITY(1,1) NOT NULL primary key,
Nivel varchar(128),
IDActividad int not null,
FOREIGN KEY (IDActividad) REFERENCES Actividades(IDActividad),
)